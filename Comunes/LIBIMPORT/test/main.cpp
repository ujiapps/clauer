#include <windows.h>

#include <LIBRT/LIBRT.h>
#include "../LIBIMPORT.h"


#include <iostream>

using namespace std;


void main (void)
{

	USBCERTS_HANDLE hClauer;


	LIBRT_Ini();


	if ( LIBRT_IniciarDispositivo(1, "00359570", &hClauer) != 0 ) {
		cerr << "ERROR 1" << endl;
		return;
	}


	if ( !LIBIMPORT_ImportarPKCS12 ("C:\\yo.p12", "00359570", &hClauer) ) {
		cerr << "ERROR 2" << endl;
		return;
	}


	LIBRT_FinalizarDispositivo(&hClauer);


}