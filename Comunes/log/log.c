#include "log.h"

#include <stdio.h>
#include <stdarg.h>
#include <time.h>

#ifdef LINUX 
#include <unistd.h>
#elif defined(WIN32)
#include <windows.h>
#endif


static FILE *g_hLog = NULL;
static int g_logLevel = 0;

/* Indica si hacemos log o no */
int g_bClauerLOG;     





unsigned long GetPID (void)
{
  /* 
   * Both linux and windows assigns a different pid for
   * the threads
   */

#ifdef LINUX
  return getpid();
#elif defined(WIN32)
  return GetCurrentThreadId();
  //return GetCurrentProcessId();
#endif
  
}





int LOG_Ini_Func ( int where, int level, char *szOutFile )
{
  if ( ! g_hLog ) {
    switch ( where ) {
    case LOG_WHERE_FILE:

      if ( ! szOutFile )
	return -1;

      g_hLog = fopen (szOutFile, "a");
      if ( ! g_hLog ) 
	return -1;

      break;

    case LOG_WHERE_STDERR:
      g_hLog = stderr;
      break;

    default:
      return -1;
    }
  }

  g_logLevel = level;

  return 0;
  
}


int LOG_End ( void )
{
  
  if ( ! g_hLog )
    return 0;

  if ( g_hLog != stderr )
    if ( fclose(g_hLog) == EOF )
      return -1;


  return 0;

}



#define MAX_DEBUG_LINE_LEN     80

void LOG_Write ( int type, int level, const char *file, int line, const char *func, const char *format, ...)
{
  va_list ap;
  char date[22];
  time_t now;
  
#ifdef WIN32
  char *aux;
  size_t size;
#endif

  if ( 0 == g_logLevel )
    return;

#ifdef WIN32
  size = strlen(file);
  aux = file+size-1;
  while ( (*aux) != '\\' && (aux != file) )
	  --aux;
  if ( *aux == '\\')
    file = aux+1;

#endif

  if ( level <= g_logLevel ) {

    if ( ! g_hLog )
      return;

    va_start(ap, format);
    
    if ( LOG_TYPE_ERROR == type )
      fprintf(g_hLog, "[ERROR] ");
    else
      fprintf(g_hLog, "[DEBUG] ");

    now = time(NULL);
    strftime(date, 22, "[%d/%m/%Y %I:%M:%S]", localtime(&now));

    fprintf(g_hLog, "%s:%ld:%s:%d:%s:", date, GetPID(), file, line, func);
    vfprintf(g_hLog, format, ap);
    
    fprintf(g_hLog, "\n");
    fflush(g_hLog);
    
    va_end(ap);
  }
  
}


