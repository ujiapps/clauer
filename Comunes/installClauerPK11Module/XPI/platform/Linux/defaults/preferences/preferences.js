pref("extensions.clauerPK11inst@uji.es.description", "chrome://clauerpk11inst/locale/clauerpk11inst.properties");
pref("extensions.clauerPK11inst@uji.es.name", "chrome://clauerpk11inst/locale/clauerpk11inst.properties");
pref("extensions.clauerpk11installer.installed", false);

pref("extensions.clauerpk11installer.libpath", "/usr/lib/libclauerpkcs11.so");
