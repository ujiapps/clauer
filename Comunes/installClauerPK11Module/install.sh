#!/bin/bash


PROFILE=default



DOTESTS=0;



for i in $*
do

  echo "testing "$i


if [ $i == "-t"  ]
then


  DOTESTS=1;

fi

done


PROFDIR=$(ls -d1 $HOME/.mozilla/firefox/*.$PROFILE)

if [  -d /usr/lib/firefox-addons ]     #estamos en Ubuntu
then
    FFXADDONSDIR=/usr/lib/firefox-addons
    FFXDIR=$(ls -d1 /usr/lib/firefox-[0-9].*)
else                                   # Debian o cualquier otra
    FFXADDONSDIR=$(ls -d1 /usr/lib/{iceweasel,firefox} 2>/dev/null)
    FFXDIR=$(ls -d1 /usr/lib/{iceweasel,firefox} 2>/dev/null)
fi


GECKODIR=$(ls -d1 /usr/lib/xulrunner-devel-[0-9].[0-9]*)
XPIDLC=$GECKODIR/bin/xpidl










######### XUL #########

cd XUL

#limpiamos el arbol de dirs
find ./ -iname "*~"| xargs rm -v 	2> /dev/null


cd clauerpk11inst

#Creamos el jar del chrome
rm -v clauerpk11inst.jar	2> /dev/null
jar -cvf clauerpk11inst.jar *


if [ -e clauerpk11inst.jar ];
then
    
    #Copiamos el jar al dir del XPI
    cp clauerpk11inst.jar ../../XPI/chrome
    rm -v clauerpk11inst.jar	2> /dev/null
    
else
    echo "Error: No se ha podido crear el archivo JAR. Instale el paquete fastjar"
    exit 1
fi

cd ../..



######### Dir XPI_temp libre de .svn #########


#limpiamos el arbol de dirs de backups
find ./ -iname "*~" | xargs rm -v        2> /dev/null

#copiamos y limpiamos el dir de la extension de .svn
cp -rf XPI XPI_temp

cd XPI_temp

find ./ -name ".svn" | xargs rm -rf -

cd ..





######### XPI #########

cd XPI_temp

find ./ -iname "*~"| xargs rm -v	2> /dev/null

#Construimos el xpi
rm -v ../clauerPK11inst.xpi	2> /dev/null
rm -v clauerPK11inst.xpi	2> /dev/null
zip -R clauerPK11inst.xpi '*'

#Lo copiamos al dir raiz
cp clauerPK11inst.xpi ../
rm -v clauerPK11inst.xpi	2> /dev/null

cd ..


######### PRUEBAS #########



if [ -e clauerPK11inst.xpi ];
then
    
    rm -f *~ 2> /dev/null

    rm -rf $PROFDIR/extensions/clauerPK11inst@uji.es      2>/dev/null
    cp -rf XPI_temp $PROFDIR/extensions/clauerPK11inst@uji.es
    
    rm -rf XPI_temp



  if [ $DOTESTS -eq "1" ];
  then


      firefox -P $PROFILE -no-remote
  
  fi
  
else
    echo "Error: No se ha podido crear el archivo XPI."
    rm -rf XPI_temp
    exit 1
fi




