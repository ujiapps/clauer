function simpleInstall(){
  //Sacamosla ruta a la libreria dinámica de las preferencias, según si es windows, linux o mac
  var prefs = Components.classes["@mozilla.org/preferences-service;1"]
                    .getService(Components.interfaces.nsIPrefService);
  prefs = prefs.getBranch("extensions.clauerpk11installer.");

  var ClauerPk11libpath = prefs.getCharPref("libpath");

  //alert("Path: "+ClauerPk11libpath);

  try{	
    CLAUERPK11_install(ClauerPk11libpath);
  }catch(e){
    //NOP
  }
  CLAUERPK11_hideoverlay();
}


function CLAUERPK11_getPKCS11Object(){
  return Components.classes["@mozilla.org/security/pkcs11;1"].getService(Components.interfaces.nsIPKCS11);
}


function CLAUERPK11_install(path){  
  PKCS11_PUB_READABLE_CERT_FLAG  =  0x1<<28; //Stored certs can be read off the token w/o logging in
  CLAUERPK11_getPKCS11Object().addModule('Modulo pkcs11 Clauer', path, PKCS11_PUB_READABLE_CERT_FLAG, 0);
}


function CLAUERPK11_hideoverlay(){
  document.getElementById("clauerpk11instmenuitem").hidden=true;
}

