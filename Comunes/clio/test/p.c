#include "../clio.h"

#include <stdio.h>
#include <stdlib.h>

#ifdef WIN32
#include <windows.h>
#endif


void testEnum ( void ) 
{
  unsigned char nDevs, ret;
  char *devices[IO_MAX_DEVICES];
  int i;


  ret = IO_EnumClauers(&nDevs, devices);
  if ( ret != IO_SUCCESS ) {
    fprintf(stderr, "[ERROR] %d\n", ret);
    return;
  }
  
  for ( i = 0 ; i < nDevs ; i++ ) {
    printf("%s\n", devices[i]);
  }
  

}




int main ( int argc, char **argv )
{
  clauer_handle_t hClauer;
  unsigned char block[BLOCK_SIZE];
  register unsigned long bn;
  int ret;
  block_info_t bi;
  FILE *fp;

  testEnum ();

  return 0;

  memset(&bi, 0, sizeof bi);
  IO_Open("\\\\.\\PHYSICALDRIVE1", &hClauer, IO_RD);
  IO_ReadInfoBlock(hClauer, &bi);
  IO_Close(hClauer);


  printf("%ld\n", sizeof bi);
  fp =fopen("block_info", "wb");
  fwrite(&bi, sizeof bi, 1, fp);
  fclose(fp);

#if 0
  ret = IO_Open("\\\\.\\PHYSICALDRIVE1", &hClauer, IO_RD);
  if ( ret != IO_SUCCESS ) {
    printf("[ERROR] ret = %d\n", ret);
    return 1;
  }


  ret = IO_ReadInfoBlock(hClauer, &bi);
  if ( ret != IO_SUCCESS ) {
    printf("[ERROR] ret = %d\n", ret);
    return 1;
  }


  IO_Close(hClauer);
  
  printf("total blocks: %ld\n", bi.totalBlocks);
  printf("version: %ld\n", bi.version);
  printf("reserved size: %ld\n", bi.rzSize);
#endif


  
  return 0;
}

