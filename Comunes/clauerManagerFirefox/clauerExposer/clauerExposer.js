/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-*/

const CLEXPOSER_CONTRACTID = "@uji.es/clauerExposer;1";
const CLEXPOSER_CID        = Components.ID("{e223c45d-5db3-4cbf-a7f5-d1c2e5163427}");

const nsISupports      = Components.interfaces.nsISupports;
const nsIFactory       = Components.interfaces.nsIFactory;
const IclauerExposer   = Components.interfaces.IclauerExposer;
const nsISecCheck      = Components.interfaces.nsISecurityCheckedComponent;
const nsIClassInfo     = Components.interfaces.nsIClassInfo;

const JAVASCRIPT_GLOBAL_CONSTRUCTOR_CATEGORY = "JavaScript global constructor";


var category = JAVASCRIPT_GLOBAL_CONSTRUCTOR_CATEGORY;
var entry    = "clauerReq";  

const clauerCryptoCID = "@uji.es/clauerCrypto;1";




function clauerExposer()
{
    this.clHandler = null;
    this.clHandler = Components.classes[clauerCryptoCID].createInstance();
    this.clHandler = this.clHandler.QueryInterface(Components.interfaces.IClauerCrypto);
};



clauerExposer.prototype.getClauerSoftVersion =
function ()
{
    return this.clHandler.GetClauerSoftVersion();
};

clauerExposer.prototype.getClauerUniverse =
function ()
{
    return this.clHandler.GetClauerUniverse();
};

clauerExposer.prototype.getClauersListString =
function ()
{
    return this.clHandler.GetClauersListString();
};


clauerExposer.prototype.getClauersList =
function (num)
{

    //dump("=========>Clauer: LIST\n");

    var count = {};
    
    var vector = this.clHandler.GetClauersList(count);

    //dump("=========>ClauerLIST: tam:"+count.value+"\n");    
    //dump("=========>ClauerLIST: vec:"+vector+"\n");

    num.value = count.value;

    return vector;
};


clauerExposer.prototype.setClauerActive =
function (deviceName, uniq)
{
    return this.clHandler.SetClauerActive(deviceName, uniq);
};

clauerExposer.prototype.getClauerId =
function ()
{
    return this.clHandler.GetClauerId();
};

clauerExposer.prototype.getClauerOwner =
function ()
{
    return this.clHandler.GetClauerOwner(); 
};



//Apa�o para sacar la URL pq en FFx 3 no va desde c++
clauerExposer.prototype.getClauerToken =
function (nombre, pwd, chal)
{

  var wMed = Components.classes["@mozilla.org/appshell/window-mediator;1"]
                        .getService(Components.interfaces.nsIWindowMediator);

  var win = wMed.getMostRecentWindow("navigator:browser");

  var url = win.gBrowser.selectedBrowser.contentDocument.defaultView.window.location.href;

  return this.clHandler.GetClauerToken(nombre, pwd, chal, url);
};

clauerExposer.prototype.setClauerCryptoMode =
function (pwd)
{
    return this.clHandler.SetClauerCryptoMode(pwd);
};

clauerExposer.prototype.setClauerClearMode =
function ()
{
    return this.clHandler.SetClauerClearMode();
};

clauerExposer.prototype.setClauerPwd =
function (pwd)
{
    return this.clHandler.SetClauerPwd(pwd);
};




clauerExposer.prototype.setNonInteractiveMode =
function ()
{
    return this.clHandler.SetNonInteractiveMode();
};

clauerExposer.prototype.formatClauer = 
function (pwd)
{
    return this.clHandler.FormatClauer(pwd);
};




clauerExposer.prototype.getClauerCertList = 
function (num)
{
    //dump("\n\n\nListando CERTS del clauer\n\n\n");
    
    var count = {};
 
    var vector = this.clHandler.GetClauerCertList(count);

    num.value = count.value;

    return vector;
};

clauerExposer.prototype.getFFXCertList = 
function (num)
{
    var count = {};
 
    var vector = this.clHandler.GetFFXCertList(count);

    num.value = count.value;

    return vector;
};



clauerExposer.prototype.putClauerCertIntoFFX = 
function (idx){
    return this.clHandler.PutClauerCertIntoFFX(idx);
};

clauerExposer.prototype.putFFXCertIntoClauer = 
    function (idx){
    return this.clHandler.PutFFXCertIntoClauer(idx);
};

clauerExposer.prototype.putClauerCertIntoP12 = 
    function (ruta, pwd, idx){
    return this.clHandler.PutClauerCertIntoP12(ruta,pwd,idx);
}; 

clauerExposer.prototype.putP12CertIntoClauer = 
    function (ruta, pwd, cas){
    return this.clHandler.PutP12CertIntoClauer(ruta,pwd, cas);
}; 

clauerExposer.prototype.deleteClauerCert = 
function (idx){
    return this.clHandler.DeleteClauerCert(idx);
};

clauerExposer.prototype.deleteFFXCert = 
function (idx){
    return this.clHandler.DeleteFFXCert(idx);
};

clauerExposer.prototype.isClauerCryptoMode = 
function (){
    return this.clHandler.IsClauerCryptoMode();
};

clauerExposer.prototype.isFFXCryptoMode = 
function (){
    return this.clHandler.IsFFXCryptoMode();
};

clauerExposer.prototype.setFFXCryptoMode = 
function (ffxpwd){
    return this.clHandler.SetFFXCryptoMode(ffxpwd);
};

clauerExposer.prototype.setFFXClearMode = 
function (){
    return this.clHandler.SetFFXClearMode();
};




//Pkcs11


clauerExposer.prototype.isPKCS11Installed = 
function(name){
    return  this.clHandler.IsPKCS11Installed(name);
};

clauerExposer.prototype.installPKCS11 = 
function(name,path){
    return  this.clHandler.InstallPKCS11(name,path);
};

clauerExposer.prototype.uninstallPKCS11 = 
function(name){
    return  this.clHandler.UninstallPKCS11(name);
};








/*********** INTERNAL ****************/

//New in Gecko 2.0 (Firefox 4.0)
Components.utils.import("resource://gre/modules/XPCOMUtils.jsm");

clauerExposer.prototype.classID = Components.ID(CLEXPOSER_CID);


// nsISecurityCheckedComponent

clauerExposer.prototype.canCreateWrapper = function (aIID)
{
  return "AllAccess";
};

clauerExposer.prototype.canCallMethod = function (aIID, methodName)
{
    switch (methodName) {
        case "getClauerSoftVersion":
          return "AllAccess";
        case "getClauersListString":
          return "AllAccess";
        case "getClauersList":
          return "AllAccess";
        case "setClauerActive":
          return "AllAccess";
        case "getClauerId":
          return "AllAccess";
        case "getClauerOwner":
          return "AllAccess";
        case "getClauerToken":
          return "AllAccess";
        case "setClauerCryptoMode":
          return "AllAccess";
        case "setClauerClearMode":
          return "AllAccess";
        case "setClauerPwd":
          return "AllAccess";        
        default:
          return "NoAccess";
        
    }
    
    //return "NoAccess";
};

clauerExposer.prototype.canGetProperty = function (aIID, propertyName)
{
  return "NoAccess";
};

clauerExposer.prototype.canSetProperty = function (aIID, propertyName)
{
  return "NoAccess";
};



// nsIClassInfo

clauerExposer.prototype.classDescription = "Clauer UJI Web Javascript functionality";


clauerExposer.prototype.getInterfaces = function(count) 
{
    var interfaceList = [IclauerExposer, nsIClassInfo, nsISecCheck];
    count.value = interfaceList.length;
    return interfaceList;
}

clauerExposer.prototype.getHelperForLanguage = function(count) 
{
return null;
}

clauerExposer.prototype.QueryInterface = function (iid) 
{
    if (iid.equals(IclauerExposer) ||
        iid.equals(nsIClassInfo) ||
        iid.equals(nsISupports) ||
        iid.equals(nsISecCheck))
        return this;

    Components.returnCode = Components.results.NS_ERROR_NO_INTERFACE;
    return null;
}



// Module

var clauerexposerModule = new Object();

clauerexposerModule.firstTime =  true;

clauerexposerModule.registerSelf = function (compMgr, fileSpec, location, type)
{
    //obtiene una ref al component manager
    compMgr = compMgr.QueryInterface(Components.interfaces.nsIComponentRegistrar);

    //registra la factoria de nuestro comp en el comp manager
    compMgr.registerFactoryLocation(CLEXPOSER_CID,
                                    "Clauer UJI Web Javascript functionality",
                                    CLEXPOSER_CONTRACTID,
                                    fileSpec,
                                    location,
                                    type);

    //obtiene una ref al category manager
    const CATMAN_CONTRACTID = "@mozilla.org/categorymanager;1";
    const nsICategoryManager = Components.interfaces.nsICategoryManager;
    var catman = Components.classes[CATMAN_CONTRACTID].
                            getService(nsICategoryManager);

   
    catman.addCategoryEntry(category,
                            entry,  //Nombre del objeto para instanciarlo en el Js de las webs
                            CLEXPOSER_CONTRACTID,
                            true, //el cambio en la categoria debe persistir tras cerrar la aplicacion
                            true); //si existia el valor, sobreescribirlo
};


clauerexposerModule.unregisterSelf = function (compMgr, fileSpec, location)
{
    compMgr.QueryInterface(Components.interfaces.nsIComponentRegistrar)
           .unregisterFactoryLocation(CLEXPOSER_CID, fileSpec);

     const catman = Components.classes['@mozilla.org/categorymanager;1']
                              .getService(Components.interfaces.nsICategoryManager);
    catman.deleteCategoryEntry(category, entry, true);
};




clauerexposerModule.getClassObject = function (compMgr, cid, iid) 
{
    if (!cid.equals(CLEXPOSER_CID))
        throw Components.results.NS_ERROR_NO_INTERFACE;

    if (!iid.equals(Components.interfaces.nsIFactory))
        throw Components.results.NS_ERROR_NOT_IMPLEMENTED;

    return clauerexposerFactory;
};

//Esta funcion es necesario implementarla pero en js es inutil, pq no se
//pueden descargar los modulos en js hasta el final
clauerexposerModule.canUnload = function(compMgr)
{
    return false;
};



// Factory
var clauerexposerFactory = new Object();

clauerexposerFactory.createInstance = function (outer, iid) 
{
    if (outer != null)
        throw Components.results.NS_ERROR_NO_AGGREGATION;

    return (new clauerExposer()).QueryInterface(iid);
};

// Entry point
function NSGetModule(compMgr, fileSpec) 
{
    return clauerexposerModule;
};





//New entry point in Gecko 2.0 (Firefox 4.0)
try{
  const NSGetFactory = XPCOMUtils.generateNSGetFactory([clauerExposer]);
 }catch(e){
  ////dump("AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA\n"+e+"\n");
 }
