#!/bin/bash


make -C ./comunes/CRYPTOWrapper/  clean
make -C ./comunes/LIBIMPORT/      clean
make -C ./comunes/LIBMSG/         clean
make -C ./comunes/LIBRT/          clean
make -C ./comunes/OPENSSL/        clean  2> /dev/null
make -C ./comunes/PCRE            clean



function limpia {
    rm -v *~     2> /dev/null
    rm -v .*~    2> /dev/null
    rm -v \#*\#  2> /dev/null
    rm -v *.xpt  2> /dev/null
    rm -v *.so*  2> /dev/null
    rm -v *.o    2> /dev/null
    rm -v *.a    2> /dev/null
    rm -v $(ls | grep -E "^(ns)?I.*\.h$") 2> /dev/null  //borra las cabeceras generadas por las interfaces
}



function explora {
    
    cd $1

    dirs=`ls -F | grep "/"`


    for i in $dirs
      do

      explora $i

    done

    limpia

    cd ..
}


explora ./
