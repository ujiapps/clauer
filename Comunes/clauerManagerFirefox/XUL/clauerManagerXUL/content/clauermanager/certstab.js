

/******************* CERT lists***********************/

function reloadCertLists(){

  loadCertificates(document.getElementById('ffxcertstree'), false);
  loadCertificates(document.getElementById('clauercertstree'), false);
}


function selectedCerts(tree){

  var t    = document.getElementById(tree);
  var data = null;

  if(tree ==  "clauercertstree"){
    if (getSelectedDev()){ 
      data = 	clauercertarr;
    }
    else return [];
  }
  else if(tree == "ffxcertstree"){
    data = ffxcertarr;
  }
  else{
    //alert("Error: Este arbol no existe en la aplicacion.");
  }
	
  var trvw = t.view.selection;


  //	for(l in t){
  //		dump(l+' -> '+t[l]+'\n');
  //	}


  //alert('entra: '+data.length);
  //alert(trvw.count);
  //alert(data.childNodes.length);

  var certList  = new Array();
  for(var i=0;i < data.length;i++){
    //alert(i);
    //alert(data.childNodes[i].localName);
    //data.childNodes[i].setAttribute("label","hola");
    if (trvw.isSelected(i)){
      //alert("Fila "+i+" seleccionada.");
      certList.push(data[i][7]);
      //alert('pos: '+i+' id: '+data[i][7]);
    }else{
      //alert("Fila "+i+" NO seleccionada.");
    }
    //alert(data.childNodes[i].getAttribute("label"));
    //alert(data.childNodes[i].childNodes[0].localName);
  }


  //for(var j=0;j<certList.length;j++){
  //alert(certList[j]);
  //}
  return certList;
}



function formatFFXField(line){

  //dump('line: '+line+'\n');

  if(!line || line==''){
    //dump("linea chunga\n");
    return ['',CLMANSTRS.GetStringFromName('corruptCert'),'','','','','',''];
  }


  var aux = line.split('#**#');
  var res = new Array();
/*
  for(var i in aux){
    if(aux[i]!=undefined)
      dump("aux["+i+"]: "+aux[i]+'\n');
  }
*/
  //alert("split de la linea: "+aux.toString());

  //El IDX
  if(!aux[0])
    return ['',CLMANSTRS.GetStringFromName('corruptCert'),'','','','','',''];
  res.push(aux[0]); 

  //alert(aux[0]);

  //El CN
  var CNRe = /(CN=[^;]+;)/;
  //dump("AUX[1]-> "+aux[1]+"\n");
  try {
    var qq = CNRe.exec(aux[1])[0];
    qq = qq.slice(3, qq.length-1);
  }
  catch(err){
    var qq = "---";
  }
  
  //alert("el CN1: "+qq);
  
  res.push(qq);

  //alert("el CN: "+qq.slice(3, qq.length-1));




  //El Tipo (hacer)
  var tipodecert = {'CA':CLMANSTRS.GetStringFromName('certtypeCA'),
		    'USER':CLMANSTRS.GetStringFromName('certtypeUSER'),
		    'OTHER':CLMANSTRS.GetStringFromName('certtypeOTHER'),
		    'ERR':'Error'};
  try{
    var ctipo = tipodecert[aux[2]];
  }
  catch(err){
    var ctipo = "---";
  }
  res.push(ctipo);	
  
  //alert("el tipo: "+aux[2]);

  //El purpose (hacer)
  var purposes = {'Sig':CLMANSTRS.GetStringFromName('purpSIG'), 
		  'KeyEnc':CLMANSTRS.GetStringFromName('purpKEYENC'), 
		  'NoRep':CLMANSTRS.GetStringFromName('purpNOREP'),
		  'DataEnc':CLMANSTRS.GetStringFromName('purpDATAENC'), 
		  'KeyAg':CLMANSTRS.GetStringFromName('purpKEYAG'), 
		  'CertSig':CLMANSTRS.GetStringFromName('purpCERTSIG'), 
		  'CRLSig':CLMANSTRS.GetStringFromName('purpCRLSIG'),
		  'EncOnly':CLMANSTRS.GetStringFromName('purpENCONLY'), 
		  'DecOnly':CLMANSTRS.GetStringFromName('purpDECONLY'),
		  'CiphAlias':CLMANSTRS.GetStringFromName('purpCIPHALIAS')};
  //CiphAlias: es un purpose que me defino yo para homogeneizar los distintos propositos de cifrado

  try{
    
    var transl = "";
    var idx;
    if(aux[3]!=""){
      var aa = aux[3].split('#');
      //Homogeneizamos los purp de cifrado
      var found = false;			
      for (idx in aa){
	if(aa[idx]=='KeyEnc'){
	  aa[idx] = 'CiphAlias';
	  if(!found) found = true;
	  else aa.splice(idx,1);
	}
	if(aa[idx]=='DataEnc'){
	  aa[idx] = 'CiphAlias';
	  if(!found) found = true;
	  else aa.splice(idx,1);
	}
	
      }
      
      
      for (idx in aa){
	transl+=(purposes[aa[idx]]+", ");
      }
    }	
    transl = transl.slice(0,-2);
  }
  catch(err){
    var transl = "---";
  }
  
  res.push(transl);

  //alert("el purpose: "+aux[3]);






  //El Issuer
  try {
    qq = CNRe.exec(aux[4])[0];
    qq = qq.slice(3, qq.length-1);
  }
  catch(err){
    qq = "---";
  }
  
  res.push(qq);
	
  //alert("el ISSUER: "+qq.slice(3, qq.length-1));


  //El serial
  if(!aux[5])
    res.push("---");
  else
    res.push(aux[5]);



  //alert("el serial: "+aux[5]);

  //Expiration date
  
  try{
    var meses = {   'Jan':'01','Feb':'02','Mar':'03',
		    'Apr':'04','May':'05','Jun':'06',
		    'Jul':'07','Aug':'08','Sep':'09',
		    'Oct':'10','Nov':'11','Dec':'12'};

    
    //alert("PRUEBA: "+meses['Nov']);
    
    var CNRe = /[ ]+/g;
    var xx = aux[6].replace(CNRe,' ');
    qq = xx.split(' '); 
    
    //alert("Fecha split: "+qq.toString());
    
    var dia = qq[1];
    if(dia<10) dia = '0'+dia;
    
    var fecha = dia +'/'+meses[qq[0]]+'/'+qq[3];
    //alert(fecha);
    res.push(fecha);  
    
    
    //Organization
    var ORe = /(O=[^;]+;)/;
    qq = ORe.exec(aux[7])[0];
    qq = qq.slice(2, qq.length-1);
  }
  catch(err){
    qq = "XX/XX/XX";
  }
  
  res.push(qq);  
  
  //alert(qq.slice(2, qq.length-1));
  
  
  return res;
  
}

function sortCertLine(a,b){

  var sa = formatFFXField(a);
  var sb = formatFFXField(b);

  //alert("Comparando "+sa[1]+" con "+ sb[1]);


  //Primero ordenamos por Tipo (CAs antes, luego Personales, y luego Otros)
  if (sa[2].toLowerCase() > sb[2].toLowerCase()) return 1;
  if (sa[2].toLowerCase() < sb[2].toLowerCase()) return -1;

  //Comparamos el CN primero
  if (sa[1].toLowerCase() > sb[1].toLowerCase()) return 1;
  if (sa[1].toLowerCase() < sb[1].toLowerCase()) return -1;

  //Luego comparamos el Purpose 
  if (sa[3].toLowerCase() > sb[3].toLowerCase()) return 1;
  if (sa[3].toLowerCase() < sb[3].toLowerCase()) return -1;

  //Luego comparamos el Issuer 
  if (sa[4].toLowerCase() > sb[4].toLowerCase()) return 1;
  if (sa[4].toLowerCase() < sb[4].toLowerCase()) return -1;

  //Y finalmente por el issuer 
  if (sa[5].toLowerCase() > sb[5].toLowerCase()) return 1;
  if (sa[5].toLowerCase() < sb[5].toLowerCase()) return -1;


  return 0;


}


function loadCertificates(tree, clean)
{
  var num = {};
  var certs = new Array();
  var showncerts = new Array();
  var acttreeview = new Object();

  //dump('Llamando a loadcerts con clean '+clean+' y tree '+tree.id+'\n');

  if(clean){
    if(tree.id ==  "clauercertstree"){
      clauercertarr = [];
    }else if(tree.id == "ffxcertstree"){
      ffxcertarr = [];
    }else{
    }
    refreshTree(tree);
    return 1;
  }

  if(tree.id ==  "clauercertstree"){
    if (getSelectedDev()){ 
      certs = clauer.getClauerCertList(num);
      clauercertarr = [];
      showncerts = clauercertarr;
      acttreeview = clauerCertListTreeView;
	
    }
  }
  else if(tree.id == "ffxcertstree"){
    certs = clauer.getFFXCertList(num);
    ffxcertarr = [];
    showncerts = ffxcertarr;
    acttreeview = ffxCertListTreeView;
  }
  else{
    //alert("Error: Este árbol no existe en la aplicación.");
  }

  //Borrar: solo debug
  //var certs = ["Cert1#**#Personal#**#Cliente, Firma#**#ACCV#**#11:11:11:11:11:11#**#21/12/2011",
  //	 "Cert2#**#Root#**##**#ACCV#**#11:11:11:11:11:11#**#21/12/2011",
  //	 "Cert3#**#Personal#**#Cifrado#**#ACCV#**#11:11:11:11:11:11#**#21/12/2011"];

  if(!certs)
    return;

  


  certs.sort(sortCertLine);

	

  //alert(certs[0]);		

  //var treedata = tree.childNodes[1];

  //while(treedata.firstChild)
  //	treedata.removeChild(treedata.firstChild);
	
	
	


  for (i=0;i<certs.length;i++){

    var linea = formatFFXField(certs[i]);
    if(!linea || linea == '')
	continue;

    //alert(linea);

    if(document.getElementById('showcaschk').checked == false){
      if (linea[2] == CLMANSTRS.GetStringFromName('certtypeCA'))
	continue;
				

    }
    if(document.getElementById('showothchk').checked == false){
      if (linea[2] == CLMANSTRS.GetStringFromName('certtypeOTHER'))
	continue
	  }

    var shownline = new Array();

    for(j=1;j<linea.length-1;j++){//El -1 es para que no salga el organization
			
      shownline[j-1] = linea[j];
    }
    shownline[j] = linea[0]; //El idx del array, lo ponemos al final

    //alert(shownline);

    showncerts.push(shownline);
  }

  document.getElementById(tree.id).view = acttreeview;  

  refreshTree(tree);


}




//function cleanClauerCertList(){
//var treedata = document.getElementById('clauercertstree').childNodes[1];
//while(treedata.firstChild)
// 		treedata.removeChild(treedata.firstChild);
//}


/********************Transfer functions***************************/


function importFromFFX()
{
  //alert(selectedCerts('ffxcertstree').toString());


  if (!getSelectedDev()){ 
    notify(CLMANSTRS.GetStringFromName('mustselectdevice'), true);
    return 0;
  }

  lockDeviceSelector();

  //vemos los índices seleccionados
  var lista = selectedCerts('ffxcertstree');

  if(lista.length <=0){
    notify(CLMANSTRS.GetStringFromName('mustselectcerts'), true);
    unlockDeviceSelector();
    return 0;
  }
	 
  if(!loginFFX()){
    return 0;
  }

  if(!loginClauer()){
    return 0;
  }


  for (certidx in lista){ 
    if(!clauer.putFFXCertIntoClauer(lista[certidx])){
      notify(CLMANSTRS.GetStringFromName('certtransfererror')+' 003', true);
      unlockDeviceSelector();
      return 0;
    }
  }

  //clauer.setClauerClearMode();
  //capturamos que dev esta seleccionado
  var sel  = getSelectedDevNum();
  //Actualizamos lista de dispositivos (por si hemos pasado de clauer vacio a clauer con propietario)
  populateDeviceList();
  //Seleccionamos el disp que estaba
  autoSeleccionarDisp(sel);
  //Actualizamos las listas de certs
  reloadCertLists();
  unlockDeviceSelector();
  return 1;

}





//Hacer esta cuando funcione bien con p12s

function exportToFFX()
{

  // alert(selectedCerts('clauercertstree').toString());



  if (!getSelectedDev()){ 
    notify(CLMANSTRS.GetStringFromName('mustselectdevice'), true);
    return 0;
  }

  lockDeviceSelector();

  //vemos los índices seleccionados
  var lista = selectedCerts('clauercertstree');

  if(lista.length <=0){
    notify(CLMANSTRS.GetStringFromName('mustselectcerts'), true);
    unlockDeviceSelector();
    return 0;
  }
	
  if(!loginClauer()){
    return 0;
  }

  if(!loginFFX()){
    return 0;
  }

  for (certidx in lista){ 
    if(!clauer.putClauerCertIntoFFX(lista[certidx])){
      notify(CLMANSTRS.GetStringFromName('certtransfererror')+' 004', true);
      unlockDeviceSelector();
      return 0;
    }
  }	

  //clauer.setClauerClearMode();
  //capturamos qué dev está seleccionado
  var sel  = getSelectedDevNum();
  //Actualizamos lista de dispositivos (por si hemos pasado de clauer vacio a clauer con propietario)
  populateDeviceList();
  //Seleccionamos el disp que estaba
  autoSeleccionarDisp(sel);
  //Actualizamos las listas de certs
  reloadCertLists();
  unlockDeviceSelector();
  return 1;

}






function backupInFile()
{
  var lista = selectedCerts('clauercertstree');

  if (!getSelectedDev()) {
    notify(CLMANSTRS.GetStringFromName('mustselectdevice'), true);
    return false;
  }
  if(lista.length <=0){
    notify(CLMANSTRS.GetStringFromName('mustselectexportcert'),true);
    return false;
  }

  if(lista.length >1){
    alert(CLMANSTRS.GetStringFromName('errormultipleexportnotimplemented'));
    return false;
  }

  //alert("cert seleccionado: "+lista[0]);
  var cert = lista[0];

  const nsIFilePicker = Components.interfaces.nsIFilePicker;
  var fp = Components.classes["@mozilla.org/filepicker;1"].createInstance(nsIFilePicker);
  fp.init(window, CLMANSTRS.GetStringFromName('exportfiledialogtitle'), nsIFilePicker.modeSave);
  fp.appendFilter(CLMANSTRS.GetStringFromName('exportfiledialogfiletype'),
                  "*.p12");
  fp.appendFilters(nsIFilePicker.filterAll);
  var rv = fp.show();

  if (rv == nsIFilePicker.returnOK || rv == nsIFilePicker.returnReplace) {

    var promptService = Components.classes["@mozilla.org/embedcomp/prompt-service;1"].getService();
    promptService = promptService.QueryInterface(Components.interfaces.nsIPromptService);
    var dummy = {};
    if (!promptService) {
      notify(CLMANSTRS.GetStringFromName('pwdinternalerror')+' 005', true);
      return false;
    }

    //Pedimos pw del clauer
    if(!loginClauer()){
      return 0;
    }



    //Pedir aquí el pass del p12
    var p12pass = new Object();
    promptService.promptPassword(window,
				 CLMANSTRS.GetStringFromName('pwdprompttitle'),
				 CLMANSTRS.GetStringFromName('pwdpromptpwd1'),
				 p12pass,
				 null,
				 dummy);

    //Repetirlo y comprobarlo
    var p12checkpass = new Object();
    promptService.promptPassword(window,
				 CLMANSTRS.GetStringFromName('pwdprompttitle'),
				 CLMANSTRS.GetStringFromName('pwdpromptpwd2'),
				 p12checkpass,
				 null,
				 dummy);

    if(p12checkpass.value!=p12pass.value){
      notify(CLMANSTRS.GetStringFromName('pwdpromptnomatch'), true);
      return false;
    }	
    p12checkpass.value = '';

	

    if(!clauer.putClauerCertIntoP12(fp.file.path,p12pass.value, cert)){
      notify(CLMANSTRS.GetStringFromName('pwdpromptp12error')+' 006', true);
      p12pass.value = '';
      return 0;
    }
    p12pass.value = ''; 

    notify(CLMANSTRS.GetStringFromName('pwdpromptok'), false);

    return true;
  }

  return false;
}


//Revisar

function importFromFile(){

  if (!getSelectedDev()) {
    notify(CLMANSTRS.GetStringFromName('mustselectdevice'), true);
    return false;
  }


  const nsIFilePicker = Components.interfaces.nsIFilePicker;
  var fp = Components.classes["@mozilla.org/filepicker;1"].createInstance(nsIFilePicker);
  fp.init(window, CLMANSTRS.GetStringFromName('importfiledialogtitle'), nsIFilePicker.modeOpen);
  fp.appendFilter(CLMANSTRS.GetStringFromName('exportfiledialogfiletype'),
                  "*.p12");
  fp.appendFilters(nsIFilePicker.filterAll);
  var rv = fp.show();
  if (rv == nsIFilePicker.returnOK || rv == nsIFilePicker.returnReplace) {

    var promptService = Components.classes["@mozilla.org/embedcomp/prompt-service;1"].getService();
    promptService = promptService.QueryInterface(Components.interfaces.nsIPromptService);
    var dummy = {};
    if (!promptService) {
      notify(CLMANSTRS.GetStringFromName('pwdinternalerror')+' 007', true);
      return false;
    }

    //Pedir aquí el pass del p12
    var p12pass = new Object();
    promptService.promptPassword(window,
				 CLMANSTRS.GetStringFromName('pwdprompttitle'),
				 CLMANSTRS.GetStringFromName('importp12pwddialog'),
				 p12pass,
				 null,
				 dummy);


    //Pedimos pw del clauer
    if(!loginClauer()){
      return 0;
    }
	
    if(!clauer.putP12CertIntoClauer(fp.file.path, p12pass.value, 1)){
      notify(CLMANSTRS.GetStringFromName('importp12error')+' 008', true);
      alert(CLMANSTRS.GetStringFromName('importp12erroralert'));
      return 0;
    }
    p12pass.value = '';
	
    //capturamos qué dev está seleccionado
    var sel  = getSelectedDevNum();


    //Actualizamos lista de dispositivos (por si hemos pasado de clauer vacio a clauer con propietario)
    populateDeviceList();


    //Seleccionamos el disp que estaba
    autoSeleccionarDisp(sel);


    //Actualizamos las listas de certs
    reloadCertLists();

    return true;
  }

  return false;
}




function borrarClauerCerts(){
	
  if (!getSelectedDev()){ 
    notify(CLMANSTRS.GetStringFromName('deletemustselectdev'), true);
    return 0;
  }



  //Pedimos confirmacion
  var promptService = Components.classes["@mozilla.org/embedcomp/prompt-service;1"].getService();
  promptService = promptService.QueryInterface(Components.interfaces.nsIPromptService);
  if (promptService)
    if(!promptService.confirm(window,
			      CLMANSTRS.GetStringFromName('deleteprompttitle'),
			      CLMANSTRS.GetStringFromName('deletepromptmsg'))){

      notify(CLMANSTRS.GetStringFromName('deletepromptcancel'), false);
      return 0;
    }



  lockDeviceSelector();

  //vemos los índices seleccionados
  var lista = selectedCerts('clauercertstree');



  if(lista.length <=0){
    notify(CLMANSTRS.GetStringFromName('mustselectcerts'), true);
    unlockDeviceSelector();
    return 0;
  }
	


  if(!loginClauer()){
    return 0;
  }



  for (certidx in lista){ 
    if(!clauer.deleteClauerCert(lista[certidx])){
      notify(CLMANSTRS.GetStringFromName('deleteprompterr')+' 009', true);
      unlockDeviceSelector();
      return 0;
    }
  }


  //clauer.setClauerClearMode();



  //capturamos qué dev está seleccionado
  var sel  = getSelectedDevNum();


  //Actualizamos lista de dispositivos (por si hemos pasado de clauer vacio a clauer con propietario)
  populateDeviceList();


  //Seleccionamos el disp que estaba
  autoSeleccionarDisp(sel);


  //Actualizamos las listas de certs
  reloadCertLists();

  unlockDeviceSelector();

  return 1;

}


function borrarFFXCerts(){

  //vemos los índices seleccionados
  var lista = selectedCerts('ffxcertstree');


  //Pedimos confirmación
  var promptService = Components.classes["@mozilla.org/embedcomp/prompt-service;1"].getService();
  promptService = promptService.QueryInterface(Components.interfaces.nsIPromptService);
  if (promptService)
    if(!promptService.confirm(window,
			      CLMANSTRS.GetStringFromName('deleteprompttitle'),
			      CLMANSTRS.GetStringFromName('deletepromptmsg'))){

      notify(CLMANSTRS.GetStringFromName('deletepromptcancel'), false);
      return 0;
    }
	


  if(lista.length <=0){
    notify(CLMANSTRS.GetStringFromName('mustselectcerts'), true);
    return 0;
  }
	

  if(!loginFFX()){
    return 0;
  }

  for (certidx in lista){ 
    if(!clauer.deleteFFXCert(lista[certidx])){
      notify(CLMANSTRS.GetStringFromName('deleteprompterr')+' 010', true);
      return 0;
    }
  }

  //Actualizamos las listas de certs
  reloadCertLists();

  return 1;

}


function loginFFX(){

  //Pedir acceso a la bd del ffx
  if(!clauer.isFFXCryptoMode()){

    var promptService = Components.classes["@mozilla.org/embedcomp/prompt-service;1"].getService();
    promptService = promptService.QueryInterface(Components.interfaces.nsIPromptService);


    var pass = {};
    var dummy = {};
    if (promptService) {
      promptService.promptPassword(window,
				   CLMANSTRS.GetStringFromName('ffxpwdprompttitle'),
				   CLMANSTRS.GetStringFromName('ffxpwdprompmsg'),
				   pass,
				   null,
				   dummy);
    }

    if(pass.value==null){
      notify(CLMANSTRS.GetStringFromName('ffxpwdprompterr'), true);
      unlockDeviceSelector();
      return 0;
    }
    //if(pass.value==""){
    //  notify(CLMANSTRS.GetStringFromName('ffxpwdprompterr'), true);
    //  unlockDeviceSelector();
    //  return 0;
    //}



    if (!clauer.setFFXCryptoMode(pass.value)){
      notify(CLMANSTRS.GetStringFromName('ffxpwdprompterr'), true);
      unlockDeviceSelector();
      return 0;
    }
    pass = "DEADBEEFCAFE";
    pass="";
  }

  return 1;
}





function loginClauer(){

  if(!clauer.isClauerCryptoMode()){


    var promptService = Components.classes["@mozilla.org/embedcomp/prompt-service;1"].getService();
    promptService = promptService.QueryInterface(Components.interfaces.nsIPromptService);

    var pass = {};
    var dummy = {};
    if (promptService) {
      promptService.promptPassword(window,
				   CLMANSTRS.GetStringFromName('ffxpwdprompttitle'),
				   CLMANSTRS.GetStringFromName('clauerpwdpromptmsg'),
				   pass,
				   null,
				   dummy);
    }

    if(pass.value==null){
      notify(CLMANSTRS.GetStringFromName('clauerpwdprompterr'), true);
      unlockDeviceSelector();
      return 0;
    }
    if(pass.value==''){
      notify(CLMANSTRS.GetStringFromName('clauerpwdprompterr'), true);
      unlockDeviceSelector();
      return 0;
    }


    if (!clauer.setClauerCryptoMode(pass.value)){
      notify(CLMANSTRS.GetStringFromName('clauerpwdprompterr'), true);
      unlockDeviceSelector();
      return 0;
    }
    pass = "DEADBEEF";
    pass="";
  }

  return 1;
}
