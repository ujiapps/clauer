function clauer_get_stringbundle()
{
  var o_stringbundle = false;

  try {
    var oBundle = Components.classes["@mozilla.org/intl/stringbundle;1"]
      .getService(Components.interfaces.nsIStringBundleService);
    o_stringbundle = oBundle.createBundle("chrome://clauermanager/locale/main.properties");
  } catch(err) {
    o_stringbundle = false;
  }
  if (!o_stringbundle) {
    //error
  }

  return o_stringbundle;
}



//Adaptar cómo saco el dispositivo activo, porque ya no puedo saber el label del treeitem

var devicesTreeView = {  

  treeBox: null,  
  selection: null,  
  
  get rowCount()                     { return clmandevicelist.length; },  
  setTree: function(treeBox)         { this.treeBox = treeBox; },  
  getCellText: function(idx, column) { 

    if(idx<clmandevicelist.length)
      return clmandevicelist[idx][column.id[column.id.length-1]]; 
    else
      return '';
  },  
  isContainer: function(idx)         { return false; },  
  isContainerOpen: function(idx)     { return false; },  
  isContainerEmpty: function(idx)    { return false; },  
  isSeparator: function(idx)         { return false; },  
  isSorted: function()               { return false; },  
  isEditable: function(idx, column)  { return false; },  
  getParentIndex: function(idx)  {},  
  getLevel: function(idx) {},  
  hasNextSibling: function(idx, after) {},  
  toggleOpenState: function(idx) {},  
  getImageSrc: function(idx, column) {},  
  getProgressMode : function(idx,column) {},  
  getCellValue: function(idx, column) {},  
  cycleHeader: function(col, elem) {},  
  selectionChanged: function() {},
  cycleCell: function(idx, column) {},  
  performAction: function(action) {},  
  performActionOnCell: function(action, index, column) {},  
  getRowProperties: function(idx, column, prop) {},  
  getCellProperties: function(idx, column, prop) {},  
  getColumnProperties: function(column, element, prop) {},  
};  





var ffxCertListTreeView = {  

  treeBox: null,  
  selection: null,  
	  
  get rowCount()                     { return ffxcertarr.length; },  
  setTree: function(treeBox)         { this.treeBox = treeBox; },  
  getCellText: function(idx, column) {

    if(idx<ffxcertarr.length)
      return ffxcertarr[idx][column.id[column.id.length-1]];
    else
      return '';

  },  
  isContainer: function(idx)         { return false; },  
  isContainerOpen: function(idx)     { return false; },  
  isContainerEmpty: function(idx)    { return false; },  
  isSeparator: function(idx)         { return false; },  
  isSorted: function()               { return false; },  
  isEditable: function(idx, column)  { return false; },  
  getParentIndex: function(idx)  {},  
  getLevel: function(idx) {},  
  hasNextSibling: function(idx, after) {},  
  toggleOpenState: function(idx) {},  
  getImageSrc: function(idx, column) {},  
  getProgressMode : function(idx,column) {},  
  getCellValue: function(idx, column) {},  
  cycleHeader: function(col, elem) {},  
  selectionChanged: function() {},
  cycleCell: function(idx, column) {},  
  performAction: function(action) {},  
  performActionOnCell: function(action, index, column) {},  
  getRowProperties: function(idx, column, prop) {},  
  getCellProperties: function(idx, column, prop) {},  
  getColumnProperties: function(column, element, prop) {},  
};  



var clauerCertListTreeView = {  

  treeBox: null,  
  selection: null,  
	  
  get rowCount()                     { return clauercertarr.length; },  
  setTree: function(treeBox)         { this.treeBox = treeBox; },  
  getCellText: function(idx, column) { 

    //dump("ACCESSING ROW: "+idx+" FROM: "+clauercertarr.length);

    if(idx<clauercertarr.length)
      return clauercertarr[idx][column.id[column.id.length-1]];
    else
      return '';
  },  
  isContainer: function(idx)         { return false; },  
  isContainerOpen: function(idx)     { return false; },  
  isContainerEmpty: function(idx)    { return false; },  
  isSeparator: function(idx)         { return false; },  
  isSorted: function()               { return false; },  
  isEditable: function(idx, column)  { return false; },  
  getParentIndex: function(idx)  {},  
  getLevel: function(idx) {},  
  hasNextSibling: function(idx, after) {},  
  toggleOpenState: function(idx) {},  
  getImageSrc: function(idx, column) {},  
  getProgressMode : function(idx,column) {},  
  getCellValue: function(idx, column) {},  
  cycleHeader: function(col, elem) {},  
  selectionChanged: function() {},
  cycleCell: function(idx, column) {},  
  performAction: function(action) {},  
  performActionOnCell: function(action, index, column) {},  
  getRowProperties: function(idx, column, prop) {},  
  getCellProperties: function(idx, column, prop) {},  
  getColumnProperties: function(column, element, prop) {},  
};  







function inicializar(){
  /*
    var promptser = Components.classes["@mozilla.org/embedcomp/prompt-service;1"]
    .getService(Components.interfaces.nsIPromptService);

    promptser.alert(null, "Debugging alert", "Hello! You have now been alerted.");
  */


  try{
    listar.setNonInteractiveMode();
  }
  catch(err){
    notify(CLMANSTRS.GetStringFromName('fatalerror')+' 001', false);
  }
  
  
  try{
    listarDispositivos();
    
    document.getElementById("arbolDispositivosConectados").view = devicesTreeView; 

    loadCertificates(document.getElementById('ffxcertstree'), false);
    
  }
  catch(err){
    notify(CLMANSTRS.GetStringFromName('fatalerror')+' 002', false);
  }
  
  try{
    document.getElementById('versionpanel').setAttribute('label',listar.getClauerSoftVersion());
  }
  catch(err){
    document.getElementById('versionpanel').setAttribute('label',"-");
  }
  
  try{
    var logonum = listar.getClauerUniverse();
  }
  catch(err){
    var logonum = 0;
  }
  if(!logonum | logonum == null | logonum == undefined){
    logonum = 0;
  }
  
  document.getElementById('logoarea').setAttribute('src','logo'+logonum+'.png');


  //Auto-Seleccionamos el clauer si s�lo hay uno y listamos sus certs

  try{
    populateDeviceList();

    loadCertificates(document.getElementById('clauercertstree'), false);
    
  }catch(err){
    dump(err);
  }

   
}

function finalizar(){

  clauer.setClauerClearMode();

  removeSelection()
 
    clauer.setFFXClearMode();

}

/************** ÁREA DE NOTIFICACIONES *****************/

function notify(message, highlight){

  var notif  = document.getElementById('notifierPanel');

  notif.setAttribute("label",message);

  if(highlight){

    notif.setAttribute("style","color:red;");  //font-weight:bold;
  }
  else{
    notif.setAttribute("style","color:black;");
  }	
  if(CLAUERcurrentTimeout!=null) clearTimeout(CLAUERcurrentTimeout); //Si escribimos algo, reiniciamos el contador de borrado
  CLAUERcurrentTimeout = setTimeout("document.getElementById('notifierPanel').setAttribute('label','');",5000);//A los 5 seg borra el panel 	
}


function startProgressBar(){
  document.getElementById('progressbar').setAttribute('mode','undetermined');
  //setTimeout("document.getElementById('progressbar').setAttribute('mode','undetermined');",1);
  return true;
}

function stopProgressBar(){
  document.getElementById('progressbar').setAttribute('mode','determined');
  //setTimeout("document.getElementById('progressbar').setAttribute('mode','determined');",1);
  return true;
}



/************** SELECTOR DE DISPOSITIVOS *****************/

function refreshTree(tree){

  var boxobject = tree.boxObject;
  boxobject = boxobject.QueryInterface(Components.interfaces.nsITreeBoxObject);
  boxobject.invalidate();
}

function populateDeviceList ()
{
  
  listarDispositivos();
  
  if(clmandevicelist.length == 1)
    autoSeleccionarDisp(0);
  else
    removeSelection();
  
}


function listarDispositivos()
{    


  var aux = {};	
  var devs = listar.getClauersList(aux);

  //dump("num de dispositivos conectados: "+devs.length+" - "+clmandevicelist.length+"\n");
  /*
  for(var i in clmandevicelist){
    dump(clmandevicelist[i]+"\n");
  }
 
  dump("------------\n");

  for(var i in devs){
    dump(devs[i]+"\n");
  }
  */

  if(clmandevicelist.length > 0){
    delete clmandevicelist;
    clmandevicelist = [];
  }

  for (var i=0;i<devs.length;i++)
    {
      listar.setClauerActive(devs[i],null);

      var owner = listar.getClauerOwner(); 

      if (owner == null) owner = CLMANSTRS.GetStringFromName('empty');

      var listitem = [owner,devs[i],listar.getClauerId()];
	
      clmandevicelist.push(listitem);
    }


 //dump("------+------\n");
/*
 for(var i in clmandevicelist){
    dump(clmandevicelist[i]+"\n");
  }
*/

  //Invalidamos el contenido actual del tree de dispositivos
  refreshTree(document.getElementById('arbolDispositivosConectados'));
  //dump("------------------------------------>Llamada aa refresh\n");

  //var cuadro = document.getElementById('cuadroDispositivosConectados');

  //var i =0;
  //var j=0;

  //while(cuadro.firstChild)
  // cuadro.removeChild(cuadro.firstChild);

  /*
    for (i=0;i<clmandevicelist.length;i++){
    treeit  = document.createElement('treeitem');
    treerw  = document.createElement('treerow');

    for(j=0;j<clmandevicelist[i].length;j++){
    treecl  = document.createElement('treecell');
    window.dump(clmandevicelist[i][j]);
    treecl.label = clmandevicelist[i][j];
    alert(treecl.label);
    treerw.appendChild(treecl);
    alert('::'+treerw.childNodes[j].label);

    if(j==1){ //EL TAG DEL DEV
    treeit.label = clmandevicelist[i][j];
    //alert(treeit.label);
    }
    }
    treeit.appendChild(treerw);
    cuadro.appendChild(treeit);

    alert(cuadro);



    }
  */
  //setTimeout("listarDispositivos()",20000); //Pero esto no creará una pila infinita de llamadas?

}

function seleccionarDisp()
{
  var tr = document.getElementById('arbolDispositivosConectados');

  document.getElementById("arbolDispositivosConectados").view = devicesTreeView;  

  if(tr ==  null  || tr == undefined)
    return;

  //dump("--------------------->"+tr.currentIndex+' y act: '+selectedDevNum+'\n');

  if(CLAUERInvalidSelection){  //Esta es una falsa selección por culpa del onselect del tree
    CLAUERInvalidSelection = 0;
    //dump(CLAUERInvalidSelection+"\n");
    return;
  }

  if(tr.disabled==false){
		

    if(tr.currentIndex>=0){
      selectedDev    = clmandevicelist[tr.currentIndex][1];
      //alert(selectedDev);
      selectedDevNum = tr.currentIndex;
      //alert(tr.currentIndex);
    }
    if(selectedDev!='none'){
      clauer.setClauerActive(selectedDev,null);
      clauer.setClauerClearMode();
      reloadCertLists();
      loadTokens();
      //alert("seleccionado el clauer: "+selectedDev);
      notify(CLMANSTRS.GetStringFromName('selecteddev')+' '+selectedDev, false);
    }
    else{
      //alert("seleccion vacia");
      notify(CLMANSTRS.GetStringFromName('selectdev'), false);
    }
  }
}

function autoSeleccionarDisp(devnum)
{

  if(devnum=='none'){
    //alert("selección vacía, esto no debería pasar");
    notify(CLMANSTRS.GetStringFromName('selectdev'), false);
  }

  if(devnum<0 || devnum >= clmandevicelist.length){
    //alert("selección vacía, esto no debería pasar");
    notify(CLMANSTRS.GetStringFromName('selectdev'), false);
  }

  selectedDevNum = devnum;
  selectedDev    = clmandevicelist[devnum][1];
	
  //alert("seleccionando el clauer: "+selectedDev);

 
  if(selectedDev!='none' && selectedDev!='null'){

    clauer.setClauerActive(selectedDev,null);
    //clauer.setClauerClearMode();
    reloadCertLists();
    //alert("seleccionado el clauer: "+selectedDev);

  }
	
}


function removeSelection()
{
  selectedDev = 'none';
  selectedDevNum = 'none';
  clauer.setClauerClearMode();
  loadCertificates(document.getElementById('clauercertstree'), true);
  notify(CLMANSTRS.GetStringFromName('selectdev'), false);


  CLAUERInvalidSelection = 1;  //Invalidamos el siguiente evento de onselect
  //dump(CLAUERInvalidSelection+"\n");

  document.getElementById("arbolDispositivosConectados").view.selection.clearSelection();

  document.getElementById("clauercertstree").view.selection.clearSelection();

}


function tabChangeRemoveSelection()
{

  selectedDev = 'none';
  selectedDevNum = 'none';
  clauer.setClauerClearMode();
  loadCertificates(document.getElementById('clauercertstree'), true);
  notify(CLMANSTRS.GetStringFromName('selectdev'), false);

  CLAUERInvalidSelection = 1;  //Invalidamos el siguiente evento de onselect
  //dump(CLAUERInvalidSelection+"\n");

  document.getElementById("arbolDispositivosConectados").view.selection.clearSelection();

  document.getElementById("arbolDispositivosConectados").currentIndex = -1;

  document.getElementById("clauercertstree").view.selection.clearSelection();

  document.getElementById("clauercertstree").currentIndex = -1;

}



function getSelectedDev()
{

  //dump("selecteddev: "+selectedDev+"\n");

  if (selectedDev=='none')
    return null;
  else
    return selectedDev;

}

function getSelectedDevNum()
{
  //dump("selecteddevnum: "+selectedDevNum+" : "+selectedDev+"\n");
  if (selectedDev=='none')
    return -1;
  else
    return selectedDevNum;

}


function lockDeviceSelector(){
  var arb   = document.getElementById('arbolDispositivosConectados');
  var cuad  = document.getElementById('cuadroDispositivosConectados');
 
  //dump('-------------------------------------------------------locking\n');

  if(arb.getAttribute("disabled")=="false"){
    arb.setAttribute("disabled","true");

  }
}




function unlockDeviceSelector(){
  var arb   = document.getElementById('arbolDispositivosConectados');
  var cuad  = document.getElementById('cuadroDispositivosConectados');

  //dump('-------------------------------------------------------unlocking\n');

  if(arb.getAttribute("disabled")=="true"){

    arb.view.selection.select(selectedDevNum);
    //arb.view.selectionChanged();
    arb.setAttribute("disabled","false");

  }
}



