
function formatPwdSecPolicy(dev,display, new1, new2)
{

  //alert("Original: "+orig);

  //alert("Actual 1: "+new1);
  //alert("Actual 2: "+new2);

  if((new1 == "" ) || ( new2 == "")){
    notifyPassError(display,CLMANSTRS.GetStringFromName('passtab.musthavenewpwd'));
    notify(CLMANSTRS.GetStringFromName('passtab.musthavenewpwd'), false);
    return false;
  }
  if(new1 != new2){
    notifyPassError(display,CLMANSTRS.GetStringFromName('passtab.newpwdmismatch'));
    notify(CLMANSTRS.GetStringFromName('passtab.newpwdmismatch'), false);
    return false;
  }

  if(pwdStrength(new1)<42){
    notifyPassError(display,CLMANSTRS.GetStringFromName('passtab.newpwdweak'));
    notify(CLMANSTRS.GetStringFromName('passtab.newpwdweak'), false);
    return false;	
  }

  
  if(!checkValidChars(new1, display)){
    notifyPassError(display,CLMANSTRS.GetStringFromName('passtab.newpwdforbiddenchars'));
    notify(CLMANSTRS.GetStringFromName('passtab.newpwdforbiddenchars'), false);
    return false;	
  }
  

  return true;
}



function formateaParticion()
{

  dispActivo = getSelectedDev();

  if (!dispActivo){  
    notifyPassError(document.getElementById('formaterrormsg'),CLMANSTRS.GetStringFromName('formattab.musthavedev'));
    notify(CLMANSTRS.GetStringFromName('formattab.musthavedev'), false);
  }
  else{
    clauer.setClauerClearMode();
    lockDeviceSelector();
	
    startProgressBar();

    if(formatPwdSecPolicy(clauer,
			  document.getElementById('formaterrormsg'),	
			  document.getElementById('frpwdnw1').value, 
			  document.getElementById('frpwdnw2').value)){




      var errorcode = clauer.formatClauer(document.getElementById('frpwdnw1').value);
      if(!errorcode){
	cleanPassError( document.getElementById('formaterrormsg'));
	notify(CLMANSTRS.GetStringFromName('formattab.ok'), false);
      }
      else{
	notifyPassError(document.getElementById('formaterrormsg'),CLMANSTRS.GetStringFromName('formattab.err'));
	notify(CLMANSTRS.GetStringFromName('formattab.err'), false);
      }
    }

    stopProgressBar();
	

    unlockDeviceSelector();


  }
  //Actualizamos la lista de dispositivos
  populateDeviceList();
  //Actualizamos las listas de certs
  //reloadCertLists();
  loadCertificates(document.getElementById('clauercertstree'), false);  //!!!VER PQ No encuentra el settree

  clauer.setClauerClearMode();
  //document.getElementById('frpwdor').value = "";
  document.getElementById('frpwdnw1').value = "";
  document.getElementById('frpwdnw2').value = "";
  fillFormatSecBar();
}



function fillFormatSecBar(){
  var secb   = document.getElementById('frsecbar');
  secb.value = pwdStrength(document.getElementById('frpwdnw1').value);


  if(!checkValidChars(document.getElementById('frpwdnw1').value, document.getElementById('formaterrormsg'))){
    secb.value = 0;
  }else{
    cleanPassError( document.getElementById('formaterrormsg'));
  }

}
