function showMenuEntry(){


  var prefs = Components.classes["@mozilla.org/preferences-service;1"]
    .getService(Components.interfaces.nsIPrefService);
  prefs = prefs.getBranch("extensions.clauermanager.");

  if(prefs.getBoolPref('hidden')){ 
    //dump('Hidden\n');
    //prefs.setBoolPref('hidden', false);
    document.getElementById('clauermanagermenuentry').setAttribute('hidden','true');
    return false;
  }

  //dump('Not hidden\n');
  return true;

}



function abrirClauerManager()
{
  var tempclauer = Components.classes["@uji.es/clauerExposer;1"].createInstance();


  var minvers = ['3','0','2'];
  var vers = new Array();
  var properversion = false;	
	
  try{	
    vers = tempclauer.getClauerSoftVersion().split('.');
  }
  catch(e){
    //alert("Excepción");
    vers = ['0','0','0'];
  }

  var idx;
  for (idx in vers){
    if(vers[idx] > minvers[idx]){ //Si un num de vers mas prioritario es superior, ya nos vale
      properversion = true;
      break;
    }
    if(vers[idx] < minvers[idx])  //Si un num de vers mas prioritario es inferior, pues no nos vale
      break;

    //Si el num de vers es igual, miramos el siguiente

    if(idx == (vers.length-1)){ //Si es igual y es el ultimo, nos vale
      properversion = true;
      break;
    }
  }

  if(properversion){
    window.open('chrome://clauermanager/content/clauermanager.xul','clauerujimanager','chrome');
  }
  else{

    var o_stringbundle = false;

    try {
      var oBundle = Components.classes["@mozilla.org/intl/stringbundle;1"]
	.getService(Components.interfaces.nsIStringBundleService);
      o_stringbundle = oBundle.createBundle("chrome://clauermanager/locale/main.properties");
    } catch(err) {
      o_stringbundle = false;
    }
    if (!o_stringbundle) {
      alert("-- Obsolete Clauer O.S. Please, download the newest version. --");
    }
	
    alert(o_stringbundle.GetStringFromName('badversion'));
  }	

}



function restartApp() {
  const nsIAppStartup = Components.interfaces.nsIAppStartup;

  // Notify all windows that an application quit has been requested.
  var os = Components.classes["@mozilla.org/observer-service;1"]
                     .getService(Components.interfaces.nsIObserverService);
  var cancelQuit = Components.classes["@mozilla.org/supports-PRBool;1"]
                             .createInstance(Components.interfaces.nsISupportsPRBool);
  os.notifyObservers(cancelQuit, "quit-application-requested", null);

  // Something aborted the quit process. 
  if (cancelQuit.data)
    return;

  // Notify all windows that an application quit has been granted.
  os.notifyObservers(null, "quit-application-granted", null);

  // Enumerate all windows and call shutdown handlers
  var wm = Components.classes["@mozilla.org/appshell/window-mediator;1"]
                     .getService(Components.interfaces.nsIWindowMediator);
  var windows = wm.getEnumerator(null);
  while (windows.hasMoreElements()) {
    var win = windows.getNext();
    if (("tryToClose" in win) && !win.tryToClose())
      return;
  }
  Components.classes["@mozilla.org/toolkit/app-startup;1"].getService(nsIAppStartup)
            .quit(nsIAppStartup.eRestart | nsIAppStartup.eAttemptQuit);
}



function checkClauerFirstRun(){

  var prefs = Components.classes["@mozilla.org/preferences-service;1"]
    .getService(Components.interfaces.nsIPrefService);
  prefs = prefs.getBranch("extensions.clauermanager.");

  if(prefs.getBoolPref('pkcs11.firstrun')){ 
    //dump('First Run\n');
    prefs.setBoolPref('pkcs11.firstrun', false);
    return true;
  }

  //dump('Not First Run\n');
  return false;
}





function installPKCS11(){
  //PKCS11_PUB_READABLE_CERT_FLAG  =  0x1<<28; //Stored certs can be read off the token w/o logging in
  //pkcs11.addmodule("Modulo pkcs11 Clauer", "/usr/lib/libclauerpkcs11.so", PKCS11_PUB_READABLE_CERT_FLAG, 0);

  //alert('Esto debería ejecutarse al iniciar el ffx');

  
  //dump("Ejecutando installpkcs11\n");
      

  var tempclauer = Components.classes["@uji.es/clauerExposer;1"].createInstance();

  if(!tempclauer){
    return 0;
  }
  /*
  // Get the "extensions.myext." branch
  var prefs = Components.classes["@mozilla.org/preferences-service;1"]
  .getService(Components.interfaces.nsIPrefService);
  prefs = prefs.getBranch("extensions.clauermanager.");
 

  if(!prefs){
  return 0;
  }

  if(prefs.getBoolPref("pkcs11.firstrun")) 
  prefs.setBoolPref("pkcs11.firstrun", false);
  */
  if(!tempclauer.isPKCS11Installed('Modulo pkcs11 Clauer')){
    tempclauer.installPKCS11('Modulo pkcs11 Clauer','/usr/lib/libclauerpkcs11.so');
   
    //dump("Acabo de instalar el pkcs11\n");
 

    var o_stringbundle = false;

    try {
      var oBundle = Components.classes["@mozilla.org/intl/stringbundle;1"]
	.getService(Components.interfaces.nsIStringBundleService);
      o_stringbundle = oBundle.createBundle("chrome://clauermanager/locale/main.properties");
    } catch(err) {
      o_stringbundle = false;
    }
    var res = false;
    if (!o_stringbundle) {
      res = window.confirm("-- Clauer Firefox support has been installed. Please, restart browser in order to complete installation. Otherwise you could get unexpected behaviour.\n Would you like to restart now? --");
    }else{
      res = window.confirm(o_stringbundle.GetStringFromName('mustreboot'));
    }

    if(res)  //Si la respuesta es afirmativa, reinciamos el firefox
      restartApp();

  }

  //tempclauer.uninstallPKCS11('Modulo pkcs11 Clauer');
}
