function pwdStrength(pass)
{
  var strength = 0;


  if(pass.length > 0)
    strength+=10;

  if(pass.length > 4)
    strength+=10;

  if(pass.length > 8){
    strength+=10;

    var AlphaRe = /[a-z]+|[A-Z]+/;
    if(AlphaRe.exec(pass))
      strength+=20;

    var CaseRe = /([A-Z].*[a-z])|([a-z].*[A-Z])/;
    if(CaseRe.exec(pass))
      strength+=20;

    var NumRe = /[0-9]/;
    if(NumRe.exec(pass))
      strength+=10;

    var OtherRe = /[-.+_;:,*@#%|~$\\!?()=&\/]/;
    if(OtherRe.exec(pass))
      strength+=20;
  }
	
  return strength;

}


function notifyPassError(display,errorstr)
{

  display.textContent = errorstr;

}


function cleanPassError(display)
{
  display.textContent = "";
}



function pwdSecPolicy(dev,display, orig, new1, new2)
{

  //alert("Original: "+orig);

  //alert("Actual 1: "+new1);
  //alert("Actual 2: "+new2);

  if(orig == "" ){
    notifyPassError(display,CLMANSTRS.GetStringFromName('passtab.musthavecurrpwd'));
    notify(CLMANSTRS.GetStringFromName('passtab.musthavecurrpwd'), false);
    return false;
  }
  if (!dev.setClauerCryptoMode(orig)){
    notifyPassError(display,CLMANSTRS.GetStringFromName('passtab.currpwderr'));
    notify(CLMANSTRS.GetStringFromName('passtab.currpwderr'), false);
    return false;
  }
  if((new1 == "" ) || ( new2 == "")){
    notifyPassError(display,CLMANSTRS.GetStringFromName('passtab.musthavenewpwd'));
    notify(CLMANSTRS.GetStringFromName('passtab.musthavenewpwd'), false);
    return false;
  }
  if((new1 == orig ) || ( new2 == orig)){
    notifyPassError(display,CLMANSTRS.GetStringFromName('passtab.newpwdlikeold'));
    notify(CLMANSTRS.GetStringFromName('passtab.newpwdlikeold'), false);
    return false;
  }
  if(new1 != new2){
    notifyPassError(display,CLMANSTRS.GetStringFromName('passtab.newpwdmismatch'));
    notify(CLMANSTRS.GetStringFromName('passtab.newpwdmismatch'), false);
    return false;
  }

  if(pwdStrength(new1)<42){
    notifyPassError(display,CLMANSTRS.GetStringFromName('passtab.newpwdweak'));
    notify(CLMANSTRS.GetStringFromName('passtab.newpwdweak'), false);
    return false;	
  }
  
  if(!checkValidChars(new1, display))
    return false;
  
  
  return true;
}



function cambiarPassword(){

  dispActivo = getSelectedDev();

  if (!dispActivo){  //hacer siempre esta comprobación, porque no se puede desactivar el clauer activo y se podría acceder a un clauer no deseado
    notifyPassError(document.getElementById('passerrormsg'),CLMANSTRS.GetStringFromName('passtab.musthavedev'));
    notify(CLMANSTRS.GetStringFromName('passtab.musthavedev'), false);
  }
  else{
	
    lockDeviceSelector();
	
    clauer.setClauerClearMode();
    startProgressBar();

    if(pwdSecPolicy(clauer,	
		    document.getElementById('passerrormsg'),
		    document.getElementById('pwdor').value, 
		    document.getElementById('pwdnw1').value, 
		    document.getElementById('pwdnw2').value)){




      if(clauer.setClauerPwd(document.getElementById('pwdnw1').value)){
	cleanPassError( document.getElementById('passerrormsg'));
	notify(CLMANSTRS.GetStringFromName('passtab.ok'), false);
      }
      else{
	notifyPassError(document.getElementById('passerrormsg'),CLMANSTRS.GetStringFromName('passtab.err'));
	notify(CLMANSTRS.GetStringFromName('passtab.err'), false);
      }

    }

    stopProgressBar();
	

    unlockDeviceSelector();


  }
  clauer.setClauerClearMode();
  document.getElementById('pwdor').value = "";
  document.getElementById('pwdnw1').value = "";
  document.getElementById('pwdnw2').value = "";
  fillPwdSecBar();

}



function checkValidChars(new1,display){

 
  var ValidRe = /^[-.+_;:,*@#%|~$\\!?()=&\/A-Za-z0-9]+$/;
  
  if(new1!=""  &&  !ValidRe.exec(new1)){
    notifyPassError(display,CLMANSTRS.GetStringFromName('passtab.newpwdforbiddenchars'));
    notify(CLMANSTRS.GetStringFromName('passtab.newpwdforbiddenchars'), false);
    return false;	
  }
  
  return true;
}




function fillPwdSecBar(){

  var secb   = document.getElementById('pwdsecbar');
  //secb.setAttribute("mode","determined");
  //alert("pwdstrength: "+pwdStrength(document.getElementById('pwdnw1').value));
  secb.value = pwdStrength(document.getElementById('pwdnw1').value);

  
  if(!checkValidChars(document.getElementById('pwdnw1').value, document.getElementById('passerrormsg'))){
    secb.value = 0;
  }else{
    cleanPassError( document.getElementById('passerrormsg'));
  }

}



