#include "osslAux.h"

#include <openssl/x509v3.h>  //Lo incluyo aqu� por problemas de redefinicion de variables con la NSS


char * OSSL_X509_get_purposes(X509 * cert){

  unsigned long kusage;

  ASN1_BIT_STRING * usage = NULL;
  
  char * retval = (char *) malloc(90*sizeof(char));

  retval[0] = '\0';


  if((usage = (ASN1_BIT_STRING *) X509_get_ext_d2i(cert, NID_key_usage, NULL, NULL))) {

    if(usage->length > 0) {
      kusage = usage->data[0];
      if(usage->length > 1)
	kusage |= usage->data[1] << 8;
    } else 
      kusage = 0;

  }

  if(OSSL_X509_is_CA(cert)){

    //Si es CA, no mostramos purposes

    return NULL;

  }

  if(kusage & KU_DIGITAL_SIGNATURE){
    //printf("Es de firma digital\n");
    strcat(retval,"Sig#");
  }
  if(kusage & KU_KEY_ENCIPHERMENT){
    //printf("Es de cifrado de claves\n");
    strcat(retval,"KeyEnc#");
  }
  if(kusage & KU_NON_REPUDIATION){
    //printf("Es de no repudio\n");
    strcat(retval,"NoRep#");
  }
  if(kusage & KU_DATA_ENCIPHERMENT){
    //printf("Es de cifrado de datos\n");
    strcat(retval,"DataEnc#");
  }
  if(kusage & KU_KEY_AGREEMENT){
    //printf("Es de negociaci�n de clave\n");
    strcat(retval,"KeyAg#");
  }
  if(kusage & KU_KEY_CERT_SIGN){
    //printf("Es de firma de certificados\n");
    strcat(retval,"CertSig#");
  }
  if(kusage & KU_CRL_SIGN){
    //printf("Es de firma de CRLs\n");
    strcat(retval,"CRLSig#");
  }
  if(kusage & KU_ENCIPHER_ONLY){
    //printf("Es de solo cifrado\n");
    strcat(retval,"EncOnly#");
  }
  if(kusage & KU_DECIPHER_ONLY){
    //printf("Es de solo descifrado\n");
    strcat(retval,"DecOnly#");
  }
  
  retval[strlen(retval)-1] = '\0';
  
  //printf("CADENA DE SALIDA: %s\n",retval);

  return retval;

}

int OSSL_X509_is_CA(X509 * cert){


//int X509_check_ca(X509 *x);

  //printf("ES CA?????????????? %d\n", X509_check_ca(cert));

  return  X509_check_ca(cert);
}




int OSSL_X509_is_rootCA(X509 * cert){
  //Si es CA y se ha firmado a si mismo

  //printf("checkca devuelve: %d \n",X509_check_ca(cert) );
  //printf("checkissued devuelve: %d \n",X509_check_issued(cert, cert) );

  //Check issued funciona como strcmp, devuelve 0 para el match
  return  (X509_check_ca(cert) && !X509_check_issued(cert, cert));
}





 /*
        for (int i=0;i<X509_PURPOSE_get_count();i++){
          
          X509_PURPOSE *ptmp;
	  int tiene,tiene2;

	    if(){
	    tiene=1;
	  }
	  else{
	    tiene=0;
	  }

	  tiene = check_purpose_smime_sign((const X509_PURPOSE *) NULL, (const X509 *) cert, X509_check_ca(cert));
	  if (tiene) printf("Es de firma\n");
	  tiene = check_purpose_smime_encrypt((const X509_PURPOSE *) NULL, (const X509 *) cert, X509_check_ca(cert));
	  if (tiene) printf("Es de cifrado\n");


//tiene = X509_check_purpose(cert, i,  X509_check_ca(cert));

          ptmp = X509_PURPOSE_get0(i);
          printf("OSSLAUX: \t%-10s\t%s\t%-10d\n", X509_PURPOSE_get0_sname(ptmp), X509_PURPOSE_get0_name(ptmp), tiene);
 
        }
   
*/
/*



    //int X509_check_trust(X509 *x, int id, int flags); ?
        //int X509_TRUST_get_trust(X509_TRUST *xp); ?


int X509_check_issued(X509 *issuer, X509 *subject);
int X509_PURPOSE_get_count(void);
X509_PURPOSE * X509_PURPOSE_get0(int idx);
int X509_PURPOSE_get_by_sname(char *sname);
int X509_PURPOSE_get_by_id(int id);
char *X509_PURPOSE_get0_name(X509_PURPOSE *xp);
char *X509_PURPOSE_get0_sname(X509_PURPOSE *xp);
int X509_PURPOSE_get_id(X509_PURPOSE *);


int X509_check_purpose(X509 *x, int id, int ca);


        certpurpose

#define X509_PURPOSE_SSL_CLIENT         1
#define X509_PURPOSE_SSL_SERVER         2
#define X509_PURPOSE_NS_SSL_SERVER      3
#define X509_PURPOSE_SMIME_SIGN         4
#define X509_PURPOSE_SMIME_ENCRYPT      5
#define X509_PURPOSE_CRL_SIGN           6
#define X509_PURPOSE_ANY                7
#define X509_PURPOSE_OCSP_HELPER        8

typedef struct x509_purpose_st {
        int purpose;
        int trust;              // Default trust ID
        int flags;
        int (*check_purpose)(const struct x509_purpose_st *,
                                const X509 *, int);
        char *name;
        char *sname;
        void *usr_data;
} X509_PURPOSE;

*/
