/*
 * DO NOT EDIT.  THIS FILE IS GENERATED FROM IclauerCrypto.idl
 */

#ifndef __gen_IclauerCrypto_h__
#define __gen_IclauerCrypto_h__


#ifndef __gen_nsISupports_h__
#include "nsISupports.h"
#endif

/* For IDL files that don't want to include root IDL files. */
#ifndef NS_NO_VTABLE
#define NS_NO_VTABLE
#endif

/* starting interface:    IClauerCrypto */
#define ICLAUERCRYPTO_IID_STR "3aa73a46-2481-4963-ae38-1e898ad6d94a"

#define ICLAUERCRYPTO_IID \
  {0x3aa73a46, 0x2481, 0x4963, \
    { 0xae, 0x38, 0x1e, 0x89, 0x8a, 0xd6, 0xd9, 0x4a }}

class NS_NO_VTABLE NS_SCRIPTABLE IClauerCrypto : public nsISupports {
 public: 

  NS_DECLARE_STATIC_IID_ACCESSOR(ICLAUERCRYPTO_IID)

  /* string GetClauerSoftVersion (); */
  NS_SCRIPTABLE NS_IMETHOD GetClauerSoftVersion(char **_retval) = 0;

  /* string GetClauerUniverse (); */
  NS_SCRIPTABLE NS_IMETHOD GetClauerUniverse(char **_retval) = 0;

  /* string GetClauersListString (); */
  NS_SCRIPTABLE NS_IMETHOD GetClauersListString(char **_retval) = 0;

  /* void GetClauersList (out unsigned long tam, [array, size_is (tam), retval] out string clauers); */
  NS_SCRIPTABLE NS_IMETHOD GetClauersList(PRUint32 *tam, char ***clauers) = 0;

  /* long SetClauerActive (in string deviceName, in long uniq); */
  NS_SCRIPTABLE NS_IMETHOD SetClauerActive(const char *deviceName, PRInt32 uniq, PRInt32 *_retval) = 0;

  /* string GetClauerId (); */
  NS_SCRIPTABLE NS_IMETHOD GetClauerId(char **_retval) = 0;

  /* string GetClauerOwner (); */
  NS_SCRIPTABLE NS_IMETHOD GetClauerOwner(char **_retval) = 0;

  /* string GetClauerToken (in string name, in string pwd, in string chal, in string activeUrl); */
  NS_SCRIPTABLE NS_IMETHOD GetClauerToken(const char *name, const char *pwd, const char *chal, const char *activeUrl, char **_retval) = 0;

  /* long SetClauerCryptoMode (in string pwd); */
  NS_SCRIPTABLE NS_IMETHOD SetClauerCryptoMode(const char *pwd, PRInt32 *_retval) = 0;

  /* long SetClauerClearMode (); */
  NS_SCRIPTABLE NS_IMETHOD SetClauerClearMode(PRInt32 *_retval) = 0;

  /* long SetClauerPwd (in string pwd); */
  NS_SCRIPTABLE NS_IMETHOD SetClauerPwd(const char *pwd, PRInt32 *_retval) = 0;

  /* long SetNonInteractiveMode (); */
  NS_SCRIPTABLE NS_IMETHOD SetNonInteractiveMode(PRInt32 *_retval) = 0;

  /* long FormatClauer (in string pwd); */
  NS_SCRIPTABLE NS_IMETHOD FormatClauer(const char *pwd, PRInt32 *_retval) = 0;

  /* void GetClauerCertList (out unsigned long tam, [array, size_is (tam), retval] out string certs); */
  NS_SCRIPTABLE NS_IMETHOD GetClauerCertList(PRUint32 *tam, char ***certs) = 0;

  /* void GetFFXCertList (out unsigned long tam, [array, size_is (tam), retval] out string certs); */
  NS_SCRIPTABLE NS_IMETHOD GetFFXCertList(PRUint32 *tam, char ***certs) = 0;

  /* long PutClauerCertIntoFFX (in long idx); */
  NS_SCRIPTABLE NS_IMETHOD PutClauerCertIntoFFX(PRInt32 idx, PRInt32 *_retval) = 0;

  /* long PutFFXCertIntoClauer (in long idx); */
  NS_SCRIPTABLE NS_IMETHOD PutFFXCertIntoClauer(PRInt32 idx, PRInt32 *_retval) = 0;

  /* long PutClauerCertIntoP12 (in string ruta, in string p12pwd, in long idx); */
  NS_SCRIPTABLE NS_IMETHOD PutClauerCertIntoP12(const char *ruta, const char *p12pwd, PRInt32 idx, PRInt32 *_retval) = 0;

  /* long PutP12CertIntoClauer (in string ruta, in string p12pwd, in long importCAs); */
  NS_SCRIPTABLE NS_IMETHOD PutP12CertIntoClauer(const char *ruta, const char *p12pwd, PRInt32 importCAs, PRInt32 *_retval) = 0;

  /* long DeleteClauerCert (in long idx); */
  NS_SCRIPTABLE NS_IMETHOD DeleteClauerCert(PRInt32 idx, PRInt32 *_retval) = 0;

  /* long DeleteFFXCert (in long idx); */
  NS_SCRIPTABLE NS_IMETHOD DeleteFFXCert(PRInt32 idx, PRInt32 *_retval) = 0;

  /* long IsClauerCryptoMode (); */
  NS_SCRIPTABLE NS_IMETHOD IsClauerCryptoMode(PRInt32 *_retval) = 0;

  /* long IsFFXCryptoMode (); */
  NS_SCRIPTABLE NS_IMETHOD IsFFXCryptoMode(PRInt32 *_retval) = 0;

  /* long SetFFXCryptoMode (in string ffxpwd); */
  NS_SCRIPTABLE NS_IMETHOD SetFFXCryptoMode(const char *ffxpwd, PRInt32 *_retval) = 0;

  /* long SetFFXClearMode (); */
  NS_SCRIPTABLE NS_IMETHOD SetFFXClearMode(PRInt32 *_retval) = 0;

  /* long IsPKCS11Installed (in string name); */
  NS_SCRIPTABLE NS_IMETHOD IsPKCS11Installed(const char *name, PRInt32 *_retval) = 0;

  /* long InstallPKCS11 (in string name, in string path); */
  NS_SCRIPTABLE NS_IMETHOD InstallPKCS11(const char *name, const char *path, PRInt32 *_retval) = 0;

  /* long UninstallPKCS11 (in string name); */
  NS_SCRIPTABLE NS_IMETHOD UninstallPKCS11(const char *name, PRInt32 *_retval) = 0;

};

  NS_DEFINE_STATIC_IID_ACCESSOR(IClauerCrypto, ICLAUERCRYPTO_IID)

/* Use this macro when declaring classes that implement this interface. */
#define NS_DECL_ICLAUERCRYPTO \
  NS_SCRIPTABLE NS_IMETHOD GetClauerSoftVersion(char **_retval); \
  NS_SCRIPTABLE NS_IMETHOD GetClauerUniverse(char **_retval); \
  NS_SCRIPTABLE NS_IMETHOD GetClauersListString(char **_retval); \
  NS_SCRIPTABLE NS_IMETHOD GetClauersList(PRUint32 *tam, char ***clauers); \
  NS_SCRIPTABLE NS_IMETHOD SetClauerActive(const char *deviceName, PRInt32 uniq, PRInt32 *_retval); \
  NS_SCRIPTABLE NS_IMETHOD GetClauerId(char **_retval); \
  NS_SCRIPTABLE NS_IMETHOD GetClauerOwner(char **_retval); \
  NS_SCRIPTABLE NS_IMETHOD GetClauerToken(const char *name, const char *pwd, const char *chal, const char *activeUrl, char **_retval); \
  NS_SCRIPTABLE NS_IMETHOD SetClauerCryptoMode(const char *pwd, PRInt32 *_retval); \
  NS_SCRIPTABLE NS_IMETHOD SetClauerClearMode(PRInt32 *_retval); \
  NS_SCRIPTABLE NS_IMETHOD SetClauerPwd(const char *pwd, PRInt32 *_retval); \
  NS_SCRIPTABLE NS_IMETHOD SetNonInteractiveMode(PRInt32 *_retval); \
  NS_SCRIPTABLE NS_IMETHOD FormatClauer(const char *pwd, PRInt32 *_retval); \
  NS_SCRIPTABLE NS_IMETHOD GetClauerCertList(PRUint32 *tam, char ***certs); \
  NS_SCRIPTABLE NS_IMETHOD GetFFXCertList(PRUint32 *tam, char ***certs); \
  NS_SCRIPTABLE NS_IMETHOD PutClauerCertIntoFFX(PRInt32 idx, PRInt32 *_retval); \
  NS_SCRIPTABLE NS_IMETHOD PutFFXCertIntoClauer(PRInt32 idx, PRInt32 *_retval); \
  NS_SCRIPTABLE NS_IMETHOD PutClauerCertIntoP12(const char *ruta, const char *p12pwd, PRInt32 idx, PRInt32 *_retval); \
  NS_SCRIPTABLE NS_IMETHOD PutP12CertIntoClauer(const char *ruta, const char *p12pwd, PRInt32 importCAs, PRInt32 *_retval); \
  NS_SCRIPTABLE NS_IMETHOD DeleteClauerCert(PRInt32 idx, PRInt32 *_retval); \
  NS_SCRIPTABLE NS_IMETHOD DeleteFFXCert(PRInt32 idx, PRInt32 *_retval); \
  NS_SCRIPTABLE NS_IMETHOD IsClauerCryptoMode(PRInt32 *_retval); \
  NS_SCRIPTABLE NS_IMETHOD IsFFXCryptoMode(PRInt32 *_retval); \
  NS_SCRIPTABLE NS_IMETHOD SetFFXCryptoMode(const char *ffxpwd, PRInt32 *_retval); \
  NS_SCRIPTABLE NS_IMETHOD SetFFXClearMode(PRInt32 *_retval); \
  NS_SCRIPTABLE NS_IMETHOD IsPKCS11Installed(const char *name, PRInt32 *_retval); \
  NS_SCRIPTABLE NS_IMETHOD InstallPKCS11(const char *name, const char *path, PRInt32 *_retval); \
  NS_SCRIPTABLE NS_IMETHOD UninstallPKCS11(const char *name, PRInt32 *_retval); 

/* Use this macro to declare functions that forward the behavior of this interface to another object. */
#define NS_FORWARD_ICLAUERCRYPTO(_to) \
  NS_SCRIPTABLE NS_IMETHOD GetClauerSoftVersion(char **_retval) { return _to GetClauerSoftVersion(_retval); } \
  NS_SCRIPTABLE NS_IMETHOD GetClauerUniverse(char **_retval) { return _to GetClauerUniverse(_retval); } \
  NS_SCRIPTABLE NS_IMETHOD GetClauersListString(char **_retval) { return _to GetClauersListString(_retval); } \
  NS_SCRIPTABLE NS_IMETHOD GetClauersList(PRUint32 *tam, char ***clauers) { return _to GetClauersList(tam, clauers); } \
  NS_SCRIPTABLE NS_IMETHOD SetClauerActive(const char *deviceName, PRInt32 uniq, PRInt32 *_retval) { return _to SetClauerActive(deviceName, uniq, _retval); } \
  NS_SCRIPTABLE NS_IMETHOD GetClauerId(char **_retval) { return _to GetClauerId(_retval); } \
  NS_SCRIPTABLE NS_IMETHOD GetClauerOwner(char **_retval) { return _to GetClauerOwner(_retval); } \
  NS_SCRIPTABLE NS_IMETHOD GetClauerToken(const char *name, const char *pwd, const char *chal, const char *activeUrl, char **_retval) { return _to GetClauerToken(name, pwd, chal, activeUrl, _retval); } \
  NS_SCRIPTABLE NS_IMETHOD SetClauerCryptoMode(const char *pwd, PRInt32 *_retval) { return _to SetClauerCryptoMode(pwd, _retval); } \
  NS_SCRIPTABLE NS_IMETHOD SetClauerClearMode(PRInt32 *_retval) { return _to SetClauerClearMode(_retval); } \
  NS_SCRIPTABLE NS_IMETHOD SetClauerPwd(const char *pwd, PRInt32 *_retval) { return _to SetClauerPwd(pwd, _retval); } \
  NS_SCRIPTABLE NS_IMETHOD SetNonInteractiveMode(PRInt32 *_retval) { return _to SetNonInteractiveMode(_retval); } \
  NS_SCRIPTABLE NS_IMETHOD FormatClauer(const char *pwd, PRInt32 *_retval) { return _to FormatClauer(pwd, _retval); } \
  NS_SCRIPTABLE NS_IMETHOD GetClauerCertList(PRUint32 *tam, char ***certs) { return _to GetClauerCertList(tam, certs); } \
  NS_SCRIPTABLE NS_IMETHOD GetFFXCertList(PRUint32 *tam, char ***certs) { return _to GetFFXCertList(tam, certs); } \
  NS_SCRIPTABLE NS_IMETHOD PutClauerCertIntoFFX(PRInt32 idx, PRInt32 *_retval) { return _to PutClauerCertIntoFFX(idx, _retval); } \
  NS_SCRIPTABLE NS_IMETHOD PutFFXCertIntoClauer(PRInt32 idx, PRInt32 *_retval) { return _to PutFFXCertIntoClauer(idx, _retval); } \
  NS_SCRIPTABLE NS_IMETHOD PutClauerCertIntoP12(const char *ruta, const char *p12pwd, PRInt32 idx, PRInt32 *_retval) { return _to PutClauerCertIntoP12(ruta, p12pwd, idx, _retval); } \
  NS_SCRIPTABLE NS_IMETHOD PutP12CertIntoClauer(const char *ruta, const char *p12pwd, PRInt32 importCAs, PRInt32 *_retval) { return _to PutP12CertIntoClauer(ruta, p12pwd, importCAs, _retval); } \
  NS_SCRIPTABLE NS_IMETHOD DeleteClauerCert(PRInt32 idx, PRInt32 *_retval) { return _to DeleteClauerCert(idx, _retval); } \
  NS_SCRIPTABLE NS_IMETHOD DeleteFFXCert(PRInt32 idx, PRInt32 *_retval) { return _to DeleteFFXCert(idx, _retval); } \
  NS_SCRIPTABLE NS_IMETHOD IsClauerCryptoMode(PRInt32 *_retval) { return _to IsClauerCryptoMode(_retval); } \
  NS_SCRIPTABLE NS_IMETHOD IsFFXCryptoMode(PRInt32 *_retval) { return _to IsFFXCryptoMode(_retval); } \
  NS_SCRIPTABLE NS_IMETHOD SetFFXCryptoMode(const char *ffxpwd, PRInt32 *_retval) { return _to SetFFXCryptoMode(ffxpwd, _retval); } \
  NS_SCRIPTABLE NS_IMETHOD SetFFXClearMode(PRInt32 *_retval) { return _to SetFFXClearMode(_retval); } \
  NS_SCRIPTABLE NS_IMETHOD IsPKCS11Installed(const char *name, PRInt32 *_retval) { return _to IsPKCS11Installed(name, _retval); } \
  NS_SCRIPTABLE NS_IMETHOD InstallPKCS11(const char *name, const char *path, PRInt32 *_retval) { return _to InstallPKCS11(name, path, _retval); } \
  NS_SCRIPTABLE NS_IMETHOD UninstallPKCS11(const char *name, PRInt32 *_retval) { return _to UninstallPKCS11(name, _retval); } 

/* Use this macro to declare functions that forward the behavior of this interface to another object in a safe way. */
#define NS_FORWARD_SAFE_ICLAUERCRYPTO(_to) \
  NS_SCRIPTABLE NS_IMETHOD GetClauerSoftVersion(char **_retval) { return !_to ? NS_ERROR_NULL_POINTER : _to->GetClauerSoftVersion(_retval); } \
  NS_SCRIPTABLE NS_IMETHOD GetClauerUniverse(char **_retval) { return !_to ? NS_ERROR_NULL_POINTER : _to->GetClauerUniverse(_retval); } \
  NS_SCRIPTABLE NS_IMETHOD GetClauersListString(char **_retval) { return !_to ? NS_ERROR_NULL_POINTER : _to->GetClauersListString(_retval); } \
  NS_SCRIPTABLE NS_IMETHOD GetClauersList(PRUint32 *tam, char ***clauers) { return !_to ? NS_ERROR_NULL_POINTER : _to->GetClauersList(tam, clauers); } \
  NS_SCRIPTABLE NS_IMETHOD SetClauerActive(const char *deviceName, PRInt32 uniq, PRInt32 *_retval) { return !_to ? NS_ERROR_NULL_POINTER : _to->SetClauerActive(deviceName, uniq, _retval); } \
  NS_SCRIPTABLE NS_IMETHOD GetClauerId(char **_retval) { return !_to ? NS_ERROR_NULL_POINTER : _to->GetClauerId(_retval); } \
  NS_SCRIPTABLE NS_IMETHOD GetClauerOwner(char **_retval) { return !_to ? NS_ERROR_NULL_POINTER : _to->GetClauerOwner(_retval); } \
  NS_SCRIPTABLE NS_IMETHOD GetClauerToken(const char *name, const char *pwd, const char *chal, const char *activeUrl, char **_retval) { return !_to ? NS_ERROR_NULL_POINTER : _to->GetClauerToken(name, pwd, chal, activeUrl, _retval); } \
  NS_SCRIPTABLE NS_IMETHOD SetClauerCryptoMode(const char *pwd, PRInt32 *_retval) { return !_to ? NS_ERROR_NULL_POINTER : _to->SetClauerCryptoMode(pwd, _retval); } \
  NS_SCRIPTABLE NS_IMETHOD SetClauerClearMode(PRInt32 *_retval) { return !_to ? NS_ERROR_NULL_POINTER : _to->SetClauerClearMode(_retval); } \
  NS_SCRIPTABLE NS_IMETHOD SetClauerPwd(const char *pwd, PRInt32 *_retval) { return !_to ? NS_ERROR_NULL_POINTER : _to->SetClauerPwd(pwd, _retval); } \
  NS_SCRIPTABLE NS_IMETHOD SetNonInteractiveMode(PRInt32 *_retval) { return !_to ? NS_ERROR_NULL_POINTER : _to->SetNonInteractiveMode(_retval); } \
  NS_SCRIPTABLE NS_IMETHOD FormatClauer(const char *pwd, PRInt32 *_retval) { return !_to ? NS_ERROR_NULL_POINTER : _to->FormatClauer(pwd, _retval); } \
  NS_SCRIPTABLE NS_IMETHOD GetClauerCertList(PRUint32 *tam, char ***certs) { return !_to ? NS_ERROR_NULL_POINTER : _to->GetClauerCertList(tam, certs); } \
  NS_SCRIPTABLE NS_IMETHOD GetFFXCertList(PRUint32 *tam, char ***certs) { return !_to ? NS_ERROR_NULL_POINTER : _to->GetFFXCertList(tam, certs); } \
  NS_SCRIPTABLE NS_IMETHOD PutClauerCertIntoFFX(PRInt32 idx, PRInt32 *_retval) { return !_to ? NS_ERROR_NULL_POINTER : _to->PutClauerCertIntoFFX(idx, _retval); } \
  NS_SCRIPTABLE NS_IMETHOD PutFFXCertIntoClauer(PRInt32 idx, PRInt32 *_retval) { return !_to ? NS_ERROR_NULL_POINTER : _to->PutFFXCertIntoClauer(idx, _retval); } \
  NS_SCRIPTABLE NS_IMETHOD PutClauerCertIntoP12(const char *ruta, const char *p12pwd, PRInt32 idx, PRInt32 *_retval) { return !_to ? NS_ERROR_NULL_POINTER : _to->PutClauerCertIntoP12(ruta, p12pwd, idx, _retval); } \
  NS_SCRIPTABLE NS_IMETHOD PutP12CertIntoClauer(const char *ruta, const char *p12pwd, PRInt32 importCAs, PRInt32 *_retval) { return !_to ? NS_ERROR_NULL_POINTER : _to->PutP12CertIntoClauer(ruta, p12pwd, importCAs, _retval); } \
  NS_SCRIPTABLE NS_IMETHOD DeleteClauerCert(PRInt32 idx, PRInt32 *_retval) { return !_to ? NS_ERROR_NULL_POINTER : _to->DeleteClauerCert(idx, _retval); } \
  NS_SCRIPTABLE NS_IMETHOD DeleteFFXCert(PRInt32 idx, PRInt32 *_retval) { return !_to ? NS_ERROR_NULL_POINTER : _to->DeleteFFXCert(idx, _retval); } \
  NS_SCRIPTABLE NS_IMETHOD IsClauerCryptoMode(PRInt32 *_retval) { return !_to ? NS_ERROR_NULL_POINTER : _to->IsClauerCryptoMode(_retval); } \
  NS_SCRIPTABLE NS_IMETHOD IsFFXCryptoMode(PRInt32 *_retval) { return !_to ? NS_ERROR_NULL_POINTER : _to->IsFFXCryptoMode(_retval); } \
  NS_SCRIPTABLE NS_IMETHOD SetFFXCryptoMode(const char *ffxpwd, PRInt32 *_retval) { return !_to ? NS_ERROR_NULL_POINTER : _to->SetFFXCryptoMode(ffxpwd, _retval); } \
  NS_SCRIPTABLE NS_IMETHOD SetFFXClearMode(PRInt32 *_retval) { return !_to ? NS_ERROR_NULL_POINTER : _to->SetFFXClearMode(_retval); } \
  NS_SCRIPTABLE NS_IMETHOD IsPKCS11Installed(const char *name, PRInt32 *_retval) { return !_to ? NS_ERROR_NULL_POINTER : _to->IsPKCS11Installed(name, _retval); } \
  NS_SCRIPTABLE NS_IMETHOD InstallPKCS11(const char *name, const char *path, PRInt32 *_retval) { return !_to ? NS_ERROR_NULL_POINTER : _to->InstallPKCS11(name, path, _retval); } \
  NS_SCRIPTABLE NS_IMETHOD UninstallPKCS11(const char *name, PRInt32 *_retval) { return !_to ? NS_ERROR_NULL_POINTER : _to->UninstallPKCS11(name, _retval); } 

#if 0
/* Use the code below as a template for the implementation class for this interface. */

/* Header file */
class _MYCLASS_ : public IClauerCrypto
{
public:
  NS_DECL_ISUPPORTS
  NS_DECL_ICLAUERCRYPTO

  _MYCLASS_();

private:
  ~_MYCLASS_();

protected:
  /* additional members */
};

/* Implementation file */
NS_IMPL_ISUPPORTS1(_MYCLASS_, IClauerCrypto)

_MYCLASS_::_MYCLASS_()
{
  /* member initializers and constructor code */
}

_MYCLASS_::~_MYCLASS_()
{
  /* destructor code */
}

/* string GetClauerSoftVersion (); */
NS_IMETHODIMP _MYCLASS_::GetClauerSoftVersion(char **_retval)
{
    return NS_ERROR_NOT_IMPLEMENTED;
}

/* string GetClauerUniverse (); */
NS_IMETHODIMP _MYCLASS_::GetClauerUniverse(char **_retval)
{
    return NS_ERROR_NOT_IMPLEMENTED;
}

/* string GetClauersListString (); */
NS_IMETHODIMP _MYCLASS_::GetClauersListString(char **_retval)
{
    return NS_ERROR_NOT_IMPLEMENTED;
}

/* void GetClauersList (out unsigned long tam, [array, size_is (tam), retval] out string clauers); */
NS_IMETHODIMP _MYCLASS_::GetClauersList(PRUint32 *tam, char ***clauers)
{
    return NS_ERROR_NOT_IMPLEMENTED;
}

/* long SetClauerActive (in string deviceName, in long uniq); */
NS_IMETHODIMP _MYCLASS_::SetClauerActive(const char *deviceName, PRInt32 uniq, PRInt32 *_retval)
{
    return NS_ERROR_NOT_IMPLEMENTED;
}

/* string GetClauerId (); */
NS_IMETHODIMP _MYCLASS_::GetClauerId(char **_retval)
{
    return NS_ERROR_NOT_IMPLEMENTED;
}

/* string GetClauerOwner (); */
NS_IMETHODIMP _MYCLASS_::GetClauerOwner(char **_retval)
{
    return NS_ERROR_NOT_IMPLEMENTED;
}

/* string GetClauerToken (in string name, in string pwd, in string chal, in string activeUrl); */
NS_IMETHODIMP _MYCLASS_::GetClauerToken(const char *name, const char *pwd, const char *chal, const char *activeUrl, char **_retval)
{
    return NS_ERROR_NOT_IMPLEMENTED;
}

/* long SetClauerCryptoMode (in string pwd); */
NS_IMETHODIMP _MYCLASS_::SetClauerCryptoMode(const char *pwd, PRInt32 *_retval)
{
    return NS_ERROR_NOT_IMPLEMENTED;
}

/* long SetClauerClearMode (); */
NS_IMETHODIMP _MYCLASS_::SetClauerClearMode(PRInt32 *_retval)
{
    return NS_ERROR_NOT_IMPLEMENTED;
}

/* long SetClauerPwd (in string pwd); */
NS_IMETHODIMP _MYCLASS_::SetClauerPwd(const char *pwd, PRInt32 *_retval)
{
    return NS_ERROR_NOT_IMPLEMENTED;
}

/* long SetNonInteractiveMode (); */
NS_IMETHODIMP _MYCLASS_::SetNonInteractiveMode(PRInt32 *_retval)
{
    return NS_ERROR_NOT_IMPLEMENTED;
}

/* long FormatClauer (in string pwd); */
NS_IMETHODIMP _MYCLASS_::FormatClauer(const char *pwd, PRInt32 *_retval)
{
    return NS_ERROR_NOT_IMPLEMENTED;
}

/* void GetClauerCertList (out unsigned long tam, [array, size_is (tam), retval] out string certs); */
NS_IMETHODIMP _MYCLASS_::GetClauerCertList(PRUint32 *tam, char ***certs)
{
    return NS_ERROR_NOT_IMPLEMENTED;
}

/* void GetFFXCertList (out unsigned long tam, [array, size_is (tam), retval] out string certs); */
NS_IMETHODIMP _MYCLASS_::GetFFXCertList(PRUint32 *tam, char ***certs)
{
    return NS_ERROR_NOT_IMPLEMENTED;
}

/* long PutClauerCertIntoFFX (in long idx); */
NS_IMETHODIMP _MYCLASS_::PutClauerCertIntoFFX(PRInt32 idx, PRInt32 *_retval)
{
    return NS_ERROR_NOT_IMPLEMENTED;
}

/* long PutFFXCertIntoClauer (in long idx); */
NS_IMETHODIMP _MYCLASS_::PutFFXCertIntoClauer(PRInt32 idx, PRInt32 *_retval)
{
    return NS_ERROR_NOT_IMPLEMENTED;
}

/* long PutClauerCertIntoP12 (in string ruta, in string p12pwd, in long idx); */
NS_IMETHODIMP _MYCLASS_::PutClauerCertIntoP12(const char *ruta, const char *p12pwd, PRInt32 idx, PRInt32 *_retval)
{
    return NS_ERROR_NOT_IMPLEMENTED;
}

/* long PutP12CertIntoClauer (in string ruta, in string p12pwd, in long importCAs); */
NS_IMETHODIMP _MYCLASS_::PutP12CertIntoClauer(const char *ruta, const char *p12pwd, PRInt32 importCAs, PRInt32 *_retval)
{
    return NS_ERROR_NOT_IMPLEMENTED;
}

/* long DeleteClauerCert (in long idx); */
NS_IMETHODIMP _MYCLASS_::DeleteClauerCert(PRInt32 idx, PRInt32 *_retval)
{
    return NS_ERROR_NOT_IMPLEMENTED;
}

/* long DeleteFFXCert (in long idx); */
NS_IMETHODIMP _MYCLASS_::DeleteFFXCert(PRInt32 idx, PRInt32 *_retval)
{
    return NS_ERROR_NOT_IMPLEMENTED;
}

/* long IsClauerCryptoMode (); */
NS_IMETHODIMP _MYCLASS_::IsClauerCryptoMode(PRInt32 *_retval)
{
    return NS_ERROR_NOT_IMPLEMENTED;
}

/* long IsFFXCryptoMode (); */
NS_IMETHODIMP _MYCLASS_::IsFFXCryptoMode(PRInt32 *_retval)
{
    return NS_ERROR_NOT_IMPLEMENTED;
}

/* long SetFFXCryptoMode (in string ffxpwd); */
NS_IMETHODIMP _MYCLASS_::SetFFXCryptoMode(const char *ffxpwd, PRInt32 *_retval)
{
    return NS_ERROR_NOT_IMPLEMENTED;
}

/* long SetFFXClearMode (); */
NS_IMETHODIMP _MYCLASS_::SetFFXClearMode(PRInt32 *_retval)
{
    return NS_ERROR_NOT_IMPLEMENTED;
}

/* long IsPKCS11Installed (in string name); */
NS_IMETHODIMP _MYCLASS_::IsPKCS11Installed(const char *name, PRInt32 *_retval)
{
    return NS_ERROR_NOT_IMPLEMENTED;
}

/* long InstallPKCS11 (in string name, in string path); */
NS_IMETHODIMP _MYCLASS_::InstallPKCS11(const char *name, const char *path, PRInt32 *_retval)
{
    return NS_ERROR_NOT_IMPLEMENTED;
}

/* long UninstallPKCS11 (in string name); */
NS_IMETHODIMP _MYCLASS_::UninstallPKCS11(const char *name, PRInt32 *_retval)
{
    return NS_ERROR_NOT_IMPLEMENTED;
}

/* End of implementation class template. */
#endif


#endif /* __gen_IclauerCrypto_h__ */
