/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2; hs-minor-mode: t  -*-*/

 #include "clauerCrypto.h"

//Password del clauer: solo ASCII 7bit

NS_IMPL_ISUPPORTS1(ClauerCrypto, IClauerCrypto)

ClauerCrypto::ClauerCrypto()
{
  m_deviceName = NULL;
  m_pwdClauer = NULL;
  validCred=1;
  validIdent=1;
  m_uniq = 0;
  
  numFFXCerts    = 0;
  numClauerCerts = 0;
  ClauerCertList = NULL;
  FFXCertList    = NULL;

  m_pwdClauer = NULL;
  m_pwdFFX = NULL;

  //USER_CERT    = 1 << 1;
   
  OpenSSL_add_all_algorithms();
  

  LIBRT_Ini();
}

ClauerCrypto::~ClauerCrypto()
{
  if(m_deviceName)
    free(m_deviceName);
  if(m_pwdClauer){
    freeSecureString(m_pwdClauer, CLUI_MAX_PASS_LEN);
  }
  if(m_pwdFFX){
    freeSecureString(m_pwdFFX, CLUI_MAX_PASS_LEN);
    m_pwdFFX = NULL;

  }
  LIBRT_Fin();
}


/* string GetClauerSoftVersion (); */
NS_IMETHODIMP ClauerCrypto::GetClauerSoftVersion(char **_retval)
{

  char * cversion, * ret, *aux;

 
  if(LIBRT_ObtenerVersion(&cversion)){ //Si devuelve error
    cversion = (char*) malloc(sizeof(char)*20);
    //cout<<"+++++++++++++++++++++ERROR OBTENIENDO VERSION++++++++++++++++"<<endl;
    strcpy(cversion, "0#0.0.0");
  }
  
  //cout<<"Cadena completa: "<<cversion<<endl;

  for(int i=0; i<strlen(cversion);i++){
    if(cversion[i]=='#'){
     // cout<<"hay almohadilla"<<endl;
      //cversion[i]='\0';
      aux = &cversion[i+1];
      //cout<<"Subcadena? "<<aux<<endl;
      ret = (char *) malloc(sizeof(char)*(strlen(aux)+1));
      strcpy(ret, aux);
      
      break;
    }	
  }
  
  //cout<<"Ret: "<<ret<<endl;

  *_retval = ret;

  free(cversion);

 // cout<<"Retorno: "<<*_retval<<endl;

  return NS_OK;
}

NS_IMETHODIMP ClauerCrypto::GetClauerUniverse(char **_retval) //Es el de la GV, GC o UJI?
{

  //0 -> default algo mal o la uji
  //1 -> accv
  //2 -> idcat
  //3 -> coitag

 char * cversion, * ret;

  LIBRT_ObtenerVersion(&cversion);
  
  for(int i=0; i<strlen(cversion);i++){
    if(cversion[i]=='#'){
      cversion[i]='\0';
      ret = cversion;
      break;
    }	
  }
  

  *_retval = ret;
  
  //cout<<"Retorno: "<<*_retval<<endl;

  return NS_OK;
}





/* string GetClauersListString (); */
NS_IMETHODIMP ClauerCrypto::GetClauersListString(char **_retval)
{
  char *devices[MAX_DEVICES];
  int nDev,i, slen=0;
	
  if ( ( LIBRT_ListarDispositivos( &nDev, (unsigned char **) devices ) != 0 ) || ( nDev == 0 )) {
    *_retval=0;
    return NS_OK;
  }
        
  for(i=0;i<nDev;i++){
    slen+= 1+strlen(devices[i]);  //1+ por los \n y el \0 del final
  }
  *_retval = (char *) malloc(slen*sizeof(char));
    
  for(i=0;i<slen;i++)
    (*_retval)[i]    = '\0';

  for(i=0;i<nDev;i++){
    strcat(*_retval,"\n");
    strcat(*_retval,devices[i]);
  }

  return NS_OK;
}



/* void GetClauersList (out unsigned long tam, [array, size_is (tam), retval] out string clauers); */
NS_IMETHODIMP ClauerCrypto::GetClauersList(PRUint32 *tam, char ***clauers)
{
  char ** devices;
  int nDev,i, slen=0;

  devices = (char **) malloc(MAX_DEVICES*sizeof(char *)); //Reservamos el vector de devices

	
  if ( ( LIBRT_ListarDispositivos( &nDev, (unsigned char **) devices ) != 0 ) || ( nDev == 0 )) {
    free(devices);
    *clauers=0;
    return NS_OK;
  }
   
    
  *clauers = devices; 

  *tam = nDev;

  return NS_OK;
}



/* long SetClauerActive (in string deviceName, in long uniq); */
NS_IMETHODIMP ClauerCrypto::SetClauerActive(const char *deviceName, PRInt32 uniq, PRInt32 *_retval)
{
  USBCERTS_HANDLE hClauer;

  
  *_retval = 0;
  if ( !deviceName || strlen(deviceName) == 0  ) {  //Si no se especifica un dispositivo, se elige el primero
    char *devices[MAX_DEVICES];
    int nDev, i;

    if ( LIBRT_ListarDispositivos( &nDev, (unsigned char **) devices ) != 0 ){
      *_retval=0;
      return NS_OK;
    }
    if ( nDev == 0 ){
      *_retval=0;
      return NS_OK;
    }
    m_deviceName = (char *) malloc (strlen(devices[0])+1);

    if ( ! m_deviceName ) {
      for ( i = 0 ; (i < nDev) && devices[i] ; i++ )
	free(devices[i]);
      return NS_OK;
    }

    strcpy(m_deviceName, devices[0]);

    for ( i = 0 ; (i < nDev) && devices[i] ; i++ )
      free(devices[i]);

  } else {

    m_deviceName = (char *) malloc(strlen(deviceName)+1);
    if ( ! m_deviceName )
      return NS_OK;

    strcpy(m_deviceName, deviceName);

  }

  if ( LIBRT_IniciarDispositivo((unsigned char *)m_deviceName, NULL, &hClauer) != 0 ) {
    free(m_deviceName);
    m_deviceName = NULL;
    *_retval=0;
    return NS_OK;
  }

  if (uniq) { //si se proporciona un id unico hw, lo guarda en el contexto del objeto (HWid no implementado)
    unsigned char uniqIdD[16], uniqIdS[16];
    if (LIBRT_ObtenerHardwareId(&hClauer, uniqIdD, uniqIdS) || memcmp(uniqIdD,uniqIdS,16)) {
      free(m_deviceName);
      m_deviceName = NULL;
      *_retval=0;
      return NS_OK;
    }
  }
  m_uniq=uniq;

  LIBRT_FinalizarDispositivo(&hClauer);

  *_retval= 1;
  return NS_OK;
}


/* string GetClauerId (); */
NS_IMETHODIMP ClauerCrypto::GetClauerId(char **_retval)
{
  USBCERTS_HANDLE hClauer;

  if ( validIdent && !confirmDialog("Esta pagina intenta acceder al clauer. Permitir?")) {
    alertDialog("Acceso no permitido");
    *_retval = 0;  //Si no es un id valido o el usuario no autoriza el acceso, devuelve null
    return NS_OK;
  }
  validIdent=0;

  if ( ! m_deviceName ) {
    *_retval = 0; //Si no se ha seleccionado dispositivo activo, devuelve null
    return NS_OK;
  }

  if ( LIBRT_IniciarDispositivo((unsigned char *)m_deviceName, NULL, &hClauer) != 0 ) {
    *_retval = 0;  //Si falla el iniciardisp, devuelve null
    return NS_OK;
  }

  *_retval = bin2hex(hClauer.idDispositivo,CLAUER_ID_LEN); //al iniciar el clauer, en idDispositivo se copia el identificador software, que puede alterarse

  if ( ! *_retval ){
    return NS_OK;
  }
  LIBRT_FinalizarDispositivo(&hClauer);


  return NS_OK;
}

/* string GetClauerOwner (); */
NS_IMETHODIMP ClauerCrypto::GetClauerOwner(char **_retval)
{
   
  USBCERTS_HANDLE hClauer;
  char * owner;

  if ( validIdent && !confirmDialog("Esta pagina intenta acceder al clauer. Permitir?")) {
    *_retval = 0;  //Si no es un id valido o el usuario no autoriza el acceso, devuelve null
    return NS_OK;
  }
  validIdent=0;


  if ( ! m_deviceName ) {
    *_retval = 0; //Si no se ha seleccionado dispositivo activo, devuelve null
    return NS_OK;
  }

  if ( LIBRT_IniciarDispositivo((unsigned char *)m_deviceName, NULL, &hClauer) != 0 ) {
    *_retval = 0;  //Si falla el iniciardisp, devuelve null
    return NS_OK;
  }

  if ( clauerOwner(&hClauer, &owner ) ) {
    *_retval = owner;
    LIBRT_FinalizarDispositivo(&hClauer);
    return NS_OK;
  }
  
  LIBRT_FinalizarDispositivo(&hClauer);
  *_retval = 0;

  return NS_OK;
}


/* long SetClauerCryptoMode (in string pwd); */
NS_IMETHODIMP ClauerCrypto::SetClauerCryptoMode(const char *pwd, PRInt32 *_retval)
{

  USBCERTS_HANDLE hClauer;
  int ret, val;
  char *aux;

  *_retval = 0;

  //cout<<"Password introducido ("<<pwd<<"): ";

  //for (int zz=0; zz<strlen(pwd);zz++)
//	cout<<std::hex<<((short)pwd[zz]);
 // cout<<endl;

  if ( ! m_deviceName ) {
    *_retval = 0;
    return NS_OK;
  }


  if (pwd) {
    aux = allocateSecureString(CLUI_MAX_PASS_LEN);
    if (!aux)
      return NS_OK;
    strncpy(aux, pwd, CLUI_MAX_PASS_LEN);
    aux[CLUI_MAX_PASS_LEN] = '\0'; //Si el pass desbordara la long max, no se escribiria el \0
  }
  else {
    aux = new char [1];
    aux[0]=0;
  }

  if (aux[0] == 0) {
    delete [] aux;

    aux = allocateSecureString(CLUI_MAX_PASS_LEN);

    if (!aux)
      return NS_OK;

    if ( !passwordDialog("Introduzca el password del clauer",aux,CLUI_MAX_PASS_LEN) ) {
      
      if (freeSecureString(aux, CLUI_MAX_PASS_LEN) !=0)
        return NS_OK;

      aux = NULL;
      *_retval = 0;
      return NS_OK;
    }
    else if (strlen(aux) == 0) {

      if (freeSecureString(aux, CLUI_MAX_PASS_LEN) !=0)
        return NS_OK;
      
      aux = NULL;
      *_retval = 0;
      return NS_OK;
    }
    m_pwdClauer = aux;
  }
  else{
    m_pwdClauer=aux;
  }

  val = LIBRT_IniciarDispositivo((unsigned char *)m_deviceName, m_pwdClauer, &hClauer);


  if (val  != 0 ){
    if (m_pwdClauer){
      if (freeSecureString(m_pwdClauer, CLUI_MAX_PASS_LEN) !=0)
        return NS_OK;
      m_pwdClauer=NULL;
      
    }
    *_retval = 0;
    return NS_OK;
    
  }

  validIdent=0;
  LIBRT_FinalizarDispositivo(&hClauer);
  
  *_retval= 1;

  return NS_OK;

}

/* long SetClauerClearMode (); */
NS_IMETHODIMP ClauerCrypto::SetClauerClearMode(PRInt32 *_retval)
{
  if (m_pwdClauer) {
    if (freeSecureString(m_pwdClauer, CLUI_MAX_PASS_LEN) !=0){
      *_retval= 0;
      return NS_OK;
    }
    m_pwdClauer = NULL;
  }
  *_retval= 1;
  return NS_OK;
}

/* long SetClauerPwd (in string pwd); */ 
NS_IMETHODIMP ClauerCrypto::SetClauerPwd(const char *pwd, PRInt32 *_retval)
{
  USBCERTS_HANDLE hClauer;
  int ret;

  char *aux, *aux2;

  *_retval= 0;

/************DEBUG ELIMINAR**************/
//  cout<<"Password introducido: "<<pwd<<endl;
//  *_retval= 0;
 // return NS_OK;
/**************************/

  if (! m_deviceName || ! m_pwdClauer) {  //si no hay clauer activo o no estamos en sesion autenticada
    return NS_OK;
  }
	
  aux = allocateSecureString(CLUI_MAX_PASS_LEN);
  if (!aux)
    return NS_OK;
  aux[0]=0;
	
  if(pwd!=0){  //si el nuevo pass se pasa por params
    strncpy(aux, pwd, CLUI_MAX_PASS_LEN+1);
    if (aux[CLUI_MAX_PASS_LEN] != '\0'){
      alertDialog("Password demasiado largo");
      if (freeSecureString(aux, CLUI_MAX_PASS_LEN) !=0)
	return NS_OK;
      *_retval= 0;
      return NS_OK;
    }
  }
  else{  //si se pide interactivamente
    if ( !passwordDialog("Introduzca nuevo password para el clauer",aux,CLUI_MAX_PASS_LEN) ) {
			
      if (freeSecureString(aux, CLUI_MAX_PASS_LEN) !=0)
        return NS_OK;
      aux = NULL;
      *_retval = 0;
      return NS_OK;
    }
		
    aux2 = allocateSecureString(CLUI_MAX_PASS_LEN);
    if (!aux2)
      return NS_OK;
    aux2[0]=0;

    if ( !passwordDialog("Verifique el nuevo password",aux2,CLUI_MAX_PASS_LEN) ) {
      if (freeSecureString(aux, CLUI_MAX_PASS_LEN) !=0)
        return NS_OK;
      aux = NULL;
      if (freeSecureString(aux2, CLUI_MAX_PASS_LEN) !=0)
        return NS_OK;
      aux2 = NULL;
      *_retval = 0;
      return NS_OK;
    }
    if(strcmp(aux,aux2)!=0){
			
      alertDialog("El nuevo password no coincide con la verificacion");
      if (freeSecureString(aux, CLUI_MAX_PASS_LEN) !=0)
        return NS_OK;
      aux = NULL;

      if (freeSecureString(aux2, CLUI_MAX_PASS_LEN) !=0)
        return NS_OK;
      aux2 = NULL;
      *_retval = 0;
      return NS_OK;
    }

    if (freeSecureString(aux2, CLUI_MAX_PASS_LEN) !=0)
      return NS_OK;
    aux2 = NULL;
    *_retval = 0;
  }

  if (aux[0] == 0) {

    if (freeSecureString(aux, CLUI_MAX_PASS_LEN) !=0)
      return NS_OK;
    aux = NULL;

    *_retval = 0;
    return NS_OK;
  }

  if ( LIBRT_IniciarDispositivo((unsigned char *)m_deviceName, m_pwdClauer, &hClauer) != 0 ||
       LIBRT_CambiarPassword(&hClauer,aux) != 0)
    {
      if (freeSecureString(m_pwdClauer, CLUI_MAX_PASS_LEN) !=0)
        return NS_OK;
      m_pwdClauer = NULL;
      if (freeSecureString(aux, CLUI_MAX_PASS_LEN) !=0)
        return NS_OK;
      aux = NULL;
      *_retval = 0;
      return NS_OK;
    }

  LIBRT_FinalizarDispositivo(&hClauer);

  if (freeSecureString(aux, CLUI_MAX_PASS_LEN) !=0)
    return NS_OK;
  aux = NULL;
  *_retval= 1;

  return NS_OK;
}

/* string GetClauerToken (in string name, in string pwd, in string chal); */
NS_IMETHODIMP ClauerCrypto::GetClauerToken(const char *name, const char *pwd, const char *chal,  const char *activeUrl,char **_retval)
{

  typedef struct {
    char nom[lNom];
    unsigned char attr;
    char cond[lCond];
    unsigned char tok[lTok];
  } credencial;

  typedef struct {
    char cab[8];
    credencial cartera[ntk];
    char fin[8];
  } blok;

  blok bloque;
  USBCERTS_HANDLE hClauer;

  char aux[lCred+1];
  char * url;
  int i, j, prim;
  long nubl;
  credencial *tok = NULL;
  credencial jarl;

  if ( ! m_deviceName ) {  //Si no hay dispositivo conectado, sale
    //cout<<"No hay dispositivo conectado"<<endl;
    *_retval = 0;
    return NS_OK;
  }

  aux[0]=0;
  if (name){  //copia el nombre del token
    strncpy(aux, name,lNom);
    //cout<<"Copiando nombre de token: "<<aux<<endl;
  }

  if ( aux[0] == 0 ) {  //Si no se proporciona un nombre de token, vuelve
    //cout<<"No hay nombre de token."<<endl;
    *_retval = 0;
    return NS_OK;
  }

  if ( LIBRT_IniciarDispositivo((unsigned char *)m_deviceName, m_pwdClauer, &hClauer) != 0 ) { //abre conexion
    //cout<<"Error conectando con el clauer"<<endl;
    *_retval = 0;
    return NS_OK;
  }

  prim= 1;
  nubl=0;
	
  while (! LIBRT_LeerTipoBloqueCrypto(&hClauer, 0x0c, prim, (unsigned char *) &bloque, &nubl))//lee el bloque de credenciales
    {
      //cout<<"Leyendo bloque de credenciales: "<<nubl<<endl;
      if ( nubl == -1) //si el num de bloque es -1 sale
	break;
      for (j = 0; j <ntk; j++){  //para cada credencial de la cartera


	if((bloque.cartera[j].attr && 1)){
	  //cout<<"Probando con credencial: "<<bloque.cartera[j].nom<<" de tipo: "<<bin2hex(&(bloque.cartera[j].attr),1)<<endl;
	  //cout<<"Attr?: "<<(bloque.cartera[j].attr && 1)<<" CMP?: "<<(! strncmp(bloque.cartera[j].nom,aux,lNom))<<endl;
	}
	if ((bloque.cartera[j].attr && 1) && (! strncmp(bloque.cartera[j].nom,aux,lNom))) { //si attr tiene el ultimo bit a 1 y el nombre coincide con el de los param, es este. sale (y se guarda una referencia)
	  //cout<<"Token encontrado."<<endl;

	  //cout<<"Este es el token1:"<<endl;
	  //cout<<"Nombre: "<<bloque.cartera[j].nom<<endl;
	  //cout<<"Attr: "<<bin2hex(&(bloque.cartera[j].attr),1)<<endl;
	  //cout<<"Condicion: "<<bloque.cartera[j].cond<<endl;
	  //cout<<"Token: "<<bin2hex(bloque.cartera[j].tok,lTok)<<endl;

	  tok=&bloque.cartera[j];
	  break;
	}
      }
      if (tok != NULL){
	//cout<<"Token encontrado (sale)."<<endl;
	break;
      }
      prim= 0;
    }

  if(tok!=NULL){
    //cout<<"Este es el token2:"<<endl;
    //cout<<"Nombre: "<<tok->nom<<endl;
    //cout<<"Attr: "<<bin2hex(&(tok->attr),1)<<endl;
    //cout<<"Condicion: "<<tok->cond<<endl;
    //cout<<"Token: "<<bin2hex((unsigned char *)tok->tok,lTok)<<endl;
  }

  if (tok == NULL) { //si ha recorrido todo el bloque y la credencial no existe sale
    //cout<<"No se encuentra el token"<<endl;
    *_retval = 0;
    return NS_OK;
  }

  if (tok->cond[0] == 0 ) {  //Si la credencial no tiene condicion (regexp), pide permiso al usuario
    //cout<<"La Credencial no tiene condicion"<<endl;
    if ( validCred && !confirmDialog("Esta pagina solicita acceso a una credencial sin condicion de su cartera. Permitir?")) {
      alertDialog("Acceso a cartera no permitido");
      *_retval = 0;  
      return NS_OK;
    }
    validCred=0;
  }
  else { //Si existe una cond en forma de regexp

    tok->cond[lCond-1]= 0;  //Pone terminador de cadena a la condicion		

    //cout<<"La Credencial tiene condicion (con  term): "<<tok->cond<<endl;

    //obtener la url activa
    url = getURL();  //comentado hasta que sepa por qu� no funciona en ffx3. usando apa�o desde el comp en js
    //url = (char *) activeUrl;

    if (url == 0){  //Si no hay url, sale
      //cout<<"No hay url"<<endl;
      *_retval = 0;
      return NS_OK;
    }
    //cout<<"-----------URL: "<< url <<endl;

    //compararla con la cond
    if (! pcrecpp::RE(tok->cond).FullMatch(url)) { //si no hace match la url con la cond, sale
      alertDialog("\n\n\tEsta pagina quiere acceder a una credencial a la que no tiene derecho.\t\n\tEl acceso se ha denegado.\n\n");
      *_retval = 0;
      return NS_OK;
    }
    //cout<<"La credencial hace match"<<endl;
  }
  //cout<<"==> GetClauerToken: tenemos acceso "<<endl;
  //Llegados a este punto, hay permiso o la cond coincide con la url
  //en este if no debe entrar nunca, porque no se generan  tokens con este bit activado. quiere decir que
  if (tok->attr > (unsigned char)128) {//Si el msb de attr esta a 1, cifra el token con el id hw único 
    unsigned char uniqIdD[16], uniqIdS[16];
    if (LIBRT_ObtenerHardwareId(&hClauer, uniqIdD, uniqIdS) || memcmp(uniqIdD,uniqIdS,16)) { //ObtenerHardwareId no esta implementada
      *_retval = 0;
      return NS_OK;
    }
    //cout<<"==> GetClauerToken:  token encriptado con uniqid "<<endl;
    arc4((char *)uniqIdD,sizeof(uniqIdD),tok->tok,lTok);
  }

  LIBRT_FinalizarDispositivo(&hClauer);

  aux[0]=0;
  if (pwd)
    strncpy(aux, pwd, lTok);
  aux[lTok]=0; // por si pwd tiene long=lTok  //ver si esto es lo que la fastidia, pq machaca el passwd ??????
  //cout<<"==> GetClauerToken: pass: "<<aux<<endl;
  if (aux[0] != 0){//si tienee pwd, cifra la credencial con el
    arc4(aux,strlen(aux),tok->tok,lTok);
    //cout<<"==> GetClauerToken: token cifrado con pass"<<endl;
  }
  aux[0]=0;
  if (chal){
    strncpy(aux, chal, lCred);
    //cout<<"==> GetClauerToken: challenge: "<<chal<<endl;
  }
  aux[lCred]=0; j=strlen(aux);//Revisar
  //cout<<"==> GetClauerToken: strlen challenge: "<<j<<endl;
  if (aux[0] != 0) { //si hay challenge, el valor devuelto es challenge cifrado con arc4, usando como contraseña el valor de la credencial (cifrado con pwd si procede)
    arc4((char *)tok->tok,lTok,(unsigned char*)aux,j);
    *_retval = bin2hex((unsigned char*)aux,j);
    //cout<<"==> GetClauerToken: ret challenge cifrado con el token: "<<*_retval<<endl;
  }
  else{
    *_retval = bin2hex((unsigned char*)tok->tok,lTok); //devuelve la credencial
    //cout<<"==> GetClauerToken: ret token cifrado con pwd y tal vez con uniqid: "<<*_retval<<endl;
  }


#ifdef ONWORK
#endif

  return NS_OK;
}









//REVISAR ESTAS

// Para que en los Chrome no salga la ventanita de confirmación al acceder al clauer en modo claro
/* void SetNonInteractiveMode (); */
NS_IMETHODIMP ClauerCrypto::SetNonInteractiveMode(PRInt32 *_retval)
{
    validIdent=0;
    *_retval = 1;
    return NS_OK;
}

NS_IMETHODIMP ClauerCrypto::FormatClauer(const char *pwd, PRInt32 *_retval)
{

  USBCERTS_HANDLE hClauer;
  int ret;
  int resultado;
  int error=7, success=0;

  char *aux, *aux2;
  //char * dispositivo;

  *_retval= error;

/************DEBUG ELIMINAR**************/
//  cout<<"Password introducido: "<<pwd<<endl;
//  *_retval= error;
 // return NS_OK;
/**************************/

  if (! m_deviceName /*|| ! m_pwdClauer*/) {  //si no hay clauer activo  //No hace falta estar autenticado //o no estamos en sesion autenticada
    return NS_OK;
  }
	
  aux = allocateSecureString(CLUI_MAX_PASS_LEN);
  if (!aux)
    return NS_OK;
  aux[0]=0;
	
  if(pwd!=0){  //si el nuevo pass se pasa por params
    strncpy(aux, pwd, CLUI_MAX_PASS_LEN+1);
    if (aux[CLUI_MAX_PASS_LEN] != '\0'){
      alertDialog("Password demasiado largo");
      if (freeSecureString(aux, CLUI_MAX_PASS_LEN) !=0)
	return NS_OK;
      *_retval= error;
      return NS_OK;
    }
  }
  else{  //si se pide interactivamente
    if ( !passwordDialog("Introduzca nuevo password para el clauer",aux,CLUI_MAX_PASS_LEN) ) {
			
      if (freeSecureString(aux, CLUI_MAX_PASS_LEN) !=0)
	return NS_OK;
      aux = NULL;
      *_retval = error;
      return NS_OK;
    }
		
    aux2 = allocateSecureString(CLUI_MAX_PASS_LEN);
    if (!aux2)
      return NS_OK;
    aux2[0]=0;

    if ( !passwordDialog("Verifique el nuevo password",aux2,CLUI_MAX_PASS_LEN) ) {
      if (freeSecureString(aux, CLUI_MAX_PASS_LEN) !=0)
	return NS_OK;
      aux = NULL;
      if (freeSecureString(aux2, CLUI_MAX_PASS_LEN) !=0)
	return NS_OK;
      aux2 = NULL;
      *_retval = error;
      return NS_OK;
    }
    if(strcmp(aux,aux2)!=0){
			
      alertDialog("El nuevo password no coincide con la verificacion");
      if (freeSecureString(aux, CLUI_MAX_PASS_LEN) !=0)
	return NS_OK;
      aux = NULL;

      if (freeSecureString(aux2, CLUI_MAX_PASS_LEN) !=0)
	return NS_OK;
      aux2 = NULL;
      *_retval = error;
      return NS_OK;
    }

    if (freeSecureString(aux2, CLUI_MAX_PASS_LEN) !=0)
      return NS_OK;
    aux2 = NULL;
    *_retval = error;
  }

  if (aux[0] == 0) {

    if (freeSecureString(aux, CLUI_MAX_PASS_LEN) !=0)
      return NS_OK;
    aux = NULL;

    *_retval = error;
    return NS_OK;
  }

  if ( LIBRT_IniciarDispositivo((unsigned char *)m_deviceName, m_pwdClauer, &hClauer))
    {

      //cout<<"Error conectando con el clauer para formatear"<<endl;
      if (freeSecureString(m_pwdClauer, CLUI_MAX_PASS_LEN) !=0)
	return NS_OK;
      m_pwdClauer = NULL;
      if (freeSecureString(aux, CLUI_MAX_PASS_LEN) !=0)
	return NS_OK;
      aux = NULL;
      *_retval = error;
      return NS_OK;
    }

    //dispositivo = (char *) malloc(strlen(m_deviceName)+1);
    //strcpy(dispositivo, m_deviceName);


    //Parece que, a diferencia de las demás funciones de la librt, pide la cadena del dispositivo, no el handler
    resultado = LIBRT_FormatearClauerCrypto((char *)m_deviceName , (char *) pwd);
    if (resultado){
       //cout<<"Error ("<<resultado<<") durante el formateo (no implementado?)"<<endl;
      if (freeSecureString(m_pwdClauer, CLUI_MAX_PASS_LEN) !=0)
	return NS_OK;
      m_pwdClauer = NULL;
      if (freeSecureString(aux, CLUI_MAX_PASS_LEN) !=0)
	return NS_OK;
      aux = NULL;
      *_retval = resultado;
      return NS_OK;
    }

  LIBRT_FinalizarDispositivo(&hClauer);

  if (freeSecureString(aux, CLUI_MAX_PASS_LEN) !=0)
    return NS_OK;
  aux = NULL;


  *_retval= success;
  return NS_OK;

}




/* void GetClauerCertList (out unsigned long tam, [array, size_is (tam), retval] out string certs); */
NS_IMETHODIMP ClauerCrypto::GetClauerCertList(PRUint32 *tam, char ***certs)
{
  
 
  USBCERTS_HANDLE hClauer;
  unsigned char * listabloques;
  long nBlock = 0;
  long *hBloques;
  unsigned char block[TAM_BLOQUE];
  unsigned char * PEMcert;


  char ca[3]  = "CA";
  char usr[5] = "USER";
  char oth[6] = "OTHER";

  char * type;

  
  char ** cabeceras;

  char * pemcert;
  int pemlen;

  BIO  *  buff;  
  X509 * certx509;
  char * msg;

  unsigned int Tipos[4] = {BLOQUE_CERT_RAIZ, BLOQUE_CERT_INTERMEDIO, BLOQUE_CERT_PROPIO, BLOQUE_CERT_OTROS };
  unsigned int tipo;  //SEGUIR: Double free de esto?? y ver el problema del purpose

  long blockpos = 0;

  FILE * QQQ;

  //cout<<"Se llama a getclauercertlist ---------------------------------------"<<endl;

  //cout<<"numClauerCerts:::::  "<<numClauerCerts<<" ---------------------------------------"<<endl;


  if (!m_deviceName){
    //cout<<"No hay  clauer"<<endl;
    return 0;
    
  }
  

  if(ClauerCertList){//Reescribimos la lista completa
    for(int i=0; i<numClauerCerts; i++){
      free(ClauerCertList[i]->PEMCert);
      free(ClauerCertList[i]);
    }
    free(ClauerCertList);
    ClauerCertList = NULL; 
  }
  numClauerCerts = 0;


  
  //cout<<"CHK1 ---------------------------------------"<<endl;


  for(int tp = 0; tp< 4; tp++){

    tipo = Tipos[tp];
    
        
    if ( LIBRT_IniciarDispositivo((unsigned char *) m_deviceName,NULL, &hClauer) != 0 ){
      //fprintf(stderr, "[ERROR] No se pudo iniciar dispositivo !!!!!!!!!!!!!!!!!!!!!!!!!!!!\n");
      return 0;
    }


    //Contamos el numero de bloques
    if ( LIBRT_LeerTodosBloquesTipo(&hClauer, tipo, NULL, NULL, &nBlock) != 0 ) {
      //fprintf(stderr, "Error contando certificados \n");
      return 0;
    }
    if ( nBlock == -1 ){
      //fprintf(stderr, "[ERROR] Al contar el n�mero de bloques\n");
      return 0;
    }

    //cout<<"Hay "<<nBlock<<" certs de tipo "<<tp<<"?"<<endl;

    //Si hay alg�n cert de ese tipo
    if(nBlock > 0){
      
 

      //cout<<"S� que continua para tipo "<<tp<<endl;


      //Reservamos el vector de bloques
      listabloques = ( unsigned char * ) malloc ( TAM_BLOQUE *sizeof(unsigned char) * nBlock );
  
      if ( ! listabloques ) {
        //fprintf(stderr, "Error. No queda memoria.\n");
        return 0;
      }
  
      hBloques = ( long *) malloc ( sizeof(long) * nBlock);
      if ( ! hBloques ) {
        //fprintf(stderr, "Error. No queda memoria.\n");
        return 0;
      }
  
      //Leemos todos los bloques de  certificados de un tipo
      if ( LIBRT_LeerTodosBloquesTipo(&hClauer, tipo, hBloques, listabloques, &nBlock) != 0 ) {
        //fprintf(stderr, "[ERROR] Enumerando certificados\n");
        return 0;
      }

/*
      if(tipo == BLOQUE_CERT_INTERMEDIO){
        QQQ = fopen("/home/paco/Desktop/listabloques.bin","w");
        
        fwrite(listabloques,1, TAM_BLOQUE*nBlock,QQQ);
        
        fclose(QQQ);
      
        
        cout<<"******************TAM BLOQUE: "<<BLOQUE_CERTINTERMEDIO_Get_Tam(listabloques)<<endl<<endl<<endl;
        cout<<"******************OBJ: "<<BLOQUE_CERTINTERMEDIO_Get_Objeto(listabloques)<<endl;

      }
*/
      //cout<<"NBLOCK DESPUES DE LEERLOS TODOS: "<<nBlock<<endl;

      // numClauerCerts += nBlock;

      //Procesamos todos los bloques y guardamos los certs en pem en el vector
      
      //cout<<"CHKC ---------------------------------------"<<endl;
      //cout<<"numClauerCerts: "<<numClauerCerts<<endl;
      
      //Cada llamada a esta funci�n, amplia el vector  y empieza a escribir en numClauerCerts, que es la siguiente libre.

      ClauerCertList = (certLine **) realloc(ClauerCertList, (numClauerCerts+nBlock)*sizeof(certLine *)); 
    
      for(int i=numClauerCerts;i<(numClauerCerts+nBlock);i++){
        
        //cout<<i<<endl;

        int lbloqiter = i-numClauerCerts;
        
        memcpy(block, listabloques+(lbloqiter*TAM_BLOQUE), TAM_BLOQUE);
        blockpos = hBloques[lbloqiter]; //As� sabemos que numd e bloque dentro del clauer contiene este objeto.
        
      /*  
        if(tipo == BLOQUE_CERT_INTERMEDIO){        
          cout<<"******************TAM BLOQUE: "<<BLOQUE_CERTINTERMEDIO_Get_Tam(block)<<endl<<endl<<endl;
          cout<<"******************OBJ: "<<BLOQUE_CERTINTERMEDIO_Get_Objeto(block)<<endl;
          
        }
*/
        
        
        if(tipo == BLOQUE_CERT_PROPIO)          PEMcert  = BLOQUE_CERTPROPIO_Get_Objeto(block);
        else if(tipo == BLOQUE_CERT_RAIZ)       PEMcert  = BLOQUE_CERTRAIZ_Get_Objeto(block);
        else if(tipo == BLOQUE_CERT_INTERMEDIO) PEMcert  = BLOQUE_CERTINTERMEDIO_Get_Objeto(block);
        else if(tipo == BLOQUE_CERT_OTROS)      PEMcert  = BLOQUE_CERTOTROS_Get_Objeto(block);
        else{
          //cout<<"ERROR: Tipo desconocido"<<endl;
          return 0;
        }

        //cout<<"CHK+ ---------------------------------------"<<endl;
        //cout<<"PEMCERT: "<<PEMcert<<endl;



/*
        if(tipo == BLOQUE_CERT_PROPIO)          cout<<"TAM: "<<BLOQUE_CERTPROPIO_Get_Tam(block)<<endl;
        else if(tipo == BLOQUE_CERT_RAIZ)       cout<<"TAM: "<<BLOQUE_CERTRAIZ_Get_Tam(block)<<endl;
        else if(tipo == BLOQUE_CERT_INTERMEDIO) cout<<"TAM: "<<BLOQUE_CERTINTERMEDIO_Get_Tam(block)<<endl;
        else if(tipo == BLOQUE_CERT_OTROS)      cout<<"TAM: "<<BLOQUE_CERTOTROS_Get_Tam(block)<<endl;
*/        



    
        ClauerCertList[i] = (certLine *) malloc(sizeof(certLine)); 

        ClauerCertList[i]->PEMCert =  (unsigned char *) malloc((strlen((const char *)PEMcert) +1) * sizeof(unsigned char));
            
        strcpy((char *)ClauerCertList[i]->PEMCert, (char *) PEMcert);
        //cout<<"PEMCERT2: "<<ClauerCertList[i]->PEMCert<<endl;


        if(tipo == BLOQUE_CERT_PROPIO)          type = usr;
        else if(tipo == BLOQUE_CERT_RAIZ)       type = ca;
        else if(tipo == BLOQUE_CERT_INTERMEDIO) type = ca;
        else                                    type = oth;

        strcpy(ClauerCertList[i]->kind, type);

        ClauerCertList[i]->numBlock = blockpos;
        
        //cout<<"Tipo: "<<ClauerCertList[i]->kind<<endl;
            
                
      }
  
      numClauerCerts +=nBlock;

      //Liberar vector de bloques
      free(listabloques);
      free(hBloques);
      nBlock = 0;

    }//if
    
    LIBRT_FinalizarDispositivo(&hClauer);


  }//for
  


  
  //cout<<"CHK2 ---------------------------------------"<<endl;
  //cout<<"numClauerCerts: "<<numClauerCerts<<endl;

  cabeceras = (char **) malloc(numClauerCerts*sizeof(char *));
  
  //cout<<"CHK2.1 ---------------------------------------"<<endl;


  for (int i=0; i<numClauerCerts; i++) 
    {

      //cout<<"CHK2.2 ---------------------------------------i: "<<i<<" Tipo: "<<ClauerCertList[i]->kind<<endl;

      
      pemcert = (char *) ClauerCertList[i]->PEMCert;
        
      //cout<<"CHK2.3 --------------------------------------CERT en pem: "<<pemcert<<endl;

            
      pemlen = strlen(pemcert) ;

      //cout<<"CHK3 ---------------------------------------LEN: "<<pemlen<<endl;

      //cout<<"PEM LEN: "<<pemlen<<endl;

      //cout<<"PEM CERT: "<<pemcert<<endl;


    
      //Procesamos el cert en PEM
      
      buff = BIO_new(BIO_s_mem());  //Crea un BIO en memoria.
      if (!BIO_write(buff,pemcert,pemlen)){  //Escribimos el cert en el bio
        //cout<<"++++++++++Fallo al escribir el PEM cert en el bio"<<endl;
        //return 0;
        cabeceras[i]= (char *) malloc(1);
        cabeceras[i][0]='\0';
        continue;
      }

      if (!(certx509 = PEM_read_bio_X509(buff, NULL, 0, NULL))){  //Lee el cert escrito en el bio y lo mete en un objeto x509 
        //cout<<"++++++++++Fallo al crear el objeto x509 :1"<<endl;
        //cout<<pemcert<<endl;
        //cout<<"BLOQUE: "<<ClauerCertList[i]->numBlock<<endl;
        //cout<<ClauerCertList[i]->PEMCert<<endl;
        //return 0;
        cabeceras[i]= (char *) malloc(1);
        cabeceras[i][0]='\0';
        continue;
      }


      //El tipo es  "CA" "USER" o  "OTHER"
      msg = buildTreeLine(i, certx509, NULL, buff, ClauerCertList[i]->kind);
      
      //cout<<"CHK4 ---------------------------------------"<<endl;


      
      //Construir array de mensajes a retornar
      
        
      
      
      
      cabeceras[i] = (char *) malloc((strlen(msg)+1)*sizeof(char)); 
      
      cabeceras[i][0]='\0';
      
      strcpy(cabeceras[i], msg);

      //cout<<"CHK5 ---------------------------------------"<<endl;


      //cout <<"MSG clauer: "<<msg<<endl;
      
      
      free(msg);
      
      
      
      //cout<<"************************************************************************"<<endl;
      
	}

  //cout<<"CHK2.4 ---------------------------------------"<<endl;


  //Ver TODOS los buffers QUE hay que liberar
  
  *certs = cabeceras;
  *tam   = numClauerCerts;
  
  
  //cout<<"*******************Todos los Mensajes*******************"<<endl;
  //for(int i=0;i<numClauerCerts;i++){
  //  cout<<(*certs)[i]<<endl;
  // }
  //cout<<"********************************************************"<<endl;

  return NS_OK;
}

/* void GetFFXCertList (out unsigned long tam, [array, size_is (tam), retval] out string certs); */
NS_IMETHODIMP ClauerCrypto::GetFFXCertList(PRUint32 *tam, char ***certs)
{

  PK11SlotInfo *slot = 0;
  //CERTCertDBHandle *db = 0;
  
  //SECStatus s;
  
  CERTCertList *list;
  CERTCertListNode *node;
    
    
  CERTCertificate* cert;
  //char * name;
  unsigned char * dercert;
  int derlen;
    
  BIO *  buff;  
  // BIO *  tempbuff;
    

    
  X509      * certx509;
 

 
  char * msg;
  char ** cabeceras;

  //CK_OBJECT_HANDLE * FookeyPtr = new CK_OBJECT_HANDLE ;


    

  
  if(FFXCertList){//Reescribimos la lista completa
    for(int i=0; i<numFFXCerts; i++){
      free(FFXCertList[i]->NSSCert);
      free(FFXCertList[i]);
    }

    free(FFXCertList);
    FFXCertList = NULL; 
  }


  numFFXCerts = 0;



  FFXCertList = (certLine **) malloc(MAX_CERTS*sizeof(certLine *));


  cabeceras = (char **) malloc(MAX_CERTS*sizeof(char *));
  


  slot = PK11_GetInternalKeySlot();

  //cout<<"Check0"<<endl;
  
  list = PK11_ListCertsInSlot(slot);//devuelve los de la generalitat (o sea, los builtin no)

   //PK11SlotInfo *PK11_KeyForDERCertExists(SECItem *derCert, CK_OBJECT_HANDLE *keyPtr, void *wincx); para ver si es personal


  //cout<<"Check1"<<endl;
 
  for (node = CERT_LIST_HEAD(list); !CERT_LIST_END(node, list); node = CERT_LIST_NEXT(node)) 
    {
            
      cert = node->cert;
            
      derlen = cert->derCert.len ;

      FFXCertList[numFFXCerts] = (certLine *) malloc(sizeof(certLine)); 

      FFXCertList[numFFXCerts]->NSSCert = (CERTCertificate *) malloc(sizeof(CERTCertificate));
            
      memcpy((void *) FFXCertList[numFFXCerts]->NSSCert, (void *)cert, sizeof(CERTCertificate));
      FFXCertList[numFFXCerts]->DERCert = FFXCertList[numFFXCerts]->NSSCert->derCert.data;
            
      dercert = FFXCertList[numFFXCerts]->DERCert; //alias


      //Procesamos el cert en DER
      
      buff = BIO_new(BIO_s_mem());  //Crea un BIO en memoria.
      if (!BIO_write(buff,dercert,derlen)){  //Escribimos el cert en el bio
        //cout<<"++++++++++Fallo al escribir el cert en el bio"<<endl;
        return 0;
      }
      //d2i_X509_bio(BIO *bp,X509 **x509);
      //tempbuff =  buff;
      if (!(certx509 = d2i_X509_bio(buff,NULL))){  //Lee el cert escrito en el bio y lo mete en un objeto x509 
        //cout<<"++++++++++Fallo al crear el objeto x509 :2"<<endl;
        return 0;
      }



      msg = buildTreeLine(numFFXCerts, certx509, cert, buff, NULL);
      
      
      //Construir array de mensajes a retornar
      
      
      
      
      
      cabeceras[numFFXCerts] = (char *) malloc((strlen(msg)+1)*sizeof(char)); 
      
      cabeceras[numFFXCerts][0]='\0';
      
      strcpy(cabeceras[numFFXCerts], msg);


     // cout<<"MENSAJE CERTS FFX: "<<msg<<endl;
     // cout<<flush;

      
      free(msg);
      









      //cout<<"************************************************************************"<<endl;
    

      numFFXCerts++;

      if((numFFXCerts%MAX_CERTS)==0){ //Ojo, la primera vuelta ya debe ser >0 (por eso incr. antes), o sino fallar�
        //cout<<"Doubling size of the FFX cert array"<<endl;
        FFXCertList = (certLine **) realloc(FFXCertList, (numFFXCerts+MAX_CERTS)*sizeof(certLine *));
        cabeceras = (char **) realloc(cabeceras, (numFFXCerts+MAX_CERTS)*sizeof(char *));

      }
        
       
	}

 



  //Ver TODOS los buffers QUE hay que liberar
  
  *certs = cabeceras;
  *tam   = numFFXCerts;
  
  
//  cout<<"*******************Todos los Mensajes*******************"<<endl;
//  for(int i=0;i<numFFXCerts;i++){
//    cout<<(*certs)[i]<<endl;
//  }
//  cout<<"********************************************************"<<endl;

  return NS_OK;
}





//Faltan los de abajo


/* long PutFFXCertIntoClauer (in long idx); */
NS_IMETHODIMP ClauerCrypto::PutFFXCertIntoClauer(PRInt32 idx, PRInt32 *_retval)
{


  USBCERTS_HANDLE hClauer;


  PK11SlotInfo *slot = 0;
    
    SECKEYPrivateKey * privkey = 0;
    SECKEYEncryptedPrivateKeyInfo * encprivkey = 0;
    
    SECStatus s;
    
    CERTCertList *list;
    CERTCertListNode *node;
    
    
    SECItem exportpw;    // password for PBE encryption of the extracted key (generate with true randomness)
    
    CERTCertificate *nssCert = NULL;
    CERTCertificate* cert;
    
       char * dercert, * hexdercert;
    int derlen;
    
    BIO *  buff;  
    BIO *  tempbuff;
    
    char * subjstr;
    char * cn;
    
    X509      * certx509;
    X509_NAME * subject;

    FILE * testfile;
    
    PRUnichar * passbuff;
    int passbufflen;

    SECStatus srv = SECSuccess;
    SEC_PKCS12ExportContext *ecx = NULL;
    SEC_PKCS12SafeInfo *certSafe = NULL, *keySafe = NULL;
    
    SGNDigestInfo *digest = NULL;


    char * importpw;

    writeP12params pars;


    unsigned char block[TAM_BLOQUE];
    long nb;
    unsigned char *b, *auxb;
    int pemsize = 0;


    //Ver si el idx est� fuera de rango
      
    //ver si tengo lista de certs ffx y lista de certs clauer, sino abortar
    
    //ver si tengo sesion autenticada en ambos, sino, abortar


    if(!m_pwdFFX){
      //cout<<"++++++++++FFX no autenticado"<<endl;
      *_retval = 0;
      return NS_OK;
    }
      


    cert = FFXCertList[idx]->NSSCert;

      
    buff = BIO_new(BIO_s_mem());  //Crea un BIO en memoria.
    if (!BIO_write(buff,cert->derCert.data,cert->derCert.len)){  //Escribimos el cert en el bio
      //cout<<"++++++++++Fallo al escribir el cert en el bio"<<endl;
      *_retval = 0;
      return NS_OK;
    }
    if (!(certx509 = d2i_X509_bio(buff,NULL))){  //Lee el cert escrito en el bio y lo mete en un objeto x509 
      //cout<<"++++++++++Fallo al crear el objeto x509 :3"<<endl;
      *_retval = 0;
      return NS_OK;
    }
    

    if(isFFXUserCert(cert)&& !OSSL_X509_is_CA(certx509)){ //Si el cert es personal, lo exportamos en p12. sino, a  pelo (en der o pem).
        
      //tempfileprueba = fopen("/home/paco/Desktop/P12.p12","w");

      
     
      
      slot = PK11_GetInternalKeySlot(); //devuelve los de la generalitat (o sea, los builtin no)
      if (!slot) { 
        //cout<<"Error obteniendo el Slot del PKCS11 interno de Firefox."<<endl;	
      }
      
      PK11_SetPasswordFunc(myPasswdCB);
      
    
      
      //cout<<"CERT seleccionado: "<<cert->subjectName<<endl;
      
      //hola es un password cutre para exportar el pkcs12 de ffx a un buffer de memoria e importarlo en el clauer, pq no deja exportar en claro
      passbufflen = NS_ConvertASCIItoUTF16("hola",4).Length();
      passbuff = (PRUnichar *) malloc((passbufflen + 1)*sizeof(PRUnichar));
      memcpy(passbuff, NS_ConvertASCIItoUTF16("hola",4).get(),(passbufflen+1)*sizeof(PRUnichar));
      
      
      //cout<<"long del pass en unichar: "<<passbufflen<<endl;
    
    
      exportpw.len  =passbufflen*sizeof(PRUnichar);
      exportpw.data = (unsigned char *) malloc((exportpw.len+sizeof(PRUnichar))*sizeof(unsigned char));
      //cout<<"long del pass en char: "<<exportpw.len<<endl;
      //cout<<"Sizeof PRUnichar: "<<sizeof(PRUnichar)<<endl;


      // memcpy( (char *) exportpw.data, (const PRUnichar *) NS_ConvertASCIItoUTF16("�",1).get(), NS_ConvertASCIItoUTF16("�",1).Length()*sizeof(PRUnichar));

      for (int i=0; i<passbufflen; i++) {
        //cout<<"it: "<<i<<endl;
        exportpw.data[2*i  ] = (unsigned char )(passbuff[i] << 8);
        exportpw.data[2*i+1] = (unsigned char )(passbuff[i]);
      }


    
    
      // **********************PKCS12**********************************************************
    
    
    
    
      //Hacer pruebas con esto. te�ricamente, todo NULL sacar� el prompt del pw de la db (si no redefino yo el pwcallback del pk11,claro), probar a quitar uno de los dos a ver cu�l es el bueno.
    
      //ecx = SEC_PKCS12CreateExportContext( (SECKEYGetPasswordKey) myP12PasswdCB, (void *) "a", slot, (void *) "a");
      ecx = SEC_PKCS12CreateExportContext( NULL, NULL, slot, (void *) m_pwdFFX);
    
      if (!ecx){
        //cout<<"Fallo al crear el contexto"<<endl;
        *_retval = 0;
        return NS_OK;
      }
    
      //cout<<"+++++++++++++++contexto creado"<<endl;
    
      if(SEC_PKCS12AddPasswordIntegrity(ecx, &exportpw, SEC_OID_SHA1)){
        //cout<<"Fallo en add pass integrity"<<endl;
       *_retval = 0;
        return NS_OK;
      }
    
      //cout<<"+++++++++++++++hay pass integrity"<<endl;
    
    
      nssCert = cert;
      
      if (!nssCert) {
        //cout<<"Fallo al leer los certificados del array interno. los has cargado?"<<endl;
       *_retval = 0;
        return NS_OK;
      }
      
    
      //cout<<"+++++++++++++++llega a valer algo nssCert?"<<endl;
      
      
      keySafe = SEC_PKCS12CreateUnencryptedSafe(ecx);
      
      certSafe=keySafe;
    
      if (!certSafe || !keySafe) {
        //cout<<"fallo al crear el safe"<<endl;
        *_retval = 0;
        return NS_OK;
      }
    
      //cout<<"+++++++++++++++crea el safe"<<endl;
  
    

    

      if (SEC_PKCS12AddCertAndKey(ecx, certSafe, NULL, nssCert,
                                  CERT_GetDefaultCertDB(), 
                                  keySafe, NULL, PR_TRUE, &exportpw,
                                  SEC_OID_PKCS12_V2_PBE_WITH_SHA1_AND_3KEY_TRIPLE_DES_CBC)){
        //cout<<"fallo al escribir el cert o la llave en el pkcs12"<<endl;
        *_retval = 0;
        return NS_OK;
      }

      /*
      // add the certificate  -> esta funci�n no est� exportada en la librer�a y por eso peta la dependencia en tde ejec.
      if(SEC_PKCS12AddCert(ecx, certSafe, NULL, nssCert, CERT_GetDefaultCertDB(), NULL, PR_FALSE)){
      cout<<"Fallo al escribir el cert en el p12"<<endl;
      *_retval = 0;
      return NS_OK;
      }
      */

      
      //cout<<"+++++++++++++++a�ade key and cert pw: "<<exportpw.data<<endl;
      
     
     
/*     
      if(SEC_PKCS12Encode(ecx, 
                          write_export_stream, 
                          (void *) tempfileprueba)){
        cout<<"fallo al codificar el pkcs12"<<endl;
        *_retval = 0;
        return NS_OK;
       
      }
*/


/*
typedef struct {
     char * p12;
     int bufflen;
     int actpos;
} writeP12params;
*/

      pars.bufflen          = 1;
      pars.p12              = (char *) malloc(pars.bufflen*sizeof(char));
      pars.actpos           = 0;
      pars.p12[pars.actpos] = '\0';
      
      
      //pars.fich = tempfileprueba;
      

      if(SEC_PKCS12Encode(ecx, 
                          write_export_stream, 
                          (void *) &pars)){
        //cout<<"fallo al codificar el pkcs12"<<endl;
        *_retval = 0;
        return NS_OK;
      }
      
      
      
      //cout<<"+++++++++++++++codifica"<<endl;
      
      
      
      
      if (ecx)
        SEC_PKCS12DestroyExportContext(ecx);
      
      //fclose(tempfileprueba);
      
      
      PK11_FreeSlot(slot);
      
      

      
      
      if ( LIBRT_IniciarDispositivo((unsigned char *) m_deviceName, m_pwdClauer, &hClauer) != 0 ){
        //fprintf(stderr, "[ERROR] No se pudo iniciar dispositivo (sesion no autenticada?).\n");
        *_retval = 0;
        return NS_OK;
      }
      
      
/*
      if(!LIBIMPORT_ImportarPKCS12 ("/home/paco/Desktop/P12.p12", "hola", &hClauer,  PRIVKEY_DEFAULT_CIPHER, NULL, 0)){
        cout<<"fallo al importar el pkcs12?"<<endl;
        *_retval = 0;
        return NS_ERROR_FAILURE;

      }
*/


/*
      cout<<"+++++++++++++Va a llamar a importarpk12"<<endl;
      printf("++++++++++++P12: %x pos: %d bufflen: %d\n",pars.p12,pars.actpos, pars.bufflen);
      cout<<"Pass: ";
      for(int k=0;k<exportpw.len;k++)
        printf("%x ",exportpw.data[k]);
      cout<<endl;
      printf("++++++++++++++++ampp12: %x, pos: %d, amppass: %x\n",&(pars.p12),(pars.actpos), &(exportpw.data) );
      printf("++++++++++++++++contampp12: %x, contamppass: %x\n",*(&(pars.p12)), *(&(exportpw.data)) );
*/


      importpw = (char *) malloc(5*sizeof(char));
      strcpy(importpw, "hola");

      if(!LIBIMPORT_ImportarPKCS12deBuffer ((BYTE *)(pars.p12),(pars.actpos), (const char *) (importpw), &hClauer,  PRIVKEY_DEFAULT_CIPHER, NULL, 0)){
        //cout<<"fallo al importar el pkcs12"<<endl;
        LIBRT_FinalizarDispositivo(&hClauer);
        *_retval = 0;
        return NS_OK;
        
      }

      free(importpw);
      //free(pars.p12);
      
    }
    else{ //Es cert de ca o de otros   
      //Importarlo en el clauer a lo bestia

         
      pemsize = PEM_write_bio_X509(buff,certx509);
      //cout<<"PEM size?"<<pemsize<<endl;
      if (!pemsize){  //Escribimos el cert x509 en el bio
        //cout<<"++++++++++Fallo al escribir el cert en el bio"<<endl;
        *_retval = 0;
        return NS_OK;
      }
      
      //Lo leemos en PEM
      b = (unsigned char * ) malloc ( (TAM_BLOQUE+1) *sizeof(unsigned char) );
      b[0] = '\0';
      auxb = (unsigned char * ) malloc ( (TAM_BLOQUE+1) *sizeof(unsigned char) );
      auxb[0] = '\0';
      
      
      while(BIO_gets(buff,(char *) auxb,TAM_BLOQUE)){
        strcat((char *)b, (const char *)auxb);
      }

      pemsize = strlen((const char *)b);

      if(!pemsize){
        //cout<<"++++++++++Fallo al leer el pem del bio"<<endl;
        *_retval = 0;
        return NS_OK;
      }
      
      
      //cout<<"Cert en PEM?:"<<b<<endl;
      
      
      if ( LIBRT_IniciarDispositivo((unsigned char *) m_deviceName, m_pwdClauer, &hClauer) != 0 ){
        //fprintf(stderr, "[ERROR] No se pudo iniciar dispositivo (sesion no autenticada?).\n");
        *_retval = 0;
        return NS_OK;
      }
      
      
      memset(block, 0, sizeof block);
      BLOQUE_Set_Claro(block);


      if(!OSSL_X509_is_CA(certx509)){
        //cout<<"-------------------------------Es cert de Other"<<endl;
        BLOQUE_CERTOTROS_Nuevo(block);
        BLOQUE_CERTOTROS_Set_Tam(block, pemsize);
        BLOQUE_CERTOTROS_Set_Objeto(block, b, pemsize);
      }
      else if(OSSL_X509_is_rootCA(certx509)){
        //cout<<"-------------------------------Es cert de CA root"<<endl;
        BLOQUE_CERTRAIZ_Nuevo(block);
        BLOQUE_CERTRAIZ_Set_Tam(block, pemsize);
        BLOQUE_CERTRAIZ_Set_Objeto(block, b, pemsize);
      }
      else if(OSSL_X509_is_CA(certx509)){
        //cout<<"-------------------------------Es cert de CA intermedia"<<endl;
        BLOQUE_CERTINTERMEDIO_Nuevo(block);
        BLOQUE_CERTINTERMEDIO_Set_Tam(block, pemsize);
        BLOQUE_CERTINTERMEDIO_Set_Objeto(block, b, pemsize);
      }


      free(b);
      free(auxb);


      if ( LIBRT_InsertarBloqueCrypto(&hClauer, block, &nb) != 0 ) {
        //fprintf(stderr, "[ERROR] Inserting the certificate.\n");
        LIBRT_FinalizarDispositivo(&hClauer);
        *_retval = 0;
        return NS_OK;      
      }
     
      //todo OK
    }


 
    *_retval = 1;
    LIBRT_FinalizarDispositivo(&hClauer);
    return NS_OK;
}











/* long PutClauerCertIntoFFX (in long idx); */  //FF3 OK
NS_IMETHODIMP ClauerCrypto::PutClauerCertIntoFFX(PRInt32 idx, PRInt32 *_retval)
{
   
  USBCERTS_HANDLE hClauer;
  long bloque;
  unsigned long tamPkcs12;
  unsigned char *pkcs12 = NULL;
  char p12pwd[5] = "pass";

  SEC_PKCS12DecoderContext *dcx = NULL;
  SECItem * exportpw;
  PK11SlotInfo *slot= 0;
  PRUnichar * passbuff;
  int passbufflen;


  BIO * buff = NULL;
  X509 * certx509 = NULL;
  unsigned char * DERcert = NULL;
  int DERlen;
  SECItem secitemDERcert;
  char * friendlyName, * certserial;
  ASN1_INTEGER * certSerialNum;


 
  
//  FILE * QQQQQ;
  //  char * caspa;

  
  if(idx < 0 || idx >= numClauerCerts){
    cout<<"�ndice fuera de rango"<<endl;
    *_retval = 0;
    return NS_OK;
  }

  if(!m_pwdFFX){
    cout<<"++++++++++FFX no autenticado"<<endl;
    *_retval = 0;
    return NS_OK;
  }

  bloque = ClauerCertList[idx]->numBlock;
  
  
  if(!strcmp(ClauerCertList[idx]->kind,"USER")){ //Si es de usuario
  


    if ( LIBRT_IniciarDispositivo((unsigned char *) m_deviceName, m_pwdClauer, &hClauer) != 0 ){
      fprintf(stderr, "[ERROR] No se pudo iniciar dispositivo (sesion no autenticada?).\n");
      *_retval = 0;
      return NS_OK;
    }

  
    cout<<"bloque a exportar: "<<bloque<<endl;

    if(LIBIMPORT_ExportarPKCS12enBuffer(bloque, p12pwd, &hClauer, &pkcs12, &tamPkcs12)){
      cout<<"Error exportando el pkcs12"<<endl;
      LIBRT_FinalizarDispositivo(&hClauer);
      *_retval = 0;
      return NS_OK;
    }
  
  
    LIBRT_FinalizarDispositivo(&hClauer);


    //  QQQQQ = fopen("/home/paco/Desktop/DUMPED.p12","w");

    //  fwrite(pkcs12,tamPkcs12,1,QQQQQ);
  

    //  fclose(QQQQQ);



    //++++++Importamos el pkcs12 en el firefox+++++++++
  

    //sacar el slot
    slot = PK11_GetInternalKeySlot();

    PK11_SetPasswordFunc(myPasswdCB);
  
 
    if (PK11_Authenticate(slot, PR_TRUE, (void *) m_pwdFFX)){
      cout<<"Error al autenticarse en la bd."<<endl;
      *_retval = 0;
      return NS_OK;
    }


  
 
    //montar el passwd en una estructura de las suyas y pasarlo a unicode
  

  
    passbufflen = NS_ConvertASCIItoUTF16(p12pwd,strlen(p12pwd)).Length();
    passbuff = (PRUnichar *) malloc((passbufflen + 1)*sizeof(PRUnichar));

    memcpy(passbuff, NS_ConvertASCIItoUTF16(p12pwd,strlen(p12pwd)).get(),(passbufflen+1)*sizeof(PRUnichar)); //alg�n fall aqu� puede ser la causa de que no decodifique bien
      
    //cout<<"BUFFER UTF ("<<passbufflen<<"): ";
    //for (int i=0; i<=passbufflen; i++) {
    //  printf(" %04X ",passbuff[i]);
    //}
    //cout<<endl;
  
      
    //cout<<"long del pass en unichar: "<<passbufflen<<endl;
    //cout<<"pasa0."<<endl;
    
    //SECITEM_AllocItem(NULL, exportpw, sizeof(PRUnichar) * (passbufflen+1));

    //cout<<"pasa."<<endl;
  
    exportpw = (SECItem *) malloc(sizeof(SECItem));
    exportpw->len  = (passbufflen+1)*sizeof(PRUnichar);
    exportpw->data = (unsigned char *) malloc((exportpw->len)*sizeof(unsigned char));
  
    //cout<<"long del pass en char: "<<exportpw.len<<endl;
    //cout<<"Sizeof PRUnichar: "<<sizeof(PRUnichar)<<endl;
  
    //cout<<"Length: "<<exportpw->len<<endl;
  
    for(int i=0;i< sizeof(PRUnichar) * (passbufflen+1);i++){
      exportpw->data[i]=0;
    }

    //cout<<"BUFF UTF16: ";
    //for (int i=0; i<exportpw->len; i++) {
    //  printf(" %02X",exportpw->data[i]);
    //}
    //cout<<endl;

  
    for (int i=0; i<=passbufflen; i++) {
      //cout<<"it: "<<i<<endl;
      exportpw->data[2*i  ] = (unsigned char )(passbuff[i] << 8);
      exportpw->data[2*i+1] = (unsigned char )(passbuff[i]);
    }
  
    //cout<<"PASS EN UTF16: ";
    //for (int i=0; i<exportpw->len; i++) {
    //  printf(" %02X",exportpw->data[i]);
    //}
    //cout<<endl;

    /*  
        exportpw.data = (unsigned char *) malloc((strlen(p12pwd)+1)*sizeof(unsigned char));
        exportpw.len  = strlen(p12pwd)+1;
        strcpy((char *)exportpw.data, (char *)p12pwd);
        cout<<"Export Pass: "<<exportpw.data<<endl;
    */


    /****************************************************/





    /****************************************************/




    /*
      extern SEC_PKCS12DecoderContext *
      SEC_PKCS12DecoderStart(SECItem *pwitem, PK11SlotInfo *slot, void *wincx,
      digestOpenFn dOpen, digestCloseFn dClose,
      digestIOFn dRead, digestIOFn dWrite, void *dArg);
    */

    //caspa = (char *) malloc(20);
    //strcpy(caspa, "caspa");
    //dcx = SEC_PKCS12DecoderStart(exportpw, slot, caspa, NULL, NULL, NULL, NULL, NULL); //donde pone caspa es el wincx de la func de colisi�n


    dcx = SEC_PKCS12DecoderStart(exportpw, slot, NULL, NULL, NULL, NULL, NULL, NULL);
    if (!dcx) {    
      cout<<"Error creando el contexto del decoder pkcs12"<<endl;
      *_retval = 0;
      return NS_OK;
    }


    if(SEC_PKCS12DecoderUpdate(dcx, pkcs12, tamPkcs12)){
      cout<<"Error alimentando el decoder pkcs12"<<endl;
      *_retval = 0;
      return NS_OK;
    }
  
  
  
    if(SEC_PKCS12DecoderVerify(dcx)){
      printf("Error %x verificando la mac del pkcs12",PORT_GetError());
      *_retval = 0;
      return NS_OK;
    }
    
    
    if(SEC_PKCS12DecoderValidateBags(dcx, MyNicknameCollisionCB)){
      cout<<"Error validando las bolsas del decoder pkcs12"<<endl;
      *_retval = 0;
      return NS_OK;
    }
    
    if(SEC_PKCS12DecoderImportBags(dcx)){
      cout<<"Error importando las bolsas del decoder pkcs12"<<endl;
      *_retval = 0;
      return NS_OK;
    }
  
    if (slot)
      PK11_FreeSlot(slot);
 
    if (dcx)
      SEC_PKCS12DecoderFinish(dcx);

    if(pkcs12)
      free(pkcs12);
    
  }
  else{ //Es cert de Ca u OTHER
    //Importarlo en el ffx a lo bestia
    
    //IMPLEMENTAR
    
   // cout<<"-----> Importar cert ca u other en el ffx desde clauer No implementado"<<endl;
    

    
    //Comprobamos si estamos autenticados en el clauer (aunque no tengamos que acceder a �l pq el cert est� leido ya)
    //Si la pol�tica es 'no hace falta autenticaci�n para leer certs del clauer', comentar este bloque
    if ( LIBRT_IniciarDispositivo((unsigned char *) m_deviceName, m_pwdClauer, &hClauer) != 0 ){
      //fprintf(stderr, "[ERROR] No se pudo iniciar dispositivo (sesion no autenticada?).\n");
      *_retval = 0;
      return NS_OK;
    }

    LIBRT_FinalizarDispositivo(&hClauer);

    
    

    //El cert en PEM
    //cout<<ClauerCertList[idx]->PEMCert<<endl;
    
    //Pasamos el PEM a estructura interna X509
    buff = BIO_new(BIO_s_mem());  //Crea un BIO en memoria.
    if (!BIO_write(buff, ClauerCertList[idx]->PEMCert,strlen( (const char *)ClauerCertList[idx]->PEMCert))){  //Escribimos el cert en el bio
      //cout<<"++++++++++Fallo al escribir el cert PEM en el bio"<<endl;
      *_retval = 0;
      return NS_OK;
    }
    if (!(PEM_read_bio_X509(buff,&certx509,0,NULL))){  //Lee el cert escrito en el bio y lo mete en un objeto x509 
      //cout<<"++++++++++Fallo al crear el objeto x509 :3"<<endl;
      *_retval = 0;
      return NS_OK;
    }
    



    //extracci�n del friendlyName  

    //  ASN1_INTEGER *  X509_get_serialNumber(X509 *x);
    certSerialNum = X509_get_serialNumber(certx509);
    
    certserial =  new char[MAXSUBJ+1];      
    
    i2a_ASN1_INTEGER(buff, certSerialNum);
    if (!BIO_gets(buff,certserial,MAXSUBJ)){  
      //cout<<"++++++++++Fallo al leer el serial number del bio"<<endl;
      return 0;
    }      
    
   // cout<<"certserial: "<<certserial<<endl;

    friendlyName = certserial;
        
    



    //Pasamos el struct x509 a DER
    if(!(DERlen = i2d_X509(certx509,&DERcert))){
      //cout<<"++++++++++Fallo al pasar el cert a DER"<<endl;
      *_retval = 0;
      return NS_OK;
    }
  

    //sacar el slot
    slot = PK11_GetInternalKeySlot();
    
    PK11_SetPasswordFunc(myPasswdCB);
    
    
    if (PK11_Authenticate(slot, PR_TRUE, (void *) m_pwdFFX)){
      //cout<<"Error al autenticarse en la bd."<<endl;
      *_retval = 0;
      return NS_OK;
    }
    
    //construimos un objeto secitem con el cert en der
    secitemDERcert.data = DERcert;
    secitemDERcert.len = (unsigned int) DERlen;
       
    
    if(PK11_ImportDERCert(slot, &secitemDERcert, NULL, friendlyName, 1)){
      //cout<<"Error al importar el cert en la bd."<<endl;
      *_retval = 0;
      return NS_OK;
    }
    


    PK11_FreeSlot(slot);


    
    *_retval = 1;
    return NS_OK;
    
  }
  
  
  *_retval = 1;
  return NS_OK;
}



/* long PutClauerCertsIntoP12 (in string ruta, in string pwd, in long idx); */
NS_IMETHODIMP ClauerCrypto::PutClauerCertIntoP12(const char *ruta, const char *p12pwd, PRInt32 idx, PRInt32 *_retval)
{

  USBCERTS_HANDLE hClauer;
  long bloque;
  
  //cout<<"Ruta: "<<ruta<<" pass: "<<p12pwd<<" Idx: "<<idx<<endl;


  if(idx < 0 || idx >= numClauerCerts){
    //cout<<"�ndice fuera de rango"<<endl;
    *_retval = 0;
    return NS_OK;
  }

  if(!p12pwd){
    //cout<<"necesita proporcionar un pw para el pkcs12"<<endl;
    *_retval = 0;
    return NS_OK;
  }

  
  if(!ruta){
    //cout<<"necesita proporcionar una ruta para el pkcs12"<<endl;
    *_retval = 0;
    return NS_OK;
  }

  
  bloque = ClauerCertList[idx]->numBlock;
  

  if ( LIBRT_IniciarDispositivo((unsigned char *) m_deviceName, m_pwdClauer, &hClauer) != 0 ){
    //fprintf(stderr, "[ERROR] No se pudo iniciar dispositivo (sesion no autenticada?).\n");
    *_retval = 0;
    return NS_OK;
  }

  
  //cout<<"bloque a exportar: "<<bloque<<endl;
  //cout<<"pwd: "<<p12pwd<<endl;
  //cout<<"ruta: "<<ruta<<endl;

  if(LIBIMPORT_ExportarPKCS12(bloque, p12pwd, &hClauer, ruta)){
    //cout<<"Bloque: "<<bloque<<endl;
    //cout<<"pwd: "<<p12pwd<<endl;
    //cout<<"ruta: "<<ruta<<endl;
    //cout<<"Error exportando el pkcs12"<<endl;
    *_retval = 0;
    return NS_OK;
  }
  
  
  LIBRT_FinalizarDispositivo(&hClauer);

  *_retval = 1;
  return NS_OK;
}




/* long PutP12CertsIntoClauer (in string ruta, in string pwd, in long importCAs); */
NS_IMETHODIMP ClauerCrypto::PutP12CertIntoClauer(const char *ruta, const char *p12pwd, PRInt32 importCAs, PRInt32 *_retval)
{
  
  USBCERTS_HANDLE hClauer;
  


  // cout<<"Ruta: "<<ruta<<endl;
  //cout<<"PWD:  "<<p12pwd<<endl;
  
  if(!p12pwd){
    //cout<<"necesita proporcionar un pw para el pkcs12"<<endl;
    *_retval = 0;
    return NS_OK;
  }
  if(!ruta){
    //cout<<"necesita proporcionar una ruta para el pkcs12"<<endl;
    *_retval = 0;
    return NS_OK;
  }
  

  
  if ( LIBRT_IniciarDispositivo((unsigned char *) m_deviceName, m_pwdClauer, &hClauer) != 0 ){
    //fprintf(stderr, "[ERROR] No se pudo iniciar dispositivo (sesion no autenticada?).\n");
    *_retval = 0;
    return NS_OK;
  }
  
  
  
  if(!LIBIMPORT_ImportarPKCS12 (ruta, p12pwd, &hClauer,  PRIVKEY_DEFAULT_CIPHER, NULL)){
    //cout<<"fallo al importar el pkcs12"<<endl;
    LIBRT_FinalizarDispositivo(&hClauer);
    *_retval = 0;
    return NS_OK;
  }
  
  LIBRT_FinalizarDispositivo(&hClauer);

  
  *_retval = 1;
  return NS_OK;
}





/* long DeleteClauerCert (in long idx); */
NS_IMETHODIMP ClauerCrypto::DeleteClauerCert(PRInt32 idx, PRInt32 *_retval)
{
 
  USBCERTS_HANDLE hClauer;  
  int i, j, found= 0, rewrite=0;
  long * hBlock = NULL;
  unsigned char bloque[TAM_BLOQUE];
  unsigned char id[20], zero[20];
  unsigned long bNumbers;    
  unsigned char *blocks = NULL, type, *auxblocks;
  unsigned int lstContainerSize = NUM_KEY_CONTAINERS;
  INFO_KEY_CONTAINER lstContainer[NUM_KEY_CONTAINERS];

  int numBlock;


  //cout<<"Borrando cert "<<idx<<" del CLAUER"<<endl;
  
  
  if(idx < 0 || idx >= numClauerCerts){
    //cout<<"�ndice fuera de rango"<<endl;
    *_retval = 0;
    return NS_OK;
  }
  
  numBlock = ClauerCertList[idx]->numBlock;
  
  if ( LIBRT_IniciarDispositivo((unsigned char *) m_deviceName, m_pwdClauer, &hClauer) != 0 ){
    //fprintf(stderr, "[ERROR] No se pudo iniciar dispositivo (sesion no autenticada?).\n");
    *_retval = 0;
    return NS_OK;
  }

  memset(zero,0,20);
    
  if ( LIBRT_LeerBloqueCrypto( &hClauer, numBlock, bloque) != 0) {
	//fprintf(stderr, "[ERROR] Unable to read the specified block");
	LIBRT_FinalizarDispositivo(&hClauer);
	*_retval = 0;
    return NS_OK;
  }
	
  /*  Nos copiamos el identificador del certificado. */
  memcpy(id, BLOQUE_CERTPROPIO_Get_Id(bloque), 20);

  if ( LIBRT_BorrarBloqueCrypto ( &hClauer,numBlock) != 0 ) { 
	//fprintf(stderr, "[ERROR] Deleting block = %d ",numBlock);
	LIBRT_FinalizarDispositivo(&hClauer);
	*_retval = 0;
    return NS_OK;
  }
    
  /* Ahora, buscamos todos los bloques que contengan este 
   * identificador y los borramos, excepto los key containers
   * en los cuales s�lo debemos modificar el puntero a la llave
   */
  
  if ( LIBRT_LeerTodosBloquesOcupados ( &hClauer, NULL, NULL, &bNumbers ) != 0 ) {
	//fprintf(stderr, "[ERROR] Unable to enumerate objects\n");
	LIBRT_FinalizarDispositivo(&hClauer);
	*_retval = 0;
    return NS_OK;
  }

  blocks = (unsigned char *) malloc (bNumbers * TAM_BLOQUE);
  auxblocks = blocks;
    
  if ( ! blocks ) {
	LIBRT_FinalizarDispositivo(&hClauer);
	//fprintf(stderr,  "[ERROR] No memory available \n");
	*_retval = 0;
    return NS_OK;
  }
    
  hBlock = ( long * ) malloc ( sizeof(long) * bNumbers );
    
  if ( ! hBlock ) {
	LIBRT_FinalizarDispositivo(&hClauer);
	free(auxblocks); 
	//fprintf(stderr,  "[ERROR] No memory available \n");
	*_retval = 0;
    return NS_OK;
  }
    
  if ( LIBRT_LeerTodosBloquesOcupados ( &hClauer, hBlock, blocks, &bNumbers ) != 0 ) {
	//fprintf(stderr, "[ERROR] Unable to enumerate objects\n");
	LIBRT_FinalizarDispositivo(&hClauer);
	free(auxblocks); 
	free(hBlock);
	*_retval = 0;
    return NS_OK;
  }
  
  for ( i = 0 ; i < bNumbers ; i++ ) {
	type = *(blocks+1);
      
	if ( type ==  BLOQUE_KEY_CONTAINERS ){
      rewrite= 0;
      if ( BLOQUE_KeyContainer_Enumerar(blocks, lstContainer, &lstContainerSize) != 0 ) {
		//printf("[ERROR] Enumerating key Containers\n");
		break;
      }
      for ( j = 0 ; j < lstContainerSize ; j++ ) {
		if ( memcmp(lstContainer[j].idExchange, id, 20) == 0 ) {
          if ( memcmp(lstContainer[j].idSignature, id, 20) == 0 || memcmp(lstContainer[j].idSignature, zero, 20) == 0) {
			if ( BLOQUE_KeyContainer_Borrar ( blocks , lstContainer[j].nombreKeyContainer ) != 0 ){
              //fprintf(stderr, "[ERROR] Unable to delete the associated key entry on the key container\n");
              LIBRT_FinalizarDispositivo(&hClauer);
              free(auxblocks); 
              free(hBlock);
              *_retval = 0;
              return NS_OK;
			}
			else {
              //printf("Container %s successfully deleted\n",lstContainer[j].nombreKeyContainer );
              /* Es necesario reescribir el bloque en el clauer */
              rewrite= 1; 
			}
          }
          else{
			if (  BLOQUE_KeyContainer_Establecer_ID_Exchange ( blocks, lstContainer[j].nombreKeyContainer, zero ) != 0  ){
              //fprintf(stderr, "[ERROR] Unable to delete the associated key entry on the key container\n");
              LIBRT_FinalizarDispositivo(&hClauer);
              free(auxblocks); 
              free(hBlock);
              *_retval = 0;
              return NS_OK;
			}
			else{
              rewrite= 1; 
			}
          }
		} else if ( memcmp(lstContainer[j].idSignature, id, 20) == 0 ) {
          if ( memcmp(lstContainer[j].idExchange , id, 20) == 0 || memcmp(lstContainer[j].idExchange , zero, 20) == 0) {
			if ( BLOQUE_KeyContainer_Borrar ( blocks , lstContainer[j].nombreKeyContainer ) != 0 ){
              //fprintf(stderr, "[ERROR] Unable to delete the associated key entry on the key container\n");
              LIBRT_FinalizarDispositivo(&hClauer);
              free(auxblocks); 
              free(hBlock);
              *_retval = 0;
              return NS_OK;
			}
			else {
             // printf("Container %s successfully deleted\n",lstContainer[j].nombreKeyContainer );
              /* Es necesario reescribir el bloque en el clauer */
              rewrite= 1; 
			}
          }
          else{
			if (  BLOQUE_KeyContainer_Establecer_ID_Signature ( blocks, lstContainer[j].nombreKeyContainer, zero ) != 0 ){
              //fprintf(stderr, "[ERROR] Unable to delete the associated key entry on the key container\n");
              LIBRT_FinalizarDispositivo(&hClauer);
              free(auxblocks); 
              free(hBlock);
              *_retval = 0;
              return NS_OK;
			}
			else{
              rewrite= 1;	
			}
          }
		}
      }
      if ( rewrite == 1 ){
		if ( BLOQUE_KeyContainer_Enumerar(blocks, lstContainer, &lstContainerSize) != 0 ) {
          //printf("[ERROR] Enumerating key Containers\n");
          break;
		}
		if ( lstContainerSize == 0 ){
          if ( LIBRT_BorrarBloqueCrypto ( &hClauer, hBlock[i] ) != 0 ) { 
			//fprintf(stderr, "[ERROR] Deleting block = %ld\n",hBlock[i]);
            LIBRT_FinalizarDispositivo(&hClauer);
			free(auxblocks); 
			free(hBlock);
			*_retval = 0;
            return NS_OK;
          }
		}
		else{
          if ( LIBRT_EscribirBloqueCrypto ( &hClauer, hBlock[i], blocks ) != 0 ){
			//fprintf(stderr, "[ERROR] Writing changes to the Clauer\n");
            LIBRT_FinalizarDispositivo(&hClauer);
			free(auxblocks); 
			free(hBlock);
			*_retval = 0;
            return NS_OK;
          }
		}
      }
	}
	else {
      found= 0;
      switch ( type ) {
      case BLOQUE_LLAVE_PRIVADA:
		if ( memcmp(id, BLOQUE_LLAVEPRIVADA_Get_Id(blocks), 20 ) == 0 ){
          found= 1;
		}
		break;

      case BLOQUE_CIPHER_PRIVKEY_PEM:
		if ( memcmp(id, BLOQUE_CIPHER_PRIVKEY_PEM_Get_Id(blocks), 20 ) == 0 ){
          found= 1;
		}
		break;
		
      case BLOQUE_PRIVKEY_BLOB:
		if ( memcmp(id, BLOQUE_PRIVKEYBLOB_Get_Id(blocks), 20 ) == 0 ){
          found= 1;
		}
		break;
	    
      case BLOQUE_CIPHER_PRIVKEY_BLOB:
		if ( memcmp(id, BLOQUE_PRIVKEYBLOB_Get_Id(blocks), 20 ) == 0 ){
          found= 1;
		}
		break;
      }
      if ( found ){
		if ( LIBRT_BorrarBloqueCrypto ( &hClauer, hBlock[i] ) != 0 ) { 
          //fprintf(stderr, "[ERROR] Deleting block = %ld\n",hBlock[i]);
          LIBRT_FinalizarDispositivo(&hClauer);
          free(auxblocks); 
          free(hBlock);
          *_retval = 0;
          return NS_OK;
		}
      }
	}
	blocks += TAM_BLOQUE;
  }
  
  
  free(auxblocks);
  free(hBlock);
  LIBRT_FinalizarDispositivo(&hClauer);
  
  *_retval = 1;
  return NS_OK;
}  //Ok en ff3



/* long DeleteClauerCert (in long idx); */
NS_IMETHODIMP ClauerCrypto::DeleteFFXCert(PRInt32 idx, PRInt32 *_retval)
{
  
  PK11SlotInfo *slot = 0;
  SECKEYPrivateKey * privkey = 0;
  
  //cout<<"Borrando cert "<<idx<<" del FFX"<<endl;

  if(idx < 0 || idx >= numFFXCerts){
    //cout<<"�ndice fuera de rango"<<endl;
    *_retval = 0;
    return NS_OK;
  }

  if(!m_pwdFFX){
    //cout<<"++++++++++FFX no autenticado"<<endl;
    *_retval = 0;
    return NS_OK;
  }
  

  slot = PK11_GetInternalKeySlot();
  
  PK11_SetPasswordFunc(myPasswdCB);
  
  
  if (PK11_Authenticate(slot, PR_TRUE, (void *) m_pwdFFX)){
    //cout<<"Error al autenticarse en la bd."<<endl;
    *_retval = 0;
    return NS_OK;
  }
  

  //SECKEYPrivateKey *PK11_FindKeyByDERCert(PK11SlotInfo *slot, CERTCertificate *cert, void *wincx);
  if((privkey = PK11_FindKeyByDERCert(slot, FFXCertList[idx]->NSSCert, NULL))==0){
    
    
    if(SEC_DeletePermCertificate(FFXCertList[idx]->NSSCert)){
      //cout<<"Error borrando el cert."<<endl;
      *_retval = 0;
      return NS_OK;

    }

  }
  else{//Es un cert propio
  
    //SECStatus PK11_DeleteTokenCertAndKey(CERTCertificate *cert,void *wincx);
    if(PK11_DeleteTokenCertAndKey(FFXCertList[idx]->NSSCert,NULL)){
      //cout<<"Error borrando el cert y la llave."<<endl;
      *_retval = 0;
      return NS_OK;
    }
    
  }


  *_retval = 1;
  return NS_OK;
}





/* long IsClauerCryptoMode (); */
NS_IMETHODIMP ClauerCrypto::IsClauerCryptoMode(PRInt32 *_retval)
{
  
  if(!m_pwdClauer){
    //cout<<"++++++++++clauer no autenticado"<<endl;
    *_retval = 0;
    return NS_OK;
  }

  //cout<<"++++++++++clauer ya autenticado"<<endl;
  *_retval = 1;
  return NS_OK;
}

/* long IsFFXCryptoMode (); */
NS_IMETHODIMP ClauerCrypto::IsFFXCryptoMode(PRInt32 *_retval)
{
  
  if(!m_pwdFFX){
    //cout<<"++++++++++FFX no autenticado"<<endl;
    *_retval = 0;
    return NS_OK;
  }
  
  //cout<<"++++++++++FFX ya autenticado"<<endl;
  *_retval = 1;
  return NS_OK;
  
}

/* long SetFFXCryptoMode (in string ffxpwd); */
NS_IMETHODIMP ClauerCrypto::SetFFXCryptoMode(const char *ffxpwd, PRInt32 *_retval)
{
   
  int ret, val;
  char *aux;
  
  PK11SlotInfo *slot= 0;
  
  *_retval = 0;
  
  //cout<<"Password introducido ("<<pwd<<"): ";
  
  //for (int zz=0; zz<strlen(pwd);zz++)
  //	cout<<std::hex<<((short)pwd[zz]);
  // cout<<endl;
  
  
  if (!ffxpwd) {
    //cout<<"++++++++++Dame un pwd"<<endl;
    *_retval = 0;
    return NS_OK;
  }
  
  aux = allocateSecureString(CLUI_MAX_PASS_LEN+1);
  if (!aux){
    return NS_OK;
  }
  strncpy(aux, ffxpwd, CLUI_MAX_PASS_LEN);
  aux[CLUI_MAX_PASS_LEN] = '\0'; //Si el pass desbordara la long max, no se escribiria el \0
  
  m_pwdFFX=aux;
  
  
  //autenticar en el ffx
  slot = PK11_GetInternalKeySlot();
  
  PK11_SetPasswordFunc(myPasswdCB);
  
  if(!m_pwdFFX){
    //cout<<"++++++++++FFX no autenticado"<<endl;
    *_retval = 0;
    return NS_OK;
  }
  
  
  if (PK11_Authenticate(slot, PR_TRUE, (void *) m_pwdFFX)){
    //cout<<"Error al autenticarse en la bd."<<endl;
    if (m_pwdFFX){
      freeSecureString(m_pwdFFX, CLUI_MAX_PASS_LEN+1);
      m_pwdFFX=NULL;
    }
    *_retval = 0;
    return NS_OK;
  }
  
  *_retval= 1;
  return NS_OK;
}


/* long SetFFXClearMode (); */
NS_IMETHODIMP ClauerCrypto::SetFFXClearMode(PRInt32 *_retval)
{
  PK11SlotInfo *slot= 0;
  
  slot = PK11_GetInternalKeySlot();
  
  PK11_SetPasswordFunc(myPasswdCB);
  
  if(PK11_Logout(slot)){
    //cout<<"++++++++++FFX logout err or not logged in"<<endl;
    *_retval = 0;
    return NS_OK;
  }
  
  if (m_pwdFFX) {
    if (freeSecureString(m_pwdFFX, CLUI_MAX_PASS_LEN) !=0)
      return NS_OK;
    m_pwdFFX = NULL;
  }

  //cout<<"++++++++++FFX logout ok"<<endl;
  *_retval = 1;
  return NS_OK;
  
}








/****Clauer PKCS11  functions ****/


/* long   IsPKCS11Installed(in string name); */
NS_IMETHODIMP ClauerCrypto::IsPKCS11Installed(const char *name, PRInt32 *_retval)
{
  
  SECMODModule *module = NULL; 
  
  //cout<<"Llama a  IsPk11: "<<name<<endl;
  //cout<<flush;

  module = SECMOD_FindModule(name);
  
  
  if (module) {

    *_retval = 1;

    //cout<<"Existe el modulo "<<name<<endl;
  }
  else{
    
    *_retval = 0;

    //cout<<"NO Existe el modulo "<<name<<endl;
  }
  
  return NS_OK;
  
}



/* long   InstallPKCS11(in string name, in string path); */
NS_IMETHODIMP ClauerCrypto::InstallPKCS11(const char *name, const char *path, PRInt32 *_retval)
{

  SECStatus stat;
  unsigned long PKCS11_PUB_READABLE_CERT_FLAG  =  0x1<<28; //Stored certs can be read off the token w/o logging in

  //cout<<"Llama a install Pk11: "<<name<<path<<endl;
  //cout<<flush;
  
  //pkcs11.addmodule("Modulo pkcs11 Clauer", "/usr/lib/libclauerpkcs11.so", PKCS11_PUB_READABLE_CERT_FLAG, 0);
  
  
  stat = SECMOD_AddNewModule(name, path, PKCS11_PUB_READABLE_CERT_FLAG, 0);
  

  //cout << "Stat: " <<stat<<endl;

  *_retval = stat;
  return NS_OK;

}



/* long   UninstallPKCS11(in string name); */
NS_IMETHODIMP ClauerCrypto::UninstallPKCS11(const char *name, PRInt32 *_retval)
{

  int tipo;  
  SECStatus stat;
  
  
  //cout<<"Llama a uninstall Pk11: "<<name<<endl;
  //cout<<flush;
  
  
  stat = SECMOD_DeleteModule(name, &tipo);
  
  
  //cout << "Stat: " <<stat<<endl;
  //cout << "Type: " <<tipo<<endl; 

//SECMOD_EXTERNAL 0       /* external module */
//SECMOD_INTERNAL 1       /* internal default module */
//SECMOD_FIPS     2       /* internal fips module */

  
  *_retval = stat;
  return NS_OK;

}















/************************** NSS Support Functions ******************************/
 /*
static SECStatus
printCertCB(CERTCertificate *cert, void *arg)
{
   int rv;
    SECItem data;
    CERTCertTrust *trust = (CERTCertTrust *)arg;
    
    data.data = cert->derCert.data;
    data.len = cert->derCert.len;

    rv = SECU_PrintSignedData(stdout, &data, "Certificate", 0, SECU_PrintCertificate);
    if (rv) {
	cerr<<"problem printing certificate"<<endl;
	return(SECFailure);
    }
    if (trust) {
	SECU_PrintTrustFlags(stdout, trust,
	                     "Certificate Trust Flags", 1);
    } else if (cert->trust) {
	SECU_PrintTrustFlags(stdout, cert->trust,
	                     "Certificate Trust Flags", 1);
    }

    printf("\n");

    return(SECSuccess);
}
*/
/*
static SECStatus
listCerts(CERTCertDBHandle *handle, char *name, PK11SlotInfo *slot,
          int raw, int ascii, void *pwarg)
{
    SECItem data;
    PRInt32 numBytes;
    SECStatus rv = SECFailure;
    CERTCertList *certs;
    CERTCertListNode *node;

    // List certs on a non-internal slot.
    if (!PK11_IsFriendly(slot) && PK11_NeedLogin(slot))
	    PK11_Authenticate(slot, PR_TRUE, pwarg);
    if (name) {
	CERTCertificate *the_cert;
	the_cert = CERT_FindCertByNicknameOrEmailAddr(handle, name);
	if (!the_cert) {
	    the_cert = PK11_FindCertFromNickname(name, NULL);
	    if (!the_cert) {
		cout<<"Could not find: "<<name<<endl;
		return SECFailure;
	    }
	}
	certs = CERT_CreateSubjectCertList(NULL, handle, &the_cert->derSubject,
		PR_Now(), PR_FALSE);
	CERT_DestroyCertificate(the_cert);

	for (node = CERT_LIST_HEAD(certs); !CERT_LIST_END(node,certs);
						node = CERT_LIST_NEXT(node)) {
	    the_cert = node->cert;
	    // now get the subjectList that matches this cert
	    data.data = the_cert->derCert.data;
	    data.len = the_cert->derCert.len;
	    if (ascii) {
		cout<<NS_CERT_HEADER<<endl<<BTOA_DataToAscii(data.data, data.len)<<endl<<NS_CERT_TRAILER<<endl;
		rv = SECSuccess;
	    } else if (raw) {
		numBytes = fwrite(data.data, sizeof(char), data.len, stdout);
		if (numBytes != (PRInt32) data.len) {
		   cerr<<"error writing raw cert"<<endl;
		    rv = SECFailure;
		}
		rv = SECSuccess;
	    } else {
		rv = printCertCB(the_cert, the_cert->trust);
	    }
	    if (rv != SECSuccess) {
		break;
	    }
	}
    } else {

	certs = PK11_ListCertsInSlot(slot);
	if (certs) {
	    for (node = CERT_LIST_HEAD(certs); !CERT_LIST_END(node,certs);
						node = CERT_LIST_NEXT(node)) {
		SECU_PrintCertNickname(node,stdout);
	    }
	    rv = SECSuccess;
	}
    }
    if (certs) {
        CERT_DestroyCertList(certs);
    }
    if (rv) {
	cout<<"problem printing certificate nicknames"<<endl;
	return SECFailure;
    }

    return SECSuccess;
}
*/

SECItem * MyNicknameCollisionCB(SECItem *old_nick, PRBool *cancel, void *wincx)
{
    
  char           *nick     = NULL;
  SECItem        *ret_nick = NULL;
  time_t  fecha;
  char randstr[9];
    
  //cout<<">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>> llama a NICKNAME COLLISSION"<<endl;
    

  if (!cancel) {
    //fprintf(stdout, "[ERROR] cancelled.\n");
    return NULL;
  }
  
  if (!old_nick)
    //fprintf(stdout, "[AVISO] no nickname for cert in PKCS12 file.\n");
  
  
  //cout<<"old_nick: "<<old_nick->data<<endl;


  
  nick = (char *) malloc((old_nick->len+9+1)*sizeof(char)); 
  if (!nick) {
    //fprintf(stdout, "[ERROR] out of memory.\n");
    return NULL;
  }
  
  //El nuevo nick ser� el viejo con una cadena aleatoria pegada
  
  //cout<<"hora: "<<time(&fecha)<<endl;

  srand(time(&fecha));
  

  //cout<<"MAX: "<<RAND_MAX<<endl;
 
  //cout<<"Rand: "<<std::hex<<rand()<<endl;

  sprintf(randstr, "%08X",rand());

  
  //cout<<"randstr: "<<randstr<<endl;

  sprintf(nick, "%s_%s",old_nick->data,randstr);

  


  //fprintf(stdout, "USANDO este nickname: %s\n", nick);
  ret_nick = (SECItem *) malloc(sizeof(SECItem));
  if(ret_nick == NULL) {
	free(nick);
    //fprintf(stdout, "[ERROR] escribiendo el retorno.\n");
	return NULL;
  }
  
  ret_nick->data = (unsigned char *)nick;
  ret_nick->len = strlen(nick);
  
  return ret_nick;

}


char * myPasswdCB(PK11SlotInfo *info, PRBool retry, void *arg)
{
        char * passwd = NULL;

        if ( (!retry) && arg ) {
            passwd = PORT_Strdup((char *)arg);
            //cout<<"+++++++++++++++++++++++++++++++CALLBACK PASS P11++++++++++++++++++++++++++++++++++"<<endl;;
        }

        return passwd;
}

/*
*  After use once, null it out otherwise PKCS11 calls us forever.
*/

/*SECItem * myP12PasswdCB (void *arg, void *handle)
{
    SECItem * pw = NULL;
    if (arg) {
        pw = SECITEM_AllocItem(NULL, NULL, PL_strlen((char *)arg));
        pw->data = (unsigned char *)PL_strdup((char *)arg);
        cout<<"+++++++++++++++++++++++++++++++CALLBACK PASS P12++++++++++++++++++++++++++++++++++"<<endl;;

    }
    return pw;
}
*/

//Implementar una que escriba en un buffer seguro (debe soportar streaming, ojo)
void PR_CALLBACK  write_export_stream(void *arg, const char *buf, unsigned long len)
{




  //cout<<"+++++++++++++++++++++++++++++++CALLBACK WRITE FILE++++++++++++++++++++++++++++++++++"<<endl;;


  writeP12params * params = (writeP12params *) arg;

  //FILE * dest = (FILE *) params->fich;
  //fwrite(buf,1, len,dest);
  
  
  
/*
typedef struct {
     char * p12;
     int bufflen;
     int actpos;
} writeP12params;
*/


 // printf("\nENTRADA++++++++++++P12: %d pos: %d bufflen: %d\n",params->p12,params->actpos, params->bufflen);
//  printf("++++++++++++len: %d\n", len);


  if(params->actpos+len >= params->bufflen){
    
   // printf("REALLOCA++++++++++++P12: %d pos: %d bufflen: %d len: %d\n",params->p12,params->actpos, params->bufflen, len);

    params->p12 = (char *) realloc(params->p12, (params->bufflen+len)*sizeof(char));
    params->bufflen += len;

   // printf("REALLOCA++++++++++++P12: %d pos: %d bufflen: %d len: %d\n",params->p12,params->actpos, params->bufflen, len);

  }
  
  if(!params->p12){
    //cout<<"P12 CALLBACK: Out of memory"<<endl;
    
  }
  else{ 

//    printf("VA A COPIAR++++++++++++P12: %d pos: %d bufflen: %d\n",params->p12,params->actpos, params->bufflen);


    memcpy(params->p12+(params->actpos*sizeof(char)), buf, len);
    params->actpos +=len;
    params->p12[params->actpos] = '\0';
  }
  
//  printf("SALE++++++++++++P12: %d pos: %d bufflen: %d\n\n",params->p12,params->actpos, params->bufflen);

  
}



int isFFXUserCert(CERTCertificate *cert)
{
  


  if ( cert->nickname && cert->trust &&
       ((cert->trust->sslFlags & CERTDB_USER ) ||
        (cert->trust->emailFlags & CERTDB_USER ) ||
        (cert->trust->objectSigningFlags & CERTDB_USER )) ) {
    return 1;
  }
  else {
    return 0;
  }




/*
  if (cert->trust &&cert->nickname && ((cert->trust->sslFlags & CERTDB_USER) ||
   (cert->trust->emailFlags, CERTDB_USER) ||
   (cert->trust->objectSigningFlags, CERTDB_USER)
  )){
    //cout<<"Es cert de USUARIO"<<endl;
    return 1;
  }
  
  //cout<<"NO Es cert de USUARIO"<<endl;
  return 0;

*/
}


/************************** Support Functions ******************************/


char * ClauerCrypto::buildTreeLine(int id, X509 * certx509, CERTCertificate *cert, BIO * buff, char * clauerKind)
{

  X509_NAME * subject, *issuer;
  ASN1_TIME * expirationDate;
  ASN1_INTEGER * certSerialNum;
  char *certname, *certkind, *certpurpose, *certissuer, *certserial, *certexpire, *certorg; //Subject->cn, issuer->cn

     
  char * subjstr, * issuerstr;
  char * cn;
  char borr[20];

  
  char * msg;
  int msglen;


  certname = certkind = certpurpose = certissuer = certserial = certexpire = certorg = 0;
    


      //------------Extraemos el CN y la O del Subject---------------  //DEvolver el subject completo y tratar en js
      
      //Extraemos el subject del x509
      if (!(subject = X509_get_subject_name(certx509))){
        //cout<<"++++++++++Fallo al LEER EL SUBJECT del x509"<<endl;
        return 0;
      }
      //Escribimos el subject en el bio
      X509_NAME_print_ex(buff, subject,0, XN_FLAG_SEP_SPLUS_SPC);      
      //Reservamos un buffer para el subject
      subjstr =  new char[MAXSUBJ+2];      
      if (!BIO_gets(buff,subjstr,MAXSUBJ)){  //Leemos el subject del bio
        //cout<<"++++++++++Fallo al leer el subject del bio"<<endl;
        return 0;
      }      
      subjstr[strlen(subjstr)+1] = '\0';
      subjstr[strlen(subjstr)] = ';';

      //cout<<"Subj completo: "<<subjstr<<endl;
      //cout<<"IDX: "<<id<<endl;

      /*
        for (int i=0; i<strlen(subjstr);i++){
          if(subjstr[i] == 'C')
            if(subjstr[i+1] == 'N')
              if(subjstr[i+2] == '=')
                cn = &subjstr[i+3];
          
        }
        for(int i=0; i<strlen(cn);i++){
          if(cn[i]==';'){
            cn[i]='\0';
            //certname = (char *) malloc((strlen(cn)+1)*sizeof(char));
            //strcpy(certname,cn);
            cn[i]=';';

            break;
          }	
        }
        
        cout<<"+*+*+*+*+*+*+* CN: "<<certname<<endl;
        

        for (int i=0; i<strlen(subjstr);i++){
          if(subjstr[i] == ' ')
            if(subjstr[i+1] == 'O')
              if(subjstr[i+2] == '=')
                cn = &subjstr[i+3];
          
        }
        for(int i=0; i<strlen(cn);i++){
          if(cn[i]==';'){
            cn[i]='\0';
            //certorg = (char *) malloc((strlen(cn)+1)*sizeof(char));
            //strcpy(certorg,cn);
            cn[i]=';';
            break;
          }	
        }
        */
      
      certname = subjstr;
      certorg  = subjstr;
      
        
                
       // cout<<"+*+*+*+*+*+*+* O: "<<certorg<<endl;
        

        //-----------Extracci�n del ISSUER-----------  //DEvolver el issuer completo y tratar en js


        //Extraemos el Issuer del x509
      if (!(issuer = X509_get_issuer_name(certx509))){
        //cout<<"++++++++++Fallo al LEER EL ISSUER del x509"<<endl;
        return 0;
      }
      //Escribimos el subject en el bio
      X509_NAME_print_ex(buff, issuer,0, XN_FLAG_SEP_SPLUS_SPC);      
      //Reservamos un buffer para el issuer
      issuerstr =  new char[MAXSUBJ+2]; 

      
      if (!BIO_gets(buff,issuerstr,MAXSUBJ)){  //Leemos el issuer del bio
        //cout<<"++++++++++Fallo al leer el issuer del bio"<<endl;
        return 0;
      }      

      issuerstr[strlen(issuerstr)+1] = '\0';
      issuerstr[strlen(issuerstr)] = ';';


      /*
        for (int i=0; i<strlen(issuerstr);i++){
          if(issuerstr[i] == 'C')
            if(issuerstr[i+1] == 'N')
              if(issuerstr[i+2] == '=')
                cn = &issuerstr[i+3];
          
        }
        for(int i=0; i<strlen(cn);i++){
          if(cn[i]==';'){
            cn[i]='\0';
            break;
          }	
        }
        
        certissuer = cn;
      */
      
      certissuer = issuerstr;
      
      //cout<<"+*+*+*+*+*+*+* CN Issuer: "<<certissuer<<endl;


        //int             X509_certificate_type(X509 *x,EVP_PKEY *pubkey /* optional */);?????????

        //-------------------SERIAL------------------------
        //  ASN1_INTEGER *  X509_get_serialNumber(X509 *x);
        certSerialNum = X509_get_serialNumber(certx509);

        certserial =  new char[MAXSUBJ+1];      
          
        i2a_ASN1_INTEGER(buff, certSerialNum);
        if (!BIO_gets(buff,certserial,MAXSUBJ)){  
          //cout<<"++++++++++Fallo al leer el serial number del bio"<<endl;
          return 0;
        }      
        
       // cout<<"+*+*+*+*+*+*+* CN Serial: "<<certserial<<endl;

      
        //-------------------Expiration------------------------
        // ASN1_TIME *  X509_get_notAfter(X509 *x)
        expirationDate = X509_get_notAfter(certx509);

        
        certexpire =  new char[MAXSUBJ+1];      
        
        ASN1_TIME_print(buff, expirationDate);
        if (!BIO_gets(buff,certexpire,MAXSUBJ)){  
          //cout<<"++++++++++Fallo al leer la expir. date del bio"<<endl;
          return 0;
        }      

        //en JS modificaremos la fecha pra que tenga un formato mejor
        
        
       // cout<<"+*+*+*+*+*+*+* CN expiration: "<<certexpire<<endl;
                
        
        
        //-------------------Purposes------------------------  (de momento vac�os)
    

        //cout<<"-----------------PROPOSITOS-----------------------"<<endl;

        certpurpose = OSSL_X509_get_purposes(certx509);
     
        //cout<<"-------------------------------------------------"<<endl;




        //----------------- Kind (de momento devuelvo cadena vac�a) -----------------------------
        //en extensiones: ver si CA=True
        //Si lo es comparar el cn del subject y del issuer. si ==, es root, sino, es intermedia
        //Si no es CA, ver si hay llave asociada a cert. si-> personal  no-> de terceros.

       /* if (PK11_KeyForDERCertExists(&(cert->derCert), FookeyPtr, (void *) "a")){  //<<Peta aqu�, desde que he a�adido la "a";
          cout<<"Peta aqu�?"<<endl;
          cout<<"Tiene llave: "<<FookeyPtr<<endl;
        }
        else{
          cout<<"Peta ac�?"<<endl;
                  
          cout<<"No tiene llave: "<<FookeyPtr<<endl;
        }
*/

        certkind = new char[6];

        if(cert){//Nos pasan un puntero a la estructura de cert de tipo FFX
          
          if (OSSL_X509_is_CA(certx509)) {
            //cout<<"***********************Es cert de CA"<<endl;
            strcpy(certkind, "CA");
          }
          else if(isFFXUserCert(cert) && !OSSL_X509_is_CA(certx509)){
            //cout<<"***********************Es cert de USUARIO"<<endl;
            strcpy(certkind, "USER");
          }
          else if(!isFFXUserCert(cert)){
            //cout<<"***********************Es cert de OTROS"<<endl;
            strcpy(certkind, "OTHER");
          }
          else{
            //cout<<"***********************Es ERROR"<<endl;
            strcpy(certkind, "ERR");
          }
          
        }else if(clauerKind){ //Is clauer
          
          if(!strcmp(clauerKind, "CA") || !strcmp(clauerKind, "USER") || !strcmp(clauerKind, "OTHER")){
            strcpy(certkind, clauerKind);
          }
          else{
            //cout<<"***********************Es ERROR"<<endl;
            strcpy(certkind, "ERR");
          }
          
        }
        else{
          //cout<<"Error: Para saber el tipo de firefox necesito el CERTCert y para saber el del clauer, necesito que lo pasen como cadena."<<endl;
        }
        
        //Serializamos la cadena a enviar por cada cert;
        
        msglen = 0;
        
        borr[0] = '\0';
        sprintf(borr,"%d",id);
        msglen += strlen(borr);



        if(certname)    msglen+=strlen(certname);
        if(certkind)    msglen+=strlen(certkind);
        if(certpurpose) msglen+=strlen(certpurpose);
        if(certissuer)  msglen+=strlen(certissuer);
        if(certserial)  msglen+=strlen(certserial);
        if(certexpire)  msglen+=strlen(certexpire);
        if(certorg)     msglen+=strlen(certorg);
        msglen += 1;                      // para el \0
        msglen += 7*strlen(SEPARADOR);    //7  separadores



        msg = (char *) malloc(msglen*sizeof(char));
        msg[0] = '\0';

        
        sprintf(msg,"%d",id); //El identificador
        
        strcat(msg,SEPARADOR);
        
        if(certname)strcat(msg,certname);
        
        strcat(msg,SEPARADOR);
        
        if(certkind)strcat(msg,certkind);
        
        strcat(msg,SEPARADOR);
        
        if(certpurpose)strcat(msg,certpurpose);
        
        strcat(msg,SEPARADOR);
        
        if(certissuer)strcat(msg,certissuer);
        
        strcat(msg,SEPARADOR);
        
        if(certserial) strcat(msg,certserial);
        
        strcat(msg,SEPARADOR);
        
        if(certexpire) strcat(msg,certexpire);
        
        strcat(msg,SEPARADOR);
        
        if(certorg) strcat(msg,certorg);

        


        //cout<<"Mensaje a enviar: "<<msg<<endl;



        delete certexpire;
        delete subjstr;
        delete issuerstr;
        delete certserial;
        delete certkind;

        return msg;


}



int ClauerCrypto::clauerOwner ( USBCERTS_HANDLE *hClauer, char ** owner )
{


  unsigned char block[TAM_BLOQUE];
  long nBlock;

  BIO *  buff;

  char * subjstr;
  char * cn;

  unsigned char * certPEM;
  unsigned long tamPEM;

  X509      * cert = NULL , * aaa = NULL;
  X509_NAME * subject;

  *owner = 0;

  if ( LIBRT_LeerTipoBloqueCrypto(hClauer, BLOQUE_CERT_PROPIO, 1, block, &nBlock) != 0 )
    return 0;

  if ( nBlock == -1 )
    return 0;
	
  //obtenemos el cert propio del clauer
  certPEM = BLOQUE_CERTPROPIO_Get_Objeto(block);
  tamPEM  = BLOQUE_CERTPROPIO_Get_Tam(block);


  buff = BIO_new(BIO_s_mem());  //Crea un BIO en memoria.
  
  if (!BIO_write(buff,certPEM,tamPEM)){  //Escribimos el cert en el bio
    //cout<<"++++++++++Fallo al escribir el cert en el bio"<<endl;
    return 0;
  }

  PEM_read_bio_X509(buff,&cert,0,NULL);

/*
  aaa = PEM_read_bio_X509(buff,&cert,0,NULL);


  cout<<"aaa: "<<aaa<<endl;
  cout<<flush;

  

  if (!aaa){  //Lee el cert escrito en el bio y lo mete en un objeto x509 
    cout<<"++++++++++Fallo al crear el objeto x509 :4"<<endl;
    return 0;
  }
*/

  //Extraemos el subject del x509
  if (!(subject = X509_get_subject_name(cert))){
    //cout<<"++++++++++Fallo al LEER EL SUBJECT del x509"<<endl;
    return 0;
  }

  //Escribimos el subject en el bio
  X509_NAME_print_ex(buff, subject,0, XN_FLAG_SEP_SPLUS_SPC);

  //Reservamos un buffer para el subject
  subjstr =  new char[MAXSUBJ+1];

  if (!BIO_gets(buff,subjstr,MAXSUBJ)){  //Leemos el subject del bio
    //cout<<"++++++++++Fallo al leer el subject del bio"<<endl;
    return 0;
  }

  for (int i=0; i<strlen(subjstr);i++){
    if(subjstr[i] == 'C')
      if(subjstr[i+1] == 'N')
	if(subjstr[i+2] == '=')
	  cn = &subjstr[i+3];

  }


  for(int i=0; i<strlen(cn);i++){
    if(cn[i]==';'){
      cn[i]='\0';
      break;
    }	
  }

  //Este filtra el DNI. Solo ACCV
  /*	for(int i=0; i<strlen(cn);i++){
    if(cn[i]==' ')
    if(cn[i+1]=='-')
    for(int j = i+1;j<strlen(cn);j++)
    if(cn[j]=='N')				
    if(cn[j+1]=='I')
    if(cn[j+2]=='F')
    if(cn[j+3]==':'){
    cn[i] = '\0';
    break;
    }

    if(cn[i]==';'){
    cn[i]='\0';
    break;
    }
    }
  */

  * owner = (char *) malloc((strlen(cn)+1)*sizeof(char));
  strcpy(*owner,cn);

  delete [] subjstr;

  BIO_free(buff);

  return 1;
}

int ClauerCrypto::Get_Local_Version ( char * version )
{

#ifdef WINDOWS
  HKEY hKey;
  DWORD tamVersion, tipo;

  //cout<<"USANDO CODIGO PARA WINDOWS"<<endl;

  if ( RegOpenKeyExA(HKEY_LOCAL_MACHINE, PRG_REG_KEY, 0, KEY_READ, &hKey) != ERROR_SUCCESS )
    return 0;

  tamVersion = VERSION_SIZE + 1;

  if ( RegQueryValueExA(hKey, PRG_REG_VERSION, NULL, &tipo, (BYTE *)version, &tamVersion) != ERROR_SUCCESS ) {
    RegCloseKey(hKey);        
    return 0;
  }

  RegCloseKey(hKey);

  if ( (tamVersion != (VERSION_SIZE + 1)) || tipo != REG_SZ )
    return 0;


  return 1;
#else
  return 0;
#endif

}

char * ClauerCrypto::bin2hex(unsigned char *bin, int l)
{

  unsigned char * ret;
  char digitos[] = "0123456789ABCDEF";

  ret = (unsigned char *) malloc ((l*2+1)*sizeof(char));

  for (unsigned char i = 0; i < l; i++) {
    ret[2*i]=  digitos[bin[i] >> 4];
    ret[2*i+1]=digitos[bin[i] & 0xf];
  }
  ret[2*l]= 0;

  return (char *) ret;
}

void ClauerCrypto::arc4(char *key, int keySize, unsigned char *msg, int msgSize)
{
  int i, j, l, x;
  char S[256], K[256];

  for (i=0; i<256; i++) {
    S[i] = i;
    K[i] = key[i % keySize];
  }
  for (i=0, j=0; i<256; i++) {
    j = (j + S[i] + K[i]) & 255;
    x = S[i]; S[i] = S[j]; S[j] = x;
  }
  for (i=0 , j=0, l=0; l<msgSize; l++) {
    i = (i + 1) & 255;
    j = (j + S[i]) & 255;
    x = S[i]; S[i] = S[j]; S[j] = x;
    msg[l] = msg[l] ^ S[(S[i] + S[j]) & 255];
  }
}


/***************  Graphic User Interface Wrapper  ****************/


void ClauerCrypto::alertDialog(const char * message)
{
  char title[] = "Clauer Alert";

  nsCOMPtr<nsIServiceManager> servMan;
  nsresult rv = NS_GetServiceManager(getter_AddRefs(servMan));

  nsCOMPtr<nsIWindowWatcher> wwatch;
  rv = servMan->GetServiceByContractID(NS_WINDOWWATCHER_CONTRACTID,
                                     NS_GET_IID(nsIWindowWatcher),
                                     getter_AddRefs(wwatch));
  nsCOMPtr<nsIPrompt> prompter;
  if (wwatch)
    wwatch->GetNewPrompter(0, getter_AddRefs(prompter));

  prompter->Alert((const PRUnichar *) NS_ConvertASCIItoUTF16(title,strlen(title)).get(), 
                  (const PRUnichar *) NS_ConvertASCIItoUTF16(message,strlen(message)).get());
}

int ClauerCrypto::confirmDialog(const char *message)
{
  char title[] = "Clauer Confirmation";
  int ret;

 nsCOMPtr<nsIServiceManager> servMan;
  nsresult rv = NS_GetServiceManager(getter_AddRefs(servMan));
  if (NS_FAILED(rv))
    return -1;

  nsCOMPtr<nsIWindowWatcher> wwatch;
  rv = servMan->GetServiceByContractID(NS_WINDOWWATCHER_CONTRACTID,
                                     NS_GET_IID(nsIWindowWatcher),
                                     getter_AddRefs(wwatch));

  nsCOMPtr<nsIPrompt> prompter;
  if (wwatch)
    wwatch->GetNewPrompter(0, getter_AddRefs(prompter));

  prompter->Confirm((const PRUnichar *) NS_ConvertASCIItoUTF16(title,strlen(title)).get(), 
		    (const PRUnichar *) NS_ConvertASCIItoUTF16(message,strlen(message)).get(), &ret);
  
  return ret;
}

int ClauerCrypto::passwordDialog(const char *message, char * pass, int maxlen)
{

  char title[] = "Clauer password solicitation";	
  int ret, chk=0;
  PRUnichar *  buff;  //unsigned short  *



  nsCOMPtr<nsIServiceManager> servMan;
  nsresult rv = NS_GetServiceManager(getter_AddRefs(servMan));
  if (NS_FAILED(rv))
    return -1;

  nsCOMPtr<nsIWindowWatcher> wwatch;
  rv = servMan->GetServiceByContractID(NS_WINDOWWATCHER_CONTRACTID,
                                     NS_GET_IID(nsIWindowWatcher),
                                     getter_AddRefs(wwatch));



  buff = NULL;


  //cout<<wwatch<<endl;

  

 // wwatch = do_GetService(NS_WINDOWWATCHER_CONTRACTID);

 // cout<<wwatch<<endl;

  //cout<<NS_WINDOWWATCHER_CONTRACTID<<endl;

  nsCOMPtr<nsIPrompt> prompter;

  if (wwatch)
    wwatch->GetNewPrompter(0, getter_AddRefs(prompter));

  prompter->PromptPassword((const PRUnichar *) NS_ConvertASCIItoUTF16(title,strlen(title)).get(), 
                           (const PRUnichar *) NS_ConvertASCIItoUTF16(message,strlen(message)).get(), 
			   &buff, //(PRUnichar **)
			   NULL,
			   &chk,
               &ret);
  
  if (ret !=0){  //si se ha pulsado ok (independientemente de si se ha escrito algo en el pass)
    pass[maxlen] = '\0';
    strncpy(pass, NS_LossyConvertUTF16toASCII((PRUnichar *)buff).get(), maxlen); //copiamos el pass devuelto por el ffx al buffer de salida (truncamos si desborda)
  
    for (int z=0;z<(NS_LossyConvertUTF16toASCII((PRUnichar *)buff).Length()+1);z++){   //Limpiamos el buffer por seguridad
      buff[z] = 0; 	
    }    
  }
  else{
    pass[0] = 0;
  }

  return ret;
}


/*******************  Secure Memory Management  ************************/


char * ClauerCrypto::allocateSecureString(const int size){

  char * aux;	

  aux = new char[size+1];  //reservamos el buffer
  if ( ! aux )
    return 0;
  for (int z=0;z<=size;z++)   //Limpiamos el buffer
    aux[z] = 'z';  //poner un /0 tras las pruebas
  aux[size] = '\0';		

  if ( mlock(aux, size+1)!=0){  //bloqueamos el buffer
    delete [] aux;	
    return 0;
  }

  return aux;
}



int    ClauerCrypto::freeSecureString(char * buffer, const int size){

  char * aux = buffer;

  for (int z=0;z<=size;z++)   //Limpiamos el buffer
    aux[z] = 'z';	//poner un /0 tras las pruebas		 
  aux[size] = '\0';		

  if ( munlock(aux, size+1)!=0)  //Lo desbloqueamos para el swap (ahora que no tiene info secreta)
    return 1;

  delete [] aux;

  buffer = NULL;
  return 0;

}


char * ClauerCrypto::getURL(void){

  nsString resul;
  nsresult rv;
  char * buffer;

  nsCOMPtr<nsIServiceManager> servMan;
  rv = NS_GetServiceManager(getter_AddRefs(servMan));
  if (NS_FAILED(rv)){
    //cout<<"Error: No se pudo acceder al service manager"<<endl;
    return 0;
  }
  
  nsCOMPtr<nsIWindowWatcher> wwatch;
  rv = servMan->GetServiceByContractID(NS_WINDOWWATCHER_CONTRACTID,
                                       NS_GET_IID(nsIWindowWatcher),
                                       getter_AddRefs(wwatch));

  nsCOMPtr<nsIWindowMediator> med;
  rv = servMan->GetServiceByContractID("@mozilla.org/appshell/window-mediator;1",
                                       NS_GET_IID(nsIWindowMediator),
                                       getter_AddRefs(med));
  
  
  // nsCOMPtr<nsIWindowMediator> med( do_GetService(, &rv ) );


  //cout<<"--------------------->Llama a getURL"<<endl;
  //cout<<flush;

  nsCOMPtr<nsIDOMWindowInternal> navWin;
  med->GetMostRecentWindow( NULL , getter_AddRefs( navWin ) );
  if ( !navWin ) {
    // There is not a window open
    //cout<<"+++++++ERROR: There is not an open window"<<endl;
    return 0;
  }
  // Get content window.
  nsCOMPtr<nsIDOMWindow> content;
  navWin->GetContent( getter_AddRefs( content ) );
  if ( !content ) {
    //cout<<"+++++++ERROR: No dom content window"<<endl;
    return 0;
  }


  // Convert that to internal interface.

  nsCOMPtr<nsIComponentManager> compMan;
  rv = NS_GetComponentManager(getter_AddRefs(compMan));
  if (NS_FAILED(rv)){
    //cout<<"Error: No se pudo acceder al component manager"<<endl;
    return 0;
  }
  
  nsCOMPtr<nsIDOMWindowInternal> internalContent;
  rv = content->QueryInterface( NS_GET_IID(nsIDOMWindowInternal),getter_AddRefs(internalContent));

  if ( !internalContent ) {
    //cout<<"+++++++ERROR: couldn't convert dom window to internal interface"<<endl;
    return 0;
  }
  // Get location.
  nsCOMPtr<nsIDOMLocation> location;
  internalContent->GetLocation( getter_AddRefs( location ) );
  if ( !location ) {
    //cout<<"+++++++ERROR: no location"<<endl;
    return 0;
  }


  // Get href for URL.
  nsString url;
  if ( NS_FAILED( location->GetHref( url ) ) ) {
    //cout<<"+++++++ERROR: no href"<<endl;
    return 0;
  }

  //cout<<"-----------LOCATION: "<< NS_LossyConvertUTF16toASCII(url.get()).get()<<endl;

  buffer = new char[NS_LossyConvertUTF16toASCII(url.get()).Length()+1];
  strcpy(buffer,NS_LossyConvertUTF16toASCII(url.get()).get());
  buffer[NS_LossyConvertUTF16toASCII(url.get()).Length()] = 0;

  return buffer;

}
