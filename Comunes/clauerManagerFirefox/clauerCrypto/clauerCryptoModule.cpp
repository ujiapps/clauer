#include "nsIModule.h"
#include "nsIGenericFactory.h"

#include "clauerCrypto.h"


NS_GENERIC_FACTORY_CONSTRUCTOR(ClauerCrypto)


static nsModuleComponentInfo components[] =
{
    {
       CLCRYPTO_CLASSNAME, 
       CLCRYPTO_CID,
       CLCRYPTO_CONTRACTID,
       ClauerCryptoConstructor
    }
};


NS_IMPL_NSGETMODULE("ClauerCryptoModule", components) 
