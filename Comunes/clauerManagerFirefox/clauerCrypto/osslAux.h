/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-*/


#ifndef _OSSLAUX_H_
#define _OSSLAUX_H_

#include <openssl/x509.h>
#include <openssl/pem.h>
#include <openssl/bio.h>

#include <string.h>



char * OSSL_X509_get_purposes(X509 * cert);


int OSSL_X509_is_CA(X509 * cert);

int OSSL_X509_is_rootCA(X509 * cert);


#endif
