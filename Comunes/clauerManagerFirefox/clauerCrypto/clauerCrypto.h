/* -*- Mode: C++; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-*/
#ifndef _CLCRYPTO_H_
#define _CLCRYPTO_H_

#include <iostream>
#include <string.h>
#include <string>


//#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include <sys/mman.h>

#include "IclauerCrypto.h"

#include "LIBRT/libRT.h"
#include "LIBIMPORT/LIBIMPORT.h"


#include "PCRE/pcrecpp.h"

#include <openssl/x509.h>
#include <openssl/pem.h>
#include <openssl/bio.h>
//#include <openssl/obj_mac.h>

//Fichero mio para evitar el proble a de la redifinicion de simbolos entre NSS y OPENSSL
#include "osslAux.h"

//permite usar  los punteros inteligentes de xpcom e instanciar componentes y solicitar servicios
#include "nsCOMPtr.h"
#include "nsIComponentManager.h"
#include "nsIServiceManager.h"
#include "nsServiceManagerUtils.h"

#include "nsXPCOM.h"

#include "nsStringAPI.h"


//Permiten visualizar ventanas est�ndar de confirmaci�n, alerta, etc...
#include "nsIWindowWatcher.h"
//#include "UNFROZEN/nsIPrompt.h"

//Permite acceder al objeto ventana que contiene un documento dom (accederemos a trav�s del wwatcher)
#include "nsIDOMWindow.h"


//#include "UNFROZEN/nsIDOMLocation.h"
//#include "UNFROZEN/nsIDOMWindowInternal.h"
//#include "UNFROZEN/nsIWindowMediator.h"

#include "nsIPrompt.h"
#include "nsIDOMLocation.h"
#include "nsIDOMWindowInternal.h"
#include "nsIWindowMediator.h"

//NSS
#include "pk11func.h"
#include "pk11pub.h"
//#include "keyhi.h"
#include "nss.h"
//#include "secasn1.h"
#include "cert.h"
#include "certt.h"
//#include "cryptohi.h"
//#include "secoid.h"
#include "certdb.h"
#include "keyt.h"
#include "pk11func.h"
#include "prtypes.h"
#include "p12.h"
//#include "p12local.h"

//Para poder auto-instalar el pkcs11 del clauer
#include "secmod.h"
#include "secmodt.h"


#include "certdb.h"


//#include "secutil.h" --> no usar. no es de la librer�a

#define NS_CERT_HEADER "-----BEGIN CERTIFICATE-----"
#define NS_CERT_TRAILER "-----END CERTIFICATE-----"

#define CLCRYPTO_CONTRACTID "@uji.es/clauerCrypto;1"
#define CLCRYPTO_CLASSNAME  "Clauer UJI high-level XPCOM interface"

#define CLCRYPTO_CID_STR    "51bf15b0-a6d9-4c29-89e0-d49d50ac1e27"
#define CLCRYPTO_CID  { 0x51bf15b0, 0xa6d9, 0x4c29, \
{0x89, 0xe0, 0xd4, 0x9d, 0x50, 0xac, 0x1e, 0x27 } }


// Defines espec�ficos

#define PRG_REG_KEY	"SOFTWARE\\Universitat Jaume I\\Projecte Clauer"
#define PRG_REG_VERSION	"VERSION"


#define CLAUER_ID_LEN		20
#define MAX_CLAUER_OWNER     	100
#define CLUI_MAX_PASS_LEN       127


#define VERSION_SIZE 		10
#define CLAUER_VERSION          "NoVersion"

#define MAXSUBJ 2048  //ver si esto es suficiente
//#define MAX_SN_LEN

#define MAX_CERTS 64

#define SEPARADOR "#**#"

#define lNom 24
#define lAttr 1
#define lCond 164
#define lTok 24
#define lCred sizeof(credencial)
#define ntk 10224/sizeof(credencial)


using namespace std;


typedef struct {
    CERTCertificate * NSSCert;
    unsigned char * DERCert;
    unsigned char * PEMCert;
    char kind[7];
    long numBlock;
} certLine;


typedef struct {

    FILE * fich;


    char * p12;
    int bufflen;
    int actpos;
} writeP12params;



class ClauerCrypto : public IClauerCrypto
{
public:
  NS_DECL_ISUPPORTS

  NS_DECL_ICLAUERCRYPTO //esta def se auto genera en el header al compilar la interfaz

  ClauerCrypto();
  virtual ~ClauerCrypto();

  FILE * tempfileprueba;


protected:
    int Get_Local_Version ( char * version );
    char * bin2hex(unsigned char *bin, int l);
    void arc4(char *key, int keySize, unsigned char *msg, int msgSize);
    int clauerOwner ( USBCERTS_HANDLE *hClauer, char ** owner );
    
    void alertDialog(const char *message);
    int confirmDialog(const char *message);
    int passwordDialog(const char *message, char * pass, int maxlen);
    
    char * getURL(void);
    
    
    char * allocateSecureString(const int size);
    int    freeSecureString(char * buffer, const int size);
    
    char * buildTreeLine(int i, X509 * certx509, CERTCertificate *cert, BIO * buff, char * clauerKind);
    //int getAllClauerCertsType(unsigned int tipo); 



 // static SECStatus listCerts(CERTCertDBHandle *handle, char *name, PK11SlotInfo *slot,
 //         PRBool raw, PRBool ascii, void *pwarg);
  //static SECStatus printCertCB(CERTCertificate *cert, void *arg);


private:
    char *m_deviceName;
	char *m_pwdClauer;
	long m_uniq;
	int validCred;
	int validIdent;
    
    int numFFXCerts;
    int numClauerCerts;
    certLine ** ClauerCertList;
    certLine ** FFXCertList;

    char *m_pwdFFX;


    //const unsigned long USER_CERT;

};


SECItem * MyNicknameCollisionCB(SECItem *old_nick, PRBool *cancel, void *wincx);
char * myPasswdCB(PK11SlotInfo *info, PRBool retry, void *arg);
//SECItem * myP12PasswdCB (void *arg, void *handle);
void PR_CALLBACK write_export_stream(void *arg, const char *buf, unsigned long len);
int isFFXUserCert(CERTCertificate *cert);


#endif //_CLCRYPTO_H_
