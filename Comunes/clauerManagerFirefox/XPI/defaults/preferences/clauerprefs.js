//Para controlar la instalación del pkcs11 del clauer.

pref("extensions.clauermanager.pkcs11.firstrun", true);
pref("extensions.clauermanager.pkcs11.installed", false);
pref("extensions.clauermanager.pkcs11.autoinstall", true);

//Poner a true para distribuir el .deb 'sin' el gestor
pref("extensions.clauermanager.hidden", false);


//pref("extensions.clauerManager@uji.es.description", "chrome://clauermanager/locale/main.properties");
//pref("extensions.clauerManager@uji.es.name", "chrome://clauermanager/locale/main.properties");
