#!/bin/bash



#Parametros para realizar las pruebas 

PROFILE=default
#PROFILE=ffx36
#PROFILE=default
#PROFILE=TESTProfile
#PROFILE=TestProfile2


FFXEXE='/home/paco/.bin/firefox/firefox'
#FFXEXE='/home/paco/.bin/firefox3.6/firefox'
#FFXEXE=firefox


#FALTA: verificar ruta para el jar, make, gcc y g++ en fedora
#       lanzar el apt-get install y el yum install de las dependencias?


#  --> verificar que hay ruta para el jar, make, gcc, g++ en debian




######### DEPENDENCIAS #########


DEBIANDEPS="g++ gcc libssl-dev xulrunner-dev fastjar" # El fastjar est� a extinguir...
FEDORADEPS="gcc gcc-c++ openssl-devel xulrunner-devel nss-devel java-1.6.0-openjdk-devel"


dpkgExists=$(dpkg --help 2>/dev/null)
#Existe el dpkg
if [ "$dpkgExists" != "" ]
    then

    echo "EXISTE EL DPKG"
    
    STOP=0
    for pck in $DEBIANDEPS
      do
      exists=$(dpkg -l | grep $pck)
      if [ "$exists" == ""  ]
	  then
	  STOP=1
	  echo "Package $pck not found."
      fi
    done
    
    [ "$STOP" -eq 1 ] && exit 1

fi


rpmExists=$(rpm --help  2>/dev/null)
#Existe el dpkg
if [ "$rpmExists" != "" ]
    then

    echo "EXISTE EL RPM"
    
    STOP=0
    for pck in $FEDORADEPS
      do
      exists=$(rpm -qa | grep $pck)
      if [ "$exists" == ""  ]
	  then
	  STOP=1
	  echo "Package $pck not found."
      fi
    done
    
    [ "$STOP" -eq 1 ] && exit 1

fi






######### VARIABLES #########


if [  -d /usr/lib/firefox-addons ]     #estamos en Ubuntu
    then 
    echo "===ES UBUNTU==="
    FFXADDONSDIR=/usr/lib/firefox-addons
    FFXDIR=$(ls -d1 /usr/lib/firefox-[0-9].*)
else                                   # Debian o cualquier otra
    echo "===ES DEBIAN==="
    FFXADDONSDIR=$(ls -d1 /usr/lib/{iceweasel,firefox} 2>/dev/null)
    FFXDIR=$(ls -d1 /usr/lib/{iceweasel,firefox} 2>/dev/null)
fi



if [ -d  /usr/include/nss  ]
    then
    NSSDIR=/usr/include/nss
elif  [ -d  /usr/include/nss3  ]
    then
    NSSDIR=/usr/include/nss3
else
    echo "No se encuentra la ruta de include NSS"
    exit 1
fi


if [ -d /usr/include/nspr ]
    then
    NSPRDIR=/usr/include/nspr
elif  [ -d /usr/include/nspr4 ]
    then
    NSPRDIR=/usr/include/nspr4
else
    echo "No se encuentra la ruta de include NSPR"
    exit 1
fi


ISFEDORA=""$(ls -d1 /usr/lib/xulrunner-sdk-[0-9].[0-9]* 2>/dev/null)
if [ "$ISFEDORA" != '' ]
    then
    echo "===ES FEDORA==="
    FFXDIR=$(ls -d1 /usr/lib/firefox-[0-9].*)
    FFXADDONSDIR=$FFXDIR
    XULDIR=$(ls -d1 /usr/lib/xulrunner-sdk-[0-9].[0-9]*) 
    IDLROOT=$(ls -d1 /usr/share/idl/xulrunner-sdk-[0-9].[0-9]*)
else
    XULDIR=$(ls -d1 /usr/lib/xulrunner-devel-[0-9].[0-9]*) 
    IDLROOT=$(ls -d1 /usr/share/idl/xulrunner-[0-9].[0-9]*)
fi


XPIDLC=$XULDIR/bin/xpidl


if [ -f "$IDLROOT/unstable/nsISupports.idl" ] 
    then
    IDLDIR=$IDLROOT/unstable
else
    IDLDIR=$IDLROOT/
fi



#Last test


if [ -d "$NSSDIR"  ]
    then :
else
    echo "Path not found: $NSSDIR" 
    [ $(echo $NSSDIR | wc -c) -gt 1 ] && echo "multiple matching paths!! select one."
    exit 1
fi

if [ -d "$NSPRDIR" ]
    then :
else
    echo "Path not found: $NSPRDIR" 
    [ $(echo $NSPRDIR | wc -c) -gt 1 ] && echo "multiple matching paths!! select one."
    exit 1
fi
if [ -d "$XULDIR"  ]
    then :
else
    echo "Path not found: $XULDIR" 
    [ $(echo $XULDIR | wc -c) -gt 1 ] && echo "multiple matching paths!! select one."
    exit 1
fi
if [ -d "$IDLROOT" ]
    then :
else
    echo "Path not found: $IDLROOT" 
    [ $(echo $IDLROOT | wc -c) -gt 1 ] && echo "multiple matching paths!! select one."
    exit 1
fi
if [ -d "$IDLDIR"  ]
    then :
else
    echo "Path not found: $IDLDIR" 
    [ $(echo $IDLDIR | wc -c) -gt 1 ] && echo "multiple matching paths!! select one."
    exit 1
fi
if [ -f "$XPIDLC"  ]
    then :
else
    echo "File not found: $XPIDLC" 
    [ $(echo $XPIDLC | wc -c) -gt 1 ] && echo "multiple matching paths!! select one."
    exit 1
fi





######### PARAMETROS #########


DOTEST=0
DOINSTALL=0
DOCLEAN=0
DOCOMPPCRE=0
DOCOMPSSL=0

for i in $*
  do
  
  if [ $i == "-h" -o $i == "--help" ];
      then
      echo "Uso: ./ff3install [universe] [opciones]"
      echo "-t      : Adem�s de compilar, instala la extensi�n y lanza el firefox"
      echo "-i      : Adem�s de compilar, instala la extensi�n a nivel del sistema (debes ser root)"
      echo "-clean  : limpia la instalacion de binarios (regenerar pcre y ossl despues)"
      echo "-pcre   : compila la libreria pcre"
      exit 0
  fi
  
  
  if [ $i == "-t" ]
      then
      DOTEST=1  
  fi
  
  if [ $i == "-i" ];
      then

      if [ -d "$FFXADDONSDIR" ]
	  then :
      else
	  echo "Path not found: $FFXADDONSDIR" 
	  [ $(echo $FFXADDONSDIR | wc -c) -gt 1 ] && echo "multiple matching paths!! select one."
	  exit 1
      fi
      
      if [ -d "$FFXDIR"  ]
	  then :
      else
	  echo "Path not found: $FFXDIR" 
	  [ $(echo $FFXDIR | wc -c) -gt 1 ] && echo "multiple matching paths!! select one."
	  exit 1
      fi
      
      DOINSTALL=1  
  fi
  
  if [ $i == "-t" ];
      then
      PROFDIR=$(ls -d1 $HOME/.mozilla/firefox/*.$PROFILE) 2> /dev/null
      echo $PROFDIR
      echo $PROFILE
      
      if [ "$PROFDIR" == "" ]
	  then
	  echo "No route to Profile"
	  exit 1
      fi
  fi
  
  if [ $i == "-pcre" ];
      then
      DOCOMPPCRE=1
      echo "Compilamos la PCRE."
  fi
  
  if [ $i == "-clean" ];
      then
      DOCLEAN=1
      echo "Limpiamos de binarios"
  fi
  
done



#Si es un n�mero
if [ "$1" -gt 0 ] 2>/dev/null
    then 
    UNIVERSE=$1
else
    UNIVERSE=0
fi



######### LIMPIEZA #########


if [ $DOCLEAN -eq "1" ];
    then
    ./clean.sh
    exit 0
fi





######### UNIVERSE VISIBILITY #########

case "$UNIVERSE" in
    
    "0" | "4" )
    #El gestor es visible
    sed -i -re  's/(extensions.clauermanager.hidden", )(true|false)/\1false/g'  XPI/defaults/preferences/clauerprefs.js
    ;;
    
    "1" | "2" | "3" )
    #El gestor est� oculto
    sed -i -re  's/(extensions.clauermanager.hidden", )(true|false)/\1true/g'   XPI/defaults/preferences/clauerprefs.js
    ;;
    
    * )
    echo "No known universe code."
    exit 1
    ;;

esac




######### REGISTRO DE LOS COMPONENTES #########

if [ $DOTEST -eq "1" ];
then
    #borramos los registros de componentes y de interfaces del profile del usuario
    rm $PROFDIR/compreg.dat	2> /dev/null
    rm $PROFDIR/xpti.dat	2> /dev/null
fi


if [ $DOINSTALL -eq "1" ];
then
    #marcamos la necesidad de regenerar las bd de componentes para todos los usuarios
    touch $FFXDIR/.autoreg
fi




######### EXPOSER #########

rm clauerExposer/*~                  2>/dev/null
rm clauerExposer/IclauerExposer.xpt  2>/dev/null

$XPIDLC -m typelib -I $IDLDIR            \
        -o clauerExposer/IclauerExposer  \
        clauerExposer/IclauerExposer.idl


if [ -e clauerExposer/IclauerExposer.xpt ]; 
then 
    
    cp -f clauerExposer/IclauerExposer.xpt XPI/components/
    cp -f clauerExposer/clauerExposer.js   XPI/components/
    echo "Exposer typelib compilada correctamente"
    
else
    echo "ERROR: No se ha generado la Typelib de la interfaz Exposer."
    exit 1
fi



######### PCRE #########


if [ $DOCOMPPCRE -eq "1" ];
    then
    
    cd comunes/PCRE
    echo -n "Entrando en: "
    pwd
    
    echo -n "Compilando PCRE"
    ./pcre-7.5/configure
    [ "$?" -ne 0 ] && echo "Error configurando la PCRE" && exit 1
    make
    [ "$?" -ne 0 ] && echo "Error compilando la PCRE" && exit 1
    #cp .libs/*.o ../
    cp -f .libs/*.a ../
    [ "$?" -ne 0 ] && echo "Error copiando librerias de PCRE" && exit 1
    cp -f ./pcre-7.5/pcrecpp.h ./
    [ "$?" -ne 0 ] && echo "Error copiando cabeceras de PCRE" && exit 1
    
    cd ../..
    
fi




######### OPENSSL #########


if [ "$DOCOMPSSL" -eq "1" ];
    then
    
    cd comunes/OPENSSL/openssl-0.9.8g/
    echo -n "Entrando en: "
    pwd
    
    echo -n "Compilando OPENSSL"
    ./config no-shared
    make
    $(make test)
    cp libcrypto.a ../../
    
    cd ./../..
    
fi




######### CRYPTO #########

cd clauerCrypto
echo -n "Entrando en: "
pwd

rm *~ 2>/dev/null

rm -vf IclauerCrypto.xpt 2>/dev/null
rm -vf clauerCrypto.so	 2>/dev/null

echo "make XULRUNNERDIR=$XULDIR NSSINCLDIR=$NSSDIR NSPRINCLDIR=$NSPRDIR ff3"
make clean
make XULRUNNERDIR=$XULDIR NSSINCLDIR=$NSSDIR NSPRINCLDIR=$NSPRDIR ff3

if [ -e IclauerCrypto.xpt ]; 
then 
    echo "Crypto typelib compilada correctamente"
    cp -f IclauerCrypto.xpt   ../XPI/components/

else
    echo "ERROR: No se ha generado la Typelib de la interfaz Crypto."
    exit 1
fi
if [ -e clauerCrypto.so ]; 
    then 
    echo "Crypto comp compilado correctamente"
    cp -f clauerCrypto.so     ../XPI/platform/Linux_x86-gcc3/components/
    
else
    echo "ERROR: No se ha generado la libreria del componente."
    exit 1
fi

cd ..




######### XUL #########

cd XUL

#Limpiamos el arbol de dirs
find ./ -iname "*~"| xargs rm -v 	2> /dev/null


cd clauerManagerXUL

#creamos el jar del chrome
rm clauermanager.jar	2> /dev/null
jar -cvf clauermanager.jar *

if [ -e clauermanager.jar ];
    then
    #copiamos el jar al dir del XPI
    cp -f clauermanager.jar ../../XPI/chrome
    rm clauermanager.jar	2> /dev/null
else
    echo "ERROR: No se ha generado el jar."
    exit 1
fi

cd ../..




######### Dir XPI_temp libre de .svn #########

#limpiamos el arbol de dirs de backups
find ./ -iname "*~"| xargs rm -v	2> /dev/null

#copiamos y limpiamos el dir de la extension de .svn
cp -rf XPI XPI_temp

cd XPI_temp

find ./ -name ".svn" | xargs rm -rf -

cd ..




######### XPI #########

#Generamos el paquete instalable xpi

cd XPI_temp

#Comprimimos el arbol de dirs
rm -v clauerManager.xpi	2> /dev/null
rm -v ../clauerManager.xpi	2> /dev/null
zip -R clauerManager.xpi '*'

#Lo copiamos al directorio raiz
cp -f clauerManager.xpi ../
rm -f clauerManager.xpi

cd ..




######### END #########

if [ -e clauerCrypto/clauerCrypto.so ]; 
then 
    
    rm *~ 2>/dev/null
    
    DIST=$(cat /home/clauer/dist 2> /dev/null)
    
    if [ -z "$DIST" ];  #Si la cadena es vac�a
	then
	echo "No se esta ejecutando en �rbol de CHROOTs."
	DIST=noDist
    else
	echo "Ejecutando en �rbol de CHROOTs."
	mkdir -p     ../$DIST/$FFXADDONSDIR/extensions/clauerManager@uji.es
	cp -rf XPI_temp/* ../$DIST/$FFXADDONSDIR/extensions/clauerManager@uji.es/
    fi
    echo 'Compilacion finalizada'
else
    echo 'Error en la compilaci�n'
    exit 1
fi




######### INSTALL #########


if [ $DOINSTALL -eq "1" ];
then
    
    rm -rf $FFXADDONSDIR/extensions/clauerManager@uji.es           2> /dev/null
    cp -rf XPI_temp/ $FFXADDONSDIR/extensions/clauerManager@uji.es
    
fi




######### PRUEBAS #########


if [ $DOTEST -eq "1" ];
    then
    
    #Instalamos la extensi�n en el profile del usuario
    rm -rf $PROFDIR/extensions/clauerManager@uji.es   2>/dev/null
    cp -rf XPI_temp $PROFDIR/extensions/clauerManager@uji.es
    
    rm -rf XPI_temp
    
    # Activamos logging del Firefox, para debug.
    #export NSPR_LOG_MODULES=nsNativeModuleLoader:5
    export NSPR_LOG_MODULES=all:5
    export NSPR_LOG_FILE=/tmp/nsprlog
    
    #lanzamos el firefox
    $FFXEXE -P $PROFILE -no-remote
    
fi




#Aunque no hagamos pruebas, hay que borrar el dir XPI_temp
rm -rf XPI_temp
    
