#ifndef __LIBIMPORT_H__
#define __LIBIMPORT_H__


#define PRIVKEY_DEFAULT_CIPHER   0
#define PRIVKEY_PASSWORD_CIPHER  1
#define PRIVKEY_DOUBLE_CIPHER    2 
#define PRIVKEY_NO_CIPHER        3 

#if defined(WIN32)

#include <windows.h>

#endif

#if defined(LINUX)

#define BOOL unsigned char
#define TRUE  1
#define FALSE 0 
#define BYTE unsigned char
#define LPTSTR char *
#define DWORD unsigned int

#define MAX_PASSPHRASE 127

// Funci�n de windows, equivalente para Linux en 
// CryptoWrapper
//
#define SecureZeroMemory CRYPTO_SecureZeroMemory

// Defines de windows para compatibilidad en 
// Linux.

// Obtenido de windows.h 
#define MAX_PATH 260

// Obtenidos de wincrypt.h
#define ALG_CLASS_KEY_EXCHANGE  (5 << 13)
#define ALG_CLASS_SIGNATURE     (1 << 13)
#define ALG_TYPE_RSA            (2 << 9)
#define ALG_SID_RSA_ANY          0

#define CALG_RSA_KEYX  (ALG_CLASS_KEY_EXCHANGE|ALG_TYPE_RSA|ALG_SID_RSA_ANY) // 0x0000a400
#define CALG_RSA_SIGN  (ALG_CLASS_SIGNATURE|ALG_TYPE_RSA|ALG_SID_RSA_ANY)    // 0x00002400 

#endif


#include <LIBRT/libRT.h>

#ifdef __cplusplus
extern "C" {
#endif

    /* Descripci�n: Importa certificado, llave privada y certificados ra�z desde un 
                    fichero pPKCS#12 al Clauer. 
    
       Par�metros: fileName: Nombre del fichero pkcs#12 a importar.
                   pwd:      Password del fichero pkcs12.
		   hClauer:  Handle del Clauer.
		   mode:     Modo en el que queremos que se cifren las claves:
		                    PRIVKEY_DEFAULT_CIPHER.
				    PRIVKEY_DOUBLE_CIPHER. 
				    PRIVKEY_PASSWORD_CIPHER.
				    PRIVKEY_NO_CIPHER.
		   
    */
  
    BOOL LIBIMPORT_ImportarPKCS12 (const char *fileName, const char *pwd, USBCERTS_HANDLE *hClauer, int mode, char *extra_pass );
  
    BOOL LIBIMPORT_ImportarPKCS12_internal (const char *fileName, const char *pwd, USBCERTS_HANDLE *hClauer, int mode, char *extra_pwd, BOOL importarCAs);
  
    BOOL LIBIMPORT_ImportarPKCS12deBuffer (BYTE * pkcs12, DWORD tam, const char *pwd, USBCERTS_HANDLE *hClauer, int mode, char *extra_pwd, BOOL importarCAs);


    BOOL LIBIMPORT_ExportarPKCS12 (long blockNum, const char *pwd, USBCERTS_HANDLE *hClauer, const char *fileName); 
    BOOL LIBIMPORT_ExportarPKCS12enBuffer (long blockNum, const char *pwd, USBCERTS_HANDLE *hClauer, BYTE ** destBuf, unsigned long * destTam);



#ifdef __cplusplus
}
#endif

#endif
