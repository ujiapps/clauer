#include "LIBRTMutex.h"

using namespace std;

int LIBRT_MUTEX_Crear (LIBRT_MUTEX *m)
{
#if defined(WIN32)
	*m = CreateMutex (NULL, 0, NULL);

	if ( *m == NULL )
		return 0;
	else
		return 1;
#elif defined(LINUX)
	pthread_mutex_init(m, NULL);
	return 1;
#endif
}



int LIBRT_MUTEX_Destruir(LIBRT_MUTEX m)
{

#if defined(WIN32)
	return CloseHandle(m);
#elif defined(LINUX)
	if ( pthread_mutex_destroy(&m) != 0 )
	  return 0;
	else
	  return 1;
#endif

}



int LIBRT_MUTEX_Lock (LIBRT_MUTEX m)
{
#if defined(WIN32)
	if ( WaitForSingleObject(m, LIBRT_MUTEX_TIMEOUT) == WAIT_FAILED ) {
		return 0;
	} else {
		return 1;
	}
#elif defined(LINUX)
	if ( pthread_mutex_lock(&m) != 0 )
	  return 0;
	else
	  return 1;
#endif
}


int LIBRT_MUTEX_Unlock (LIBRT_MUTEX m)
{
#if defined(WIN32)
	if ( ReleaseMutex(m) == 0 ) {
		return 0;
	} else {
		return 1;
	}
#elif defined(LINUX)
	if (pthread_mutex_unlock(&m) != 0 )
	  return 0;
	else
	  return 1;
#endif
}
