#include "nls.h"

#ifndef MAC
int nls_lang_init()
{
  
#ifdef WIN32
  char str_codepage[64];
  UINT codepage; 
#endif 
    
    /* Reset the locale variables LC_* */
    setlocale(LC_ALL, "");
  
#ifdef LINUX
  setlocale(LC_TIME, "" );
  setlocale(LC_MESSAGES, "");
#endif
 

  /* Bind with MO File */
  bindtextdomain( PACKAGE, LOCALE_PATH );
  textdomain( PACKAGE );

  /* Get system locale codepage and set gettext 
   * translation to this locale codepage. 
   */
  
#ifdef LINUX 
  if ( ! bind_textdomain_codeset( PACKAGE, nl_langinfo(CODESET) )){
    fprintf(stderr,_("[ERROR] Changing the language to your locale codepage, messages will be printed in English"));
    return 1;
  }
#elif WIN32
  codepage= GetConsoleOutputCP(); 
  str_codepage[0]='C';  str_codepage[1]='P';
  _itoa_s( codepage, str_codepage+2, 62 , 10 ); 
  
  // printf( "OBTENIDO CODEPAGE: %s y codepage del terminal= %d \n", str_codepage, codepage );
  // A pesar de que la funci�n GetConsoleOutput parece que nos indica que el codepage es CP850,
  // el codepage que muestra bien los acentos tanto abiertos como cerrados es el CP1252, existe
  // un problema al mostrar mayusculas acentuadas.
  if ( ! bind_textdomain_codeset( PACKAGE, str_codepage )){
    fprintf(stderr,_("[ERROR] Changing the language to your locale codepage, messages will be printed in English"));
    return 1;
  }
#endif
  return 0;
}
#endif
