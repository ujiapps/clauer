#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>

#include <CRYPTOWrapper/CRYPTOWrap.h>
#include <LIBRT/libRT.h>
#include <clio/clio.h>

#include "misc.h"
#include <stdlib.h>

#ifdef WIN32
#define snprintf  _snprintf
#endif



//#define MAX_DEVICE_LEN	256


typedef enum { 
    ST_INI,
    ST_HELP,
    ST_ALL,
    ST_DEV_INI,
    ST_DEV,
    ST_DEV_ALL,
    ST_PWD,
    ST_LIST,
    ST_ERR
} parse_st_t;



//static int g_deleteAll;                  // Indica si hay que eliminar todo el dispositivo
static char g_szDevice[MAX_PATH_LEN+1];  // El dispositivo seleccionado o el nombre del fichero
//static int g_isFile;                     // Indica si se trata de un fichero o no
static int g_setPassword;                // Indica si se debe establecer una nueva password



void usage ( void )
{
    fprintf(stderr, "Modo de empleo: cldestroy (-h|--help) | (-l|--list) |[-p password] (-d (device|file|ALL))\n");
    fprintf(stderr, "Borra los objetos criptogr�ficos del clauer.\n");
    fprintf(stderr, "Elimina únicamente los objetos, conservando la patición y el\n");
    fprintf(stderr, "bloque de información\n\n");

    fprintf(stderr, "Las opciones posibles son:\n");
    fprintf(stderr, "   -h, --help               imprime este mensaje de ayuda\n");
    fprintf(stderr, "   -l, --list               lista los clauers insertados en el sistema\n");
    fprintf(stderr, "   -d, --dev (device|ALL)   selecciona el dispositivo/fichero device o ALL para todos"
                    "                            los clauers insertados en el sistema\n"
                    "   -p, --password           establecer nueva password\n\n"
	            "Sin argumentos, destruye todos los clauers insertados en el sistema\n\n");
    fprintf(stderr, "EJEMPLOS\n\n"
	    "\t. Listar los clauers insertados en el sistema:\n"
	    "\t        cldestroy -l\n\n"
	    "\t. Eliminar TODOS los claues insertados en el sistema:\n"
	    "\t        cldestroy\n\n"
	    "\t. Eliminar el clauer \\\\.\\PHYSICALDRIVE1:\n"
	    "\t        cldestroy -d \\\\.\\PHYSICALDRIVE1\n\n");
}



int parse ( int argc, char **argv, parse_st_t *st, char errMsg[256] )
{

    int op;

    memset(g_szDevice, 0, sizeof g_szDevice);
    g_setPassword = 0;

    *st = ST_INI;

    for ( op = 1 ; (op < argc) && (*st != ST_ERR) ; op++ ) {


	if ( *st == ST_INI ) {

	  if ( ( strcmp(argv[op], "-h") == 0 ) ||
	       ( strcmp(argv[op], "--help") == 0 ) ) {
	    
	    *st = ST_HELP;
	  } else if ( ( strcmp(argv[op], "-l") == 0 ) || strcmp(argv[op], "--list") == 0 ) {
	    *st = ST_LIST;
	  } else if ( ( strcmp(argv[op], "-d") == 0 ) || strcmp(argv[op], "--device") == 0 ) {
	    *st = ST_DEV_INI;
	  } else if ( ( strcmp(argv[op], "-p") == 0 ) || strcmp(argv[op], "--password") == 0 ) {
	    g_setPassword = 1;
	  } else {
	    
	    snprintf(errMsg, 256, "Opci�n inv�lida: %s", argv[op]);
	    *st = ST_ERR;
	  }

	} else if ( *st == ST_HELP ) {
	  snprintf(errMsg, 256, "-h o --help deben aparecer solas");
	  *st = ST_ERR;
	} else if ( *st == ST_DEV_INI ) {
	  strncpy(g_szDevice, argv[op], MAX_PATH_LEN);
	  g_szDevice[MAX_PATH_LEN] = 0;
	  *st = ST_DEV;
	} else if ( *st == ST_DEV ) {
	  snprintf(errMsg, 256, "Opci�n inv�lida: %s\n", argv[op]);
	  *st = ST_ERR;
	}
    }
    
    
    /* Una �ltima comprobaci�n de errores. Si hemos acabado en un estado
     * no final (ST_DEV_INI) --> ERROR
     */
    
    if ( *st == ST_DEV_INI ) {
      snprintf(errMsg, 256, "-d o --dev deben ir seguidos por el nombre del dispositivo\n");
      *st = ST_ERR;
    }
    

    return 0;
    
}




#define MAX_PROMPT    500
int destroy ( int all )
{
  int bAllDevices, nDev, i, err, j;
  char *devs[MAX_DEVICES];
  char sn;//, auxPrompt[MAX_PROMPT+1];
  clauer_handle_t hClauer;
  block_info_t ib;
  unsigned char block[BLOCK_SIZE];
  long n;

  char pwd[MAX_PASSPHRASE], pwdConfirmacion[MAX_PASSPHRASE];

  /* Si no se especific� dispositivo, eliminamos todos los clauers insertados
   */

  if ( *g_szDevice == 0 || (strcmp(g_szDevice, "ALL") == 0) ) {
    bAllDevices = 1;
  } else
    bAllDevices = 0;

  /* Pido confirmaci�n para la eliminaci�n de los dispositivos
   */

  printf("Se va a proceder a la destrucci�n de ");
  if ( bAllDevices )
    printf("TODOS los clauers insertados en el sistema\n");
  else
    printf("%s\n", g_szDevice);

  printf("�Desea continuar? (s/n) ");
  fflush(stdout);
  scanf("%c", &sn);

  putchar('\n');

  if ( toupper(sn) != 'S' ) {
    printf("[ATENCI�N] Operaci�n cancelada por el usuario\n");
    return 1;
  }
    
  /* Construyo la lista de dispositivos a eliminar
   */

  memset(devs, 0, sizeof(char *) * MAX_DEVICES);
  
  if ( bAllDevices ) {

      if ( LIBRT_ListarDispositivos(&nDev, (unsigned char **)devs) != 0 ) {
      fprintf(stderr, "[ERROR] Obteniendo clauers del sistema\n");
      return 0;
    }

    if ( nDev == 0 ) {
      fprintf(stderr, "[ATENCI�N] No hay clauers insertados en el sistema.\n"
	              "           No se realiz� ninguna operaci�n\n");
      return 0;
    }
  } else {
    nDev = 1;
    devs[0] = (char *) malloc ( sizeof g_szDevice );
    if ( ! devs[0] ) {
      fprintf(stderr, "[ERROR] Memoria insuficiente\n");
      return 0;
    }

    strcpy(devs[0], g_szDevice);
  }



  /* Comienza la eliminaci�n
   */


  putchar('\n');
  for ( i = 0 ; i < nDev ; i++ ) {
    
    printf("Eliminando %s... ", devs[i]);
    fflush(stdout);
    
    /* Compruebo si el dispositivo es un clauer
     */

    if ( IO_Is_Clauer(devs[i]) == IO_IS_NOT_CLAUER ) {
      fprintf(stderr, "[ERROR]\n"
	              "        %s no es un clauer\n", devs[i]);
      continue;
    }

    /* Ahora empiezo a eliminar
     */

    err = IO_Open(devs[i], &hClauer, IO_RDWR,  IO_CHECK_IS_CLAUER);
    if ( err != IO_SUCCESS ) {
      fprintf(stderr,   "[ERROR]\n"
	                "        No se pudo abrir %s\n", devs[i]);
      if ( err == ERR_IO_NO_PERM ) {
	fprintf(stderr, "        Debe ser administrador para poder ejecutar cldestroy\n");
	fprintf(stderr, "        Se aborta la ejecuci�n del programa\n");
	return 0;
      }
      continue;
    }
    
    /* Machacamos s�lo hasta el current block
     */

    if ( IO_ReadInfoBlock(hClauer, &ib) != IO_SUCCESS ) {
      fprintf(stderr, "[ERROR]\n"
	      "        No se pudo obtener informaci�n del clauer\n");
      IO_Close(hClauer);
      continue;
    }

    /* Haremos cuatro pasadas para borrar el contenido de la
     * partici�n criptogr�fica
     */

    memset(block, 0x00, sizeof block);
    j = 0;
    do {

      if ( IO_Seek(hClauer, 0, IO_SEEK_RESERVED) != IO_SUCCESS ) {

	if ( j == 0 ) {
	  fprintf(stderr, "[ERROR]\n"
		  "        No se pudo borrar el dispositivo \n");
	  break;
	} else {
	  fprintf(stderr, "[ATENCI�N]\n"
		  "           Se omitieron varias pasadas de borrado debido a un error\n");
	  break;
	}
      }
      
      err = IO_SUCCESS;
      
      if ( ib.cb == -1 )
	n = ib.rzSize + ib.totalBlocks;
      else
	n = ib.rzSize + ib.cb;
      
      while ( n-- && err == IO_SUCCESS) 
	err = IO_Write(hClauer, block);

      if ( (err != ERR_IO_EOF) && (err != IO_SUCCESS) ) {
	
	fprintf(stderr, "[ERROR]\n"
		"        No se pudo borrar por completo %s\n", devs[i]);
	break;
      } else {

	if ( j == 0 )
	  memset(block+8, 0xaa, sizeof block - 8);
	else if ( j == 1 )
	  memset(block + 8, 0x55, sizeof block - 8);
	else if ( j == 2 )
	  memset(block + 8, 0x00, sizeof block - 8);
      }

      ++j;

    } while ( j < 4 );

    /* Si salimos del bucle en la primera pasada, mal asunto
     */

    if ( j == 0 ) {
      IO_Close(hClauer);
      continue;
    }
    
    /* Dejamos current block apuntando a -1. Si no podemos no pasa
     * nada
     */

    ib.cb = 0;
    IO_WriteInfoBlock(hClauer, &ib);
    
    if ( g_setPassword ) {
      /* Pedimos la nueva password
       */
      char aux[80];
      char newIden[40];
      int aux_tam;
      
      while ( 1 ) {
      
	snprintf(aux, 79, "\n\tIntroduzca nueva contrase�a para %s: ", devs[i]); 
	if ( CLUTILS_AskPassphrase(aux, pwd) != 0 ) {
	  fprintf(stderr, "[ERROR] Error pidiendo contrase�a\n");
	  IO_Close(hClauer);
	  continue;
	}
	snprintf(aux, 79, "\tIntroduzca confirmaci�n para %s: ", devs[i]);
	if ( CLUTILS_AskPassphrase(aux, pwdConfirmacion) != 0 ) {
	  fprintf(stderr, "[ERROR] Error pidiendo contrase�a\n");
	  IO_Close(hClauer);
	  continue;
	}

	if ( strcmp(pwd, pwdConfirmacion) != 0 ) {
	  fprintf(stderr, "[ERROR] Las contrase�as no coinciden. Int�ntelo de nuevo\n");
	  fflush(stderr);
	} else {
	  break;
	}
      }

      /* Ahora la cambiamos la contrase�a
       */

      if ( CRYPTO_PBE_Cifrar ( pwd, ib.id, 20, 1000, 1, CRYPTO_CIPHER_DES_EDE3_CBC, (unsigned char *)"UJI - Clauer PKI storage system", 32, (unsigned char *)newIden, &aux_tam ) != 0 ) {
	fprintf(stderr, "[ERROR] No se pudo cambiar contrase�a. El clauer ha quedado vac�o.\n"
		"        Puede seguir operando con �l con su contrase�a antigua\n");
	IO_Close(hClauer);
	continue;
      }
      
      memcpy(ib.idenString, newIden, 40);


      if ( IO_WriteInfoBlock( hClauer, &ib) != IO_SUCCESS ) {
	fprintf(stderr, "[ERROR] No se pudo cambiar contrase�a. El clauer ha quedado vac�o.\n"
		"        Es posible que pueda seguir operando con su clauer con la contrase�a antigua\n");
	IO_Close(hClauer);
	continue;
      }
      
    }
      
    /* Acabamos con este dispositivo
     */

    IO_Close(hClauer);

    printf("[OK]");
    putchar('\n');
    fflush(stdout);
  }

  for ( i = 0 ; i < nDev ; i++ )
    free(devs[i]);

  printf("Operaci�n terminada con �xito\n");

  return 1;

}




int main ( int argc, char **argv )
{
    char errMsg[256];
    parse_st_t st;
  
    LIBRT_Ini();
    CRYPTO_Ini();

    parse(argc, argv, &st, errMsg);
  
    switch ( st ) {
    
    case ST_ERR:
	fprintf(stderr, "[ERROR] %s\n", errMsg);
	usage();
	break;
    
    case ST_HELP:
	usage();
	break;
    
    case ST_LIST:
	CLUTILS_PrintClauers();
	break;

    case ST_DEV:
    case ST_INI:
	destroy(0);
	break;

    case ST_ALL:
	destroy(1);
	break;

    default:
	break;
    }
  
    return 0;

}

