/* clwall. Utilidad para la gesti�n de carteras de tokens
 */

#include <stdio.h>
#include <string.h>

#include "misc.h"

#include <LIBRT/libRT.h>



typedef enum {
  ST_INI,
  ST_ERR,
  ST_ADD,
  ST_LIST,
  ST_HELP,
  ST_SHOW
} parse_st;


typedef struct {
  char nombre[24];
  unsigned char attr;
  char cond[164];
  char valor[24];
} token_t;


#define MAX_TOKEN_NAME_SIZE    24
#define MAX_TOKEN_COND_SIZE    164


char g_device[MAX_PATH_LEN+1];            /* dispositivo seleccionado */


void usage ( void )
{

  fprintf(stderr, "Modo de empleo: clwall (-h|--help) | (-l | --list) | ...\n"
	  "Gestiona la cartera de tokens de un clauer\n\n"
	  "Las opciones posibles son:\n"
	  "   -h, --help            imprime este mensaje de ayuda\n"
	  "   -l, --list            lista los clauers presentes en el sistema\n"
	  "   -d, --device          selecciona el clauer que queremos listar\n"
	  "   -a, --add             a�ade nuevos tokens a la cartera\n"
	  "   -s, --show            muestra la cartera de tokens\n\n"
	  );

}






void parse ( int argc, char **argv, parse_st *st, char errMsg[256] )
{
  int op;

  *g_device = 0;
  *st       = ST_INI;
  

  for ( op = 1 ; ( op < argc ) && ( *st != ST_ERR ) ; op++ ) {

    
    if ( strcmp("-a", argv[op]) == 0 || strcmp("--add", argv[op]) == 0 )
      *st = ST_ADD;
    else if ( strcmp("-s", argv[op]) == 0 || strcmp("--show", argv[op]) == 0 )
      *st = ST_SHOW;
    else if ( strcmp("-h", argv[op]) == 0 || strcmp("--help", argv[op]) == 0 )
      *st = ST_HELP;
    else if ( strcmp("-l", argv[op]) == 0 || strcmp("--list", argv[op]) == 0 )
      *st = ST_LIST;
    else if ( strcmp("-d", argv[op]) == 0 || strcmp("--add", argv[op]) == 0 ) {
      ++op;
      if ( op < argc ) {
	strncpy(g_device, argv[op], MAX_PATH_LEN);
	g_device[MAX_PATH_LEN] = 0;
      } else {
	*st = ST_ERR;
	strcpy(errMsg, "No se especific� dispositivo a utilizar");
      }
    } else 
      *st = ST_ERR;
  }

  if ( *st == ST_INI )
    *st = ST_SHOW;

}



#define BUF_SIZE    1024

int addTokens ( void )
{
  USBCERTS_HANDLE hClauer;
  unsigned char bloque[TAM_BLOQUE];
  long nb;
  int nDevs,i;
  unsigned char *devs[MAX_DEVICES];
  char passphrase[MAX_PASSPHRASE];

  token_t t;
  FILE *fTokens = NULL;
  char buf[BUF_SIZE];
  char *p;
  unsigned long attr;
  char *end;
  
  
  
  if ( ! *g_device ) {

    if ( LIBRT_ListarDispositivos(&nDevs, devs) != 0 ) {
      fprintf(stderr, "[ERROR] No se pudo enumerar dispositivos\n");
      return 1;
    }    
    if ( nDevs == 0 ) {
      fprintf(stderr, "[ERROR] No hay clauers insertados en el sistema\n");
      return 1;
    } else if ( nDevs > 1 ) {
      fprintf(stderr, "[ERROR] M�s de un clauer insertado en el sistema.\n"
	      "        Utilice opci�n -d\n");
      return 1;
    }
    strcpy(g_device, devs[0]);
    for ( i = 0 ; i < nDevs ; i++ )
      free(devs[i]);

  }


  /* El formato de entrada es:
   *
   * (nombre\natributo\ncondici�n\n(valor)?\n)*
   *
   */

  buf[BUF_SIZE-1] = 0;
  fTokens = stdin;
  while ( ! feof(fTokens) ) {

    /* nombre */

    if ( ! fgets(buf, BUF_SIZE, fTokens) ) {
      fprintf(stderr, "[ERROR] Formato de token incorrecto: %s\n"
	      "        Paramos inserci�n de tokens\n");
      return 1;
    }
    strncpy(t.nombre, buf, 24);
    t.nombre[23] = 0;

    p = strchr(t.nombre, '\r');
    if ( p )
      *p = 0;
    else if ( p = strchr(t.nombre, '\n') )
      *p = 0;

    /* atributo */

    if ( ! fgets(buf, BUF_SIZE, fTokens) ) {
      fprintf(stderr, "[ERROR] Formato de token incorrecto: %s\n"
	      "        Atributo incorrecto\n");
      return 1;
    }
    p = strchr(buf, '\r');
    if ( p )
      *p = 0;
    else if ( p = strchr(buf, '\n') )
      *p = 0;
    attr = strtol(buf, &end, 0);
    if ( *end ) {
      printf("Caracter incorrecto: %c\n", *end);
      fprintf(stderr, "[ERROR] Formato de token incorrecto: %s\n"
	      "        Atributo no v�lido\n");
      return 1;
    }
    t.attr = (unsigned char ) attr;

    /* condici�n */

    if ( fgets(buf, BUF_SIZE, fTokens) ) {

    } else {
      fprintf(stderr, "[ERROR] Formato de token incorrecto: %s\n"
	      "        Paramos inserci�n de tokens\n");
      return 1;
    }
    p = strchr(buf, '\r');
    if ( p )
      *p = 0;
    else if ( p = strchr(t.nombre, '\n') )
      *p = 0;
    strncpy(t.nombre, buf, 24);
    t.nombre[23] = 0;

    /* valor */

    if ( fgets(buf, BUF_SIZE, fTokens) ) {

    } else {
      fprintf(stderr, "[ERROR] Formato de token incorrecto: %s\n"
	      "        Paramos inserci�n de tokens\n");
      return 1;
    }
    p = strchr(buf, '\r');
    if ( p )
      *p = 0;
    else if ( p = strchr(t.nombre, '\n') )
      *p = 0;
    if ( *buf != '\r' && *buf != '\n' ) {
      strncpy(t.cond, buf, 164);
      t.cond[163] = 0;    
    } else
      t.cond[0] = 0;

    /* Cambio de l�nea */

    fgets(buf, BUF_SIZE, fTokens);

  }
  return 0;
}





int  showTokens ( void )
{

  USBCERTS_HANDLE hClauer;
  unsigned char bloque[TAM_BLOQUE];
  long nb;
  int nDevs,i;
  unsigned char *devs[MAX_DEVICES];
  char passphrase[MAX_PASSPHRASE];



  if ( ! *g_device ) {

    if ( LIBRT_ListarDispositivos(&nDevs, devs) != 0 ) {
      fprintf(stderr, "[ERROR] No se pudo enumerar dispositivos\n");
      return 1;
    }    
    if ( nDevs == 0 ) {
      fprintf(stderr, "[ERROR] No hay clauers insertados en el sistema\n");
      return 1;
    } else if ( nDevs > 1 ) {
      fprintf(stderr, "[ERROR] M�s de un clauer insertado en el sistema.\n"
	      "        Utilice opci�n -d\n");
      return 1;
    }
    strcpy(g_device, devs[0]);
    for ( i = 0 ; i < nDevs ; i++ )
      free(devs[i]);

  }


  if ( CLUTILS_AskPassphrase("Contrase�a del clauer: ", passphrase) != 0 ) {
    fprintf(stderr, "[ERROR] Pidiendo contrase�a del clauer\n");
    return 1;
  }


  if ( CLUTILS_ConnectEx2(&hClauer, passphrase, g_device) != 0 ) {
    fprintf(stderr, "[ERROR] No se pudo conectar a dispositivo\n");
    return 1;
  }
  
  if ( LIBRT_LeerTipoBloqueCrypto(&hClauer, BLOQUE_CRYPTO_WALLET, 1, bloque, &nb) != 0 ) {
    fprintf(stderr, "[ERROR] Error leyendo primer bloque de tipo cartera\n");
    LIBRT_FinalizarDispositivo(&hClauer);
    return 1;
  }

  while ( nb != -1 ) {

    CLUTILS_PrintWallet(nb, bloque);
    
    if ( LIBRT_LeerTipoBloqueCrypto(&hClauer, BLOQUE_CRYPTO_WALLET, 0, bloque, &nb) != 0 ) {
      fprintf(stderr, "[ERROR] Error leyendo primer bloque de tipo cartera\n");
      LIBRT_FinalizarDispositivo(&hClauer);
      return 1;
    }
  }

  LIBRT_FinalizarDispositivo(&hClauer);

  return 0;
}






int main ( int argc, char **argv )
{

  parse_st st;
  char errMsg[256];
  int err;

  LIBRT_Ini();

  parse(argc, argv, &st, errMsg);

  if ( st == ST_ERR ) {
    fprintf(stderr, "ERROR: %s\n", errMsg);
    usage();
  } else if ( st == ST_HELP ) {
    usage();
  } else if ( st == ST_LIST ) {
    err= ! CLUTILS_PrintClauers();
  } else if ( st == ST_SHOW ) {
    err= showTokens();
  } else if ( st == ST_ADD ) {
    err= addTokens();
  }

  return err;
}
