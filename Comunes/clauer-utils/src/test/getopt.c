#include<getopt.h>
#include <stdio.h>
#include<string.h>

#include <LIBRT/libRT.h>

enum {OPT_DELETE_BLOCK, OPT_DELETE_CERT, OPT_DEVICE, OPT_HELP, OPT_EXIT};
int num_block=-1, num_cert=-1;
char device[MAX_PATH_LEN];




void usage ( void )
{ 
    fprintf(stderr, "Modo de empleo: cldel [-h] | [-d dispositivo]  ( -b NUM | -c NUM ) \n");
    fprintf(stderr, "Elimina bloques del clauer\n\n");
    fprintf(stderr, "Las opciones posibles son:\n");
    fprintf(stderr, "   -h, --help              Imprime este mensaje de ayuda\n");
    fprintf(stderr, "   -l, --list              Lista los clauers presentes en el sistema\n");
    fprintf(stderr, "   -d, --device  disp      Selecciona el clauer con el que queremos operar\n");
    fprintf(stderr, "   -b, --block NUM         Elimina el bloque n�mero NUM del clauer.\n");
    fprintf(stderr, "   -c, --certificate NUM   Elimina el certificado y sus bloques asociados del clauer.\n\n");
}



int parse( int argc, char ** argv ){
    int c=0, option_index=0, ret;
    static struct option long_options[] = {
	{"block", 1, 0, OPT_DELETE_BLOCK},
	{"certificate", 1, 0, OPT_DELETE_CERT},
	{"device", 1, 0, OPT_DEVICE},
	{"help", 0, 0, OPT_HELP},
	{0, 0, 0, 0}
    };	
    
    opterr= 0;
    if ( argc == 0 ){
	usage();
	return OPT_EXIT;
    }
    while ( (c= getopt_long (argc, argv, "hld:b:c:",
			     long_options, &option_index)) != -1 ){
	switch (c){
	case 'd':
	case OPT_DEVICE:
	    strncpy(device, optarg, MAX_PATH_LEN);
	    break;
	case 'b':
	case OPT_DELETE_BLOCK:
	    if ( num_cert != -1 ){
		fprintf(stderr, "Error: opciones incorrectas, -b no puede ir con -c\n");
		return OPT_EXIT;
	    }
	    num_block= atoi(optarg);
	    ret= OPT_DELETE_BLOCK;
	    break;
	case 'c':
	case OPT_DELETE_CERT:
	    if ( num_block != -1 ){
		fprintf(stderr, "Error: opciones incorrectas, -b no puede ir con -c\n");
		return OPT_EXIT;
	    }
	    num_cert= atoi(optarg);
	    ret= OPT_DELETE_CERT;
	    break;
	default:
	    usage();
	    return OPT_EXIT;
	}  
    }
    
    return ret;
}

int main(int argc, char ** argv){
    
    switch ( parse( argc, argv ) ){
    case OPT_DELETE_BLOCK:
	printf("Hay que borrar bloque= %d\n", num_block);
	break;
    case OPT_DELETE_CERT:
	printf("Hay que borrar el certificado= %d\n", num_cert );
	break;
    case OPT_EXIT:
	break;
    }
    return 0;
}
