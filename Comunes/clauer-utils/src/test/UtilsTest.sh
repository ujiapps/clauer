#!/bin/bash


function check_exit_code {
    if [ $? -eq 1 ]
	then
	echo -e "$error Al hacer $1"
	exit 1
    else 
	echo -e "$ok $1 correcto"
    fi 
}

function check_exec {
    if [ ! -f $1 ]
    then 
	echo -e "$error No encuentro el ejecutable $1"
	exit 1 
    fi
}


verde="\033[1;32m"
rojo="\033[1;31m"
gris="\033[1;30m"
fin_color="\033[0m"
ok="$verde[OK]   $fin_color"
error="$rojo[ERROR]$fin_color"
info="$gris[INFO] $fin_color"
uid=`id -ur` 
RDM=$RANDOM

CLMAKEFS=../clmakefs
CLWBLOCK=../clwblock
CLLS=../clls
CLDEL=../cldel
CLPASSWD=../clpasswd
CLIMPORT=../climport

#Somos root 
if [ $uid != 0 ]
then 
    echo -e "$error Para poder ejecutar este script debes ser root"
    exit 1
fi

#No machacamos ningun fichero
if [ -f /CRYF_000.cla ]
then
    echo -e "$info Detectado CRYF_000.cla en / haciendo backup en /tmp/CRYF_000.cla_$RDM "
    mv /CRYF_000.cla /tmp/CRYF_000.cla_$RDM
fi 

dd if=/dev/zero of=/CRYF_000.cla count=400 bs=10240 >& /dev/null  

echo "---"
#PRUEBA CLMAKEFS
check_exec $CLMAKEFS
$CLMAKEFS -d /CRYF_000.cla -p 123clauer  
check_exit_code $CLMAKEFS

echo "---"
#PRUEBA CLLS
check_exec $CLLS 
$CLLS -d /CRYF_000.cla
check_exit_code $CLLS

echo "---"
#PRUEBA CLWBLOCK
check_exec $CLWBLOCK
$CLWBLOCK -d /CRYF_000.cla -f bloque_cert -p 123clauer >/dev/null
check_exit_code $CLWBLOCK

#Comprobamos que el bloque se ha escrito realmente.
out=`$CLLS -d /CRYF_000.cla -t ALL -p 123clauer   `
if [ X"$out" =  X"0::CERT::CIFRADO::PAUL SANTAPAU NEBOT - NIF:20247075Z::84d9065ee81ebb1e7b978bf7e66688699d692111" ]
then
    echo -e "$ok Salida correcta: $out"
else
    echo -e "$error en la salida de $CLLS:  $out"
fi


echo "---"
#PRUEBA CLDEL
echo -e "$info Borrando bloque 0 ..."
check_exec $CLDEL
$CLDEL -d /CRYF_000.cla -p 123clauer -b 0 -y  >& /dev/null  
check_exit_code $CLDEL

#Comprobamos que el bloque se ha borrado realmente.
out=`$CLLS -d /CRYF_000.cla -t ALL -p 123clauer   `
if [ X"$out" =  X"" ]
then
    echo -e "$ok Bloque borrado correctamente"
else
    echo -e "$error �El bloque no se ha borrado?  salida de $CLLS:  $out"
fi


echo "---"
#PRUEBA CLPASSWD
check_exec $CLPASSWD
echo -e "$info Cambiando password a clauer4ever ..."
$CLPASSWD  -d /CRYF_000.cla  -op 123clauer -np clauer4ever
check_exit_code $CLPASSWD

echo -e "$info Cambiando password a 123clauer ..."
$CLPASSWD  -d /CRYF_000.cla  -op clauer4ever -np 123clauer
check_exit_code $CLPASSWD

$CLLS -d /CRYF_000.cla -t ALL -p 123clauer
check_exit_code $CLLS


echo "---"
#PRUEBA CLIMPORT
check_exec $CLIMPORT
echo -e "$info Importando pkcs12 ..."
$CLIMPORT  -d /CRYF_000.cla  -p 123clauer  -i 1234 -f uactivo951v_firma.p12 >& /dev/null
check_exit_code $CLIMPORT

echo -e "$info Salida de clls: "
$CLLS -d /CRYF_000.cla -t ALL -p 123clauer | sed "s/\(^[0-9][0-9]*\)/        \1/g"
check_exit_code $CLLS


echo "---"
#PRUEBA CLDEL
echo -e "$info Borrando certificado en posici�n 3 ..."
check_exec $CLDEL
$CLDEL -d /CRYF_000.cla -p 123clauer -c 3  -y  >& /dev/null  
check_exit_code $CLDEL

#Comprobamos que el bloque se ha borrado realmente.
out=`$CLLS -d /CRYF_000.cla -t ALL -p 123clauer   `
if [ X"$out" =  X"" ]
then
    echo -e "$ok Certificado borrado correctamente"
else
    echo -e "$error �El certificado no se ha borrado?  salida de $CLLS:  $out"
fi

echo "---"
#FIN Recuperamos el fichero que estaba en / 
if [ -f /tmp/CRYF_000.cla_$RDM ]
then
  echo -e "$info Recuperando fichero /tmp/CRYF_000.cla_$RDM a /CRYF_000.cla"
  mv /tmp/CRYF_000.cla_$RDM /CRYF_000.cla
fi

#TODO
#Hacer esta varias veces y comprobar donde se escriben los bloques.
#clmakefs -d /CRYF_000.cla -p 123clauer && sync && clwblock -d /CRYF_000.cla  -f block  -p 123claue



#echo -e "$ok "
#echo -e  "$error "
