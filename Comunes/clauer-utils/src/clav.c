/* Indica si los clauers insertados tienen el bug
 * de la ausencia de llaves privadas
 *
 * clav == clauer avisador
 */


#include <stdio.h>
#include <stdlib.h>

#include "misc.h"
#include <LIBRT/LIBRT.h>





int main ( int argc, char **argv )
{
  
  USBCERTS_HANDLE hClauer;
  unsigned char bloque[TAM_BLOQUE];
  long nb, nbLlave, cert, llave;
  long *hBloques, *hBloquesLlave;
  unsigned char *bloquesCert, *bloquesLlave;
  char pass[MAX_PASSPHRASE];

  int tieneLlavePEM = 0;

  LIBRT_Ini();
  


  /* Enumero certificados y para cada uno de ellos
   * compruebo si tiene o no llave pem asociada
   */
  
  if ( CLUTILS_AskPassphrase("Contraseņa del Clauer: ", pass) != 0 )
    return 1;
  
  if ( CLUTILS_ConnectEx(&hClauer, pass) != 0 ) 
    return 1; 
  
  if ( LIBRT_LeerTodosBloquesTipo(&hClauer, BLOQUE_CERT_PROPIO, NULL, NULL, &nb) != 0 ) {
    fprintf(stderr, "[ERROR] Enumerando certificados\n");
    return 1;
  }
  
  if ( nb == 0 ) {
    printf("[OK]\n");
    return 0;
  }
  
  bloquesCert = ( unsigned char * ) malloc ( TAM_BLOQUE * nb );
  if ( ! bloquesCert ) {
    fprintf(stderr, "[ERROR] Out of memory\n");
    return 1;
  }
  
  hBloques = ( long *) malloc ( sizeof(long) * nb);
  if ( ! hBloques ) {
    fprintf(stderr, "[ERROR] Out of memory\n");
    return 1;
  }
  
  if ( LIBRT_LeerTodosBloquesTipo(&hClauer, BLOQUE_CERT_PROPIO, hBloques, bloquesCert, &nb) != 0 ) {
    fprintf(stderr, "[ERROR] Enumerando certificados\n");
    return 1;
  }
  
  if ( LIBRT_LeerTodosBloquesTipo(&hClauer, BLOQUE_LLAVE_PRIVADA, NULL, NULL, &nbLlave) != 0 ) {
    fprintf(stderr, "[ERROR] Enumerando certificados\n");
    return 1;
  }
  
  if ( nbLlave == 0 ) {
    printf("[BUG PRESENTE]\n");
    fflush(stdout);
#ifdef WIN32
    Beep(16000, 500);
#endif
    return 0;
  }
  
  bloquesLlave = ( unsigned char * ) malloc ( TAM_BLOQUE * nbLlave );
  if ( ! bloquesLlave ) {
    fprintf(stderr, "[ERROR] Out of memory\n");
    return 1;
  }
  
  hBloquesLlave = ( long *) malloc ( sizeof(long) * nbLlave);
  if ( ! hBloquesLlave ) {
    fprintf(stderr, "[ERROR] Out of memory\n");
    return 1;
  }
  
  if ( LIBRT_LeerTodosBloquesTipo(&hClauer, BLOQUE_CERT_PROPIO, hBloquesLlave, bloquesLlave, &nbLlave) != 0 ) {
    fprintf(stderr, "[ERROR] Enumerando certificados\n");
    return 1;
  }
  
  
  
  for ( cert = 0 ; cert < nb ; cert++ ) {
    tieneLlavePEM = 0;
    for ( llave = 0 ; !tieneLlavePEM && llave < nbLlave ; llave++ ) {
      if ( memcmp(BLOQUE_CERTPROPIO_Get_Id(bloquesCert+cert*TAM_BLOQUE),
		  BLOQUE_LLAVEPRIVADA_Get_Id(bloquesLlave+llave*TAM_BLOQUE),
		  20) == 0 ) 
	{
	  tieneLlavePEM = 1;
	}
    }
    
    if ( ! tieneLlavePEM ) {
      printf("[BUG PRESENTE] Certificado en bloque %ld sin llave PEM asociada\n", hBloques[cert]);
      fflush(stdout);
#ifdef WIN32
      Beep(1000, 300);
#endif
      break;
    }
  }
  
  CLUTILS_Destroy(bloquesLlave, TAM_BLOQUE*nbLlave);
  free(bloquesLlave);
  CLUTILS_Destroy(bloquesCert, TAM_BLOQUE*nb);
  free(bloquesCert);
  free(hBloques);
  free(hBloquesLlave);

  return 0;

}


