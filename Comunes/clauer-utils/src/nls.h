
#ifdef MAC
	#define _(String)  String
	#define N_(String) String
#else

/*  
 * NLS stands for native languaje support 
 * here an initial language setup is done
 * by calling the lang function.
 * 
 */

#ifndef __NLS_UTILS_H__
#define __NLS_UTILS_H__

#include<locale.h>   /* Definition of Locale Variables LC_*  */
#ifdef LINUX
 #include <libintl.h>  /* Function definitions for NLS */
#else 
 #include "libintl.h"  /* Function definitions for NLS */
#endif
#include <stdio.h>
 
#ifdef LINUX
  #include <langinfo.h>
#elif WIN32 
  #include <windows.h>
#endif 


#define _(String) gettext (String)
#define N_(String) gettext_noop (String)

#define PACKAGE "clauer-utils"

#ifdef WIN32 
  #define LOCALE_PATH "c:\\Archivos de programa\\Universitat Jaume I\\Projecte Clauer\\Lang\\"
#elif LINUX 
  #define LOCALE_PATH "/usr/share/locale/"
#endif

int nls_lang_init();

#endif
#endif
