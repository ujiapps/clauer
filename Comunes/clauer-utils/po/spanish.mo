��    �      t    �        	   	          !     .     C  '   ^  1   �     �  #   �  .   �  >   '  $   f  D   �  F   �  B     D   Z  E   �  E   �  !   +  J   M  <   �  5   �  C     A   O  M   �  E   �  N   %  F   t  G   �  C     ^   G  8   �  ;   �  <     <   X  3   �  1   �  6   �  5   2  :   h  ?   �  D   �  C   (  n   l  @   �  n     s   �  r   �  K   r  8   �  C   �  V   ;  B   �     �  '   �  9        Q     n     �      �  %   �     �  <     >   M  6   �  =   �  "      D   $      i      n       q   &   �   3   �      �      	!     !     =!     ]!  !   q!  "   �!     �!     �!     �!      �!     "     )"     G"     W"     c"     ~"     �"  (   �"     �"     �"     #      #     6#     J#     h#     �#     �#  "   �#     �#  4   �#     ,$     E$     S$     c$  ?   j$     �$     �$     �$     �$  %   �$  !   %  +   9%  0   e%     �%     �%  "   �%     �%     �%     �%     &  9   (&  '   b&  ;   �&  F   �&  #   '  0   1'  P   b'     �'  Y   �'  I   (  2   g(     �(  N   �(  .   )  '   7)  0   _)  o   �)  )    *  2   **  6   ]*  Z   �*  !   �*  %   +     7+     T+  (   q+  &   �+  #   �+  #   �+  #   	,     -,  &   I,  4   p,     �,  F   �,  6   -  K   <-  +   �-     �-     �-     �-     .     #.  X   @.  &   �.  Z   �.  I   /  ?   e/  G   �/  $   �/  -   0  $   @0  ,   e0  &   �0  "   �0  #   �0  >    1  (   ?1  .   h1  1   �1  *   �1  3   �1  3   (2  +   \2  &   �2  9   �2  )   �2  &   3  R   :3  H   �3  .   �3  B   4  4   H4  M   }4  -   �4     �4  �   5     �5  a  �5  	   I7     S7     a7     n7     �7  '   �7  5   �7     �7  '   8  1   C8  E   u8  %   �8  E   �8  F   '9  D   n9  F   �9  J   �9  E   E:  *   �:  H   �:  :   �:  :   :;  H   u;  D   �;  K   <  K   O<  Q   �<  O   �<  E   ==  B   �=  U   �=  6   >  9   S>  :   �>  <   �>  7   ?  6   =?  ;   t?  ;   �?  =   �?  C   *@  C   n@  C   �@  �   �@  D   |A  y   �A  }   ;B  }   �B  E   7C  :   }C  P   �C  S   	D  <   ]D     �D  '   �D  6   �D     E     3E  !   RE  #   tE  0   �E      �E  9   �E  ?   $F  5   dF  A   �F  (   �F  K   G     QG     VG  "   YG  (   |G  8   �G     �G     �G     H     -H     MH  #   cH      �H     �H     �H     �H  !   �H     I     I     ;I     QI     `I     |I     �I  -   �I     �I     �I     J      J     8J     QJ     pJ     �J  !   �J  &   �J  &   �J  )   K  "   IK     lK     K     �K  E   �K     �K     �K     L     %L  /   7L  #   gL  5   �L  2   �L     �L     M  &   !M     HM     `M     lM     �M  D   �M  -   �M  A   N  L   XN  $   �N  9   �N  Y   O     ^O  _   nO  E   �O  -   P  %   BP  T   hP  4   �P  .   �P  0   !Q  }   RQ  2   �Q  7   R  8   ;R  a   tR  "   �R  .   �R     (S     FS  5   dS  &   �S  "   �S  "   �S  *   T  $   2T  +   WT  0   �T     �T  >   �T  ;   U  P   MU  0   �U  #   �U     �U  #   V     ,V     HV  h   fV  "   �V  f   �V  K   YW  A   �W  L   �W  &   4X  ;   [X  '   �X  3   �X  *   �X  *   Y  %   IY  A   oY  -   �Y  2   �Y  5   Z  ,   HZ  M   uZ  M   �Z  2   [  ,   D[  D   q[  2   �[  *   �[  f   \  V   {\  5   �\  P   ]  7   Y]  T   �]  6   �]  '   ^  �   E^     _     O   �   %   �      �           N   [   �      �   K   �   }   b               �   �   0      4   1   �       �   �   ?   G          |   �   �   �   A   ]   �   f   �       >       2       B   m   n   q   h   �                              &           �      #   r   �          5   �       Q       S   l   �   �   9   =      `   �           �       *   �   3      �   W   6   �   �   ;   U   �   �   �   �   k   j      @   �      �   o   w   M   �   \       �   /          )   �   g   �   �   	                   a   z               V   -   .       �   e   �          E      �          �   C       y   J          �   !   c   �   ,   �   ^           �                 �   Y   �   �               �   u   {   (              P   �   �      �   t          
   L      i             '           �         �               _   �       Z       �   �   X       R   d       I       H           v   �       x       �   8      �   �   �   $   �       s       :   T   �   7           �   <   �   �      +   �   "   �   �   �       ~   F      �   �   D       p   �    	%22.22s  	%22.22s %ld
 	%22.22s %s
 	Clauer's password:  	Type the password for %s: 	[ERROR] Inserting the certificate: %s
 	[ERROR] Unable to guess the size of the file %s
 	[ERROR] Unable to open %s
 	[ERROR] Unable to open the device
 	[ERROR] Unable to read the information block
 	[ERROR] You have to be permission (admin) to open the device
 
 *** %d more zeroes till here *** 
 
========= KEY CONTAINERS BLOCK type: %02x position: %d ===========
 
========== BLOQUE CERTIFICADO Tipo: %02x posicion: %d ==============
 
========== PRIVATE KEY BLOB type: %02x position: %d ============
 
========== PRIVATE KEY BLOB type: %02x position: %d ==============
 
=============== UNKNOWN BLOCK type: %02x position: %d =============
 
==================================================================

 
MODE for private keys could be:
 
The identifier NUM is the first number than appears when executing clls

     -d, --device            Selects the clauer to be listed
     -h, --help              Prints this help message
     -l, --list              List the clauers plugged on the system
     0  Default mode. Ciphered with the clauer's global password.
     1  Ciphered with a password different than the clauer's global password.
     2  Ciphered with clauer's global password and an extra password.
     3  No ciphering is applied, the private key is stored clear. (INSECURE) 

     If the password is not indicated it will be prompted with getpass
    -b, --block NUM          Deletes the block number NUM from Clauer.

    -c, --CA                  imports a intermediate CA certificate
    -c, --certificate NUM    Deletes the certificate and their associated blocks in the Clauer
    -d,  --device        Selects the clauer to be listed
    -d, --device              Selects the clauer to be used
    -d, --device             Selects the clauer to be listed
    -d, --device DEVICE      Selects the clauer to work with
    -f, --fp12                imports a pkcs12 file
    -h,  --help          Prints this help message
    -h, --help                Prints this help message
    -h, --help               Prints this help message
    -i, --import-password     Password for the pkcs12 file
    -l,  --list          List the clauers plugged on the system
    -l, --list                List the clauers plugged on the system
    -l, --list               List the clauers plugged on the system
    -np, --new-password  Insecure mode of give the password, if not given, the password is asked with getpass.
    -o, --other               imports a other person certificate
    -op, --old-password  Insecure mode of give the password, if not given, the password is asked with getpass.
    -p, --password password   Insecure mode of give the password, if not given, the password is asked with getpass.
    -p, --password password  Insecure mode of give the password, if not given, the password is asked with getpass.
    -r, --raw                Raw content listing mode incompatible with -t.
    -r, --root                imports a root certificate
    -t TYPE, --type TYPE     Lists all the objects of a given type

    -x, --extra-password password  The password that have to be used for modes 1 and 2
    -y, --yes                No ask for confirmation when deleting
    ALL.  All the objects
    CA.   For intermediate certificates
    CERT. For certificates with an associated private key
    CONT. For key containers
    PRIV. For private keys
    ROOT. For root certificates
    TOK.  For the token's wallet
    UNK.  For the unknown type blocks
    WEB.  For web certificates.
   -b, --block block   exports the object in the block block
   -c, --crypto        dumps the whole criptographic partition
   -d, --device        Selects the clauer to be listed
   -l, --list          List the clauers plugged on the system
   -o, --out file      output file
   -p, --p12 block     exports a pkcs#12 with the given block number
 %02x %c -b o --block must be given alone -c o --certfiicate must be given alone -l o --list must be given alone. Invalid option: %s -r is incompatible with -t
 Are you sure (y/n)?  Block %d successfully deleted
 Changes the Clauer's password

 Clauer's password:  Clauers inserted on the system:

 Container %s successfully deleted
 Current Block: Delete blocks from Clauer

 Deleted block:     Elements that are no options: %s Enter to continue:  Exports objects from Clauer

 Format version: Identifier: Importing Certificates...
 Importing PKCS12...
 Imports objects to the Clauer

 Impossible to get certificate's subject
 Intro para continuar:  Invalid option: %s Key Containers List the objects in the Clauer

 M$ Private Key Blob More than one password given
 New Clauer's password:  New password (confirmation):  No block to delete specified No certificate to delete specified No device name specified No more options allowed when -h or --help are given
 No object type specified Options are:
 Own Certificate Owner: Prints the information block content of the specified clauers

 Private Key Repeat the password:  Reserved zone size: Root Certificate TYPE can be one of the next values:

 The block is empty!, exiting ...
 The password is longer than 127 characters
 The passwords do not  match, write them again!.
 The possible options are:
 Total Blocks: Unable to generate random numbers
 Unable to guess it Unknown Unknown object type: %s Unknown option: %s Usage Mode: clexport OPTIONS {block num of certificate}?
 Usage mode: %s -d device [-p password]
 Usage mode: cldel [-h] | [-d device]  ( -b NUM | -c NUM ) 
 Usage mode: climport -h | -l | [-d device] ([-p pkcs12]* [-c cert]*)*
 Usage mode: clls [-h] | (-t TYPE)*
 Usage mode: clpasswd -h | -l | -d (device|file)
 Usage mode: clview (-h|--help) | (-l|--list) | (-d|--device (device|file|ALL))+
 Web Certificate Without arguments, it prints  the information block of all clauers plugged on the system
 You are going to delete all the blocks associated with the certificate:
	 You are going to delete the block that contains:
	 You can only select one device [ATENTION] More than a Clauer plugged on the system.
           Use -d option
 [ATENTION] No Clauers detected on the system.
 [ERROR] %s is not a valid block number
 [ERROR] An error happened changing the password
 [ERROR] An error happened when dumping crypto partition
        The dumped file countains an invalid partition
 [ERROR] Associated private key not found
 [ERROR] Block %ld contains an unexportable object
 [ERROR] Block %ld does not contains a own certificate
 [ERROR] Changing the language to your locale codepage, messages will be printed in English [ERROR] Ciphering the identifier. [ERROR] Closing Clauer's connectiono
 [ERROR] Deleting block = %d
 [ERROR] Deleting block = %d  [ERROR] Device name longer than allowed
 [ERROR] Enumerating blocks of type %s
 [ERROR] Enumerating key Containers
 [ERROR] Enumerating key containers
 [ERROR] Error getting confirmation
 [ERROR] Getting size of %s
 [ERROR] Impossible to finalize device
 [ERROR] Impossible to get the CN of the certificate
 [ERROR] Listing devices
 [ERROR] Looking for the private key associated to the certificate %ld
 [ERROR] Maximum number of exportable objects exceeded
 [ERROR] More than a Clauer plugged on the system.
           Use -d option
 [ERROR] No Clauers detected on the system.
 [ERROR] No memory available 
 [ERROR] Opening %s
 [ERROR] Passwords do not match
 [ERROR] Reading from Clauer
 [ERROR] Reading the file %s
 [ERROR] This password has less future than Chewaka on a depilation challenge. Try again
 [ERROR] Unable to build up the pkcs12
 [ERROR] Unable to connect with the Clauer %s
        Correct device? Try to use -l option
 [ERROR] Unable to connect with the Clauer %s
        Incorrect password?
 [ERROR] Unable to connect with the Clauer. Incorrect password?
 [ERROR] Unable to delete the associated key entry on the key container
 [ERROR] Unable to enumerate objects
 [ERROR] Unable to get Clauers on the system.
 [ERROR] Unable to get the block %ld
 [ERROR] Unable to get the information block
 [ERROR] Unable to get the new password [ERROR] Unable to get the password [ERROR] Unable to get the password
 [ERROR] Unable to import the certificate, incorrect password?
 [ERROR] Unable to initialize the device
 [ERROR] Unable to open/create the output file
 [ERROR] Unable to open/create the output file %s
 [ERROR] Unable to read the specified block [ERROR] Unable to unlock newPass memory errno= %d.
 [ERROR] Unable to unlock oldPass memory errno= %d.
 [ERROR] Unable to write in the output file
 [ERROR] Unable to write on the device
 [ERROR] Unknown block type, Really is it a Certificate? 
 [ERROR] Unknown error opening the device
 [ERROR] Writing changes to the Clauer
 [ERROR] You have to had permission (admin) to open the device to dump crypto zone
 [WARNING] Maximum number of certificates exceeded. Next will be ignored
 [WARNING] Maximum number of devices execeeded
 [WARNING] Maximum number of pkcs12 exceeded. Next will be ignored
 [WARNING] More than a Clauer plugged on the system.
 [WARNING] More than a Clauer plugged on the system.
           Use -d option
 [WARNING] No Clauers detected on the system.
 [WARNING] Unknown block type
 [WARNING] You have given more than a pkcs12 file with --import-password option or -i
           the password for all the pkcs12 files must be the same if this option is indicated
. pkcs12's password for %s:  Project-Id-Version: clauer-utils 0.1
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2006-10-11 19:35+0200
PO-Revision-Date: 2006-08-18 13:03+0200
Last-Translator: paul <psantapau@sg.uji.es>
Language-Team: Spanish <es@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
 	%22.22s  	%22.22s %ld
 	%22.22s %s
 	Password del Clauer 	Escribe el password para %s: 	[ERROR] Insertando el certificado: %s
 	[ERROR] Imposible obtener el tamaño del fichero %s
 	[ERROR] No puedo abrir %s
 	[ERROR] No puedo abrir el dispositivo
 	[ERROR] No puedo leer el bloque de información
 	[ERROR] Tienes que tener permisos (admin) para abrir el dispositivo
 
 *** %d ceros más hasta aquí *** 
 
========= BLOQUE KEY CONTAINERS tipo: %02x posicion: %d ===========
 
========== BLOQUE CERTIFICADO Tipo: %02x posicion: %d ==============
 
========== BLOB LLAVE PRIVADA tipo: %02x posicion: %d ============
 
========== BLOB LLAVE PRIVADA tipo: %02x posicion: %d ==============
 
=============== BLOQUE DESCONOCIDO tipo: %02x posicion: %d =============
 
==================================================================

 
MODO para las llaves privadas puede ser:
 
El identificador NUM es el primer numero que aparace al ejecutar clls

     -d, --device            Selecciona el clauer a listar
     -h, --help              Imprime este mensaje de ayuda
     -l, --list              Lista los caluaers insertados en el sistema
     0  Modo por defecto. Cifrado con la password global del clauer.
     1  Cifrado con una password diferente a la password global del clauer.
     2  Cifrado con la password global del clauer y con una password extra.
     3  No se aplica cifrado, la llave privada se almacena en claro. (INSEGURO) 

     Si no se indica el password, será solicitado mediante la función getpass
    -b, --block NUM          Borra el bloque número NUM del Clauer.

    -c, --CA                  importa un certificado CA intermedio
    -c, --certificate NUM    Borra el certificado y sus bloquesasociados en el Clauer
    -d,  --device        Selecciona el Clauer a listar
    -d, --device              Selecciona el clauer a usar
    -d, --device             Selecciona el clauer a listar
    -d, --device DEVICE      Selecciona el clauer a utilizar
    -f, --fp12                importa un fichero pkcs12
    -h,  --help          Imprime este mensaje de ayuda
    -h, --help                Imprime este mensaje de ayuda
    -h, --help                Imprime este mensaje de ayuda
    -i, --import-password     Password para el fichero pkcs12
    -l,  --list          Lista los clauers conectados a la máquina
    -l,  --list          Lista los clauers conectados a la máquina
    -l,  --list          Lista los clauers conectados a la máquina
    -np, --new-password  Modo inseguro de indicar la password, si no se indica, el password se solicita mediante la función getpass.
    -o, --other               importa un certificado de otra persona
    -op, --old-password  Modo inseguro de indicar la password, si no se indica, el password se solicita mediante getpass.
    -p, --password password   Modo inseguro de indicar la password, si no se indica la password se solicita mediante getpass.
    -p, --password password   Modo inseguro de indicar la password, si no se indica la password se solicita mediante getpass.
    -r, --raw                Modo de listado Raw incompatible con -t.
    -r, --root                importa un certificado raíz
    -t TYPE, --type TYPE     Lista todos los objetos del tipo indicado con TYPE

    -x, --extra-password password  El password que debe usarse para los modos 1 y 2
    -y, --yes                No pide confirmaicón al borrar
    ALL.  Todos los objetos
    CA.   Para certificados intermedios
    CERT. Para certificados con llave privada asociada
    CONT. Para key containers
    PRIV. Para llaves privadas
    ROOT. Para certificados raíz
    TOK.  Para la cartera de tokens
    UNK.  Para los tipos de bloques desconocidos
    WEB.  Para certificados web.
   -b, --block block   exporta el objeto del bloque block
   -c, --crypto        vuelca toda la partición criptográfica
   -d, --device        Selecciona ael clauer a listar
   -l, --list          Lista los clauers conectados en el sistema
   -o, --out file      fichero de salida
   -p, --p12 block     exporta un pkcs#12 con el número de bloque indicado
 %02x %c -b o --block deben indicarse solas -c o --certfiicate deben indicarse solas -l o --list deben indicarse solas. Opción inválida: %s -r es incompatible con -t
 ¿Esta seguro (y/n)?  Bloque %d borrado con éxito
 Cambia el password del Clauer

 Password del Clauer:  Clauers insertados en el sistema:

 Container %s borrado con éxito
 Bloque actual: Borra bloque del Clauer

 Borrado bloque:     Elementos que no son opciones: %s Intro para continuar:  Exporta objetos del Clauer

 Versión del formato: Identificador: Importando certificados...
 Importando PKCS12...
 Importa objetos al Clauer

 Imposible obtener el subject del certificado
 Intro para continuar: Opción inválida: %s Key Containers Lista los objetos en el Clauer

 M$ Blob de llave privada Se indico más de un password
 Nueva password para el Clauer:  Nueva password (confirmación) No se especificó bloque a borrar No se especificó certificado a borrar No se especifico nombre de dispositivo No se admiten opciones junto -h o --help
 No se especifico el tipo de objeto Las opciones son:
 Certificado propio Propietario: Imprime el contenido del bloque de información del Clauer indicado

 Llave privada Repite el password:  Tamaño de la zona reservada Certificado Raíz TIPO puede ser uno de los siguientes valores:

 El bloque está vacio, saliendo...
 La longitud del password es mayor que 127 caracteres
 Los passwords no coindiden, vuelva a teclearlos!.
 Las opciones posibles son:
 Bloques totales: Imposible generar números aleatorios
 No se ha podido obtener Desconocido Tipo de bloque desconocido: %s Opción desconocida: %s Modo de uso: clexport OPCIONES {número de bloque del certificado}?
 Modo de uso: %s -d dispositivo [-p password]
 Modo de uso: cldel [-h] | [-d dispositivo]  ( -b NUM | -c NUM ) 
 Modo de uso: climport -h | -l | [-d dispositivo] ([-p pkcs12]* [-c cert]*)*
 Modo de uso: clls [-h] | (-t TIPO)*
 Modo de uso: clpasswd -h | -l | -d (dispositivo|fichero)
 Modo de uso: clview (-h|--help) | (-l|--list) | (-d|--device (disopsitivo|fichero|ALL))+
 Certificado Web Sin argumentos, imprime el bloque de información de todos los clauers conectadosen el sistema
 Se dispone a borrar todos los bloques asociados con el certificado:
	 Se dispone a borrar el bloque que contiene:
	 Solo puede seleccionar un dispositivo [ATENCIÓN] Más de un clauer conectado en el sistema.
           Use la opción -d
 [ATENCIÓN] No se detectaron Clauers en el sistema.
 [ERROR] %s no es un número de bloque válido
 [ERROR] Ocurrió un error cambiando la password
 [ERROR] Ocurrió un error volcando la partición criptográfica
        el fichero volcado contiene una partición inválida
 [ERROR] No se encontró la llave privada asociada
 [ERROR] El bloque %ld contiene un bloque no exportable
 [ERROR] El bloque %ld no contiene un certificado propio
 [ERROR] Cambiando el lenguaje a su código de páginas local, los mensajes aparecerán en Inglés [ERROR] Cifrando el identificador. [ERROR] Terminando la conexión con el Clauer
 [ERROR] Borrando bloque = %d
 [ERROR] Borrando bloque = %d  [ERROR] Nombre del dispositivo mayor de lo permitido
 [ERROR] Enumerando bloques de typo %s
 [ERROR] Enumerando key Containers
 [ERROR] Enumerando key containers
 [ERROR] Error obteniendo la confirmación
 [ERROR] Obteniendo el tamaño de %s
 [ERROR] Imposible finalizar el dispositivo
 [ERROR] Imposible obtener el CN del certificado
 [ERROR] Listado dispositivos
 [ERROR] Buscando la llave privada asociada al certificado %ld
 [ERROR] Excedido el máximo número de objetos exportables
 [ERROR] Más de un Clauer conectado en el sistema.
           Use la opción -d
 [ERROR] No se detectaron Clauers en el sistema.
 [ERROR] No hay memoria disponible 
 [ERROR] Abrinedo %s
 [ERROR] Los passwords no coinciden
 [ERROR] Leyendo del Clauer
 [ERROR] Leendo el fichero %s
 [ERROR] Este password tiene menos futuro que Chewaka en un concurson de depilación. Intentalo de nuevo
 [ERROR] Imposible crear el pkcs12
 [ERROR] Imposible conectar con el Clauer %s
        ¿dipositivo correcto? Intenta usar la opción -l
 [ERROR] Imposible conectar con el Clauer %s
        ¿password incorrecto?
 [ERROR] Imposible conectar con el Clauer. ¿Password incorrecto?
 [ERROR] No puedo obtener la entrada asociada a la llave en el key container
 [ERROR] No puedo enumerar los objetos
 [ERROR] No puedo obtener los Clauer conectados al sistema.
 [ERROR] No puedo obtener el bloque %ld
 [ERROR] No puedo obtener el bloque de información
 [ERROR] No puedo obtener la nueva password [ERROR] No puedo obtener la nueva password [ERROR] No puedo obtener la password
 [ERROR] No puedo importar el certificado, ¿password incorrecta?
 [ERROR] Imposible inicializar el dispositivo
 [ERROR] No puedo abrir/crear el fichero de salida
 [ERROR] no puedo abrir/crear el fichero de salida %s
 [ERROR] No puedo leer el bloque especificado [ERROR] no puedo desbloquear la memoria correspondiente a newPass errno= %d.
 [ERROR] No puedo desbloquear la memoria correspondiente a oldPass errno= %d.
 [ERROR] No puedo escribir en el fichero de salida
 [ERROR] No puedo escribir en el dispositivo
 [ERROR] Tipo de bloque desconocido, ¿Es realmente un certificado? 
 [ERROR] Error desconocido abriendo el dispositivo
 [ERROR] Escribiendo los cambios al Clauer
 [ERROR] Tienes que tener permiso (admin) para abrir el dispositivo para volcar la zona criptográfica
 [ATENCIÓN] Excedido número máximo de certificados. Los siguientes serán ignorados
 [ATENCIÓN] Número máximo de dispositivos superado
 [ATENCIÓN] Número máximo de pkcs12 superado. Los siguientes serán ignorados
 [ATENCIÓN] Más de un Clauer conectado en el sistema.
 [ATENCIÓN] Más de un Clauer conectado en el sistema.
           Use la opción -d
 [ATENCIÓN] No se de detectaron Clauer en el sistema.
 [ATENCIÓN] Tipo de bloque desconocido
 [ATENCIÓN] Ha indicado más de un fichero pkcs12 con la opción --import-password o -i
           el password para todos los ficheros pkcs12 debe ser el mismo si se indica esta opción
. Password del pkcs12 para %s:  