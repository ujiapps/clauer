��    �      �  �   �      x  	   y     �     �     �     �  '   �  1   �     (  #   D  .   h  >   �  J   �  <   !  5   ^  C   �  F   �  G     C   g  ^   �  8   
  ;   C  <     <   �  3   �  1   -  6   _  5   �  :   �  ?     D   G  C   �  n   �  @   ?  n   �  s   �  r   c  8   �  C     B   S     �  '   �  9   �          /     J      j     �  <   �  >   �  6   '  /   ^  =   �  "   �  D   �      4  &   U  3   |     �     �     �       !     "   :     ]     l     �      �     �     �     �     �          %  (   E     n     �      �     �     �     �             "   =     `  4   y     �     �     �     �  ?   �     ,     8     N     b  %   s  !   �  +   �  0   �           3   "   A      d      w            �   9   �   '   �   ;   !  F   H!  #   �!  0   �!  P   �!     5"  Y   E"  I   �"  2   �"     #  N   ;#  .   �#  '   �#  0   �#  o   $  )   �$  2   �$  6   �$  !   %  %   8%     ^%     {%  (   �%  &   �%  #   �%  #   &  #   0&     T&  &   p&  4   �&     �&  F   �&  6   ,'  K   c'  +   �'     �'     �'     (     -(     J(  X   g(  &   �(  Z   �(  I   B)  ?   �)  G   �)  $   *  -   9*  $   g*  ,   �*  &   �*  "   �*  #   +  >   '+  (   f+  .   �+  1   �+  *   �+  3   ,  3   O,  +   �,  &   �,  9   �,  )   -  &   :-  R   a-  H   �-  .   �-  B   ,.  4   o.  M   �.  -   �.      /  �   >/     �/  a  0  	   p1     z1     �1     �1      �1  $   �1  9   �1     *2  $   D2  -   i2  B   �2  I   �2  ;   $3  ;   `3  G   �3  T   �3  F   94  ?   �4  X   �4  8   5  9   R5  ;   �5  =   �5  6   6  7   =6  <   u6  <   �6  ;   �6  D   +7  D   p7  D   �7  �   �7  G   �8  ~   �8  �   K9  �   �9  9   Q:  Q   �:  @   �:     ;  &   :;  6   a;     �;     �;  !   �;  $   �;      <  7   ><  =   v<  5   �<  8   �<  B   #=  &   f=  G   �=  #   �=  )   �=  8   #>     \>     p>     �>     �>  !   �>  $   �>     ?     ?     3?      F?     g?     �?     �?     �?     �?     �?  .   �?     #@     8@  "   G@     j@      �@     �@  )   �@  ,   �@  1   A  "   JA  8   mA  '   �A     �A     �A     �A  J   �A     IB     WB     nB     �B  (   �B      �B  (   �B  8   C     HC     dC  (   rC     �C  
   �C     �C     �C  9   �C  '   ,D  ;   TD  F   �D  #   �D  0   �D  P   ,E     }E  ]   �E  B   �E  .   -F  %   \F  Q   �F  1   �F  -   G  3   4G  r   hG  0   �G  5   H  3   BH  $   vH  /   �H     �H     �H  -   I  $   3I  !   XI  !   zI     �I     �I  )   �I  1   J     6J  @   TJ  4   �J  L   �J  .   K  &   FK     mK  #   �K     �K     �K  p   �K  '   PL  c   xL  K   �L  @   (M  B   iM  %   �M  5   �M  (   N  4   1N  ,   fN  '   �N  (   �N  @   �N  /   %O  2   UO  5   �O  -   �O  4   �O  4   !P  /   VP  -   �P  A   �P  -   �P  #   $Q  ^   HQ  N   �Q  0   �Q  I   'R  3   qR  R   �R  1   �R  %   *S  �   PS     T                &              �   �                %       *      g   $       �   �          W   �       �              x   �       I   5       H   �   �   '   R   Y         <   �          3   �   +       Q   s          n   \   {   �      :   z           �   �           �   v      �   L   �   6   �       F   /          q   
   �      �      w              O       t   [   "       }   9   J   �       �   h   Z   4   >   �   G       �         A          E       j   -       P          �       �   �   ?   �           �       �      �   b   ]   D          �   �                  |   @       �   �   ,          `   f   0   d   �       N       k   T   U       �   �       �       p           �       y       C   �   e   V   o   �   c       �       .   �   K       M   �   #   �   (   �   8   a   7   	   )   �   !              ^   �       �       ;   �       2          X   _           i       �               �      �   B   m   �   r   S           ~       l   1   �   u   =    	%22.22s  	%22.22s %ld
 	%22.22s %s
 	Clauer's password:  	Type the password for %s: 	[ERROR] Inserting the certificate: %s
 	[ERROR] Unable to guess the size of the file %s
 	[ERROR] Unable to open %s
 	[ERROR] Unable to open the device
 	[ERROR] Unable to read the information block
 	[ERROR] You have to be permission (admin) to open the device
 
The identifier NUM is the first number than appears when executing clls

     -d, --device            Selects the clauer to be listed
     -h, --help              Prints this help message
     -l, --list              List the clauers plugged on the system
     If the password is not indicated it will be prompted with getpass
    -b, --block NUM          Deletes the block number NUM from Clauer.

    -c, --CA                  imports a intermediate CA certificate
    -c, --certificate NUM    Deletes the certificate and their associated blocks in the Clauer
    -d,  --device        Selects the clauer to be listed
    -d, --device              Selects the clauer to be used
    -d, --device             Selects the clauer to be listed
    -d, --device DEVICE      Selects the clauer to work with
    -f, --fp12                imports a pkcs12 file
    -h,  --help          Prints this help message
    -h, --help                Prints this help message
    -h, --help               Prints this help message
    -i, --import-password     Password for the pkcs12 file
    -l,  --list          List the clauers plugged on the system
    -l, --list                List the clauers plugged on the system
    -l, --list               List the clauers plugged on the system
    -np, --new-password  Insecure mode of give the password, if not given, the password is asked with getpass.
    -o, --other               imports a other person certificate
    -op, --old-password  Insecure mode of give the password, if not given, the password is asked with getpass.
    -p, --password password   Insecure mode of give the password, if not given, the password is asked with getpass.
    -p, --password password  Insecure mode of give the password, if not given, the password is asked with getpass.
    -r, --root                imports a root certificate
    -t TYPE, --type TYPE     Lists all the objects of a given type

    -y, --yes                No ask for confirmation when deleting
    ALL.  All the objects
    CA.   For intermediate certificates
    CERT. For certificates with an associated private key
    CONT. For key containers
    PRIV. For private keys
    ROOT. For root certificates
    TOK.  For the token's wallet
    WEB.  For web certificates.
   -b, --block block   exports the object in the block block
   -c, --crypto        dumps the whole criptographic partition
   -d, --device        Selects the clauer to be listed
   -h, --help          Prints this help message
   -l, --list          List the clauers plugged on the system
   -o, --out file      output file
   -p, --p12 block     exports a pkcs#12 with the given block number
 -b o --block must be given alone -c o --certfiicate must be given alone -l o --list must be given alone. Invalid option: %s Are you sure (y/n)?  Block %d successfully deleted
 Changes the Clauer's password

 Clauer's password:  Clauers inserted on the system:

 Container %s successfully deleted
 Current Block: Delete blocks from Clauer

 Deleted block:     Elements that are no options: %s Exports objcts from Clauer

 Format version: Identifier: Importing Certificates...
 Importing PKCS12...
 Imports objects to the Clauer

 Impossible to get certificate's subject
 Invalid option: %s Key Containers List the objects in the Clauer

 Microsoft Private Key Blob More than one password given
 New Clauer's password:  New password (confirmation):  No block to delete specified No certificate to delete specified No device name specified No more options allowed when -h or --help are given
 No object type specified Options are:
 Own Certificate Owner: Prints the information block content of the specified clauers

 Private Key Repeat the password:  Reserved zone size: Root Certificate TYPE can be one of the next values:

 The block is empty!, exiting ...
 The password is longer than 127 characters
 The passwords do not  match, write them again!.
 The possible options are:
 Total Blocks: Unable to generate random numbers
 Unable to guess it Unknown Unknown object type: %s Unknown option: %s Usage Mode: clexport OPTIONS {block num of certificate}?
 Usage mode: %s -d device [-p password]
 Usage mode: cldel [-h] | [-d device]  ( -b NUM | -c NUM ) 
 Usage mode: climport -h | -l | [-d device] ([-p pkcs12]* [-c cert]*)*
 Usage mode: clls [-h] | (-t TYPE)*
 Usage mode: clpasswd -h | -l | -d (device|file)
 Usage mode: clview (-h|--help) | (-l|--list) | (-d|--device (device|file|ALL))+
 Web Certificate Without arguments, it prints  the information block of all clauers plugged on the system
 You are going to delete all the blocks associated with the certificate:
	 You are going to delete the block that contains:
	 You can only select one device [ATENTION] More than a Clauer plugged on the system.
           Use -d option
 [ATENTION] No Clauers detected on the system.
 [ERROR] %s is not a valid block number
 [ERROR] An error happened changing the password
 [ERROR] An error happened when dumping crypto partition
        The dumped file countains an invalid partition
 [ERROR] Associated private key not found
 [ERROR] Block %ld contains an unexportable object
 [ERROR] Block %ld does not contains a own certificate
 [ERROR] Ciphering the identifier. [ERROR] Closing Clauer's connectiono
 [ERROR] Deleting block = %d
 [ERROR] Deleting block = %d  [ERROR] Device name longer than allowed
 [ERROR] Enumerating blocks of type %s
 [ERROR] Enumerating key Containers
 [ERROR] Enumerating key containers
 [ERROR] Error getting confirmation
 [ERROR] Getting size of %s
 [ERROR] Impossible to finalize device
 [ERROR] Impossible to get the CN of the certificate
 [ERROR] Listing devices
 [ERROR] Looking for the private key associated to the certificate %ld
 [ERROR] Maximum number of exportable objects exceeded
 [ERROR] More than a Clauer plugged on the system.
           Use -d option
 [ERROR] No Clauers detected on the system.
 [ERROR] No memory available 
 [ERROR] Opening %s
 [ERROR] Passwords do not match
 [ERROR] Reading from Clauer
 [ERROR] Reading the file %s
 [ERROR] This password has less future than Chewaka on a depilation challenge. Try again
 [ERROR] Unable to build up the pkcs12
 [ERROR] Unable to connect with the Clauer %s
        Correct device? Try to use -l option
 [ERROR] Unable to connect with the Clauer %s
        Incorrect password?
 [ERROR] Unable to connect with the Clauer. Incorrect password?
 [ERROR] Unable to delete the associated key entry on the key container
 [ERROR] Unable to enumerate objects
 [ERROR] Unable to get Clauers on the system.
 [ERROR] Unable to get the block %ld
 [ERROR] Unable to get the information block
 [ERROR] Unable to get the new password [ERROR] Unable to get the password [ERROR] Unable to get the password
 [ERROR] Unable to import the certificate, incorrect password?
 [ERROR] Unable to initialize the device
 [ERROR] Unable to open/create the output file
 [ERROR] Unable to open/create the output file %s
 [ERROR] Unable to read the specified block [ERROR] Unable to unlock newPass memory errno= %d.
 [ERROR] Unable to unlock oldPass memory errno= %d.
 [ERROR] Unable to write in the output file
 [ERROR] Unable to write on the device
 [ERROR] Unknown block type, Really is it a Certificate? 
 [ERROR] Unknown error opening the device
 [ERROR] Writing changes to the Clauer
 [ERROR] You have to had permission (admin) to open the device to dump crypto zone
 [WARNING] Maximum number of certificates exceeded. Next will be ignored
 [WARNING] Maximum number of devices execeeded
 [WARNING] Maximum number of pkcs12 exceeded. Next will be ignored
 [WARNING] More than a Clauer plugged on the system.
 [WARNING] More than a Clauer plugged on the system.
           Use -d option
 [WARNING] No Clauers detected on the system.
 [WARNING] Unknown block type
 [WARNING] You have given more than a pkcs12 file with --import-password option or -i
           the password for all the pkcs12 files must be the same if this option is indicated
. pkcs12's password for %s:  Project-Id-Version: clauer-utils 0.1
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2006-08-18 12:58+0200
PO-Revision-Date: 2006-08-18 13:05+0200
Last-Translator: paul <psantapau@sg.uji.es>
Language-Team: Spanish <es@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
 	%22.22s  	%22.22s %ld
 	%22.22s %s
 	Password del Clauer 	Escribeix la password per a %s: 	[ERROR] Inserint el certificat: %s
 	[ERROR] Impossible obtindre la grandària del fitxer %s
 	[ERROR] No puc obrir %s
 	[ERROR] No puc obrir el dispositiu
 	[ERROR] No puc llegir el bloc d'informació
 	[ERROR] Has de tindre permisos (admin) per a obrir el dispositiu
 
L'identificador NUM és el primer nombre que apareix a l'executar clls

     -d, --device            Selecciona el clauer a llistar
     -h, --help              Imprimix este missatge d'ajuda
     -l, --list              Llista els clauers inserits en el sistema 
     Si no s'indica el password, serà sol·licitat per mitjà de la funció getpass
    -b, --block NÚM          Esborra el bloc nombre NÚM del Clauer.

    -c, --CA                  importa un certificat CA intermig
    -c, --certificat NUM    Esborra el certificat i els seus blocsassociats en el Clauer
    -d,  --device        Selecciona el Clauer a llistar 
    -d, --device              Selecciona el clauer a usar
    -d, --device             Selecciona el clauer a llistar
    -d, --device DEVICE      Selecciona el clauer a utilitzar
    -f, --fp12                importa un fitxer pkcs12
    -h,  --help          Imprimix este missatge d'ajuda
    -h, --help                Imprimix este missatge d'ajuda
    -h, --help                Imprimix este missatge d'ajuda
    -i, --import-password     Password per al fitxer pkcs12
    -l,  --list          Llista els clauers connectats a la màquina
    -l,  --list          Llista els clauers connectats a la máquina
    -l,  --list          Llista els clauers connectats a la máquina
    -np, --new-password  Mode insegur d'indicar la password, si no s'indica, el password se sol·licita per mitjà de la funció getpass.
    -o, --other               importa un certificat d'una altra persona
    -op, --old-password  Mode insegur d'indicar la password, si no s'indica, el password se sol·licita per mitjà de getpass.
    -p, --password password   Mode insegur d'indicar la password, si no s'indica la password se sol·licita per mitjà de getpass.
    -p, --password password   Mode insegur d'indicar la password, si no s'indica la password se sol·licita per mitjà de getpass.
    -r, --root                importa un certificat arrel
    -t TYPE, --type TYPE     Llista tots els objectes del tipus indicat amb TYPE

    -y, --yes                No demana confirmació a l'esborrar
    ALL.  Tots els objectes
    CA.   Per a certificats intermedis
    CERT. Per a certificats amb clau privada associada
    CONT. Per a key containers
    PRIV. Per a claus privades
    ROOT. Per a certificats arrel
    TOK.  Per a la cartera de tokens
    WEB.  Per a certificats web.
   -b, --block block   exporta l'objecte del bloc block
   -c, --crypto        bolca tota la partició criptogràfica
   -d, --device        Selecciona el clauer a llistar
   -h, --help          Imprimeix aquest missatge d'ajuda
   -l, --list          Llista els clauers connectats en el sistema
   -o, --out file      fitxer d'eixida
   -p, --p12 block     exporta un pkcs#12 amb el nombre de bloc indicat
 -b o --block han d'indicar-se soles -c o --certfiicate han d'indicar-se soles -l o --list han d'indicar-se soles. Opció invàlida: %s Està segur (y/n)?  Bloc %d esborrament amb èxit
 Canvia el password del Clauer

 Password del Clauer:  Clauers inserits en el sistema:

 Contenidor %s esborrament amb èxit
 Bloc actual: Esborra bloc del Clauer

 Esborrat bloc:     Elements que no són opcions: %s Exporta objectes del Clauer

 Versió del format: Identificador: Important certificats...
 Important PKCS12...
 Importa objectes al Clauer

 Impossible obtindre el subject del certificat
 Opció invàlida: %s Key Containers Llista els objectes en el Clauer

 Microsoft Private Key Blob S'ha indicat més d'un password
 Nova password del Clauer:  Nova password del Clauer (confirmació):  No s'ha especificat cap block per a esborrar No s'ha especificat cap certificat per a esborrar No s'ha especificat cap dispositiu -h o --help no poden anar acompanyades d'altres opcions
 No s'ha especificat cap tipus d'objecte Les opcions són:
 Certificat propi Propietari: Imprimeix el contingut del block d'informació dels clauers especificats

 Claue Privada Repeteix la password:  Tamany de la zona reservada: Certificat Arrel TYPE pot ser un dels valors següents:

 El block està buit. Eixint ...
 La password es major que 127 caràcters
 Les passwords no concorden. Escriu-les una altra vegada
 Les opcions possibles son:
 Total Blocks: No s'ha pogut generar nombres aleatoris
 Impossible esbrinar-lo Desconegut Tipus d'objecte desconegut: %s Opció incorrecta: %s Mode d'ús: clexport OPTIONS {block num of certificate}?
 Mode d'ús: %s -d device [-p password]
 Mode d'ús: cldel [-h] | [-d device]  ( -b NUM | -c NUM ) 
 Mode d'ús: climport -h | -l | [-d device] ([-p pkcs12]* [-c cert]*)*
 Mode d'ús: clls [-h] | (-t TYPE)*
 Mode d'ús: clpasswd -h | -l | -d (device|file)
 Mode d'ús: clview (-h|--help) | (-l|--list) | (-d|--device (device|file|ALL))+
 Certificat Web Sense argument, imprimeix el block d'informació de tots els clauers insertats en el sistema
 Estàs apunt d'esborrar tots els blocks associats al certificat:
	 Estàs apunt d'esborrar el block que conté:
	 Només pots seleccionar un dispositiu [ATENCIÓ] Més d'un Clauer insertat al sistema.
          Utilitze la opció -d
 [ATENCIÓ] No s'han detectat clauers al sistema.
 [ERROR] %s no és un número de block vàlid
 [ERROR] Un error ha ocorregut canviant la password
 [ERROR] Un error ha ocorregut volcant la patició criptogràfica
        El fitxer conté una partició invàlida
 [ERROR] No s'ha robat la clau privada associada
 [ERROR] El block %ld conté un objecte no exportable
 [ERROR] El block %ld no conté un certificat propi
 [ERROR] Error xifrant identificador. [ERROR] Error tancant la connexió del Clauer.
 [ERROR] Esborrant bloc = %d
 [ERROR] Esborrant bloc = %d  [ERROR] El nom del dispositiu és massa gran
 [ERROR] Enumerant block de tipus %s
 [ERROR] Enumerant key containers
 [ERROR] Enumerant key containers
 [ERROR] Obtenint confirmació
 [ERROR] Obtenint tamany de %s
 [ERROR] Impossible finalitzar dispositiu
 [ERROR] Impossible obtindre el CN del certificat
 [ERROR] Llistant dispositius
 [ERROR] Buscant la clau privada asossiada amd el certificat %ld
 [ERROR] Nombre màxim d'objectes exportables exedit
 [ERROR] Més d'un Clauer insertat al sistema.
        Utilitze la opció -d
 [ERROR] No s'han detectat clauers al sistema.
 [ERROR] No hi ha memòria disponible 
 [ERROR] Obrint %s
 [ERROR] Les passwords no concorden
 [ERROR] Llegint del Clauer
 [ERROR] Llegint del fitxer %s
 [ERROR] Aquesta password password té menys futur que el Chewaka en un concurs de depilació. Intenta-ho de nou
 [ERROR] Impossible construir el pkcs12
 [ERROR] Imposible conectar amb el Clauer %s
        Dispositiu correcte? Prova a usar la opció -l
 [ERROR] Impossible connectar amb el Clauer %s
        Password incorrecta?
 [ERROR] Impossible conectar amb el Clauer. Password incorrecta?
 [ERROR] Impossible esborrar l'entrada de la clau al key container
 [ERROR] Impossible enumerar objectes
 [ERROR] Impossible obtindre els Clauers del sistema.
 [ERROR] Impossible obtindre el bloc %ld
 [ERROR] Impossible obtindre la informació del bloc
 [ERROR] Impossible obtindre la nova password [ERROR] Impossible obtindre la password [ERROR] Impossible obtindre la password
 [ERROR] Impossible importar el certificat. Password incorrecta?
 [ERROR] Imopossible inicialitzar el dispositiu
 [ERROR] Impossible obrir/crear el fitxer d'eixida
 [ERROR] Impossible obrir/crear el fitxer d'eixida %s
 [ERROR] Impossible llegir el bloc especificat [ERROR] Impossible desbloquejar memòria errno= %d.
 [ERROR] Impossible desbloquejar memòria errno= %d.
 [ERROR] Impossible escriure al fitxer d'eixida
 [ERROR] Impossible escriure en el dispositiu
 [ERROR] Tipus de block desconegut. Segur que és un certificat? 
 [ERROR] Error deconegut obrint el dispositiu
 [ERROR] Escribint canvis al Clauer
 [ERROR] Tens que tindre permissos (admin) per a poder fer el volcar de la zona criptogràfica
 [ATENCIÓ] Nombre màxim de certificats excedit. Els següents seran ignorats
 [ATENCIÓ] Màxim nombre de dispositius excedit
 [ATENCIÓ] Màxim nombre de pkcs12 excedit. Els següents seran ignorats
 [ATENCIÓ] Més d'un Clauer inserit en el sistema.
 [ATENCIÓ] Més d'un Clauer inserit en el sistema.
          Utilitze l'opció -d
 [ATENCIÓ] No s'han detectat Clauers al sistema.
 [ATENCIÓ] Tipus de block desconegut
 [ATENCIÓ] Has indicat més d'un fitxer pkcs12 amb l'opció --import-password  o -i
          El password per a tots els fitxers pkcs12 té que ser la mateixa si aquesta opció s'indica
. Password del pkcs12 per a %s:  