#ifndef _CLVPRVDA_H_
#define _CLVPRVDA_H_ 1

#include "pkcs11.h"
#include "clave.h"

class ClavePrivada : public Clave
{
 public:
    ClavePrivada(void);	// Constructor -sobrecargado-
    ClavePrivada(CK_ATTRIBUTE_PTR pTemplate, CK_ULONG ulCount);	// Constructor
    virtual ~ClavePrivada(void);	// Destructor
    void copiarObjeto(ClavePrivada *pNuevoObjeto);
    CK_RV modificarObjeto(CK_ATTRIBUTE_PTR pTemplate, CK_ULONG ulCount);
    void volcarPlantilla(CK_ATTRIBUTE_PTR pTemplate, CK_ULONG ulMaxCount, CK_ULONG &ulCount);
    unsigned int numeroAtributos(void);
    CK_BYTE *get_subject (void);
    CK_BBOOL get_sensitive (void);
    CK_BBOOL get_decrypt (void);
    CK_BBOOL get_sign (void);
    CK_BBOOL get_signRecover (void);
    CK_BBOOL get_unwrap (void);
    CK_BBOOL get_extractable (void);
    CK_BBOOL get_alwaysSensitive (void);
    CK_BBOOL get_neverExtractable (void);

 protected:
    CK_BYTE *subject;
    CK_ULONG tamSubject;
    CK_BBOOL sensitive;
    CK_BBOOL bsensitive;
    CK_BBOOL decrypt;
    CK_BBOOL bdecrypt;
    CK_BBOOL sign;
    CK_BBOOL bsign;
    CK_BBOOL signRecover;
    CK_BBOOL bsignRecover;
    CK_BBOOL unwrap;
    CK_BBOOL bunwrap;
    CK_BBOOL extractable;
    CK_BBOOL bextractable;
    CK_BBOOL alwaysSensitive;
    CK_BBOOL balwaysSensitive;
    CK_BBOOL neverExtractable;
    CK_BBOOL bneverExtractable;
};

inline CK_BYTE *ClavePrivada::get_subject (void)
{
    return subject;
}

inline CK_BBOOL ClavePrivada::get_sensitive (void)
{
    return sensitive;
}

inline CK_BBOOL ClavePrivada::get_decrypt (void)
{
    return decrypt;
}

inline CK_BBOOL ClavePrivada::get_sign (void)
{
    return sign;
}

inline CK_BBOOL ClavePrivada::get_signRecover (void)
{
    return signRecover;
}

inline CK_BBOOL ClavePrivada::get_unwrap (void)
{
    return unwrap;
}

inline CK_BBOOL ClavePrivada::get_extractable (void)
{
    return extractable;
}

inline CK_BBOOL ClavePrivada::get_alwaysSensitive (void)
{
    return alwaysSensitive;
}

inline CK_BBOOL ClavePrivada::get_neverExtractable (void)
{
    return neverExtractable;
}

#endif
