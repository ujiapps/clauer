#ifndef _LOBJETO_H_
#define _LOBJETO_H_ 1


#include "pkcs11.h"
#include "objeto.h"

#ifdef DEBUG
#include "log.h"
#endif

//----------------------------------------------------------------
//------------------------nodoObjeto------------------------------
//----------------------------------------------------------------

class nodoObjeto
{
 public:
    nodoObjeto(Objeto *valor);	//Constructor
    ~nodoObjeto(void);			//Destructor

    void set_sig(nodoObjeto *valor);
    nodoObjeto *get_sig(void);
	
    void set_ant(nodoObjeto *valor);
    nodoObjeto *get_ant(void);
	
    void set_objeto(Objeto *valor);
    Objeto *get_objeto(void);
 protected:
    nodoObjeto *sig;
    nodoObjeto *ant;
    Objeto *pObjeto;
	

};

// Metodos set y get
inline void nodoObjeto::set_sig(nodoObjeto *valor)
{
    sig = valor;
}

inline nodoObjeto *nodoObjeto::get_sig(void)
{
    return sig;
}
	
inline void nodoObjeto::set_ant(nodoObjeto *valor)
{
    ant = valor;
}

inline nodoObjeto *nodoObjeto::get_ant(void)
{
    return ant;
}

inline void nodoObjeto::set_objeto(Objeto *valor)
{
    pObjeto = valor;
}

inline Objeto *nodoObjeto::get_objeto(void)
{
    return pObjeto;
}
//----------------------------------------------------------------
//------------------------listaObjetos----------------------------
//----------------------------------------------------------------

class listaObjetos
{
 public:
    listaObjetos(void);
    ~listaObjetos(void);

    void insertarObjeto(Objeto *valor);
    void deleteAllObjects(void);
    void eliminarObjeto(Objeto *valor);
    Objeto *buscarObjeto(CK_OBJECT_HANDLE valor);
	
    void iniciarBusquedaPlantilla(CK_ATTRIBUTE_PTR pPlantilla, CK_ULONG numeroAtributos);
    void iniciarBusqueda(void);   
    CK_OBJECT_HANDLE buscarPlantilla(void);
    void finalizarBusquedaPlantilla(void);
    CK_BBOOL estaBusquedaIniciada(void);

    void avanzar(void);
    int  esFin(void);
    Objeto *elementoActual(void);
	
	
 protected:
    CK_ATTRIBUTE_PTR pPlantillaBusqueda;
    CK_ULONG plantillaNumeroAtributos;
    CK_BBOOL busquedaIniciada;
    nodoObjeto *cabeceraListaObjetos;
    nodoObjeto *cursor;
    int _contador;
	
};

inline CK_BBOOL listaObjetos::estaBusquedaIniciada(void)
{
    return busquedaIniciada;
}

#endif
