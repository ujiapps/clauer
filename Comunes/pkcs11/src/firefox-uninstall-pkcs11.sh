
if $(touch /tmp/uninstall.html 2>/dev/null); 
then   
cat > /tmp/uninstall.html <<EOF
<script language="javascript">
        pkcs11.deletemodule("Modulo pkcs11 Clauer");
</script>
EOF
else 
  echo "No puedo escribir el en directorio /tmp"
  exit -1
fi

if [ "$(which firefox)" ] 
then 
	firefox /tmp/uninstall.html
	rm /tmp/uninstall.html
	echo "Instalación Finalizada" 
else
        echo "No puedo encontrar firefox"
	exit -1
fi
