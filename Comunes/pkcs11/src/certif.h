#ifndef _CERTIF_H_
#define _CERTIF_H_ 1

#include "pkcs11.h"
#include "objeto.h"

class Certificado : public Objeto
{
 public:
    Certificado(void);  // Constructor -sobrecargado-
    Certificado(CK_ATTRIBUTE_PTR pTemplate, CK_ULONG ulCount);	// Constructor
    ~Certificado(void);	// Destructor
    void copiarObjeto(Certificado *pNuevoObjeto);
    CK_RV modificarObjeto(CK_ATTRIBUTE_PTR pTemplate, CK_ULONG ulCount);
    void volcarPlantilla(CK_ATTRIBUTE_PTR pTemplate, CK_ULONG ulMaxCount, CK_ULONG &ulCount);
    unsigned int numeroAtributos(void);
    CK_CERTIFICATE_TYPE get_certificateType (void);

 protected:
    // Por cada atributo tenemos otro con el mismo nombre, pero con una 'b'
    // delante, que indica si el atributo tiene un valor asignado.
    CK_CERTIFICATE_TYPE certificateType;
    CK_BBOOL bcertificateType;

};

// Metodos get

inline CK_CERTIFICATE_TYPE Certificado::get_certificateType ()
{
    return certificateType;
}


#endif
