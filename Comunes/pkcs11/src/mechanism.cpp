/*
 * Description: Contains the functions related to clauer token, it 
 *              handles objects and Clauer initialization. 
 *              At first we support just one Clauer, and 
 *              clauer supports just one mechanism for 
 *              Signing and ciphering/ deciphering with 
 *              RSA keys.
 * 
 *                       Clauer Team 2006 
 **/               


#include "mechanism.h"

Mechanism::Mechanism(CK_MECHANISM_TYPE _type, CK_ULONG _ulMinKeySize, CK_ULONG _ulMaxKeySize, CK_FLAGS _flags)
{
    type = _type;
    ulMinKeySize = _ulMinKeySize;
    ulMaxKeySize = _ulMaxKeySize;
    flags = _flags;

}

Mechanism::~Mechanism(void)
{
    // Destructor do nothing 
}

CK_RV Mechanism::C_GetMechanismInfo(CK_MECHANISM_INFO_PTR pInfo)
{
    pInfo->ulMinKeySize = ulMinKeySize;
    pInfo->ulMaxKeySize = ulMaxKeySize;
    pInfo->flags = flags;
    return CKR_OK;
}

