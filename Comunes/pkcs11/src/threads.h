#ifndef __THREADS_H__
#define __THREADS_H__

#ifdef LINUX
#include <pthread.h>
#include <unistd.h>
#elif defined(WIN32)
#include <windows.h>
#endif

#include <stdlib.h>
#include <stdio.h>

#include "common.h"


#ifdef WIN32
#include <map>
#include <string>
struct PASS_CACHE_INFO {
    char pass[128];             // the password null terminated
    char id[20];             // the privkey id
    int ttl;              // time stamp. Indicates the last time the password was requested
};
#endif

using namespace std;

typedef struct ID_ACCESS_HANDLE {
#ifdef LINUX
    pthread_mutex_t clauerIdMutex; // Mutex that protects newClauerId
#elif defined(WIN32)
    HANDLE clauerIdMutex;
    HANDLE keyIdMutex;
    map<string, PASS_CACHE_INFO *> keyCache;
    int global_pass_ttl;
#endif
    unsigned char newClauerId[CLAUER_ID_LEN];
} ID_ACCESS_HANDLE;

#ifdef LINUX
void * updateIdThread(void * ptr);
#elif defined(WIN32)
DWORD WINAPI updateIdThread(void *ptr);
#endif

#endif
