/*
 * Description: Contains the functions related to the Mechanism 
 *              associated with clauer token. 
 * 
 * Notes: Windows code is not been implemented yet. 
 *  
 * 
 *                         Clauer Team 2006 
 **/               


#ifndef __MECNISM_H__
#define __MECNISM_H__ 

#include "pkcs11.h"

// Key sizes
#define MIN_KEY_SIZE 1024 
#define MAX_KEY_SIZE 4096

class Mechanism 
{
 public:
    Mechanism(CK_MECHANISM_TYPE _type, CK_ULONG _ulMinKeySize, CK_ULONG _ulMaxKeySize, CK_FLAGS _flags);
    ~Mechanism(void);	// Destructor
    CK_RV C_GetMechanismInfo(CK_MECHANISM_INFO_PTR pInfo);
  
    CK_MECHANISM_TYPE get_type (void);
    CK_ULONG get_ulMinKeySize (void);
    CK_ULONG get_ulMaxKeySize (void);
    CK_FLAGS get_flags (void);
  
 protected:
    CK_MECHANISM_TYPE type;
    CK_ULONG ulMinKeySize;
    CK_ULONG ulMaxKeySize;
    CK_FLAGS flags;
};
// Metodos get
inline CK_MECHANISM_TYPE Mechanism::get_type (void)
{
    return type;
}

inline CK_ULONG Mechanism::get_ulMinKeySize (void)
{
    return ulMinKeySize;
}

inline CK_ULONG Mechanism::get_ulMaxKeySize (void)
{
    return ulMaxKeySize;
}

inline CK_FLAGS Mechanism::get_flags (void)
{
    return flags;
}

#endif
