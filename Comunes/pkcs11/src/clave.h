#ifndef _CLAVE_H_
#define _CLAVE_H_ 1

#include "pkcs11.h"
#include "objeto.h"

class Clave : public Objeto
{
 public:
    Clave(void);	// Constructor -sobrecargado-
    Clave(CK_ATTRIBUTE_PTR pTemplate, CK_ULONG ulCount);	// Constructor
    ~Clave(void);	// Destructor
    void copiarObjeto(Clave *pNuevoObjeto);
    CK_RV modificarObjeto(CK_ATTRIBUTE_PTR pTemplate, CK_ULONG ulCount);
    void volcarPlantilla(CK_ATTRIBUTE_PTR pTemplate, CK_ULONG ulMaxCount, CK_ULONG &ulCount);
    unsigned int numeroAtributos(void);

    CK_KEY_TYPE get_keyType (void);
    CK_BYTE *get_id (void);
    CK_DATE get_startDate (void);
    CK_DATE get_endDate (void);
    CK_BBOOL get_derive (void);
    CK_BBOOL get_local (void);
 protected:	// atributos
    CK_KEY_TYPE keyType;
    CK_BBOOL bkeyType;
    CK_BYTE *id;
    CK_ULONG tamId;
    CK_DATE startDate;
    CK_BBOOL bstartDate;
    CK_DATE endDate;
    CK_BBOOL bendDate;
    CK_BBOOL derive;
    CK_BBOOL bderive;
    CK_BBOOL local;
    CK_BBOOL blocal;
};


// Metodos get
inline CK_KEY_TYPE Clave::get_keyType (void)
{
    return keyType;
}

inline CK_BYTE *Clave::get_id (void)
{
    return id;
}

inline CK_DATE Clave::get_startDate (void)
{
    return startDate;
}

inline CK_DATE Clave::get_endDate (void)
{
    return endDate;
}

inline CK_BBOOL Clave::get_derive (void)
{
    return derive;
}

inline CK_BBOOL Clave::get_local (void)
{
    return local;
}

#endif
