#ifndef _CERTIFICADO_X509_H_
#define _CERTIFICADO_X509_H_ 1

#include "pkcs11.h"
#include "certif.h"
#include "common.h"

class CertificadoX509 : public Certificado
{
 public:
    CertificadoX509(void);		// Constructor -sobrecargado-
    CertificadoX509(CK_ATTRIBUTE_PTR pTemplate, CK_ULONG ulCount, unsigned char * certId);	// Constructor
    ~CertificadoX509(void);		// Destructor
    void copiarObjeto(CertificadoX509 *pNuevoObjeto);
    CK_RV modificarObjeto(CK_ATTRIBUTE_PTR pTemplate, CK_ULONG ulCount);
    void volcarPlantilla(CK_ATTRIBUTE_PTR pTemplate, CK_ULONG ulMaxCount, CK_ULONG &ulCount);
    unsigned int numeroAtributos(void);

    CK_BYTE *get_subject (void);
    CK_BYTE *get_id (void);
    CK_BYTE *get_issuer (void);
    CK_BYTE *get_serial_number (void);
    CK_BYTE *get_value (void);
	unsigned char * get_certId(void);
 protected:
    // Por cada atributo tenemos otro con el mismo nombre, pero con
    // el prefijo 'tam', que guarda el tamano del objeto apuntado
    // por el atributo pareja.

    CK_BYTE *subject;
    CK_ULONG tamSubject;

    CK_BYTE *id;
    CK_ULONG tamId;

    CK_BYTE *issuer;
    CK_ULONG tamIssuer;
	
    CK_BYTE *serial_number;
    CK_ULONG tamSerial_number;
	
    CK_BYTE *value;
    CK_ULONG tamValue;
	unsigned char _certId[CERT_ID_LEN];

};

// Metodos get

inline unsigned char *CertificadoX509::get_certId(void)
{
	return _certId;
}

inline CK_BYTE *CertificadoX509::get_subject (void)
{
    return subject;
}

inline CK_BYTE *CertificadoX509::get_id (void)
{
    return id;
}

inline CK_BYTE *CertificadoX509::get_issuer (void)
{
    return issuer;
}

inline CK_BYTE *CertificadoX509::get_serial_number (void)
{
    return serial_number;
}

inline CK_BYTE *CertificadoX509::get_value (void)
{
    return value;
}

#endif
