#include <stdlib.h>  // malloc
#include "certif.h"
#include <string.h>	// memcpy


Certificado::Certificado(void):Objeto()
{
	bcertificateType = FALSE;
}

Certificado::Certificado(CK_ATTRIBUTE_PTR pTemplate, CK_ULONG ulCount):Objeto(pTemplate,ulCount)
{
    unsigned int contador;
    
    bcertificateType = FALSE;
    
    contador = 0;
    while (contador < ulCount)
	{
	    switch(pTemplate[contador].type)
		{
		case CKA_CERTIFICATE_TYPE:
		    certificateType = *((CK_CERTIFICATE_TYPE *)pTemplate[contador].pValue);
		    bcertificateType = TRUE;
		    break;
		}
	    contador++;
	}
}

Certificado::~Certificado(void)
{
    // El destructor no hace nada. No tiene que liberar memoria.
}

void Certificado::copiarObjeto(Certificado *pNuevoObjeto)
{
    Objeto::copiarObjeto(pNuevoObjeto);
    pNuevoObjeto->certificateType = this->certificateType;
    pNuevoObjeto->bcertificateType = this->bcertificateType;
}

CK_RV Certificado::modificarObjeto(CK_ATTRIBUTE_PTR pTemplate, CK_ULONG ulCount)
{
    unsigned int contador;
    CK_RV ck_rv;
    
    contador = 0;
    while (contador < ulCount)
	{
	    switch(pTemplate[contador].type)
		{
		case CKA_CERTIFICATE_TYPE:
		    if (bmodifiable && modifiable)
			{
			    certificateType = *((CK_CERTIFICATE_TYPE *)pTemplate[contador].pValue);
			    bcertificateType = TRUE;
			}
		    else
			return CKR_ATTRIBUTE_READ_ONLY;
		    break;
		default:
		    ck_rv = Objeto::modificarObjeto(&pTemplate[contador],1);
		    if (ck_rv != CKR_OK)
			return ck_rv;
		}
	    contador++;
	}
    return CKR_OK;
}


void Certificado::volcarPlantilla(CK_ATTRIBUTE_PTR pTemplate, CK_ULONG ulMaxCount, CK_ULONG &ulCount)
{
    unsigned long contador;
    
    Objeto::volcarPlantilla(pTemplate,ulMaxCount,contador);
    
    if (contador < ulMaxCount)
	{
	    pTemplate[contador].type = CKA_CERTIFICATE_TYPE;
	    if (bcertificateType)
		{
		    pTemplate[contador].ulValueLen = sizeof(certificateType);
		    pTemplate[contador].pValue = (CK_OBJECT_CLASS *) malloc(pTemplate[contador].ulValueLen);
		    *((CK_CERTIFICATE_TYPE *)(pTemplate[contador].pValue)) = certificateType;
		}
	    else
		{
		    pTemplate[contador].pValue = NULL;
		    pTemplate[contador].ulValueLen = (CK_ULONG) -1;
		}
	    contador++;
	}
    ulCount = contador;
}

unsigned int Certificado::numeroAtributos(void)
{
	return  Objeto::numeroAtributos() + 1;
}

