// p11Test.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "pkcs11.h"
#include "windows.h"
#include "malloc.h"

int lock(){
	printf("pasando por lock");
  return 0;
}

int _tmain(int argc, _TCHAR* argv[])
{
	typedef struct CK_C_INITIALIZE_ARGS {
		(void*) CreateMutex;
		(void*) DestroyMutex;
		(void*) LockMutex;
		(void*) UnlockMutex;
		CK_FLAGS flags;
		CK_VOID_PTR pReserved;
	} CK_C_INITIALIZE_ARGS;


	/* First we define the type of the fuctions */
	typedef CK_RV (*C_Initialize) (CK_VOID_PTR pReserved);
	typedef CK_RV (*C_GetInfo)(CK_INFO_PTR pInfo);	
	typedef CK_RV (*C_GetSlotList)(CK_BBOOL tokenPresent, CK_SLOT_ID_PTR pSlotList, CK_ULONG_PTR pulCount);
	typedef CK_RV (*C_GetSlotInfo)(CK_SLOT_ID slotId, CK_SLOT_INFO_PTR pInfo);
	typedef CK_RV (*C_GetTokenInfo)(CK_SLOT_ID slotId, CK_TOKEN_INFO_PTR pInfo);
	typedef CK_RV (*C_OpenSession)(CK_SLOT_ID slotID, CK_FLAGS flags, CK_VOID_PTR pApplication, CK_NOTIFY Notify, CK_SESSION_HANDLE_PTR phSession );
	typedef CK_RV (*C_CloseSession)(CK_SESSION_HANDLE phSession );
	typedef CK_RV (*C_Login)(CK_SESSION_HANDLE hSession, CK_USER_TYPE userType, CK_CHAR_PTR pPin, CK_ULONG ulPinLen );
	typedef CK_RV (*C_SignInit)(CK_SESSION_HANDLE hSession, CK_MECHANISM_PTR  pMechanism, CK_OBJECT_HANDLE  hKey );
	typedef CK_RV (*C_Sign)(CK_SESSION_HANDLE hSession, CK_BYTE_PTR pData, CK_ULONG ulDataLen, CK_BYTE_PTR pSignature,      /* The signed data buffer              */
                            CK_ULONG_PTR pulSignatureLen );
	typedef CK_RV (*C_VerifyInit)( CK_SESSION_HANDLE hSession, CK_MECHANISM_PTR  pMechanism, CK_OBJECT_HANDLE  hKey );
	typedef CK_RV (*C_Verify)( CK_SESSION_HANDLE hSession, CK_BYTE_PTR pData, CK_ULONG ulDataLen, CK_BYTE_PTR pSignature, CK_ULONG ulSignatureLen );
	typedef CK_RV (*C_DestroyObject)( CK_SESSION_HANDLE hSession, CK_OBJECT_HANDLE  hObject);
	typedef CK_RV (*C_FindObjectsInit)(CK_SESSION_HANDLE hSession, CK_ATTRIBUTE_PTR  pTemplate, CK_ULONG ulCount);
	typedef CK_RV (*C_FindObjects) (CK_SESSION_HANDLE hSession, CK_OBJECT_HANDLE_PTR phObject, CK_ULONG ulMaxObjectCount, CK_ULONG_PTR pulObjectCount );
	typedef CK_RV (*C_FindObjectsFinal)(CK_SESSION_HANDLE hSession);
    typedef CK_RV (*C_CreateObject)(CK_SESSION_HANDLE hSession, CK_ATTRIBUTE_PTR pTemplate, CK_ULONG ulCount, CK_OBJECT_HANDLE_PTR phObject);


	/* Function result */
	CK_RV res;
	/* Next we load up the library */ 
	HINSTANCE DLLHandle;
	//DLLHandle= LoadLibrary("C:\\WINDOWS\\system32\\pkcs11-win.dll");
	DLLHandle= LoadLibrary("C:\\Documents\ and\ Settings\\Administrador\\Escritorio\\Clauer_forja\\Comunes\\pkcs11\\pkcs11-win\\accv-release\\pkcs11-win.dll");
	//DLLHandle= LoadLibrary("c:\\csp11.dll");
	printf("\n ERROR: %d\n", GetLastError());
	if ( DLLHandle != 0 ){
		printf("LoadLibrary Ok, going on ...\n");
	}
	else{
		printf("LoadLibrary Failed\n");
		return 1;
	}

	/* Getting the memory address of initialize function */
	C_Initialize C_InitializePtr;
	C_InitializePtr = (C_Initialize) GetProcAddress(DLLHandle, "C_Initialize");  	

	if ( C_InitializePtr != 0 ) {
		printf("C_Initialize address got successfuly ...\n");
	}
	else{
		printf("C_Initialize address discover failed \n"); 
		return 1;
	}

	/**
	* 
	* INITIALIZE FUNCTION 
	*
	**/ 
	CK_VOID_PTR pReservedArg;	
	pReservedArg= malloc(sizeof(CK_VOID_PTR));

	CK_C_INITIALIZE_ARGS args;

	args.CreateMutex= &lock;
	args.DestroyMutex= &lock;
	args.LockMutex=&lock;
	args.UnlockMutex=&lock; 
	args.flags=0x00000001;
	args.pReserved=NULL_PTR;
	printf("a");
	res = C_InitializePtr(NULL);//&args/*pReservedArg*/);
	if ( res != CKR_OK){
		printf("La funci�n Initialize devolvio error= 0x%x", res );
		return -1;
	} 

	/**
	* 
	* GETINFO FUNCTION 
	*
	**/ 
	C_GetInfo C_GetInfoPtr;
	C_GetInfoPtr= (C_GetInfo) GetProcAddress(DLLHandle, "C_GetInfo");  	

	if ( C_GetInfoPtr != 0 ) {
		printf("C_GetInfo address got successfuly ...\n");
	}
	else{
		printf("C_GetInfo address discover failed \n"); 
		return 1;
	}

	CK_INFO_PTR pInfoArg= (CK_INFO_PTR) malloc(sizeof(CK_INFO));
	res= C_GetInfoPtr(pInfoArg);
	if ( res != CKR_OK){
		printf("La funci�n GetInfo devolvio error= 0x%x", res );
	} 

	printf("\n----- Module Info -----\n");
	printf("  Version:           %ul \n", pInfoArg->cryptokiVersion);
	printf("  Library desc:      ");
	for(int i=-2; i<30; i++){
		printf("%c", pInfoArg->libraryDescription[i]);
	}
	printf("\n");
	printf("  Manufacturer desc: ");
	for(int i=0; i<32; i++){
		printf("%c", pInfoArg->manufacturerID[i]);
	}
	printf("\n");
	printf("----- End Module Info -----\n\n");

	//Sleep(10000);


	/**
	* 
	* GETSLOTLIST FUNCTION 
	*
	**/ 
	C_GetSlotList  C_GetSlotListPtr;
	C_GetSlotListPtr = (C_GetSlotList) GetProcAddress(DLLHandle, "C_GetSlotList");
	
	if ( C_GetSlotListPtr != 0 ) {
		printf("C_GetSlotList address got successfuly ...\n");
	}
	else{
		printf("C_GetSlotList address discover failed \n"); 
		return 1;
	}
	
	CK_BBOOL tokenPresent= TRUE; //(CK_BBOOL) malloc(sizeof CK_BBOOL);
	CK_SLOT_ID_PTR pSlotList=NULL_PTR;
	CK_ULONG_PTR pulCount= (CK_ULONG_PTR) malloc(sizeof CK_ULONG);


	res= C_GetSlotListPtr(tokenPresent, pSlotList, pulCount);
	if ( res != CKR_OK){
		printf("La funci�n GetSlotList devolvio error= 0x%x", res );
	} 


	printf("Detected: %ld slots avaliable\n", *pulCount);
	


	/**
	* 
	* GETSLOTINFO FUNCTION 
	*
	**/
	printf("Getting info from first available slot ...\n");
	C_GetSlotInfo  C_GetSlotInfoPtr;
	C_GetSlotInfoPtr = (C_GetSlotInfo) GetProcAddress(DLLHandle, "C_GetSlotInfo");
	
	if ( C_GetSlotInfoPtr != 0 ) {
		printf("C_GetSlotInfo address got successfuly ...\n");
	}
	else{
		printf("C_GetSlotInfo address discover failed \n"); 
		return 1;
	}

	CK_SLOT_ID slotId = 1;
	CK_SLOT_INFO slotInfo;

	res= C_GetSlotInfoPtr(slotId,&slotInfo);
	if ( res != CKR_OK){
		printf("La funci�n GetSlotInfoPtr devolvio error= 0x%x", res );
	} 


	printf("\n\n----- Slot Info -----\n");
	printf("  FirmwareVersion: %ld\n", slotInfo.firmwareVersion);
	printf("  HardwareVersion: %ld\n ", slotInfo.hardwareVersion);
	printf("  ManufacturerId: ");
	for (int i=0; i<32; i++){
		printf("%c", slotInfo.manufacturerID[i]);
	}	
	printf("\n");
	
	printf("  slotDescription: ");
	for (int i=0; i<64; i++){
		printf("%c", slotInfo.slotDescription[i]);
	}	
	printf("\n----- End Slot Info -----\n");
	printf("\n");


	/**
	* 
	* GETTOKENINFO FUNCTION 
	*
	**/ 
	printf("Getting info from token at first available slot ...\n");
	C_GetTokenInfo  C_GetTokenInfoPtr;
	C_GetTokenInfoPtr = (C_GetTokenInfo) GetProcAddress(DLLHandle, "C_GetTokenInfo");
	
	if ( C_GetTokenInfoPtr != 0 ) {
		printf("C_GetTokenInfo address got successfuly ...\n");
	}
	else{
		printf("C_GetTokenInfo address discover failed \n"); 
		return 1;
	}

	slotId = 1;
	CK_TOKEN_INFO tokenInfo;

	res= C_GetTokenInfoPtr(slotId,&tokenInfo);
	if ( res != CKR_OK){
		printf("La funci�n GetTokenInfo devolvio error= 0x%x", res );
	} 

	printf("\n\n----- Token Info -----\n");
	printf("  FirmwareVersion: %ld\n", slotInfo.firmwareVersion);
	printf("  HardwareVersion: %ld\n ", slotInfo.hardwareVersion);
	printf("  Label: ");
	for (int i=0; i<32; i++){
		printf("%c", tokenInfo.label[i]);
	}	
	printf("\n");
	
	printf("  Manufacturer: ");
	for (int i=0; i<32; i++){
		printf("%c", tokenInfo.manufacturerID[i]);
	}	
	printf("\n----- End Slot Info -----\n");
	printf("\n");	




	/**
	* 
	* OpenSession FUNCTION 
	*
	**/ 
	C_OpenSession C_OpenSessionPtr;
	C_OpenSessionPtr= (C_OpenSession) GetProcAddress(DLLHandle, "C_OpenSession");

	if ( C_OpenSessionPtr != 0 ) {
		printf("C_OpenSession address got successfuly ...\n");
	}
	else{
		printf("C_OpenSession address discover failed \n"); 
		//return 1;
	}
	
	CK_FLAGS flags= ( CKF_SERIAL_SESSION ); 
	CK_VOID_PTR pApplication= NULL_PTR;
	CK_NOTIFY Notify= NULL_PTR;
	CK_SESSION_HANDLE hSession;

	res= C_OpenSessionPtr(slotId, flags, &pApplication, Notify, &hSession);
	if ( res != CKR_OK){
		printf("La funci�n OpenSession devolvio error= 0x%x", res );
	} 

   
	/**
	* 
	* CloseSession FUNCTION 
	*
	**/ 
	C_CloseSession C_CloseSessionPtr;
	C_CloseSessionPtr= (C_CloseSession) GetProcAddress(DLLHandle, "C_CloseSession");

	if ( C_CloseSessionPtr != 0 ) {
		printf("C_CloseSession address got successfuly ...\n");
	}
	else{
		printf("C_CloseSession address discover failed \n"); 
		//return 1;
	}
	
	res= C_CloseSessionPtr(hSession);
	if ( res != CKR_OK){
		printf("La funci�n CloseSession devolvio error= 0x%x", res );
	} 

	// To here...
	return 0;
	// ...

	/**
	* 
	* Login FUNCTION 
	*
	**/ 
	C_Login C_LoginPtr;
	C_LoginPtr= (C_Login) GetProcAddress(DLLHandle, "C_Login");

	if ( C_LoginPtr != 0 ) {
		printf("C_Login address got successfuly a...\n");
	}
	else{
		printf("C_Login address discover failed \n"); 
		//return 1;
	}
	
	res= C_LoginPtr(hSession, CKU_USER, (CK_CHAR_PTR )"123clauer", 9 );
	if ( res != CKR_OK){
		printf("La funci�n C_Login devolvio error= 0x%x\n", res );	
	} 
	printf("La funci�n C_Login devolvio = 0x%x\n", res );	

	printf("\n");

	CK_BBOOL itrue = TRUE;
	CK_OBJECT_CLASS certificateClass = CKO_CERTIFICATE;
	CK_BYTE * certificateValue;
	CK_OBJECT_HANDLE hData;

	FILE * pFile= fopen("C:\\rootca.crt","rb");
	long lSize, read;
	
	if ( !pFile ){
		printf("Error cannot open file!!");
		return -1;
	}
	

	// obtain file size:
	fseek (pFile , 0 , SEEK_END);
	lSize = ftell (pFile);
	rewind (pFile);




	// allocate memory to contain the whole file:
    certificateValue = (CK_BYTE *) malloc (sizeof(char)*lSize);
    read= fread (certificateValue,1,lSize,pFile);
	printf("READ: %d\n", read);
	fclose (pFile);
	
	CK_ATTRIBUTE certificateTemplate[] = {
	 {CKA_CLASS, &certificateClass,
	 sizeof(certificateClass)},
	 {CKA_TOKEN, &itrue, sizeof(itrue)},
	 {CKA_VALUE, certificateValue, lSize}
	};


	//Now we get the CreateObject Function 
	C_CreateObject  C_CreateObjectPtr;
	C_CreateObjectPtr= (C_CreateObject) GetProcAddress(DLLHandle, "C_CreateObject");

	if ( C_CreateObjectPtr != 0 ) {
		printf("C_CreateObject  address got successfuly a...\n");
	}
	else{
		printf("C_CreateObject address discover failed \n"); 
		//return 1;
	}
	
	res= C_CreateObjectPtr(hSession, certificateTemplate, 3, &hData );
	if ( res != CKR_OK){
		printf("La funci�n C_CreateObject devolvio error= 0x%x\n", res );	
	} 
	printf("La funci�n C_CreateObject devolvio = 0x%x\n", res );	
    //END CreateObject


	return -1; 

	/* Inicializamos la funci�n de firma */
	/* C_SignInit Function */
	C_SignInit C_SignInitPtr;
	C_SignInitPtr= (C_SignInit) GetProcAddress(DLLHandle, "C_SignInit");

	if ( C_SignInitPtr != 0 ) {
		printf("C_SignInit address got successfuly a...\n");
	}
	else{
		printf("C_SignInit address discover failed \n"); 
		//return 1;
	}
	
	CK_MECHANISM mech;
	mech.mechanism= 0x00000006;
	mech.pParameter= NULL_PTR;

	res= C_SignInitPtr(hSession, (CK_MECHANISM_PTR)&mech , 2);
	if ( res != CKR_OK){
		printf("La funci�n C_SignInit devolvio error= 0x%x\n", res );	
		//return 1;
	} 
	printf("La funci�n C_SignInit devolvio = 0x%x\n", res );	



	printf("\n");
	/* Funci�n de firma en s� */
	/* C_Sign function */

/* 
   (CK_SESSION_HANDLE hSession, 
	CK_BYTE_PTR pData, 
	CK_ULONG ulDataLen, 
	CK_BYTE_PTR pSignature,
    CK_ULONG_PTR pulSignatureLen );
*/ 

	C_Sign C_SignPtr;
	C_SignPtr= (C_Sign) GetProcAddress(DLLHandle, "C_Sign");

	if ( C_SignPtr != 0 ) {
		printf("C_Sign address got successfuly a...\n");
	}
	else{
		printf("C_Sign address discover failed \n"); 
		//return 1;
	}


	CK_BYTE_PTR pData= (CK_BYTE_PTR) malloc(10);
	CK_ULONG ulDataLen= 10;
	memcpy(pData,"123456789",10);

	CK_BYTE_PTR pSignature= (CK_BYTE_PTR) malloc(128);
	CK_ULONG ulSignatureLen=128; 

	res= C_SignPtr(hSession, pData, ulDataLen, pSignature, &ulSignatureLen );
	if ( res != CKR_OK){
		printf("La funci�n C_Sign devolvio error= 0x%x\n", res );	
	} 
	printf("La funci�n C_Sign devolvio = 0x%x\n", res );	

	printf("El tama�o de la firma es: %ld\nY la firma: \n", ulSignatureLen );
	for ( int i=0;i<ulSignatureLen;i++){
		printf("%02x", pSignature[i]);
	}
	printf("\n\n");

	/* Ahora verifiquemos */ 
	/* Inicializamos la funci�n de verificaci�n */
	/* C_VerifyInit Function */
	C_VerifyInit C_VerifyInitPtr;
	C_VerifyInitPtr= (C_VerifyInit) GetProcAddress(DLLHandle, "C_VerifyInit");

	if ( C_VerifyInitPtr != 0 ) {
		printf("C_VerifyInit address got successfuly a...\n");
	}
	else{
		printf("C_VerifyInit address discover failed \n"); 
		//return 1;
	}

	res= C_VerifyInitPtr(hSession, (CK_MECHANISM_PTR)&mech , 2);
	if ( res != CKR_OK){
		printf("La funci�n VerifyInit devolvio error= 0x%x\n", res );	
	} 
	printf("La funci�n VerifyInit devolvio = 0x%x\n", res );	


	/* Inicializamos la funci�n de verificaci�n */
	/* C_Verify Function */
	C_Verify C_VerifyPtr;
	C_VerifyPtr= (C_Verify) GetProcAddress(DLLHandle, "C_Verify");

	if ( C_VerifyPtr != 0 ) {
		printf("C_Verify address got successfuly a...\n");
	}
	else{
		printf("C_Verify address discover failed \n"); 
		//return 1;
	}

	res= C_VerifyPtr(hSession, pData, ulDataLen, pSignature, ulSignatureLen );
	if ( res != CKR_OK){
		printf("La funci�n C_Verify devolvio error= 0x%x\n", res );	
	} 
	printf("La funci�n C_Verify devolvio = 0x%x\n", res );	


	/* OJO CON ESTA PARTE, ES PELIGROSA!!!! */
	/* Borramos las claves generadas*/
	/* C_DestroyObject Function */
	C_DestroyObject C_DestroyObjectPtr;
	C_DestroyObjectPtr= (C_DestroyObject) GetProcAddress(DLLHandle, "C_DestroyObject");

	if ( C_DestroyObjectPtr != 0 ) {
		printf("C_DO address got successfuly a...\n");
	}
	else{
		printf("C_DO address discover failed \n"); 
		return 1;
	}
	for ( int j=0; j<40; j++ ){
		res= C_DestroyObjectPtr(hSession, j);
		if ( res != CKR_OK){
			printf("La funci�n C_DO devolvio error= 0x%x\n", res );	
		} 
		printf("La funci�n C_DO devolvio = 0x%x\n", res );	
	}
	


	/* Aqui hacemos un FindObjects*/
	//C_FindObjectsInint(CK_SESSION_HANDLE hSession, CK_ATTRIBUTE_PTR  pTemplate, CK_ULONG ulCount);
 	C_FindObjectsInit C_FindObjectsInitPtr;
	C_FindObjectsInitPtr= (C_FindObjectsInit) GetProcAddress(DLLHandle, "C_FindObjectsInit");

	if ( C_FindObjectsInitPtr != 0 ) {
		printf("C_FindObjectsInit address got successfuly a...\n");
	}
	else{
		printf("C_FindObjectsInit address discover failed \n"); 
		return 1;
	}

	C_FindObjects C_FindObjectsPtr;
	C_FindObjectsPtr= (C_FindObjects) GetProcAddress(DLLHandle, "C_FindObjects");

	if ( C_FindObjectsPtr != 0 ) {
		printf("C_FindObjects address got successfuly a...\n");
	}
	else{
		printf("C_FindObjects address discover failed \n"); 
		return 1;
	}


 	C_FindObjectsFinal C_FindObjectsFinalPtr;
	C_FindObjectsFinalPtr= (C_FindObjectsFinal) GetProcAddress(DLLHandle, "C_FindObjectsFinal");

	if ( C_FindObjectsFinalPtr != 0 ) {
		printf("C_FindObjectsFinal address got successfuly a...\n");
	}
	else{
		printf("C_FindObjectsFinal address discover failed \n"); 
		return 1;
	}



	CK_OBJECT_HANDLE hObject;
	CK_ULONG ulObjectCount;
	CK_RV rv;

	rv = C_FindObjectsInitPtr(hSession, NULL_PTR, 0);
	if ( rv != CKR_OK){
		printf(" 1 La funci�n C_FindObjects devolvio error= 0x%x\n", rv );
	}
	while (1) {
		rv = C_FindObjectsPtr(hSession, &hObject, 1,
						&ulObjectCount);
		if ( rv != CKR_OK){
			printf("2 La funci�n C_FindObjects devolvio error= 0x%x\n", rv );
		}
		printf("ulcount= %d\n",ulObjectCount);
		if (rv != CKR_OK || ulObjectCount == 0)
			break;
		}
	rv = C_FindObjectsFinalPtr(hSession);
	if ( rv != CKR_OK){
		printf("3 La funci�n C_FindObjects devolvio error= 0x%x\n", rv );
	} 
	printf("La funci�n C_FindObjects devolvio = 0x%x\n", res );	

	/***************/

	return 0;
}

