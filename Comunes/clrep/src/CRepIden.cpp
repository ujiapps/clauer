#include "CRepIden.h"

#include <LIBRT/libRT.h>
#include <CRYPTOWrapper/CRYPTOWrap.h>

#ifdef WIN32
#include <windows.h>
#include <wincrypt.h>
#endif


CRepIden::CRepIden  ( CLREP_PWD_CB pwdCb /* = NULL */ )
{
	_pwdCb = pwdCb;
}



CRepIden::~CRepIden ( void )
{
}



int CRepIden::test ( char *device )
{
	return CLREP_TEST_REPAIR;
}

int CRepIden::repair ( char *device )
{
	unsigned char id[20];
	unsigned char bloqueBlob[TAM_BLOQUE];
	long nbBlob;
	USBCERTS_HANDLE hClauer;
	unsigned char *llavePEM = NULL;
	unsigned long tamLlave;
	unsigned char bloqueCert[TAM_BLOQUE];
	long nbCert;
	unsigned char bloqueLlave[TAM_BLOQUE];
	long nbLlave;

	int ret = CLREP_OK;
	
	try {

		if ( LIBRT_IniciarDispositivo((unsigned char *)device, _pwd, &hClauer) != 0 ) 
			throw ERR_CLREP_CANT_INITIALIZE;

		/* Empezamos recalculando identificadores para los blobs
		 */

		if ( LIBRT_LeerTipoBloqueCrypto(&hClauer, BLOQUE_PRIVKEY_BLOB, 1, bloqueBlob, &nbBlob) != 0 ) {
			LIBRT_FinalizarDispositivo(&hClauer);
			throw ERR_CLREP;
		}

		while ( nbBlob != -1 ) {
			
			/* Para calcular el identificador la paso a PEM...
			 * Lo ideal ser�a una funci�n espec�fica, pero ahora mismo
			 * no dispongo de ella y no quiero arriesgarme a programar
			 * m�s bugs
			 */
			
			if ( CRYPTO_BLOB2LLAVE(BLOQUE_PRIVKEYBLOB_Get_Objeto(bloqueBlob),
				                   BLOQUE_PRIVKEYBLOB_Get_Tam(bloqueBlob),
								   NULL,
								   &tamLlave) != 0 )
			{
				LIBRT_FinalizarDispositivo(&hClauer);
				throw ERR_CLREP;
			}

			llavePEM = new unsigned char[tamLlave];
			if ( ! llavePEM ) {
				LIBRT_FinalizarDispositivo(&hClauer);
				throw ERR_CLREP_OUT_OF_MEMORY;
			}

			if ( CRYPTO_BLOB2LLAVE(BLOQUE_PRIVKEYBLOB_Get_Objeto(bloqueBlob),
				                   BLOQUE_PRIVKEYBLOB_Get_Tam(bloqueBlob),
								   llavePEM,
								   &tamLlave) != 0 )
			{
				LIBRT_FinalizarDispositivo(&hClauer);
				throw ERR_CLREP;
			}

			/* Calculo el nuevo identificador
			 */

			if ( CRYPTO_LLAVE_PEM_Id(llavePEM, tamLlave, 1, NULL, id) != 0 ) {
				LIBRT_FinalizarDispositivo(&hClauer);
				throw ERR_CLREP;
			}

			CRYPTO_SecureZeroMemory(llavePEM, tamLlave);
			delete [] llavePEM;
			llavePEM = NULL;

			/* Busco el container asociado al blob y actualizo con el nuevo
			 * identificador
			 */

			unsigned char bloqueContainer[TAM_BLOQUE];
			long nbContainer;

			if ( LIBRT_LeerTipoBloqueCrypto(&hClauer, BLOQUE_KEY_CONTAINERS, 1, bloqueContainer, &nbContainer) != 0 ) {
				LIBRT_FinalizarDispositivo(&hClauer);
				throw ERR_CLREP;
			}

			int containerUpdated = 0;
			unsigned char zeroId[20];
			memset(zeroId, 0, sizeof zeroId);

			while ( !containerUpdated && (nbContainer != -1) ) {

				INFO_KEY_CONTAINER lstContainers[NUM_KEY_CONTAINERS];
				unsigned int tamLstContainers, c;
				
				if ( BLOQUE_KeyContainer_Enumerar(bloqueContainer, lstContainers, &tamLstContainers) != 0 ) {
					LIBRT_FinalizarDispositivo(&hClauer);
					throw ERR_CLREP;
				}
				
				for ( c = 0 ; !containerUpdated && (c < tamLstContainers) ; c++ ) {

					if ( memcmp(lstContainers[c].idExchange,
								BLOQUE_PRIVKEYBLOB_Get_Id(bloqueBlob),
								20) == 0 )
					{

						/* Si hay un llave de tipo AT_SIGNATURE, no hay arreglo. El motivo es que
						 * no puedo sobreescribir ese container y nukear las aplicaciones que
						 * dependan de �l. As� es que devolvemos error
						 */

						if ( memcmp(lstContainers[c].idSignature,
							        zeroId,
									20) != 0 )
						{
							LIBRT_FinalizarDispositivo(&hClauer);
							throw ERR_CLREP;
						}

						BLOQUE_KeyContainer_Establecer_ID_Exchange(bloqueContainer, lstContainers[c].nombreKeyContainer, id);
						BLOQUE_KeyContainer_Establecer_ID_Signature(bloqueContainer, lstContainers[c].nombreKeyContainer, zeroId);
						containerUpdated=1;

					} else if ( memcmp(lstContainers[c].idSignature,
									   BLOQUE_PRIVKEYBLOB_Get_Id(bloqueBlob),
									   20) == 0 )
					{

						/* Si hay un llave de tipo AT_KEYEXCHANGE, no hay arreglo. El motivo es que
						 * no puedo sobreescribir ese container y nukear las aplicaciones que
						 * dependan de �l. As� es que devolvemos error.
						 */
			
						if ( memcmp(lstContainers[c].idExchange,
									zeroId,
									20) != 0 )
						{
							LIBRT_FinalizarDispositivo(&hClauer);
							throw ERR_CLREP;
						}

						BLOQUE_KeyContainer_Establecer_ID_Signature(bloqueContainer, lstContainers[c].nombreKeyContainer, id);
						BLOQUE_KeyContainer_Establecer_ID_Exchange(bloqueContainer, lstContainers[c].nombreKeyContainer, zeroId);
						containerUpdated=1;
					}
				}

				if ( containerUpdated ) {

					if ( LIBRT_EscribirBloqueCrypto(&hClauer, nbContainer, bloqueContainer) != 0 ) {
						LIBRT_FinalizarDispositivo(&hClauer);
						throw ERR_CLREP;
					}

				} else {
				
					if ( LIBRT_LeerTipoBloqueCrypto(&hClauer, BLOQUE_KEY_CONTAINERS, 0, bloqueContainer, &nbContainer) != 0 ) {
						LIBRT_FinalizarDispositivo(&hClauer);
						throw ERR_CLREP;
					}

				}
			}

			/* Si no se actualiz� el container aqu�, es que no hab�a bloque de container
			 * asociado. Lo que hacemos es crear un nuevo container... y asociarlo al blob
			 */

			if ( ! containerUpdated ) {

				int found = 0;

				if ( LIBRT_LeerTipoBloqueCrypto(&hClauer, BLOQUE_KEY_CONTAINERS, 1, bloqueContainer, &nbContainer) != 0 ) {
					LIBRT_FinalizarDispositivo(&hClauer);
					throw ERR_CLREP;
				}

				while ( !found && nbContainer != -1 ) {

					INFO_KEY_CONTAINER ikc[NUM_KEY_CONTAINERS];
					unsigned int nkc;
					
					if ( BLOQUE_KeyContainer_Enumerar(bloqueContainer, ikc, &nkc) != 0 ) {
						LIBRT_FinalizarDispositivo(&hClauer);
						throw ERR_CLREP;
					}
					
					if ( nkc < NUM_KEY_CONTAINERS ) 
						found = 1;
					else {

						if ( LIBRT_LeerTipoBloqueCrypto(&hClauer, BLOQUE_KEY_CONTAINERS, 0, bloqueContainer, &nbContainer) != 0 ) {
							LIBRT_FinalizarDispositivo(&hClauer);
							throw ERR_CLREP;
						}
					}
				}

				char containerName[257];

				strcpy(containerName, "CLAFOR_Container_");
				CRYPTO_Random(239, (unsigned char *) containerName+17);
				for ( int k = 0; k < 239; k++ ) 
					*(containerName+17+k) = 97 + ((unsigned char)*(containerName+17+k) % 26);

				*(containerName+256) = 0;

				if ( nbContainer == -1 ) {		
					memset(bloqueContainer, 0, sizeof bloqueContainer);
					BLOQUE_Set_Claro(bloqueContainer);
					BLOQUE_KeyContainer_Nuevo(bloqueContainer);
				}

				BLOQUE_KeyContainer_Insertar(bloqueContainer, containerName);

				BLOBHEADER *bh = (BLOBHEADER *) BLOQUE_PRIVKEYBLOB_Get_Objeto(bloqueBlob);
				switch ( bh->aiKeyAlg ) {
					case CALG_RSA_SIGN:
					case AT_SIGNATURE:
						BLOQUE_KeyContainer_EstablecerSIGNATURE(bloqueContainer, containerName, nbContainer, 0);
						BLOQUE_KeyContainer_EstablecerEXCHANGE(bloqueContainer, containerName, -1, 0);
						BLOQUE_KeyContainer_Establecer_ID_Signature(bloqueContainer, containerName, id);
						BLOQUE_KeyContainer_Establecer_ID_Exchange(bloqueContainer, containerName, zeroId);
						break;

					case CALG_RSA_KEYX:
					case AT_KEYEXCHANGE:
						BLOQUE_KeyContainer_EstablecerSIGNATURE(bloqueContainer, containerName, -1, 0);
						BLOQUE_KeyContainer_EstablecerEXCHANGE(bloqueContainer, containerName, nbContainer, 0);
						BLOQUE_KeyContainer_Establecer_ID_Signature(bloqueContainer, containerName, zeroId);
						BLOQUE_KeyContainer_Establecer_ID_Exchange(bloqueContainer, containerName, id);
						break;
				}
					
		
				if ( nbContainer == -1 ) {
					if ( LIBRT_InsertarBloqueCrypto(&hClauer, bloqueContainer, &nbContainer) != 0 ) {
						LIBRT_FinalizarDispositivo(&hClauer);
						throw ERR_CLREP;
					}
				} else {

					if ( LIBRT_EscribirBloqueCrypto(&hClauer, nbContainer, bloqueContainer) != 0 ) {
						LIBRT_FinalizarDispositivo(&hClauer);
						throw ERR_CLREP;
					}
				}
			}

			// Actualizamos el blob con el nuevo identificador
			 
			BLOQUE_PRIVKEYBLOB_Set_Id(bloqueBlob, id);

			if ( LIBRT_EscribirBloqueCrypto(&hClauer, nbBlob, bloqueBlob) != 0 ) {
				LIBRT_FinalizarDispositivo(&hClauer);
				throw ERR_CLREP;
			}

			CRYPTO_SecureZeroMemory(bloqueBlob, TAM_BLOQUE);

			if ( LIBRT_LeerTipoBloqueCrypto(&hClauer, BLOQUE_PRIVKEY_BLOB, 0, bloqueBlob, &nbBlob) != 0 ) {
				LIBRT_FinalizarDispositivo(&hClauer);
				throw ERR_CLREP;
			}

		}
		
		/* Recalculamos identificadores para certificados propios
		 */

		if ( LIBRT_LeerTipoBloqueCrypto(&hClauer, BLOQUE_CERT_PROPIO, 1, bloqueCert, &nbCert) != 0 ) {
			LIBRT_FinalizarDispositivo(&hClauer);
			throw ERR_CLREP;
		}

		while ( nbCert != -1 ) {

			if ( CRYPTO_CERT_PEM_Id(BLOQUE_CERTPROPIO_Get_Objeto(bloqueCert),
								    BLOQUE_CERTPROPIO_Get_Tam(bloqueCert),
									id) != 0 )
			{
				LIBRT_FinalizarDispositivo(&hClauer);
				throw ERR_CLREP;
			}

			BLOQUE_CERTPROPIO_Set_Id(bloqueCert, id);

			if ( LIBRT_EscribirBloqueCrypto(&hClauer, nbCert, bloqueCert) != 0 ) {
				LIBRT_FinalizarDispositivo(&hClauer);
				throw ERR_CLREP;
			}

			if ( LIBRT_LeerTipoBloqueCrypto(&hClauer, BLOQUE_CERT_PROPIO, 0, bloqueCert, &nbCert) != 0 ) {
				LIBRT_FinalizarDispositivo(&hClauer);
				throw ERR_CLREP;
			}
		}

		/* Por �ltimo llaves privadas PEM
		 */

		if ( LIBRT_LeerTipoBloqueCrypto(&hClauer, BLOQUE_LLAVE_PRIVADA, 1, bloqueLlave, &nbLlave) != 0 ) {
			LIBRT_FinalizarDispositivo(&hClauer);
			throw ERR_CLREP;
		}

		while ( nbLlave != -1 ) {

			if ( CRYPTO_LLAVE_PEM_Id(BLOQUE_LLAVEPRIVADA_Get_Objeto(bloqueLlave),
				                     BLOQUE_LLAVEPRIVADA_Get_Tam(bloqueLlave),
									 1,
									 NULL,
									 id) != 0 )
			{
				LIBRT_FinalizarDispositivo(&hClauer);
				throw ERR_CLREP;
			}

			BLOQUE_LLAVEPRIVADA_Set_Id(bloqueLlave, id);

			if ( LIBRT_EscribirBloqueCrypto(&hClauer, nbLlave, bloqueLlave) != 0 ) {
				LIBRT_FinalizarDispositivo(&hClauer);
				throw ERR_CLREP;
			}

			CRYPTO_SecureZeroMemory(bloqueLlave, TAM_BLOQUE);

			if ( LIBRT_LeerTipoBloqueCrypto(&hClauer, BLOQUE_LLAVE_PRIVADA, 0, bloqueLlave, &nbLlave) != 0 ) {
				LIBRT_FinalizarDispositivo(&hClauer);
				throw ERR_CLREP;
			}	
		}
	}
	catch ( int err ) {
		ret = err;
	}

	CRYPTO_SecureZeroMemory(bloqueLlave, TAM_BLOQUE);
	CRYPTO_SecureZeroMemory(bloqueBlob, TAM_BLOQUE);
	if ( llavePEM ) {
		CRYPTO_SecureZeroMemory(llavePEM, tamLlave);
		delete [] llavePEM;
		llavePEM = 0;
	}

	return ret;

}



void CRepIden::getDescription ( char desc[CLREP_MAX_DESC] )
{

}




