#include "CRepExchangeSignature.h"

#include <LIBRT/libRT.h>
#include <CRYPTOWrapper/CRYPTOWrap.h>



CRepExchangeSignature::CRepExchangeSignature  ( CLREP_PWD_CB pwdCb /*= NULL*/ )
{
	_pwdCb = pwdCb;
}


CRepExchangeSignature::~CRepExchangeSignature ( void )
{

}



int CRepExchangeSignature::test ( char *device )
{
	return CLREP_TEST_REPAIR;
}



int CRepExchangeSignature::repair ( char *device )
{

	USBCERTS_HANDLE hClauer;
	unsigned char bloqueCert[TAM_BLOQUE];
	long nbCert;
	unsigned long certKu;
	unsigned char bloqueBlob[TAM_BLOQUE];
	long nbBlob;

	int ret = CLREP_OK;

  /* Este test necesita iniciar una sesi�n autenticada
   * contra el CLOS
   */

  if ( ! _pwd ) {
    if ( !_pwdCb ) {
      return CLREP_NEED_PASS;
    }
    if ( _pwdCb(_pwd) != 0 ) {
      *_pwd = 0;
      return ERR_CLREP_PASS;
    }
  }

  /* Iniciamos sesi�n
   */


  try {
    
    if ( LIBRT_IniciarDispositivo((unsigned char *) device, _pwd, &hClauer) != 0 )
      throw ERR_CLREP_CANT_INITIALIZE;

	if ( LIBRT_LeerTipoBloqueCrypto(&hClauer, BLOQUE_CERT_PROPIO, 1, bloqueCert, &nbCert) != 0 ) {
		LIBRT_FinalizarDispositivo(&hClauer);
		throw ERR_CLREP;
	}

	while ( nbCert != -1 ) {

		/* Obtengo el key usage del certificado. Si no puedo obtenerlo, seguimos con
		 * el siguiente certificado
		 */

		if ( CRYPTO_X509_Get_KeyUsage(BLOQUE_CERTPROPIO_Get_Objeto(bloqueCert),
								      BLOQUE_CERTPROPIO_Get_Tam(bloqueCert),
								      &certKu) == 0 )
		{
			
			/* Busco blob asociado al certificado propio
			 */

			if ( LIBRT_LeerTipoBloqueCrypto(&hClauer, BLOQUE_PRIVKEY_BLOB, 1, bloqueBlob, &nbBlob) != 0 ) {
				LIBRT_FinalizarDispositivo(&hClauer);
				throw ERR_CLREP;
			}

			while ( nbBlob != -1 ) {

				if ( memcmp(BLOQUE_CERTPROPIO_Get_Id(bloqueCert),
							BLOQUE_PRIVKEYBLOB_Get_Id(bloqueBlob),
							20) == 0 ) 
				{

					/* Compruebo si el tipo del blob es correcto o no
					 */

					BLOBHEADER *blobHeader  = ( BLOBHEADER *) BLOQUE_PRIVKEYBLOB_Get_Objeto(bloqueBlob);
					unsigned short algId = AT_KEYEXCHANGE;
					int reparar = 0;

					if ( (blobHeader->aiKeyAlg == AT_SIGNATURE) || (blobHeader->aiKeyAlg == CALG_RSA_SIGN ) ) {

						/* Si el blob es AT_SIGNATURE entonces el kusage del certificado
						 * debiera ser DIGITAL_SIGNATURE, KEY_CERT_SIGN o CRL_SIGN
						 */
						
						if ( ! ( (certKu & CRYPTO_KU_DIGITAL_SIGNATURE) ||
							     (certKu & CRYPTO_KU_KEY_CERT_SIGN) ||
								 (certKu & CRYPTO_KU_CRL_SIGN) ) )
						{
							/* Establecemos el blob como AT_KEYEXCHANGE */
							algId = AT_KEYEXCHANGE;		
							reparar = 1;
						}

					} else if ( (blobHeader->aiKeyAlg == AT_KEYEXCHANGE) || ( blobHeader->aiKeyAlg == CALG_RSA_KEYX) ) {

						if ( (certKu & CRYPTO_KU_DIGITAL_SIGNATURE) ||
							 (certKu & CRYPTO_KU_KEY_CERT_SIGN) ||
							 (certKu & CRYPTO_KU_CRL_SIGN) )
						{
							/* Establecemos el blob como AT_KEYEXCHANGE */
							algId = AT_SIGNATURE;
							reparar = 1;
						}
					}

					/* Si hay que reparar, busco el container asociado para actualizarlo tambi�n
					 */
					
					if ( reparar) {

						unsigned char bloqueContainer[TAM_BLOQUE];
						int containerUpdated;
						unsigned char zeroId[20];
						long nbContainer;

						memset(zeroId, 0, 20);

						if ( LIBRT_LeerTipoBloqueCrypto(&hClauer, BLOQUE_KEY_CONTAINERS, 1, bloqueContainer, &nbContainer) != 0 ) {
							LIBRT_FinalizarDispositivo(&hClauer);
							throw ERR_CLREP;
						}

						containerUpdated = 0;
						while ( !containerUpdated && nbContainer != -1 ) {
				
							INFO_KEY_CONTAINER lstContainers[NUM_KEY_CONTAINERS];
							unsigned int tamLstContainers, c;
					
							if ( BLOQUE_KeyContainer_Enumerar(bloqueContainer, lstContainers, &tamLstContainers) != 0 ) {
								LIBRT_FinalizarDispositivo(&hClauer);
								throw ERR_CLREP;
							}
					
							for ( c = 0 ; !containerUpdated && (c < tamLstContainers) ; c++ ) {

								if ( memcmp(lstContainers[c].idExchange,
											BLOQUE_PRIVKEYBLOB_Get_Id(bloqueBlob),
											20) == 0 )
								{
									if ( memcmp(lstContainers[c].idSignature,
												zeroId,
												20) != 0 )
									{
										LIBRT_FinalizarDispositivo(&hClauer);
										throw ERR_CLREP;
									}

									BLOQUE_KeyContainer_Establecer_ID_Signature(bloqueContainer, lstContainers[c].nombreKeyContainer, lstContainers[c].idExchange);
									BLOQUE_KeyContainer_Establecer_ID_Exchange(bloqueContainer, lstContainers[c].nombreKeyContainer, zeroId);

									containerUpdated = 1;

								} else if ( memcmp(lstContainers[c].idSignature,
												   BLOQUE_PRIVKEYBLOB_Get_Id(bloqueBlob),
												   20) == 0 )
								{
									if ( memcmp(lstContainers[c].idExchange,
												zeroId,
												20) != 0 )
									{
										LIBRT_FinalizarDispositivo(&hClauer);
										throw ERR_CLREP;
									}

									BLOQUE_KeyContainer_Establecer_ID_Exchange(bloqueContainer, lstContainers[c].nombreKeyContainer, lstContainers[c].idSignature);
									BLOQUE_KeyContainer_Establecer_ID_Signature(bloqueContainer, lstContainers[c].nombreKeyContainer, zeroId);
									

									containerUpdated = 1;
								}
							} // for containers

							if (  containerUpdated ) {
	
								/* Escribo el bloque container
								 */

								if ( LIBRT_EscribirBloqueCrypto(&hClauer, nbContainer, bloqueContainer) != 0 ) {
									LIBRT_FinalizarDispositivo(&hClauer);
									throw ERR_CLREP;
								}

							} else {
								if ( LIBRT_LeerTipoBloqueCrypto(&hClauer, BLOQUE_KEY_CONTAINERS, 1, bloqueContainer, &nbContainer) != 0 ) {
									LIBRT_FinalizarDispositivo(&hClauer);
									throw ERR_CLREP;
								}
							}
						}

						/* Escribo el bloque blob
						 */

						*((WORD *)(BLOQUE_PRIVKEYBLOB_Get_Objeto(bloqueBlob) + 2 + sizeof(WORD))) = algId;

						if ( LIBRT_EscribirBloqueCrypto(&hClauer, nbBlob, bloqueBlob) != 0 ) {
							LIBRT_FinalizarDispositivo(&hClauer);
							throw ERR_CLREP;
						}

					} // if reparar

				}  // if blob encontrado

				CRYPTO_SecureZeroMemory(bloqueBlob, TAM_BLOQUE);

				if ( LIBRT_LeerTipoBloqueCrypto(&hClauer, BLOQUE_PRIVKEY_BLOB, 0, bloqueBlob, &nbBlob) != 0 ) {
					LIBRT_FinalizarDispositivo(&hClauer);
					throw ERR_CLREP;
				}

			} // while nbBlob
		}

		if ( LIBRT_LeerTipoBloqueCrypto(&hClauer, BLOQUE_CERT_PROPIO, 0, bloqueCert, &nbCert) != 0 ) {
			LIBRT_FinalizarDispositivo(&hClauer);
			throw ERR_CLREP;
		}
	} // while nbCert

  }
  catch ( int err ) {
	ret = err;
  }

  CRYPTO_SecureZeroMemory(bloqueBlob, TAM_BLOQUE);

  return ret;

}




void CRepExchangeSignature::getDescription ( char desc[CLREP_MAX_DESC] )
{



}

