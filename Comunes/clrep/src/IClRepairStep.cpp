#include "IClRepairStep.h"

IClRepairStep::IClRepairStep ( void ) : _pwdCb(NULL) 
{ 
  *_pwd = 0;
}



IClRepairStep::~IClRepairStep ( void )
{
  if ( *_pwd ) 
    destroyPwd();
}




int IClRepairStep::setPwd ( char pwd[CLREP_MAX_PASS] )
{
  if ( *_pwd ) {
    destroyPwd();
    *_pwd = 0;
  }
  
  ::strncpy(_pwd, pwd, CLREP_MAX_PASS);
  _pwd[CLREP_MAX_PASS-1] = 0;
  
  return CLREP_OK;
}




void IClRepairStep::destroyPwd ( void )
{
  if ( *_pwd ) {
    size_t pwdSize;
    pwdSize = ::strlen(_pwd);
#ifdef WIN32
    SecureZeroMemory(_pwd, pwdSize);
#endif
    *_pwd = 0;
  }

}



string IClRepairStep::getPwd ( void )
{
  return string(_pwd);
}

