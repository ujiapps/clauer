/* Fichero: CRepIden.h
 *
 * Depende de: 
 *
 * Deficiencia que resuelve:
 *
 *		En Clauers con formato v1 el c�lculo de los indentificadores de los blobs
 *      se realizaba de manera incorrecta. Esto provoca que los certificados no
 *		salgan correctamente listados en Crypto API.
 *
 *      Este paso de reparaci�n recalcula los identificadores de certificados
 *      propios, blobs y llave
 *
 * Algoritmo de resoluci�n:
 *
 *		  PARA certPropio EN Certificados Propios del Clauer HACER
 *             id <- IDCERT(certPropio)
 *             bloqueCert[id] <- id
 *             SobreEscribirBloque(bloqueCert)
 *        FP
 *             
 *        PARA llavePEM EN Llaves PEM del Clauer HACER
 *             id <- IDLLAVE(certPropio)
 *             bloqueLlavePEM[id] <- id
 *             SobreEscribirBloque(bloqueLlavePEM)
 *        FP
 *
 *        PARA blob EN blobs del Clauer HACER
 *             id <- IDBLOB(blob)
 *             Actualizar key container con el nuevo identificador
 *             bloqueBlob[id] <- id
 *             SobreEscribirBloque(bloqueBlob)
 *        FP
 */

#ifndef __CLAUER_CREPIDEN_H__
#define __CLAUER_CREPIDEN_H__

#include "IClRepairStep.h"


class CRepIden : public IClRepairStep 
{

public:
  CRepIden  ( CLREP_PWD_CB pwdCb = NULL );
  ~CRepIden ( void );

  int test            ( char *device );
  int repair          ( char *device );
  void getDescription ( char desc[CLREP_MAX_DESC] );

};

#endif
