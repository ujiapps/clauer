/* Fichero: CRepExchangeSignature.h
 *
 * Depende de: 
 *
 * Deficiencia que resuelve:
 *
 *        Debido a un bug en la LIBIMPORT, algunos clauers ten�an mal asociados los tipos
 *        de blobs (AT_EXCHANGE y AT_SIGNATURE) con el prop�sito del certificado propio
 *        al que estaban asociados. La forma correcta de asociar es la siguiente:
 *
 *             Si el KEY_USAGE del certificado es firma entonces el blob debe ser AT_SIGNATURE
 *             en otro caso el blob debe ser AT_KEYEXCHANGE
 *
 * Algoritmo de resoluci�n:
 *
 *             PARA certPropio en Certs Propios del Clauer ENTONCES
 *                 
 *                  blob <- Blob asociado al cert propio
 *                  kc <- key container asociado al blob
 *                  SI keyUsage(certPropio) == FIRMA Y tipoBlob(blob) != AT_SIGNATURE ENTONCES
 *                       blob[tipo] <- AT_SIGNATURE
 *                       kc[AT_SIGNATURE] <- id_blob
 *                       kc[AT_KEYEXCHANGE] <- 0
 *                  FS
 *
 *             FP
 */

#ifndef __CLAUER_CREPEXCHANGESIGNATURE_H__
#define __CLAUER_CREPEXCHANGESIGNATURE_H__

#include "IClRepairStep.h"

class CRepExchangeSignature : public IClRepairStep
{

public:
  CRepExchangeSignature  ( CLREP_PWD_CB pwdCb = NULL );
  ~CRepExchangeSignature ( void );

  int test            ( char *device );
  int repair          ( char *device );
  void getDescription ( char desc[CLREP_MAX_DESC] );

};

#endif
