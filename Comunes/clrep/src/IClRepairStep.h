/* Interfaz que debe implementar un paso de arreglo del clauer
 */

#ifndef __CLAUER_I_CL_REPAIR_H__
#define __CLAUER_I_CL_REPAIR_H__

#include <iostream>
#ifdef WIN32
#include <windows.h>
#endif

#define CLREP_MAX_PASS   128    // Tama�o m�ximo para una password
#define CLREP_MAX_DESC   256    // Tama�o m�ximo para la descripci�n de un
                                // paso de reparaci�n
#define CLREP_TEST_OK      1    // El clauer es correcto para este paso de reparaci�n
#define CLREP_TEST_REPAIR  2    // El clauer necesita ser reparado
#define CLREP_TEST_ERROR   3    // Se produjo un error durante el test

#define ERR_CLREP          0    // Se produjo un error en la funci�n
#define CLREP_OK           1    // La funci�n se ejecut� corretamente
#define CLREP_NEED_PASS    2    // La funci�n necesita que se suministre una password
                                // para poder completarse
#define ERR_CLREP_PASS              3    // Ocurri� un error pidiendo password
#define ERR_CLREP_CANT_INITIALIZE   4    // No se pudo iniciar dispositivo
#define ERR_CLREP_OUT_OF_MEMORY     5    // Out of memory


using namespace std;

typedef int ( *CLREP_PWD_CB ) ( char pwd [CLREP_MAX_PASS] );


/* Interfaz que deben implementar los pasos de reparaci�n
 */

class IClRepairStep {
 protected:

  char _pwd[CLREP_MAX_PASS];
  CLREP_PWD_CB _pwdCb;

  void destroyPwd ( void );

 public:

  IClRepairStep ( void );
  virtual ~IClRepairStep ( void );

  int setPwd ( char pwd[CLREP_MAX_PASS] );
  string getPwd ( void );

  virtual int  test           ( char *device ) = 0;
  virtual int  repair         ( char *device ) = 0;
  virtual void getDescription ( char desc[CLREP_MAX_DESC] ) = 0;

};


#endif
