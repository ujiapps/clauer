#ifndef __CREPSTEPLIST_H__
#define __CREPSTEPLIST_H__

#include "IClRepairStep.h"




class CRepStepList {
 public:
  static const int totalSteps = 3;    // N�mero total de pasos de reparaci�n
                                      // existentes actualmente. Si se a�ade
                                      // uno nuevo, este n�mero hay que
                                      // actualizarlo
  
 protected:

  IClRepairStep *_rsl[totalSteps+1]; // Lista con todos los pasos de reparaci�n
                                     // que se van a dar
  int _tn;                           // El n�mero de test que estamos pasando 
                                     // en la actualidad

 public:
  CRepStepList(CLREP_PWD_CB pwdCb = NULL) throw ( int );
  ~CRepStepList(void);

  void first ( void );
  IClRepairStep * next ( void );

  IClRepairStep * operator[] (int pos);

};

#endif
