#include "CRepStepList.h"

#include "CRepIden.h"
#include "CLRepPEMKeys.h"
#include "CRepExchangeSignature.h"

/*! \brief Constructor de la lista.
 *
 * El contructor toma como par�metro opcional una callback para pedir
 * passwords. Inicializa la lista de pasos de reparaci�n. 
 *
 * \param pwdCb
 *        Callback para pedir contrase�as.
 *
 * \warning
 *        Lanza una excepci�n de tipo entero en caso de error
 *
 */

CRepStepList::CRepStepList (CLREP_PWD_CB pwdCb /* = NULL */) 
{

  // Inicializamos la lista de pasos de reparaci�n. El orden de los
  // tests es relevante de modo que hay que ir con cuidado cuando
  // se actualice. Es decir, un test de reparaci�n puede asumir un
  // estado del clauer resultante de un arreglo de un test anterior
  // El �ltimo elemento de la lista TIENE que ser NULL

  _rsl[0] = new CRepIden();
  if ( ! _rsl[0] )
	  throw 1;

  _rsl[1] = new CRepPemKeys();
  if ( ! _rsl[1] )
    throw 1;

  _rsl[2] = new CRepExchangeSignature();
  if ( ! _rsl[0] )
	  throw 1;

  _rsl[3] = NULL;

  _tn = 0;
  
}


/*! \brief Destruye la lista de pasos de reparaci�n.
 *
 * Destruye la lista de pasos de reparaci�n.
 */

CRepStepList::~CRepStepList ( void )
{
  int i = 0;
  while ( _rsl[i] ) {
    delete _rsl[i];
    i++;
  }
}



/*! \brief Posiciona el puntero del paso de reparaci�n actual
 *         al primer paso de reparaci�n.
 *
 * Posiciona el puntero del paso de reparaci�n actual al primer
 * paso de reparaci�n
 */

void CRepStepList::first ( void )
{
  _tn = 0;
}


/*! \brief Devuelve el siguiente paso de reparaci�n
 */

IClRepairStep * CRepStepList::next ( void )
{
  if ( _tn >= totalSteps ) {
    return NULL;
  } else {
    return _rsl[_tn++];
  }
}


/*! \brief Devuelve el paso de reparaci�n pos-�simo
 */

IClRepairStep * CRepStepList::operator[] ( int pos )
{
  if ( pos >= totalSteps || pos < 0 )
    return NULL;
  else
    return _rsl[pos];
}

