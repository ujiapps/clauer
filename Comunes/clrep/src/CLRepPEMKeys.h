/* Fichero: CLRepPEMKeys.h
 *
 * Depende de: 
 *
 * Deficiencia que resuelve:
 *
 *        En clauers con formato 1 y debido a un bug en el sistema operativo del
 *        clauer, las llaves privadas PEM asociadas a un certificado (en la importación)
 *        de un pkcs#12, por ejemplo, no son importadas, impidiendo realizar la
 *        autenticación mediante el módulo PKCS#11. Las aplicaciones que utilizan
 *        Microsoft Crypto API no se ven afectadas ya que utilizan bloques de tipo
 *        BLOB que sí se importan correctamente.
 *
 * Algoritmo de resolución:
 *
 *        PARA certPropio EN Certificados Propios del Clauer HACER
 *
 *             llavePEM <- Llave PEM asociada a certPropio;
 *             SI ! llavePEM ENTONCES
 *                 blob <- Blob asociado a certPropio
 *                      SI ! blob ENTONCES
 *                          Borrar certPropio
 *                      SINO
 *                          llavePEM <- BLOB2PEM(blob)
 *                          Insertar llavePEM en Clauer
 *                      FINSI
 *             FINSI
 *        FIN PARA
 */

#ifndef __CLAUER_CLREPPEMKEYS_H__
#define __CLAUER_CLREPPEMKEYS_H__

#include "IClRepairStep.h"

class CRepPemKeys : public IClRepairStep {
 public:
  CRepPemKeys  ( CLREP_PWD_CB pwdCb = NULL );
  ~CRepPemKeys ( void );

  int test            ( char *device );
  int repair          ( char *device );
  void getDescription ( char desc[CLREP_MAX_DESC] );

};

#endif

