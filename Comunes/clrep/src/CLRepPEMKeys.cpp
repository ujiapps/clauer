#include "CLRepPEMKeys.h"

#ifdef WIN32
#include <windows.h>
#endif

#include <LIBRT/libRT.h>
#include <CRYPTOWrapper/CRYPTOWrap.h>


CRepPemKeys::CRepPemKeys ( CLREP_PWD_CB pwdCb /* = NULL */ )
{
  _pwdCb = pwdCb;
}


CRepPemKeys::~CRepPemKeys ( void )
{
}



int CRepPemKeys::test ( char *device )
{
  return CLREP_TEST_OK;
}


/* Para cada certificado propio busco la llave privada PEM asociada.
 * Si no se encuentra, busco blob del CryptoAPI y lo transformo a 
 * formato PEM 
 */

int CRepPemKeys::repair ( char *device )
{
  unsigned char bloqueLlave[TAM_BLOQUE];
  long nbLlave;
  int bExisteLlavePEM;
  unsigned char bloqueCert[TAM_BLOQUE];
  long nbCert;
  unsigned char *llavePEM = NULL;
  unsigned long tamLlavePEM;
  unsigned char bloqueBlob[TAM_BLOQUE];
  long nbBlob;
  int bExisteBlob;

  USBCERTS_HANDLE hClauer;

  int ret = CLREP_OK;

  /* Este test necesita iniciar una sesi�n autenticada
   * contra el CLOS
   */

  if ( ! _pwd ) {
    if ( ! _pwdCb ) {
      return CLREP_NEED_PASS;
    }
    if ( _pwdCb(_pwd) != 0 ) {
      *_pwd = 0;
      return ERR_CLREP_PASS;
    }
  }

  /* Iniciamos sesi�n
   */

  try {
    
    if ( LIBRT_IniciarDispositivo((unsigned char *) device, _pwd, &hClauer) != 0 ) {
		throw ERR_CLREP_CANT_INITIALIZE;
    }  
    
    /* Para cada certificado propio
     *
     *            Buscar llave privada asociada
     *            SI no existe ENTONCES
     *                BUSCAR BLOB asociado
     *                SI no existe ENTONCES
     *                    Borrar certificado
     *                SINO
     *                    Transformar blob en PEM
     *                    Almacenar
     *                FINSI
     *            FINSI
     *            
     */
    
    if ( LIBRT_LeerTipoBloqueCrypto(&hClauer, BLOQUE_CERT_PROPIO, 1, bloqueCert, &nbCert) != 0 ) {
      LIBRT_FinalizarDispositivo(&hClauer);
      throw ERR_CLREP;
    }

    if ( nbCert == -1 ) {
      LIBRT_FinalizarDispositivo(&hClauer);
      throw CLREP_OK;
    }

    while ( nbCert != -1 ) {

      /* Compruebo si tiene llave privada asociada o no
       */


	  bExisteLlavePEM = 0;
      if ( LIBRT_LeerTipoBloqueCrypto(&hClauer, BLOQUE_LLAVE_PRIVADA, 1, bloqueLlave, &nbLlave) != 0 ) {
		  LIBRT_FinalizarDispositivo(&hClauer);
		  throw ERR_CLREP;
      }

      while ( (nbLlave != -1) && !bExisteLlavePEM ) {
		if ( memcmp(BLOQUE_CERTPROPIO_Get_Id(bloqueCert), BLOQUE_LLAVEPRIVADA_Get_Id(bloqueLlave), 20) == 0 )
			bExisteLlavePEM = 1;
		else
			if ( LIBRT_LeerTipoBloqueCrypto(&hClauer, BLOQUE_LLAVE_PRIVADA, 0, bloqueLlave, &nbLlave) != 0 ) {
				LIBRT_FinalizarDispositivo(&hClauer);
				throw ERR_CLREP;
		    }
      }

      if ( ! bExisteLlavePEM ) {

		/* Buscamos un blob asociado y lo transformamos
		 */


		if ( LIBRT_LeerTipoBloqueCrypto(&hClauer, BLOQUE_PRIVKEY_BLOB, 1, bloqueBlob, &nbBlob) != 0 ) {
		  LIBRT_FinalizarDispositivo(&hClauer);
		  throw ERR_CLREP;
		}

		bExisteBlob = 0;
		while ( !bExisteBlob && (nbBlob != -1) ) {
		  if ( memcmp(BLOQUE_CERTPROPIO_Get_Id(bloqueCert), BLOQUE_PRIVKEYBLOB_Get_Id(bloqueBlob), 20) == 0 ) {
			bExisteBlob = 1;
		  } else {
			if ( LIBRT_LeerTipoBloqueCrypto(&hClauer, BLOQUE_PRIVKEY_BLOB, 0, bloqueBlob, &nbBlob) != 0 ) {
			  LIBRT_FinalizarDispositivo(&hClauer);
			  throw ERR_CLREP;
			}
		  }
		}

	/* Si el blob no existe borro el certificado
	 */

	if ( ! bExisteBlob ) {	  
	  LIBRT_BorrarBloqueCrypto(&hClauer, nbCert);
	} else {

	  /* Convierto el blob a llave PEM y la inserto
	   */

	  
	  if ( CRYPTO_BLOB2LLAVE(BLOQUE_PRIVKEYBLOB_Get_Objeto(bloqueBlob),
				 BLOQUE_PRIVKEYBLOB_Get_Tam(bloqueBlob),
				 NULL,
				 &tamLlavePEM) != 0 )
	    {
	      LIBRT_FinalizarDispositivo(&hClauer);
	      throw ERR_CLREP;
	    }


	  llavePEM = new unsigned char [tamLlavePEM];
	  if ( ! llavePEM ) {
	    LIBRT_FinalizarDispositivo(&hClauer);
	    throw ERR_CLREP;
	  }

	  if ( CRYPTO_BLOB2LLAVE(BLOQUE_PRIVKEYBLOB_Get_Objeto(bloqueBlob),
				 BLOQUE_PRIVKEYBLOB_Get_Tam(bloqueBlob),
				 llavePEM,
				 &tamLlavePEM) != 0 )
	    {
	      LIBRT_FinalizarDispositivo(&hClauer);
	      throw ERR_CLREP;
	    }	  
	  
	  CRYPTO_SecureZeroMemory(bloqueBlob, TAM_BLOQUE);
	  unsigned char idLlave[20];

	  if ( CRYPTO_LLAVE_PEM_Id ( llavePEM, tamLlavePEM, 1, NULL, idLlave) != 0 ) {
	    LIBRT_FinalizarDispositivo(&hClauer);
	    throw ERR_CLREP;
	  }

	  /* Construyo un nuevo bloque y lo inserto
	   */
	  
	  CRYPTO_SecureZeroMemory(bloqueLlave, TAM_BLOQUE);

	  BLOQUE_Set_Cifrado(bloqueLlave);
	  BLOQUE_LLAVEPRIVADA_Nuevo(bloqueLlave);
	  BLOQUE_LLAVEPRIVADA_Set_Tam(bloqueLlave, tamLlavePEM);
	  BLOQUE_LLAVEPRIVADA_Set_Id(bloqueLlave, idLlave);
	  BLOQUE_LLAVEPRIVADA_Set_Objeto(bloqueLlave, llavePEM, tamLlavePEM);

	  CRYPTO_SecureZeroMemory(llavePEM, tamLlavePEM);
	  delete [] llavePEM;
	  llavePEM = NULL;

	  if ( LIBRT_InsertarBloqueCrypto(&hClauer, bloqueLlave, &nbLlave) != 0 ) {
	    LIBRT_FinalizarDispositivo(&hClauer);
	    throw ERR_CLREP;
	  }

	  CRYPTO_SecureZeroMemory(bloqueLlave, TAM_BLOQUE);
	}
	
  }

  if ( LIBRT_LeerTipoBloqueCrypto(&hClauer, BLOQUE_CERT_PROPIO, 0, bloqueCert, &nbCert) != 0 ) {
	LIBRT_FinalizarDispositivo(&hClauer);
	throw ERR_CLREP;
   }
  }

  } catch ( int err ) {
    ret = err;
  }


  if ( llavePEM ) {
	  CRYPTO_SecureZeroMemory(llavePEM, tamLlavePEM);
	  delete [] llavePEM;
  }
  CRYPTO_SecureZeroMemory(bloqueLlave, TAM_BLOQUE);
  CRYPTO_SecureZeroMemory(bloqueBlob, TAM_BLOQUE);

  return ret;

}







void CRepPemKeys::getDescription ( char desc[CLREP_MAX_DESC] ) 
{


}
