#ifndef __SESSION_H__
#define __SESSION_H__

#include "transport.h"
#ifdef DEBUG
#include "log.h"
#endif
#include "common.h"
#include "block.h"
#include <clio.h>

enum session_type { SESS_RDONLY,
		    SESS_RDWR };

typedef enum session_type session_type_t;

struct session {
    session_type_t type;       // The type of the session
    trans_object_t tr;         // The transport object to the client
    clauer_handle_t hClauer;   // The handle to the clauer object
    block_info_t *ib;           // The info block of the clauer
    char *pwd;            // The clauer's passphrase
    int opt_cryf;         // List cryf files on mounted system. ( floppy devices are not include ) 
    int opt_floppy;       // List cryf files on floppy devices.
	int admin;            // Indicates whether the client is admin or not. 
};

typedef struct session session_t;


int SESSION_New ( session_type_t type,
		  trans_object_t tr,
		  clauer_handle_t *hClauer,
		  char *pwd,
		  block_info_t *ib,
		  options_t * opt,
		  session_t **s);


#define LOG_TO 1  /* 1 level */
                  /* 2  */

#endif
