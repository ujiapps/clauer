#include <stdio.h>
#include <stdlib.h>

#include <LIBRT/libRT.h>

#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>




int main( void ){
  
  USBCERTS_HANDLE hClauer;
  int nDisp=0,i;
  long  numBloque;
  char dev[10];
  unsigned char  *buffer;
  char *pass;
  unsigned long bytesBuffer;
  unsigned char hwIdSistema[16], hwIdDispositivo[16];

  /* 2. Iniciamos una sesi�n no autenticada */
  
  strncpy(dev, "/dev/sdb",8);
  dev[8]= '\0';
  
  if ( LIBRT_IniciarDispositivo((unsigned char *) dev, NULL, &hClauer) != 0 ) {
    fprintf(stderr, "[E] [+] Initializing device. \n");
    exit(1);
  }
  else{
    printf("[+] [+] Iniciada sesi�n no autenticada.\n");
  }

  /* 5. Invocamos funci�n 14, GetHardwareId */
  if ( LIBRT_ObtenerHardwareId (&hClauer, hwIdDispositivo, hwIdSistema ) != 0 ) {
    fprintf(stderr, "[E] [E] Obtaining HardwareId \n");
    exit(-1);
  }
  else{
      printf("Device Hardware clauer ID= 0x");
      for (i=0; i<16; i++){
	  printf("%02x",hwIdDispositivo[i]);
      }
      printf("\n");

      printf("System Hardware clauer ID= 0x");
      for (i=0; i<16; i++){
	  printf("%02x",hwIdSistema[i]);
      }
      printf("\n");
  }
  
  if ( LIBRT_FinalizarDispositivo (&hClauer)!= 0 ) {
      fprintf(stderr, "[E] [E] Finalizing Device \n");
  }
  return 0;
  
}
