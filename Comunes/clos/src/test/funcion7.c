#include <stdio.h>
#include <stdlib.h>

#include <LIBRT/libRT.h>

#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>




int main( void ){
  
  USBCERTS_HANDLE hClauer;
  int nDisp=0,i;
  long  numBloque;
  unsigned char *dev = NULL, *buffer;
  char *pass;
  unsigned long bytesBuffer;
  unsigned char block[TAM_BLOQUE];

  /* 2. Iniciamos una sesi�n no autenticada */
  if ( LIBRT_IniciarDispositivo('a', NULL, &hClauer) != 0 ) {
    fprintf(stderr, "[E] [+] Initializing device. \n");
    exit(1);
  }
  else{
    printf("[+] [+] Iniciada sesi�n no autenticada.\n");
  }

  /* 5. Invocamos funci�n 13, BorrarBloqueCrypto */
  numBloque=3;
  if ( LIBRT_BorrarBloqueCrypto (&hClauer, numBloque)!= 0 ) {
    fprintf(stderr, "[E] [E] Deleting crypto block %d. \n",numBloque);
    // Error es lo correcto, debemos continuar.
  }
  else{
    printf("[+] [E] Deleted Crypto Block on position= %d .\n",numBloque);
  }
  
  
  /* 6. Invocamos funci�n 7 con tipo 140 */
  if ( LIBRT_LeerTipoBloqueCrypto (&hClauer, 1, 1,block , &numBloque) != 0 ) {
    fprintf(stderr, "[E] [E] Reading crypto block type 140.\n");
    // Error es lo correcto, debemos continuar.
  }
  else{
    printf("[+] [E] Readed Crypto Block of type 140 on position= %d.\n",numBloque);
  }
}
