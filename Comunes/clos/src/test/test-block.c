#include <stdio.h>
#include <stdlib.h>

#include "../block.h"
#include "../../io/io.h"
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <string.h>
#define MAX 100

struct infoZone{
  char idenString[40];
  unsigned char id[20];
  int nrsv;
  int currentBlock;
  int totalBlocks;
  int formatVersion;
};



unsigned char buffer[10240];
unsigned char bufferAux[10240];


int main ( void )
{

  /*clauer_handle_t hClauer;*/
  block_object_t b;
  struct infoZone iz;
  char pass[10]= "caca";
  char pass2[10]="pedo";

  buffer[0]= 0x0;
  buffer[1]= 0x0;

  int fp= open("/dev/urandom",O_RDONLY);
  int izfp= open("infoZone",O_RDONLY);
  read(izfp,&iz,10240);

  CRYPTO_Ini();
  int i;
  int res;
  
  printf("\nTesting block_Object cipher and decipher object: \n");
  for (i=0;i<MAX;i++){
    pass[i%10]++;
    pass2[i%10]++;
    
    read(fp,buffer,10240);
    memcpy(bufferAux,buffer,10240);
  
    if ( (res=BLOCK_OBJECT_Cipher((block_object_t *)buffer, pass, (block_info_t *)&iz))!= 0 ){
      fprintf(stderr,"  ERROR: En primer cifrado razon: %d\n",res);
      return 1;
       
      }
   	
    if (!memcmp(buffer,bufferAux,8)){
      fprintf(stderr,"  ERROR: Las cabeceras no coinciden despues del cifrado.\n");
      return 1;
   
    }
   
    if ( (res=BLOCK_OBJECT_Decipher((block_object_t *)buffer, pass, (block_info_t *)&iz)) !=0 ){
      fprintf(stderr,"  ERROR: En primer descifrado razon:%d\n", res);
      return 1;
    }


    if ( (res=BLOCK_OBJECT_Cipher((block_object_t *)buffer, pass2, (block_info_t *)&iz)) != 0 ){
      fprintf(stderr,"  ERROR: En segundo cifrado razon: %d\n",res);
      return 1;
    }

    if ( (res=BLOCK_OBJECT_Decipher((block_object_t *)buffer, pass2, (block_info_t *)&iz)) != 0 ){ 
      fprintf(stderr,"  ERROR: En segundo descifrado razon: %d\n",res);
      return 1;
    }
   
    if (!memcmp(buffer, bufferAux,10240)){
       fprintf(stderr,"  ERROR: los bloques no coinciden.\n");
      return 1;
   }
    else{
	printf("\r");
    	printf("[+] Bloque %d de %d			[OK]",i,MAX);
    	fflush(stdout);
    }
  }
  printf("\n");
  	
  /*if ( IO_Open("/dev/sda", &hClauer) != CLOS_SUCCESS ) {
    fprintf(stderr, "ERROR opening the device");
    return 1;
  }
*/
  
 CRYPTO_Fin();

  return 0;
}
