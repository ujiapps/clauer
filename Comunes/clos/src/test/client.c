#include <stdio.h>
#include <stdlib.h>

#include <LIBRT/libRT.h>

#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>



int main ( int argc, char **argv)
{
	USBCERTS_HANDLE hClauer;
	int nDisp,i;
	unsigned char *dev[128], *buffer;
	char *pass;
	unsigned long bytesBuffer;
	unsigned char block[TAM_BLOQUE];
	
	

	LIBRT_Ini();

	/* First function
         */

	printf("[T1] Trying an anonymous session... \n");
	fflush(stdout);
	
        char *  version; 
	
	if ( LIBRT_ObtenerVersion(&version) != 0 ) {
	  fprintf(stderr, "[ERROR] Imposible obtener version\n");
        return 1;
	}

        printf("La versi�n obtenida es: %s", version);

        exit(-1); 
	
	if ( nDisp==0 ){
	  printf("[E] No clauers on system... \n");
	  return 1;
	}

 	if ( LIBRT_IniciarDispositivo(dev[0], NULL, &hClauer) != 0 ) {
		fprintf(stderr, "[ERROR] Initializing device\n");
		exit(1);
	}	

	printf("[OK]\n");
	printf("     Device ID: ");
	for ( i = 0 ; i < 20 ; i++ ) 
		printf("%02x", hClauer.idDispositivo[i]);
	printf("\n");	

	printf("[T2] Reading zona reservada... ");
	fflush(stdout);

	if ( LIBRT_LeerZonaReservada(&hClauer, NULL, &bytesBuffer) != 0 ) {
		fprintf(stderr, "[ERROR]\n");
		exit(1);
	}

	buffer = ( unsigned char * ) malloc ( bytesBuffer );

	if ( LIBRT_LeerZonaReservada(&hClauer, buffer, &bytesBuffer) != 0 ) {
		fprintf(stderr, "[ERROR]\n");
		exit(1);
	}

	printf("[OK]\n");

	printf("[T3] Writing reserved zone (should fail)... ");
	fflush(stdout);

	memset(buffer,0,bytesBuffer);

	/*printf ("Enviando %d bytesBuffer\n", bytesBuffer);
	if ( LIBRT_EscribirZonaReservada(&hClauer, buffer, bytesBuffer) != 0 ) {
		printf("[OK] (No puedo escribir)\n");
	} else {
		fprintf(stderr, "[ERROR]\n");
		exit(1);
	}
	*/

	printf("[T4] Reading info zone... ");
	fflush(stdout);
	
	if ( LIBRT_LeerBloqueIdentificativo ( &hClauer, block) != 0 ) {
		fprintf(stderr, "[ERROR] leyendo el identificativo del clauer\n");
		exit(1);
	}

	printf("[OK]\n");
	printf("     ID: ");
	for ( i = 0 ; i < 20 ; i++ ) 
		printf("%02x", *(block+40+i));
	printf("\n");
	fflush(stdout);
	

	/* We're trying an authenticated session now
	 */

	pass = getpass("     CLAUER'S PASSWORD: ");

        printf("[T3] Trying an authenticated session... ");
        fflush(stdout);

	if ( LIBRT_IniciarDispositivo(dev[0], pass, &hClauer) != 0 ) {
		fprintf(stderr, "[ERROR]\n");
		exit(1);
	}

	printf("[OK]\n");


	printf("[TX] Closing session... ");
	if ( LIBRT_FinalizarDispositivo(&hClauer) != 0 ) {
		fprintf(stderr, "[ERROR]\n");
		exit(1);
	}
	printf("[OK]\n");



}


