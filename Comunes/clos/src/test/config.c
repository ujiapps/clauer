#include <stdio.h>
#include <string.h>
	
#define BUFF_SIZE 1024

int main(int  argc, char ** argv){
  int fsize, type, i, idx=0, state=1;
  FILE * fd;
  char buff[BUFF_SIZE+1],aux_buff[BUFF_SIZE+1], version[10];
  char * p, * nl, * eq, *ini, *end;

  printf("Parsing file: %s\n",argv[1]);
  fd= fopen(argv[1],"r"); 
  
  fsize= fread(buff,1,BUFF_SIZE,fd);

  if (fsize<BUFF_SIZE) 
    buff[fsize]= 0;
  else
    buff[BUFF_SIZE]=0;
  
  //strip out comments 
  //state=1 -> copy
  //state=0 -> not to copy
 
  for(i=0; i<BUFF_SIZE; i++){
    if (buff[i]=='#') 
        state=0;
    if (buff[i]=='\n')
        state=1;
    if (state)       
        aux_buff[idx++]=buff[i];
    
  }
  aux_buff[idx]=0;
 
 printf("\n\n\n%s\n\n\n", aux_buff);

  //Parse for type
  p= strstr(buff,"type"); 
  if (!p){
    fprintf(stderr, "type not found in config, assuming 0.");
    type=0; 
  } 
  else{
   nl= strstr(p,"\n");
   eq= strstr(p, "=");
   if (nl && eq){
     type=atoi(eq+1);
   }
  }

  //Parse for version 
  p= strstr(buff,"version"); 
  if (!p){
    fprintf(stderr, "version not found in config, setting 0.0.0");
    strncpy(version,"0.0.0",5);
  }
  else{
   eq= strstr(p, "=");
   ini=eq+1; 
   end=eq+1;
   while(*end!='#' && *end!='\n' && *end!=0) end++; 
   *end=0;
   //Take out whitespaces:
   // leading ones
   while ( *ini==' ' && ini!=end )ini++;
   // Trailing ones
   while ( end!=ini && *(end-1)==' ') {*(end-1)=0; end--;}
   strncpy(version,ini,10);
  } 

  printf("%s#%d",version,type);
}
