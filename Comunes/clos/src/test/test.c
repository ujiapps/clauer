#include <stdio.h>
#include <stdlib.h>

#include <LIBRT/libRT.h>
#include <time.h>
#include <stdlib.h>

#ifdef LINUX
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#endif


/**
*
* This test checks some points of the protocol 
* to be implemented for clauer compatibility.
* 
* We must begin with an empty formated clauer
* CAUTION, the clauer blocks will be messed!!!
*
* The password must be 123clauer
**/


int main( void ){

	USBCERTS_HANDLE hClauer;
	int nDisp=0,i;
	unsigned char type, ichar;
	long  numBloque;
#ifdef NO_CRYF_SUPPORT
	unsigned char * dev, *buffer;
#else
	unsigned char * dev[128], *buffer;
#endif 
	char *pass;
	unsigned long bytesBuffer;
	unsigned char block[TAM_BLOQUE];

	char  password[128];
	int j, r, k, l, m=0, n=0;
	long long ttotal=0;
	CLAUER_LAYOUT_INFO cli;



	LIBRT_Ini();


	for (i=0; i<100 ; i++){
		printf("\n\n========= ITERACION %d =========\n\n", i);
		// Listamos los dispositivos USB.
		if ( LIBRT_ListarUSBs(&nDisp, dev) != 0  ) {
			fprintf(stderr, "[E] [+] Imposible enumerar dispositivos\n");
			return 1;
		}
		else{
			if (nDisp == 0){
				fprintf(stderr, "[+] [+] There are no USBS on system.\n");
				exit(1);
			}
			printf("[+] [+] Enumerar dispositivos, encontrados %d :  \n",nDisp);
			for (k=0; k<nDisp ; k++)
				printf("    USB: %s \n",dev[k]);
			printf("\n\n");
		}

		for (n=10; n<100; n+=20){
			printf("\n\nFORMATEANDO AL %d%% de datos.\n",n);
			// Escribimos MBR del dispositivo
			if ( LIBRT_CrearClauer(dev[0], n) != 0  ) {
				fprintf(stderr, "[E] [+] Error creando clauer\n");
				return 1;
			}
			LIBRT_RegenerarCache();
			if ( LIBRT_FormatearClauerDatos(dev[0]) != 0  ) {
				fprintf(stderr, "[E] [+] Formateando el clauer\n");
				return 1;
			}
			LIBRT_RegenerarCache();
			if ( n != 100 ){
				if ( LIBRT_FormatearClauerCrypto( dev[0], "123clauer" ) != 0  ) {
					fprintf(stderr, "[E] [+] Formateando el clauer\n");
					return 1;
				}
			}
			else{
				if ( LIBRT_FormatearClauerCrypto( dev[0], "123clauer" ) != 0  ) {
					fprintf(stderr, "[E] [E] Formateando el clauer con 100% de datos OK!\n");
				}
			}

			if ( LIBRT_ObtenerLayout( dev[0], &cli ) != 0  ) {
				fprintf(stderr, "[E] [+] Obteniendo layout del clauer\n");
				return 1;
			}

			ttotal= 0;
			printf("Obtenido layout: \n");
			printf("\tNumero de particiones: %d\n\n", cli.npartitions);
			for (m=0; m<cli.npartitions;m++){
				printf("\tParticion: %d\n", m);
				printf("\tTamano: %lld bytes\n", cli.cpi[m].size);
				printf("\tTipo: 0x%02x\n\n", cli.cpi[m].type);
				ttotal += cli.cpi[m].size;
			}
			printf("Tamano total= %lld\n", ttotal);
			printf("%d%%= %lld diferencia= %d\n", n, ttotal*n/100, cli.cpi[0].size - ttotal*n/100   );
			printf("%d%%= %lld diferencia= %d\n", 100-n, ttotal*(100-n)/100, cli.cpi[3].size - ttotal*(100-n)/100   );

			LIBRT_RegenerarCache();
			// 1. Listamos dispositivos   
			if ( LIBRT_ListarDispositivos(&nDisp, dev) != 0  ) {
				fprintf(stderr, "[E] [+] Imposible enumerar dispositivos\n");
				return 1;
			}
			else{
				if (nDisp == 0){
					fprintf(stderr, "[+] [+] There are no clauers on system.\n");
					exit(1);
				}
				printf("[+] [+] Enumerar dispositivos, encontrados %d : { ",nDisp);
				for (k=0; k<nDisp ; k++)
					printf("%s ",dev[k]);
				printf("}\n");
			}

			memset(&cli, 0, sizeof(cli));
		}

		printf("\n\nEliminando clauer ...\n");
		if ( LIBRT_EliminarClauer( dev[0] ) !=0 ){
			fprintf(stderr, "[E] [+] No puedo eliminar clauer\n");
			return 1;
		}

		if ( LIBRT_FormatearClauerDatos(dev[0]) != 0  ) {
			fprintf(stderr, "[E] [+] Formateando el clauer\n");
			return 1;
		}

		memset(&cli, 0, sizeof(cli));
		if ( LIBRT_ObtenerLayout( dev[0], &cli ) != 0  ) {
			fprintf(stderr, "[E] [+] Obteniendo layout del clauer\n");
			return 1;
		}

		ttotal= 0;
		printf("Obtenido layout: \n");
		printf("\tNumero de particiones: %d\n\n", cli.npartitions);
		for (m=0; m<cli.npartitions;m++){
			printf("\tParticion: %d\n", m);
			printf("\tTamano: %lld bytes\n", cli.cpi[m].size);
			printf("\tTipo: 0x%02x\n\n", cli.cpi[m].type);
			ttotal += cli.cpi[m].size;
		}
		
		printf("Tama�o total= %lld\n", ttotal);
		//system("PAUSE");
		printf("Reformateamos con 0%% y lanzamos las pruebas\n"); 
		if ( LIBRT_CrearClauer(dev[0], 0) != 0  ) {
			fprintf(stderr, "[E] [+] Error creando clauer\n");
			return 1;
		}

		if ( LIBRT_FormatearClauerDatos(dev[0]) != 0  ) {
			fprintf(stderr, "[E] [+] Formateando el clauer\n");
			return 1;
		}
		
		if ( LIBRT_FormatearClauerCrypto( dev[0], "123clauer" ) != 0  ) {
			fprintf(stderr, "[E] [+] Formateando el clauer\n");
			return 1;
		}
		

		// 2. Iniciamos una sesi�n no autenticada 
		if ( LIBRT_IniciarDispositivo(dev[0], NULL, &hClauer) != 0 ) {
			fprintf(stderr, "[E] [+] Initializing device. \n");
			exit(1);
		}
		else{
			printf("[+] [+] Iniciada sesi�n no autenticada.\n");
		}
		LIBRT_FinalizarDispositivo(&hClauer);


		// 3. Invocamos funci�n 11, escribirBloqueCrypto 
		LIBRT_IniciarDispositivo(dev[0], NULL, &hClauer);
		if ( LIBRT_EscribirBloqueCrypto (&hClauer, 3, block)!= 0 ) {
			fprintf(stderr, "[E] [E] Writting crypto block. \n");
			// Error es lo correcto, Debemos continuar.
		}
		else{
			printf("[+] [E] Writting Crypto Block.\n");
		}
		LIBRT_FinalizarDispositivo(&hClauer);


		// 4. Invocamos funci�n 12, insertarBloqueCrypto 
		LIBRT_IniciarDispositivo(dev[0], NULL, &hClauer);
		if ( LIBRT_InsertarBloqueCrypto (&hClauer, block,&numBloque)!= 0 ) {
			fprintf(stderr, "[E] [E] Inserting crypto block. \n");
			// Error es lo correcto, debemos continuar.
		}
		else{
			printf("[+] [E] Inserting Crypto Block on position= %d .\n",numBloque);
		}
		LIBRT_FinalizarDispositivo(&hClauer);


		// 5. Invocamos funci�n 13, BorrarBloqueCrypto
		LIBRT_IniciarDispositivo(dev[0], NULL, &hClauer);
		numBloque=3;
		if ( LIBRT_BorrarBloqueCrypto (&hClauer, numBloque)!= 0 ) {
			fprintf(stderr, "[E] [E] Deleting crypto block %d. \n",numBloque);
			// Error es lo correcto, debemos continuar.
		}
		else{
			if (numBloque != -1 )
				printf("[+] [E] Deleted Crypto Block on position= %d .\n",numBloque);
			else 
				printf("[E] [E] Deleting crypto block.\n");
		}
		LIBRT_FinalizarDispositivo(&hClauer);


		// 6. Invocamos funci�n 7 con tipo 140 
		LIBRT_IniciarDispositivo(dev[0], NULL, &hClauer);
		if ( LIBRT_LeerTipoBloqueCrypto (&hClauer, 1, 1,block , &numBloque) != 0 ) {
			fprintf(stderr, "[E] [+] Reading crypto block type 140.\n");
			// Error es lo correcto, debemos continuar.
		}
		else{
			if (numBloque == -1)
				fprintf(stderr, "[+] [+] Reading crypto block type 140 returns -1.\n");
			else
				printf("[E] [+] Readed Crypto Block of type 140 on position= %d.\n",numBloque);
		}
		LIBRT_FinalizarDispositivo(&hClauer);


		// 7. Invocamos funci�n 20 finalizamos sesi�n 
		LIBRT_IniciarDispositivo(dev[0], NULL, &hClauer);
		if ( LIBRT_FinalizarDispositivo(&hClauer) != 0 ) {
			fprintf(stderr, "[E] [+] I couldn't end session\n");
			// It is not neccessary to exit, we will open an authenticated session.
		}
		else{
			printf("[+] [+] Unauthenticated session closed successfully.\n");
		}
		LIBRT_FinalizarDispositivo(&hClauer);


		// 8. Iniciamos una sesi�n autenticada 
		if ( LIBRT_IniciarDispositivo(dev[0], "123clauer", &hClauer) != 0 ) {
			fprintf(stderr, "[E] [+] Can't start an Authenticated session. \n");
			exit(1);
		}
		else{
			printf("[+] [+] Authenticated session started successfuly.\n");
		}
		LIBRT_FinalizarDispositivo(&hClauer);

		// 9. Insertar 3 bloques tipo 69, obtener sus posiciones y ver que estan en 0,1 y 2 
		LIBRT_IniciarDispositivo(dev[0], "123clauer", &hClauer);
		block[0]=185; block[1]=69;
		for ( k=0 ; k<3 ; k++ ){
			if ( LIBRT_InsertarBloqueCrypto (&hClauer, block, &numBloque) != 0){
				fprintf(stderr, "[E] [+] Can't Insert block. \n");
				exit(1);
			}
			else{
				if ( numBloque != k ){
					fprintf(stderr,"[E] [+] Block must be written on pos %d instead of %d\n",k,numBloque);
				}
				else{
					printf("[+] [+] Block inserted ok on position %d\n",k);
				}
			}
		}
		LIBRT_FinalizarDispositivo(&hClauer);


		// 10. Borramos bloque 2
		LIBRT_IniciarDispositivo(dev[0], "123clauer", &hClauer);
		numBloque= 2;
		if ( LIBRT_BorrarBloqueCrypto (&hClauer, numBloque) != 0 ){
			fprintf(stderr,"[E] [+] Cant delete block %d\n",numBloque);
			exit(1);
		}
		else{
			printf("[+] [+] Block %d successfuly deleted.\n", numBloque);
		}
		LIBRT_FinalizarDispositivo(&hClauer);



		// 11. Borramos bloque 4 
		LIBRT_IniciarDispositivo(dev[0], "123clauer", &hClauer);
		numBloque= 4;
		if ( LIBRT_BorrarBloqueCrypto (&hClauer, numBloque) != 0 ){
			fprintf(stderr,"[E] [+] Cant delete block %d\n",numBloque);
			exit(1);
		}
		else{
			printf("[+] [+] Block %d successfuly deleted.\n", numBloque);
		}
		LIBRT_FinalizarDispositivo(&hClauer);

		// 12. Borramos bloque 1000000 cuando el numero de bloques es 492 
		LIBRT_IniciarDispositivo(dev[0], "123clauer", &hClauer);
		numBloque= 1000000;
		if ( LIBRT_BorrarBloqueCrypto (&hClauer, numBloque) != 0 ){
			fprintf(stderr,"[E] [E] Cant delete block %d\n",numBloque);
			// Here is ok
		}
		else{
			printf("[+] [E] Block %d successfuly deleted.\n", numBloque);
		}
		LIBRT_FinalizarDispositivo(&hClauer);


		// 13. Insertamos bloque y comprobamos que es el 2
		LIBRT_IniciarDispositivo(dev[0], "123clauer", &hClauer);
		if ( LIBRT_InsertarBloqueCrypto (&hClauer, block, &numBloque) != 0){
			fprintf(stderr, "[E] [+] Can't Insert block. \n");
			exit(1);
		}
		else{
			if ( numBloque != 2 ){
				fprintf(stderr,"[E] [+] Block must be written on pos 2 instead of %d\n",numBloque);
			}
			else{
				printf("[+] [+] Block inserted ok on position %d\n", numBloque);
			}
		}
		LIBRT_FinalizarDispositivo(&hClauer);




		// 14. Insertar bloques hasta error (llenamos el clauer) 
		LIBRT_IniciarDispositivo(dev[0], "123clauer", &hClauer);
		block[0]=185; block[1]=69; i=0;
		while ( 1 ){
			i++;
			if ( LIBRT_InsertarBloqueCrypto (&hClauer, block, &numBloque) != 0){
				fprintf(stderr, "[E] [E] Inserted %d blocks until error. \n",i);
				break;
			}
		}
		LIBRT_FinalizarDispositivo(&hClauer);





		// 15. Borramos bloque 100 y 120 
		LIBRT_IniciarDispositivo(dev[0], "123clauer", &hClauer);
		numBloque= 100;
		if ( LIBRT_BorrarBloqueCrypto (&hClauer, numBloque) != 0 ){
			fprintf(stderr,"[E] [+] Cant delete block %d\n",numBloque);
			// Here is ok
		}
		else{
			printf("[+] [+] Block %d successfuly deleted.\n", numBloque);
		}

		numBloque= 120;
		if ( LIBRT_BorrarBloqueCrypto (&hClauer, numBloque) != 0 ){
			fprintf(stderr,"[E] [+] Cant delete block %d\n",numBloque);
			// Here is ok
		}
		else{
			printf("[+] [+] Block %d successfuly deleted.\n", numBloque);
		}
		LIBRT_FinalizarDispositivo(&hClauer);


		// 16. Insertamos 2 bloques y comprobamos que se ha hecho en 100 y 120  
		LIBRT_IniciarDispositivo(dev[0], "123clauer", &hClauer);
		if ( LIBRT_InsertarBloqueCrypto (&hClauer, block, &numBloque) != 0){
			fprintf(stderr, "[E] [+] Can't Insert block. \n");
			exit(1);
		}
		else{
			if ( numBloque != 100 ){
				fprintf(stderr,"[E] [+] Block must be written on pos 100 instead of %d\n",numBloque);
			}
			else{
				printf("[+] [+] Block inserted ok on position %d\n",numBloque);
			}
		}
		LIBRT_FinalizarDispositivo(&hClauer);


		LIBRT_IniciarDispositivo(dev[0], "123clauer", &hClauer);
		if ( LIBRT_InsertarBloqueCrypto (&hClauer, block, &numBloque) != 0){
			fprintf(stderr, "[E] [+] Can't Insert block. \n");
			exit(1);
		}
		else{
			if ( numBloque != 120 ){
				fprintf(stderr,"[E] [+] Block must be written on pos 120 instead of %d\n",numBloque);
			}
			else{
				printf("[+] [+] Block inserted ok on position %d\n",numBloque);
			}
		}
		LIBRT_FinalizarDispositivo(&hClauer);



		// 17. Leer primer bloque de tipo 69 y borrarlo 
		LIBRT_IniciarDispositivo(dev[0], "123clauer", &hClauer);
		numBloque= 0;
		if ( LIBRT_LeerTipoBloqueCrypto (&hClauer, 69, 1, block, &numBloque) != 0){
			fprintf(stderr, " [E] [+] Can't find 69 type block\n");
			exit(1);
		}


		if ( LIBRT_BorrarBloqueCrypto (&hClauer, numBloque)!=0 ) {
			fprintf(stderr, " [E] [+] Can't delete block num %d\n",numBloque);
			exit(1);
		}

		printf("[+] [+] Block of type 69 successfuly found and deleted\n");
		LIBRT_FinalizarDispositivo(&hClauer);

		// 18. Leer siguiente bloque. 
		LIBRT_IniciarDispositivo(dev[0], "123clauer", &hClauer);
		i=0;
		while (1){
			if ( LIBRT_LeerTipoBloqueCrypto (&hClauer, 69, 0, block, &numBloque) != 0){
				fprintf(stderr, "[E] [E] %d Deleted blocks until error.\n",i);
				break;
			}
			// 19. Borrarlo. 
			if ( numBloque != -1 ) {
				if ( LIBRT_BorrarBloqueCrypto (&hClauer, numBloque)!=0 ) {
					fprintf(stderr, "[E] [+] Can't delete block num %d\n",numBloque);
					exit(1);
				}
				i++;
			} else
				break;
		}
		LIBRT_FinalizarDispositivo(&hClauer);

		// 20. Leer Primer bloque tipo 69. 
		LIBRT_IniciarDispositivo(dev[0], "123clauer", &hClauer);
		if ( LIBRT_LeerTipoBloqueCrypto (&hClauer, 69, 1, block, &numBloque) != 0){
			fprintf(stderr, "[E] [E] Block not found\n",i);
		}
		else
			if ( numBloque == -1 ) 
				fprintf(stderr, "[+] [+] Block not found %d\n",numBloque);
			else
				fprintf(stderr, "[E] [+] Block found at pos %d\n",numBloque);


		LIBRT_FinalizarDispositivo(&hClauer);
		// 21. Escribimos 10 bloques tipo 20 con contenidos 1-10 
		//     Escribimos 10 bloques tipo 140 con contenidos 50-60
		//     Comprobamos orden de inserci�n. 


		LIBRT_IniciarDispositivo(dev[0], "123clauer", &hClauer);
		block[0]=185; block[1]=20;block[8]=1;
		for ( i=0 ; i<20 ; i++ ){
			if (i==10) block[1]=140;
			if ( LIBRT_InsertarBloqueCrypto (&hClauer, block, &numBloque) != 0){
				fprintf(stderr, "[E] [+] Can't Insert block. \n");
				exit(1);
			}
			else{
				if ( numBloque != i ){
					fprintf(stderr,"[E] [+] Block must be written on pos %d instead of %d\n",i,numBloque);
				}
				else{
					printf("[+] [+] Block inserted ok on position %d\n",i);
				}
			}
			block[8]++;
		}
		LIBRT_FinalizarDispositivo(&hClauer);

		// Comprobamos que se han insertado correctamente. 
		numBloque=-1; type= 20;

		//  if ( LIBRT_LeerTipoBloqueCrypto (&hClauer, 20, 1, block, &numBloque) != 0){
		//  fprintf(stderr, "[E] [+] Block not found\n",i);
		//}

		LIBRT_IniciarDispositivo(dev[0], "123clauer", &hClauer);
		for (ichar=0;ichar<20;ichar++){
			if (ichar==10) type=140;
			if ( LIBRT_LeerTipoBloqueCrypto (&hClauer, type, 0, block, &numBloque) != 0){
				fprintf(stderr, "[E] [+] Block not found\n",i);
			}
			else{
				if (block[1]==type  && block[8]==(ichar+1))
					fprintf(stdout, "[+] [+] First block of type %d  found ok at position %d \n",type,numBloque);
				else{	
					fprintf(stderr, "[E] [+] Error on  block of type %d (block[1]=%d)  found  at position %d ichar=%d\n",type,block[1],block[8],ichar);
				}
			}
		}  
		LIBRT_FinalizarDispositivo(&hClauer);

		// 22. Probamos a cambiar el password 100 veces con cadenas random.


		srand(time(NULL));
		LIBRT_IniciarDispositivo(dev[0], "123clauer", &hClauer);
		for( i = 0; i < 10 ; i++ ){   

			for ( j=0 ; j< i+10; j++ ){
				r= rand()%256;
				while (r==0)
					r= rand()%256;

				password[j]= (char)r;
			}
			password[j]=0;

			if (  LIBRT_CambiarPassword  ( &hClauer, password)  != 0){
				fprintf(stderr, "[E] [+] Cannot change the password.\n",i);
				exit(-1);
			}
			else{

				fprintf(stdout, "[+] [+] Password changed length= %d\n",j);
				if ( LIBRT_FinalizarDispositivo(&hClauer) != 0 ) {
					fprintf(stderr, "[E] [+] I couldn't end session\n");
				}
				else{
					printf("[+] [+] Unauthenticated session closed successfully.\n");
				}

				if ( LIBRT_IniciarDispositivo(dev[0], password, &hClauer) != 0 ) {
					fprintf(stderr, "[E] [+] Can't start an Authenticated session with password= %s. \n", password);
					exit(1);
				}
				else{
					printf("[+] [+] Authenticated session started successfuly with pass= %s\n",password);
				}


			}
		}
		LIBRT_CambiarPassword  ( &hClauer, "123clauer");
		LIBRT_FinalizarDispositivo(&hClauer);


	}
	fflush(stdout);
	fflush(stderr);

	LIBRT_Fin();
}
