#include <stdio.h>
#include <stdlib.h>

#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <arpa/inet.h>



int sendall(int s, char *buf, int len)
{
    int total = 0;        // how many bytes we've sent
    int bytesleft = len; // how many we have left to send
    int n;

    while(total < len) {
	n = send(s, buf+total, bytesleft, 0);
	if (n == -1) { break; }
	total += n;
	bytesleft -= n;
    }

    return n==-1?-1:0; // return -1 on failure, 0 on success
} 


int main ( int argc, char **argv)
{
    int fd,i;
    struct sockaddr_in addr;
    unsigned char aux, f, *buf, err;

    fd = socket(PF_INET, SOCK_STREAM, 0);

    if ( fd == -1 ) {
	fprintf(stderr, "[ERROR] Creating socket\n");
	exit(1);
    }

    memset((void *) &addr, 0, sizeof(struct sockaddr_in));

    addr.sin_family = AF_INET;
    addr.sin_port = htons(969);
    inet_aton("127.0.0.1", &addr.sin_addr);

    if ( connect(fd, (struct sockaddr *) &addr, sizeof(struct sockaddr)) == -1 ) {
	fprintf(stderr, "[ERROR] Connecting to server\n");
	exit(1);
    }

    /* Function 1: start an authenticated session */

    printf("TEST 1: Starting an anoymous session... ");

    f=1;
    if ( sendall(fd,&f, 1) == -1 ) {
	fprintf(stderr, "[ERROR] Sending function ID\n");
	exit(1);
    }
    aux='a';
    if ( sendall(fd,&aux, 1) == -1 ) {
	fprintf(stderr, "[ERROR] Sending device id\n");
	exit(1);
    }
    aux=0;
    if ( sendall(fd, &aux, 1) == -1 ) {
	fprintf(stderr, "[ERROR] Sending password len\n");
	exit(1);
    }
    buf = (unsigned char *) malloc (20);
    if ( ! buf ) {
	fprintf(stderr, "[ERROR] Out of memory\n");
	exit(1);
    }
    if ( recv(fd, &err, 1, 0) == -1 ) {
	fprintf(stderr, "[ERROR] Receiving error code\n");
	exit(1);
    }
    if ( recv(fd, buf, 20, 0) == -1 ) {
	fprintf(stderr, "[ERROR] Receiving clauer's id\n");
	exit(1);
    }
    switch (err) {
	case 0 :
	    printf("[OK]\n");
	    printf("\tClauer ID: ");
	    for ( i = 0 ; i < 20 ; i++ ) 
		printf("%02x ",	buf[i]);
	    printf("\n");
	    fflush(stdout);	

	    break;

	default:
	    fprintf(stderr, "[ERROR] Code returned by server: %d\n", err);
	    break;
    }

    /* Close de session */

    f=1;
    if ( sendall(fd, &f, 1) == -1 ) {
	fprintf(stderr, "[ERROR] Closing session\n");
	exit(1);
    }
    close(fd);

}


