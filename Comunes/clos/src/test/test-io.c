#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "../io.h"
#include "../../log/log.h"
#include "../../err.h"




int main ( int argc, char **argv )
{

  int ret, i;
  clauer_handle_t hClauer;
  block_info_t ib;
  block_object_t ob;


  if ( argc > 1 ) {

    if ( (strncmp(argv[1], "-d", 2) == 0) || (strncmp(argv[1], "--debug", 2) == 0) ) {
      printf("YEPA\n");
      if ( LOG_Ini(LOG_WHERE_STDERR, 2) != CLOS_SUCCESS )
	printf("Error iniciando log\n");
    }

    
  }


  /********/

  printf("[1] TEST Detección 1. Introduzca un CLAUER (pulse enter) ");
  fflush(stdout);
  ret = getchar();
  
  ret = IO_Is_Clauer("/dev/sda");

  switch ( ret ) {
    
  case IO_IS_CLAUER:
    printf("    [OK]\n");
    break;

  case IO_IS_NOT_CLAUER:
    printf("    [ERROR] :: Library says that that's not a Clauer. ¿sure?\n");
    break;

  case ERR_IO_NO_PERM:
    printf("    [ERROR] :: You don't have privileges in order to open the device\n");
    exit(1);

  default:
    printf("    [ERROR] :: Turn on debugging");
    exit(1);
  }

  
  /********/

  printf("[2] TEST. Plug a device that is not a clauer ( press enter ) ");
  fflush(stdout);
  ret = getchar();
  
  ret = IO_Is_Clauer("/dev/sda");

  switch ( ret ) {
    
  case IO_IS_NOT_CLAUER:
    printf("    [OK]\n");
    break;

  case IO_IS_CLAUER:
    printf("    [ERROR] :: Library says that that's a Clauer. ¿sure?\n");
    break;

  case ERR_IO_NO_PERM:
    printf("    [ERROR] :: You don't have privileges in order to open the device\n");
    exit(1);

  default:
    printf("    [ERROR] :: Turn on debugging");
    exit(1);
  }
  
  /************/


  printf("[3] IO_Open(). Passing NULL as a device... ");

  ret = IO_Open(NULL, &hClauer);
  if ( ret == ERR_INVALID_PARAMETER )
    printf("[OK]\n");
  else
    printf("[ERROR] :: Returned value: %d\n", ret);

  /*************/

  printf("[4] IO_Open(). Passing NULL as a handle...");

  ret = IO_Open("/dev/sda", NULL);
  if ( ret == ERR_INVALID_PARAMETER )
    printf("[OK]\n");
  else
    printf("[ERROR] :: Returned value: %d\n", ret);

  /********************/

  printf("[5] IO_Open(). Passing both parameters as NULL...");
  
  ret = IO_Open(NULL, NULL);
  if ( ret == ERR_INVALID_PARAMETER )
    printf("[OK]\n");
  else
    printf("[ERROR] :: Returned value: %d\n", ret);

  /********************/

  printf("[6] IO_Open(). Opening /dev/sda... ");

  ret = IO_Open("/dev/sda", &hClauer);

  if ( ret == ERR_IO_NOT_CLAUER ) {
    printf("[ERROR] :: It's not a clauer. Please use a clauer\n");
    exit(1);
  } else if ( ret != CLOS_SUCCESS ) {
    printf("[ERROR] :: Returned value: %d\n", ret );
    exit(1);
  }

  printf("[OK]\n");
  

  /********************/

  printf("[7] IO_ReadInfoBlock(). Passing NULL handle... ");

  ret = IO_ReadInfoBlock(NULL, &ib);

  if ( ret == ERR_INVALID_PARAMETER )
    printf("[OK]\n");
  else if ( ret != CLOS_SUCCESS )
    printf("[ERROR] :: Returned value: %d\n", ret);

  /********************/

  printf("[8] IO_ReadInfoBlock(): Passing NULL block...");
  
  ret = IO_ReadInfoBlock(hClauer, NULL);

  if ( ret == ERR_INVALID_PARAMETER )
    printf("[OK]\n");
  else if ( ret != CLOS_SUCCESS ) {
    printf("[ERROR] :: Returned value: %d\n", ret);
    exit(1);
  }

  /********************/

  printf("[9] IO_ReadInfoBlock(): Passing NULL handle...");
  
  ret = IO_ReadInfoBlock(NULL, &ib);

  if ( ret == ERR_INVALID_PARAMETER )
    printf("[OK]\n");
  else if ( ret != CLOS_SUCCESS ) {
    printf("[ERROR] :: Returned value: %d\n", ret);
    exit(1);
  }

  /********************/

  printf("[10] IO_ReadInfoBlock(): Passing both NULL...");
  
  ret = IO_ReadInfoBlock(NULL, NULL);

  if ( ret == ERR_INVALID_PARAMETER )
    printf("[OK]\n");
  else if ( ret != CLOS_SUCCESS ) {
    printf("[ERROR] :: Returned value: %d\n", ret);
    exit(1);
  }
  
  /********************/

  printf("[11] IO_ReadInfoBlock(): Dumping info block...");
  
  ret = IO_ReadInfoBlock(hClauer, &ib);

  if ( ret != CLOS_SUCCESS ) {
    printf("[ERROR] :: Returned vale: %d\n", ret);
    exit(1);
  }

  printf("[OK]\n");

  printf("     ID: ");
  for ( i = 0 ; i < 20 ; i++ )
    printf("%02x", ib.id[i]);
  printf("\n");
  printf("     Format Version: %ld\n", ib.version);
  printf("     Object Zone Size (blocks): %ld\n", ib.totalBlocks);
  printf("     Reserved Zone Size (blocks): %ld\n", ib.rzSize);
  printf("     Current Block: %ld\n", ib.cb);

  /********************/

  printf("[12] IO_Read: Passing NULL handle...");

  ret = IO_Read(NULL, &ob);

  if ( ret == ERR_INVALID_PARAMETER ) {
    printf("[OK]\n");
  } else {
    printf("[ERROR] :: Value returned %d\n", ret);
    exit(1);
  }
  
  /********************/

  printf("[13] IO_Read: Passing NULL block...");

  ret = IO_Read(hClauer, NULL);

  if ( ret == ERR_INVALID_PARAMETER ) {
    printf("[OK]\n");
  } else {
    printf("[ERROR] :: Value returned %d\n", ret);
    exit(1);
  }
  
  /********************/

  printf("[14] IO_Read: Passing both NULL parameters...");

  ret = IO_Read(NULL, NULL);

  if ( ret == ERR_INVALID_PARAMETER ) {
    printf("[OK]\n");
  } else {
    printf("[ERROR] :: Value returned %d\n", ret);
    exit(1);
  }


  /************************/

  printf("[15] IO_Read: reading the first object block...");

  ret = IO_Read(hClauer, &ob);

  if ( ret != CLOS_SUCCESS ) {
    printf("[ERROR] :: Value returned: %d\n", ret);
    exit(1);
  } else {
    printf("[OK]\n");
  }

  printf("     OBJECT TYPE: %d\n", ob.type);

  /************************/

  printf("[15] IO_Read: reading another object block...");

  ret = IO_Read(hClauer, &ob);

  if ( ret != CLOS_SUCCESS ) {
    printf("[ERROR] :: Value returned: %d\n", ret);
    exit(1);
  } else {
    printf("[OK]\n");
  }

  printf("     OBJECT TYPE: %d\n", ob.type);

  /************************/

  
  
  return 0;
}






