// clos-win.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"

#include <windows.h>
#include <winsvc.h>
#include <winuser.h>
#include <winbase.h>
#include <tchar.h>
#include <dbt.h>
#include <devguid.h>

#include <stdlib.h>

#include "transport.h"
#include "log.h"
#include "common.h"

#include <CRYPTOWrapper/CRYPTOWrap.h>
#include <clio/clio.h>

//DEFINE_GUID(GUID_CLASS_USB_DEVICE, 0xA5DCBF10L, 0x6530,0x11D2, 0x90,0x1F, 0x00, 0xC0, 0x4F, 0xB9, 0x51, 0xED);

#define DESCRIPCION_SERVICIO	TEXT("Sistema Operatiu del Projecte Clauer")
#define NOMBRE_SERVICIO			TEXT("CLOS")
#define CL_OS_WIN_XP   0
#define CL_OS_WIN_2000 1
#define CL_OS_UNKNOWN  2

SERVICE_STATUS          MyServiceStatus; 
SERVICE_STATUS_HANDLE   MyServiceStatusHandle; 

static BOOL g_bParar;

HANDLE g_hMutexCache;    // Mutex para la cach� de dispositivos

/*
 * Prototipos de las funciones del servicio
 */

VOID  MyServiceStart (DWORD dwArgc, LPTSTR *lpszArgv); 
DWORD MyServiceCtrlHandler (DWORD opcode,DWORD dwEventType,LPVOID lpEventData,LPVOID lpContext); 
DWORD MyServiceInitialization (DWORD argc, LPTSTR *argv, DWORD *specificError); 

void RunService       (void);
void InstallService   (void);
void UninstallService (void);
void ImprimirUso      (TCHAR *nombrePrograma);

int CLOS_win_main ( void );


void InstallService()
{
	SC_HANDLE serviceControlManager = OpenSCManager( 0, 0, SC_MANAGER_CREATE_SERVICE );

	if ( serviceControlManager )
	{
		TCHAR path[ MAX_PATH + 1 ];
		if ( GetModuleFileName( 0, path, sizeof(path)/sizeof(path[0]) ) > 0 )
		{
			SERVICE_DESCRIPTION descripcionServicio;
			SC_HANDLE service = CreateService( serviceControlManager,
							NOMBRE_SERVICIO, NOMBRE_SERVICIO,
							SERVICE_ALL_ACCESS, SERVICE_WIN32_OWN_PROCESS|SERVICE_INTERACTIVE_PROCESS,
							SERVICE_AUTO_START, SERVICE_ERROR_IGNORE, path,
							NULL, NULL, NULL, NULL, NULL );


			if ( !service ) {
				fprintf(stderr, "ERROR. Imposible instalar servicio: %d\n", GetLastError());
				exit(1);
			}

			/*
			 * Introducimos el texto descriptivo del servicio
			 */

			descripcionServicio.lpDescription = DESCRIPCION_SERVICIO;

			if ( !ChangeServiceConfig2(service,SERVICE_CONFIG_DESCRIPTION, (LPVOID) &descripcionServicio) ) {
				fprintf(stderr, "ERROR. Imposible establecer la descripci�n del servicio\n");
				fprintf(stderr, "       C�digo de error = %ld\n", GetLastError());
				exit(1);
			}

			if ( service )
				CloseServiceHandle( service );
		}

		CloseServiceHandle( serviceControlManager );
	}
}






void UninstallService()
{
	SC_HANDLE serviceControlManager = OpenSCManager( 0, 0, SC_MANAGER_CONNECT );

	if ( serviceControlManager )
	{
		SC_HANDLE service = OpenService( serviceControlManager,
			NOMBRE_SERVICIO, SERVICE_QUERY_STATUS | DELETE );
		if ( service )
		{
			SERVICE_STATUS serviceStatus;
			if ( QueryServiceStatus( service, &serviceStatus ) )
			{
				if ( serviceStatus.dwCurrentState == SERVICE_STOPPED )
					DeleteService( service );
			}

			CloseServiceHandle( service );
		}

		CloseServiceHandle( serviceControlManager );
	}
}


 

int _tmain( int argc, TCHAR* argv[] )
{

	LOG_Ini(LOG_WHERE_FILE, 10);

	if ( argc > 1 && lstrcmpi( argv[1], TEXT("/i") ) == 0 )
	{
		InstallService();
	}
	else if ( argc > 1 && lstrcmpi( argv[1], TEXT("/u") ) == 0 )
	{
		UninstallService();
	}
	else if ( argc > 1 && lstrcmpi( argv[1], TEXT("/h") ) == 0 )
	{
		ImprimirUso(argv[0]);

	}
	else {

		g_hMutexCache = CreateMutex(NULL, FALSE, NULL);
		if ( ! g_hMutexCache ) {
			LOG_Error(1, "No se pudo crear mutex para cach�: %ld", GetLastError());
			return 1;
		}
		RunService();
	}

	return 0;
}




void RunService(void) 
{ 
    SERVICE_TABLE_ENTRY   DispatchTable[] = 
    { 
        { NOMBRE_SERVICIO, (LPSERVICE_MAIN_FUNCTION) MyServiceStart      }, 
        { NULL,              NULL          } 
    }; 


    StartServiceCtrlDispatcher(DispatchTable);
} 



DWORD GetOS(){
	OSVERSIONINFO OSversion;

	OSversion.dwOSVersionInfoSize=sizeof(OSVERSIONINFO);
	GetVersionEx(&OSversion);

	switch(OSversion.dwPlatformId)
	{
	case VER_PLATFORM_WIN32s: 
		LOG_Msg(1,"SAlimos por VER_PLATFORM_WIN32s");
	    return CL_OS_UNKNOWN;
		   
	break;
	case VER_PLATFORM_WIN32_WINDOWS:
		if(OSversion.dwMinorVersion==0){
			LOG_Msg(1,"SAlimos por VER_PLATFORM_WIN32_WINDOWS OSversion.dwMinorVersion==0");
			 return CL_OS_UNKNOWN; 
		}else 
			if(OSversion.dwMinorVersion==10){  
				LOG_Msg(1,"SAlimos por VER_PLATFORM_WIN32_WINDOWS OSversion.dwMinorVersion==10");
				 return CL_OS_UNKNOWN;
			}
			else
				if(OSversion.dwMinorVersion==90)  {
					LOG_Msg(1,"SAlimos por VER_PLATFORM_WIN32_WINDOWS OSversion.dwMinorVersion==90");				
					return CL_OS_UNKNOWN;
				}
     break;
	 case VER_PLATFORM_WIN32_NT:
		 if(OSversion.dwMajorVersion==5 && OSversion.dwMinorVersion==0){
			 LOG_Msg(1,"SAlimos por CL_OS_WIN_2000");
             return CL_OS_WIN_2000;
		 }
		 else	
			 if(OSversion.dwMajorVersion==5 &&   OSversion.dwMinorVersion==1){
				 LOG_Msg(1,"SAlimos por CL_OS_WIN_XP");
			     return CL_OS_WIN_XP;
			 }
			 else	
				if(OSversion.dwMajorVersion<=4){
					 LOG_Msg(1,"SAlimos por CL_OS_UNKNOWN 1 ");
					return CL_OS_UNKNOWN;
				}
				else{	
					 LOG_Msg(1,"SAlimos por CL_OS_UNKNOWN 2 ");
					return CL_OS_UNKNOWN;
				}
	break;
	}		
	return CL_OS_UNKNOWN;
}


VOID MyServiceStart (DWORD dwArgc, LPTSTR *lpszArgv)
{
	int result;
	DEV_BROADCAST_DEVICEINTERFACE NotificationFilter;
	HDEVNOTIFY hDevNotify;

	clauer_handle_t hClauer;
	int ret, i;
	
	g_bParar = FALSE;

	MyServiceStatus.dwServiceType             = SERVICE_WIN32_OWN_PROCESS;
	MyServiceStatus.dwCurrentState            = SERVICE_START_PENDING;
	MyServiceStatus.dwControlsAccepted        = SERVICE_ACCEPT_SHUTDOWN | SERVICE_ACCEPT_STOP | SERVICE_ACCEPT_PAUSE_CONTINUE;
	MyServiceStatus.dwWin32ExitCode           = 0;
	MyServiceStatus.dwServiceSpecificExitCode = 0;
	MyServiceStatus.dwCheckPoint              = 0;
	MyServiceStatus.dwWaitHint                = 0;

	MyServiceStatusHandle = RegisterServiceCtrlHandlerEx( NOMBRE_SERVICIO, (LPHANDLER_FUNCTION_EX) MyServiceCtrlHandler, NULL);  
    if (MyServiceStatusHandle == (SERVICE_STATUS_HANDLE)0) 
    {
		LOG_Msg(1,"Fallo el MyServiceStatusHandle");
        return; 
    } 

	LOG_Msg(1,"MyServiceStatusHandle correcto");

	// Registramos para la notificaci�n de mensajes

	
	ZeroMemory(&NotificationFilter, sizeof NotificationFilter);
	NotificationFilter.dbcc_size       = sizeof(DEV_BROADCAST_DEVICEINTERFACE);
	NotificationFilter.dbcc_devicetype = DBT_DEVTYP_DEVICEINTERFACE;
	NotificationFilter.dbcc_classguid  = GUID_DEVCLASS_DISKDRIVE;
	
	hDevNotify = RegisterDeviceNotification( MyServiceStatusHandle, &NotificationFilter, DEVICE_NOTIFY_SERVICE_HANDLE | DEVICE_NOTIFY_ALL_INTERFACE_CLASSES);
	if ( ! hDevNotify ) {
		LOG_Error(1, "Registering for device notification: %ld", GetLastError());
		return;
	}

	// Initialize device cache

	if ( IO_EnumClauers(&(child_info.nDevs), child_info.devices, IO_CHECK_IS_CLAUER) != IO_SUCCESS ) {
		ReleaseMutex(g_hMutexCache);
		return;
	}

	for( i=0 ; i < child_info.nDevs && i < IO_MAX_DEVICES  ; i++ ){
	    
		child_info.ibs[i] = ( block_info_t * ) malloc(sizeof(block_info_t));
	    
		ret = IO_Open(child_info.devices[i], &(hClauer), IO_RD, IO_CHECK_IS_CLAUER );
	    
		if ( ret != IO_SUCCESS ) {
			ReleaseMutex(g_hMutexCache);
			return;
		}
	    
		ret = IO_ReadInfoBlock( hClauer, child_info.ibs[i] );
		if ( ret != IO_SUCCESS ) {
			ReleaseMutex(g_hMutexCache);
			return;
		}

		ret = IO_Close( hClauer );
	    
		if ( ret != IO_SUCCESS ) {
			ReleaseMutex(g_hMutexCache);
			return;
		}
	}

    // Initialization complete - report running status. 

    MyServiceStatus.dwCurrentState       = SERVICE_RUNNING; 
    MyServiceStatus.dwCheckPoint         = 0; 
    MyServiceStatus.dwWaitHint           = 0; 
 
    if (!SetServiceStatus (MyServiceStatusHandle, &MyServiceStatus)) ;
		//LOG_Escribir(logHandle, "service: Error estableciendo estado del servicio\n");
 
    // This is where the service does its work. 

	result = CLOS_win_main();
	if ( result != CLOS_SUCCESS ) {	
		MyServiceStatus.dwCurrentState            = SERVICE_STOPPED;
		MyServiceStatus.dwCheckPoint              = 0; 
		MyServiceStatus.dwWaitHint                = 0;
		MyServiceStatus.dwWin32ExitCode           = ERROR_SERVICE_SPECIFIC_ERROR;
		MyServiceStatus.dwServiceSpecificExitCode = 1;
		
		if (!SetServiceStatus (MyServiceStatusHandle, &MyServiceStatus)) ;
	}

    return; 
}



DWORD MyServiceCtrlHandler (DWORD opcode,DWORD dwEventType,LPVOID lpEventData,LPVOID lpContext)
{
	int i, ret;
	clauer_handle_t hClauer;

	LOG_Debug(1, "EVENTO = %d", opcode);

	
    switch(opcode) {

		case SERVICE_CONTROL_PAUSE:
			MyServiceStatus.dwCurrentState = SERVICE_PAUSED;
			break;

		case SERVICE_CONTROL_CONTINUE:
			MyServiceStatus.dwCurrentState = SERVICE_RUNNING;
			break;

		case SERVICE_CONTROL_STOP:

			g_bParar = TRUE;

			MyServiceStatus.dwWin32ExitCode = 0;
			MyServiceStatus.dwCurrentState = SERVICE_STOPPED;
			MyServiceStatus.dwCheckPoint = 0;
			MyServiceStatus.dwWaitHint = 0;

			if ( !SetServiceStatus(MyServiceStatusHandle, &MyServiceStatus) );
				return NO_ERROR;

		case SERVICE_CONTROL_DEVICEEVENT:

			LOG_Msg(1, "DEVICEEVENT");

			if ( WaitForSingleObject(g_hMutexCache, 20000) != WAIT_FAILED ) {

				switch (dwEventType) {

					/* The system broadcasts the DBT_DEVICEARRIVAL device event when a device or piece of media 
					   has been inserted and becomes available. 
					 */

					case DBT_DEVICEARRIVAL:

						LOG_Msg(1, "DEVICEARRIVAL");

						for( i=0 ; i < child_info.nDevs && i < IO_MAX_DEVICES  ; i++ ){
							free( child_info.devices[i] );
							free( child_info.ibs[i] );
						}

						if ( IO_EnumClauers(&(child_info.nDevs), child_info.devices, IO_CHECK_IS_CLAUER) != IO_SUCCESS ) {
							ReleaseMutex(g_hMutexCache);
							return 1;
						}

						for( i=0 ; i < child_info.nDevs && i < IO_MAX_DEVICES  ; i++ ){
						    
							child_info.ibs[i] = ( block_info_t * ) malloc(sizeof(block_info_t));
						    
							ret = IO_Open(child_info.devices[i], &(hClauer), IO_RD, IO_CHECK_IS_CLAUER );
						    
							if ( ret != IO_SUCCESS ) {
								ReleaseMutex(g_hMutexCache);
								return 1;
							}
						    
							ret = IO_ReadInfoBlock( hClauer, child_info.ibs[i] );
							if ( ret != IO_SUCCESS ) {
								ReleaseMutex(g_hMutexCache);
								return 1;
							}

							ret = IO_Close( hClauer );
						    
							if ( ret != IO_SUCCESS ) {
								ReleaseMutex(g_hMutexCache);
								return 1;
							}
						}

						break;
					
					/* The system broadcasts the DBT_DEVICEREMOVECOMPLETE device event when a device or piece of 
					   media has been physically removed. */

					case DBT_DEVICEREMOVECOMPLETE:	

						LOG_Msg(1,"REMOVECOMPLETE");
						for( i=0 ; i < child_info.nDevs && i < IO_MAX_DEVICES  ; i++ ){
							free( child_info.devices[i] );
							free( child_info.ibs[i] );
						}

						if ( IO_EnumClauers(&(child_info.nDevs), child_info.devices, IO_CHECK_IS_CLAUER) != IO_SUCCESS ) {
							ReleaseMutex(g_hMutexCache);						
							return 1;
						}

						for( i=0 ; i < child_info.nDevs && i < IO_MAX_DEVICES  ; i++ ){
						    
							child_info.ibs[i] = ( block_info_t * ) malloc(sizeof(block_info_t));
						    
							ret = IO_Open(child_info.devices[i], &(hClauer), IO_RD, IO_CHECK_IS_CLAUER );
						    
							if ( ret != IO_SUCCESS ) {
								ReleaseMutex(g_hMutexCache);
								return 1;
							}
						    
							ret = IO_ReadInfoBlock( hClauer, child_info.ibs[i] );
							if ( ret != IO_SUCCESS ) {
								ReleaseMutex(g_hMutexCache);
								return 1;
							}

							ret = IO_Close( hClauer );
						    
							if ( ret != IO_SUCCESS ) {
								ReleaseMutex(g_hMutexCache);
								return 1;
							}
						}

													
						break;

					default:
						break;

				}

				ReleaseMutex(g_hMutexCache);

			} else {
				LOG_Error(1, "No se pudo adquirir MUTEX CACHE. Se deja como estaba: %ld", GetLastError());
			}

			break;



		default:
			return ERROR_CALL_NOT_IMPLEMENTED;
    } 
 
    if (!SetServiceStatus (MyServiceStatusHandle,  &MyServiceStatus)) ;
   
    return NO_ERROR; 

}


void ImprimirUso (TCHAR *nombrePrograma)
{

	fprintf(stderr, "%s [/i] | [/u] | [/r]\n", nombrePrograma);
	fprintf(stderr, "\t. /i Instala el servicio\n");
	fprintf(stderr, "\t. /u Desinstala el servicio\n");
	fprintf(stderr, "\t. /r Ejecuta el servicio\n");

}



int CLOS_win_main ( void )
{
  HANDLE hChildStdInRd, hChildStdInWr;
  SECURITY_ATTRIBUTES sa;
  PROCESS_INFORMATION piProcInfo; 
  STARTUPINFO siStartInfo;
  WSAPROTOCOL_INFO  pi;
  BOOL bFuncRetn = FALSE; 
  DWORD dwWritten;
  trans_object_t tr_listener, tr_client;
  int ret, i, tam, err, zero, res=0, pid, tamVersion=0;
  clauer_handle_t hClauer;

  CRYPTO_Ini();
  TRANS_Init();

  ret = TRANS_Create(&tr_listener);
  if ( ret != CLOS_SUCCESS ) 
    return 1;
  
  while ( 1 ) {
    
    ret = TRANS_Accept(tr_listener, &tr_client);
    if ( ret != CLOS_SUCCESS ) 
		return 1;
	
	LOG_Msg(1,"ABRO SOCKET");
	if ( TRANS_SetRcvTimeout(tr_client, 10000) != CLOS_SUCCESS ) { 
		LOG_MsgError(1, "Imposible to stablish timeout"); 
		return ERR_CLOS; 
	}
	
	pid= TRANS_GetPidFromPort(tr_client);

	if ( TRANS_Receive(tr_client, &(child_info.funcId), 1) != CLOS_SUCCESS ) {	
		LOG_Msg(1,"\n\nCIERRO SOCKET");
		TRANS_Close(tr_client);
		continue; 
	}

	LOG_Debug(1,"GOT FUNCID: %d", child_info.funcId);
	
	/* Regeneramos la cache */	
	if ( child_info.funcId == 15 || GetOS() == CL_OS_WIN_2000 ){
		LOG_Msg(1, "Regenerando la CACHE");
		res= 0;
		for( i=0 ; i < child_info.nDevs && i < IO_MAX_DEVICES  ; i++ ){
			free( child_info.devices[i] );
			free( child_info.ibs[i] );
		}
	
		if ( IO_EnumClauers(&(child_info.nDevs), child_info.devices, IO_CHECK_IS_CLAUER) != IO_SUCCESS ) {
			ReleaseMutex(g_hMutexCache);
			res= 1;
			child_info.nDevs= 0;
		}
			
		for( i=0 ; i < child_info.nDevs && i < IO_MAX_DEVICES  ; i++ ){
			child_info.ibs[i] = ( block_info_t * ) malloc(sizeof(block_info_t));
			ret = IO_Open(child_info.devices[i], &(hClauer), IO_RD, IO_CHECK_IS_CLAUER );
	
			if ( ret != IO_SUCCESS ) {
				ReleaseMutex(g_hMutexCache);
				res=1;
				child_info.nDevs= 0;
			}

			ret = IO_ReadInfoBlock( hClauer, child_info.ibs[i] );
			if ( ret != IO_SUCCESS ) {
				ReleaseMutex(g_hMutexCache);
				res=1;
				child_info.nDevs= 0;
			}

			ret = IO_Close( hClauer );
			if ( ret != IO_SUCCESS ) {
				ReleaseMutex(g_hMutexCache);
				res=1;
				child_info.nDevs= 0;
			}
		}
		
		if ( child_info.funcId == 15 ){
			TRANS_Send(tr_client, &res, 1);
			LOG_Msg(1,"\n\nCIERRO SOCKET");
			TRANS_Close(tr_client);
			continue;
		}
	}

	/* Fin regeneracion cache */
	
	/* Obtenemos la version y el tipo */
	get_version_and_type(&child_info);

	sa.bInheritHandle = TRUE;
	sa.lpSecurityDescriptor = NULL;
	sa.nLength = sizeof(SECURITY_ATTRIBUTES);

	if ( CreatePipe(&hChildStdInRd, &hChildStdInWr, &sa, sizeof(WSAPROTOCOL_INFO)) ) {		

		SetHandleInformation( hChildStdInWr, HANDLE_FLAG_INHERIT, 0);

		// Set up members of the PROCESS_INFORMATION structure. 
		ZeroMemory( &piProcInfo, sizeof(PROCESS_INFORMATION) );
		// Set up members of the STARTUPINFO structure. 
 
		ZeroMemory( &siStartInfo, sizeof(STARTUPINFO) );
		siStartInfo.cb = sizeof(STARTUPINFO); 
		// siStartInfo.hStdError = hChildStdoutWr;
		// siStartInfo.hStdOutput = hChildStdoutWr;
		siStartInfo.hStdInput = hChildStdInRd;
		siStartInfo.dwFlags |= STARTF_USESTDHANDLES;

		if ( CreateProcess(NULL, 
						   "clos-client.exe",      // command line 
						   NULL,           // process security attributes 
						   NULL,           // primary thread security attributes 
						   TRUE,           // handles are inherited 
						   0,              // creation flags 
						   NULL,           // use parent's environment 
						   NULL,           // use parent's current directory 
						   &siStartInfo,   // STARTUPINFO pointer 
						   &piProcInfo) )
		{ // receives PROCESS_INFORMATION 

			// Duplicamos el socket.
			LOG_Msg(1,"\n\nRecibiendo petici�n, forkeando y pasando datos de cache");

			if ( WSADuplicateSocket(TRANS_GetSocket(tr_client) , piProcInfo.dwProcessId, &pi )) {
				CloseHandle(hChildStdInWr);
				LOG_Msg(1,"\n\nCIERRO SOCKET");
				TRANS_Close(tr_client);
				LOG_Error(1,"Al duplicar el socket, error= %ld",GetLastError());
				continue;
			}

			// Escribimos en el pipe. 

			LOG_Debug(9, "sizeof pi = %ld", sizeof(WSAPROTOCOL_INFO));

			if ( ! WriteFile(hChildStdInWr, &pi, sizeof pi, &dwWritten, NULL)) {
				CloseHandle(hChildStdInWr);
				LOG_Msg(1,"\n\nCIERRO SOCKET");
				TRANS_Close(tr_client);
				LOG_Error(1,"Al escribir en el pipe, error= %ld",GetLastError());
				continue;
			}

			// Pasamos la cach�
           
			if ( WaitForSingleObject(g_hMutexCache, 20000) != WAIT_FAILED ) {	

				if ( ! WriteFile(hChildStdInWr, &(child_info.nDevs), sizeof(int), &dwWritten, NULL) ) {
					CloseHandle(hChildStdInWr);
					LOG_Msg(1,"\n\nCIERRO SOCKET");
					TRANS_Close(tr_client);
					ReleaseMutex(g_hMutexCache);
					LOG_Error(1,"Al pasar la cach�, error= %ld",GetLastError());
					continue;
				}

				err = 0;
				for ( i = 0 ; i < child_info.nDevs ; i++ ) {
					tam = (int)strlen(child_info.devices[i]) + 1;
					if ( ! WriteFile(hChildStdInWr, &tam, sizeof(int), &dwWritten, NULL) ) {
						err = 1;
						LOG_Error(1,"Al escribir en el pipe, error= %ld",GetLastError());
						break;
					}
					LOG_Debug(9,"dwWritten: %ld",dwWritten);
					if ( ! WriteFile(hChildStdInWr, child_info.devices[i], tam, &dwWritten, NULL) ) {
						err = 1;
						LOG_Error(1,"Al escribir en el pipe, error= %ld",GetLastError());
						break;
					}
					LOG_Debug(9,"dwWritten: %ld",dwWritten);
					if ( ! WriteFile(hChildStdInWr, child_info.ibs[i], sizeof(block_info_t), &dwWritten, NULL)) {
						err = 1;
						LOG_Error(1,"Al escribir en el pipe, error= %ld",GetLastError());
						break;
					}
				}
				if ( ! WriteFile(hChildStdInWr, &pid, sizeof(pid), &dwWritten, NULL)) {
					err = 1;
					LOG_Error(1,"Al escribir en el pipe, error= %ld",GetLastError());
				}

				if ( err ) {
					CloseHandle(hChildStdInWr);
					LOG_Msg(1,"\n\nCIERRO SOCKET");				
					TRANS_Close(tr_client);
					ReleaseMutex(g_hMutexCache);
					LOG_Error(1,"Al hacer ReleaseMutex, error= %ld",GetLastError());
					continue;
				}

				if ( ! WriteFile(hChildStdInWr, &(child_info.funcId), sizeof(child_info.funcId), &dwWritten, NULL)) {
					CloseHandle(hChildStdInWr);
					LOG_Msg(1,"\n\nCIERRO SOCKET");
					TRANS_Close(tr_client);
					ReleaseMutex(g_hMutexCache);
					LOG_Error(1,"Al escribir en el pipe, error= %ld",GetLastError());
					continue;
				}	

				
				//Enviamos el type.
				if ( ! WriteFile(hChildStdInWr, &(child_info.sw_type), sizeof(child_info.sw_type), &dwWritten, NULL)) {
					err = 1;
					LOG_Error(1,"Al escribir en el pipe, error= %ld",GetLastError());
				}

				//Enviamos el tama�o del version string y version string.
				if (child_info.sw_version != NULL){
					tamVersion= strlen(child_info.sw_version)+1;
					if ( ! WriteFile(hChildStdInWr, &tamVersion, sizeof(tamVersion), &dwWritten, NULL)) {
						err = 1;
						LOG_Error(1,"Al escribir en el pipe, error= %ld",GetLastError());
					}
					if ( ! WriteFile(hChildStdInWr, child_info.sw_version, tamVersion, &dwWritten, NULL)) {
						err = 1;
						LOG_Error(1,"Al escribir en el pipe, error= %ld",GetLastError());
					}
				}
				else {
					tamVersion= 0;
					if ( ! WriteFile(hChildStdInWr, &tamVersion, sizeof(tamVersion), &dwWritten, NULL)) {
						err = 1;
						LOG_Error(1,"Al escribir en el pipe, error= %ld",GetLastError());
					}
				}

				ReleaseMutex(g_hMutexCache);
			}
			else{
				LOG_Error(1,"No se puedo adquirir el mutex, error= %ld",GetLastError());
				zero = 0;
				if ( ! WriteFile(hChildStdInWr, &zero, sizeof(int), &dwWritten, NULL) ) {
					CloseHandle(hChildStdInWr);
					LOG_Error(1,"Al escribir en el pipe, error= %ld",GetLastError());
					continue;
				}
			}
			// Close the pipe handle so the child process stops reading. 

			CloseHandle(piProcInfo.hProcess);
			CloseHandle(piProcInfo.hThread);

			CloseHandle(hChildStdInWr); 
			CloseHandle(hChildStdInRd);

		LOG_Msg(1,"Forkeo ok\n\n");
	   }

	}
	LOG_Msg(1,"CIERRO SOCKET");
	TRANS_Close(tr_client);
  }

	return CLOS_SUCCESS;
}



