#ifndef __BLOCK_H__
#define __BLOCK_H__

#include <stdio.h>
#include <blocktypes.h>

#include "err.h"


enum object_mode {
	MODE_CIPHERED,
	MODE_CLEAR,
	MODE_EMPTY,
	MODE_ERR
};
typedef enum object_mode object_mode_t;

/* A generic block */

int BLOCK_OBJECT_New       ( block_object_t **block );
int BLOCK_OBJECT_Free      ( block_object_t *block );
int BLOCK_OBJECT_Delete    ( block_object_t *block );
int BLOCK_OBJECT_New_Empty ( block_object_t *ob );

object_mode_t BLOCK_OBJECT_Get_Mode ( block_object_t *ob );

int BLOCK_OBJECT_Cipher   ( block_object_t *ob, char *pwd, block_info_t *ib );
int BLOCK_OBJECT_Decipher ( block_object_t *ob, char *pwd, block_info_t *ib );

void BLOCK_PRINT_InfoBlock   ( block_info_t *block, FILE *fp );
void BLOCK_PRINT_ObjectBlock ( block_object_t *block, FILE *fp);

#endif
