
#ifndef __COMMON_H__ 
#define __COMMON_H__

#define DEFAULT_FILE "/etc/clos.conf"
#define BUFF_SIZE 1024

#ifdef WIN32 
#include "windows.h"
#endif 

#include "clio.h"
#include "log.h"

/* types */ 
typedef struct {
    int cryf;
    int floppy;
    char * config;
} options_t; 


typedef struct {
    options_t opt;
    unsigned char nDevs;
    char * devices[IO_MAX_DEVICES];
    block_info_t * ibs[IO_MAX_DEVICES];
    int checksum;
    unsigned char funcId; /* Just in case the function is 15 ( empty cache ). */
    int sw_type;
    char * sw_version;
} child_info_t;


/* We declare the global variable here */
child_info_t child_info; 


/* functions */
void usage( void );
void parse( int argc, char ** argv,  options_t * opts );


/* defines */
#define ID_LEN 20

#endif 
