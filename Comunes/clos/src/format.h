#ifndef _CLAUER_FORMAT_H
#define _CLAUER_FORMAT_H

#ifdef WIN32 
//#include <windows.h>  //Conflicto de compilacion entre winsock.h y winsock2.h  
//#include <winbase.h>
#include <tchar.h>
#endif

#include "func.h"
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>



/* Data structures */
typedef struct _PARTITION {
        long int cyl_ini;
        long int head_ini;
        long int sect_ini;

        long int cyl_fin;
        long int head_fin;
        long int sect_fin;

        long int id;

        long int rel_sectors;
        long int num_sectors;
} PARTITION;


typedef struct _clauer_partition_info{
 //All in bytes 
 long long size;
 int type; 
}CLAUER_PARTITION_INFO;

typedef struct _clauer_layout_info{
 int npartitions;
 CLAUER_PARTITION_INFO cpi[4];
}CLAUER_LAYOUT_INFO;


typedef struct infoZone{
 	  char idenString[40];
 	  unsigned char id[20];
 	  int nrsv;
 	  int currentBlock;
 	  int totalBlocks;
 	  int formatVersion;
 	  char info[10240-76];
}INFO_ZONE;

/* Functions */
int FORMAT_CreateClauer(char * device, char porcentaje);
int FORMAT_GetSize(char * device, long long * tamanyo);
int FORMAT_GetClauerLayout(char * device, CLAUER_LAYOUT_INFO * cli);
int FORMAT_GetOptimalGeometry( char * device, int *heads, int *sectors, int *cylinders );
int FORMAT_CreateClauer(char * device, char percent);
int FORMAT_FormatClauerData( char * device );
int FORMAT_FormatearUnidadLogica( char *unidad );
int FORMAT_FormatClauerCrypto( char * device, char * pwd );

/* External functions */
extern int IO_GetBytesSectorByPath( const char * device );

#define NRSV_BLOCKS 7

#endif
