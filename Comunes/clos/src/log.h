#ifndef __LOG_H__
#define __LOG_H__

#include "block.h"

#define LOG_TYPE_ERROR   0
#define LOG_TYPE_DEBUG   1

#ifdef WIN32
#define LOG_FILE "c:\\clos-client.log"
#elif defined(LINUX)
#define LOG_FILE "/tmp/clos.log"
#endif

/* Para usar la librer�a es necesario definir la constante
 * simb�lica LOG en el fichero que se necesite. Cuando esta
 * constante no est� definida, las macros se definen como
 * blancos, de modo que el c�digo de loggeo se elimina.
 *
 * El compilador de microsoft no admite n�mero de argumentos
 * variables en macros, por lo tanto, la definici�n de �stas
 * LOG_Debug() y LOG_Error() cambian seg�n estemos o no utilizando
 * un compilador GNU C (LINUX) o no. Esto hace que �stas macros,
 * en la versi�n WIN32 sean un tanto m�s limitadas. Se a�ade
 * la macro LOG_Msg() que permite escribir un �nico mensaje
 * sin cadena de formato. Las macros LOG_Debug() y LOG_Error()
 * admiten cadena de formato con un �nico car�cter de conversi�n
 */


#ifndef LOG

#define LOG_Ini 
#define LOG_Msg
#define LOG_BeginFunc
#define LOG_EndFunc 
#define LOG_Debug
#define LOG_Error
#define LOG_MsgError 

#else 

#if defined(LINUX)

#define LOG_Debug(level,format, args...)   LOG_Write(LOG_TYPE_DEBUG, level, __FILE__,  __LINE__,  __FUNCTION__, format, ## args)
#define LOG_Error(level,format, args...)   LOG_Write(LOG_TYPE_ERROR, level, __FILE__,  __LINE__,  __FUNCTION__, format, ## args)
#define LOG_MsgError(level, msg)           LOG_Error(level, "%s", msg)
#define LOG_Msg(level, msg)                LOG_Debug(level, "%s", msg)

#elif defined(WIN32)

#define LOG_Debug(level,format, arg)   LOG_Write(LOG_TYPE_DEBUG, level, __FILE__, __LINE__, __FUNCTION__, format, arg)
#define LOG_Error(level,format, arg)   LOG_Write(LOG_TYPE_ERROR, level, __FILE__, __LINE__, __FUNCTION__, format, arg)
#define LOG_MsgError(level, msg)           LOG_Error(level, "%s", msg)
#define LOG_Msg(level, msg)            LOG_Debug(level, "%s", msg)

#endif

#define LOG_Ini(where, level)      LOG_Ini_Func (where, level)
#define LOG_BeginFunc(level)       LOG_Debug(level, "BEGIN %s", __FUNCTION__);
#define LOG_EndFunc(level,ret)     LOG_Write(LOG_TYPE_DEBUG, level, __FILE__, __LINE__, __FUNCTION__, "END %s : %d", __FUNCTION__, ret)

#endif /* defined(LOG) */

/*
 * Functions
 */

#define LOG_WHERE_STDERR  1
#define LOG_WHERE_FILE    2
#define LOG_WHERE_DEFAULT LOG_WHERE_FILE

int  LOG_Ini_Func ( int where, int level );
int  LOG_End      ( void );
void LOG_Write    ( int type, int level, char *file, int line, const char *func, const char *format, ...);

void LOG_WriteBlockObject ( int level, block_object_t *ob );
void LOG_WriteBlockInfo   ( int level, block_info_t *ib );



#endif

