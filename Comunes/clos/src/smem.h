#ifndef __SMEM_H__
#define __SMEM_H__

#include <stdio.h>
#include "err.h"

int SMEM_New     ( void **b, unsigned long size );
int SMEM_Free    ( void *b, unsigned long size );
int SMEM_Destroy ( void *b, unsigned long size );

#endif
