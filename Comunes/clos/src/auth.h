#ifndef __AUTH_H__
#define __AUTH_H__

#include "clio.h"
#include "err.h"


int AUTH_VerifyClauerPassphrase ( clauer_handle_t hClauer, char *passphrase);
int AUTH_IsPidAdmin(int pid);
int AUTH_IsSocketPidAdmin(int fd); 


#endif
