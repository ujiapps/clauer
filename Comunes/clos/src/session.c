#include "session.h"

#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include "err.h"


struct session_pool {
  session_t *first;
  session_t *last;
  unsigned long size;
};
typedef struct session_pool session_pool_t;


/*! \brief Allocates a new session object initilized with the parameters.
 *
 * Allocates a new session object initialized with the parameters.
 *
 * \param type
 *        The session type. It can be SESS_RDONLY or SESS_RDWR.
 *
 * \param tr
 *        The transport object to be used to communicate with the user.
 * 
 * \param hClauer
 *        The handleof the device
 * 
 * \param pwd
 *        The clauer's password. This buffer MUST BE ALLOCATED using
 *        SMEM_New().
 *
 * \param ib
 *        The information block of the device.
 *
 * \param s
 *        The session object that will be allocated.
 *
 * \retval CLOS_SUCCESS
 *         Ok
 *
 * \retval ERR_INVALID_PARAMETER
 *         One or more of the parameters are invalid.
 *
 * \retval ERR_OUT_OF_MEMORY
 *         Cannot allocate memory
 *
 * \remarks The buffers are not copied, so don't free them once the
 *          SESSION_New() has been called.
 *          Rememember that pwd must be allocated with SMEM_New()
 */

int SESSION_New ( session_type_t type,
		  trans_object_t tr,
		  clauer_handle_t *hClauer,
		  char *pwd,
		  block_info_t *ib,
		  options_t * opt,
		  session_t **s)
{

  if ( ! s )
    return ERR_INVALID_PARAMETER;

  *s = NULL;

  *s = ( session_t * ) malloc ( sizeof(session_t) );

  if ( ! *s ) 
    return ERR_OUT_OF_MEMORY;
  
  (*s)->type       = type;
  (*s)->tr         = tr;
  (*s)->hClauer    = hClauer;
  (*s)->pwd        = pwd;
  (*s)->ib         = ib;
  
  if ( opt ){
      (*s)->opt_cryf   = opt->cryf;
      (*s)->opt_floppy = opt->floppy;
  } 
  else {
      (*s)->opt_cryf   = 1;
      (*s)->opt_floppy = 1; /* Floppy is not longer working */
  }
 
  return CLOS_SUCCESS;

}




int SESSION_Free ( session_t *s )
{
	int ret;

	if ( s->pwd ) {
	    ret = SMEM_Free(s->pwd,(unsigned long) (strlen(s->pwd)));
		if ( ret != CLOS_SUCCESS )
			return ERR_CLOS;	
	}

	if ( s->ib )
		free(s->ib);

	return CLOS_SUCCESS;
}


