#ifndef __STUB_H__
#define __STUB_H__

#include "transport.h"
#include "common.h"


#define FUNC_ENUMERATE_DEVICES                 0
#define FUNC_START_SESSION                     1
#define FUNC_READ_RESERVED_ZONE		       2
#define FUNC_WRITE_RESERVED_ZONE               3
#define FUNC_CHANGE_PASSPHRASE                 4
#define FUNC_READ_INFO_ZONE                    5
#define FUNC_READ_OBJECT_ZONE_BLOCK            6 
#define FUNC_READ_ENUM_FIRST_OBJECT_TYPE       7
#define FUNC_READ_ENUM_OBJECT_TYPE             8
#define FUNC_READ_ALL_TYPE_OBJECTS             9
#define FUNC_READ_ALL_OCCUPED_BLOCKS          10
#define FUNC_WRITE_OBJECT_ZONE_BLOCK          11
#define FUNC_INSERT_OBJECT_ZONE_BLOCK         12
#define FUNC_ERASE_OBJECT_BLOCK               13
#define FUNC_GET_HARDWARE_ID                  14
#define FUNC_REGENERATE_CACHE                 15
#define FUNC_GET_CLOS_VERSION                 16 // The versi�n is hardcoded on the code must be increased 
                                                 // on each release 
#define FUNC_GET_LOGICAL_UNIT                 17

#define FUNC_CLOSE_SESSION                    20 
#define FUNC_ENUMERATE_USBS                  128
#define FUNC_CREATE_CLAUER                   129
#define FUNC_GET_LAYOUT                      130
#define FUNC_FORMAT_DATA_PARTITION           131
#define FUNC_FORMAT_CRYPTO_PARTITION         132
	
#define FUNC_LOCK_CLAUER                      21 /* 14 */
#define FUNC_UNLOCK_CLAUER                    22 /* 15 */


int stub ( trans_object_t tr );
void timeout_handler ( int signal );






#endif
