#ifndef __FUNC_H__
#define __FUNC_H__

#ifdef DEBUG
#include "log.h"
#endif
#include "transport.h"
#include <clio/clio.h>
#include "block.h"
#include "session.h"

#define MAX_CLAUERS 1024
#define MAX_PATH_LEN 128
#define MAX_PASS_LEN 127
#define HW_ID_LEN    16  // It will be an md5

#define ERROR_NO_ADMIN 10 // Take care with libRT when changing this.

int FUNC_EnumerateDevices      ( session_t *s );
int FUNC_StartSession          ( session_t *s );
int FUNC_ReadReservedZone      ( session_t *s );
int FUNC_WriteReservedZone     ( session_t *s );
int FUNC_ChangePassphrase      ( session_t *s );
int FUNC_ReadInfoZone          ( session_t *s );
int FUNC_ReadObjectZoneBlock   ( session_t *s );
int FUNC_ReadEnumFirstObjectType   ( session_t *s );
int FUNC_ReadEnumObjectType        ( session_t *s );
int FUNC_ReadAllTypeObjects    ( session_t *s );
int FUNC_ReadAllOccupedBlocks  ( session_t *s );
int FUNC_WriteObjectZoneBlock  ( session_t *s );
int FUNC_InsertObjectZoneBlock ( session_t *s );
int FUNC_EraseObjectBlock      ( session_t *s );
int FUNC_GetHardwareId         ( session_t *s );
int FUNC_GetClosVersion        ( session_t *s );
int FUNC_GetLogicalUnitFromPhysicalDrive ( session_t *s );
int FUNC_CloseSession          ( session_t *s );
int FUNC_EnumerateDevices      ( session_t *s );
int FUNC_EnumerateUSBs         ( session_t *s );
int FUNC_CreateClauer          ( session_t *s );
int FUNC_FormatData            ( session_t *s );
int FUNC_FormatCrypto          ( session_t *s );
int FUNC_GetClauerLayout       ( session_t *s);
int FUNC_LockClauer            ( session_t *s );
int FUNC_UnlockClauer          ( session_t *s );

#endif

