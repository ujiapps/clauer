#include <windows.h>
#include <wincrypt.h>

#include <iostream>

#include "../CRYPTOWrap.h"


using namespace std;



void LeerObjeto (char *fileName, BYTE **objeto, DWORD *tam)
{
	FILE *f;

	f = fopen(fileName, "rb");
	fseek(f, 0, SEEK_END);
	*tam = ftell(f);
	fseek(f, 0, SEEK_SET);
	*objeto = new BYTE [*tam];
	fread(*objeto, 1, *tam, f);
	fclose(f);
}




void main (void)
{
	HCRYPTPROV hProv;
	HCRYPTKEY hKey;
	unsigned char blob[10240];
	DWORD blobSize = 10240;

	CryptAcquireContext(&hProv, NULL, NULL, PROV_RSA_FULL, CRYPT_VERIFYCONTEXT);
	CryptGenKey(hProv, AT_SIGNATURE, (2048<<16) | CRYPT_EXPORTABLE, &hKey);
	CryptExportKey(hKey, 0, PRIVATEKEYBLOB, 0, blob, &blobSize);
	CryptDestroyKey(hKey);
	CryptReleaseContext(hProv,0);

	FILE *fp = fopen("C:\\mi_blob", "wb");
	fwrite(blob, blobSize,1,fp);
	fclose(fp);


	unsigned long tamLlave;
	unsigned char *llave;

	CRYPTO_Ini();

	if ( CRYPTO_BLOB2LLAVE(blob, blobSize, NULL, &tamLlave) != 0 ){
		fprintf(stderr, "[ERROR] 1\n");
		return;
	}

	llave = (unsigned char *) malloc (tamLlave);
	if ( ! llave ) {
		fprintf(stderr, "[ERROR] malloc devuelve null\n");
		return;
	}

	if ( CRYPTO_BLOB2LLAVE(blob, blobSize, llave, &tamLlave) != 0 ) {
		fprintf(stderr, "[ERROR] 2\n");
		return;
	}

	fp = fopen("C:\\llave.pem", "wb");
	fwrite(llave, tamLlave,1, fp);
	fclose(fp);

}


