#include <stdio.h>
#include <stdlib.h>

#include "CRYPTOWrap.h"


#include <string.h>



void pruebaPBE (void)
{

    char auxPassword[5];
    PASSWORD password;
    char salt[8];
    char *bloqueClaro, *bloqueDescifrado;
    BYTE *bloqueCifrado = NULL;
    int tamBloqueCifrado, tamBloqueDescifrado, i;

    CRYPTO_Random(8, salt);

    strcpy(auxPassword, "kkkk");
    password = PASSWORD_Nueva(auxPassword);

    bloqueClaro = (char *) malloc (5120);

    for ( i = 0 ; i < 5120 ; i++ )
	bloqueClaro[i] = 'A';

    /*
     * Cifro
     */

    if ( CRYPTO_PBE_Cifrar(password, salt, 1, CRYPTO_CIPHER_AES_128_CBC, bloqueClaro, 5120, NULL, &tamBloqueCifrado) ) {
	printf("Error cifrando 1\n");
	exit(1);
    }
    bloqueCifrado = (BYTE *) malloc (tamBloqueCifrado);
    if ( CRYPTO_PBE_Cifrar(password, salt, 1, CRYPTO_CIPHER_AES_128_CBC, bloqueClaro, 5120, bloqueCifrado, &tamBloqueCifrado) ) {
	printf("Error cifrando 2\n");
	exit(1);
    }
    

    printf("TamBloqueCifrado = %d\n", tamBloqueCifrado);

    /*
     * Descifro
     */

    if ( CRYPTO_PBE_Descifrar(password, salt, 1, CRYPTO_CIPHER_AES_128_CBC, bloqueCifrado, tamBloqueCifrado, NULL, &tamBloqueDescifrado) ) {
	printf("Error descifrando 1\n");
	exit(1);
    }
    printf("tamBloqueDescifrado = %d\n", tamBloqueDescifrado);
    bloqueDescifrado = (BYTE *) malloc (tamBloqueDescifrado);

    if ( CRYPTO_PBE_Descifrar(password, salt, 1, CRYPTO_CIPHER_AES_128_CBC, bloqueCifrado, tamBloqueCifrado, bloqueDescifrado, &tamBloqueDescifrado) ) {
	printf("Error descifrando 2\n");
	exit(1);
    }

    printf("tamBloqueDescifrado = %d\n", tamBloqueDescifrado);

    while ( i < 30000 ) 
      printf("%d\n",i++);



}





void pruebaCifrado (void)
{
    
    CRYPTO_KEY llave;
    BYTE bloqueClaro[5120], *bloqueCifrado = NULL, *bloqueDescifrado = NULL;
    int tamBufferCifrado, tamBloqueDescifrado;
    int i;

    for ( i = 0 ; i < 5120 ; i++ ) 
	bloqueClaro[i] = 'A';

    CRYPTO_KEY_New(CRYPTO_CIPHER_AES_128_CBC, &llave);
    
    if ( CRYPTO_Cifrar(llave, 0, CRYPTO_CIPHER_AES_128_CBC, bloqueClaro, 5120, NULL, &tamBufferCifrado) ) {
	fprintf(stderr, "ERROR. Cifrando 1\n");
	return;
    }
    
    bloqueCifrado = (BYTE *) malloc (tamBufferCifrado);

    if ( CRYPTO_Cifrar(llave, 0, CRYPTO_CIPHER_AES_128_CBC, bloqueClaro, 5120, bloqueCifrado, &tamBufferCifrado) ) {
	fprintf(stderr, "ERROR. Cifrando 2\n");
	return;
    }
    
    /*
     * Descifrar el bloque
     */

    if ( CRYPTO_Descifrar(llave, 0, CRYPTO_CIPHER_AES_128_CBC, bloqueCifrado, tamBufferCifrado, NULL, &tamBloqueDescifrado) ) {
	fprintf(stderr, "ERROR. Descifrando 1\n");
	return;
    }

    bloqueDescifrado = (BYTE *) malloc (tamBloqueDescifrado);

    if ( CRYPTO_Descifrar(llave, 0, CRYPTO_CIPHER_AES_128_CBC, bloqueCifrado, tamBufferCifrado, bloqueDescifrado, &tamBloqueDescifrado) ) {
      fprintf(stderr, "ERROR. Descifrando 2\n");
      return;
    }
    
    for ( i = 0 ; i < tamBloqueDescifrado ; i++ )  
      printf("%c", bloqueDescifrado[i]);

}





int main (int argc, char **argv)
{

    CRYPTO_Ini();

    pruebaPBE();

    printf("YA TA\n");

    CRYPTO_Fin();

    return 0;

}
