#include <windows.h>

#include <iostream>

#include "../b64.h"


using namespace std;




BOOL PEM2DER (BYTE *pem, unsigned long tamPEM, BYTE *der, unsigned long *tamDER)
{
	BYTE *cuerpo_i, *cuerpo_f,*aux;
	BOOL encontrado = FALSE, ret = TRUE;
    unsigned long pos;

	/* Buscamos la cabecera
     */
	pos = 0;
	aux = pem;
	while (!encontrado && (pos<tamPEM)) {
		while ( ((*aux) != '-') && (pos< tamPEM) ) {
			++aux;
            ++pos;
		}
        if ( strncmp((char *)aux,"-----BEGIN CERTIFICATE-----", 27) == 0 )
			encontrado = TRUE;
		else {
			++aux;
            ++pos;
		}
	}

    if ( !encontrado ) {
		ret = FALSE;
		goto finPEM2DER;
	}


    cuerpo_i = aux + 28; /* del begin certificate y el cambio de l�nea */

   /*
	* Ahora eliminamos el pie
	*/

	aux = cuerpo_i;
    encontrado = FALSE;
    while ( !encontrado && (pos < tamPEM)) {
		while ( (*aux != '-') && (pos < tamPEM)) {
			++aux;
            ++pos;
		}
        if ( strncmp((char *)aux,"-----END CERTIFICATE-----", 25) == 0 ) {
			encontrado = TRUE;
		} else {
			++pos;
            ++aux;
		}
	}


    if (!encontrado) {
		ret = FALSE;
		goto finPEM2DER;
	}

	cuerpo_f = aux-1;

    B64_Decodificar(cuerpo_i, cuerpo_f-cuerpo_i+1,NULL,tamDER);

    if ( !der ) {
		ret = TRUE;
		goto finPEM2DER;
	}

	B64_Decodificar(cuerpo_i, cuerpo_f-cuerpo_i+1,der,tamDER);

finPEM2DER:


	return ret;

}


void main (void)
{
	BYTE *pem, *der, *derBueno;
	DWORD tamPEM, tamDER, tamDERBueno;
	FILE *fp;

	fp = fopen("C:\\folch.der", "rb");
	fseek(fp, 0, SEEK_END);
	tamDERBueno = ftell(fp);
	fseek(fp, 0, SEEK_SET);
	derBueno = new BYTE [tamDERBueno];
	fread(derBueno, 1, tamDERBueno, fp);
	fclose(fp);

	fp = fopen("C:\\folch.pem", "rb");
	fseek(fp, 0, SEEK_END);
	tamPEM = ftell(fp);
	fseek(fp, 0, SEEK_SET);
	pem = new BYTE [tamPEM];
	fread(pem, 1, tamPEM, fp);
	fclose(fp);

	if ( ! PEM2DER(pem, tamPEM, NULL, &tamDER) ) {
		cerr << "ERROR 1" << endl;
		return;
	}

	der = new BYTE [tamDER];

	if ( ! PEM2DER(pem, tamPEM, der, &tamDER) ) {
		cerr << "ERROR 1" << endl;
		return;
	}

	cout << "OK" << endl;

}
