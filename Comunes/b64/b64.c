#include <stdio.h>
#include <stdlib.h>

/*
 * Tabla de codificaci�n
 */

static unsigned char *tb64 = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";

/*
 * Tabla de codificacion inversa. Algunas posiciones no son relevantes.
 * Funciona de la siguiente manera. Tomamos un caracter de base64 (exceptuando
 * el =. Le restamos 43 (posicion del '+' en la tabla de codigo ASCII) y
 * el resultado es el indice que utilizaremos para acceder a la tabla inversa.
 */

static unsigned char tb64_i[] = {62,-1,-1,-1,63,52,53,54,55,56,57,58,59,60,61,-1,-1,-1,-1,-1,-1,-1,0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,-1,-1,-1,-1,-1,-1,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51};



void B64_Codificar (unsigned char *in, unsigned long tamIn, unsigned char *out, unsigned long *tamOut)
{
  int pad;
  register unsigned long to, ti, pi, po,nl;
  unsigned char last[3];

  pad = tamIn % 3;
  if ( pad )
    ti = tamIn + 3 - pad;
  else
    ti = tamIn;
  
  to = 4*ti/3;
  to += to / 72 + 1;
  
  *tamOut = to;
  
  if ( !out )
    return;
  
  ti -= 3;
  po = 0;
  nl = 0;
  for ( pi = 0 ; pi < ti ; pi+=3,po+=4 ) {
    
    *(out+po)   = tb64[*(in+pi) >> 2];
    *(out+po+1) = tb64[((*(in+pi) & 0x3) << 4) | (*(in+pi+1)>>4)];
    *(out+po+2) = tb64[((*(in+pi+1) & 0x0f) << 2) | ((*(in+pi+2) & 0xc0) >> 6)];
    *(out+po+3) = tb64[*(in+pi+2) & 0x3f];
    
    /*B64_CodificarBloque(in+pi, out+po);*/
    
    nl+=4;
    if ( nl == 72 ) {
      *(out+po+4) = '\n';
      ++po;
      nl = 0;
    }
    
  }
  
  /*
   * el �ltimo bloque
   */
  
  if ( !pad ) {
    *(out+po)   = tb64[*(in+pi) >> 2];
    *(out+po+1) = tb64[((*(in+pi) & 0x3) << 4) | (*(in+pi+1)>>4)];
    *(out+po+2) = tb64[((*(in+pi+1) & 0x0f) << 2) | ((*(in+pi+2) & 0xc0) >> 6)];
    *(out+po+3) = tb64[*(in+pi+2) & 0x3f];
  } else {
    last[0] = *(in+pi);
    if ( pad == 1 )
      last[1] = 0;
    else
      last[1] = *(in+pi+1);
    last[2]=0;

    *(out+po)   = tb64[last[0] >> 2];
    *(out+po+1) = tb64[((last[0] & 0x3) << 4) | (last[1]>>4)];
    *(out+po+2) = tb64[((last[1] & 0x0f) << 2) | ((last[2] & 0xc0) >> 6)];
    *(out+po+3) = tb64[last[2] & 0x3f];

    if ( pad == 1 )
      *(out+po+2) = '=';

    *(out+po+3) = '=';
  }

  *(out+po+4) = '\n';

}





void B64_Decodificar (unsigned char *in, unsigned long tamIn, unsigned char *out, unsigned long *tamOut)
{
  
  register unsigned long i;
  unsigned long eTamIn;
  int bytesPadding = 0;
  unsigned char *aux;
  
  
  if ( !tamOut )
    return;
  
  
  /* Calculamos el tama�o efectivo de la entrada (eliminamos los cambios de l�nea)
   */
  
  eTamIn = tamIn;
  
  i = 0;
  while ( i < tamIn ) {
    
    if ( *(in+i) == '\n' ) 
      --eTamIn;
    
    
    if ( *(in+i) == '\r' ) 
      --eTamIn;
    
    ++i;
  }
  
  /* Calculamos el tama�o de la salida
   */
  
  aux = in + tamIn - 1;
  
  if ( *aux == '\n' )
    --aux;
  
  if ( *aux == '\r' )
    --aux;
  
  if ( *aux == '=' ) {
    ++bytesPadding;
    --aux;
    if ( *aux == '=' ) 
      ++bytesPadding;
  }
  
  *tamOut = 3*eTamIn / 4 - bytesPadding;
  
  if ( !out )
    return;
  
  /* Comenzamos a decodificar. En el bucle todo menos el �ltimo bloque de 4 bytes
   */
  
  i = 0;
  while ( i < (eTamIn-4) ) {
    
    if ( (*in != '\r') && (*in != '\n') ) {
      
      *out = (tb64_i[*in - 43] << 2) | (tb64_i[*(in+1)-43] >> 4);
      *(out+1) = (tb64_i[*(in+1)-43] << 4 ) | ( tb64_i[*(in+2)-43] >> 2 );
      *(out+2) = (tb64_i[*(in+2)-43] << 6 ) | tb64_i[*(in+3)-43];			
      
      i   += 4;
      in  += 4;
      out += 3;
      
    } else
      ++in;
  }
  
  /* El ultimo bloque 
   */
  
  if ( *in == '\r' )
    ++in;
  
  if ( *in == '\n' )
    ++in;
  
  *out = (tb64_i[*in - 43] << 2) | (tb64_i[*(in+1) - 43] >> 4);
  if ( bytesPadding != 2 ) {
    *(out+1) = (tb64_i[*(in+1)-43] << 4) | (tb64_i[*(in+2)-43] >> 2);
    if ( bytesPadding == 0 )
      *(out+2) = (tb64_i[*(in+2)-43] << 6) | (tb64_i[*(in+3)-43]);
  }
  
}



