// NetejarClauerDlg.cpp : implementation file
//

#include "stdafx.h"
#include "NetejarClauer.h"
#include "NetejarClauerDlg.h"

#include <USBLowLevel/usb_lowlevel.h>
#include <libFormat/usb_format.h>


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


#include "mbr_c.c"

/////////////////////////////////////////////////////////////////////////////
// CNetejarClauerDlg dialog

CNetejarClauerDlg::CNetejarClauerDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CNetejarClauerDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CNetejarClauerDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
	// Note that LoadIcon does not require a subsequent DestroyIcon in Win32
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CNetejarClauerDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CNetejarClauerDlg)
	DDX_Control(pDX, IDOK, m_btnAceptar);
	DDX_Control(pDX, IDCANCEL, m_btnCancel);
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CNetejarClauerDlg, CDialog)
	//{{AFX_MSG_MAP(CNetejarClauerDlg)
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CNetejarClauerDlg message handlers

BOOL CNetejarClauerDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon
	
	// TODO: Add extra initialization here
	
	return TRUE;  // return TRUE  unless you set the focus to a control
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CNetejarClauerDlg::OnPaint() 
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, (WPARAM) dc.GetSafeHdc(), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

// The system calls this to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CNetejarClauerDlg::OnQueryDragIcon()
{
	return (HCURSOR) m_hIcon;
}


/* 0 ok
 * 1 error
 * 2 > 1 stick
 * 3 0 sticks
 */
int ObtenerClauer ( int *device, char unidad[3] )
{
	char **unis = NULL, **aux;
	int * disp = NULL, nDisp, err = 0, nClauers = 0,i;
	HANDLE_DISPOSITIVO h;
	PARTICION particiones[4];

	if ( lowlevel_enumerar_dispositivos(NULL, NULL, &nDisp ) != 0 ) {
		err = 1;
		goto finObtenerClauer;
	}

	if ( 0 == nDisp ) {
		err = 3;
		goto finObtenerClauer;
	}

	unis = new char * [nDisp+1];
	if ( ! unis ) {
		err = 1;
		goto finObtenerClauer;
	}

	memset(unis, 0, (nDisp+1)*sizeof(char *));

	for ( i = 0 ; i < nDisp ; i++ ) {
		*(unis+i) = new char [3];

		if ( ! *(unis+i) ) {
			err = 1;
			goto finObtenerClauer;
		}
	}

	disp = new int [nDisp];

	if ( ! disp ) {
		err = 1;
		goto finObtenerClauer;
	}

	if ( lowlevel_enumerar_dispositivos(unis, disp, &nDisp) != 0 ) {
		err = 1;
		goto finObtenerClauer;
	}


	/* A ver cu�les de ellos son clauers */

	for ( i = 0 ; i < nDisp ; i++ ) {

		if ( lowlevel_abrir_dispositivo ( *(disp+i), &h ) != 0 ) {
			err = 1;
			goto finObtenerClauer;
		}

		if ( lowlevel_leer_particiones( h, particiones ) != 0 ) {
			err = 1;
			goto finObtenerClauer;			
		}

		if ( 0x69 == particiones[3].id ) {

			if ( nClauers > 0 ) {
				err = 2;
				goto finObtenerClauer;
			}

			++nClauers;

			*device = *(disp+i);
			memcpy(unidad, *(unis+i), 3);
		}
	}

	if ( nClauers == 0 ) 
		err = 3;


finObtenerClauer:

	if ( unis ) {
		aux = unis;
			
		while ( *aux ) {
			delete [] *aux;
			++aux;
		}

		delete [] unis;
	}

	if ( disp ) 
		delete [] disp;


	return err;

}




void CNetejarClauerDlg::OnOK() 
{

	int device;
	char unidad[3];

	switch ( ObtenerClauer(&device, unidad)	) {
	case 1:
		MessageBox("Error buscando clauers en el sistema", "CLNET", MB_OK|MB_ICONERROR);
		EndDialog(1);
		break;

	case 2:
		MessageBox("M�s de un stick insertado en el sistema\r\nDeje s�lo el que desee utilizar", "CLNET", MB_OK|MB_ICONINFORMATION);
		return;

	case 3:
		MessageBox("No se encontraron clauers para limpiar en el sistema\r\nNo hay clauers insertados o el clauer no necesita limpieza", "CLNET", MB_OK|MB_ICONINFORMATION);
		return;
	}


	if ( MessageBox("�Seguro que desea limpiar el stick? (TODOS los datos se perder�n)", "CLNET :: ATENCI�N", MB_YESNO|MB_ICONWARNING) == IDNO ) {
		return;
	}


	/* Escribimos el mbr */

	HANDLE h;
	char deviceName[17];
	DWORD bytesWritten;

	sprintf(deviceName, "\\\\.\\PHYSICALDRIVE%d", device);

	h = CreateFile(deviceName,
				   GENERIC_READ | GENERIC_WRITE,        // lo abrimos para leer y escribir
				   FILE_SHARE_READ | FILE_SHARE_WRITE,  // compartido para leer y escribir
				   NULL,                                                            // el handle no se puede heredar
				   OPEN_EXISTING,                                           // se supone que existe
				   FILE_ATTRIBUTE_NORMAL | FILE_FLAG_WRITE_THROUGH | FILE_FLAG_NO_BUFFERING, // fichero normal
				   NULL);                                                           // sin template file


	if ( h == INVALID_HANDLE_VALUE ) {
		MessageBox("Imposible abrir dispositivo", "CLNET :: ERROR", MB_OK | MB_ICONERROR);
		return ;
	}

	if ( ! WriteFile(h, mbr, 512, &bytesWritten, NULL) ) {
		CloseHandle(h);
		MessageBox("Imposible escribir mbr", "CLNET :: ERROR", MB_OK | MB_ICONERROR);
		return ;
	}

	if ( ! CloseHandle(h) ) {
		MessageBox("Imposible Cerrar dispositivo", "CLNET :: ERROR", MB_OK | MB_ICONERROR);
		return;
	}

	/* Ahora vamos a formatear */

	if ( lowlevel_abrir_dispositivo ( device, &h ) != 0 ) {
		MessageBox("Imposible abrir dispositivo", "CLNET :: ERROR" , MB_OK | MB_ICONERROR);
		return;
	}

	int result;

	result = format_releer_mbr( h );

	if ( result == ERR_FORMAT_FUNCION_NO_DISPONIBLE ) {

		MessageBox("Por favor, retire el stick USB y pulse el bot�n Aceptar",
 				   "CLNET :: Atenci�n", MB_OK|MB_ICONEXCLAMATION);

		MessageBox("Por favor, inserte de nuevo el stick y pulse el bot�n Aceptar",
  				"CLNET :: Atenci�n", MB_OK|MB_ICONEXCLAMATION);
	}

	if ( lowlevel_cerrar_dispositivo( h ) != 0 ) {
		MessageBox("Error cerrando dispositivo", "CLNET :: ERROR", MB_OK| MB_ICONERROR);
		return;
	}

	result = format_formatear_unidad_logica_win( unidad );
	if ( result != ERR_FORMAT_NO ) {
		lowlevel_cerrar_dispositivo(h);
		MessageBox("No se pudo formatear dispositivo",
			       "CLNET :: ERROR", MB_ICONERROR | MB_OK);
		return;
	}


	lowlevel_cerrar_dispositivo(h);


	MessageBox("Operaci�n realizada con �xito", "CLNET :: ATENCI�N", MB_OK | MB_ICONINFORMATION);

}

