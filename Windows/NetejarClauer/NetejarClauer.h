// NetejarClauer.h : main header file for the NETEJARCLAUER application
//

#if !defined(AFX_NETEJARCLAUER_H__E015C88A_EFBB_486B_8D57_E7E5469D12B4__INCLUDED_)
#define AFX_NETEJARCLAUER_H__E015C88A_EFBB_486B_8D57_E7E5469D12B4__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"		// main symbols

/////////////////////////////////////////////////////////////////////////////
// CNetejarClauerApp:
// See NetejarClauer.cpp for the implementation of this class
//

class CNetejarClauerApp : public CWinApp
{
public:
	CNetejarClauerApp();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CNetejarClauerApp)
	public:
	virtual BOOL InitInstance();
	//}}AFX_VIRTUAL

// Implementation

	//{{AFX_MSG(CNetejarClauerApp)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_NETEJARCLAUER_H__E015C88A_EFBB_486B_8D57_E7E5469D12B4__INCLUDED_)
