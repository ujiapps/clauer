// NetejarClauerDlg.h : header file
//

#if !defined(AFX_NETEJARCLAUERDLG_H__1B5B313A_7CA6_4510_831A_38D71A3E350E__INCLUDED_)
#define AFX_NETEJARCLAUERDLG_H__1B5B313A_7CA6_4510_831A_38D71A3E350E__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

/////////////////////////////////////////////////////////////////////////////
// CNetejarClauerDlg dialog

class CNetejarClauerDlg : public CDialog
{
// Construction
public:
	CNetejarClauerDlg(CWnd* pParent = NULL);	// standard constructor

// Dialog Data
	//{{AFX_DATA(CNetejarClauerDlg)
	enum { IDD = IDD_NETEJARCLAUER_DIALOG };
	CButton	m_btnAceptar;
	CButton	m_btnCancel;
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CNetejarClauerDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	HICON m_hIcon;

	// Generated message map functions
	//{{AFX_MSG(CNetejarClauerDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	virtual void OnOK();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_NETEJARCLAUERDLG_H__1B5B313A_7CA6_4510_831A_38D71A3E350E__INCLUDED_)
