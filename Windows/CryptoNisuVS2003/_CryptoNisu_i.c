

/* this ALWAYS GENERATED file contains the IIDs and CLSIDs */

/* link this file in with the server and any clients */


 /* File created by MIDL compiler version 6.00.0361 */
/* at Fri Sep 29 12:19:36 2006
 */
/* Compiler settings for _CryptoNisu.idl:
    Oicf, W1, Zp8, env=Win32 (32b run)
    protocol : dce , ms_ext, c_ext, robust
    error checks: allocation ref bounds_check enum stub_data 
    VC __declspec() decoration level: 
         __declspec(uuid()), __declspec(selectany), __declspec(novtable)
         DECLSPEC_UUID(), MIDL_INTERFACE()
*/
//@@MIDL_FILE_HEADING(  )

#if !defined(_M_IA64) && !defined(_M_AMD64)


#pragma warning( disable: 4049 )  /* more than 64k source lines */


#ifdef __cplusplus
extern "C"{
#endif 


#include <rpc.h>
#include <rpcndr.h>

#ifdef _MIDL_USE_GUIDDEF_

#ifndef INITGUID
#define INITGUID
#include <guiddef.h>
#undef INITGUID
#else
#include <guiddef.h>
#endif

#define MIDL_DEFINE_GUID(type,name,l,w1,w2,b1,b2,b3,b4,b5,b6,b7,b8) \
        DEFINE_GUID(name,l,w1,w2,b1,b2,b3,b4,b5,b6,b7,b8)

#else // !_MIDL_USE_GUIDDEF_

#ifndef __IID_DEFINED__
#define __IID_DEFINED__

typedef struct _IID
{
    unsigned long x;
    unsigned short s1;
    unsigned short s2;
    unsigned char  c[8];
} IID;

#endif // __IID_DEFINED__

#ifndef CLSID_DEFINED
#define CLSID_DEFINED
typedef IID CLSID;
#endif // CLSID_DEFINED

#define MIDL_DEFINE_GUID(type,name,l,w1,w2,b1,b2,b3,b4,b5,b6,b7,b8) \
        const type name = {l,w1,w2,{b1,b2,b3,b4,b5,b6,b7,b8}}

#endif !_MIDL_USE_GUIDDEF_

MIDL_DEFINE_GUID(IID, IID_IClauer,0xC6F30741,0x9C2D,0x4B00,0xAB,0xC3,0x74,0x00,0xAD,0xC6,0x2F,0x0C);


MIDL_DEFINE_GUID(IID, IID_IHuella,0x548611C0,0xF1CF,0x4987,0x9F,0x58,0xCF,0x91,0x9F,0x64,0x34,0x01);


MIDL_DEFINE_GUID(IID, LIBID_CryptoNisu,0x1A4FCA86,0x07B0,0x4FEC,0x80,0xCB,0xBC,0x5B,0xE5,0xA7,0xC8,0x8E);


MIDL_DEFINE_GUID(CLSID, CLSID_CClauer,0xEF9DBF85,0xF6AE,0x44A9,0xA0,0xF2,0x34,0x42,0x12,0xE3,0x93,0x82);


MIDL_DEFINE_GUID(CLSID, CLSID_CHuella,0x80454CE9,0x5F0F,0x418B,0xAC,0x53,0xC9,0x23,0x66,0x1E,0xC7,0x59);

#undef MIDL_DEFINE_GUID

#ifdef __cplusplus
}
#endif



#endif /* !defined(_M_IA64) && !defined(_M_AMD64)*/

