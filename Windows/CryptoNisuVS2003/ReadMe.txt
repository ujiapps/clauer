========================================================================
    BIBLIOTECA DE PLANTILLAS ACTIVA: Informaci�n general del proyecto CryptoNisu
========================================================================

El Asistente para aplicaciones ha creado el proyecto CryptoNisu con el fin de utilizarlo como base de referencia
para escribir la biblioteca de v�nculos din�micos (archivo DLL).
Proyecto implementado con los atributos de Visual C++.

Archivo que incluye un resumen del contenido de los archivos que
componen el proyecto.

CryptoNisu.vcproj
    Archivo de proyecto principal para proyectos de VC++ generados con el Asistente para aplicaciones. 
    Contiene informaci�n acerca de la versi�n de Visual C++ que gener� el archivo y 
    de las plataformas, configuraciones y caracter�sticas del proyecto seleccionadas con
    el Asistente para aplicaciones.

_CryptoNisu.idl
    El compilador genera este archivo una vez creado el proyecto. Incluye las definiciones IDL 
    de la biblioteca de tipos, las interfaces y las coclases definidas en el proyecto.
    El compilador MIDL procesa el archivo que va a generar:
        Definiciones de interfaces de C++ y declaraciones de GUID (_CryptoNisu.h)
        Definiciones de GUID                                      (_CryptoNisu_i.c)
        Una biblioteca de tipos                                   (_CryptoNisu.tlb)
        C�digo de c�lculo de referencias                          (_CryptoNisu_p.c y dlldata.c)
CryptoNisu.cpp
    Archivo que contiene el mapa de objetos y la implementaci�n de las exportaciones de los archivos DLL.
CryptoNisu.rc
    Listado de los recursos de Microsoft Windows
    que utiliza el programa.

CryptoNisu.def
    Archivo de definici�n de m�dulo que suministra el vinculador con informaci�n acerca de las exportaciones
    que el archivo DLL necesita. Contiene exportaciones para:
        DllGetClassObject  
        DllCanUnloadNow    
        GetProxyDllInfo    
        DllRegisterServer	
        DllUnregisterServer

/////////////////////////////////////////////////////////////////////////////
Otros archivos est�ndar:

StdAfx.h, StdAfx.cpp
    Archivos utilizados para generar un archivo de encabezado precompilado (PCH)
    llamado CryptoNisu.pch, as� como un archivo de tipos precompilado llamado StdAfx.obj.

Resource.h
    Archivo de encabezado est�ndar que define los identificadores de recursos.

/////////////////////////////////////////////////////////////////////////////
Proyecto DLL del proxy o c�digo auxiliar, y archivo de definici�n de m�dulo:

CryptoNisups.vcproj
    Archivo de proyecto para generar el archivo DLL del proxy o c�digo auxiliar si es necesario.
	El archivo IDL del proyecto principal debe contener al menos una interfaz
	y se debe compilar primero este archivo antes de generar el archivo DLL del proxy o c�digo auxiliar. Este proceso genera
	dlldata.c, CryptoNisu_i.c y CryptoNisu_p.c, que son necesarios para
	generar el archivo DLL del proxy o c�digo auxiliar.

CryptoNisups.def
    Archivo de definici�n de m�dulo que suministra el vinculador con informaci�n acerca de las exportaciones
    que el proxy o c�digo auxiliar necesita.
/////////////////////////////////////////////////////////////////////////////
