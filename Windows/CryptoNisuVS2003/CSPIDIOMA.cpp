#include <stdafx.h>

#include "CSPIDIOMA.h"

#include <stdio.h>

static char *idioma = NULL;
static char *etiquetas[NUM_ETIQUETAS];


/*! TRUE Ok
 *  FALSE error
 */

BOOL CSP_IDIOMA_Cargar (void)
{

	HKEY hKey;
	DWORD tamValue;

	if ( idioma != NULL )
		return FALSE;

	if ( RegOpenKeyEx(HKEY_LOCAL_MACHINE, 
			     "SOFTWARE\\Universitat Jaume I\\Projecte Clauer", 
				 0,
				 KEY_QUERY_VALUE,
				 &hKey) != ERROR_SUCCESS ) 
	{
		return FALSE;
	}

	if ( RegQueryValueEx(hKey,"Idioma", NULL, NULL, NULL, &tamValue) != ERROR_SUCCESS ) {
		RegCloseKey(hKey);
		return FALSE;
	}

	if ( tamValue > 5 ) {
		RegCloseKey(hKey);
		return FALSE;
	}

	idioma = new char [tamValue];

	if ( !idioma ) {
		RegCloseKey(hKey);
		return FALSE;
	}

	if ( RegQueryValueEx(hKey,"Idioma", NULL, NULL, (BYTE *)idioma, &tamValue) != ERROR_SUCCESS ) {
		delete [] idioma;
		idioma = NULL;
		RegCloseKey(hKey);
		return FALSE;
	}

	RegCloseKey(hKey);

	/* Cargamos la tabla de etiquetas
	 */

	if ( strcmp(idioma, "1034") == 0 ) 
	{
		/* Espa�ol 
		 */

		etiquetas[CSP_IDIOMA_DLGPASSWORD_DESCRIPCION]  = "Introduzca el PIN que protege su Clauer";
		etiquetas[CSP_IDIOMA_DLGPASSWORD_CAPTION]      = "PIN del Clauer";

		etiquetas[CSP_IDIOMA_DLGPASSWORD_KEY_DESCRIPCION] = "Introduzca la contrase�a que protege el certificado";
		etiquetas[CSP_IDIOMA_DLGPASSWORD_KEY_CAPTION]     = "Contrase�a del certificado";
		etiquetas[CSP_IDIOMA_DLGPASSWORD_KEY]             = "Password:";

		etiquetas[CSP_IDIOMA_ACEPTAR]  = "Aceptar";
		etiquetas[CSP_IDIOMA_CANCELAR] = "Cancelar";
		etiquetas[CSP_IDIOMA_DLGPASSWORD_PIN]          = "PIN:";
		etiquetas[CSP_IDIOMA_NOCLAUER]                 = "No hay Clauer. Introduzca el Clauer a utilizar";
		etiquetas[CSP_IDIOMA_PROJECTE_CLAUER]		   = "UJI :: Proyecto Clauer";
		etiquetas[CSP_IDIOMA_MASDEUNCLAUER]            = "M�s de un dispositivo Clauer insertado. Por favor, deje insertado �nicamente el que desee utilizar";
		etiquetas[CSP_IDIOMA_CONTINUA_GVA]             = "Parece que utiliza un PIN d�bil.\r\nLe recomendamos que lo cambie lo antes posible";

		etiquetas[CSP_IDIOMA_DLGPASSWORD_DESC_INCORRECTA] = "PIN insertado incorrecto. Tiene tres intentos. Introduzca el PIN que protege su Clauer";
		etiquetas[CSP_IDIOMA_DLGPASSWORD_KEY_DESC_INCORRECTA] = "Contrase�a incorrecta. Tiene tres intentos. Introduzca la contrase�a que protege el certificado";

		etiquetas[CSP_IDIOMA_NUEVA_PASSWORD_DESCRIPCION]  = "Introduzca la contrase�a que proteger� su llave privada";
		etiquetas[CSP_IDIOMA_NUEVA_PASSWORD_CONTRASENYA]  = "Contrase�a";
		etiquetas[CSP_IDIOMA_NUEVA_PASSWORD_CONFIRMACION] = "Confirmaci�n";

		etiquetas[CSP_IDIOMA_NUEVA_PASSWORD_CONCORDAR] = "Contrase�a y confirmaci�n deben coincidir. Int�ntelo de nuevo";
		etiquetas[CSP_IDIOMA_NUEVA_PASSWORD_CAPTION] = "Contrase�a para la protecci�n de llaves";


	} else if ( strcmp(idioma, "1033") == 0 ) 
	{
		/* Ingl�s 
		 */

		etiquetas[CSP_IDIOMA_DLGPASSWORD_DESCRIPCION]  = "Type in the Clauer's PIN";
		etiquetas[CSP_IDIOMA_DLGPASSWORD_CAPTION]      = "Clauer's PIN";

		etiquetas[CSP_IDIOMA_DLGPASSWORD_KEY_DESCRIPCION] = "Type in the certificate's password";
		etiquetas[CSP_IDIOMA_DLGPASSWORD_KEY_CAPTION]     = "Certificate's Password";
		etiquetas[CSP_IDIOMA_DLGPASSWORD_KEY]             = "Password:";

		etiquetas[CSP_IDIOMA_ACEPTAR]  = "Ok";
		etiquetas[CSP_IDIOMA_CANCELAR] = "Cancel";
		etiquetas[CSP_IDIOMA_DLGPASSWORD_PIN]          = "PIN:";
		etiquetas[CSP_IDIOMA_NOCLAUER]                 = "There's no Clauer. Insert the Clauer to use";
		etiquetas[CSP_IDIOMA_PROJECTE_CLAUER]		   = "UJI :: Clauer Project";
		etiquetas[CSP_IDIOMA_MASDEUNCLAUER]            = "There is more than one Clauer device plugged. Please, keep the one you want to use";
		etiquetas[CSP_IDIOMA_CONTINUA_GVA]             = "It seems that you typed a weak PIN.\r\nWe recommend you to change it as soon as possible.";

		etiquetas[CSP_IDIOMA_DLGPASSWORD_DESC_INCORRECTA] = "Invalid PIN. You have three attempts. Type in the Clauer's PIN";
		etiquetas[CSP_IDIOMA_DLGPASSWORD_KEY_DESC_INCORRECTA] = "Invalid password. You have three attempts. Type in the certificate's password";

		etiquetas[CSP_IDIOMA_NUEVA_PASSWORD_DESCRIPCION]  = "Type in the key's password";
		etiquetas[CSP_IDIOMA_NUEVA_PASSWORD_CONTRASENYA]  = "Password";
		etiquetas[CSP_IDIOMA_NUEVA_PASSWORD_CONFIRMACION] = "Confirmation";
		etiquetas[CSP_IDIOMA_NUEVA_PASSWORD_CONCORDAR] = "Password and confirmation don't match. Try it again";
		etiquetas[CSP_IDIOMA_NUEVA_PASSWORD_CAPTION] = "Key protection password";

	} else if ( strcmp(idioma, "1027") == 0 )
	{
		/* Valenci�
		 */

		etiquetas[CSP_IDIOMA_DLGPASSWORD_DESCRIPCION]  = "Introdueix el PIN que protegeix el Clauer";
		etiquetas[CSP_IDIOMA_DLGPASSWORD_CAPTION]      = "PIN del Clauer";

		etiquetas[CSP_IDIOMA_DLGPASSWORD_KEY_DESCRIPCION] = "Introdueix la contrasenya que protegeix el certificat";
		etiquetas[CSP_IDIOMA_DLGPASSWORD_KEY_CAPTION] = "Contrasenya del certificat";
		etiquetas[CSP_IDIOMA_DLGPASSWORD_KEY] = "Contrasenya:";

		etiquetas[CSP_IDIOMA_ACEPTAR]  = "Acceptar";
		etiquetas[CSP_IDIOMA_CANCELAR] = "Cancelar";
		etiquetas[CSP_IDIOMA_DLGPASSWORD_PIN]          = "PIN:";		
		etiquetas[CSP_IDIOMA_NOCLAUER]                 = "No hi ha Clauers al sistema. Introdueix el Clauer a utilitzar";
		etiquetas[CSP_IDIOMA_PROJECTE_CLAUER]		   = "UJI :: Projecte Clauer";
		etiquetas[CSP_IDIOMA_MASDEUNCLAUER]            = "M�s d'un Clauer insertat al sistema. Per favor, deixe nom�s el que vulga utilitzar";
		etiquetas[CSP_IDIOMA_CONTINUA_GVA]             = "Pareix que has introdu�t un PIN feble.\r\nEt recomanem que el canvies el m�s aviat possible.";

		etiquetas[CSP_IDIOMA_DLGPASSWORD_DESC_INCORRECTA] = "PIN incorrecte. Tens tres intents. Introdueix la contrasenya que protegeix el seu Clauer";
		etiquetas[CSP_IDIOMA_DLGPASSWORD_KEY_DESC_INCORRECTA] = "Contrasenya incorrecta. Tens tres intents. Introdueix la contrasenya que protegeix el certificat";

		etiquetas[CSP_IDIOMA_NUEVA_PASSWORD_DESCRIPCION]  = "Introdueix la contrasenya de la clau privada";
		etiquetas[CSP_IDIOMA_NUEVA_PASSWORD_CONTRASENYA]  = "Contrasenya";
		etiquetas[CSP_IDIOMA_NUEVA_PASSWORD_CONFIRMACION] = "Confirmaci�";
		etiquetas[CSP_IDIOMA_NUEVA_PASSWORD_CONCORDAR]    = "La contrasenya y la confirmaci� no concorden. Intenta-ho de nou";
		etiquetas[CSP_IDIOMA_NUEVA_PASSWORD_CAPTION] = "Contrasenya per a la protecci� de claus";

	} else if ( strcmp(idioma, "1036") == 0 )
	{
		/* Franc�s
		 */

		etiquetas[CSP_IDIOMA_DLGPASSWORD_DESCRIPCION]  = "Introduisez le PIN du Clauer";
		etiquetas[CSP_IDIOMA_DLGPASSWORD_CAPTION]      = "PIN du Clauer";

		etiquetas[CSP_IDIOMA_DLGPASSWORD_KEY_DESCRIPCION] = "Introduzca la contrase�a que protege el certificado";
		etiquetas[CSP_IDIOMA_DLGPASSWORD_KEY_CAPTION]     = "Contrase�a del certificado";
		etiquetas[CSP_IDIOMA_DLGPASSWORD_KEY]             = "Password:";

		etiquetas[CSP_IDIOMA_ACEPTAR]  = "Accepter";
		etiquetas[CSP_IDIOMA_CANCELAR] = "Annuler";
		etiquetas[CSP_IDIOMA_DLGPASSWORD_PIN]          = "PIN:";		
		etiquetas[CSP_IDIOMA_NOCLAUER]                 = "Il n'y a pas de Clauers dans le syst�me. Introduisez le Clauer � utiliser";
		etiquetas[CSP_IDIOMA_PROJECTE_CLAUER]		   = "UJI :: Projecte Clauer"; 
		etiquetas[CSP_IDIOMA_MASDEUNCLAUER]            = "Plus d'une Clauer ins�r� au syst�me. Laissez seulement ce que vous voulez utiliser.";
		etiquetas[CSP_IDIOMA_CONTINUA_GVA]             = "Il semble que vous utilisez un PIN faible.\r\nNous vous recommandons de le changer le plus rapidement possible.";

		etiquetas[CSP_IDIOMA_DLGPASSWORD_DESC_INCORRECTA]     = "Invalid PIN. You have three attempts. Type in the Clauer's PIN";
		etiquetas[CSP_IDIOMA_DLGPASSWORD_KEY_DESC_INCORRECTA] = "Invalid password. You have three attempts. Type in the certificate's password";

		etiquetas[CSP_IDIOMA_NUEVA_PASSWORD_DESCRIPCION]  = "Type in the key's password";
		etiquetas[CSP_IDIOMA_NUEVA_PASSWORD_CONTRASENYA]  = "Password";
		etiquetas[CSP_IDIOMA_NUEVA_PASSWORD_CONFIRMACION] = "Confirmation";

		etiquetas[CSP_IDIOMA_NUEVA_PASSWORD_CONCORDAR] = "Password and confirmation don't match. Try it again";
		etiquetas[CSP_IDIOMA_NUEVA_PASSWORD_CAPTION] = "Key protection password";


	} else
	{
		/* Idioma no reconocido 
		 */
	
		delete [] idioma;
		idioma = NULL;

		return FALSE;

	}



	return TRUE;

}


BOOL CSP_IDIOMA_Descargar (void)
{
	if ( idioma ) {
		delete [] idioma;
		idioma = NULL;
	}

	return TRUE;
}




LPSTR CSP_IDIOMA_Get (DWORD etiqueta)
{
	if ( !idioma )
		return NULL;

	if ( etiqueta > (NUM_ETIQUETAS - 1) )
		return NULL;

	return etiquetas[etiqueta];
}



BOOL CSP_IDIOMA_Cargado (void)
{

	return idioma != NULL;


}

