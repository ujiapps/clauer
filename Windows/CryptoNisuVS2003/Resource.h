//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by CryptoNisu.rc
//
#define IDS_PROJNAME                    100
#define IDR_CRYPTONISU                  101
#define IDB_CLAUER                      102
#define IDB_HUELLA                      103

#define IDC_EDITPASSWORD                1003
#define IDC_EDITPIN                     1003
#define IDC_BUTTON1                     1004
#define IDACERCADE                      1004
#define IDR_DEFAULT1                    1005
#define IDD_DIALOG1                     1006
#define IDD_PASSWORD                    1006
#define IDC_DESCRIPCION                 1006
#define IDI_UJI                         1007
#define IDC_PIN                         1007
#define IDD_NUEVA_PASSWORD              1009
#define IDC_CONTRASENYA                 1010
#define IDC_CONFIRMACION                1011
#define IDC_NUEVA_CONTRASENYA           1012
#define IDC_NUEVA_CONFIRMACION          1013
#define IDC_NUEVA_DESCRIPCION           1014

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        201
#define _APS_NEXT_COMMAND_VALUE         32768
#define _APS_NEXT_CONTROL_VALUE         201
#define _APS_NEXT_SYMED_VALUE           104
#endif
#endif
