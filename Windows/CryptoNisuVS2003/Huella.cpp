// Huella.cpp: implementación de CHuella
#include "stdafx.h"
#include "Huella.h"
#include ".\huella.h"


// CHuella

bool permite = false;

STDMETHODIMP CHuella::loadFile(BSTR filePath, LONG* result)
{
	USES_CONVERSION;
    HANDLE hFile = NULL;
	BOOL bResult = FALSE;
    HCRYPTPROV hProv = 0;
    HCRYPTHASH hHash = 0;
	DWORD leidos = 0;
	byte buffer[2048];

	*result=0;

    if (!permite) {
		permite= (MessageBox(
			"Vamos a acceder a los ficheros de su ordenador\n"
			"para calcular su Huella Digital", "Aviso", MB_YESNO) == IDYES);
	    if (!permite)
			return S_FALSE;
	}
	
	hFile = CreateFile(W2A(filePath),
			GENERIC_READ,
			FILE_SHARE_READ,
			NULL,
			OPEN_EXISTING,
			FILE_FLAG_SEQUENTIAL_SCAN,
			NULL);
	if ((NULL == hFile) || (INVALID_HANDLE_VALUE == hFile))
		return S_FALSE;
	elTama = GetFileSize (hFile, NULL);

	if (!CryptAcquireContext(&hProv,
			NULL,
			NULL,
			PROV_RSA_FULL,
			CRYPT_VERIFYCONTEXT)) {
		CloseHandle(hFile);
		return S_FALSE;
	}

    if (!CryptCreateHash(hProv, CALG_SHA1, 0, 0, &hHash)) {
		CloseHandle(hFile);
		CryptReleaseContext(hProv, 0);
		return S_FALSE;
	}

    while (bResult = ReadFile(hFile, buffer, sizeof(buffer), &leidos, NULL)) {
        if (0 == leidos) break;        
        if (!CryptHashData(hHash, buffer, leidos, 0)) {
			CloseHandle(hFile);
			CryptReleaseContext(hProv, 0);
			CryptDestroyHash(hHash);
			return S_FALSE;
		}
	}

    if (!bResult) {
        CloseHandle(hFile);
        CryptReleaseContext(hProv, 0);
        CryptDestroyHash(hHash);
		return S_FALSE;
	}

    cbHash = 20;
	CloseHandle(hFile);
    CryptGetHashParam(hHash, HP_HASHVAL, elHash, &cbHash, 0);    
    CryptReleaseContext(hProv, 0);
    CryptDestroyHash(hHash);

	*result=1;
	return S_OK;
}

STDMETHODIMP CHuella::get_size(DOUBLE* pVal)
{
	*pVal= elTama;
	if (elTama == -1)
		return S_FALSE;
	return S_OK;
}

STDMETHODIMP CHuella::get_hexValue(BSTR* pVal)
{
	USES_CONVERSION;
	if (cbHash == 0) {
		*pVal= SysAllocString(L"");
		return S_FALSE;
	}
	char laHuella[sizeof(elHash)*2+1];
	char digitos[] = "0123456789ABCDEF";

    for (byte i = 0; i < cbHash; i++) {
       laHuella[2*i]=  digitos[elHash[i] >> 4];
	   laHuella[2*i+1]=digitos[elHash[i] & 0xf];
	   }
	laHuella[2*cbHash]= 0;
	*pVal= SysAllocString(A2W(laHuella));
	return S_OK;
}

