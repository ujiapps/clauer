// Clauer.h: declaraci�n de CClauer
#pragma once
#include "resource.h"       // S�mbolos principales
#include <atlctl.h>
#include "LIBRT/libRT.h"
#include "CSPIDIOMA.h"


// IClauer
[
	object,
	uuid(C6F30741-9C2D-4B00-ABC3-7400ADC62F0C),
	dual,
	helpstring("Interfaz IClauer"),
	pointer_default(unique)
]
__interface IClauer : public IDispatch
{
	[id(1), helpstring("method getClauerSoftVersion")] HRESULT getClauerSoftVersion([out,retval] BSTR* version);
	[id(2), helpstring("method getClauersList")] HRESULT getClauersList([out,retval] VARIANT* lista);
	[id(3), helpstring("method setClauerActive")] HRESULT setClauerActive([in] BSTR deviceName, [in] LONG uniq, [out,retval] LONG* pVal);
	[id(4), helpstring("method getClauerId")] HRESULT getClauerId([out,retval] BSTR* pVal);
	[id(5), helpstring("method getClauerOwner")] HRESULT getClauerOwner([out,retval] BSTR* pVal);
	[id(6), helpstring("method getClauerToken")] HRESULT getClauerToken([in] BSTR name, [in] BSTR pwd, [in] BSTR chal, [out,retval] BSTR* pVal);
	[id(7), helpstring("method setClauerCryptoMode")] HRESULT setClauerCryptoMode([in] BSTR pwd, [out,retval] LONG* pVal);
	[id(8), helpstring("method setClauerClearMode")] HRESULT setClauerClearMode([out,retval] LONG* pVal);
	[id(9), helpstring("method setClauerPwd")] HRESULT setClauerPwd([in] BSTR pwd, [out,retval] LONG* pVal);
};


// CClauer
[
	coclass,
	threading("apartment"),
	vi_progid("CryptoNisu.Clauer"),
	progid("CryptoNisu.Clauer.1"),
	version(1.0),
	uuid("EF9DBF85-F6AE-44A9-A0F2-344212E39382"),
	helpstring("Clauer Class"),
	support_error_info(IClauer),
	registration_script("control.rgs")
]
class ATL_NO_VTABLE CClauer : 
	public IClauer,
	public IPersistStreamInitImpl<CClauer>,
	public IOleControlImpl<CClauer>,
	public IOleObjectImpl<CClauer>,
	public IOleInPlaceActiveObjectImpl<CClauer>,
	public IViewObjectExImpl<CClauer>,
	public IOleInPlaceObjectWindowlessImpl<CClauer>,
	public IPersistStorageImpl<CClauer>,
	public ISpecifyPropertyPagesImpl<CClauer>,
	public IQuickActivateImpl<CClauer>,
	public IDataObjectImpl<CClauer>,
	public IProvideClassInfo2Impl<&__uuidof(CClauer), NULL>,
	public IObjectSafetyImpl<CClauer, INTERFACESAFE_FOR_UNTRUSTED_CALLER | INTERFACESAFE_FOR_UNTRUSTED_DATA>,
	public CComControl<CClauer>,
	public ISupportErrorInfo
{
public:

	CClauer()
	{
		m_deviceName = NULL;
		m_pwdClauer = NULL;
		validCred=1;
		validIdent=1;
		m_uniq = 0;
		LIBRT_Ini();
		CSP_IDIOMA_Cargar();
	}

	~CClauer () {
		if (m_pwdClauer) {
			SecureZeroMemory(m_pwdClauer,strlen(m_pwdClauer));
			delete [] m_pwdClauer;
			m_pwdClauer = NULL;
		}
	}

DECLARE_OLEMISC_STATUS(OLEMISC_RECOMPOSEONRESIZE | 
	OLEMISC_CANTLINKINSIDE | 
	OLEMISC_INSIDEOUT | 
	OLEMISC_ACTIVATEWHENVISIBLE | 
	OLEMISC_SETCLIENTSITEFIRST
)


BEGIN_PROP_MAP(CClauer)
	PROP_DATA_ENTRY("_cx", m_sizeExtent.cx, VT_UI4)
	PROP_DATA_ENTRY("_cy", m_sizeExtent.cy, VT_UI4)
	// Entradas de ejemplo
	// PROP_ENTRY("Descripci�n de la propiedad", dispid, clsid)
	// PROP_PAGE(CLSID_StockColorPage)
END_PROP_MAP()


BEGIN_MSG_MAP(CClauer)
	CHAIN_MSG_MAP(CComControl<CClauer>)
	DEFAULT_REFLECTION_HANDLER()
END_MSG_MAP()
// Prototipos de controlador:
//  LRESULT Controlador de mensajes(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
//  LRESULT Controlador de comandos(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled);
//  LRESULT Controlador de notificaciones(int idCtrl, LPNMHDR pnmh, BOOL& bHandled);

// IViewObjectEx
	DECLARE_VIEW_STATUS(VIEWSTATUS_SOLIDBKGND | VIEWSTATUS_OPAQUE)

// IClauer
private:
	char *m_deviceName;
	char *m_pwdClauer;
	long m_uniq;
	int validCred;
	int validIdent;
	HWND hWindow;
	
public:
	HRESULT OnDraw(ATL_DRAWINFO& di)
	{
		RECT& rc = *(RECT*)di.prcBounds;
		// Establecer la regi�n Clip en el rect�ngulo especificado por di.prcBounds
		HRGN hRgnOld = NULL;
		if (GetClipRgn(di.hdcDraw, hRgnOld) != 1)
			hRgnOld = NULL;
		bool bSelectOldRgn = false;

		HRGN hRgnNew = CreateRectRgn(rc.left, rc.top, rc.right, rc.bottom);

		if (hRgnNew != NULL)
		{
			bSelectOldRgn = (SelectClipRgn(di.hdcDraw, hRgnNew) != ERROR);
		}

		Rectangle(di.hdcDraw, rc.left, rc.top, rc.right, rc.bottom);
		SetTextAlign(di.hdcDraw, TA_CENTER|TA_BASELINE);
		LPCTSTR pszText = _T("ATL 7.0 : Clauer");
		TextOut(di.hdcDraw, 
			(rc.left + rc.right) / 2, 
			(rc.top + rc.bottom) / 2, 
			pszText, 
			lstrlen(pszText));

		if (bSelectOldRgn)
			SelectClipRgn(di.hdcDraw, hRgnOld);

		return S_OK;
	}


	DECLARE_PROTECT_FINAL_CONSTRUCT()

	HRESULT FinalConstruct()
	{
		return S_OK;
	}
	
	void FinalRelease() 
	{
	}
	STDMETHOD(getClauerSoftVersion)(BSTR* version);
	STDMETHOD(getClauersList)(VARIANT* lista);
	STDMETHOD(setClauerActive)(BSTR deviceName, LONG uniq, LONG* pVal);
	STDMETHOD(getClauerId)(BSTR* pVal);
	STDMETHOD(getClauerOwner)(BSTR* pVal);
	STDMETHOD(getClauerToken)(BSTR name, BSTR pwd, BSTR chal, BSTR* pVal);
	STDMETHOD(setClauerCryptoMode)(BSTR pwd, LONG* pVal);
	STDMETHOD(setClauerClearMode)(LONG* pVal);
	STDMETHOD(setClauerPwd)(BSTR pwd, LONG *pVal);
};

