// Huella.h: declaraci�n de CHuella
#pragma once
#include "resource.h"       // S�mbolos principales
#include <atlctl.h>

// IHuella
[
	object,
	uuid(548611C0-F1CF-4987-9F58-CF919F643401),
	dual,
	helpstring("Interfaz IHuella"),
	pointer_default(unique)
]
__interface IHuella : public IDispatch
{
	[id(3), helpstring("method loadFile")] HRESULT loadFile([in] BSTR filePath, [out, retval] LONG* result);
	[propget, id(4), helpstring("property Size")] HRESULT size([out, retval] DOUBLE* pVal);
	[propget, id(5), helpstring("property hexValue")] HRESULT hexValue([out, retval] BSTR* pVal);
};


// CHuella
[
	coclass,
	threading("apartment"),
	vi_progid("CryptoNisu.Huella"),
	progid("CryptoNisu.Huella.1"),
	version(1.0),
	uuid("80454CE9-5F0F-418B-AC53-C923661EC759"),
	helpstring("Huella Class"),
	support_error_info(IHuella),
	registration_script("control.rgs")
]
class ATL_NO_VTABLE CHuella : 
	public IHuella,
	public IPersistStreamInitImpl<CHuella>,
	public IOleControlImpl<CHuella>,
	public IOleObjectImpl<CHuella>,
	public IOleInPlaceActiveObjectImpl<CHuella>,
	public IViewObjectExImpl<CHuella>,
	public IOleInPlaceObjectWindowlessImpl<CHuella>,
	public IPersistStorageImpl<CHuella>,
	public ISpecifyPropertyPagesImpl<CHuella>,
	public IQuickActivateImpl<CHuella>,
	public IDataObjectImpl<CHuella>,
	public IProvideClassInfo2Impl<&__uuidof(CHuella), NULL>,
	public CComControl<CHuella>
{
private:
	BYTE elHash[20];
	DWORD cbHash, elTama;

public:
	CHuella()
	{ cbHash = 0;
	  elTama = -1;
	}

DECLARE_OLEMISC_STATUS(OLEMISC_RECOMPOSEONRESIZE | 
	OLEMISC_CANTLINKINSIDE | 
	OLEMISC_INSIDEOUT | 
	OLEMISC_ACTIVATEWHENVISIBLE | 
	OLEMISC_SETCLIENTSITEFIRST
)


BEGIN_PROP_MAP(CHuella)
	PROP_DATA_ENTRY("_cx", m_sizeExtent.cx, VT_UI4)
	PROP_DATA_ENTRY("_cy", m_sizeExtent.cy, VT_UI4)
	// Entradas de ejemplo
	// PROP_ENTRY("Descripci�n de la propiedad", dispid, clsid)
	// PROP_PAGE(CLSID_StockColorPage)
END_PROP_MAP()


BEGIN_MSG_MAP(CHuella)
	CHAIN_MSG_MAP(CComControl<CHuella>)
	DEFAULT_REFLECTION_HANDLER()
END_MSG_MAP()
// Prototipos de controlador:
//  LRESULT Controlador de mensajes(UINT uMsg, WPARAM wParam, LPARAM lParam, BOOL& bHandled);
//  LRESULT Controlador de comandos(WORD wNotifyCode, WORD wID, HWND hWndCtl, BOOL& bHandled);
//  LRESULT Controlador de notificaciones(int idCtrl, LPNMHDR pnmh, BOOL& bHandled);

// IViewObjectEx
	DECLARE_VIEW_STATUS(VIEWSTATUS_SOLIDBKGND | VIEWSTATUS_OPAQUE)

// IHuella
public:
		HRESULT OnDraw(ATL_DRAWINFO& di)
		{
		RECT& rc = *(RECT*)di.prcBounds;
		// Establecer la regi�n Clip en el rect�ngulo especificado por di.prcBounds
		HRGN hRgnOld = NULL;
		if (GetClipRgn(di.hdcDraw, hRgnOld) != 1)
			hRgnOld = NULL;
		bool bSelectOldRgn = false;

		HRGN hRgnNew = CreateRectRgn(rc.left, rc.top, rc.right, rc.bottom);

		if (hRgnNew != NULL)
		{
			bSelectOldRgn = (SelectClipRgn(di.hdcDraw, hRgnNew) != ERROR);
		}

		Rectangle(di.hdcDraw, rc.left, rc.top, rc.right, rc.bottom);
		SetTextAlign(di.hdcDraw, TA_CENTER|TA_BASELINE);
		LPCTSTR pszText = _T("ATL 7.0 : Huella");
		TextOut(di.hdcDraw, 
			(rc.left + rc.right) / 2, 
			(rc.top + rc.bottom) / 2, 
			pszText, 
			lstrlen(pszText));

		if (bSelectOldRgn)
			SelectClipRgn(di.hdcDraw, hRgnOld);

		return S_OK;
	}


	DECLARE_PROTECT_FINAL_CONSTRUCT()

	HRESULT FinalConstruct()
	{
		return S_OK;
	}
	
	void FinalRelease() 
	{
	}
	STDMETHOD(loadFile)(BSTR filePath, LONG* result);
	STDMETHOD(get_size)(DOUBLE* pVal);
	STDMETHOD(get_hexValue)(BSTR* pVal);
};

