

/* this ALWAYS GENERATED file contains the definitions for the interfaces */


 /* File created by MIDL compiler version 6.00.0361 */
/* at Fri Sep 29 12:19:36 2006
 */
/* Compiler settings for _CryptoNisu.idl:
    Oicf, W1, Zp8, env=Win32 (32b run)
    protocol : dce , ms_ext, c_ext, robust
    error checks: allocation ref bounds_check enum stub_data 
    VC __declspec() decoration level: 
         __declspec(uuid()), __declspec(selectany), __declspec(novtable)
         DECLSPEC_UUID(), MIDL_INTERFACE()
*/
//@@MIDL_FILE_HEADING(  )

#pragma warning( disable: 4049 )  /* more than 64k source lines */


/* verify that the <rpcndr.h> version is high enough to compile this file*/
#ifndef __REQUIRED_RPCNDR_H_VERSION__
#define __REQUIRED_RPCNDR_H_VERSION__ 475
#endif

#include "rpc.h"
#include "rpcndr.h"

#ifndef __RPCNDR_H_VERSION__
#error this stub requires an updated version of <rpcndr.h>
#endif // __RPCNDR_H_VERSION__

#ifndef COM_NO_WINDOWS_H
#include "windows.h"
#include "ole2.h"
#endif /*COM_NO_WINDOWS_H*/

#ifndef ___CryptoNisu_h__
#define ___CryptoNisu_h__

#if defined(_MSC_VER) && (_MSC_VER >= 1020)
#pragma once
#endif

/* Forward Declarations */ 

#ifndef __IClauer_FWD_DEFINED__
#define __IClauer_FWD_DEFINED__
typedef interface IClauer IClauer;
#endif 	/* __IClauer_FWD_DEFINED__ */


#ifndef __IHuella_FWD_DEFINED__
#define __IHuella_FWD_DEFINED__
typedef interface IHuella IHuella;
#endif 	/* __IHuella_FWD_DEFINED__ */


#ifndef __CClauer_FWD_DEFINED__
#define __CClauer_FWD_DEFINED__

#ifdef __cplusplus
typedef class CClauer CClauer;
#else
typedef struct CClauer CClauer;
#endif /* __cplusplus */

#endif 	/* __CClauer_FWD_DEFINED__ */


#ifndef __CHuella_FWD_DEFINED__
#define __CHuella_FWD_DEFINED__

#ifdef __cplusplus
typedef class CHuella CHuella;
#else
typedef struct CHuella CHuella;
#endif /* __cplusplus */

#endif 	/* __CHuella_FWD_DEFINED__ */


/* header files for imported files */
#include "prsht.h"
#include "mshtml.h"
#include "mshtmhst.h"
#include "exdisp.h"
#include "objsafe.h"

#ifdef __cplusplus
extern "C"{
#endif 

void * __RPC_USER MIDL_user_allocate(size_t);
void __RPC_USER MIDL_user_free( void * ); 

#ifndef __IClauer_INTERFACE_DEFINED__
#define __IClauer_INTERFACE_DEFINED__

/* interface IClauer */
/* [unique][helpstring][dual][uuid][object] */ 


EXTERN_C const IID IID_IClauer;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("C6F30741-9C2D-4B00-ABC3-7400ADC62F0C")
    IClauer : public IDispatch
    {
    public:
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE getClauerSoftVersion( 
            /* [retval][out] */ BSTR *version) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE getClauersList( 
            /* [retval][out] */ VARIANT *lista) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE setClauerActive( 
            /* [in] */ BSTR deviceName,
            /* [in] */ LONG uniq,
            /* [retval][out] */ LONG *pVal) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE getClauerId( 
            /* [retval][out] */ BSTR *pVal) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE getClauerOwner( 
            /* [retval][out] */ BSTR *pVal) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE getClauerToken( 
            /* [in] */ BSTR name,
            /* [in] */ BSTR pwd,
            /* [in] */ BSTR chal,
            /* [retval][out] */ BSTR *pVal) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE setClauerCryptoMode( 
            /* [in] */ BSTR pwd,
            /* [retval][out] */ LONG *pVal) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE setClauerClearMode( 
            /* [retval][out] */ LONG *pVal) = 0;
        
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE setClauerPwd( 
            /* [in] */ BSTR pwd,
            /* [retval][out] */ LONG *pVal) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IClauerVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IClauer * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IClauer * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IClauer * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            IClauer * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            IClauer * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            IClauer * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            IClauer * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *getClauerSoftVersion )( 
            IClauer * This,
            /* [retval][out] */ BSTR *version);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *getClauersList )( 
            IClauer * This,
            /* [retval][out] */ VARIANT *lista);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *setClauerActive )( 
            IClauer * This,
            /* [in] */ BSTR deviceName,
            /* [in] */ LONG uniq,
            /* [retval][out] */ LONG *pVal);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *getClauerId )( 
            IClauer * This,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *getClauerOwner )( 
            IClauer * This,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *getClauerToken )( 
            IClauer * This,
            /* [in] */ BSTR name,
            /* [in] */ BSTR pwd,
            /* [in] */ BSTR chal,
            /* [retval][out] */ BSTR *pVal);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *setClauerCryptoMode )( 
            IClauer * This,
            /* [in] */ BSTR pwd,
            /* [retval][out] */ LONG *pVal);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *setClauerClearMode )( 
            IClauer * This,
            /* [retval][out] */ LONG *pVal);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *setClauerPwd )( 
            IClauer * This,
            /* [in] */ BSTR pwd,
            /* [retval][out] */ LONG *pVal);
        
        END_INTERFACE
    } IClauerVtbl;

    interface IClauer
    {
        CONST_VTBL struct IClauerVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IClauer_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define IClauer_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define IClauer_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define IClauer_GetTypeInfoCount(This,pctinfo)	\
    (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo)

#define IClauer_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo)

#define IClauer_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)

#define IClauer_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)


#define IClauer_getClauerSoftVersion(This,version)	\
    (This)->lpVtbl -> getClauerSoftVersion(This,version)

#define IClauer_getClauersList(This,lista)	\
    (This)->lpVtbl -> getClauersList(This,lista)

#define IClauer_setClauerActive(This,deviceName,uniq,pVal)	\
    (This)->lpVtbl -> setClauerActive(This,deviceName,uniq,pVal)

#define IClauer_getClauerId(This,pVal)	\
    (This)->lpVtbl -> getClauerId(This,pVal)

#define IClauer_getClauerOwner(This,pVal)	\
    (This)->lpVtbl -> getClauerOwner(This,pVal)

#define IClauer_getClauerToken(This,name,pwd,chal,pVal)	\
    (This)->lpVtbl -> getClauerToken(This,name,pwd,chal,pVal)

#define IClauer_setClauerCryptoMode(This,pwd,pVal)	\
    (This)->lpVtbl -> setClauerCryptoMode(This,pwd,pVal)

#define IClauer_setClauerClearMode(This,pVal)	\
    (This)->lpVtbl -> setClauerClearMode(This,pVal)

#define IClauer_setClauerPwd(This,pwd,pVal)	\
    (This)->lpVtbl -> setClauerPwd(This,pwd,pVal)

#endif /* COBJMACROS */


#endif 	/* C style interface */



/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IClauer_getClauerSoftVersion_Proxy( 
    IClauer * This,
    /* [retval][out] */ BSTR *version);


void __RPC_STUB IClauer_getClauerSoftVersion_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IClauer_getClauersList_Proxy( 
    IClauer * This,
    /* [retval][out] */ VARIANT *lista);


void __RPC_STUB IClauer_getClauersList_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IClauer_setClauerActive_Proxy( 
    IClauer * This,
    /* [in] */ BSTR deviceName,
    /* [in] */ LONG uniq,
    /* [retval][out] */ LONG *pVal);


void __RPC_STUB IClauer_setClauerActive_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IClauer_getClauerId_Proxy( 
    IClauer * This,
    /* [retval][out] */ BSTR *pVal);


void __RPC_STUB IClauer_getClauerId_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IClauer_getClauerOwner_Proxy( 
    IClauer * This,
    /* [retval][out] */ BSTR *pVal);


void __RPC_STUB IClauer_getClauerOwner_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IClauer_getClauerToken_Proxy( 
    IClauer * This,
    /* [in] */ BSTR name,
    /* [in] */ BSTR pwd,
    /* [in] */ BSTR chal,
    /* [retval][out] */ BSTR *pVal);


void __RPC_STUB IClauer_getClauerToken_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IClauer_setClauerCryptoMode_Proxy( 
    IClauer * This,
    /* [in] */ BSTR pwd,
    /* [retval][out] */ LONG *pVal);


void __RPC_STUB IClauer_setClauerCryptoMode_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IClauer_setClauerClearMode_Proxy( 
    IClauer * This,
    /* [retval][out] */ LONG *pVal);


void __RPC_STUB IClauer_setClauerClearMode_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IClauer_setClauerPwd_Proxy( 
    IClauer * This,
    /* [in] */ BSTR pwd,
    /* [retval][out] */ LONG *pVal);


void __RPC_STUB IClauer_setClauerPwd_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);



#endif 	/* __IClauer_INTERFACE_DEFINED__ */


#ifndef __IHuella_INTERFACE_DEFINED__
#define __IHuella_INTERFACE_DEFINED__

/* interface IHuella */
/* [unique][helpstring][dual][uuid][object] */ 


EXTERN_C const IID IID_IHuella;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("548611C0-F1CF-4987-9F58-CF919F643401")
    IHuella : public IDispatch
    {
    public:
        virtual /* [helpstring][id] */ HRESULT STDMETHODCALLTYPE loadFile( 
            /* [in] */ BSTR filePath,
            /* [retval][out] */ LONG *result) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_size( 
            /* [retval][out] */ DOUBLE *pVal) = 0;
        
        virtual /* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE get_hexValue( 
            /* [retval][out] */ BSTR *pVal) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IHuellaVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IHuella * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IHuella * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IHuella * This);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfoCount )( 
            IHuella * This,
            /* [out] */ UINT *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetTypeInfo )( 
            IHuella * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo **ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE *GetIDsOfNames )( 
            IHuella * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR *rgszNames,
            /* [in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE *Invoke )( 
            IHuella * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS *pDispParams,
            /* [out] */ VARIANT *pVarResult,
            /* [out] */ EXCEPINFO *pExcepInfo,
            /* [out] */ UINT *puArgErr);
        
        /* [helpstring][id] */ HRESULT ( STDMETHODCALLTYPE *loadFile )( 
            IHuella * This,
            /* [in] */ BSTR filePath,
            /* [retval][out] */ LONG *result);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_size )( 
            IHuella * This,
            /* [retval][out] */ DOUBLE *pVal);
        
        /* [helpstring][id][propget] */ HRESULT ( STDMETHODCALLTYPE *get_hexValue )( 
            IHuella * This,
            /* [retval][out] */ BSTR *pVal);
        
        END_INTERFACE
    } IHuellaVtbl;

    interface IHuella
    {
        CONST_VTBL struct IHuellaVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IHuella_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define IHuella_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define IHuella_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define IHuella_GetTypeInfoCount(This,pctinfo)	\
    (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo)

#define IHuella_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo)

#define IHuella_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)

#define IHuella_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)


#define IHuella_loadFile(This,filePath,result)	\
    (This)->lpVtbl -> loadFile(This,filePath,result)

#define IHuella_get_size(This,pVal)	\
    (This)->lpVtbl -> get_size(This,pVal)

#define IHuella_get_hexValue(This,pVal)	\
    (This)->lpVtbl -> get_hexValue(This,pVal)

#endif /* COBJMACROS */


#endif 	/* C style interface */



/* [helpstring][id] */ HRESULT STDMETHODCALLTYPE IHuella_loadFile_Proxy( 
    IHuella * This,
    /* [in] */ BSTR filePath,
    /* [retval][out] */ LONG *result);


void __RPC_STUB IHuella_loadFile_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IHuella_get_size_Proxy( 
    IHuella * This,
    /* [retval][out] */ DOUBLE *pVal);


void __RPC_STUB IHuella_get_size_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][id][propget] */ HRESULT STDMETHODCALLTYPE IHuella_get_hexValue_Proxy( 
    IHuella * This,
    /* [retval][out] */ BSTR *pVal);


void __RPC_STUB IHuella_get_hexValue_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);



#endif 	/* __IHuella_INTERFACE_DEFINED__ */



#ifndef __CryptoNisu_LIBRARY_DEFINED__
#define __CryptoNisu_LIBRARY_DEFINED__

/* library CryptoNisu */
/* [helpstring][uuid][version] */ 


EXTERN_C const IID LIBID_CryptoNisu;

EXTERN_C const CLSID CLSID_CClauer;

#ifdef __cplusplus

class DECLSPEC_UUID("EF9DBF85-F6AE-44A9-A0F2-344212E39382")
CClauer;
#endif

EXTERN_C const CLSID CLSID_CHuella;

#ifdef __cplusplus

class DECLSPEC_UUID("80454CE9-5F0F-418B-AC53-C923661EC759")
CHuella;
#endif
#endif /* __CryptoNisu_LIBRARY_DEFINED__ */

/* Additional Prototypes for ALL interfaces */

unsigned long             __RPC_USER  BSTR_UserSize(     unsigned long *, unsigned long            , BSTR * ); 
unsigned char * __RPC_USER  BSTR_UserMarshal(  unsigned long *, unsigned char *, BSTR * ); 
unsigned char * __RPC_USER  BSTR_UserUnmarshal(unsigned long *, unsigned char *, BSTR * ); 
void                      __RPC_USER  BSTR_UserFree(     unsigned long *, BSTR * ); 

unsigned long             __RPC_USER  VARIANT_UserSize(     unsigned long *, unsigned long            , VARIANT * ); 
unsigned char * __RPC_USER  VARIANT_UserMarshal(  unsigned long *, unsigned char *, VARIANT * ); 
unsigned char * __RPC_USER  VARIANT_UserUnmarshal(unsigned long *, unsigned char *, VARIANT * ); 
void                      __RPC_USER  VARIANT_UserFree(     unsigned long *, VARIANT * ); 

/* end of Additional Prototypes */

#ifdef __cplusplus
}
#endif

#endif


