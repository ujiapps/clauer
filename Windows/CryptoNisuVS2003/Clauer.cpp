// Clauer.cpp: implementaci�n de CClauer
#include "stdafx.h"
#include "Clauer.h"
#include ".\clauer.h"
#include "LIBRT/libRT.h"
#include <atlrx.h>
#include <atlsafe.h>

#include "../clui/clui/clui.h"

#include "CSPIdioma.h"

#define VERSION_SIZE	10
#define PRG_REG_KEY	"SOFTWARE\\Universitat Jaume I\\Projecte Clauer"
#define PRG_REG_VERSION	"VERSION"

BOOL CLUPDATE_Get_Local_Version ( char version[VERSION_SIZE+1] )
{
	HKEY hKey;
	DWORD tamVersion, tipo;

	if ( RegOpenKeyExA(HKEY_LOCAL_MACHINE, PRG_REG_KEY, 0, KEY_READ, &hKey) != ERROR_SUCCESS )
		return FALSE;

	tamVersion = VERSION_SIZE + 1;

	if ( RegQueryValueExA(hKey, PRG_REG_VERSION, NULL, &tipo, (BYTE *)version, &tamVersion) != ERROR_SUCCESS ) {
		RegCloseKey(hKey);        
		return FALSE;
	}

	RegCloseKey(hKey);

	if ( (tamVersion != (VERSION_SIZE + 1)) || tipo != REG_SZ )
		return FALSE;


	return TRUE;
}

const char *bin2hex(unsigned char *bin, int l)
{
	unsigned char *hex;
	char digitos[] = "0123456789ABCDEF";

	hex= (unsigned char *) malloc (l*2+1);

	for (byte i = 0; i < l; i++) {
		hex[2*i]=  digitos[bin[i] >> 4];
		hex[2*i+1]=digitos[bin[i] & 0xf];
	}
	hex[2*l]= 0;
	return (char *)hex;
}

void arc4(char *key, int keySize, unsigned char *msg, int msgSize)
{
	int i, j, l, x;
	char S[256], K[256];

	for (i=0; i<256; i++) {
		S[i] = i;
		K[i] = key[i % keySize];
	}
	for (i=0, j=0; i<256; i++) {
		j = (j + S[i] + K[i]) & 255;
		x = S[i]; S[i] = S[j]; S[j] = x;
	}
	for (i=0 , j=0, l=0; l<msgSize; l++) {
		i = (i + 1) & 255;
		j = (j + S[i]) & 255;
		x = S[i]; S[i] = S[j]; S[j] = x;
		msg[l] = msg[l] ^ S[(S[i] + S[j]) & 255];
	}
}


// CClauer

STDMETHODIMP CClauer::getClauerSoftVersion(BSTR* version)
{
	USES_CONVERSION;

	char cversion[VERSION_SIZE+1];

	if ( ! CLUPDATE_Get_Local_Version  ( cversion ) ) {
		*version = SysAllocString(L"");
		if ( ! *version )
			return E_FAIL;
		return S_FALSE;
	}

	*version = SysAllocString(A2W(cversion));
	if ( ! *version )
		return E_FAIL;
	return S_OK;
}


STDMETHODIMP CClauer::getClauersList(VARIANT* lista)
{/*
	CComSafeArray<char> lis;
	DWORD numElements[] = {0};
	char *devices[MAX_DEVICES];
	int nDev, i;

	memset(devices, 0, sizeof(devices));
	if ( ( LIBRT_ListarDispositivos( &nDev, (unsigned char **) devices ) != 0 ) || ( nDev == 0 )) {
		lis.Create(VT_R8, 1, numElements);
		*lista = lis.Detach();
		return S_FALSE;
	}

	numElements[0]= nDev;
	lis.Create(VT_R8,1,numElements);

	for ( i = 0 ; (i < nDev) && devices[i] ; i++ ) {
		lis.PutElement(i, devices[i]);
		free(devices[i]);
	}
	
	*lista = lis.Detach();
	*/
	return S_OK;
}

STDMETHODIMP CClauer::setClauerActive(BSTR deviceName, LONG uniq, LONG* pVal)
{
	USES_CONVERSION;

	USBCERTS_HANDLE hClauer;

	*pVal = 0;
	if ( !deviceName || SysStringLen(deviceName) == 0  ) {
		char *devices[MAX_DEVICES];
		int nDev, i;

		memset(devices, 0, MAX_DEVICES);
		if ( LIBRT_ListarDispositivos( &nDev, (unsigned char **) devices ) != 0 )
			return S_FALSE;

		if ( nDev == 0 )
			return S_OK;

		m_deviceName = (char *) malloc (strlen(devices[0])+1);

		if ( ! m_deviceName ) {
			for ( i = 0 ; (i < nDev) && devices[i] ; i++ )
				free(devices[i]);
			return E_FAIL;
		}

		strcpy(m_deviceName, devices[0]);

		for ( i = 0 ; (i < nDev) && devices[i] ; i++ )
			free(devices[i]);

	} else {

		m_deviceName = (char *) malloc(strlen(W2A(deviceName))+1);
		if ( ! m_deviceName )
			return E_FAIL;

		strcpy(m_deviceName, W2A(deviceName));

	}

	if ( LIBRT_IniciarDispositivo((unsigned char *)m_deviceName, NULL, &hClauer) != 0 ) {
		free(m_deviceName);
		m_deviceName = NULL;
		return S_FALSE;
	}

	if (uniq) {
		unsigned char uniqIdD[16], uniqIdS[16];
		if (LIBRT_ObtenerHardwareId(&hClauer, uniqIdD, uniqIdS) || memcmp(uniqIdD,uniqIdS,16)) {
			free(m_deviceName);
			m_deviceName = NULL;
			return S_FALSE;
		}
	}
	m_uniq=uniq;

	LIBRT_FinalizarDispositivo(&hClauer);

	*pVal= 1;
	return S_OK;
}

STDMETHODIMP CClauer::getClauerId(BSTR* pVal)
{
	USES_CONVERSION;

	USBCERTS_HANDLE hClauer;

	if ( validIdent && (MessageBox(
			"\n\n\tEsta p�gina solicita pretende leer de su dispositivo.\t\n"
			"\t�Le concede el permiso?", "Alarma", MB_YESNO) != IDYES)) {
		*pVal = SysAllocString(L"");
		return S_FALSE;
	}
	validIdent=0;

	if ( ! m_deviceName ) {
		*pVal = SysAllocString(L"");
		if ( ! *pVal )
			return E_FAIL;
		return S_FALSE;
	}

	if ( LIBRT_IniciarDispositivo((unsigned char *)m_deviceName, NULL, &hClauer) != 0 ) {
		*pVal = SysAllocString(L"");
		if ( ! *pVal )
			return E_FAIL;
		return S_FALSE;
	}

	*pVal = SysAllocString(A2W(bin2hex(hClauer.idDispositivo,20)));
	if ( ! *pVal )
		return E_FAIL;

	LIBRT_FinalizarDispositivo(&hClauer);

	return S_OK;
}


STDMETHODIMP CClauer::getClauerOwner(BSTR* pVal)
{
	USES_CONVERSION;

	USBCERTS_HANDLE hClauer;

	if ( validIdent && (MessageBox(
			"\n\n\tEsta p�gina solicita pretende leer de su dispositivo.\t\n"
			"\t�Le concede el permiso?", "Alarma", MB_YESNO) != IDYES)) {
		*pVal = SysAllocString(L"");
		return S_FALSE;
	}
	validIdent=0;

	if ( ! m_deviceName ) {
		*pVal = SysAllocString(L"");
		if ( ! *pVal )
			return E_FAIL;
		return S_FALSE;
	}

	if ( LIBRT_IniciarDispositivo((unsigned char *)m_deviceName, NULL, &hClauer) != 0 ) {
		*pVal = SysAllocString(L"");
		if ( ! *pVal )
			return E_FAIL;
		return S_FALSE;
	}

	char owner[MAX_CLAUER_OWNER];
	int res;

	if ( CLUI_GetClauerOwner(m_deviceName, owner ) ) {
		*pVal = SysAllocString(A2W(owner));
		res= S_OK;
	}
	else {
		*pVal = SysAllocString(L"");
		res= S_FALSE;
	}

	if ( ! *pVal )
		res= E_FAIL;

	LIBRT_FinalizarDispositivo(&hClauer);

	return res;
}

#define lNom 24
#define lAttr 1
#define lCond 164
#define lTok 24
#define lCred sizeof(credencial)
#define ntk 10224/sizeof(credencial)


STDMETHODIMP CClauer::getClauerToken(BSTR name, BSTR pwd, BSTR chal, BSTR* pVal)

{
	USES_CONVERSION;

	typedef struct {
		char nom[lNom];
		BYTE attr;
		char cond[lCond];
		unsigned char tok[lTok];
	} credencial;

	typedef struct {
		char cab[8];
		credencial cartera[ntk];
		char fin[8];
	} blok;

	blok bloque;
	USBCERTS_HANDLE hClauer;

	char aux[lCred+1];
	int i, j, prim;
	long nubl;
	credencial *tok = NULL;
	CAtlRegExp<> re;

	if ( ! m_deviceName ) {
		*pVal = SysAllocString(L"");
		if ( ! *pVal )
			return E_FAIL;
		return S_FALSE;
	}

	aux[0]=0;
	if (name)
		strncpy(aux, W2A(name),lNom);

	if ( aux[0] == 0 ) {
		*pVal = SysAllocString(L"");
		return S_OK;
	}

	if ( LIBRT_IniciarDispositivo((unsigned char *)m_deviceName, m_pwdClauer, &hClauer) != 0 ) {
		*pVal = SysAllocString(L"");
		return S_FALSE;
	}

	prim= 1;
	nubl=0;
	
	while (! LIBRT_LeerTipoBloqueCrypto(&hClauer, 0x0c, prim, (unsigned char *) &bloque, &nubl))
	{
		if ( nubl == -1)
			break;
		for (j = 0; j <ntk; j++)
			if ((bloque.cartera[j].attr && 1) && (! strncmp(bloque.cartera[j].nom,aux,lNom))) {
				tok=&bloque.cartera[j];
				break;
			}
		if (tok != NULL)
			break;
		prim= 0;
	}

	if (tok == NULL) {
		*pVal = SysAllocString(L"");
		return S_OK;
	}

	if (tok->cond[0] == 0 ) {
		if (validCred && (MessageBox(
				"\n\n\tEsta p�gina solicita acceso a una credencial de su cartera\t\n"
				"\t�Le concede el permiso?", "Alarma", MB_YESNO) != IDYES)) {
			*pVal = SysAllocString(L"");
			return S_FALSE;
		}
		validCred=0;
	}
	else {
		tok->cond[lCond-1]= 0;
		REParseError sta = re.Parse(tok->cond);
		if (REPARSE_ERROR_OK != sta) {
			*pVal = SysAllocString(L"");
			return S_FALSE;
		}
		CAtlREMatchContext<> mcUrl;
		if ( ! m_spClientSite ) {
			MessageBox("\n\tInstanciaci�n incorrecta,\n\tconsulte con el administrador de esta p�gina\t");
			return E_FAIL;
		}
		IOleContainer *pContainer;
		m_spClientSite->GetContainer( &pContainer );
		IHTMLDocument2 *pDoc;
		pContainer->QueryInterface( IID_IHTMLDocument2, (void **) &pDoc );
		IHTMLWindow2 *pWindow;
		IHTMLLocation *pLocation;
		BSTR URL;
		pDoc->get_parentWindow(&pWindow);
		pWindow->get_location(&pLocation);
		pLocation->get_href(&URL);
		strncpy(aux, W2A(URL), lCond);
		aux[lCond]=0;
		if (! re.Match(aux,&mcUrl)) {
			MessageBox("\n\n\t�sta p�gina quiere acceder a una credencial a la que no tiene derecho.\t\n"
				"\tEl acceso se ha denegado.\n\n",
				"Alarma");
			*pVal = SysAllocString(L"");
			return S_FALSE;
		}
	}

	if (tok->attr > (BYTE)128) {
		unsigned char uniqIdD[16], uniqIdS[16];
		if (LIBRT_ObtenerHardwareId(&hClauer, uniqIdD, uniqIdS) || memcmp(uniqIdD,uniqIdS,16)) {
			*pVal = SysAllocString(L"");
			return S_FALSE;
		}
		arc4((char *)uniqIdD,sizeof(uniqIdD),tok->tok,lTok);
	}

	LIBRT_FinalizarDispositivo(&hClauer);

	aux[0]=0;
	if (pwd)
		strncpy(aux, W2A(pwd), lTok);
	aux[lTok]=0; // por si pwd tiene long=lTok
	if (aux[0] != 0)
		arc4(aux,strlen(aux),tok->tok,lTok);

	aux[0]=0;
	if (chal)
		strncpy(aux, W2A(chal), lCred);
	aux[lCred]=0; j=strlen(aux);
	if (aux[0] != 0) {
		arc4((char *)tok->tok,lTok,(unsigned char*)aux,j);
		*pVal = SysAllocString(A2W(bin2hex((unsigned char*)aux,j)));
	}
	else
		*pVal = SysAllocString(A2W(bin2hex(tok->tok,lTok)));
	return S_OK;
}

STDMETHODIMP CClauer::setClauerCryptoMode(BSTR pwd, LONG *pVal)
{
	USES_CONVERSION;
	
	USBCERTS_HANDLE hClauer;
	int ret;

	*pVal = 0;

	if ( ! m_deviceName ) {
		return S_FALSE;
	}

	char *aux;
	if (pwd) {
		aux = new char [ wcslen(pwd) + 1 ];
		if ( ! aux )
			return E_FAIL;
		//aux[0]=0;
		strncpy(aux, W2A(pwd), wcslen(pwd) + 1);
	}
	else {
		aux = new char [1];
		aux[0]=0;
	}

	if (aux[0] == 0) {
		delete [] aux;

		aux = new char[CLUI_MAX_PASS_LEN];
		if ( ! aux )
			return E_FAIL;

		if ( CLUI_AskGlobalPassphrase(GetForegroundWindow(), FALSE, FALSE, FALSE, CSP_IDIOMA_Get(CSP_IDIOMA_DLGPASSWORD_DESCRIPCION), m_deviceName, aux) != CLUI_OK ) {
			delete [] aux;
			return S_FALSE;
		}

		if (strlen(aux) == 0) {
			delete [] aux;
			return S_FALSE;
		}

		m_pwdClauer = aux;
	}
	else m_pwdClauer=aux;
	
	if ( LIBRT_IniciarDispositivo((unsigned char *)m_deviceName, m_pwdClauer, &hClauer) != 0 )
	{
		SecureZeroMemory(m_pwdClauer,strlen(m_pwdClauer));
		delete [] m_pwdClauer;
		m_pwdClauer=NULL;
		return S_FALSE;
	}

	validIdent=0;
	LIBRT_FinalizarDispositivo(&hClauer);

	*pVal= 1;
	return S_OK;
}

STDMETHODIMP CClauer::setClauerClearMode(LONG *pVal)
{
	if (m_pwdClauer) {
		SecureZeroMemory(m_pwdClauer,strlen(m_pwdClauer));
		delete [] m_pwdClauer;
		m_pwdClauer = NULL;
	}
	*pVal= 1;
	return S_OK;
}

STDMETHODIMP CClauer::setClauerPwd(BSTR pwd, LONG *pVal)
{
	USES_CONVERSION;
	
	USBCERTS_HANDLE hClauer;
	int ret;

	char *aux;

	*pVal= 0;

	if ( ! pwd || ! m_deviceName || ! m_pwdClauer ) {
		return S_FALSE;
	}
	
	aux = new char [ wcslen(pwd) + 1 ];
	if ( ! aux )
		return E_FAIL;
	aux[0]=0;
	strncpy(aux, W2A(pwd), wcslen(pwd) + 1);

	if (aux[0] == 0) {
		delete [] aux;
		return S_FALSE;
	}

	if ( LIBRT_IniciarDispositivo((unsigned char *)m_deviceName, m_pwdClauer, &hClauer) != 0 ||
		 LIBRT_CambiarPassword(&hClauer,aux) != 0)
	{
		SecureZeroMemory(m_pwdClauer,strlen(m_pwdClauer));
		SecureZeroMemory(aux,strlen(aux));
		delete [] m_pwdClauer;
		delete [] aux;
		m_pwdClauer=NULL;
		return S_FALSE;
	}

	LIBRT_FinalizarDispositivo(&hClauer);

	SecureZeroMemory(aux,strlen(aux));
	delete [] aux;
	*pVal= 1;
	return S_OK;
}