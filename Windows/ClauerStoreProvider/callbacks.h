#ifndef __CALLBACKS_H__
#define __CALLBACKS_H__

#include <windows.h>
#include <wincrypt.h>

#include <LIBRT/LIBRT.h>

/* Información que pasamos a las callbacks en HCERTSTOREPROV
 */

class CInfoCallbacks {

	HCERTSTORE hStore;				/* Handle al store */
	BYTE clauerId[20];				/* Id del insertado en la apertura del store */

public:

	CInfoCallbacks (void);
	~CInfoCallbacks (void);

	void SetHStore   (HCERTSTORE hStore);
	void SetClauerId (BYTE clauerId[20]);
	
	inline HCERTSTORE GetHStore   (void);
	void	   GetClauerId (BYTE clauerId[20]);

};


typedef struct {
	HCERTSTORE hStore;
	char szDevice[MAX_PATH_LEN];
} INFO_CALLBACKS;



BOOL WINAPI CertStoreProvDeleteCertCallback(
  HCERTSTOREPROV hStoreProv,
  PCCERT_CONTEXT pCertContext,
  DWORD dwFlags
);

BOOL WINAPI CertStoreProvWriteCertCallback(
  HCERTSTOREPROV hStoreProv,
  PCCERT_CONTEXT pCertContext,
  DWORD dwFlags
);

void WINAPI CertStoreProvCloseCallback(
  HCERTSTOREPROV hStoreProv,
  DWORD dwFlags
);


BOOL WINAPI CertStoreProvControlCallback(
  HCERTSTOREPROV hStoreProv,
  DWORD dwFlags,
  DWORD dwCtrlType,
  void const* pvCtrlPara
);

#endif
