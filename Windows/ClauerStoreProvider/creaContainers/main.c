#include <windows.h>
#include <wincrypt.h>

#include <CryptoWrapper/CRYPTOWrap.h>
#include <LIBRT/UtilBloques/UtilBloques.h>
#include <LIBRT/LIBRT.h>
#include <b64/b64.h>



BOOL DER2PEM (BYTE *der, unsigned long tamDER, BYTE *pem, unsigned long *tamPEM)
{
  unsigned long atp;

  B64_Codificar(der, tamDER, NULL, &atp);
  atp += 28+27;

  *tamPEM = atp;
  if ( !pem )
    return TRUE;

  strcpy((char *)pem, "-----BEGIN CERTIFICATE-----");
  *(pem+27) = '\n';
  B64_Codificar(der, tamDER, pem+28, &atp);
  strcpy((char *)(pem+28+atp), "-----END CERTIFICATE-----\n");

  return TRUE;
}






void ImportarCert (char *file, USBCERTS_HANDLE *usbHandle)
{

	FILE *f;
	DWORD tam, aux, tamPEM;
	BYTE *cert, bloque[TAM_BLOQUE];
	BYTE *pem;
	int i;

	f = fopen(file, "rb");
	fseek(f,0,SEEK_END);
	tam = ftell(f);
	fseek(f,0,SEEK_SET);
	cert = (BYTE *) malloc (sizeof(BYTE)*tam);
	fread(cert,1,tam,f);
	fclose(f);

	DER2PEM (cert, tam, NULL, &tamPEM);
	pem = (BYTE *) malloc (tamPEM);
	DER2PEM (cert, tam, pem, &tamPEM);

	for ( i = 0 ; i < tamPEM ; i++ )
		printf("%c", pem[i]);
	fflush(stdout);

	BLOQUE_CERTOTROS_Nuevo(bloque);
	BLOQUE_CERTOTROS_Set_Tam(bloque,tamPEM);
	BLOQUE_CERTOTROS_Set_Objeto(bloque,pem,tamPEM);

	LIBRT_InsertarBloqueCrypto(usbHandle,bloque,&aux);

}



void ImportarPKCS12 (char *file, char *pwd, USBCERTS_HANDLE hClauer)
{

	FILE *f;
	DWORD tam,tamTotal = 0;
	BYTE *pkcs12, *llavePrivada, *certLlave, *certs,*blob;
	int i;
	int nb;
	BYTE bloque[TAM_BLOQUE];
	BYTE id[20];
	char container[MAX_PATH];
	unsigned long tamBlob;

	unsigned long tamLlavePrivada, tamCertLlave, *tamCerts, numCerts;

	LIBRT_Ini();
	CRYPTO_Ini();

	/* Leo PKCS12 */

	f = fopen(file, "rb");
	fseek(f,0,SEEK_END);
	tam = ftell(f);
	fseek(f,0,SEEK_SET);
	pkcs12 = (BYTE *) malloc (tam);
	fread(pkcs12,1,tam,f);
	fclose(f);


	CRYPTO_ParsePKCS12 (pkcs12, tam, pwd, NULL, &tamLlavePrivada, NULL, &tamCertLlave, NULL, NULL, &numCerts);

	llavePrivada = (BYTE *) malloc (tamLlavePrivada);
	certLlave = (BYTE *) malloc (tamCertLlave);
	tamCerts = (unsigned long *) malloc (sizeof(unsigned long)*numCerts);

	CRYPTO_ParsePKCS12 (pkcs12, tam, pwd,
       					llavePrivada, &tamLlavePrivada,
						certLlave, &tamCertLlave,
						NULL, tamCerts, &numCerts);

	for ( i = 0 ; i < numCerts ; i++ )
		tamTotal += tamCerts[i];

	certs = (BYTE *) malloc (tamTotal);


	CRYPTO_ParsePKCS12 (pkcs12, tam, pwd,
       					llavePrivada, &tamLlavePrivada,
						certLlave, &tamCertLlave,
						certs, tamCerts, &numCerts);


	/* A importar */

	CRYPTO_LLAVE_PEM_Id (llavePrivada, tamLlavePrivada, 1, NULL, id);
	BLOQUE_LLAVEPRIVADA_Nuevo(bloque);
	BLOQUE_LLAVEPRIVADA_Set_Tam(bloque, tamLlavePrivada);
	BLOQUE_LLAVEPRIVADA_Set_Id(bloque,id);
	BLOQUE_LLAVEPRIVADA_Set_Objeto(bloque, llavePrivada, tamLlavePrivada);
	LIBRT_InsertarBloqueCrypto(&hClauer,bloque,&nb);

	CRYPTO_LLAVE2BLOB(llavePrivada, tamLlavePrivada, NULL, NULL, &tamBlob);
	blob = (BYTE *) malloc (tamBlob);
	CRYPTO_LLAVE2BLOB(llavePrivada, tamLlavePrivada, NULL, blob, &tamBlob);
	BLOQUE_PRIVKEYBLOB_Nuevo(bloque);
	BLOQUE_PRIVKEYBLOB_Set_Tam(bloque, tamBlob);
	BLOQUE_PRIVKEYBLOB_Set_Id(bloque, id);
	BLOQUE_PRIVKEYBLOB_Set_Objeto(bloque, blob, tamBlob);
	LIBRT_InsertarBloqueCrypto(&hClauer,bloque,&nb);

	/* A�ado un bloque key container siempre */

	CRYPTO_Random(10,container);
	for ( i = 0 ; i < 10 ; i++ ) 
		container[i] = 'a' + ((unsigned char)container[i] % 10);

	container[9] = '\0';

	BLOQUE_KeyContainer_Nuevo(bloque);
	BLOQUE_KeyContainer_Insertar(bloque, container);
	BLOQUE_KeyContainer_EstablecerSIGNATURE(bloque, container, nb, TRUE);
	LIBRT_InsertarBloqueCrypto(&hClauer, bloque, &nb);

	CRYPTO_CERT_PEM_Id (certLlave, tamCertLlave,id);
	BLOQUE_CERTPROPIO_Nuevo(bloque);
	BLOQUE_CERTPROPIO_Set_Tam(bloque, tamCertLlave);
	BLOQUE_CERTPROPIO_Set_Id(bloque, id);
	BLOQUE_CERTPROPIO_Set_Objeto(bloque, certLlave, tamCertLlave);
	LIBRT_InsertarBloqueCrypto(&hClauer,bloque,&nb);

	for ( i = 0 ; i < numCerts ; i++ ) {
		BLOQUE_CERTRAIZ_Nuevo(bloque);
		BLOQUE_CERTRAIZ_Set_Tam(bloque, tamCerts[i]);
		BLOQUE_CERTRAIZ_Set_Objeto(bloque, certs+i*TAM_BLOQUE, tamCerts[i]);
		LIBRT_InsertarBloqueCrypto(&hClauer,bloque,&nb);
	}

	/* S'acab� */

	free(certs);
	free(llavePrivada);
	free(tamCerts);
	free(certLlave);
	free(pkcs12);


}


int main (int argc, char **argv)
{
	char pass[20], *aux;
	USBCERTS_HANDLE h;

	LIBRT_Ini();
	CRYPTO_Ini();

	if ( argc < 3 ) {
		fprintf(stderr, "ERROR. Uso: %s pkcs12 cert\n", argv[0]);
		return 1;
	}

	if ( LIBRT_IniciarDispositivoVentana(&h) != 0 ) {
		fprintf(stderr,  "No se pudo iniciar dispositivo\n");
		return 1;
	}

	printf("Importador pkcs12 (s�per beta)\n");
	printf("   No hay control de errores pecador, as� es que cuidad�n\n\n");

	printf("Password de importaci�n: ");
	fflush(stdout);
	fgets(pass,20,stdin);

	aux = pass;
	while ( (*aux != '\r') && (*aux != '\n') && ( *aux != '\0')) 
		++aux;
	
	if ( *aux == '\n' )
		printf("Nueva l�nea\n");
	else if ( *aux == '\r' )
		printf("Retorno de carro\n");

	*aux = '\0';

	printf("Password: %s", pass);
	fflush(stdout);

	ImportarPKCS12(argv[1], pass,h);
	ImportarCert(argv[2],&h);

	return 0;

}
