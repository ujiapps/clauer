#ifndef __UTILSTORE_H__
#define __UTILSTORE_H__

#include <windows.h>
#include <wincrypt.h>

#include <LIBRT/LIBRT.h>


BOOL Conectar ( USBCERTS_HANDLE *hClauer, int *nDispositivos );

BOOL DER2PEM       (BYTE *der, unsigned long tamDER, BYTE *pem, unsigned long *tamPEM);
BOOL PEM2DER       (BYTE *pem, unsigned long tamPEM, BYTE *der, unsigned long *tamDER);
BOOL ASCII2UNICODE (char *a, unsigned short *u);

PCCERT_CONTEXT CERT_GetCertificateContext (BYTE *certDER, DWORD tamCert);

BOOL CL_CalculateID ( PCCERT_CONTEXT certCtx, unsigned char id[20] );

BOOL CargarMY          (HCERTSTORE hCertStore, USBCERTS_HANDLE *usbHandle);
BOOL CargarAddressBook (HCERTSTORE hCertStore, USBCERTS_HANDLE *usbHandle);
BOOL CargarCA		   (HCERTSTORE hCertStore, USBCERTS_HANDLE *usbHandle);
BOOL CargarRoot        (USBCERTS_HANDLE *usbHandle);

#endif
