// Log.h: interface for the CLog class.
//
//////////////////////////////////////////////////////////////////////

#if !defined(AFX_LOG_H__2F18F3E5_452A_4772_8B3C_04141A0CD1E3__INCLUDED_)
#define AFX_LOG_H__2F18F3E5_452A_4772_8B3C_04141A0CD1E3__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

class CLog  
{
public:
	CLog();
	virtual ~CLog();

};

#endif // !defined(AFX_LOG_H__2F18F3E5_452A_4772_8B3C_04141A0CD1E3__INCLUDED_)
