#include <windows.h>
#include <wincrypt.h>
#include <dbt.h>

#include "callbacks.h"
#include "utilstore.h"

#include <LIBRT/LIBRT.h>
#include <oolog/oolog.h>
#include <clui/clui.h>

#include <list>

#ifdef _DEBUG
extern COOLOG log;
#endif

extern BOOL logOpened;
extern HINSTANCE hDll;

using namespace std;

static list<HANDLE> lstEvents;
WNDCLASSEX wcx;
HWND hwnd = 0;




CInfoCallbacks::CInfoCallbacks (void)
{
	ZeroMemory(this->clauerId, 20);
	this->hStore = 0;
}



CInfoCallbacks::~CInfoCallbacks (void)
{

}


void CInfoCallbacks::SetHStore (HCERTSTORE hStore)
{
	this->hStore = hStore;
}


void CInfoCallbacks::SetClauerId (BYTE clauerId[20])
{
	memcpy((void *) this->clauerId, clauerId, 20);
}

	
inline HCERTSTORE CInfoCallbacks::GetHStore (void)
{
	return this->hStore;
}



void CInfoCallbacks::GetClauerId (BYTE clauerId[20])
{
	memcpy((void *) clauerId, (void *) this->clauerId, 20);
}



/* La runtina encargada del procesamiento de los mensajes que llegan a la ventana. 
 * Aqu� s�lo nos interesa, de momento, procesar el mensaje WM_DEVICECHANGE
 */

LRESULT CALLBACK MainWndProc(
    HWND hwnd,        // handle to window
    UINT uMsg,        // message identifier
    WPARAM wParam,    // first message parameter
    LPARAM lParam)    // second message parameter
{

	BYTE *dispositivos = NULL;
	list<HANDLE>::iterator i;



    switch (uMsg) 
    { 
        case WM_CREATE: 
            // Initialize the window. 
            return 0; 
 
        case WM_PAINT: 
            // Paint the window's client area. 
            return 0; 
 
        case WM_SIZE: 
            // Set the size and position of the window. 
            return 0; 
 
        case WM_DESTROY: 
            // Clean up window-specific data objects. 
            return 0; 
 
		case WM_DEVICECHANGE:

			switch ( wParam ) {
			case DBT_DEVICEARRIVAL:

				/* Vale... esto no es lo mejor, pero bueno. Cada vez que se inserte un dispositivo
				 * actualizamos los stores por si acaso
				 */

				for ( i = lstEvents.begin() ; i != lstEvents.end() ; i++ ) {
					SetEvent(*i);
				}


				break;

			case DBT_DEVICEREMOVECOMPLETE:

				/* �dem a lo de antes */

				for ( i = lstEvents.begin() ; i != lstEvents.end() ; i++ ) {
					SetEvent(*i);
				}

				break;

			}
			return 0;

		// 
        // Process other messages. 
        // 
 
        default: 
            return DefWindowProc(hwnd, uMsg, wParam, lParam); 
    } 
    return 0; 
}


/******************************************************************************/
/* Callbacks
 */

BOOL WINAPI CertStoreProvDeleteCertCallback(
  HCERTSTOREPROV hStoreProv,
  PCCERT_CONTEXT pCertContext,
  DWORD dwFlags
)
{
	return TRUE;
}








void WINAPI CertStoreProvCloseCallback(
  HCERTSTOREPROV hStoreProv,
  DWORD dwFlags
)
{
	USBCERTS_HANDLE *usbHandle = (USBCERTS_HANDLE *) hStoreProv;

#ifdef _DEBUG
	log.EnterFunc("Close Callback");
	log.ExitFunc();

	logOpened = FALSE;
	log.Close();

#endif

}








BOOL WINAPI CertStoreProvControlCallback(
  HCERTSTOREPROV hStoreProv,
  DWORD dwFlags,
  DWORD dwCtrlType,
  void const* pvCtrlPara
)
{

	PCCERT_CONTEXT pCertPrev, pCertNext;
	CInfoCallbacks *info = ( CInfoCallbacks *) hStoreProv;
	USBCERTS_HANDLE hClauer;
	int nDisp;

	list<HANDLE>::iterator i;

	BOOL ret = TRUE;
	BOOL encontrado;

#ifdef _DEBUG
	log.EnterFunc("CertStoreProvControlCallback");

	log << "dwCtrlType: ";
	switch ( dwCtrlType ) {
	case CERT_STORE_CTRL_RESYNC:
		log << "CERT_STORE_CTRL_RESYNC" << endl;
		break;

	case CERT_STORE_CTRL_NOTIFY_CHANGE:
		log << "CERT_STORE_CTRL_NOTIFY_CHANGE" << endl;
		break;

	case CERT_STORE_CTRL_COMMIT:
		log << "CERT_STORE_CTRL_COMMIT" << endl;
		break;

	case CERT_STORE_CTRL_AUTO_RESYNC:
		log << "CERT_STORE_CTRL_AUTO_RESYNC" << endl;
		break;

	case CERT_STORE_CTRL_CANCEL_NOTIFY:
		log << "CERT_STORE_CTRL_CANCEL_NOTIFY" << endl;
		break;
	}
	

	log << "dwFlags: ";

	if ( CERT_STORE_CTRL_COMMIT == dwFlags ) {

		switch ( dwFlags ) {
		case CERT_STORE_CTRL_COMMIT_FORCE_FLAG:
			log << "CERT_STORE_CTRL_COMMIT_FORCE_FLAG" << endl;
			break;

		case CERT_STORE_CTRL_COMMIT_CLEAR_FLAG:
			log << "CERT_STORE_CTRL_COMMIT_CLEAR_FLAG" << endl;
			break;

	/*	case CERT_STORE_CTRL_INHIBIT_DUPLICATE_HANDLE_FLAG:
			log << "CERT_STORE_CTRL_INHIBIT_DUPLICATE_HANDLE_FLAG" << endl;
			break;
	*/
		}

	} else {

		log << "NO TRASCENDENTE" << endl;

	}
#endif

	if ( CERT_STORE_CTRL_RESYNC == dwCtrlType ) {

		/* Borro los certificados del store y los vuelvo a cargar
		 */

		pCertPrev = NULL;
		pCertNext = NULL;
		pCertPrev = CertEnumCertificatesInStore(info->GetHStore(), NULL );
		if ( pCertPrev ) {
		
			do {
			
				pCertNext = CertEnumCertificatesInStore(info->GetHStore(), pCertPrev);
				CertDeleteCertificateFromStore(pCertPrev);
				pCertPrev = pCertNext;

			} while ( pCertNext );

		}

		/* Conectamos con el store para volver a cargar los dispositivos
		 */

		if ( ! Conectar(&hClauer,&nDisp) ) {
			ret = FALSE;
			goto finCertStoreProvControlCallback;
		}

		if ( 0 == nDisp ) {

			ret = TRUE;
			goto finCertStoreProvControlCallback;

		} else if ( nDisp > 1 ) {
			MessageBox(NULL, "Hay m�s de un Clauer insertado en el sistema.\r\nDeje s�lo el que desee utilizar", "Projecte Clauer :: ATENCI�N", MB_OK|MB_ICONWARNING);
			ret = TRUE;
			goto finCertStoreProvControlCallback;
		}

/*		CargarAddressBook (info->GetHStore(), &hClauer);
		CargarCA (info->GetHStore(), &hClauer);
		CargarRoot (info->GetHStore(), &hClauer);		
*/
		if ( ! CargarMY(info->GetHStore(), &hClauer) ) {
			LIBRT_FinalizarDispositivo(&hClauer);
			return FALSE;
		}

		if ( LIBRT_FinalizarDispositivo(&hClauer) != 0 ) {
			return FALSE;
		}

	} else if ( CERT_STORE_CTRL_NOTIFY_CHANGE == dwCtrlType ) {

		BOOL ok = TRUE;

		if ( ! GetClassInfoEx(hDll, "ClauerStoreProviderWindowClass", &wcx) ) {

			ZeroMemory(&wcx, sizeof(WNDCLASSEX));

			wcx.cbSize        = sizeof(wcx);						// size of structure 
			wcx.style         = CS_HREDRAW |CS_VREDRAW;				// redraw if size changes 
			wcx.lpfnWndProc   = MainWndProc;						// points to window procedure 
			wcx.cbClsExtra    = 0;									// no extra class memory 
			wcx.cbWndExtra    = 0;									// no extra window memory 
			wcx.hInstance     = hDll;								// handle to instance 
			wcx.hIcon         = NULL;								// hIcon
			wcx.hCursor       = NULL;								// predefined arrow 
			wcx.hbrBackground = NULL;								// white background brush 
			wcx.lpszMenuName  = NULL;		                        // name of menu resource 
			wcx.lpszClassName = "ClauerStoreProviderWindowClass";   // name of window class 
			wcx.hIconSm       = NULL;

			if ( ! RegisterClassEx(&wcx) ) {
				ok = FALSE;
				goto finCertStoreProvControlCallback;
			}
		}

		if ( ! hwnd ) {
			hwnd = CreateWindow( 
				"ClauerStoreProviderWindowClass",			// name of window class 
				"Na",										// title-bar string 
				CS_HREDRAW | CS_VREDRAW |CS_GLOBALCLASS ,	// top-level window 
				0,											// default horizontal position 
				0,											// default vertical position 
				100,										// default width 
				100,										// default height 
				(HWND) GetForegroundWindow(),				// no owner window 
				(HMENU) NULL,								// use class menu 
				hDll,										// handle to application instance 
				(LPVOID) NULL);								// no window-creation data 
 
			if ( ! hwnd ) {
				ok = FALSE;
				goto finCertStoreProvControlCallback;
			}
		}

		encontrado = FALSE;
		for ( i = lstEvents.begin() ; (i != lstEvents.end()) && !encontrado ; i++ ) {
			if ( *i == *((HANDLE *) pvCtrlPara) )
				encontrado = TRUE;
		}

		if ( ! encontrado )
			lstEvents.push_back(*((HANDLE *) pvCtrlPara));

	} else if ( CERT_STORE_CTRL_CANCEL_NOTIFY == dwCtrlType ) {

		lstEvents.remove(*((HANDLE *) pvCtrlPara));

	} else if ( CERT_STORE_CTRL_NOTIFY_CHANGE == dwCtrlType ) {

		SetLastError(ERROR_NOT_SUPPORTED);
		ret = FALSE;

	} else if ( CERT_STORE_CTRL_AUTO_RESYNC == dwCtrlType ) {

		SetLastError(ERROR_NOT_SUPPORTED);
		ret = FALSE;

	} else {

		SetLastError(ERROR_NOT_SUPPORTED);
		ret = FALSE;

	}


finCertStoreProvControlCallback:

#ifdef _DEBUG
	if ( FALSE == ret ) 
		log.ExitFunc("ERROR");
	else
		log.ExitFunc();
#endif

	return ret;

}




/*! \brief Borra un certificado y las llaves asociadas del clauer
 *
 * Borra un certificado y las llaves asociadas del clauer. En concreto lo que
 * borramos es:
 *
 *      -# Container asociado
 *      -# El BLOB asociado
 *      -# La llave PEM asociada
 *      -# El certificado
 */











BOOL WINAPI CertStoreProvWriteCertCallback(
  HCERTSTOREPROV hStoreProv,
  PCCERT_CONTEXT pCertContext,
  DWORD dwFlags
)
{
	USBCERTS_HANDLE hClauer;
	BOOL ret = TRUE;
	char szPwd[CLUI_MAX_PASS_LEN];
	int retPin;
	INFO_CALLBACKS *ic = (INFO_CALLBACKS *) hStoreProv;

	unsigned char block[TAM_BLOQUE];
	long bn;

	unsigned char *certPEM = NULL;
	unsigned long certPEMSize;
	unsigned char idCert[20];

	unsigned char *der = NULL;
	unsigned long derSize;
	PCCERT_CONTEXT derCtx = NULL;

	LPWSTR lpwzFriendlyName = NULL;
	LPSTR lpszFriendlyName = NULL;
	DWORD dwlpwzFriendlyNameSize, dwlpszFriendlyNameSize;
	int friendlyNameExists;

	unsigned char blockBlob[TAM_BLOQUE];
	long bnBlob;
	BOOL bBlobFound;

	BOOL bInsertCertificate;

	/* Si no se especific� un dispositivo volvemos
	 */

	if ( *(ic->szDevice) ) {

		/* Pasamos el certificado a formato PEM
		 */
		
		if ( ! DER2PEM(pCertContext->pbCertEncoded, pCertContext->cbCertEncoded, NULL, &certPEMSize) ) {
			ret = FALSE;
			goto endCertStoreProvWriteCertCallback;
		}
		certPEM = ( unsigned char * ) malloc ( certPEMSize );
		if ( ! certPEM ) {
			ret = FALSE;
			goto endCertStoreProvWriteCertCallback;
		}
		if ( ! DER2PEM(pCertContext->pbCertEncoded, pCertContext->cbCertEncoded, certPEM, &certPEMSize) ) {
			ret = FALSE;
			goto endCertStoreProvWriteCertCallback;
		}

		/* Calculo el identificador del certificado
		 */

		if ( ! CL_CalculateID(pCertContext, idCert) ) {
			ret = FALSE;
			goto endCertStoreProvWriteCertCallback;
		}

		/* Si el certificado no tiene la llave asociada en el clauer,
		 * devolvemos FALSE --> pasamos al siguiente store
		 */

		if ( LIBRT_IniciarDispositivo((unsigned char *) ic->szDevice, NULL, &hClauer) != 0 ) {
			ret = FALSE;
			goto endCertStoreProvWriteCertCallback;
		}

		if ( LIBRT_LeerTipoBloqueCrypto(&hClauer, BLOQUE_KEY_CONTAINERS, 1, blockBlob, &bnBlob) != 0 ) {
			ret = FALSE;
			goto endCertStoreProvWriteCertCallback;
		}
		bBlobFound = FALSE;
		while ( ! bBlobFound && bnBlob != -1 ) {
			INFO_KEY_CONTAINER ikc[NUM_KEY_CONTAINERS];
			unsigned int numIkc;

			if ( BLOQUE_KeyContainer_Enumerar(blockBlob, ikc, &numIkc) != 0 ) {
				ret = FALSE;
				goto endCertStoreProvWriteCertCallback;
			}		

			for ( int i = 0 ; i < numIkc; i++ ) {
				if ( memcmp(idCert,ikc[i].idExchange, 20) == 0 ) {
					bBlobFound = TRUE;
					break;
				} else if ( memcmp(idCert,ikc[i].idSignature, 20 ) == 0 ) {
					bBlobFound = TRUE;
					break;
				}
			}
			if ( ! bBlobFound ) {
				if ( LIBRT_LeerTipoBloqueCrypto(&hClauer, BLOQUE_KEY_CONTAINERS, 0, blockBlob, &bnBlob) != 0 ) {
					LIBRT_FinalizarDispositivo(&hClauer);
					ret = FALSE;
					goto endCertStoreProvWriteCertCallback;
				}
			}
		}
		LIBRT_FinalizarDispositivo(&hClauer);
		if ( ! bBlobFound ) {
			ret = FALSE;
			goto endCertStoreProvWriteCertCallback;
		}

		/* Extraigo el friendlyname del cert si existe
		 */

		if ( ! CertGetCertificateContextProperty(pCertContext, CERT_FRIENDLY_NAME_PROP_ID, NULL, &dwlpwzFriendlyNameSize) ) {

			if ( GetLastError() == CRYPT_E_NOT_FOUND )
				friendlyNameExists = 0;
			else {
				ret = FALSE;
				goto endCertStoreProvWriteCertCallback;
			}

		} else {
			friendlyNameExists = 1;
		}

		if ( friendlyNameExists ) {

			lpwzFriendlyName = ( LPWSTR ) malloc ( dwlpwzFriendlyNameSize );
			if ( ! lpwzFriendlyName ) {
				ret = FALSE;
				goto endCertStoreProvWriteCertCallback;
			}

			if ( ! CertGetCertificateContextProperty(pCertContext, CERT_FRIENDLY_NAME_PROP_ID, lpwzFriendlyName, &dwlpwzFriendlyNameSize) ) {
				ret = FALSE;
				goto endCertStoreProvWriteCertCallback;
			}

			dwlpszFriendlyNameSize = WideCharToMultiByte(CP_ACP, 0, lpwzFriendlyName, dwlpwzFriendlyNameSize/sizeof(WCHAR), NULL, 0, NULL, NULL);
			if ( ! dwlpszFriendlyNameSize ) {
				ret = FALSE;
				goto endCertStoreProvWriteCertCallback;
			}

			lpszFriendlyName = ( char * ) malloc ( dwlpszFriendlyNameSize );
			if ( ! lpszFriendlyName ) {
				ret = FALSE;
				goto endCertStoreProvWriteCertCallback;
			}

			if ( ! WideCharToMultiByte(CP_ACP, 0, lpwzFriendlyName, dwlpwzFriendlyNameSize/sizeof(WCHAR), lpszFriendlyName, dwlpszFriendlyNameSize, NULL, NULL) ) {
				ret = FALSE;
				goto endCertStoreProvWriteCertCallback;
			}

			free(lpwzFriendlyName);
			lpwzFriendlyName = NULL;
		}



		/* Inserto el bloque si no exist�a previamente
		 */


		SecureZeroMemory(block, TAM_BLOQUE);
		BLOQUE_Set_Claro(block);
		BLOQUE_CERTPROPIO_Nuevo(block);
		BLOQUE_CERTPROPIO_Set_Objeto(block, certPEM, certPEMSize);
		BLOQUE_CERTPROPIO_Set_Tam(block, certPEMSize);
		BLOQUE_CERTPROPIO_Set_Id(block, idCert);
		if ( friendlyNameExists )
			BLOQUE_CERTPROPIO_Set_FriendlyName(block, lpszFriendlyName);
		else
			BLOQUE_CERTPROPIO_Set_FriendlyName(block, "");

		/* Insertamos el bloque
		 */

		retPin = CLUI_AskGlobalPassphrase(NULL, FALSE, FALSE, TRUE, NULL, ic->szDevice, szPwd);
		if ( retPin == CLUI_ERR || retPin == CLUI_CANCEL ) {
			ret = FALSE;
			goto endCertStoreProvWriteCertCallback;
		}

		if ( LIBRT_IniciarDispositivo((unsigned char *) (ic->szDevice), szPwd, &hClauer) != 0 ) {
			ret = FALSE;
			goto endCertStoreProvWriteCertCallback;
		}

		SecureZeroMemory(szPwd, CLUI_MAX_PASS_LEN);

		bInsertCertificate = TRUE;
		unsigned char blockCert[TAM_BLOQUE];
		long bnCert;
		if ( LIBRT_LeerTipoBloqueCrypto(&hClauer, BLOQUE_CERT_PROPIO, 1, blockCert, &bnCert) != 0 ) {
			LIBRT_FinalizarDispositivo(&hClauer);
			ret = FALSE;
			goto endCertStoreProvWriteCertCallback;
		}
		while ( bInsertCertificate && bnCert != -1 ) {

			if ( memcmp(idCert, BLOQUE_CERTPROPIO_Get_Id(blockCert), 20) == 0 ) {

				if ( ! PEM2DER(BLOQUE_CERTPROPIO_Get_Objeto(block), 
							   BLOQUE_CERTPROPIO_Get_Tam(block),
							   NULL,
							   &derSize) )
				{
					ret = FALSE;
					goto endCertStoreProvWriteCertCallback;
				}
				der = ( unsigned char * ) malloc ( derSize );
				if ( ! der ) {
					ret = FALSE;
					goto endCertStoreProvWriteCertCallback;
				}
				if ( ! PEM2DER(BLOQUE_CERTPROPIO_Get_Objeto(block), 
							   BLOQUE_CERTPROPIO_Get_Tam(block),
							   der,
							   &derSize) )
				{
					ret = FALSE;
					goto endCertStoreProvWriteCertCallback;
				}

				derCtx = CERT_GetCertificateContext (der, derSize);
				if ( ! derCtx ) {
					ret = FALSE;
					goto endCertStoreProvWriteCertCallback;
				}

				free(der);
				der = NULL;

				if ( CertCompareCertificate(X509_ASN_ENCODING,
											pCertContext->pCertInfo,
											derCtx->pCertInfo) ) 
					bInsertCertificate = FALSE;

				CertFreeCertificateContext(derCtx);
				derCtx = NULL;
			}

			if ( bInsertCertificate ) {

				if ( LIBRT_LeerTipoBloqueCrypto(&hClauer, BLOQUE_CERT_PROPIO, 0, blockCert, &bnCert) != 0 ) {
					LIBRT_FinalizarDispositivo(&hClauer);
					ret = FALSE;
					goto endCertStoreProvWriteCertCallback;
				}
			} 
		}


		if ( bInsertCertificate ) {
			if ( LIBRT_InsertarBloqueCrypto(&hClauer, block, &bn) != 0 ) {
				LIBRT_FinalizarDispositivo(&hClauer);
				ret = FALSE;
				goto endCertStoreProvWriteCertCallback;
			}
		}

		LIBRT_FinalizarDispositivo(&hClauer);
	} else {
		ret = FALSE;
	}
	
endCertStoreProvWriteCertCallback:

	SecureZeroMemory(blockBlob, TAM_BLOQUE);
	SecureZeroMemory(szPwd, CLUI_MAX_PASS_LEN);
	SecureZeroMemory(block, TAM_BLOQUE);
	if ( certPEM ) {
		SecureZeroMemory(certPEM, certPEMSize);
		free(certPEM);
	}
	if ( lpwzFriendlyName ) 
		free(lpwzFriendlyName);
	if ( lpszFriendlyName )
		free(lpszFriendlyName);
	if ( der )
		free(der);

	return ret;
}

