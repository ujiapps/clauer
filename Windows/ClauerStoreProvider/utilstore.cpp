#include "utilstore.h"

#include <b64/b64.h>
#include <LIBRT/LIBRT.h>

#ifdef _DEBUG
#include <oolog/oolog.h>
#endif

#include <string>

#ifdef _DEBUG
extern COOLOG log;
#endif


/*! \brief Conecta contra un dispositivo.
 *
 * Conecta contra un dispositivo
 */

BOOL Conectar ( USBCERTS_HANDLE *hClauer, int *nDispositivos )
{
	BOOL ok = TRUE;
	BYTE *dispositivos[MAX_DEVICES];

	memset(dispositivos, 0, MAX_DEVICES * sizeof(char *));

	/* Comprobamos que exista uno y s�lo un Clauer insertado
	 */

	if ( LIBRT_ListarDispositivos(nDispositivos, dispositivos) != 0 ) {
		ok = FALSE;
		goto finConectar;
	}

	if ( *nDispositivos == 0 )  {
		ok = TRUE;
		goto finConectar;
	} else if ( *nDispositivos > 1 ) {
		MessageBox(NULL, "Hay m�s de un Clauer insertado en el sistema.\r\nDeje s�lo el que desee utilizar", "Projecte Clauer :: ATENCI�N", MB_OK|MB_ICONWARNING);
		ok = TRUE;
		goto finConectar;
	}

	if ( LIBRT_IniciarDispositivo(dispositivos[0], NULL, hClauer) != 0 ) {
		ok = FALSE;
		goto finConectar;
	}


finConectar:

	for ( int i = 0 ; i < MAX_DEVICES ; i++ )
		if ( dispositivos[i] )
			free(dispositivos[i]);
		else
			break;

	return ok;

}








/*! \brief Convierte un certificado en formato DER a PEM.
 *
 * Convierte un certificado en formato DER a PEM.
 *
 * \param der
 *		  ENTRADA. El certificado en formato DER.
 *
 * \param tamDER
 *		  ENTRADA. El tama�o (en bytes) del certificado alojado en der.
 *
 * \param pem
 *		  SALIDA. El certificado en formato PEM.
 *
 * \param tamPEM
 *		  SALIDA. El tama�o del certificado PEM devuelto.
 *
 * \retval TRUE
 *		   Ok
 *
 * \retval FALSE
 *		   Error
 */

BOOL DER2PEM (BYTE *der, unsigned long tamDER, BYTE *pem, unsigned long *tamPEM)
{
  unsigned long atp;

  B64_Codificar(der, tamDER, NULL, &atp);
  atp += 28+27;

  *tamPEM = atp;
  if ( !pem ) 
    return TRUE;

  strcpy((char *)pem, "-----BEGIN CERTIFICATE-----");
  *(pem+27) = '\n';
  B64_Codificar(der, tamDER, pem+28, &atp);
  strcpy((char *)(pem+28+atp), "-----END CERTIFICATE-----\n");

  return TRUE;
}



/*! \brief Convierte un certificado en formato PEM a DER.
 *
 * Convierte un certificado en formato PEM a DER.
 *
 * \param pem
 *		  ENTRADA. El certificado en formato PEM.
 *
 * \param tamPEM
 *		  ENTRADA. El tama�o (en bytes) del certificado alojado en pem.
 *
 * \param der
 *		  SALIDA. El certificado en formato DER.
 *
 * \param tamDER
 *		  SALIDA. El tama�o del certificado DER devuelto.
 *
 * \retval TRUE
 *		   Ok
 *
 * \retval FALSE
 *		   Error
 */

BOOL PEM2DER (BYTE *pem, unsigned long tamPEM, BYTE *der, unsigned long *tamDER)
{
	BYTE *cuerpo_i, *cuerpo_f,*aux;
	BOOL encontrado = FALSE, ret = TRUE;
    unsigned long pos;

	/* Buscamos la cabecera
     */
	pos = 0;
	aux = pem;
	while (!encontrado && (pos<tamPEM)) {
		while ( ((*aux) != '-') && (pos< tamPEM) ) {
			++aux;
            ++pos;
		}
        if ( strncmp((char *)aux,"-----BEGIN CERTIFICATE-----", 27) == 0 )
			encontrado = TRUE;
		else {
			++aux;
            ++pos;
		}
	}

    if ( !encontrado ) {
		ret = FALSE;
		goto finPEM2DER;
	}


    cuerpo_i = aux + 28; /* del begin certificate y el cambio de l�nea */

   /*
	* Ahora eliminamos el pie
	*/

	aux = cuerpo_i;
    encontrado = FALSE;
    while ( !encontrado && (pos < tamPEM)) {
		while ( (*aux != '-') && (pos < tamPEM)) {
			++aux;
            ++pos;
		}
        if ( strncmp((char *)aux,"-----END CERTIFICATE-----", 25) == 0 ) {
			encontrado = TRUE;
		} else {
			++pos;
            ++aux;
		}
	}


    if (!encontrado) {
		ret = FALSE;
		goto finPEM2DER;
	}

	cuerpo_f = aux-1;

    B64_Decodificar(cuerpo_i, cuerpo_f-cuerpo_i+1,NULL,tamDER);

    if ( !der ) {
		ret = TRUE;
		goto finPEM2DER;
	}

	B64_Decodificar(cuerpo_i, cuerpo_f-cuerpo_i+1,der,tamDER);

finPEM2DER:


	return ret;

}


/*! \brief Crea un contexto de certificado para CAPI a partir de un certificado en
 *		   formato DER.
 *
 * Crea un contexto de certificado para CAPI a partir de un certificado en
 * formato DER.
 *
 * \param certDER
 *		  Certificado en formato DER.
 *
 * \param tamCert
 *		  Tama�o del certificado en formato DER.
 *
 */

PCCERT_CONTEXT CERT_GetCertificateContext (BYTE *certDER, DWORD tamCert)
{
	PCCERT_CONTEXT certSalida = NULL;

	certSalida = CertCreateCertificateContext(X509_ASN_ENCODING,certDER,tamCert);

	return certSalida;
}





/* \brief Calcula el identificados de un certificado.
 *
 * Calcula el identificador de un certificado. El m�todo es el mismo que el realizado para las llaves privadas
 * y es el siguiente:
 *
 *     -# Obtener llave p�blica del certificado
 *     -# Obtener m�dulo de la llave p�blica (n)
 *     -# Obtener exponente p�blico de la llave p�blica (e)
 *     -# Pasar a little endian n: nl
 *     -# Pasar a little endian e: el
 *     -# Concatenar nl y el: nl+el
 *     -# Devolver SHA1(nl+el)
 *
 * \param cert
 *		  ENTRADA. Contiene el certificado.
 *
 * \param id
 *		  SALIDA. El identificador calculado.
 *
 * \relval TRUE
 *		   Ok
 *
 * \retval FALSE
 *		   Error
 */


BOOL CL_CalculateID ( PCCERT_CONTEXT certCtx, unsigned char id[20] ) 
{

  HCRYPTPROV hProv = 0;
  HCRYPTKEY hKey = 0;
  HCRYPTHASH hHash = 0;
  BOOL ret = TRUE;

  BYTE *blob;
  DWORD dwBlobSize, dwIdSize;

  if ( ! certCtx )
    return FALSE;

  if ( ! CryptAcquireContext(&hProv, NULL, "Microsoft Enhanced Cryptographic Provider v1.0", PROV_RSA_FULL, CRYPT_VERIFYCONTEXT) ) {
    ret = FALSE;
    goto endCL_CalculateID;
  }

  if ( ! CryptImportPublicKeyInfo( hProv, X509_ASN_ENCODING, &(certCtx->pCertInfo->SubjectPublicKeyInfo), &hKey ) ) {
    ret = FALSE;
    goto endCL_CalculateID;
  }

  if ( ! CryptExportKey(hKey, 0, PUBLICKEYBLOB, 0, NULL, &dwBlobSize) ) {
    ret = FALSE;
    goto endCL_CalculateID;
  }

  blob = new BYTE[dwBlobSize];
  if ( ! blob ) {
    ret = FALSE;
    goto endCL_CalculateID;
  }
  if ( ! CryptExportKey(hKey, 0, PUBLICKEYBLOB, 0, blob, &dwBlobSize) ) {
    ret = FALSE;
    goto endCL_CalculateID;
  }

  CryptDestroyKey(hKey);
  hKey = 0;

  if ( ! CryptCreateHash(hProv, CALG_SHA1, 0, 0, &hHash) ) {
    ret = FALSE;
    goto endCL_CalculateID;
  }

  if ( ! CryptHashData(hHash, 
		       blob+sizeof(PUBLICKEYSTRUC)+sizeof(RSAPUBKEY),
		       ((RSAPUBKEY *) (blob+sizeof(PUBLICKEYSTRUC)))->bitlen / 8,
		       0) ) 
    {
      ret = FALSE;
      goto endCL_CalculateID;
    }

  if ( ! CryptHashData(hHash,
		       (BYTE *) &(((RSAPUBKEY *)(blob + sizeof(PUBLICKEYSTRUC)))->pubexp),
		       4,
		       0 ) )
  {
    ret = FALSE;
    goto endCL_CalculateID;
  }

  dwIdSize = 20;  
  if ( !CryptGetHashParam(hHash, HP_HASHVAL, id, &dwIdSize, 0) ) {
    ret = FALSE;
    goto endCL_CalculateID;
  }

  
 endCL_CalculateID:

  if ( hHash ) 
    CryptDestroyHash(hHash);
  if ( hKey )
    CryptDestroyKey(hKey);
  if ( hProv )
    CryptReleaseContext(hProv, 0);
  if ( blob )
    delete [] blob;


  return ret;

}













/*! \brief Callback para determinar si existe el Address Book. De la funci�n CertEnumSystemStore()
 *
 * Callback para determinar si existe el Address Book. De la funci�n CertEnumSystemStore(). Para saber
 * los par�metros ver documentaci�n del msdn:
 *
 * http://msdn.microsoft.com/library/default.asp?url=/library/en-us/seccrypto/security/certenumsystemstorecallback.asp
 *
 */

BOOL WINAPI CertEnumSystemStoreCallback(
  const void* pvSystemStore,
  DWORD dwFlags,
  PCERT_SYSTEM_STORE_INFO pStoreInfo,
  void* pvReserved,
  void* pvArg
)
{

	if ( lstrcmpW(L"AddressBook", (const wchar_t *) pvSystemStore) == 0 )
		*((BOOL *)pvArg) = TRUE;
	
	return TRUE;
}


/* Comprueba si existe el store Address Book
 */

BOOL ExisteAddressBook (void)
{
	BOOL salida = FALSE;

	if ( ! CertEnumSystemStore(CERT_SYSTEM_STORE_CURRENT_USER,NULL,(void *) &salida,CertEnumSystemStoreCallback) )
		salida = FALSE;

	return salida;
}


/*! \brief Carga los certificados de otras personas si el AddressBook existe
 *
 * Carga los certificados de otras personas si el AddressBook existe. 
 *
 * \param usbHandle
 *		  Handle al runtime
 *
 * \retval TRUE
 *		   Ok
 *
 * \retval FALSE
 *		   Error
 */

BOOL CargarAddressBook (HCERTSTORE store, USBCERTS_HANDLE *usbHandle)
{

	long nBloque;
	BYTE bloque[TAM_BLOQUE], *der = NULL;
	unsigned long tamDer;
	PCCERT_CONTEXT ctxCert = NULL;

	BOOL ret= TRUE;

#ifdef _DEBUG
	log.EnterFunc("CargarAddressBook");
#endif

	/* Si no existe el store others, pasamos ol�mpicamente */

/*	if ( ! ExisteAddressBook()) {
		ret = TRUE;
		goto finCargarAddressBook;
	}
*/
	/* Sacamos los bloques del stick */

	if ( LIBRT_LeerTipoBloqueCrypto(usbHandle, BLOQUE_CERT_OTROS, TRUE, bloque, &nBloque) != 0 ) {
		ret = FALSE;
		goto finCargarAddressBook;
	}

	while ( nBloque != -1 ) {
				
		if ( ! PEM2DER(BLOQUE_CERTOTROS_Get_Objeto(bloque),
			 	       BLOQUE_CERTOTROS_Get_Tam(bloque),
					   NULL, &tamDer) ) 
		{
			ret = FALSE;
			goto finCargarAddressBook;
		}

		der = new BYTE[tamDer];

		if ( ! der ) {
			ret = FALSE;
			goto finCargarAddressBook;
		}

		if ( ! PEM2DER(BLOQUE_CERTOTROS_Get_Objeto(bloque),
			 	       BLOQUE_CERTOTROS_Get_Tam(bloque),
					   der, &tamDer) ) 
		{	
			ret = FALSE;
			goto finCargarAddressBook;
		}

		ctxCert = CERT_GetCertificateContext (der, tamDer);

		if ( ! ctxCert ) {
			ret = FALSE;
			goto finCargarAddressBook;
		}

		SecureZeroMemory(der, tamDer);
		delete [] der;
		der = NULL;
		tamDer = 0;

		if ( ! CertAddCertificateContextToStore(store, ctxCert,CERT_STORE_ADD_REPLACE_EXISTING,NULL) ) {
			ret = FALSE;
			goto finCargarAddressBook;
		}


		if ( LIBRT_LeerTipoBloqueCrypto(usbHandle, BLOQUE_CERT_OTROS, FALSE, bloque, &nBloque) != 0 ) {
			ret = FALSE;
			goto finCargarAddressBook;
		}

	}



finCargarAddressBook:

	SecureZeroMemory(bloque, TAM_BLOQUE);

	/* Esta limpieza debe ir antes del CertCloseStore. No entiendo muy bien por qu�
	 * pero si no lo hago as� el store se queda colgado
	 */

	if ( der ) {
		SecureZeroMemory(der, tamDer);
		delete [] der;
		der = NULL;
		tamDer = 0;
	}


#ifdef _DEBUG
	if ( ret == FALSE )
		log.ExitFunc("ERROR");
	else
		log.ExitFunc();
#endif

	return ret;	

}





/*! \brief Carga los certificados con llave privada asociada
 *
 * Carga los certificados con llave privada asociada.
 *
 * \param hCertStore
 *		  El store recibido por CertDllOpenStoreProv()
 *
 * \param usbHandle
 *		  Handle al runtime
 *
 * \retval FALSE
 *		   Error
 *
 * \retval TRUE
 *		   Ok
 *
 */

BOOL CargarMY (HCERTSTORE hCertStore, USBCERTS_HANDLE *usbHandle)
{
	CRYPT_KEY_PROV_INFO infoCsp;

	BYTE *bloquesCert = NULL, *bloquesContainer, *idCert;
	long numBloquesCert, numBloquesContainer, *h;
	long k,i;
	unsigned int tamContainer,j;

	int kk = 0;
	
	INFO_KEY_CONTAINER *ikc;
	BOOL ret = TRUE;;
	BOOL encExchange, encSignature;

	LPWSTR wszCSPName;
	int nDispositivos=0;

#ifdef _DEBUG
	log.EnterFunc("CargarMY");
	log << "COMIENZA LOGEO" << endl; 
#endif

	if ( LIBRT_LeerTodosBloquesTipo (usbHandle, BLOQUE_CERT_PROPIO, NULL, NULL, &numBloquesCert) != 0 ) {
		ret = FALSE;
		goto finCargarMY;
	}

	bloquesCert = new BYTE [TAM_BLOQUE * numBloquesCert];
	if ( !bloquesCert ) {
		ret = FALSE;

#ifdef _DEBUG
		log << __LINE__ << "No hay bloque cert" << endl;
#endif
		goto finCargarMY;

	}

	h = new long[numBloquesCert];

	if  ( ! h ) {
		delete [] bloquesCert;
		ret = FALSE;
#ifdef _DEBUG
		log << __LINE__ << "::No hay memoria" <<endl;
#endif	
		goto finCargarMY;
	}

	if ( LIBRT_LeerTodosBloquesTipo (usbHandle, BLOQUE_CERT_PROPIO, h, bloquesCert, &numBloquesCert) != 0 )  {
		delete [] h;
		delete [] bloquesCert;
		ret = FALSE;
#ifdef _DEBUG
		log << __LINE__ << ":: Error leyendo todos bloques tipo crypto " <<endl;
#endif	
		goto finCargarMY;
	}

#ifdef _DEBUG
		log << __LINE__ << ":: Leidos " << numBloquesCert << " bloques " <<endl;
#endif	

	delete [] h;
	h = NULL;

	if ( LIBRT_LeerTodosBloquesTipo (usbHandle, BLOQUE_KEY_CONTAINERS, NULL, NULL, &numBloquesContainer) != 0 ) {
		delete [] bloquesCert;
		ret = FALSE;
#ifdef _DEBUG
		log << __LINE__ << ":: Error al leer keycontainers" <<endl;
#endif	
		goto finCargarMY;
	}

	bloquesContainer = new BYTE[TAM_BLOQUE*numBloquesContainer];
	if ( !bloquesContainer ) {
		delete [] bloquesCert;
		ret = FALSE;
#ifdef _DEBUG
		log << __LINE__ << ":: ERROR No hay memoria" <<endl;
#endif	
		goto finCargarMY;
	}

	h = new long[numBloquesContainer];

	if ( !h ) {
		delete [] bloquesCert;
		delete [] bloquesContainer;
		ret = FALSE;
#ifdef _DEBUG
		log << __LINE__ << ":: ERROR No hay memoria" <<endl;
#endif	
		goto finCargarMY;
	}

	if ( LIBRT_LeerTodosBloquesTipo (usbHandle, BLOQUE_KEY_CONTAINERS, h, bloquesContainer, &numBloquesContainer) != 0 ) {
		delete [] bloquesCert;
		delete [] bloquesContainer;
		delete [] h;
		ret = FALSE;
#ifdef _DEBUG
		log << __LINE__ << ":: ERROR leer keyContainers" <<endl;
#endif	
		goto finCargarMY;
	}


#ifdef _DEBUG
		log << __LINE__  << ":: Leidos " << numBloquesContainer << " bloques container." <<endl;
#endif	

	delete [] h;
	h = NULL;

	/*
	 * Para cada key container obtengo los id de las llaves que contiene,
	 * busco su certificado asociado y lo inserto en el store
	 */

	wszCSPName = new WCHAR [ ::strlen("UJI Clauer CSP") + 1 ];
	if ( ! wszCSPName ) {
		delete [] bloquesCert;
		delete [] bloquesContainer;
		ret = FALSE;
		goto finCargarMY;
	}

	if ( ! MultiByteToWideChar(CP_ACP, MB_PRECOMPOSED, "UJI Clauer CSP", -1, wszCSPName, ::strlen("UJI Clauer CSP") + 1) ) {
			delete [] bloquesCert;
			delete [] bloquesContainer;
			delete [] wszCSPName;
			ret = FALSE;
			goto finCargarMY;
	}

	for ( i = 0 ; i < numBloquesContainer ; i++ ) {

		BLOQUE_KeyContainer_Enumerar(bloquesContainer+i*TAM_BLOQUE,NULL,&tamContainer);
		ikc = new INFO_KEY_CONTAINER [tamContainer];
		if ( !ikc ) {
			delete [] bloquesCert;
			delete [] bloquesContainer;
			delete [] wszCSPName;
			ret = FALSE;

#ifdef _DEBUG 	
			log << __LINE__ << ":: Enumerando containers." <<endl;
#endif 
			goto finCargarMY;
		}

		BLOQUE_KeyContainer_Enumerar(bloquesContainer+i*TAM_BLOQUE,ikc,&tamContainer);

		for ( j = 0 ; j < tamContainer ; j++ ) {

		   /* Busco el id en los certificados
		    */

		  encSignature = FALSE;
			encExchange = FALSE;
			k = 0;

			while ( !encExchange && !encSignature && (k < numBloquesCert) ) {

				idCert = BLOQUE_CERTPROPIO_Get_Id(bloquesCert+k*TAM_BLOQUE);

				if ( memcmp(idCert, ikc[j].idExchange, 20) == 0 ) {
					encExchange = TRUE;
				} else if ( memcmp(idCert, ikc[j].idSignature, 20) == 0 ) {
					encSignature = TRUE;
				} else
					k++;
			}

			/*
			 * Si encontrado, entonces lo inserto en el store
			 */

			if ( encExchange || encSignature ) {

				BYTE *certBuf,*certDER;
				unsigned long tamDER;
				long tamCert;
				PCCERT_CONTEXT cert;
				LPWSTR wszContainer;
				char *friendlyName;

				certBuf = BLOQUE_CERTPROPIO_Get_Objeto(bloquesCert + k*TAM_BLOQUE);
				tamCert = BLOQUE_CERTPROPIO_Get_Tam(bloquesCert + k*TAM_BLOQUE);

				if ( !PEM2DER(certBuf, tamCert, NULL,&tamDER) ) {

					delete [] ikc;
					delete [] bloquesCert;
					delete [] bloquesContainer;
					delete [] wszCSPName;

					ret = FALSE;
#ifdef _DEBUG 	
					log << __LINE__ << ":: ERROR pasando de PEM A DER." <<endl;
#endif 

					goto finCargarMY;
				}

				certDER = new BYTE[tamDER];
				if ( !certDER ) {


					delete [] ikc;
					delete [] bloquesCert;
					delete [] bloquesContainer;
					delete [] wszCSPName;

					ret = FALSE;
#ifdef _DEBUG 	
					log << __LINE__ << ":: ERROR No memoria. " <<endl;
#endif 

					goto finCargarMY;

				}

				if (!PEM2DER(certBuf, tamCert, certDER, &tamDER)) {

					delete [] ikc;
					delete [] bloquesCert;
					delete [] bloquesContainer;
					delete [] wszCSPName;
					delete [] certDER;

					ret = FALSE;
#ifdef _DEBUG 	
					log << __LINE__ << ":: ERROR pen2der." <<endl;
#endif 

					goto finCargarMY;
				}

				cert = CERT_GetCertificateContext (certDER, tamDER);
				if ( !cert ) {

					delete [] ikc;
					delete [] bloquesCert;
					delete [] bloquesContainer;
					delete [] wszCSPName;
					delete [] certDER;

					ret = FALSE;
#ifdef _DEBUG 	
					log << __LINE__ << ":: ERROR CERT_GetCertificateContext." <<endl;
#endif 

					goto finCargarMY;
				}

				friendlyName = BLOQUE_CERTPROPIO_Get_FriendlyName(bloquesCert + k*TAM_BLOQUE);

				if ( friendlyName ) {

					CRYPT_DATA_BLOB blobFriendly;
					WCHAR *wsFriendlyName;
					int tamWsFriendlyName;

					char auxFriendlyName[31];

					memset(auxFriendlyName, 0, sizeof auxFriendlyName);
					if ( *friendlyName ) {
						strcpy(auxFriendlyName, "CLAUER_");
						strncpy(auxFriendlyName+7, friendlyName, 31-7);
						auxFriendlyName[30] = 0;
					} else {
						strcpy(auxFriendlyName, "CLAUER");
					}
					
					tamWsFriendlyName = MultiByteToWideChar(CP_ACP, 0, auxFriendlyName, -1, NULL, 0);
					if ( tamWsFriendlyName > 0 ) {

						wsFriendlyName = new WCHAR[tamWsFriendlyName];

						if ( wsFriendlyName ) {
							if ( MultiByteToWideChar(CP_ACP, 0, auxFriendlyName, -1, wsFriendlyName, tamWsFriendlyName) > 0 ) {

								blobFriendly.cbData = tamWsFriendlyName*sizeof(WCHAR);
								blobFriendly.pbData = (unsigned char *) wsFriendlyName;

								CertSetCertificateContextProperty(cert, CERT_FRIENDLY_NAME_PROP_ID, 0, &blobFriendly);
							}

							delete [] wsFriendlyName;

						}

					}
				}

				wszContainer = new WCHAR [ strlen(ikc[j].nombreKeyContainer) + 1 ];
				if ( ! wszContainer ) {

					delete [] ikc;
					delete [] bloquesCert;
					delete [] bloquesContainer;
					delete [] wszCSPName;
					delete [] certDER;

					ret = FALSE;

#ifdef _DEBUG 	
					log << __LINE__ << ":: ERROR No memoria." <<endl;
#endif 

					goto finCargarMY;
				}
				
				if ( ! MultiByteToWideChar(CP_ACP, MB_PRECOMPOSED, ikc[j].nombreKeyContainer, -1, wszContainer, strlen(ikc[j].nombreKeyContainer) + 1 ) ) {
					delete [] ikc;
					delete [] bloquesCert;
					delete [] bloquesContainer;
					delete [] wszCSPName;
					delete [] certDER;

					ret = FALSE;
					goto finCargarMY;
				}

				infoCsp.pwszContainerName = wszContainer;
				infoCsp.pwszProvName      = wszCSPName;
				infoCsp.dwProvType        = PROV_RSA_FULL;
				infoCsp.dwFlags           = 0;
				infoCsp.cProvParam        = 0;
				infoCsp.rgProvParam       = NULL;

				if ( encSignature ) 
					infoCsp.dwKeySpec = AT_SIGNATURE;
				else
					infoCsp.dwKeySpec = AT_KEYEXCHANGE;

				if ( ! CertSetCertificateContextProperty(cert, CERT_KEY_PROV_INFO_PROP_ID, 0, &infoCsp) ) {

					delete [] ikc;
					delete [] bloquesCert;
					delete [] bloquesContainer;
					delete [] wszCSPName;
					delete [] certDER;
					delete [] wszContainer;

					ret = FALSE;

#ifdef _DEBUG 	
					log << __LINE__ << ":: ERROR Certificatecontextproperty ." <<endl;
#endif 

					goto finCargarMY;
				}

				if ( ! CertAddCertificateContextToStore(hCertStore, cert, CERT_STORE_ADD_NEWER, NULL) ) {


					delete [] ikc;
					delete [] bloquesCert;
					delete [] bloquesContainer;
					delete [] wszCSPName;
					delete [] certDER;	
					delete [] wszContainer;

					CertFreeCertificateContext(cert);

					ret = FALSE;
#ifdef _DEBUG 	
					log << __LINE__ << ":: ERROR CertAddCertificateContextToStore." <<endl;
#endif 

					goto finCargarMY;
				}

				CertFreeCertificateContext(cert);
				delete [] wszContainer;

			}
		}
	}

	delete [] ikc;
	delete [] bloquesCert;
	delete [] bloquesContainer;
	delete [] wszCSPName;
#ifdef _DEBUG 	
	log << __LINE__ << ":: Salimos cargar MY." <<endl;
#endif 

finCargarMY:

#ifdef _DEBUG
	if ( ret == FALSE )
		log.ExitFunc("ERROR");
	else
		log.ExitFunc();
		log << endl << endl << endl;
#endif

	return ret;
}




/*! \brief Carga las CAs
 *
 * Carga las CAs.
 *
 * \param usbHandle
 *        Handle al dispositivo USB
 *
 * \retval TRUE
 *	 	   Ok
 *
 * \retval FALSE
 *		   Error
 */

BOOL CargarCA (HCERTSTORE store, USBCERTS_HANDLE *usbHandle)
{
	BYTE bloque[TAM_BLOQUE], *der = NULL;
	long nBloque;
	BOOL ok = TRUE;
	DWORD tamDER;
	PCCERT_CONTEXT ctxCert = NULL;

#ifdef _DEBUG
	log.EnterFunc("CargarCA");
#endif


	/* Leemos el primer certificado intermedio
	 */

	if ( LIBRT_LeerTipoBloqueCrypto(usbHandle, BLOQUE_CERT_INTERMEDIO, TRUE, bloque, &nBloque) != 0 ) {
		ok = FALSE;
		goto finCargarCA;
	}

	if ( nBloque == -1 ) {
		ok = TRUE;
		goto finCargarCA;
	}
	
	do {

		/* Convertimos el certificado a DER
		 */

		if ( !PEM2DER(BLOQUE_CERTINTERMEDIO_Get_Objeto(bloque),
			 		  BLOQUE_CERTINTERMEDIO_Get_Tam(bloque),
					  NULL,
					  &tamDER) ) 
		{
			ok = FALSE;
			goto finCargarCA;
		}


		der = new BYTE [tamDER];

		if  (!der ) 
		{
			ok = FALSE;
			goto finCargarCA;
		}


		if ( !PEM2DER(BLOQUE_CERTINTERMEDIO_Get_Objeto(bloque),
			 		  BLOQUE_CERTINTERMEDIO_Get_Tam(bloque),
					  der,
					  &tamDER) ) 
		{
			ok = FALSE;
			goto finCargarCA;
		}


		/* Creamos un certificate context y lo insertamos en el store
		 */

		ctxCert = CERT_GetCertificateContext(der, tamDER);
		if ( !ctxCert ) {
			ok = FALSE;
			goto finCargarCA;
		}

		if ( !CertAddCertificateContextToStore(store, ctxCert, CERT_STORE_ADD_USE_EXISTING, NULL) ) {
			DWORD err;
			err = GetLastError();
			ok = FALSE;
			goto finCargarCA;
		}

		SecureZeroMemory(der, tamDER);
		delete [] der;
		der = NULL;

		/* Leemos el siguiente bloque
		 */

		if ( LIBRT_LeerTipoBloqueCrypto(usbHandle, BLOQUE_CERT_INTERMEDIO, FALSE, bloque, &nBloque) != 0 ) {
			ok = FALSE;
			goto finCargarCA;
		}

		
	} while ( nBloque != -1 );

finCargarCA:

	SecureZeroMemory(bloque, TAM_BLOQUE);
	nBloque = -1;

	if ( der ) {
		SecureZeroMemory(der, tamDER);
		delete [] der;
		der = NULL;
		tamDER = 0;
	}

#ifdef _DEBUG
	if ( ok )
		log.ExitFunc();
	else
		log.ExitFunc("ERROR");
#endif
	
	return ok;
}





/*! \brief Carga las CAs
 *
 * Carga las CAs.
 *
 * \param usbHandle
 *        Handle al dispositivo USB
 *
 * \retval TRUE
 *	 	   Ok
 *
 * \retval FALSE
 *		   Error
 */

BOOL CargarRoot (USBCERTS_HANDLE *usbHandle)
{
	BYTE bloque[TAM_BLOQUE], *der = NULL;
	long nBloque;
	BOOL ok = TRUE;
	DWORD tamDER;
	PCCERT_CONTEXT ctxCert = NULL;
	HCERTSTORE store = NULL;

#ifdef _DEBUG
	log.EnterFunc("CargarRoot");
#endif

	/* Si store == NULL, entonces abrimos el store root
	 */

	store = CertOpenSystemStore(0, "ROOT");
	if ( ! store ) {
		ok = FALSE;
		goto finCargarRoot;
	}

	/* Leemos el primer certificado raiz
	 */

	if ( LIBRT_LeerTipoBloqueCrypto(usbHandle, BLOQUE_CERT_RAIZ, TRUE, bloque, &nBloque) != 0 ) {
		ok = FALSE;
		goto finCargarRoot;
	}

	if ( nBloque == -1 ) {
		ok = TRUE;
		goto finCargarRoot;
	}
	
	do {

		/* Convertimos el certificado a DER
		 */

		if ( !PEM2DER(BLOQUE_CERTRAIZ_Get_Objeto(bloque),
			 		  BLOQUE_CERTRAIZ_Get_Tam(bloque),
					  NULL,
					  &tamDER) ) 
		{
			ok = FALSE;
			goto finCargarRoot;
		}


		der = new BYTE [tamDER];

		if  (!der ) 
		{
			ok = FALSE;
			goto finCargarRoot;
		}


		if ( !PEM2DER(BLOQUE_CERTRAIZ_Get_Objeto(bloque),
			 		  BLOQUE_CERTRAIZ_Get_Tam(bloque),
					  der,
					  &tamDER) ) 
		{
			ok = FALSE;
			goto finCargarRoot;
		}


		/* Creamos un certificate context y lo insertamos en el store
		 */

		ctxCert = CERT_GetCertificateContext(der, tamDER);
		if ( !ctxCert ) {
			ok = FALSE;
			goto finCargarRoot;
		}

		if ( !CertAddCertificateContextToStore(store, ctxCert, CERT_STORE_ADD_USE_EXISTING, NULL) ) {
			DWORD err;
			err = GetLastError();
			ok = FALSE;
			goto finCargarRoot;
		}

		SecureZeroMemory(der, tamDER);
		delete [] der;
		der = NULL;

		/* Leemos el siguiente bloque
		 */

		if ( LIBRT_LeerTipoBloqueCrypto(usbHandle, BLOQUE_CERT_RAIZ, FALSE, bloque, &nBloque) != 0 ) {
			ok = FALSE;
			goto finCargarRoot;
		}

		
	} while ( nBloque != -1 );

finCargarRoot:

	SecureZeroMemory(bloque, TAM_BLOQUE);
	nBloque = -1;

	if ( der ) {
		SecureZeroMemory(der, tamDER);
		delete [] der;
		der = NULL;
		tamDER = 0;
	}

	if ( store )
		CertCloseStore(store, 0);

#ifdef _DEBUG
	if ( ok )
		log.ExitFunc();
	else
		log.ExitFunc("ERROR");
#endif
	
	return ok;
}


