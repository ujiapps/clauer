#include <windows.h>
#include <wincrypt.h>

#include <stdio.h>


BOOL WINAPI CertEnumSystemStoreCallback(
  const void* pvSystemStore,
  DWORD dwFlags,
  PCERT_SYSTEM_STORE_INFO pStoreInfo,
  void* pvReserved,
  void* pvArg
)
{

	if ( lstrcmpW(L"AddressBook", (const wchar_t *) pvSystemStore) == 0 )
		*((BOOL *)pvArg) = TRUE;
	

	return TRUE;
}




BOOL ExisteAddressBook (void)
{

	BOOL salida = FALSE;

	if (!CertEnumSystemStore(CERT_SYSTEM_STORE_CURRENT_USER,NULL,(void *) &salida,CertEnumSystemStoreCallback) ) {
		printf("ERORR\n");
		return 1;
	}

	return salida;
}




int main (void)
{

if ( ExisteAddressBook )
printf("OK\n");
else
printf("NOL\n");
	return 0;
}
