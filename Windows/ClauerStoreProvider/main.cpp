#include <windows.h>
#include <wincrypt.h>

#include <iostream>
#include <map>
#include <memory>
#include <string>

#ifdef _DEBUG
#include <oolog/oolog.h>
#endif

#include <LIBRT/LIBRT.h>
#include "utilstore.h"
#include "callbacks.h"

using namespace std;

// Some macros

#define CLAUERSTORE_OID	"ClauerStoreProvider"				// The store OID
#define FICHEROLOG		"C:\\ClauerStoreProvider.log"		// The log file

#define PARAM_MY		0
#define PARAM_OTHERS	1
#define PARAM_CA		2
#define PARAM_ROOT		3

#ifdef _DEBUG
COOLOG log;
BOOL logOpened = FALSE;
#endif


HINSTANCE hDll;


BOOL WINAPI DllMain(
    HINSTANCE hinstDLL,  // handle to DLL module
    DWORD fdwReason,     // reason for calling function
    LPVOID lpReserved )  // reserved
{

#ifdef _DEBUG
	/* Abrimos el log en C:\*/
	log.Open("C:\\Store.log");
	log << "Registrado o cargado ";
	logOpened = TRUE;
#endif
		
    switch( fdwReason ) 
    { 
        case DLL_PROCESS_ATTACH:

			hDll = hinstDLL;
			DisableThreadLibraryCalls(hinstDLL);
			LIBRT_Ini();
            break;

		case DLL_PROCESS_DETACH:
			LIBRT_Fin();
			break;

    }
    return TRUE;
}


/////////////////////////////////////////////////////////////////////////////////
// CertDllOpenStoreProv

BOOL WINAPI CertDllOpenStoreProv (IN LPCSTR lpszStoreProvider,
						      IN DWORD dwEncodingType,
							  IN HCRYPTPROV hCryptProv,
							  IN DWORD dwFlags,
							  IN const void *pvPara,
							  IN HCERTSTORE hCertStore,
							  IN OUT PCERT_STORE_PROV_INFO pStoreProvInfo)
{
	BYTE *dispositivos[MAX_DEVICES];
	BOOL ret = TRUE, dispositivoIniciado = FALSE;
	USBCERTS_HANDLE *usbHandle = NULL;
	int nDispositivos=0, i;

	DWORD dwStoreToOpen;

#ifdef _DEBUG
	log.EnterFunc("CertDllOpenStoreProv");
#endif 

	memset(dispositivos, 0, MAX_DEVICES * sizeof(char *));

	if ( ! LIBRT_HayRuntime() ) {
		ret = TRUE;

#ifdef  _DEBUG 	
		log << __LINE__ << "ERROR Hay runtime dice que nanai" << endl;
#endif 
		goto finCertDllOpenStoreProv;
	}

	if ( ! pvPara ) {
		ret = TRUE;

#ifdef  _DEBUG 	
		log << __LINE__ << "Salimos por pvPara" << endl;
#endif 

		goto finCertDllOpenStoreProv;
	}
	dwStoreToOpen = *(( DWORD * ) pvPara);

	/* Si hay m�s de un dispositivo insertado, me quedo con el primero de ellos
	 */
	int err;
	if ( (err= LIBRT_ListarDispositivos(&nDispositivos, dispositivos)) != 0 ) {
		ret = TRUE;

#ifdef  _DEBUG 	
		log << __LINE__ << "ERROR  Listando devices error= " << err << endl;
#endif 

		goto finCertDllOpenStoreProv;
	}

	if ( nDispositivos == 0 )  {
		ret = TRUE;
#ifdef  _DEBUG 	
		log << __LINE__ << "ERROR No hay dispositivos" << endl;
#endif 

		goto finCertDllOpenStoreProv;
	}


	usbHandle = new USBCERTS_HANDLE;

	if ( !usbHandle ) {
		ret = TRUE;
#ifdef  _DEBUG 	
		log << __LINE__ << "ERROR No podemos conseguir el handle" << endl;
#endif 

		goto finCertDllOpenStoreProv;
	}

	if ( LIBRT_IniciarDispositivo(dispositivos[0], NULL, usbHandle) != 0 ) {
		ret = TRUE;
#ifdef  _DEBUG 	
		log << __LINE__ << "ERROR Iniciando el device" << endl;
#endif 

		goto finCertDllOpenStoreProv;
	}

	dispositivoIniciado = TRUE;
	
	/* Cargamos el store root en local. Esto es una minichapucilla
	 * pero ahora mismo es la �nica forma de poder lidiar con el
	 * tema ya que no podemos enganchar un store f�sico en el root
	 */

	CargarRoot(usbHandle);

	/* Cargamos las CAs si podemos
	 */

	if ( dwStoreToOpen == PARAM_CA ) {
		CargarCA (hCertStore, usbHandle);
/*	} else if ( dwStoreToOpen == PARAM_ROOT ) {
		CargarRoot (hCertStore, usbHandle);*/
	} else if ( dwStoreToOpen == PARAM_OTHERS ) {
		CargarAddressBook(hCertStore, usbHandle);
	} else if ( dwStoreToOpen == PARAM_MY ) {
		if ( !CargarMY(hCertStore, usbHandle) ) {
			ret = TRUE;

#ifdef  _DEBUG 	
		log << __LINE__ << "PARAM MY" << endl;
#endif 

			goto finCertDllOpenStoreProv;
		}
	}


finCertDllOpenStoreProv:

	/* Inicializamos el vector de callbacks 
	 */

	if ( dwStoreToOpen == PARAM_MY ) {
		INFO_CALLBACKS *ic = (INFO_CALLBACKS *) malloc ( sizeof(INFO_CALLBACKS) );
		if ( ic ) {
			ic->hStore = hCertStore;
			if ( nDispositivos > 0 ) {
				strncpy(ic->szDevice, (char *) dispositivos[0], MAX_PATH_LEN);
				ic->szDevice[MAX_PATH_LEN-1] = 0;
			} else {
				*(ic->szDevice) = 0;
			}

			pStoreProvInfo->hStoreProv        = (HCERTSTOREPROV) ic;	
			pStoreProvInfo->cStoreProvFunc    = 3;

			pStoreProvInfo->rgpvStoreProvFunc = (void **) malloc (sizeof(void *)*(pStoreProvInfo->cStoreProvFunc));
			for ( i = 0 ; i < (pStoreProvInfo->cStoreProvFunc) ; i++ ) 
				pStoreProvInfo->rgpvStoreProvFunc[i] = NULL;
			pStoreProvInfo->rgpvStoreProvFunc[2] = CertStoreProvWriteCertCallback;
		}
	}

	for ( i = 0 ; i < MAX_DEVICES ; i++ )
		if ( dispositivos[i] )
			free(dispositivos[i]);
		else
			break;

	if ( usbHandle ) {
		if ( dispositivoIniciado ) {
			LIBRT_FinalizarDispositivo(usbHandle);
			delete usbHandle;
			usbHandle = NULL;
		}
	}

	return ret;
}





//  The DllRegisterServer Entry Point
STDAPI DllRegisterServer(void)
{
	// Declare and initialize variables.

	LPCWSTR pvSystemName = L"ClauerStore"; 
	CERT_PHYSICAL_STORE_INFO PhysicalStoreInfo;
	DWORD dwStoreToRegister;

	/*
	 * Registramos la nueva funcionalidad
	 */

	if( ! CryptRegisterOIDFunction(
		0,				                    
		CRYPT_OID_OPEN_STORE_PROV_FUNC,     
		CLAUERSTORE_OID,					  
		L"ClauerStoreProvider.dll",            
		NULL				                
		))                                 
	{
		return E_FAIL;
	}

	// Registramos el store MY

	dwStoreToRegister = PARAM_MY;

	memset(&PhysicalStoreInfo, 0, sizeof(CERT_PHYSICAL_STORE_INFO));
	
	PhysicalStoreInfo.cbSize                  = sizeof(CERT_PHYSICAL_STORE_INFO);
	PhysicalStoreInfo.pszOpenStoreProvider    = (LPSTR)CLAUERSTORE_OID;
	PhysicalStoreInfo.dwFlags                 = CERT_PHYSICAL_STORE_REMOTE_OPEN_DISABLE_FLAG;
	PhysicalStoreInfo.dwFlags                |= CERT_PHYSICAL_STORE_ADD_ENABLE_FLAG;
	PhysicalStoreInfo.OpenParameters.pbData   = (LPBYTE) &dwStoreToRegister;
	PhysicalStoreInfo.OpenParameters.cbData   = sizeof(DWORD);
	PhysicalStoreInfo.dwPriority              = 1;
	PhysicalStoreInfo.dwOpenEncodingType      = 0;

	if(!CertRegisterPhysicalStore(
		L"MY",
		CERT_SYSTEM_STORE_CURRENT_USER,
		L"ClauerStore",
		&PhysicalStoreInfo,
		NULL
		))
	{
		return E_FAIL;
	}

	/* Registramos los stores AddressBook, CA y Root
	 */

	LPWSTR stores[] = { L"MY", L"AddressBook", L"CA", L"Root" };

	for ( DWORD dwStoreToRegister = 1 ; dwStoreToRegister < 3 ; dwStoreToRegister++ ) {

		memset(&PhysicalStoreInfo, 0, sizeof(CERT_PHYSICAL_STORE_INFO));
		
		PhysicalStoreInfo.cbSize                  = sizeof(CERT_PHYSICAL_STORE_INFO);
		PhysicalStoreInfo.pszOpenStoreProvider    = (LPSTR)CLAUERSTORE_OID;
		PhysicalStoreInfo.dwFlags                 = CERT_PHYSICAL_STORE_REMOTE_OPEN_DISABLE_FLAG;
		PhysicalStoreInfo.OpenParameters.pbData   = (LPBYTE) &dwStoreToRegister;
		PhysicalStoreInfo.OpenParameters.cbData   = sizeof(DWORD);
		PhysicalStoreInfo.dwPriority              = 1;
		PhysicalStoreInfo.dwOpenEncodingType      = 0;

		if(!CertRegisterPhysicalStore(
			stores[dwStoreToRegister],
			CERT_SYSTEM_STORE_CURRENT_USER,
			L"ClauerStore",
			&PhysicalStoreInfo,
			NULL
			))
		{
			return E_FAIL;
		}
	}



	return S_OK;
}



// The DllUnregisterServer Entry Point

STDAPI DllUnregisterServer(void)
{
	HRESULT     hr = S_OK;

	if( ! CryptUnregisterOIDFunction(
		0,								 // Encoding type
		CRYPT_OID_OPEN_STORE_PROV_FUNC,  // Function name
		CLAUERSTORE_OID					 // OID
		))
	{
		if(ERROR_FILE_NOT_FOUND != GetLastError())
           hr = E_FAIL;
	}

	LPWSTR stores[] = { L"MY", L"AddressBook", L"CA", L"Root" };

	for ( int i = 0 ; i < 3 ; i++ ) {
		if ( ! CertUnregisterPhysicalStore(stores[i],
						  CERT_SYSTEM_STORE_CURRENT_USER, 
						  L"ClauerStore") )
		{
			return E_FAIL;
		}
	}


	return hr;
}
