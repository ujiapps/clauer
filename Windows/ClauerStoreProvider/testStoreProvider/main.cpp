#include <windows.h>
#include <wincrypt.h>

#include <iostream>

#include <b64/b64.h>
#include <LIBRT/LIBRT.h>

#include <string>

using namespace std;


BOOL WINAPI CertDllOpenStoreProv(LPCSTR, DWORD, HCRYPTPROV, DWORD, const void *, HCERTSTORE, PCERT_STORE_PROV_INFO);



void EnumCertificates (HCERTSTORE hStore)
{

	PCCERT_CONTEXT lpCert = NULL;
	TCHAR nombre[1000];


	while ( lpCert = CertEnumCertificatesInStore(hStore, lpCert) ) {

		if ( ! CertGetNameString(lpCert, CERT_NAME_SIMPLE_DISPLAY_TYPE, 0, NULL, nombre, 999) ) {
			cerr << "Error obteniendo subject del certificado" << endl;
			return;
		}

		cout << nombre << endl;


	}


}



void main ()
{
	HCERTSTORE hStore;
	HANDLE hEvent;
	DWORD causa;
	
	
	hEvent = CreateEvent(NULL, FALSE, FALSE, NULL);

	if ( ! hEvent ) {
		cerr << "ERROR CREANDO EVENTO" << endl;
		return;
	}

	hStore = CertOpenSystemStore(0, "MY");

	do {

		EnumCertificates(hStore);

		if ( ! CertControlStore(hStore,0,CERT_STORE_CTRL_NOTIFY_CHANGE, &hEvent) ) {
			cerr << "ERROR" << endl;
			return;
		}

		causa = WaitForSingleObject(hEvent, 10000);

		if ( causa == WAIT_TIMEOUT ) {
			printf("TIMEOUT\n");
			return;
		}

		if ( ! CertControlStore(hStore, 0, CERT_STORE_CTRL_RESYNC, &hEvent) ) {
			cerr << "ERROR 2" << endl;
			return;
		}

		cout << "-------------------------------------------------------" << endl;

	} while ( 1);

/*	int i;
	cout << "pulsa enter" << endl;
	cin >> i;
*/



	CertCloseStore(hStore, 0);
}
