#include <windows.h>
#include "iu.h"

#include "resource.h"


#include <stdio.h>


static HWND hWndForeground = NULL;

static char *gNombrePrograma = NULL;
static char *gSetup = NULL;
static int gNMensaje = 1;



BOOL IU_CrearVentana (HINSTANCE hDll, HWND *hVentana)
{

    WNDCLASS wc;
	WNDCLASSEX info;


	if (!GetClassInfoEx(hDll, "clauerGUI",&info)) {
		wc.style         = CS_HREDRAW | CS_VREDRAW;
		wc.lpfnWndProc   = DefWindowProc;
		wc.hInstance     = hDll;
		wc.lpszClassName = "clauerGUI";
		wc.lpszMenuName  = NULL;
		wc.hbrBackground = (HBRUSH)( COLOR_WINDOW + 1 );
		wc.hIcon         = LoadIcon( hDll, MAKEINTRESOURCE( IDI_ICON1 ) );
		wc.hCursor       = LoadCursor( NULL, IDC_ARROW );
		wc.cbClsExtra    = wc.cbWndExtra = 0;
	
		if ( !RegisterClassA(&wc) ) {
			return FALSE;
		}
	}

	*hVentana = CreateWindowEx(WS_EX_APPWINDOW | WS_EX_TOPMOST,
                              "clauerGUI",
                              "Clauer User Interface",
                              WS_MINIMIZEBOX |
                              WS_SIZEBOX |
                              WS_CAPTION |
                              WS_MAXIMIZEBOX |
                              WS_POPUP |
                              WS_SYSMENU,
                              0, 0,
                              640, 480,
                              NULL, NULL, hDll, NULL );

	if ( ! *hVentana ) 
		return FALSE;
	

	return TRUE;

}







BOOL WINAPI PINDlgProc (HWND hDlg, UINT msg, WPARAM wParam, LPARAM lParam)
{

	HWND hDesktop;
	RECT dimDesktop, dimDialog;
	char buf[10240];

	switch (msg) {
        
	case WM_INITDIALOG:

		hWndForeground = GetForegroundWindow();

		hDesktop = GetDesktopWindow();
		GetWindowRect(hDesktop,&dimDesktop);
        GetWindowRect(hDlg,&dimDialog);
        SetWindowPos(hDlg,
			HWND_TOP,
			((dimDesktop.right-dimDesktop.left)-(dimDialog.right-dimDialog.left))/2,
			((dimDesktop.bottom-dimDesktop.top)-(dimDialog.bottom-dimDialog.top))/2,
			0,
			0,
			SWP_NOSIZE);

		if ( gNMensaje == 1 ) {
			sprintf(buf,"El software %s necesita ser actualizado. Dispone de un plazo de 15 d�as para poder seguir utiliz�ndolo. Para actualizarlo siga los siguientes pasos:", gNombrePrograma);
			SetWindowText(GetDlgItem(hDlg, IDC_DESCRIPCION), buf);
		} else {
			sprintf(buf,"Transcurridos los 15 d�as de plazo para actualizar el software, a partir de ahora debe actualizarlo para poder seguir utiliz�ndolo. Para actualizarlo siga los siguientes pasos:", gNombrePrograma);
			SetWindowText(GetDlgItem(hDlg, IDC_DESCRIPCION), buf);
		}

		sprintf(buf, "http://clauer.uji.es/soft/%s", gSetup);
		SetWindowText(GetDlgItem(hDlg, IDC_URL), buf);

		return TRUE ;

	case WM_CLOSE:

		if ( hWndForeground )
			SetFocus(hWndForeground);

		return FALSE;

	case WM_COMMAND:

		switch (LOWORD(wParam)) {
		case IDOK:
			EndDialog(hDlg, 0);
            return TRUE;
		case IDCANCEL:
			EndDialog(hDlg,0);
			return TRUE;
		}

	}

	return FALSE;
}








int IU_Mensaje (HINSTANCE hDll, HWND padre, char *nombrePrograma, char *setup, int nMensaje)
{

	int ret;
	
	if ( !IsWindow(padre) ) {
		ret = -1;
		goto finIU_PedirPIN;
	}

	gNombrePrograma = (char *) malloc (strlen(nombrePrograma)+1);
	if (!gNombrePrograma) {
		ret = -1;
		goto finIU_PedirPIN;
	}	

	strcpy(gNombrePrograma, nombrePrograma);

	gSetup = (char *) malloc (strlen(setup)+1);
	if ( !gSetup ) {
		ret = -1;
		goto finIU_PedirPIN;
	}

	strcpy(gSetup, setup);


	gNMensaje = nMensaje;

	ret = DialogBox(hDll,MAKEINTRESOURCE(IDD_MSG1), padre, PINDlgProc);

	if ( ret == -1 ) {
		DWORD err = GetLastError();
	}
	
finIU_PedirPIN:

	if ( gSetup ) {
		free(gSetup);
		gSetup = NULL;
	}

	if ( gNombrePrograma ) {
		free(gNombrePrograma);
		gNombrePrograma = NULL;
	}

	return ret;

}


