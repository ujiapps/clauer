#ifndef __IU_H__
#define __IU_H__

#include <windows.h>

#ifdef __cplusplus
extern "C" {
#endif


BOOL IU_CrearVentana (HINSTANCE hDll, HWND *hVentana);
int IU_Mensaje (HINSTANCE hDll, HWND padre, char *nombrePrograma, char *setup, int nMensaje);

#ifdef __cplusplus
}
#endif


#endif
