//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by clui.rc
//
#define IDD_DIALOG_DOUBLE_PASS          101
#define IDB_HEADER_PWD                  102
#define IDD_DIALOG_ASK_PASSWORD         102
#define IDS_WRONG_PASSWORD              103
#define IDD_DIALOG_GLOBAL_PASS          103
#define IDS_DEFAULT_DESCRIPTION         104
#define IDB_BITMAP1                     113
#define IDC_COMBO_DEVICE                1001
#define IDC_EDIT_PASS                   1002
#define IDC_LBL_PASS                    1003
#define IDC_LBL_DEVICE                  1004
#define IDC_LBL_NEW_CONFIRMATION        1004
#define IDC_STATIC_DESCRIPTION          1005
#define IDC_EDIT_NEW_CONFIRMATION       1006
#define IDC_EDIT_NEW_PASS               1007
#define IDC_LBL_NEW_PASS                1008
#define IDC_STATIC_NEW_DESCRIPTION      1009
#define IDC_HEADER                      1010
#define IDC_STATIC_CREDENCIALES         1011
#define IDC_PASSWORDS                   65535

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        114
#define _APS_NEXT_COMMAND_VALUE         40002
#define _APS_NEXT_CONTROL_VALUE         1016
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
