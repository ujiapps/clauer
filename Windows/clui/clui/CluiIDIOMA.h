#ifndef __CSPIDIOMA_H__
#define __CSPIDIOMA_H__

#include <windows.h>

#ifdef __cplusplus
extern "C" {
#endif

/* Prototipos
 */

BOOL  CLUI_IDIOMA_Cargar    (void);
BOOL  CLUI_IDIOMA_Descargar (void);
BOOL  CLUI_IDIOMA_Cargado   (void);
LPSTR CLUI_IDIOMA_Get       (DWORD etiqueta);

#ifdef __cplusplus
}
#endif

/* Etiquetas a utilizar para obtener las traducciones
 */

#define NUM_ETIQUETAS	16

#define CLUI_IDIOMA_OK							0
#define CLUI_IDIOMA_CANCEL						1
#define CLUI_IDIOMA_CREDENTIALS					2
#define CLUI_IDIOMA_PASSPHRASE					3
#define CLUI_IDIOMA_DEVICE						4
#define CLUI_IDIOMA_DEFAULT_DESC_GLOBAL			5
#define CLUI_IDIOMA_DEFAULT_DESC_NEW			6
#define CLUI_IDIOMA_NEW_PASSPHRASE				7
#define CLUI_IDIOMA_NEW_CONFIRMATION			8
#define CLUI_IDIOMA_PASSWORDS					9
#define CLUI_IDIOMA_CERT_PASS					10
#define CLUI_IDIOMA_WRONG_PASS					11
#define CLUI_IDIOMA_ERROR						12
#define CLUI_IDIOMA_DONT_MATCH					13
#define CLUI_IDIOMA_WARNING						14
#define CLUI_IDIOMA_CHANGE_PASSPHRASE			15


#endif

