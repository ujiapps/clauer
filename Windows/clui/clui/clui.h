#ifndef __CLAUER_CLUI_H__
#define __CLAUER_CLUI_H__

#include <windows.h>

#define CLUI_MAX_DEVICE_LEN   128
#define CLUI_MAX_PASS_LEN	  128
#define CLUI_MAX_DESC		  128

#define MAX_CLAUER_OWNER     100

/*
#ifdef __cplusplus
extern "C" {
#endif
*/
// Pide la contrase�a global del clauer

int CLUI_AskGlobalPassphrase ( HWND hWndParent,						// Handle a la ventana padre
							   BOOL bAskDevice,						// Indica si debe enumerar o no dispositivos
							   BOOL bDeviceWithOwner,				// Indica si calcular o no propietario
							   BOOL bVerifyPassword,				// Indica si se debe o no verificar password
							   char szDescription[CLUI_MAX_DESC],   // Indica qu� texto mostrar en la descripci�n de la ventana
							   char szDevice[CLUI_MAX_DEVICE_LEN],	// El dispositivo elegido o que se desea utilizar
							   char szPass[CLUI_MAX_PASS_LEN] );	// La password le�da

int CLUI_AskNewPassphrase ( HWND hWndParent,						// Handle a la ventana padre
						    char szDescription[CLUI_MAX_DESC],		// Descripci�n de la ventana
							char szPass[CLUI_MAX_PASS_LEN]);		// Password


int CLUI_AskDoublePassphrase ( HWND hWndParent,
							  char szDescription[CLUI_MAX_DESC],
							  char szPass[CLUI_MAX_PASS_LEN] );


BOOL CLUI_GetClauerOwner ( char *device, char owner[MAX_CLAUER_OWNER] );


// Valores de retorno

#define CLUI_ERR   -1
#define CLUI_OK     0
#define CLUI_CANCEL 1

/*
#ifdef __cplusplus
}
#endif
*/


#endif
