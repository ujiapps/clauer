#include "CluiIDIOMA.h"

#include <stdio.h>

static char *idioma = NULL;
static char *etiquetas[NUM_ETIQUETAS];


/*! TRUE Ok
 *  FALSE error
 */

BOOL CLUI_IDIOMA_Cargar (void)
{

	HKEY hKey;
	DWORD tamValue;

	if ( idioma != NULL )
		return FALSE;

	if ( RegOpenKeyEx(HKEY_LOCAL_MACHINE, 
			     "SOFTWARE\\Universitat Jaume I\\Projecte Clauer", 
				 0,
				 KEY_QUERY_VALUE,
				 &hKey) != ERROR_SUCCESS ) 
	{
		return FALSE;
	}

	if ( RegQueryValueEx(hKey,"Idioma", NULL, NULL, NULL, &tamValue) != ERROR_SUCCESS ) {
		RegCloseKey(hKey);
		return FALSE;
	}

	if ( tamValue > 5 ) {
		RegCloseKey(hKey);
		return FALSE;
	}

	idioma = new char [tamValue];

	if ( !idioma ) {
		RegCloseKey(hKey);
		return FALSE;
	}

	if ( RegQueryValueEx(hKey,"Idioma", NULL, NULL, (BYTE *)idioma, &tamValue) != ERROR_SUCCESS ) {
		delete [] idioma;
		idioma = NULL;
		RegCloseKey(hKey);
		return FALSE;
	}

	RegCloseKey(hKey);

	/* Cargamos la tabla de etiquetas
	 */


	if ( strcmp(idioma, "1034") == 0 ) 
	{
		/* Castellano */

		etiquetas[CLUI_IDIOMA_OK]          = "Aceptar";
		etiquetas[CLUI_IDIOMA_CANCEL]      = "Cancelar";
		etiquetas[CLUI_IDIOMA_CREDENTIALS] = "Credenciales";
		etiquetas[CLUI_IDIOMA_PASSPHRASE]  = "Contrase�a:";
		etiquetas[CLUI_IDIOMA_DEVICE]			   = "Dispositivo";
		etiquetas[CLUI_IDIOMA_DEFAULT_DESC_GLOBAL] = "Introduzca la contrase�a que protege el dispositivo";
		etiquetas[CLUI_IDIOMA_DEFAULT_DESC_NEW]    = "Escriba la contrase�a y su confirmaci�n";
		etiquetas[CLUI_IDIOMA_NEW_PASSPHRASE] = "Contrase�a:";
		etiquetas[CLUI_IDIOMA_NEW_CONFIRMATION] = "Confirmaci�n:";
		etiquetas[CLUI_IDIOMA_PASSWORDS] = " Contrase�as ";
		etiquetas[CLUI_IDIOMA_CERT_PASS] = "Introduzca la contrase�a";
		etiquetas[CLUI_IDIOMA_WRONG_PASS] = "Contrase�a incorrecta";
		etiquetas[CLUI_IDIOMA_ERROR] = "Error";
		etiquetas[CLUI_IDIOMA_DONT_MATCH] = "La contrase�a y su confirmaci�n no coinciden";
		etiquetas[CLUI_IDIOMA_WARNING] = "Atenci�n";
		etiquetas[CLUI_IDIOMA_CHANGE_PASSPHRASE] = "Ha introducido una contrase�a d�bil para acceder a su dispositivo.\r\n"
			                                       "Debe cambiar la contrase�a antes de poder utilizarlo.\r\n"
												   "Para ello puede utilizar el Gestor del Clauer";

	} else if ( strcmp(idioma, "1027") == 0 )
	{
		/* Catal�n */

		etiquetas[CLUI_IDIOMA_OK]          = "Acceptar";
		etiquetas[CLUI_IDIOMA_CANCEL]      = "Cancel-lar";
		etiquetas[CLUI_IDIOMA_CREDENTIALS] = "Credencials";
		etiquetas[CLUI_IDIOMA_PASSPHRASE]  = "Paraula de pas:";
		etiquetas[CLUI_IDIOMA_DEVICE]      = "Dispositiu";
		etiquetas[CLUI_IDIOMA_DEFAULT_DESC_GLOBAL] = "Introdueix la paraula de pas que protegeix el dispositiu";
		etiquetas[CLUI_IDIOMA_DEFAULT_DESC_NEW]    = "Introdueix la paraula de pas i la seua confirmaci�";

		etiquetas[CLUI_IDIOMA_NEW_PASSPHRASE] = "Paraula de pas:";
		etiquetas[CLUI_IDIOMA_NEW_CONFIRMATION] = "Confirmaci�:";
		etiquetas[CLUI_IDIOMA_PASSWORDS] = " Paraules de pas ";
		etiquetas[CLUI_IDIOMA_CERT_PASS] = "Introdueix la paraula de pas";
		etiquetas[CLUI_IDIOMA_WRONG_PASS] = "Paraula de pas incorrecta";
		etiquetas[CLUI_IDIOMA_ERROR] = "Error";

		etiquetas[CLUI_IDIOMA_DONT_MATCH] = "La paraula de pas i la seua confirmaci� no coincideixen";
		etiquetas[CLUI_IDIOMA_WARNING] = "Atenci�";
		etiquetas[CLUI_IDIOMA_CHANGE_PASSPHRASE] = "Has introdu�t una paraula de pas feble per a accedir al dispositiu.\r\n"
			                                       "Has de canviar-la abans de poder utilitzar-lo.\r\n"
												   "Per a aix� pot utilitzar el Gestor del Clauer";

	} else {

		/* Ingl�s ( = 1033 ) */

		etiquetas[CLUI_IDIOMA_OK]          = "Ok";
		etiquetas[CLUI_IDIOMA_CANCEL]      = "Cancel";
		etiquetas[CLUI_IDIOMA_CREDENTIALS] = "Credentials";
		etiquetas[CLUI_IDIOMA_PASSPHRASE]  = "Passphrase:";
		etiquetas[CLUI_IDIOMA_DEVICE]      = "Device:";
		etiquetas[CLUI_IDIOMA_DEFAULT_DESC_GLOBAL] = "Type in the device's passphrase";
		etiquetas[CLUI_IDIOMA_DEFAULT_DESC_NEW]  = "Type in the passphrase and its confirmation";

		etiquetas[CLUI_IDIOMA_NEW_PASSPHRASE] = "Passphrase:";
		etiquetas[CLUI_IDIOMA_NEW_CONFIRMATION] = "Confirmation:";
		etiquetas[CLUI_IDIOMA_PASSWORDS] = " Passphrases ";
		etiquetas[CLUI_IDIOMA_CERT_PASS] = "Type in the passphrase";
		etiquetas[CLUI_IDIOMA_WRONG_PASS] = "Wrong passphrase";
		etiquetas[CLUI_IDIOMA_ERROR] = "Error";

		etiquetas[CLUI_IDIOMA_DONT_MATCH] = "Passphrase and confirmation doesn't match";
		etiquetas[CLUI_IDIOMA_WARNING] = "Warning";
		etiquetas[CLUI_IDIOMA_CHANGE_PASSPHRASE] = "You must change the device's passphrase before using it\r\n"
			                                       "You can use the Clauer Manager program";

	}


	return TRUE;

}


BOOL CLUI_IDIOMA_Descargar (void)
{
	if ( idioma ) {
		delete [] idioma;
		idioma = NULL;
	}

	return TRUE;
}




LPSTR CLUI_IDIOMA_Get (DWORD etiqueta)
{
	if ( !idioma )
		return NULL;

	if ( etiqueta > (NUM_ETIQUETAS - 1) )
		return NULL;

	return etiquetas[etiqueta];
}



BOOL CLUI_IDIOMA_Cargado (void)
{

	return idioma != NULL;


}

