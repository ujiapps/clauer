// clui_test.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"

#include "../clui/clui.h"



int _tmain(int argc, _TCHAR* argv[])
{
	char device[CLUI_MAX_DEVICE_LEN], pass[CLUI_MAX_PASS_LEN];
	int ret;

	strcpy(device, "\\\\.\\PHYSICALDRIVE1");
	ret = CLUI_AskGlobalPassphrase(NULL, FALSE, TRUE, FALSE, NULL, device,pass);
	switch ( ret ) {
	case CLUI_ERR:
		printf("ERROR\n");
		break;
	case CLUI_OK:
		printf("OK\n");
		printf("device: %s\n", device);
		printf("pass: %s\n", pass);
		printf("len: %ld\n", strlen(pass));
		break;
	case CLUI_CANCEL:
		printf("CANCEL\n");
		break;
	}

	return 0;
}

