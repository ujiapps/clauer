// DlgPaso5_1.cpp : implementation file
//

#include "stdafx.h"
#include "Admin_CLAUER.h"
#include "DlgPaso5_1.h"
#include "Admin_CLAUERDlg.h"
#include "IDIOMA.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// DlgPaso5_1 dialog


DlgPaso5_1::DlgPaso5_1(CWnd* pParent /*=NULL*/)
	: CDialog(DlgPaso5_1::IDD, pParent)
{
	//{{AFX_DATA_INIT(DlgPaso5_1)
	m_editPIN = _T("");
	//}}AFX_DATA_INIT
}


void DlgPaso5_1::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(DlgPaso5_1)
	DDX_Control(pDX, IDC_DESCRIPCION, m_cuadro);
	DDX_Control(pDX, IDC_PIN, m_pin);
	DDX_Control(pDX, IDC_TEXT2, m_texto2);
	DDX_Control(pDX, IDC_TEXT1, m_texto);
	DDX_Text(pDX, IDC_PASS_DISQ, m_editPIN);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(DlgPaso5_1, CDialog)
	//{{AFX_MSG_MAP(DlgPaso5_1)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// DlgPaso5_1 message handlers

CString DlgPaso5_1::GetPIN()
{
	UpdateData(TRUE);
	return m_editPIN;
}

BOOL DlgPaso5_1::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here
	CFont *font = new CFont;
	font->CreatePointFont(12, "System");
	m_texto.SetFont(font);
	m_texto2.SetFont(font);

	m_cuadro.SetWindowText(IDIOMA_Get(IDIOMA_PASO5_1_CUADRO));
	m_pin.SetWindowText(IDIOMA_Get(IDIOMA_PASO5_1_PIN));
	m_texto2.SetWindowText(IDIOMA_Get(IDIOMA_PASO5_1_NEGRITA2));
	m_texto.SetWindowText(IDIOMA_Get(IDIOMA_PASO5_1_NEGRITA1));

	delete font;
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}



BOOL DlgPaso5_1::PreTranslateMessage(MSG* pMsg) 
{
	// TODO: Add your specialized code here and/or call the base class
	if(pMsg->message==WM_KEYDOWN)
	{
		if ( (pMsg->wParam==VK_ESCAPE) )
			pMsg->wParam=NULL ;
		else if ( (pMsg->wParam==VK_RETURN) ) {
			((CAdmin_CLAUERDlg *) this->GetParent())->OnBotonSiguiente();
			//((CAdmin_CLAUERDlg *) AfxGetApp())->OnBotonSiguiente();
			return TRUE;
		}
	}
	return CDialog::PreTranslateMessage(pMsg);
}
