// DlgDelete.cpp: archivo de implementaci�n
//

#include "stdafx.h"
#include "Admin_CLAUER.h"
#include "DlgDelete.h"
#include "Admin_CLAUERDlg.h"
#include "IDIOMA.h"

#include "libdel.h"
#include <librt/librt.h>
#include <cryptowrapper/cryptowrap.h>

#include "DlgContras.h"

// Cuadro de di�logo de DlgDelete

IMPLEMENT_DYNAMIC(DlgDelete, CDialog)

DlgDelete::DlgDelete(CWnd* pParent /*=NULL*/)
	: CDialog(DlgDelete::IDD, pParent)
{

}

DlgDelete::~DlgDelete()
{
}

void DlgDelete::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_TEXTO1, m_texto);
	DDX_Control(pDX, IDC_TEXTO2, m_texto2);
	DDX_Control(pDX, IDC_LIST2, m_ListBox);
}


BEGIN_MESSAGE_MAP(DlgDelete, CDialog)
	ON_BN_CLICKED(IDC_BUTTON1, &DlgDelete::OnBnClickedButton1)
	ON_WM_SHOWWINDOW()
END_MESSAGE_MAP()


// Controladores de mensajes de DlgDelete

BOOL DlgDelete::OnInitDialog()
{
	CDialog::OnInitDialog();

	// TODO:  Agregue aqu� la inicializaci�n adicional

	CFont *font = new CFont;
	font->CreatePointFont(12, "System");
	m_texto.SetFont(font);

	m_texto.SetWindowText(IDIOMA_Get(IDIOMA_PASO_33_NEGRITA));
	m_texto2.SetWindowText(IDIOMA_Get(IDIOMA_PASO_33_DESP_1));

	delete font;

	return TRUE;  // return TRUE unless you set the focus to a control
	// EXCEPCI�N: las p�ginas de propiedades OCX deben devolver FALSE
}

BOOL DlgDelete::PreTranslateMessage(MSG* pMsg)
{
	// TODO: Agregue aqu� su c�digo especializado o llame a la clase base

	if(pMsg->message==WM_KEYDOWN)
	{
		if ( (pMsg->wParam==VK_ESCAPE) )
			pMsg->wParam=NULL ;
		else if ( (pMsg->wParam==VK_RETURN) ) {
			((CAdmin_CLAUERDlg *) this->GetParent())->OnBotonSiguiente();
			return TRUE;
		}
	}
	return CDialog::PreTranslateMessage(pMsg);
}


BOOL DlgDelete::LoadCertList ( void )
{
	USBCERTS_HANDLE hClauer;
	unsigned char bloque[TAM_BLOQUE];
	int nDispositivos,i;
	unsigned char * d[MAX_DEVICES];

	// Enumero certificados a borrar

	memset(d,0,MAX_DEVICES * sizeof(unsigned char *));

	if ( LIBRT_ListarDispositivos(&nDispositivos,d) != 0 ) 
		return FALSE;
	if ( nDispositivos == 0 ) 
		return FALSE;

	if ( LIBRT_IniciarDispositivo(d[0], NULL, &hClauer) != 0 ) {
		for ( i = 0 ; *(d+i) ; i++ )
			free(d[i]);
		return FALSE;
	}

	for ( i = 0 ; *(d+i) ; i++ )
		free(d[i]);

	unsigned char *cert = NULL;
	unsigned long tamCert;
	long nBloque;

	if ( LIBRT_LeerTipoBloqueCrypto(&hClauer, BLOQUE_CERT_PROPIO, 1, bloque, &nBloque) != 0 ) {
		LIBRT_FinalizarDispositivo(&hClauer);
		return FALSE;
	}
	while ( nBloque != -1 ) {

		cert    = BLOQUE_CERTPROPIO_Get_Objeto(bloque);
		tamCert = BLOQUE_CERTPROPIO_Get_Tam(bloque);

		DN *subject = CRYPTO_DN_New();
		if ( ! subject ) {
			LIBRT_FinalizarDispositivo(&hClauer);
			return FALSE;
		}
		DN *issuer = CRYPTO_DN_New();
		if ( ! issuer ) {
			LIBRT_FinalizarDispositivo(&hClauer);
			return FALSE;
		}
		if ( ! CRYPTO_CERT_SubjectIssuer(cert, tamCert, subject, issuer)) {
			LIBRT_FinalizarDispositivo(&hClauer);
			return FALSE;
		}

		// inserto el cert en la lista
		int index = m_ListBox.InsertString(m_ListBox.GetCount(), subject->CN);
		if ( index >= 0 ) {
			m_mListBlock.SetAt(index, nBloque);
		}

		// espero que la liberaci�n no afecte a la lista
		// de certificados

		CRYPTO_DN_Free(subject);
		CRYPTO_DN_Free(issuer);

		// Ala, siguiente
		if ( LIBRT_LeerTipoBloqueCrypto(&hClauer, BLOQUE_CERT_PROPIO, 0, bloque, &nBloque) != 0 ) {
			LIBRT_FinalizarDispositivo(&hClauer);
			return FALSE;
		}
	}

	LIBRT_FinalizarDispositivo(&hClauer);

	return TRUE;
}


//Boton eliminar
void DlgDelete::OnBnClickedButton1()
{
	int selIndex = 0;

	int ret = m_ListBox.GetSel(selIndex);
	while ( ret == 0 ) {
		++selIndex;
		ret = m_ListBox.GetSel(selIndex);
	}
	if ( ret == LB_ERR )
		return;

	if ( ret > 0 ) {

		// Pido contrase�a

		CString strPin;

		DlgContras m_dlgContras;

		int nResponse = m_dlgContras.DoModal();

		if (nResponse == IDOK)
		{
			// TODO: Place code here to handle when the dialog is
			//  dismissed with OK
			if(m_dlgContras.m_pin.IsEmpty()){
				CWnd::MessageBox(IDIOMA_Get(IDIOMA_CONTRASENYA_NO_CORRECTA), IDIOMA_Get(IDIOMA_TITULO_CONTRASENYA_MAL), MB_ICONERROR);
				//				SetPaso(23);
				//				break;
				goto endDialog;
			}else{
				strPin= m_dlgContras.m_pin;
			}
		}

		unsigned char * dispositius[MAX_DEVICES];
		int numDisp;
		USBCERTS_HANDLE handle_certs;

		LIBRT_ListarDispositivos(&numDisp,dispositius);

		if ( LIBRT_IniciarDispositivo(dispositius[0],strPin.GetBuffer(0),&handle_certs) != 0 ) {
			CWnd::MessageBox(IDIOMA_Get(IDIOMA_CONTRASENYA_MAL), IDIOMA_Get(IDIOMA_TITULO_CONTRASENYA_MAL), MB_ICONERROR);
			goto endDialog;
		}


		// Iniciamos dispositivo

		int nDispositivos,i;
		unsigned char * d[MAX_DEVICES];
		unsigned long bn;
		
		memset(d,0,MAX_DEVICES * sizeof(unsigned char *));

		if ( LIBRT_ListarDispositivos(&nDispositivos,d) != 0 ) 
			return;
		if ( nDispositivos == 0 ) 
			return;

		// Borramos

		bn = this->m_mListBlock[selIndex];
		if ( ! CLDEL_CertKeys ( (char *) d[0], (char *) strPin.GetString(), bn ) ) {

			this->m_ListBox.ResetContent();
			LoadCertList();

			// ERROR

			for ( i = 0 ; d[i] ; i++ )
				free(d[i]);
			return;
		}
		for ( i = 0 ; d[i] ; i++ )
			free(d[i]);
		
	}

	this->m_ListBox.ResetContent();
	LoadCertList();


	CWnd::MessageBox(IDIOMA_Get(IDIOMA_PASO_33_OK), "", MB_ICONINFORMATION);


endDialog:

	;
	
}

void DlgDelete::OnShowWindow(BOOL bShow, UINT nStatus)
{
	CDialog::OnShowWindow(bShow, nStatus);

	if ( ! LoadCertList() )
		return;

	// Pido contrase�a

/*
    CString strPin;
	DlgContras dlgContras(this);
	int ret;

	ret = dlgContras.DoModal();
	if ( ret == IDCANCEL )
	{
		// cancelo
		return;
	} else if ( ret == IDABORT ) {
		// error
		return;
	}

	strPin= dlgContras.m_pin;
	*/

	
/*
	//m_ListBox
	HCERTSTORE hStore = 0;
	PCCERT_CONTEXT   pCertContext=NULL;
//	HANDLE           hCertStore;

	unsigned char fingerprint[20];
	DWORD dwFinger;

	dwFinger = sizeof(fingerprint);

	int index = 0;
 

	char pszNameString[256];

 	CString dato;

    DWORD dwClauerStoreParam = CLAUER_STORE_MY_PARAM;


	WCHAR store2[] = L"My\\ClauerStore";
	hStore = CertOpenStore(CERT_STORE_PROV_PHYSICAL, 0, NULL, CERT_SYSTEM_STORE_CURRENT_USER, (const void *) store2);
	if ( ! hStore ) {

		//ERROR

	}
	pCertContext=NULL;
	while(pCertContext = CertEnumCertificatesInStore(hStore, pCertContext)) {
		if(CertGetNameString(pCertContext, CERT_NAME_SIMPLE_DISPLAY_TYPE, 0, NULL, pszNameString, 128)) {
			dato = pszNameString;
			CertGetNameString(pCertContext, CERT_NAME_SIMPLE_DISPLAY_TYPE, CERT_NAME_ISSUER_FLAG, NULL, pszNameString, 128);
			dato.Append(" ");
			dato.Append(pszNameString);
			dato.Append(" ");
			CertGetNameString(pCertContext, CERT_NAME_FRIENDLY_DISPLAY_TYPE, 0, NULL, pszNameString, 128);
			dato.Append(pszNameString);
 			index = m_ListBox.InsertString(m_ListBox.GetCount(),dato.GetBuffer(0));
			unsigned char sha1Hash[20];
			CRYPT_HASH_BLOB hb;
			hb.pbData = NULL;
			hb.cbData = 20;

			if ( ! CertGetCertificateContextProperty ( pCertContext, CERT_HASH_PROP_ID, sha1Hash, &hb.cbData )) {
 
				// ERROR


			}else{

				//A�adir lista
				//CString finger = fingerprint;
				//certificadosClauer[index] = finger.GetString();
    				certificadosClauer[index] = sha1Hash;
				//memcpy(certificadosClauer[index], sha1Hash,20);
				//certificadosClauer[index].cbData = hb.cbData;


			}

		}else {
	          
			//ERROR 

		}
	}

	m_ListBox.SetCurSel(0);

	*/
}
