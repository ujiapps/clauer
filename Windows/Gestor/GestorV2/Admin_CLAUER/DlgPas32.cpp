// DlgPas32.cpp: archivo de implementaci�n
//

#include "stdafx.h"
#include "Admin_CLAUER.h"
#include "DlgPas32.h"

#include "IDIOMA.h"


// Cuadro de di�logo de DlgPas32

IMPLEMENT_DYNAMIC(DlgPas32, CDialog)

DlgPas32::DlgPas32(CWnd* pParent /*=NULL*/)
	: CDialog(DlgPas32::IDD, pParent)
{
 m_radio = -1;
}

DlgPas32::~DlgPas32()
{
}

void DlgPas32::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_TEXTO_NEGRO, m_texto);
	DDX_Control(pDX, IDC_CUADRO, m_texto2);
	DDX_Control(pDX, IDC_A_FICHERO, m_texto3);
	DDX_Control(pDX, IDC_A_IE, m_texto4);
	DDX_Radio(pDX, IDC_RADIO1, m_radio);
}


BEGIN_MESSAGE_MAP(DlgPas32, CDialog)
	ON_WM_SHOWWINDOW()
END_MESSAGE_MAP()


// Controladores de mensajes de DlgPas32

BOOL DlgPas32::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here

	m_radio = 0;
	UpdateData(FALSE);
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void DlgPas32::OnShowWindow(BOOL bShow, UINT nStatus)
{
	CDialog::OnShowWindow(bShow, nStatus);

	// TODO: Agregue aqu� su c�digo de controlador de mensajes

		CDialog::OnShowWindow(bShow, nStatus);

	// TODO: Agregue aqu� su c�digo de controlador de mensajes

	CFont *font = new CFont;
	font->CreatePointFont(12, "System");
	m_texto.SetFont(font);
	m_texto.SetWindowText(IDIOMA_Get(IDIOMA_PASO_32_NEGRITA));
	m_texto2.SetWindowText(IDIOMA_Get(IDIOMA_PASO_32_CUADRO));
	m_texto3.SetWindowText(IDIOMA_Get(IDIOMA_PASO_32_DESP_1));
	m_texto4.SetWindowText(IDIOMA_Get(IDIOMA_PASO_32_DESP_2));
	delete font;
}
