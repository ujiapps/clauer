#include "stdafx.h"
#include "libdel.h"
#include "misc.h"

#include <LIBRT/libRT.h>
#include <CRYPTOWrapper/CRYPTOWrap.h>



int CLDEL_CertKeys ( char *szDevice,
		     char *szPwd,
		     long bn )
{
    USBCERTS_HANDLE h;
    int found= 0, rewrite=0;
    unsigned long j, i, bNumbers;
    long * hBlock;
    unsigned char *blocks = NULL, type;
    unsigned char bloque[TAM_BLOQUE];
    unsigned char id[20], zero[20];
    unsigned int lstContainerSize = NUM_KEY_CONTAINERS;
    INFO_KEY_CONTAINER lstContainer[NUM_KEY_CONTAINERS];
      

    memset(zero,0,20);

	if ( ! MISC_CLAUER_Ini((unsigned char *) szDevice, szPwd, &h) )
		return 0;
    
    if ( LIBRT_LeerBloqueCrypto( &h, bn, bloque) != 0) {
		LIBRT_FinalizarDispositivo(&h);
		return 0;
    }
	
    /*  Nos copiamos el identificador del certificado. */

    memcpy(id, BLOQUE_CERTPROPIO_Get_Id(bloque), 20);

	// borro el cert
    if ( LIBRT_BorrarBloqueCrypto ( &h, bn) != 0 ) { 
		LIBRT_FinalizarDispositivo(&h);
		return 0;
    }
    
    /* Ahora, buscamos todos los bloques que contengan este 
     * identificador y los borramos, excepto los key containers
     * en los cuales s�lo debemos modificar el puntero a la llave
     */
    
    if ( LIBRT_LeerTodosBloquesOcupados ( &h, NULL, NULL, &bNumbers ) != 0 ) {
		LIBRT_FinalizarDispositivo(&h);
		return 0;
    }
    
    blocks = (unsigned char *) malloc (bNumbers * TAM_BLOQUE);
    
    if ( ! blocks ) {
		LIBRT_FinalizarDispositivo(&h);
		return 0;
    }
    
    hBlock = ( long * ) malloc ( sizeof(long) * bNumbers );
    
    if ( ! hBlock ) {
		LIBRT_FinalizarDispositivo(&h);
		free(blocks);
		return 0;
    }
    
    if ( LIBRT_LeerTodosBloquesOcupados ( &h, hBlock, blocks, &bNumbers ) != 0 ) {
		free(blocks);
		free(hBlock);
		LIBRT_FinalizarDispositivo(&h);
		return 0;
    }
    
    for ( i = 0 ; i < bNumbers ; i++ ) {
		type = *(blocks+1);
		if ( type ==  BLOQUE_KEY_CONTAINERS ) {
			rewrite= 0;
			if ( BLOQUE_KeyContainer_Enumerar(blocks, lstContainer, &lstContainerSize) != 0 ) {
				break;
			}
			for ( j = 0 ; j < lstContainerSize ; j++ ) {
			if ( memcmp(lstContainer[j].idExchange, id, 20) == 0 ) {
				if ( memcmp(lstContainer[j].idSignature, id, 20) == 0 || 
				 memcmp(lstContainer[j].idSignature, zero, 20) == 0) {
				if ( BLOQUE_KeyContainer_Borrar ( blocks , lstContainer[j].nombreKeyContainer ) != 0 ){
					 return 0;
				}
				else {
					/* Es necesario reescribir el bloque en el clauer */
					rewrite= 1; 
				}
				}
				else{
				if (  BLOQUE_KeyContainer_Establecer_ID_Exchange ( blocks, lstContainer[j].nombreKeyContainer, zero ) != 0  ){
					return 1;
				}
				else{
					 rewrite= 1; 
				}
				}
			} else if ( memcmp(lstContainer[j].idSignature, id, 20) == 0 ) {
				if ( memcmp(lstContainer[j].idExchange , id, 20) == 0 || 
				 memcmp(lstContainer[j].idExchange , zero, 20) == 0) {
				if ( BLOQUE_KeyContainer_Borrar ( blocks , lstContainer[j].nombreKeyContainer ) != 0 ){
					return 0;
				}
				else {
						/* Es necesario reescribir el bloque en el clauer */
						rewrite= 1; 
				}
				}
				else{
				if (  BLOQUE_KeyContainer_Establecer_ID_Signature ( blocks, lstContainer[j].nombreKeyContainer, zero ) != 0 ){
					return 1;
				}
				else{
					rewrite= 1;	
				}
				}
			}
			}
			if ( rewrite == 1 ){
			if ( BLOQUE_KeyContainer_Enumerar(blocks, lstContainer, &lstContainerSize) != 0 ) {
				break;
			}
			
			if ( lstContainerSize == 0 ){
				if ( LIBRT_BorrarBloqueCrypto ( &h, hBlock[i] ) != 0 ) { 
				return 0;
				}
			}
			else{
				if ( LIBRT_EscribirBloqueCrypto ( &h, hBlock[i], blocks ) != 0 ){
				return 0;
				}
			}
			}
		}
		else {
			found= 0;
			switch ( type ) {
				case BLOQUE_LLAVE_PRIVADA:
					if ( memcmp(id, BLOQUE_LLAVEPRIVADA_Get_Id(blocks), 20 ) == 0 ){
						found= 1;
					}
					break;

				case BLOQUE_CIPHER_PRIVKEY_PEM:
					if ( memcmp(id, BLOQUE_CIPHER_PRIVKEY_PEM_Get_Id(blocks), 20 ) == 0 ){
						found= 1;
					}
					break;

				case BLOQUE_PRIVKEY_BLOB:
					if ( memcmp(id, BLOQUE_PRIVKEYBLOB_Get_Id(blocks), 20 ) == 0 ){
						found= 1;
					}
					break;
			    
				case BLOQUE_CIPHER_PRIVKEY_BLOB:
					if ( memcmp(id, BLOQUE_PRIVKEYBLOB_Get_Id(blocks), 20 ) == 0 ){
						found= 1;
					}
					break;
			}
			if ( found ){
				if ( LIBRT_BorrarBloqueCrypto ( &h, hBlock[i] ) != 0 ) { 
					return 0;
				}
			}
		}
		blocks += TAM_BLOQUE;
    }
    return 1;
}



