#include "stdafx.h"
#include "IDIOMA.h"

#include <stdio.h>

static char *idioma = NULL;
static char *etiquetas[NUM_ETIQUETAS];


/*! TRUE Ok
 *  FALSE error
 */

BOOL IDIOMA_Cargar (void)
{

	HKEY hKey;
	DWORD tamValue;

	if ( idioma != NULL )
		return FALSE;

	if ( RegOpenKeyEx(HKEY_LOCAL_MACHINE, 
			     "SOFTWARE\\Universitat Jaume I\\Projecte Clauer", 
				 0,
				 KEY_QUERY_VALUE,
				 &hKey) != ERROR_SUCCESS ) 
	{
		return FALSE;
	}

	if ( RegQueryValueEx(hKey,"Idioma", NULL, NULL, NULL, &tamValue) != ERROR_SUCCESS ) {
		RegCloseKey(hKey);
		return FALSE;
	}

	if ( tamValue > 5 ) {
		RegCloseKey(hKey);
		return FALSE;
	}

	idioma = new char [tamValue];

	if ( !idioma ) {
		RegCloseKey(hKey);
		return FALSE;
	}

	if ( RegQueryValueEx(hKey,"Idioma", NULL, NULL, (BYTE *)idioma, &tamValue) != ERROR_SUCCESS ) {
		delete [] idioma;
		idioma = NULL;
		RegCloseKey(hKey);
		return FALSE;
	}

	RegCloseKey(hKey);

	/* Cargamos la tabla de etiquetas
	 */

	if ( strcmp(idioma, "1027") == 0 )
	{
		/* Valenci�
		 */
		
		//BOTONES
		etiquetas[IDIOMA_BOTON_CANCEL]						= "Cancel�lar";
		etiquetas[IDIOMA_BOTON_SIGUIENTE]					= "Seg�ent >";
		etiquetas[IDIOMA_BOTON_FINALIZAR]					= "Finalitzar";
		etiquetas[IDIOMA_BOTON_EMPEZAR]						= "Comen�ar";	
		etiquetas[IDIOMA_BOTON_IMPORTAR]					= "Importar";		
		etiquetas[IDIOMA_BOTON_ELIMINAR]					= "Formatejar";
		etiquetas[IDIOMA_BOTON_ANTERIOR]					= "< Anterior";
		etiquetas[IDIOMA_BOTON_ACEPTAR]						= "Acceptar";
		
		
		etiquetas[IDIOMA_PASO1_NEGRITA]						= "Introdu�u la mem�ria USB si encara no ho heu fet.";
		etiquetas[IDIOMA_PASO1_EXPLICACION]					= "Aquest programa us permetr� modificar o formatejar un nou  Clauer idCAT. Si us plau, seguiu les instruccions que es mostren en pantalla.";

		etiquetas[IDIOMA_PASO2_NEGRITA]						= "El  Clauer idCAT no t� format criptogr�fic, a continuaci� es formatejar�. Per poder continuar heu d'especificar una paraula de pas prou segura.";
		etiquetas[IDIOMA_PASO2_CUADRO1]						= "Paraula de pas del  Clauer idCAT";
		etiquetas[IDIOMA_PASO2_CONTRAS]						= "Paraula de pas:";
		etiquetas[IDIOMA_PASO2_CONF]						= "Confirmaci�:";
		etiquetas[IDIOMA_PASO2_CUADRO]						="Paraula de pas del  Clauer idCAT";
		etiquetas[IDIOMA_PASO2_NEGR_FOR]			        ="A continuaci� es formatejar� la zona criptogr�fica. Per poder continuar heu d'especificar una paraula de pas prou segura."; 

		etiquetas[IDIOMA_PASO3_NEGRITA]						= "Es comen�ar� a formatejar el  Clauer idCAT. Si us plau, no traieu la mem�ria USB mentre duri l'operaci�.";
		etiquetas[IDIOMA_PASO3_BARRA]						= "Progr�s de l'operaci�";
		etiquetas[IDIOMA_PASO3_DESCRIP]						= "Descripci�";
		etiquetas[IDIOMA_PASO3_DESCRIPTION]					= "Premeu el bot� Comen�ar per continuar";
		etiquetas[IDIOMA_PASO3_ADVERTENCIA]					= "Totes les dades de la mem�ria USB s'esborraran";

		etiquetas[IDIOMA_PASO4_NEGRITA]						= "El  Clauer idCATs'ha particionat correctament, seleccioneu una opci�.";
		etiquetas[IDIOMA_PASO4_OPC]						= "Opcions";
		etiquetas[IDIOMA_PASO4_DISQ]						= "Importar certificat del disquet de la UJI";
		etiquetas[IDIOMA_PASO4_UBIC]						= "Importar certificat des d'un fitxer .p12 o pfx";
		etiquetas[IDIOMA_PASO4_IE]						= "Importar certificat instal�lat en l'Internet Explorer";
		etiquetas[IDIOMA_PASO4_FIN]						= "Finalitzar";

		etiquetas[IDIOMA_PASO5_1_NEGRITA1]					= "1. Introdu�u el disquet amb els certificats que us va proporcionar el personal de registre de la UJI";
		etiquetas[IDIOMA_PASO5_1_NEGRITA2]					= "2. Escriviu el PIN del disquet";
		etiquetas[IDIOMA_PASO5_1_CUADRO]					= "PIN del disquet";
		etiquetas[IDIOMA_PASO5_1_PIN]						= "PIN:";

		etiquetas[IDIOMA_PASO5_2_NEGRITA]					= "Escriviu la ubicaci� del certificat que voleu importar";
		etiquetas[IDIOMA_PASO5_2_CUADRO]					= "Ubicaci�";
		etiquetas[IDIOMA_PASO5_2_SELECT]					= "Trieu el certificat";

		etiquetas[IDIOMA_PASO5_3_NEGRITA]					= "Indiqueu la paraula de pas del certificat contingut al fitxer que heu seleccionat";

		etiquetas[IDIOMA_PASO5_3_CUADRO]					= "Paraula de pas del certificat";
		etiquetas[IDIOMA_PASO5_3_CONTR]						= "Paraula de pas:";

		etiquetas[IDIOMA_PASO5_6_NEGRITA]					= "El Gestor del  Clauer idCATha finalitzat correctament.";
		etiquetas[IDIOMA_PASO5_6_DESCRIP1]					= "El  Clauer idCATs'ha formatejat i particionat correctament. Si tamb� heu triat importar un certificat podreu utilitzar el  Clauer idCAT com a dispositiu criptogr�fic.";
		etiquetas[IDIOMA_PASO5_6_DESCRIP2]					= "Per modificar el  Clauer idCAT nom�s cal que torneu a utilitzar aquest programa.";

		etiquetas[IDIOMA_DIALOG_CONTRAS_TEXTO]					= "Introdu�u la paraula de pas que protegeix el vostre  Clauer idCAT";
		etiquetas[IDIOMA_DIALOG_CONTRAS_CUADRO]					= "Paraula de pas";
		etiquetas[IDIOMA_DIALOG_CONTRAS_CONTR]					= "Paraula de pas:";
		etiquetas[IDIOMA_DIALOG_CONTRAS_TITULO]					= "Paraula de pas del  Clauer idCAT";

		etiquetas[IDIOMA_DIALOG_WIN98_NEGRITA]					= "Introdu�u el disquet amb els certificats que us van proporcionar juntament amb el  Clauer idCAT.";
		etiquetas[IDIOMA_DIALOG_WIN98_CUADRO1]					= "Escriviu el PIN actual";
		etiquetas[IDIOMA_DIALOG_WIN98_CUADRO2]					= "Escriviu el nou PIN";
		etiquetas[IDIOMA_DIALOG_WIN98_PIN]					= "PIN:";
		etiquetas[IDIOMA_DIALOG_WIN98_NUEVO_PIN]				= "Nou PIN:";
		etiquetas[IDIOMA_DIALOG_WIN98_CONF]					= "Confirmaci�";
		etiquetas[IDIOMA_DIALOG_WIN98_BOTON_MODIFICAR]				= "Modificar";
		etiquetas[IDIOMA_DIALOG_WIN98_TITULO]					= "Gestor del  Clauer idCAT :: Modificar PIN del disquet";

		etiquetas[IDIOMA_DIALOG_SEGURO_TEXTO]					= "A continuaci� es formatejar� la zona criptogr�fica, tots els certificats que contingui el  Clauer idCAT seran eliminats.";
		etiquetas[IDIOMA_DIALOG_SEGURO_PREGUNTA]				= "Esteu segur que voleu formatejar la zona criptogr�fica?";
		etiquetas[IDIOMA_DIALOG_SEGURO_SI]					= "S�";
		etiquetas[IDIOMA_DIALOG_SEGURO_NO]					= "No";

		etiquetas[IDIOMA_PASO_50_NEGRITA]					= "El Gestor del  Clauer idCAT ha finalitzat correctament.";
		etiquetas[IDIOMA_PASO_50_DESCRIP1]					= "Per modificar el  Clauer idCAT nom�s cal que torneu a utilitzar aquest programa.";

		etiquetas[IDIOMA_PASO_20_NEGRITA]					= "S'ha detectat un  Clauer idCAT que cont� una zona criptogr�fica, seleccioneu una opci�.";
		etiquetas[IDIOMA_PASO_20_MODIFICAR_PASSWORD]				= "Modificar la paraula de pas";
		etiquetas[IDIOMA_PASO_20_GESTIONAR_CERTIFICADOS]			= "Gestionar certificats";
		etiquetas[IDIOMA_PASO_20_ELIMINAR_CRIPTO]				= "Formatejar zona criptogr�fica";
		etiquetas[IDIOMA_PASO_20_MODIFICAR_ZONA]				= "Modificar grand�ria de la zona criptogr�fica";
		etiquetas[IDIOMA_PASO_20_FIN]						= "Finalitzar";

		etiquetas[IDIOMA_PASO_21_NEGRITA]					= "Modificar la paraula de pas";
		etiquetas[IDIOMA_PASO_21_CUADRO1]					= "Escriviu la paraula de pas actual";
		etiquetas[IDIOMA_PASO_21_CUADRO2]					= "Escriviu la nova paraula de pas";
		etiquetas[IDIOMA_PASO_21_CONTRA]					= "Paraula de pas:";
		etiquetas[IDIOMA_PASO_21_NUEVA]						= "Nova paraula de pas:";
		etiquetas[IDIOMA_PASO_21_CONF]						= "Confirmaci�:";

		etiquetas[IDIOMA_PASO_22_NEGRITA]					= "Seleccioneu una opci�";
		etiquetas[IDIOMA_PASO_22_CUADRO]					= "Gestionar certificats";
		etiquetas[IDIOMA_PASO_22_IMP]						= "Importar certificats al  Clauer idCAT";	
		etiquetas[IDIOMA_PASO_22_EXP]						= "Exportar certificats des del  Clauer idCAT";
		etiquetas[IDIOMA_PASO_22_ELIM]						= "Eliminar certificats del  Clauer idCAT";
		etiquetas[IDIOMA_PASO_22_DISQ]						= "Modificar PIN del disquet de la UJI";

		etiquetas[IDIOMA_PASO_23_NEGRITA]					= "Seleccioneu una opci�";
		etiquetas[IDIOMA_PASO_23_CUADRO]					= "Opcions";
		etiquetas[IDIOMA_PASO_23_DISQ]						= "Importar certificat del disquet de la UJI";
		etiquetas[IDIOMA_PASO_23_UB]						= "Importar certificat des d'un fitxer";
		etiquetas[IDIOMA_PASO_23_IE]						= "Importar certificat instal�lat a l'Internet Explorer";

		etiquetas[IDIOMA_PASO_28_TEXT1]						= "Formatejar zona criptogr�fica";
		etiquetas[IDIOMA_PASO_28_TEXT2]						= "Aquesta operaci� comporta l'eliminaci� de tots el certificats i totes les claus i credencials que cont� el  Clauer idCAT. No afectar� la zona de dades.\n\nDespr�s podreu tornar a importar els certificats si en teniu una c�pia.";
		etiquetas[IDIOMA_PASO_28_TEXT3]						= "Nom�s podreu utilitzar el  Clauer idCAT com a disc dur de butxaca.";

		etiquetas[IDIOMA_FICHERO_ABIERTO]					= "L'aplicaci� ja est� oberta";

		etiquetas[IDIOMA_CREAR_FICHERO]						= "Error quan es creava el fitxer de bloqueig";

		etiquetas[IDIOMA_NO_ENCONTRAR_STICK]					= "No s'ha pogut determinar la pres�ncia de mem�ries USB en el sistema";
		
		etiquetas[IDIOMA_NO_HAY_STICK]						= "No hi ha cap mem�ria USB inserida en el sistema\nSi us plau, premeu el bot� Acceptar i inseriu el dispositiu\nque vulgueu inicialitzar";

		etiquetas[IDIOMA_MUCHOS_STICKS]						= "Hi ha m�s d'una mem�ria USB inserida en el sistema\nSi us plau, premeu el bot� Acceptar i deixeu inserit\n�nicament el dispostiu que vulgueu inicialitzar";

		etiquetas[IDIOMA_NO_INICIALIZAR]					= "Impossible inicialitzar el dispositiu.\n";
	
		etiquetas[IDIOMA_COMPROBANDO_ADMINISTRADOR]				= "Impossible comprovar si sou administrador.\n";

		etiquetas[IDIOMA_NO_ADMINISTRADOR]					= "Heu de ser administrador per poder inicialitzar un nou  Clauer idCAT";

		etiquetas[IDIOMA_IMPOSIBLE_CERRAR_DISP]					= "Impossible tancar dispositiu.\n";

		etiquetas[IDIOMA_PIN_COINCIDENCIA]					= "La paraula de pas i la seva confirmaci� han de coincidir";
		
		etiquetas[IDIOMA_LONGITUD_PIN]						= "Heu d'escriure una paraula de pas d'un m�nim de 8 car�cters";

		etiquetas[IDIOMA_NO_BASE]						= "Cal que tingueu instal�lat el programari base";

		etiquetas[IDIOMA_NO_IMPLEMENTADO]					= "Opci� no implementada.";
		
		etiquetas[IDIOMA_TITULO_PIN_INCORRECTO]					= "Gestor  Clauer idCAT :: ERROR :: Paraula de pas incorrecta";

		etiquetas[IDIOMA_TITULO_NO_CERTIFICADOS]				= "Gestor  Clauer idCAT :: ERROR :: No s'han trobat certificats";

		etiquetas[IDIOMA_NO_IMPORTAR_CERTIFICADO]				= "No s'ha pogut importar el certificat";

		etiquetas[IDIOMA_SELECCIONAR_CERTIFICADO]				= "Heu de seleccionar un certificat";

		etiquetas[IDIOMA_CONTRASENYA_MAL]					= "Paraula de pas del  Clauer idCAT incorrecta\n";

		etiquetas[IDIOMA_CONTRASENYA_IDCAT]					= "Cal formatejar la zona criptogr�fica abans d'importar certificats al  Clauer idCAT, per a aix� heu de ser administrador.";

		etiquetas[IDIOMA_TITULO_CONTRASENYA_MAL]			        = " Clauer idCAT :: Paraula de pas  Clauer idCAT";

		etiquetas[IDIOMA_NO_MODIFICAR_CONTRASENYA]				= "No ha estat possible modificar la paraula de pas del  Clauer idCAT\n";

		etiquetas[IDIOMA_NO_FINALIZADO]						= "Error finalitzant dispositiu\n";

		etiquetas[IDIOMA_CONTRASENYA_MODIFICADA]				= "La paraula de pas s'ha modificat correctament.\n";

		etiquetas[IDIOMA_CONTRASENYA_NO_CORRECTA]				= "Paraula de pas del  Clauer idCAT incorrecta\n";

		etiquetas[IDIOMA_TITULO_ERROR_BUSCANDO]					= " Clauer idCAT :: ERROR :: Buscant mem�ria USB";

		etiquetas[IDIOMA_NO_ENCONTRADOS_CERTIFICADOS]				= "Gestor  Clauer idCAT :: ERROR :: No s'han trobat certificats";

		etiquetas[IDIOMA_CERTIFICADO_OK]					= "El certificat s'ha importat correctament.\n";

		etiquetas[IDIOMA_TITULOS_CERTIFICADOS]					= "Gestor clauer  :: Certificats";

		etiquetas[IDIOMA_NO_MODIFICAR_MBR]					= "Impossible canviar particions del mbr.\n";

		etiquetas[IDIOMA_RETIRAR_STICK]						= "Si us plau, retireu la mem�ria USB i premeu el bot� Acceptar\n";

		etiquetas[IDIOMA_TITULO_RETIRAR]					= "Gestor  Clauer idCAT :: Traieu la mem�ria USB";

		etiquetas[IDIOMA_INSERTAR_STICK]					= "Si us plau, inseriu de nou la mem�ria USB i premeu el bot� Acceptar\n";

		etiquetas[IDIOMA_TITULO_INSERTAR]					= "Gestor  Clauer idCAT :: Inseriu la mem�ria USB";

		etiquetas[IDIOMA_TITULO_ELIMINAR_ZONA_CRIPTO]				= "Gestor  Clauer idCAT :: Formategeu la zona criptogr�fica";

		etiquetas[IDIOMA_NO_INSERTADO]						= "Encara no heu inserit la mem�ria USB. Si us plau,\n inseriu-la de nou i premeu el bot� Acceptar";

		etiquetas[IDIOMA_MUCHOS_STICKS_INSERT]					= "El programa ha detectat m�s d'un stick USB inserit en el sistema.\n Si us plau, deixeu inserida �nicament la mem�ria amb la qual va comen�ar el proc�s\n d'inicialitzaci�.";

		etiquetas[IDIOMA_IMPOSIBLE_FORMATEAR_LOGICA]				= "Impossible formatejar la unitat l�gica del dispositiu.\n";

		etiquetas[IDIOMA_ERROR_FORMATEANDO]					= " Clauer idCAT :: ERROR :: Formatejant";

		etiquetas[IDIOMA_ELIMINADO_CRIPTO]					= "S'ha formatejat la zona criptogr�fica correctament.\n"; 

		etiquetas[IDIOMA_TITULO_CRIPTO]						= "Gestor  Clauer idCAT :: Zona criptogr�fica";

		etiquetas[IDIOMA_PROCESO_FINALIZADO]					= "El proc�s ha finalitzat correctament\nPremeu Acceptar";

		etiquetas[IDIOMA_TITULO_FINALIZADO]					= " Clauer idCAT :: Finalitzat";

		etiquetas[IDIOMA_NO_CERT_DISQUET]					= "No s'ha pogut llegir cap certificat en la unitat A:\n";

		etiquetas[IDIOMA_PIN_INCORRECTO]					= "Paraula de pas incorrecta. Torneu a escriure la paraula de pas.\nComproveu que heu respectat maj�scules i min�scules (si n'hi ha)\nPremeu el bot� Acceptar";

		etiquetas[IDIOMA_ERROR_VERIFICANDO_PASS]				= "S'ha produ�t un error mentre es verificava la vostra paraula de pas. Proveu-ho de nou. \n";

		etiquetas[IDIOMA_ERROR_ENTRADA_SALIDA]					= "S'ha produ�t un error E/S.\n";

		etiquetas[IDIOMA_NO_IMPORTAR_PKCS12]					= "No s'ha pogut importar el fitxer PKCS12";

		etiquetas[IDIOMA_PASS_NO_SEGURA1]					= "La nova paraula de pas no �s prou segura, proveu-ne una altra.";

		etiquetas[IDIOMA_PASS_NO_SEGURA]					= "La nova paraula de pas no �s prou segura, proveu-ne una altra.\nHi ha massa nombres repetits.";

		etiquetas[IDIOMA_PASS_LETRAS]						= "La nova paraula de pas no �s prou segura, proveu-ne una altra.\nHi ha massa lletres repetides.";

		etiquetas[IDIOMA_MIN_LETRAS]						= "La nova paraula de pas no �s prou segura, proveu-ne una altra.\nHa de tenir com a m�nim 3 lletres.";
	
		etiquetas[IDIOMA_MIN_NUM]						= "La nova paraula de pas no �s prou segura, proveu-ne una altra.\nHa de tenir com a m�nim 3 nombres.";

		etiquetas[IDIOMA_INICIALIZANDO]						= "Inicialitzant dispositiu";

		etiquetas[IDIOMA_FORMATEANDO]						= "Formatejant dispositiu...";

		etiquetas[IDIOMA_ERROR_GRAVE]						= "S'ha produ�t un error greu en l'aplicaci�.\n";

		etiquetas[IDIOMA_NO_DETERMINAR_IDIOMA]					= "No s'ha pogut determinar l'idioma.";

		etiquetas[IDIOMA_NO_NT_VIEJO]						= "El Gestor no suporta Windows NT 3.51";

		etiquetas[IDIOMA_ADVERTENCIA_CAMBIO_PIN]				= "Esteu utilitzant Windows 95/98/Me/NT 4.0\\nEl Gestor nom�s us permetr� modificar el PIN del disquet de la UJI";

		etiquetas[IDIOMA_ABRIR_CERTIFICADO]					= "No s'ha pogut obrir el certificat.";
	
		etiquetas[IDIOMA_ESCRIBIR_CERTIFICADOS]					= "No s'ha pogut escriure el certificat.";
		
		etiquetas[IDIOMA_CERRAR_CERTIFICADOS]					= "No s'ha pogut tancar el certificat.";

		etiquetas[IDIOMA_NO_MODIFICAR_PIN]					= "No s'ha pogut modificar el PIN del disquet.\n";

		etiquetas[IDIOMA_PIN_MODIFICADO]					= "PIN del disquet modificat correctament.";

		etiquetas[IDIOMA_NO_LEER_CERTIFICADO]					= "No s'ha pogut llegir certificat\n";

		etiquetas[IDIOMA_DELETE_NEGRITA]					="Certificats a eliminar del  Clauer idCAT";
		etiquetas[IDIOMA_DELETE_EXPLICACION]				="Seleccioni el certificat que es troba en el  Clauer idCATque desitja eliminar";

		etiquetas[IDIOMA_PASO_23_DESC] = "Importar certificat instal�lat a l'Internet Explorer";

		etiquetas[IDIOMA_PASO_25_NEGRITA] = "Indiqueu la ubicaci� del fitxer que inclou el certificat que voleu importar al  Clauer idCAT";

		etiquetas[IDIOMA_PASO_31_NEGRITA_IMP] = "Seleccioneu el certificat que voleu importar dins del  Clauer idCAT";

		etiquetas[IDIOMA_PASO_31_NEGRITA_EXP] = "Seleccioneu el certificat que voleu exportar cap a l'Internet Explorer";

		etiquetas[IDIOMA_PASO_31_DESP_EXP] = "Exportar certificat del  Clauer idCAT a l'Internet Explorer";

		etiquetas[IDIOMA_PASO_31_DESP_IMP] = "Importar certificat de l'Internet Explorer cap al  Clauer idCAT";

		etiquetas[IDIOMA_PASO_32_NEGRITA] = "Seleccioneu cap a on voleu exportar el certificat que est� dins del  Clauer idCAT.";

		etiquetas[IDIOMA_PASO_32_DESP_1] = "Exportar a un fitxer";

		etiquetas[IDIOMA_PASO_32_DESP_2] = "Exportar a l'Internet Explorer";

		etiquetas[IDIOMA_PASO_32_CUADRO] = "Opcions";

		etiquetas[IDIOMA_PASO_33_NEGRITA] = "Eliminar certificats del  Clauer idCAT";

		etiquetas[IDIOMA_PASO_33_OK] = "Certificat esborrat correctament";

		etiquetas[IDIOMA_PASO_33_DESP_1] = "Seleccioneu el certificat que desitgeu eliminar del Clauer idCAT";

		etiquetas[IDIOMA_PASO_34_NEGRITA] = "Seleccioneu el certificat del Clauer idCAT que voleu exportar cap a fitxer. El nou fitxer tindr� la mateixa paraula de pas que el Clauer idCAT.";

		etiquetas[IDIOMA_PASO_34_DESP_1] = "Exportaci� del Clauer idCAT cap a fitxer";

		etiquetas[IDIOMA_PASO_34_DESP_2] = "Certificat a exportar:";

		etiquetas[IDIOMA_PASO_34_DESP_3] = "Fitxer dest�:";

		etiquetas[IDIOMA_OK_EXP] = "La exportaci� s'ha realitzat amb �xit";

		etiquetas[IDIOMA_OK_IMP] = "La importaci� s'ha realitzat amb �xit";

		etiquetas[IDIOMA_BOTON_COPIAR] = "Copiar";

		etiquetas[IDIOMA_BOTON_MOVER] = "Moure";	

		
		etiquetas[IDIOMA_ERROR_EXP] = "La exportaci� no s'ha realitzat amb �xit. Assegura't que el certificat �s exportable";

		etiquetas[IDIOMA_ERROR_IMP] = "La importaci� no s'ha realitzat amb �xit. Assegura't que el certificat �s exportable";

	} else if ( strcmp(idioma, "1072") == 0 ){
		/* Idioma no reconocido 
		Entonces Aranes
		 */
		//BOTONES
		etiquetas[IDIOMA_BOTON_CANCEL]						= "Cancellar";
		etiquetas[IDIOMA_BOTON_SIGUIENTE]					= "Seg�ent >";
		etiquetas[IDIOMA_BOTON_FINALIZAR]					= "Finalizar";
		etiquetas[IDIOMA_BOTON_EMPEZAR]						= "Comen�ar";	
		etiquetas[IDIOMA_BOTON_IMPORTAR]					= "Importar";		
		etiquetas[IDIOMA_BOTON_ELIMINAR]					= "Formatar";
		etiquetas[IDIOMA_BOTON_ANTERIOR]					= "< Anterior";
		etiquetas[IDIOMA_BOTON_ACEPTAR]						= "Acceptar";
		
		
		etiquetas[IDIOMA_PASO1_NEGRITA]						= "Introdusisque era mem�ria USB se non ac a h�t encara";
		etiquetas[IDIOMA_PASO1_EXPLICACION]					= "Aguest programa li permeter� modificar o formatar un nau Clauer idCAT, se vos platz seguisque es instruccions que se m�stren en pantalha.";

		etiquetas[IDIOMA_PASO2_NEGRITA]						= "Eth clauer  non a format criptografic, se procedir� a formatar-lo. Entad a�� pense ua paraula de pas sufisentament segura.";
		etiquetas[IDIOMA_PASO2_CUADRO1]						= "Paraula de pas deth clauer ";
		etiquetas[IDIOMA_PASO2_CONTRAS]						= "Paraula de pas:";
		etiquetas[IDIOMA_PASO2_CONF]						= "Confirmacion:";
		etiquetas[IDIOMA_PASO2_CUADRO]						= "Paraula de pas deth clauer ";
		etiquetas[IDIOMA_PASO2_NEGR_FOR]			        = "Se procedir� a formatar era z�na criptografica. Ent� h�r a�� pense ua paraula de pas sufisentament segura."; 

		etiquetas[IDIOMA_PASO3_NEGRITA]						= "Se comen�ar� eth formatatge deth clauer . Se vos platz non retire era mem�ria USB pendent era durada dera operacion.";
		etiquetas[IDIOMA_PASO3_BARRA]						= "Progr�s dera operacion";
		etiquetas[IDIOMA_PASO3_DESCRIP]						= "Descripcion";
		etiquetas[IDIOMA_PASO3_DESCRIPTION]					= "Sarre eth boton Comen�ar ent� contunhar";
		etiquetas[IDIOMA_PASO3_ADVERTENCIA]					= "Totes es dades deth stick USB se borraran";

		etiquetas[IDIOMA_PASO4_NEGRITA]						= "Eth clauer  a estat particionat corr�ctament, seleccione ua opcion.";
		etiquetas[IDIOMA_PASO4_OPC]							= "Opcions";
		etiquetas[IDIOMA_PASO4_DISQ]						= "Importar certificat dera disqueta dera UJI";
		etiquetas[IDIOMA_PASO4_UBIC]						= "Importar certificat des d'un fich�r .p12 o pfx";
		etiquetas[IDIOMA_PASO4_IE]						= "Importar certificat installat en Internet Explorer";
		etiquetas[IDIOMA_PASO4_FIN]						= "Finalizar";

		etiquetas[IDIOMA_PASO5_1_NEGRITA1]					= "1. Introdusisque era disqueta damb es certificats que li proporcion�c eth personau de registre dera UJI";
		etiquetas[IDIOMA_PASO5_1_NEGRITA2]					= "2 .Introdusisque eth PIN dera disqueta";
		etiquetas[IDIOMA_PASO5_1_CUADRO]					= "PIN dera disqueta";
		etiquetas[IDIOMA_PASO5_1_PIN]						= "PIN:";

		etiquetas[IDIOMA_PASO5_2_NEGRITA]					= "Introdusisque era situacion deth certificat que desire importar";
		etiquetas[IDIOMA_PASO5_2_CUADRO]					= "Situacion";
		etiquetas[IDIOMA_PASO5_2_SELECT]					= "Escuelhe eth certificat";

		etiquetas[IDIOMA_PASO5_3_NEGRITA]					= "Indique era contrasenha deth certificat inclos ath fitxer .P12 o .PFX";
		etiquetas[IDIOMA_PASO5_3_CUADRO]					= "Paraula de pas deth certificat";
		etiquetas[IDIOMA_PASO5_3_CONTR]						= "Paraula de pas:";

		etiquetas[IDIOMA_PASO5_6_NEGRITA]					= "Eth Gestor deth clauer  a finalizat corr�ctament.";
		etiquetas[IDIOMA_PASO5_6_DESCRIP1]					= "Eth Clauer idCAT a estat formatat e particionat corr�ctament. Se tanben a escuelhut importar un certificat poder� emplegar eth  Clauer idCAT coma dispositiu criptografic.";
		etiquetas[IDIOMA_PASO5_6_DESCRIP2]					= "Ent� modificar eth  Clauer idCAT sonque auetz de tornar a emplegar aguest programa.";

		etiquetas[IDIOMA_DIALOG_CONTRAS_TEXTO]					= "Introdusisque era paraula de pas que proteg�s eth s�n clauer ";
		etiquetas[IDIOMA_DIALOG_CONTRAS_CUADRO]					= "Paraula de pas";
		etiquetas[IDIOMA_DIALOG_CONTRAS_CONTR]					= "Paraula de pas:";
		etiquetas[IDIOMA_DIALOG_CONTRAS_TITULO]					= "Paraula de pas deth clauer ";

		etiquetas[IDIOMA_DIALOG_WIN98_NEGRITA]					= "Introdusisque era disqueta damb es certificats que se li balh�ren amassa damb eth  Clauer idCAT.";
		etiquetas[IDIOMA_DIALOG_WIN98_CUADRO1]					= "Introdusisque PIN actuau";
		etiquetas[IDIOMA_DIALOG_WIN98_CUADRO2]					= "Introdusisque nau PIN";
		etiquetas[IDIOMA_DIALOG_WIN98_PIN]					= "PIN:";
		etiquetas[IDIOMA_DIALOG_WIN98_NUEVO_PIN]				= "Nau PIN:";
		etiquetas[IDIOMA_DIALOG_WIN98_CONF]					= "Confirmacion";
		etiquetas[IDIOMA_DIALOG_WIN98_BOTON_MODIFICAR]				= "Modificar";
		etiquetas[IDIOMA_DIALOG_WIN98_TITULO]					= "Gestor deth clauer  :: Modificar PIN dera disqueta";

		etiquetas[IDIOMA_DIALOG_SEGURO_TEXTO]					= "Se procedir� a formatar era z�na criptografica, toti es certificats que contengue eth clauer  seran eliminadi.";
		etiquetas[IDIOMA_DIALOG_SEGURO_PREGUNTA]				= "Ei segur de formatar era z�na criptografica? ";
		etiquetas[IDIOMA_DIALOG_SEGURO_SI]					= "�c";
		etiquetas[IDIOMA_DIALOG_SEGURO_NO]					= "Non";

		etiquetas[IDIOMA_PASO_50_NEGRITA]					= "Eth Gestor deth clauer  a finalizat corr�ctament.";
		etiquetas[IDIOMA_PASO_50_DESCRIP1]					= "Ent� modificar eth clauer  sonque auetz de tornar a emplegar aguest programa.";

		etiquetas[IDIOMA_PASO_20_NEGRITA]					= "S'a detectat un clauer  que conten ua z�na criptografica, seleccione ua opcion.";
		etiquetas[IDIOMA_PASO_20_MODIFICAR_PASSWORD]				= "Modificar era paraula de pas";
		etiquetas[IDIOMA_PASO_20_GESTIONAR_CERTIFICADOS]			= "Gestionar certificats";
		etiquetas[IDIOMA_PASO_20_ELIMINAR_CRIPTO]				= "Formatar z�na criptografica";
		etiquetas[IDIOMA_PASO_20_MODIFICAR_ZONA]				= "Modificar dimension dera z�na criptografica";
		etiquetas[IDIOMA_PASO_20_FIN]						= "Finalizar";

		etiquetas[IDIOMA_PASO_21_NEGRITA]					= "Modificar era paraula de pas";
		etiquetas[IDIOMA_PASO_21_CUADRO1]					= "Introdusisque era paraula de pas actuau";
		etiquetas[IDIOMA_PASO_21_CUADRO2]					= "Introdusisque era naua paraula de pas";
		etiquetas[IDIOMA_PASO_21_CONTRA]					= "Paraula de pas:";
		etiquetas[IDIOMA_PASO_21_NUEVA]						= "Naua paraula de pas:";
		etiquetas[IDIOMA_PASO_21_CONF]						= "Confirmacion:";

		etiquetas[IDIOMA_PASO_22_NEGRITA]					= "Seleccione ua opcion";
		etiquetas[IDIOMA_PASO_22_CUADRO]					= "Gestionar certificats";
		etiquetas[IDIOMA_PASO_22_IMP]						= "Importar certificats entath clauer ";	
		etiquetas[IDIOMA_PASO_22_EXP]						= "Exportar certificats des deth clauer ";
		etiquetas[IDIOMA_PASO_22_ELIM]						= "Eliminar certificats deth clauer ";
		etiquetas[IDIOMA_PASO_22_DISQ]						= "Modificar PIN dera disqueta dera UJI";

		etiquetas[IDIOMA_PASO_23_NEGRITA]					= "Seleccione una opcio";
		etiquetas[IDIOMA_PASO_23_CUADRO]				    = "Opcions";
		etiquetas[IDIOMA_PASO_23_DISQ]						= "Importar certificat dera disqueta dera UJI";
		etiquetas[IDIOMA_PASO_23_UB]						= "Importar eth certificado desde un fichero";
		etiquetas[IDIOMA_PASO_23_IE]						= "Importar certificat installat en Internet Explorer";

		etiquetas[IDIOMA_PASO_28_TEXT1]						= "Formatar z�na criptografica";
		etiquetas[IDIOMA_PASO_28_TEXT2]						= "Aguesta operacion supause era eliminacion de toti es certificats, claus e credenciaus que contengue eth  Clauer idCAT. Non afectar� ara z�na de dades.\n\nDemp�s poder� tornar a importar es certificats se dispause d'ua c�pia.";
		etiquetas[IDIOMA_PASO_28_TEXT3]						= "Sonque poder� emplegar eth clauer  coma disc dur de p�cha.";

		etiquetas[IDIOMA_FICHERO_ABIERTO]					= "Era aplicacion ja ei dub�rta";

		etiquetas[IDIOMA_CREAR_FICHERO]						= "Erran�a en crear eth fich�r de blocatge";

		etiquetas[IDIOMA_NO_ENCONTRAR_STICK]					= "Non se podec determinar era pres�ncia de mem�ries USB en sist�ma";
		
		etiquetas[IDIOMA_NO_HAY_STICK]						= "Non i a cap mem�ria USB inserida en sist�ma\nSe vos platz, sarre eth boton Acceptar e inserisque eth dispositiu\nque desire inicializar";

		etiquetas[IDIOMA_MUCHOS_STICKS]						= "I a m�s d'ua mem�ria USB inserida en sist�ma\nSe vos platz, sarre eth boton Acceptar e deishatz inserit\nunencament eth dispostiu que desire inicializar";

		etiquetas[IDIOMA_NO_INICIALIZAR]					= "Impossible realizar era inicializacion deth dispositiu.\n";
	
		etiquetas[IDIOMA_COMPROBANDO_ADMINISTRADOR]				= "Impossible verificar se �s administrador.\n";

		etiquetas[IDIOMA_NO_ADMINISTRADOR]					= "Auetz d��ster administrador ent� poder inicializar un nau  Clauer idCAT";

		etiquetas[IDIOMA_IMPOSIBLE_CERRAR_DISP]					= "Impossible barrar dispositiu.\n";

		etiquetas[IDIOMA_PIN_COINCIDENCIA]					= "Era paraula de pas e era sua confirmacion an de co�ncidir";
		
		etiquetas[IDIOMA_LONGITUD_PIN]						= "A d�introdusir ua paraula de pas d'un minim de 8 caract�rs";

		etiquetas[IDIOMA_NO_BASE]						= "A de besonh auer installat eth programari base";

		etiquetas[IDIOMA_NO_IMPLEMENTADO]					= "Opcion non implementada.";
		
		etiquetas[IDIOMA_TITULO_PIN_INCORRECTO]					= "Gestor clauer  :: ERRAN�A :: Paraula de pas incorr�cta";

		etiquetas[IDIOMA_TITULO_NO_CERTIFICADOS]				= "Gestor clauer  :: ERRAN�A :: Non s'an trobat certificats";

		etiquetas[IDIOMA_NO_IMPORTAR_CERTIFICADO]				= "Non s'a podut importar eth certificat";

		etiquetas[IDIOMA_SELECCIONAR_CERTIFICADO]				= "A de seleccionar un certificat";

		etiquetas[IDIOMA_CONTRASENYA_MAL]					= "Paraula de pas deth clauer  incorr�cta\n";

		etiquetas[IDIOMA_CONTRASENYA_IDCAT]					= "Cau formatar era z�na criptografica abantes d'importar certificats entath clauer , entad a�� a d��ster administrador.";

		etiquetas[IDIOMA_TITULO_CONTRASENYA_MAL]			        = " Clauer idCAT :: Paraula de pas clauer ";

		etiquetas[IDIOMA_NO_MODIFICAR_CONTRASENYA]				= "Non a estat possible modificar era paraula de pas deth clauer \n";

		etiquetas[IDIOMA_NO_FINALIZADO]						= "Erran�a finalizant dispositiu\n";

		etiquetas[IDIOMA_CONTRASENYA_MODIFICADA]				= "Era paraula de pas s'a modificat corr�ctament.\n";

		etiquetas[IDIOMA_CONTRASENYA_NO_CORRECTA]				= "Paraula de pas deth clauer  incorr�cta\n";

		etiquetas[IDIOMA_TITULO_ERROR_BUSCANDO]					= " Clauer idCAT :: ERRAN�A :: Cercant mem�ria USB";

		etiquetas[IDIOMA_NO_ENCONTRADOS_CERTIFICADOS]				= "Gestor clauer  :: ERRAN�A :: Non s'an trobat certificats ";

		etiquetas[IDIOMA_CERTIFICADO_OK]					= "Eth certificat s'a importat corr�ctament.\n";

		etiquetas[IDIOMA_TITULOS_CERTIFICADOS]					= "Gestor clauer  :: Certificats";

		etiquetas[IDIOMA_NO_MODIFICAR_MBR]					= "Impossible cambiar particions deth mbr.\n";

		etiquetas[IDIOMA_RETIRAR_STICK]						= "Se vos platz, retire era mem�ria USB e sarre eth boton Acceptar\n";

		etiquetas[IDIOMA_TITULO_RETIRAR]					= "Gestor clauer  :: Extr�igue era mem�ria USB";

		etiquetas[IDIOMA_INSERTAR_STICK]					= "Se vos platz, inserte de nau era mem�ria USB e sarre eth boton Acceptar\n";

		etiquetas[IDIOMA_TITULO_INSERTAR]					= "Gestor clauer  :: Inserte era mem�ria USB";

		etiquetas[IDIOMA_TITULO_ELIMINAR_ZONA_CRIPTO]				= "Gestor clauer  :: Formatar z�na criptografica";

		etiquetas[IDIOMA_NO_INSERTADO]						= "Non a insertat encara era sua mem�ria USB. Se vos platz,\n inserte-la de nau e sarre eth boton Acceptar";

		etiquetas[IDIOMA_MUCHOS_STICKS_INSERT]					= "Eth programa a detectat m�s d'un stick USB inserit en sist�ma.\n Se vos platz, d�ishe inserit unencament era mem�ria damb era quau comenc�c eth proc�s\n d'inicializacion.";

		etiquetas[IDIOMA_IMPOSIBLE_FORMATEAR_LOGICA]				= "Impossible formatar era unitat logica deth dispositiu.\n";

		etiquetas[IDIOMA_ERROR_FORMATEANDO]					= "clauer  :: ERRAN�A :: Formatant";

		etiquetas[IDIOMA_ELIMINADO_CRIPTO]					= "S'a formatat era z�na criptografica corr�ctament.\n"; 

		etiquetas[IDIOMA_TITULO_CRIPTO]						= "Gestor clauer  :: Z�na criptografica";

		etiquetas[IDIOMA_PROCESO_FINALIZADO]					= "Eth proc�s a acabat damb �xit\nSarre Acceptar";

		etiquetas[IDIOMA_TITULO_FINALIZADO]					= "clauer  :: Finalizat";

		etiquetas[IDIOMA_NO_CERT_DISQUET]					= "Non se podec li�ger certificat ena unitat A:\n";

		etiquetas[IDIOMA_PIN_INCORRECTO]					= "Paraula de pas incorr�cta. Torne a introdusir era paraula de pas.\nCompr�ve que a respectat majuscules i minuscules (se i auesse)\nSarre eth boton Acceptar";

		etiquetas[IDIOMA_ERROR_VERIFICANDO_PASS]				= "A succedit ua erran�a verificant eth s�n password. Sage de nau. \n";

		etiquetas[IDIOMA_ERROR_ENTRADA_SALIDA]					= "A succedit ua erran�a E/S.\n";

		etiquetas[IDIOMA_NO_IMPORTAR_PKCS12]					= "Non s'a podut importar eth fich�r PKCS12";

		etiquetas[IDIOMA_PASS_NO_SEGURA1]					= "Era naua paraula de pas non ei pro segura, sage ua auta.";

		etiquetas[IDIOMA_PASS_NO_SEGURA]					= "Era naua paraula de pas non ei pro segura, sage ua auta.\nI a massa numer�s repetidi.";

		etiquetas[IDIOMA_PASS_LETRAS]						= "Era naua paraula de pas non ei pro segura, sage damb ua auta.\nI a massa letres repetides.";

		etiquetas[IDIOMA_MIN_LETRAS]						= "Era naua paraula de pas non ei pro segura, sage damb ua auta.\nA d�auer com a minim 3 letres.";
	
		etiquetas[IDIOMA_MIN_NUM]						= "Era naua paraula de pas non ei pro segura, sage damb ua auta.\nA d�auer com a minim 3 numer�s.";

		etiquetas[IDIOMA_INICIALIZANDO]						= "Inicializant dispositiu";

		etiquetas[IDIOMA_FORMATEANDO]						= "Formatant dispositiu...";

		etiquetas[IDIOMA_ERROR_GRAVE]						= "Succedic un error gr�u ena aplicacion.\n";

		etiquetas[IDIOMA_NO_DETERMINAR_IDIOMA]					= "Non s'a podut determinar er idi�ma.";

		etiquetas[IDIOMA_NO_NT_VIEJO]						= "Eth Gestor non sup�rte Windows NT 3.51";

		etiquetas[IDIOMA_ADVERTENCIA_CAMBIO_PIN]				= "Emplegue Windows 95/98/Me/NT 4.0\\nEth Gestor sonque li permeter� modificar eth PIN dera disqueta dera UJI";

		etiquetas[IDIOMA_ABRIR_CERTIFICADO]					= "Non s'a podut daurir eth certificat.";
	
		etiquetas[IDIOMA_ESCRIBIR_CERTIFICADOS]					= "Non s'a podut escr�uer eth certificat.";
		
		etiquetas[IDIOMA_CERRAR_CERTIFICADOS]					= "Non s'a podut barrar eth certificat.";

		etiquetas[IDIOMA_NO_MODIFICAR_PIN]					= "Non s'a podut modificar eth PIN dera disqueta.\n";

		etiquetas[IDIOMA_PIN_MODIFICADO]					= "PIN dera disqueta modificat corr�ctament.";

		etiquetas[IDIOMA_NO_LEER_CERTIFICADO]					= "Non s�a podut li�ger certificat\n";

		etiquetas[IDIOMA_DELETE_NEGRITA]					="Certificats a eliminar del  Clauer idCAT";

		etiquetas[IDIOMA_DELETE_EXPLICACION]				="Seleccioni el certificat que es troba en el  Clauer idCATque desitja eliminar";

		etiquetas[IDIOMA_PASO_23_DESC] = "Importar eth certificat instalat en eth Internet Explorer";

		etiquetas[IDIOMA_PASO_25_NEGRITA] = "Indicatz era ubicacion deth fitxer que cont� eth certificat que vos importar en eth  Clauer idCAT";

		etiquetas[IDIOMA_PASO_31_NEGRITA_IMP] = "Seleccionatz eth certificat que voletz importar laguens eth  Clauer idCAT";

		etiquetas[IDIOMA_PASO_31_NEGRITA_EXP] = "Seleccionatz eth certificat que voletz exportar ath Internet Explorer";

		etiquetas[IDIOMA_PASO_31_DESP_EXP] = "Exportar deth  Clauer idCAT ath Internet Explorer";

		etiquetas[IDIOMA_PASO_31_DESP_IMP] = "Importar deth Internet Explorer ath  Clauer idCAT";

		etiquetas[IDIOMA_PASO_32_NEGRITA] = "Triatz a on volets exportar eth certificat que esta laguens deth  Clauer idCAT.";

		etiquetas[IDIOMA_PASO_32_DESP_1] = "Exportar a un fitxer";

		etiquetas[IDIOMA_PASO_32_DESP_2] = "Exportar a Internet Explorer";

		etiquetas[IDIOMA_PASO_32_CUADRO] = "Opcions";

		etiquetas[IDIOMA_PASO_33_NEGRITA] = "Eliminar certificats deth  Clauer idCAT";

		etiquetas[IDIOMA_PASO_33_OK] = "Certificat esborrat correctament";

		etiquetas[IDIOMA_PASO_33_DESP_1] = "Seleccionatz eth certificat que desiratz eliminar deth  Clauer idCAT";

		etiquetas[IDIOMA_PASO_34_NEGRITA] = "Seleccionatz eth certificat deth  Clauer idCAT que voletz exportar ath fitxer. El nou fitxer tindr� la mateixa paraula de pas que el Clauer idCAT.";

		etiquetas[IDIOMA_PASO_34_DESP_1] = "Exportacion deth  Clauer idCAT a fitxer";

		etiquetas[IDIOMA_PASO_34_DESP_2] = "Certificat a exportar:";

		etiquetas[IDIOMA_PASO_34_DESP_3] = "Fitxer de desti:";

		etiquetas[IDIOMA_OK_EXP] = "Era exportacion sa realizat damb �xit";

		etiquetas[IDIOMA_OK_IMP] = "Era importacion sa realizat damb �xit";

		etiquetas[IDIOMA_BOTON_COPIAR] = "Copiar";

		etiquetas[IDIOMA_BOTON_MOVER] = "Moure";	

		etiquetas[IDIOMA_ERROR_IMP] = "Era exportacion no sa realizat damb �xit. Assegura't que el certificat �s exportable";
		
		etiquetas[IDIOMA_ERROR_EXP] = "Era importacion no sa realizat damb �xit. Assegura't que el certificat �s exportable";

	}else
	{
		/* Idioma no reconocido 
		Entonces Espa�ol
		 */

			//BOTONES
		etiquetas[IDIOMA_BOTON_CANCEL]						= "Cancelar";
		etiquetas[IDIOMA_BOTON_SIGUIENTE]					= "Siguiente >";
		etiquetas[IDIOMA_BOTON_FINALIZAR]					= "Finalizar";
		etiquetas[IDIOMA_BOTON_EMPEZAR]						= "Empezar";
		etiquetas[IDIOMA_BOTON_IMPORTAR]					= "Importar";
		etiquetas[IDIOMA_BOTON_ELIMINAR]					= "Formatear";
		etiquetas[IDIOMA_BOTON_ANTERIOR]					= "< Anterior";
		etiquetas[IDIOMA_BOTON_ACEPTAR]						= "Aceptar";
		
		
		etiquetas[IDIOMA_PASO1_NEGRITA]						= "Introduzca la memoria USB si no lo ha hecho.";
		etiquetas[IDIOMA_PASO1_EXPLICACION]					= "Este programa le permitir� modificar o preparar un nuevo  Clauer idCAT. Por favor, siga las instrucciones que se muestran en pantalla.";

		etiquetas[IDIOMA_PASO2_NEGRITA]						= "El  Clauer idCAT no tiene formato criptogr�fico, a continuaci�n se va a formatear. Para ello debe introducir una contrase�a suficientemente segura.";
		etiquetas[IDIOMA_PASO2_CUADRO1]						= "Contrase�a del  Clauer idCAT";
		etiquetas[IDIOMA_PASO2_CONTRAS]						= "Contrase�a:";
		etiquetas[IDIOMA_PASO2_CONF]						= "Confirmaci�n:";
		etiquetas[IDIOMA_PASO2_CUADRO]						= "Contrase�a del  Clauer idCAT";
		etiquetas[IDIOMA_PASO2_NEGR_FOR]					= "Se va a formatear la zona criptogr�fica. Para ello debe introducir una contrase�a suficientemente segura.";

		etiquetas[IDIOMA_PASO3_NEGRITA]						= "Se va a preparar el  Clauer idCAT. Por favor, no retire la memoria USB mientras dure la operaci�n.";
		etiquetas[IDIOMA_PASO3_BARRA]						= "Progreso de la operaci�n";
		etiquetas[IDIOMA_PASO3_DESCRIP]						= "Descripci�n";
		etiquetas[IDIOMA_PASO3_DESCRIPTION]					= "Pulse en el bot�n Empezar para continuar";
		etiquetas[IDIOMA_PASO3_ADVERTENCIA]					= "Todos los datos del stick USB se borraran";

		etiquetas[IDIOMA_PASO4_NEGRITA]						= "El  Clauer idCATha sido particionado correctamente, elija una opci�n.";
		etiquetas[IDIOMA_PASO4_OPC]						= "Opciones";
		etiquetas[IDIOMA_PASO4_DISQ]						= "Importar certificado del disquete de la UJI";
		etiquetas[IDIOMA_PASO4_UBIC]						= "Importar certificado desde otra ubicaci�n";
		etiquetas[IDIOMA_PASO4_IE]						= "Importar certificado instalado en Internet Explorer";
		etiquetas[IDIOMA_PASO4_FIN]						= "Finalizar";

		etiquetas[IDIOMA_PASO5_1_NEGRITA1]					= "1. Introduzca el disquete con los certificados que le proporcion� el personal de registro de la UJI";
		etiquetas[IDIOMA_PASO5_1_NEGRITA2]					= "2. Teclee el PIN del disquete";
		etiquetas[IDIOMA_PASO5_1_CUADRO]					= "PIN del disquete";
		etiquetas[IDIOMA_PASO5_1_PIN]						= "PIN:";

		etiquetas[IDIOMA_PASO5_2_NEGRITA]					= "Teclee la ubicaci�n del certificado que desea importar";
		etiquetas[IDIOMA_PASO5_2_CUADRO]					= "Ubicaci�n";
		etiquetas[IDIOMA_PASO5_2_SELECT]					= "Elija el certificado";

		etiquetas[IDIOMA_PASO5_3_NEGRITA]					= "Indica la contrase�a del certificado incluido al fichero .P12 o .PFX";
		etiquetas[IDIOMA_PASO5_3_CUADRO]					= "Contrase�a del certificado";
		etiquetas[IDIOMA_PASO5_3_CONTR]						= "Contrase�a:";
	
		etiquetas[IDIOMA_PASO5_6_NEGRITA]					= "El Gestor del  Clauer idCATha finalizado correctamente.";
		etiquetas[IDIOMA_PASO5_6_DESCRIP1]					= "El  Clauer idCAT ha sido formateado y particionado correctamente. Si tambi�n ha elegido importar un certificado podr� usar el  Clauer idCATcomo dispositivo criptogr�fico.";
		etiquetas[IDIOMA_PASO5_6_DESCRIP2]					= "Para modificar el  Clauer idCAT�nicamente debe volver a emplear este programa.";

		etiquetas[IDIOMA_DIALOG_CONTRAS_TEXTO]					= "Introduzca la contrase�a que protege su  Clauer idCAT";
		etiquetas[IDIOMA_DIALOG_CONTRAS_CUADRO]					= "Contrase�a";
		etiquetas[IDIOMA_DIALOG_CONTRAS_CONTR]					= "Contrase�a:";
		etiquetas[IDIOMA_DIALOG_CONTRAS_TITULO]					= "Contrase�a del  Clauer idCAT";

		etiquetas[IDIOMA_DIALOG_WIN98_NEGRITA]					= "Introduzca el disquete con los certificados que se le proporcionaron junto con el  Clauer idCAT.";
		etiquetas[IDIOMA_DIALOG_WIN98_CUADRO1]					= "Teclee el PIN actual";
		etiquetas[IDIOMA_DIALOG_WIN98_CUADRO2]					= "Teclee el nuevo PIN";
		etiquetas[IDIOMA_DIALOG_WIN98_PIN]					= "PIN:";
		etiquetas[IDIOMA_DIALOG_WIN98_NUEVO_PIN]				= "Nuevo PIN:";
		etiquetas[IDIOMA_DIALOG_WIN98_CONF]					= "Confirmaci�n";
		etiquetas[IDIOMA_DIALOG_WIN98_BOTON_MODIFICAR]				= "Modificar";
		etiquetas[IDIOMA_DIALOG_WIN98_TITULO]					= "Gestor del  Clauer idCAT:: Modificar PIN del disquete";

		etiquetas[IDIOMA_DIALOG_SEGURO_TEXTO]					= "A continuaci�n se va a formatear la zona criptogr�fica, todos los certificados que contenga el  Clauer idCAT ser�n eliminados.";
		etiquetas[IDIOMA_DIALOG_SEGURO_PREGUNTA]				= "�Est� seguro de que desea formatear la zona criptogr�fica?";
		etiquetas[IDIOMA_DIALOG_SEGURO_SI]					= "S�";
		etiquetas[IDIOMA_DIALOG_SEGURO_NO]					= "No";

		etiquetas[IDIOMA_PASO_50_NEGRITA]					= "El Gestor del clauer ha finalizado correctamente.";
		etiquetas[IDIOMA_PASO_50_DESCRIP1]					= "Para modificar el  Clauer idCAT�nicamente debe volver a emplear este programa.";
		
		etiquetas[IDIOMA_PASO_20_NEGRITA]					= "Se ha detectado un  Clauer idCATque contiene una zona criptogr�fica, elija una opci�n.";
		etiquetas[IDIOMA_PASO_20_MODIFICAR_PASSWORD]				= "Modificar contrase�a";
		etiquetas[IDIOMA_PASO_20_GESTIONAR_CERTIFICADOS]			= "Gestionar certificados";
		etiquetas[IDIOMA_PASO_20_ELIMINAR_CRIPTO]				= "Formatear zona criptogr�fica";
		etiquetas[IDIOMA_PASO_20_MODIFICAR_ZONA]				= "Modificar tama�o de la zona criptogr�fica";
		etiquetas[IDIOMA_PASO_20_FIN]						= "Finalizar";

		etiquetas[IDIOMA_PASO_21_NEGRITA]					= "Modificar contrase�a";
		etiquetas[IDIOMA_PASO_21_CUADRO1]					= "Introduzca la contrase�a actual";
		etiquetas[IDIOMA_PASO_21_CUADRO2]					= "Introduzca la nueva contrase�a";
		etiquetas[IDIOMA_PASO_21_CONTRA]					= "Contrase�a:";
		etiquetas[IDIOMA_PASO_21_NUEVA]						= "Nueva contrase�a:";
		etiquetas[IDIOMA_PASO_21_CONF]						= "Confirmaci�n:";

		etiquetas[IDIOMA_PASO_22_NEGRITA]					= "Elija una opci�n";
		etiquetas[IDIOMA_PASO_22_CUADRO]					= "Gestionar certificados";
		etiquetas[IDIOMA_PASO_22_IMP]						= "Importar certificados al  Clauer idCAT";	
		etiquetas[IDIOMA_PASO_22_EXP]						= "Exportar certificados desde el  Clauer idCAT";
		etiquetas[IDIOMA_PASO_22_ELIM]						= "Eliminar certificados del  Clauer idCAT";
		etiquetas[IDIOMA_PASO_22_DISQ]						= "Modificar PIN del disquete de la UJI";
		
		etiquetas[IDIOMA_PASO_23_NEGRITA]					= "Elija una opci�n";
		etiquetas[IDIOMA_PASO_23_CUADRO]					= "Opciones";
		etiquetas[IDIOMA_PASO_23_DISQ]						= "Importar certificado del disquete de la UJI";
		etiquetas[IDIOMA_PASO_23_UB]						= "Importar eth certificado desde un fichero";
		etiquetas[IDIOMA_PASO_23_IE]						= "Importar eth certificat instalat en eth Internet Explorer";

		etiquetas[IDIOMA_PASO_28_TEXT1]						= "Formatear zona criptogr�fica";
		etiquetas[IDIOMA_PASO_28_TEXT2]						= "Esta operaci�n va a eliminar todos los certificados, claves y credenciales que contiene el  Clauer idCAT. No afectar� a la zona de datos.\n\nDespu�s podr� volver a importar los certificados si dispone de una copia.";
		etiquetas[IDIOMA_PASO_28_TEXT3]						= "S�lo podr� utilizar el  Clauer idCATcomo disco duro de bolsillo.";

		etiquetas[IDIOMA_FICHERO_ABIERTO]					= "La aplicaci�n ya est� abierta";

		etiquetas[IDIOMA_CREAR_FICHERO]						= "Error al crear el fichero de bloqueo";

		etiquetas[IDIOMA_NO_ENCONTRAR_STICK]					= "No se pudo determinar la presencia de memorias USB en el sistema.";
		
		etiquetas[IDIOMA_NO_HAY_STICK]						= "No hay ninguna memoria USB insertada en el sistema\nPor favor, pulse el bot�n Aceptar e inserte el dispositivo\nque desee inicializar";

		etiquetas[IDIOMA_MUCHOS_STICKS]						= "Hay m�s de una memoria USB insertada en el sistema\nPor favor, pulse el bot�n Aceptar y deje insertado\n�nicamente el dispostivo que desee inicializar";
	
		etiquetas[IDIOMA_NO_INICIALIZAR]					= "Imposible realizar la inicializaci�n del dispositivo.\n";
		
		etiquetas[IDIOMA_COMPROBANDO_ADMINISTRADOR]				= "Imposible comprobar si el usuario es administrador.\n";
		
		etiquetas[IDIOMA_NO_ADMINISTRADOR]					= "Debe ser administrador para poder preparar un nuevo  Clauer idCAT.";
		
		etiquetas[IDIOMA_IMPOSIBLE_CERRAR_DISP]				= "Imposible cerrar dispositivo.\n";

		etiquetas[IDIOMA_PIN_COINCIDENCIA]					= "La contrase�a y su confirmaci�n deben coincidir.";
		
		etiquetas[IDIOMA_LONGITUD_PIN]						= "Debe introducir una contrase�a de un m�nimo de 8 caracteres.";

		etiquetas[IDIOMA_NO_BASE]							= "Debe tener instalado el software base.";
		
		etiquetas[IDIOMA_NO_IMPLEMENTADO]					= "Opci�n no implementada.";
		
		etiquetas[IDIOMA_TITULO_PIN_INCORRECTO]				= "Gestor  Clauer idCAT:: ERROR :: Contrase�a incorrecta";
		
		etiquetas[IDIOMA_TITULO_NO_CERTIFICADOS]			= "Gestor  Clauer idCAT:: ERROR :: No se encontraron certificados";
		
		etiquetas[IDIOMA_NO_IMPORTAR_CERTIFICADO]			= "No se pudo importar el certificado";
		
		etiquetas[IDIOMA_SELECCIONAR_CERTIFICADO]			= "Debe seleccionar un certificado";

		etiquetas[IDIOMA_CONTRASENYA_MAL]					= "Contrase�a del  Clauer idCATincorrecta\n";

		etiquetas[IDIOMA_TITULO_CONTRASENYA_MAL]			= " Clauer idCAT:: Contrase�a  Clauer idCAT";

		etiquetas[IDIOMA_NO_MODIFICAR_CONTRASENYA]			= "No ha sido posible modificar la contrase�a del  Clauer idCAT\n";

		etiquetas[IDIOMA_NO_FINALIZADO]						= "Error al finalizar dispositivo\n";

		etiquetas[IDIOMA_CONTRASENYA_MODIFICADA]			= "La contrase�a se ha modificado correctamente.\n";

		etiquetas[IDIOMA_CONTRASENYA_NO_CORRECTA]			= "Contrase�a del  Clauer idCATincorrecta\n";

		etiquetas[IDIOMA_CONTRASENYA_IDCAT]					= "Debe formatear la zona criptogr�fica antes de importar certificados al  Clauer idCAT, para ello debe de ser administrador.";

		etiquetas[IDIOMA_TITULO_ERROR_BUSCANDO]				= " Clauer idCAT:: ERROR :: Buscando sticks";

		etiquetas[IDIOMA_NO_ENCONTRADOS_CERTIFICADOS]		= "Gestor  Clauer idCAT:: ERROR :: No se encontraron certificados";

		etiquetas[IDIOMA_CERTIFICADO_OK]					= "El certificado se ha importado correctamente.\n";

		etiquetas[IDIOMA_TITULOS_CERTIFICADOS]				= "Gestor clauer :: Certificados";

		etiquetas[IDIOMA_NO_MODIFICAR_MBR]					="Imposible cambiar particiones del mbr.\n";

		etiquetas[IDIOMA_RETIRAR_STICK]						= "Por favor, retire el stick USB y pulse el bot�n Aceptar\n";

		etiquetas[IDIOMA_TITULO_RETIRAR]					= "Gestor  Clauer idCAT:: Extraiga el stick";

		etiquetas[IDIOMA_INSERTAR_STICK]					= "Por favor, inserte de nuevo el stick y pulse el bot�n Aceptar\n";

		etiquetas[IDIOMA_TITULO_INSERTAR]					= "Gestor  Clauer idCAT:: Inserte el stick";

		etiquetas[IDIOMA_TITULO_ELIMINAR_ZONA_CRIPTO]		= "Gestor  Clauer idCAT:: Formatear zona criptogr�fica";

		etiquetas[IDIOMA_NO_INSERTADO]						= "No ha insertado todav�a su stick USB. Por favor,\n ins�rtelo de nuevo y pulse el bot�n Aceptar";

		etiquetas[IDIOMA_MUCHOS_STICKS_INSERT]				= "El programa ha detectado m�s de un stick USB insertado en el sistema.\n Por favor, deje insertado �nicamente el stick con el que empez� el proceso\n de inicializaci�n.";

		etiquetas[IDIOMA_IMPOSIBLE_FORMATEAR_LOGICA]		= "Imposible formatear unidad l�gica del dispositivo.\n";

		etiquetas[IDIOMA_ERROR_FORMATEANDO]					= " Clauer idCAT:: ERROR :: Formateando";

		etiquetas[IDIOMA_ELIMINADO_CRIPTO]					= "La zona criptogr�fica se ha formateado correctamente.\n";

		etiquetas[IDIOMA_TITULO_CRIPTO]						= "Gestor  Clauer idCAT:: Zona criptogr�fica";

		etiquetas[IDIOMA_PROCESO_FINALIZADO]				= "El proceso termin� correctamente.\nPulse Aceptar";

		etiquetas[IDIOMA_TITULO_FINALIZADO]					= " Clauer idCAT:: Finalizado";

		etiquetas[IDIOMA_NO_CERT_DISQUET]					= "No se pudo leer certificado en la unidad A:\n";

		etiquetas[IDIOMA_PIN_INCORRECTO]					= "Contrase�a incorrecta. Vuelva a introducir la contrase�a.\nCompruebe que respet� may�sculas y min�sculas (si las hubiere)\nPulse el bot�n Aceptar";

		etiquetas[IDIOMA_ERROR_VERIFICANDO_PASS]			= "Ocurri� un error verificando su contrase�a. Int�ntelo de nuevo. \n";

		etiquetas[IDIOMA_ERROR_ENTRADA_SALIDA]				= "Ocurri� un error de E/S.\n";

		etiquetas[IDIOMA_NO_IMPORTAR_PKCS12]				= "No se pudieron importar los PKCS12.";

		etiquetas[IDIOMA_PASS_NO_SEGURA1]					= "La nueva contrase�a no es lo suficientemente segura, int�ntelo con otra.";

		etiquetas[IDIOMA_PASS_NO_SEGURA]					= "La nueva contrase�a no es lo suficientemente segura, int�ntelo con otra.\nHay demasiados n�meros repetidos.";

		etiquetas[IDIOMA_PASS_LETRAS]						= "La nueva contrase�a no es lo suficientemente segura, int�ntelo con otra.\nHay demasiadas letras repetidas.";

		etiquetas[IDIOMA_MIN_LETRAS]						= "La nueva contrase�a no es lo suficientemente segura, int�ntelo con otra.\nDebe emplear como m�nimo 3 letras.";
		
		etiquetas[IDIOMA_MIN_NUM]							= "La nueva contrase�a no es lo suficientemente segura, int�ntelo con otra.\nDebe emplear como m�nimo 3 n�meros.";

		etiquetas[IDIOMA_INICIALIZANDO]						= "Inicializando dispositivo";

		etiquetas[IDIOMA_FORMATEANDO]						= "Formateando dispositivo...";

		etiquetas[IDIOMA_ERROR_GRAVE]						= "Ocurri� un error grave en la aplicaci�n.\n";

		etiquetas[IDIOMA_NO_DETERMINAR_IDIOMA]				= "No se ha podido determinar el idioma.";

		etiquetas[IDIOMA_NO_NT_VIEJO]						= "El Gestor no soporta Windows NT 3.51";

		etiquetas[IDIOMA_ADVERTENCIA_CAMBIO_PIN]			= "Est� utilizando Windows 95/98/Me/NT 4.0\nEl Gestor s�lo le permitir� modificar el PIN del disquete de la UJI";

		etiquetas[IDIOMA_ABRIR_CERTIFICADO]					= "No se pudo abrir el certificado.";
		
		etiquetas[IDIOMA_ESCRIBIR_CERTIFICADOS]				= "No se pudo escribir el certificado.";
		
		etiquetas[IDIOMA_CERRAR_CERTIFICADOS]				= "No se pudo cerrar el certificado.";

		etiquetas[IDIOMA_NO_MODIFICAR_PIN]					= "No se pudo modificar el PIN del disquete.\n";

		etiquetas[IDIOMA_PIN_MODIFICADO]					= "PIN del disquete modificado correctamente.";

		etiquetas[IDIOMA_NO_LEER_CERTIFICADO]				= "No se pudo leer el certificado\n";

		etiquetas[IDIOMA_DELETE_NEGRITA]					="Certificats a eliminar del  Clauer idCAT";

		etiquetas[IDIOMA_DELETE_EXPLICACION]				="Seleccioni el certificat que es troba en el  Clauer idCATque desitja eliminar";

		etiquetas[IDIOMA_PASO_23_DESC] = "Importar certificado instalado en Internet Explorer";

		etiquetas[IDIOMA_PASO_25_NEGRITA] = "Indica la ubicaci�n del fichero que incluye el certificado que quieres importar al  Clauer idCAT";

		etiquetas[IDIOMA_PASO_31_NEGRITA_IMP] = "Selecciona el certificado que quieres importar dentro del  Clauer idCAT";

		etiquetas[IDIOMA_PASO_31_NEGRITA_EXP] = "Selecciona el certificado que quieres exportar al Internet Explorer";

		etiquetas[IDIOMA_PASO_31_DESP_EXP] = "Exportar del  Clauer idCAT a Internet Explorer";

		etiquetas[IDIOMA_PASO_31_DESP_IMP] = "Importar de Internet Explorer a  Clauer idCAT";

		etiquetas[IDIOMA_PASO_32_NEGRITA] = "Escoje donde quieres exportar el certificado que esta dentro del  Clauer idCAT.";

		etiquetas[IDIOMA_PASO_32_DESP_1] = "Exportar a un fichero";

		etiquetas[IDIOMA_PASO_32_CUADRO] = "Opciones";

		etiquetas[IDIOMA_PASO_32_DESP_2] = "Exportar a Internet Explorer";

		etiquetas[IDIOMA_PASO_33_NEGRITA] = "Eliminar certificados del  Clauer idCAT";

		etiquetas[IDIOMA_PASO_33_OK] = "Certificado borrado correctamente";

		etiquetas[IDIOMA_PASO_33_DESP_1] = "Selecciona el certificado que desea eliminar del  Clauer idCAT";

		etiquetas[IDIOMA_PASO_34_NEGRITA] = "Selecciona el certificado del  Clauer idCAT que quieres exportar a fichero. El nuevo fichero tendr� la misma contrase�a que el Clauer idCAT.";

		etiquetas[IDIOMA_PASO_34_DESP_1] = "Exportaci�n del  Clauer idCAT a fichero";

		etiquetas[IDIOMA_PASO_34_DESP_2] = "Certificado a exportar:";

		etiquetas[IDIOMA_PASO_34_DESP_3] = "Fichero de destino";

		etiquetas[IDIOMA_OK_EXP] = "La exportaci�n se ha realizado con exito";

		etiquetas[IDIOMA_OK_IMP] = "La importaci�n se ha realizado con exito";

		etiquetas[IDIOMA_BOTON_COPIAR] = "Copiar";

		etiquetas[IDIOMA_BOTON_MOVER] = "Mover";	

		etiquetas[IDIOMA_ERROR_IMP] = "La importaci�n no se ha realizado con exito. Asegurate que que el certificado es exportable";
		
		etiquetas[IDIOMA_ERROR_EXP] = "La exportaci�n no se ha realizado con exito. Asegurate que que el certificado es exportable";
	}
	return TRUE;

}


BOOL IDIOMA_Descargar (void)
{
	if ( idioma ) {
		delete [] idioma;
		idioma = NULL;
	}

	return TRUE;
}




LPSTR IDIOMA_Get (DWORD etiqueta)
{
	if ( !idioma )
		return NULL;

	if ( etiqueta > (NUM_ETIQUETAS - 1) )
		return NULL;

	return etiquetas[etiqueta];
}



BOOL IDIOMA_Cargado (void)
{

	return idioma != NULL;


}

