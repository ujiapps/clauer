// DlgPaso5_3.cpp : implementation file
//

#include "stdafx.h"
#include "Admin_CLAUER.h"
#include "DlgPaso5_3.h"
#include "Admin_CLAUERDlg.h"
#include "IDIOMA.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// DlgPaso5_3 dialog


DlgPaso5_3::DlgPaso5_3(CWnd* pParent /*=NULL*/)
	: CDialog(DlgPaso5_3::IDD, pParent)
{
	//{{AFX_DATA_INIT(DlgPaso5_3)
	m_pin = _T("");
	//}}AFX_DATA_INIT
}


void DlgPaso5_3::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(DlgPaso5_3)
	DDX_Control(pDX, IDC_DESCRIPCION, m_cuadro);
	DDX_Control(pDX, IDC_CONTR, m_contr);
	DDX_Control(pDX, IDC_TEXT, m_texto);
	DDX_Text(pDX, IDC_PASSWORD, m_pin);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(DlgPaso5_3, CDialog)
	//{{AFX_MSG_MAP(DlgPaso5_3)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// DlgPaso5_3 message handlers

CString DlgPaso5_3::GetPIN()
{
	UpdateData(TRUE);
	return m_pin;
}

BOOL DlgPaso5_3::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here
	CFont *font = new CFont;
	font->CreatePointFont(12, "System");
	m_texto.SetFont(font);

	m_texto.SetWindowText(IDIOMA_Get(IDIOMA_PASO5_3_NEGRITA));
	m_cuadro.SetWindowText(IDIOMA_Get(IDIOMA_PASO5_3_CUADRO));
	m_contr.SetWindowText(IDIOMA_Get(IDIOMA_PASO5_3_CONTR));

	delete font;
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

BOOL DlgPaso5_3::PreTranslateMessage(MSG* pMsg) 
{
	// TODO: Add your specialized code here and/or call the base class
	if(pMsg->message==WM_KEYDOWN)
	{
		if ( (pMsg->wParam==VK_ESCAPE) )
			pMsg->wParam=NULL ;
		else if ( (pMsg->wParam==VK_RETURN) ) {
			((CAdmin_CLAUERDlg *) this->GetParent())->OnBotonSiguiente();
			//((CAdmin_CLAUERDlg *) AfxGetApp())->OnBotonSiguiente();
			return TRUE;
		}
	}
	return CDialog::PreTranslateMessage(pMsg);
}
