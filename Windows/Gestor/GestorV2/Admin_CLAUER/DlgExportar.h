#pragma once
#include "afxwin.h"




// Cuadro de di�logo de DlgExportar

class DlgExportar : public CDialog
{
	DECLARE_DYNAMIC(DlgExportar)

public:
	DlgExportar(CWnd* pParent = NULL);   // Constructor est�ndar
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	virtual ~DlgExportar();

// Datos del cuadro de di�logo
	enum { IDD = IDD_DIALOG_EXPORTAR };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // Compatibilidad con DDX/DDV
	virtual BOOL OnInitDialog();
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedRadio1();
public:
	int m_radio;
public:
	afx_msg void OnBnClickedRadio2();
public:
	afx_msg void OnShowWindow(BOOL bShow, UINT nStatus);
public:
	CStatic m_texto;
	CStatic m_texto2;
	CStatic m_texto3;
};
