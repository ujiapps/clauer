// DlgExport.cpp: archivo de implementaci�n
//

#include "stdafx.h"
#include "Admin_CLAUER.h"
#include "DlgExport.h"
#include ".\dlgexport.h"


// Cuadro de di�logo de DlgExport

IMPLEMENT_DYNAMIC(DlgExport, CDialog)
DlgExport::DlgExport(CWnd* pParent /*=NULL*/)
	: CDialog(DlgExport::IDD, pParent)
{
}

DlgExport::~DlgExport()
{
}

void DlgExport::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_RADIO1, m_PKCS12);
	DDX_Control(pDX, IDCANCEL, m_CRYF);
}


BEGIN_MESSAGE_MAP(DlgExport, CDialog)
	ON_WM_ACTIVATE()
	ON_WM_CLOSE()
	ON_WM_KILLFOCUS()
	ON_BN_CLICKED(IDC_RADIO1, OnBnClickedRadio1)
	ON_BN_CLICKED(IDC_RADIO2, OnBnClickedRadio2)
END_MESSAGE_MAP()


// Controladores de mensajes de DlgExport

void DlgExport::OnActivate(UINT nState, CWnd* pWndOther, BOOL bMinimized)
{
	CDialog::OnActivate(nState, pWndOther, bMinimized);

	// TODO: Agregue aqu� su c�digo de controlador de mensajes
	m_PKCS12.SetCheck(BST_CHECKED);
	//m_checked=0;
	//UpdateData(FALSE);
}

void DlgExport::OnKillFocus(CWnd* pNewWnd)
{
	this->UpdateData(FALSE);
	CDialog::OnKillFocus(pNewWnd);

	// TODO: Agregue aqu� su c�digo de controlador de mensajes
}

void DlgExport::OnBnClickedRadio1()
{
	// TODO: Agregue aqu� su c�digo de controlador de notificaci�n de control
	m_checked=0;
}

void DlgExport::OnBnClickedRadio2()
{
	// TODO: Agregue aqu� su c�digo de controlador de notificaci�n de control
	m_checked=1;
}
