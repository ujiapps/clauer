// DlgExportar.cpp: archivo de implementaci�n
//

#include "stdafx.h"
#include "Admin_CLAUER.h"
#include "DlgExportar.h"

#include "IDIOMA.h"

#include "Admin_CLAUERDlg.h"

// Cuadro de di�logo de DlgExportar

IMPLEMENT_DYNAMIC(DlgExportar, CDialog)

DlgExportar::DlgExportar(CWnd* pParent /*=NULL*/)
	: CDialog(DlgExportar::IDD, pParent)
	, m_radio(0)
{

}


DlgExportar::~DlgExportar()
{
}

void DlgExportar::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Radio(pDX, IDC_RADIO1, m_radio);
	DDX_Control(pDX, IDC_DESC_NEGRE, m_texto);
	DDX_Control(pDX, IDC_RADIO_FICHERO, m_texto2);
	DDX_Control(pDX, IDC_RADIO_IE, m_texto3);
}


BOOL DlgExportar::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here

	m_radio = 0;
	UpdateData(FALSE);
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

BEGIN_MESSAGE_MAP(DlgExportar, CDialog)
	ON_BN_CLICKED(IDC_RADIO1, &DlgExportar::OnBnClickedRadio1)
	ON_BN_CLICKED(IDC_RADIO2, &DlgExportar::OnBnClickedRadio2)
	ON_WM_SHOWWINDOW()
END_MESSAGE_MAP()


// Controladores de mensajes de DlgExportar

void DlgExportar::OnBnClickedRadio1()
{
	// TODO: Agregue aqu� su c�digo de controlador de notificaci�n de control
}

void DlgExportar::OnBnClickedRadio2()
{
	// TODO: Agregue aqu� su c�digo de controlador de notificaci�n de control
}

BOOL DlgExportar::PreTranslateMessage(MSG* pMsg) 
{
	// TODO: Add your specialized code here and/or call the base class
	if(pMsg->message==WM_KEYDOWN)
	{
		switch (pMsg->wParam) {

		case VK_ESCAPE:
			pMsg->wParam=NULL ;
			break;

		case VK_RETURN:

			((CAdmin_CLAUERDlg *) this->GetParent())->OnBotonSiguiente();
			pMsg->wParam = NULL;
			return TRUE;
			break;

		}
	}
	return CDialog::PreTranslateMessage(pMsg);
}


void DlgExportar::OnShowWindow(BOOL bShow, UINT nStatus)
{
	CDialog::OnShowWindow(bShow, nStatus);

	// TODO: Agregue aqu� su c�digo de controlador de mensajes

	CFont *font = new CFont;
	font->CreatePointFont(12, "System");
	m_texto.SetFont(font);
	m_texto.SetWindowText(IDIOMA_Get(IDIOMA_PASO_32_NEGRITA));
	m_texto2.SetWindowText(IDIOMA_Get(IDIOMA_PASO_32_DESP_1));
	m_texto3.SetWindowText(IDIOMA_Get(IDIOMA_PASO_32_DESP_2));
	delete font;
}
