#if !defined(AFX_THREAD_H__F1E5F92D_A7FB_45F5_A27C_159090A54943__INCLUDED_)
#define AFX_THREAD_H__F1E5F92D_A7FB_45F5_A27C_159090A54943__INCLUDED_

#include "DlgPaso3.h"

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// thread.h : header file
//



/////////////////////////////////////////////////////////////////////////////
// thread thread

class thread : public CWinThread
{
	DECLARE_DYNCREATE(thread)
protected:
	           // protected constructor used by dynamic creation

// Attributes
public:

// Operations
public:
	void SetPINClauer(CString pin);
	CString GetMsgError();
	BOOL GetError();
	BOOL Terminado();
	thread();           // protected constructor used by dynamic creation
	virtual ~thread();
	int Run();
	void SetDlg(DlgPaso3 *dlgPaso3);
// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(thread)
	public:
	virtual BOOL InitInstance();
	virtual int ExitInstance();
	//}}AFX_VIRTUAL

// Implementation
protected:
	CString m_strPINClauer;
	CString m_strErrMsg;
	BOOL m_bError;
	BOOL m_bFinished;
	DlgPaso3 *m_dlgPaso3;

	// Generated message map functions
	//{{AFX_MSG(thread)
		// NOTE - the ClassWizard will add and remove member functions here.
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_THREAD_H__F1E5F92D_A7FB_45F5_A27C_159090A54943__INCLUDED_)
