// DlgExportarPkcs12.cpp: archivo de implementación
//

#include "stdafx.h"
#include "Admin_CLAUER.h"
#include "DlgExportarPkcs12.h"

#include "Admin_CLAUERDlg.h"

#include "IDIOMA.h"

#include <librt/librt.h>
#include <cryptowrapper/cryptowrap.h>


// Cuadro de diálogo de DlgExportarPkcs12

IMPLEMENT_DYNAMIC(DlgExportarPkcs12, CDialog)

DlgExportarPkcs12::DlgExportarPkcs12(CWnd* pParent /*=NULL*/)
	: CDialog(DlgExportarPkcs12::IDD, pParent)
	, m_editPkcs12File(_T(""))
{

}

DlgExportarPkcs12::~DlgExportarPkcs12()
{
}

void DlgExportarPkcs12::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_EDIT_PKCS12_FILE, m_editPkcs12File);
	DDX_Control(pDX, IDC_COMBO_CERTS, m_cboCerts);
	DDX_Control(pDX, 1083, m_texto);
	DDX_Control(pDX, IDC_CUADRO_PKCS12, m_texto2);
	DDX_Control(pDX, IDC_DESTINO, n_texto3);
	DDX_Control(pDX, IDC_A_EXPORTAR, m_texto4);
}


BEGIN_MESSAGE_MAP(DlgExportarPkcs12, CDialog)
	ON_BN_CLICKED(IDC_BUTTON1, &DlgExportarPkcs12::OnBnClickedButton1)
	ON_WM_SHOWWINDOW()
END_MESSAGE_MAP()


// Controladores de mensajes de DlgExportarPkcs12

void DlgExportarPkcs12::OnBnClickedButton1()
{

	CFileDialog * dlgFile = NULL;
	dlgFile = new CFileDialog(
		FALSE,
		_T("p12"),
		NULL,
		OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT,
		_T("Pkcs12 files (*.p12)|*.p12||"),
		this,
		0);
	if ( ! dlgFile )
		return;

	if ( dlgFile->DoModal() == IDOK ) {
		CString strFileName = dlgFile->GetPathName();
		this->m_editPkcs12File = dlgFile->GetPathName();
		
	}

	delete dlgFile;

	UpdateData(FALSE);
	
}

// Carga la lista de certificados disponibles
int DlgExportarPkcs12::LoadCertCombo(void)
{
	USBCERTS_HANDLE hClauer;
	unsigned char bloque[TAM_BLOQUE];
	int nDispositivos,i;
	unsigned char * d[MAX_DEVICES];

	// Enumero certificados a borrar

	memset(d,0,MAX_DEVICES * sizeof(unsigned char *));

	if ( LIBRT_ListarDispositivos(&nDispositivos,d) != 0 ) 
		return FALSE;
	if ( nDispositivos == 0 ) 
		return FALSE;

	if ( LIBRT_IniciarDispositivo(d[0], NULL, &hClauer) != 0 ) {
		for ( i = 0 ; *(d+i) ; i++ )
			free(d[i]);
		return FALSE;
	}

	for ( i = 0 ; *(d+i) ; i++ )
		free(d[i]);

	unsigned char *cert = NULL;
	unsigned long tamCert;
	long nBloque;

	if ( LIBRT_LeerTipoBloqueCrypto(&hClauer, BLOQUE_CERT_PROPIO, 1, bloque, &nBloque) != 0 ) {
		LIBRT_FinalizarDispositivo(&hClauer);
		return FALSE;
	}
	while ( nBloque != -1 ) {

		cert    = BLOQUE_CERTPROPIO_Get_Objeto(bloque);
		tamCert = BLOQUE_CERTPROPIO_Get_Tam(bloque);

		DN *subject = CRYPTO_DN_New();
		if ( ! subject ) {
			LIBRT_FinalizarDispositivo(&hClauer);
			return FALSE;
		}
		DN *issuer = CRYPTO_DN_New();
		if ( ! issuer ) {
			LIBRT_FinalizarDispositivo(&hClauer);
			return FALSE;
		}
		if ( ! CRYPTO_CERT_SubjectIssuer(cert, tamCert, subject, issuer)) {
			LIBRT_FinalizarDispositivo(&hClauer);
			return FALSE;
		}

		// inserto el cert en la lista
		CString strCert;
		char *friendlyName = BLOQUE_CERTPROPIO_Get_FriendlyName(bloque);
		strCert = subject->CN;
		if ( friendlyName )
			strCert = strCert + CString(" (") + friendlyName + ")";

		int index = m_cboCerts.InsertString(m_cboCerts.GetCount(), strCert);
		if ( index >= 0 ) {
			m_mCboBlock.SetAt(index, nBloque);
		}

		// espero que la liberación no afecte a la lista
		// de certificados

		CRYPTO_DN_Free(subject);
		CRYPTO_DN_Free(issuer);

		// Ala, siguiente

		if ( LIBRT_LeerTipoBloqueCrypto(&hClauer, BLOQUE_CERT_PROPIO, 0, bloque, &nBloque) != 0 ) {
			LIBRT_FinalizarDispositivo(&hClauer);
			return FALSE;
		}
	}

	LIBRT_FinalizarDispositivo(&hClauer);

	m_cboCerts.SetCurSel(0);

	return TRUE;
}

void DlgExportarPkcs12::OnShowWindow(BOOL bShow, UINT nStatus)
{
	CDialog::OnShowWindow(bShow, nStatus);

	LoadCertCombo();

	CFont *font = new CFont;
	font->CreatePointFont(12, "System");
	m_texto.SetFont(font);
	m_texto.SetWindowText(IDIOMA_Get(IDIOMA_PASO_34_NEGRITA));
	m_texto2.SetWindowText(IDIOMA_Get(IDIOMA_PASO_34_DESP_1));
	m_texto4.SetWindowText(IDIOMA_Get(IDIOMA_PASO_34_DESP_2));
	n_texto3.SetWindowText(IDIOMA_Get(IDIOMA_PASO_34_DESP_3));
	delete font;
}


BOOL DlgExportarPkcs12::GetSelectedCertBlock ( int &selCertBlock )
{
	//int selIndex = 0;
	
	this->UpdateData(FALSE);

	int ret = this->m_cboCerts.GetCurSel();	
	/*while ( ret == 0 ) {
		++selIndex;
		ret = this->m_cboCerts.GetCurSel(selIndex);
	}*/
	selCertBlock = this->m_mCboBlock[ret];
	return TRUE;
}



BOOL DlgExportarPkcs12::GetSelectedFile ( CString &strPkcs12 )
{
	this->UpdateData(FALSE);
	strPkcs12 = m_editPkcs12File;
	return TRUE;
}

BOOL DlgExportarPkcs12::PreTranslateMessage(MSG* pMsg) 
{
	// TODO: Add your specialized code here and/or call the base class
	if(pMsg->message==WM_KEYDOWN)
	{
		switch (pMsg->wParam) {

		case VK_ESCAPE:
			pMsg->wParam=NULL ;
			break;

		case VK_RETURN:

			((CAdmin_CLAUERDlg *) this->GetParent())->OnBotonSiguiente();
			pMsg->wParam = NULL;
			return TRUE;
			break;

		}
	}
	return CDialog::PreTranslateMessage(pMsg);
}


