// DlgPaso2.cpp : implementation file
//

#include "stdafx.h"
#include "Admin_CLAUER.h"
#include "DlgPaso2.h"
#include "Admin_CLAUERDlg.h"
#include "IDIOMA.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// DlgPaso2 dialog


DlgPaso2::DlgPaso2(CWnd* pParent /*=NULL*/)
	: CDialog(DlgPaso2::IDD, pParent)
{
	//{{AFX_DATA_INIT(DlgPaso2)
	m_editPIN = _T("");
	m_editConfirmation = _T("");
	//}}AFX_DATA_INIT
}


void DlgPaso2::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(DlgPaso2)
	DDX_Control(pDX, IDC_CUADRO, m_cuadro);
	DDX_Control(pDX, IDC_PIN, m_pin);
	DDX_Control(pDX, IDC_CONFIRMACION, m_conf);
	DDX_Control(pDX, IDC_LBL_CREAR, m_texto);
	DDX_Text(pDX, IDC_EDIT_PIN, m_editPIN);
	DDX_Text(pDX, IDC_EDIT_CONFIRMACION, m_editConfirmation);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(DlgPaso2, CDialog)
	//{{AFX_MSG_MAP(DlgPaso2)
	ON_WM_SHOWWINDOW()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// DlgPaso2 message handlers


CString DlgPaso2::GetConfirmacion()
{
	UpdateData(TRUE);
	return m_editConfirmation;
}

CString DlgPaso2::GetPIN()
{
	UpdateData(TRUE);
	return m_editPIN;
}

void DlgPaso2::OnShowWindow(BOOL bShow, UINT nStatus) 
{
	byte *dispos = NULL;

//	int nDispositivos;

	CDialog::OnShowWindow(bShow, nStatus);
	// TODO: Add your message handler code here
	CFont *font = new CFont;
	font->CreatePointFont(12, "System");
	m_texto.SetFont(font);
	m_texto.SetWindowText(IDIOMA_Get(IDIOMA_PASO2_NEGRITA));
	m_conf.SetWindowText(IDIOMA_Get(IDIOMA_PASO2_CONF));
	m_pin.SetWindowText(IDIOMA_Get(IDIOMA_PASO2_CONTRAS));
	m_cuadro.SetWindowText(IDIOMA_Get(IDIOMA_PASO2_CUADRO));
	delete font;
}

BOOL DlgPaso2::PreTranslateMessage(MSG* pMsg) 
{
	// TODO: Add your specialized code here and/or call the base class
	if(pMsg->message==WM_KEYDOWN)
	{
		switch (pMsg->wParam) {

		case VK_ESCAPE:
			pMsg->wParam=NULL ;
			break;

		case VK_RETURN:

			((CAdmin_CLAUERDlg *) this->GetParent())->OnBotonSiguiente();
			pMsg->wParam = NULL;
			return TRUE;
			break;

		}
	}
	return CDialog::PreTranslateMessage(pMsg);
}
