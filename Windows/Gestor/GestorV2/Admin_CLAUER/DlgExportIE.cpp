// DlgExportIE.cpp: archivo de implementaci�n
//

#include "stdafx.h"
#include "Admin_CLAUER.h"
#include "DlgExportIE.h"
#include ".\dlgexportie.h"

#include <wincrypt.h>
#include <tchar.h>
#include <windows.h>


// Cuadro de di�logo de DlgExportIE

IMPLEMENT_DYNAMIC(DlgExportIE, CDialog)
DlgExportIE::DlgExportIE(CWnd* pParent /*=NULL*/)
	: CDialog(DlgExportIE::IDD, pParent)
{
}

DlgExportIE::~DlgExportIE()
{
}

void DlgExportIE::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LIST3, m_ListExportIE);
}


BEGIN_MESSAGE_MAP(DlgExportIE, CDialog)
	ON_WM_SHOWWINDOW()
END_MESSAGE_MAP()


// Controladores de mensajes de DlgExportIE

void DlgExportIE::OnShowWindow(BOOL bShow, UINT nStatus)
{
	CDialog::OnShowWindow(bShow, nStatus);

	// TODO: Agregue aqu� su c�digo de controlador de mensajes

	HCERTSTORE hStore = 0;
	PCCERT_CONTEXT   pCertContext=NULL;
	HANDLE           hCertStore;

	char pszNameString[256];

	CString dato;

	//hStore = CertOpenStore(CERT_STORE_PROV_SYSTEM_REGISTRY, 0, 0, CERT_STORE_OPEN_EXISTING_FLAG | CERT_SYSTEM_STORE_CURRENT_USER, L"My");
	hStore = CertOpenSystemStore(0, _T("MY"));
	if ( ! hStore ) {

		//ERROR

	}

	while(pCertContext= CertEnumCertificatesInStore(hStore, pCertContext)) {

		if(CertGetNameString(pCertContext, CERT_NAME_SIMPLE_DISPLAY_TYPE, 0, NULL, pszNameString, 128)) {

			dato = pszNameString;
			CertGetNameString(pCertContext, CERT_NAME_RDN_TYPE, 0, NULL, pszNameString, 128);
			dato.Append(" - ");
			dato.Append(pszNameString);
			dato.Append(" - ");
			CertGetNameString(pCertContext, CERT_NAME_FRIENDLY_DISPLAY_TYPE, 0, NULL, pszNameString, 128);
			dato.Append(pszNameString);
			m_ListExportIE.AddString(dato.GetBuffer(0));

		}else {
	          
			//ERROR 

		}
	}
  CertCloseStore(hStore, 0);
  hStore = 0;


}
