#pragma once
#include "afxwin.h"


// Cuadro de di�logo de DlgExportIE

class DlgExportIE : public CDialog
{
	DECLARE_DYNAMIC(DlgExportIE)

public:
	DlgExportIE(CWnd* pParent = NULL);   // Constructor est�ndar
	virtual ~DlgExportIE();

// Datos del cuadro de di�logo
	enum { IDD = IDD_DIALOG_EXPORT_IE };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // Compatibilidad con DDX o DDV

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnShowWindow(BOOL bShow, UINT nStatus);
	CListBox m_ListExportIE;
};
