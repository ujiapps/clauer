// thread.cpp : implementation file
//

#include "stdafx.h"
#include "Admin_CLAUER.h"
#include "thread.h"
#include "IDIOMA.h"
#include "Admin_CLAUERDlg.h"
#include "log.h"

#include <libFormat/usb_format.h>
#include <LIBIMPORT/LIBIMPORT.h>


#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// thread

IMPLEMENT_DYNCREATE(thread, CWinThread)

thread::thread()
{
	this->m_bAutoDelete = FALSE;
}

thread::~thread()
{
}

BOOL thread::InitInstance()
{
	// TODO:  perform and per-thread initialization here
	m_bFinished=FALSE;
	m_bError=FALSE;
	return TRUE;
}

int thread::ExitInstance()
{
	// TODO:  perform any per-thread cleanup here
	return CWinThread::ExitInstance();
}

BEGIN_MESSAGE_MAP(thread, CWinThread)
	//{{AFX_MSG_MAP(thread)
	// NOTE - the ClassWizard will add and remove mapping macros here.
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// thread message handlers

void thread::SetDlg(DlgPaso3 *dlgPaso3)
{
	this->m_dlgPaso3 = dlgPaso3;
}

int thread::Run()
{
	int retval = 0;
	int err=0;
	int numDispositivos;
	unsigned char * d[MAX_DEVICES];
	char ** unidades = NULL;
	int *dispositivos = NULL;

	CString msg;

	m_bError = FALSE;

	m_dlgPaso3->SetPaso(100/5);

	m_dlgPaso3->SetDescripcion(IDIOMA_Get(IDIOMA_INICIALIZANDO));

	/* Formateamos
	*/
	LOG_Msg(1,"Entramos en formateo global, Thread de formateo.");
	while ( 1 ) {

		m_dlgPaso3->Paso();


		LOG_Msg(1,"Listando los dispositivos conectados en el sistema.");
		if((err=LIBRT_ListarUSBs(&numDispositivos,d))!= ERR_LIBRT_NO) {
            LOG_Error(1,"Error en LIBRT_ListarUSBs devolvio %d", err);
			m_bError = TRUE;
			m_strErrMsg = IDIOMA_Get(IDIOMA_ERROR_LISTARUSBS);

			retval = 1;
			goto finRun;

		}

		m_dlgPaso3->Paso();

		LOG_Msg(1,"Ejecutando funcion LIBRT_CrearClauer.");
		if((err=LIBRT_CrearClauer ( (char *)d[0], 0)) != ERR_LIBRT_NO) {
			LOG_Error(1,"Error en LIBRT_CrearClauer devolvio %d", err);
			m_bError = TRUE;
			m_strErrMsg = IDIOMA_Get(IDIOMA_ERROR_CREARCLAUER);
			retval = 1;
			goto finRun;


		}
		LOG_Msg(1,"Funcion LIBRT_CrearClauer ejecutada correctamente.");
		m_dlgPaso3->Paso();

		m_dlgPaso3->SetDescripcion(IDIOMA_Get(IDIOMA_FORMATEANDO));

		LOG_Msg(1,"Ejecutando funcion LIBRT_FormatearClauerDatos.");
		if((err=LIBRT_FormatearClauerDatos( (char *)d[0])) != ERR_LIBRT_NO) {
			LOG_Error(1,"Error en LIBRT_FormatearClauerDatos devolvio %d", err);
			m_bError = TRUE;
			m_strErrMsg = IDIOMA_Get(IDIOMA_ERROR_FORMATEARCLAUERDATOS);

			retval = 1;
			goto finRun;


		}
		LOG_Msg(1,"Funcion LIBRT_FormatearClauerDatos ejecutada correctamente.");

		m_dlgPaso3->Paso();

		LOG_Msg(1,"Creando zona criptografica LIBRT_FormatearClauerCrypto.");
		if((err=LIBRT_FormatearClauerCrypto((char *)d[0],m_strPINClauer.GetBuffer(0))) != ERR_LIBRT_NO) {
		    LOG_Error(1,"Error en LIBRT_FormatearClauerCrypto devolvio %d", err);
			m_bError = TRUE;
			m_strErrMsg = IDIOMA_Get(IDIOMA_ERROR_FORMATEARCLAUERCRYPTO);

			retval = 1;
			goto finRun;


		}


		LOG_Msg(1,"Funcion LIBRT_FormatearClauerCrypto ejecutada correctamente.");
		m_dlgPaso3->Paso();
		LOG_Msg(1,"Saliendo correctamente de la funcion de formateo global.");
finRun:
	
		m_bFinished = TRUE;
		return retval;
	}
}

BOOL thread::Terminado()
{
	return m_bFinished;
}


BOOL thread::GetError()
{
	return m_bError;
}

CString thread::GetMsgError()
{
	return m_strErrMsg;
}

void thread::SetPINClauer(CString pin)
{
	m_strPINClauer = pin;
}