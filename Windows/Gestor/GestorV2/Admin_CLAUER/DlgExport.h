#pragma once
#include "afxwin.h"


// Cuadro de di�logo de DlgExport

class DlgExport : public CDialog
{
	DECLARE_DYNAMIC(DlgExport)

public:
	DlgExport(CWnd* pParent = NULL);   // Constructor est�ndar
	virtual ~DlgExport();

// Datos del cuadro de di�logo
	enum { IDD = IDD_DIALOG_EXPORT };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // Compatibilidad con DDX o DDV

	DECLARE_MESSAGE_MAP()
public:
	int m_radio;
	afx_msg void OnActivate(UINT nState, CWnd* pWndOther, BOOL bMinimized);
	CButton m_PKCS12;
	CButton m_CRYF;
	int m_checked;
	afx_msg void OnKillFocus(CWnd* pNewWnd);
	afx_msg void OnBnClickedRadio1();
	afx_msg void OnBnClickedRadio2();
};
