// DlgPaso21.cpp : implementation file
//

#include "stdafx.h"
#include "Admin_CLAUER.h"
#include "DlgPaso21.h"
#include "Admin_CLAUERDlg.h"
#include "IDIOMA.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// DlgPaso21 dialog


DlgPaso21::DlgPaso21(CWnd* pParent /*=NULL*/)
	: CDialog(DlgPaso21::IDD, pParent)
{
	//{{AFX_DATA_INIT(DlgPaso21)
	m_pinconf = _T("");
	m_pinnuevo = _T("");
	m_pinviejo = _T("");
	//}}AFX_DATA_INIT
}


void DlgPaso21::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(DlgPaso21)
	DDX_Control(pDX, IDC_PIN, m_pin);
	DDX_Control(pDX, IDC_DESC2, m_cuadro2);
	DDX_Control(pDX, IDC_DESC1, m_cuadro1);
	DDX_Control(pDX, IDC_CONT, m_cont);
	DDX_Control(pDX, IDC_CONF, m_conf);
	DDX_Control(pDX, IDC_TEXT, m_texto);
	DDX_Text(pDX, PIN_CONFIR, m_pinconf);
	DDX_Text(pDX, PIN_NUEVO, m_pinnuevo);
	DDX_Text(pDX, PIN_VIEJO, m_pinviejo);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(DlgPaso21, CDialog)
	//{{AFX_MSG_MAP(DlgPaso21)
	ON_WM_SHOWWINDOW()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// DlgPaso21 message handlers
CString DlgPaso21::GetConfirmacion()
{
	UpdateData(TRUE);
	return m_pinconf;
}

CString DlgPaso21::GetPIN()
{
	UpdateData(TRUE);
	return m_pinnuevo;
}

CString DlgPaso21::GetViejo()
{
	UpdateData(TRUE);
	return m_pinviejo;
}

void DlgPaso21::OnShowWindow(BOOL bShow, UINT nStatus) 
{
	CDialog::OnShowWindow(bShow, nStatus);
	
	// TODO: Add your message handler code here
	CFont *font = new CFont;
	font->CreatePointFont(12, "System");
	m_texto.SetFont(font);
	delete font;

	m_texto.SetWindowText(IDIOMA_Get(IDIOMA_PASO_21_NEGRITA));
	m_cuadro1.SetWindowText(IDIOMA_Get(IDIOMA_PASO_21_CUADRO1));
	m_cuadro2.SetWindowText(IDIOMA_Get(IDIOMA_PASO_21_CUADRO2));
	m_cont.SetWindowText(IDIOMA_Get(IDIOMA_PASO_21_CONTRA));
	m_pin.SetWindowText(IDIOMA_Get(IDIOMA_PASO_21_NUEVA));
	m_conf.SetWindowText(IDIOMA_Get(IDIOMA_PASO_21_CONF));

}

BOOL DlgPaso21::PreTranslateMessage(MSG* pMsg) 
{
	// TODO: Add your specialized code here and/or call the base class
	if(pMsg->message==WM_KEYDOWN)
	{
		if ( (pMsg->wParam==VK_ESCAPE) )
			pMsg->wParam=NULL ;
		else if ( (pMsg->wParam==VK_RETURN) ) {
			((CAdmin_CLAUERDlg *) this->GetParent())->OnBotonSiguiente();
			return TRUE;
		}
	}
	return CDialog::PreTranslateMessage(pMsg);
}
