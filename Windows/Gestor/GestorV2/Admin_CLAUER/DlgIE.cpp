// DlgIE.cpp: archivo de implementaci�n
//

#include "stdafx.h"
#include "Admin_CLAUER.h"
#include "DlgIE.h"
#include "Admin_CLAUERDlg.h"
#include "IDIOMA.h"

#include "gestorimport.h"

#include <LIBRT/LIBRT.h>
#include <CRYPTOWrapper/CRYPTOWrap.h>

#include ".\dlgie.h"

#include <wincrypt.h>
#include <tchar.h>
#include <windows.h>

// Cuadro de di�logo de DlgIE

IMPLEMENT_DYNAMIC(DlgIE, CDialog)
DlgIE::DlgIE(CWnd* pParent /*=NULL*/)
	: CDialog(DlgIE::IDD, pParent)
{
}

DlgIE::~DlgIE()
{
}

void DlgIE::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_COMBO2, m_combo);
	DDX_Control(pDX, IDC_COMBO1, m_comboClauer);
	DDX_Control(pDX, IDC_TEXTOie, m_texto);
	DDX_Control(pDX, IDC_COMBO, m_texto2);
	DDX_Control(pDX, IDC_BUTTON3, m_botoMover);
	DDX_Control(pDX, IDC_BUTTON4, m_botoCopiar);
}


BEGIN_MESSAGE_MAP(DlgIE, CDialog)
	ON_WM_SHOWWINDOW()
	ON_CBN_SELCHANGE(IDC_COMBO2, OnCbnSelchangeCombo2)
	ON_CBN_SELCHANGE(IDC_COMBO1, OnCbnSelchangeCombo1)
	ON_BN_CLICKED(IDC_BUTTON4, OnBnClickedButton4)
	ON_BN_CLICKED(IDC_BUTTON2, OnBnClickedButton2)
	ON_STN_CLICKED(1083, &DlgIE::OnStnClicked1083)
	ON_BN_CLICKED(IDC_BUTTON3, &DlgIE::OnBnClickedButton3)
END_MESSAGE_MAP()


// Controladores de mensajes de DlgIE

void DlgIE::OnShowWindow(BOOL bShow, UINT nStatus)
{
	CDialog::OnShowWindow(bShow, nStatus);

	// TODO: Agregue aqu� su c�digo de controlador de mensajes
	HCERTSTORE hStore = 0;
	PCCERT_CONTEXT   pCertContext=NULL;
//	HANDLE           hCertStore;

unsigned char fingerprint[20];
DWORD dwFinger;

dwFinger = sizeof(fingerprint);

int index = 0;
 

	char pszNameString[256];

 	CString dato;

    DWORD dwClauerStoreParam = CLAUER_STORE_MY_PARAM;

	if( ((CAdmin_CLAUERDlg *) this->GetParent())->import == 1 ) {


	WCHAR store2[] = L"My\\.Default";
	hStore = CertOpenStore(CERT_STORE_PROV_PHYSICAL, 0, NULL, CERT_SYSTEM_STORE_CURRENT_USER, (const void *) store2);

	if ( ! hStore ) {

		//ERROR

	}


	pCertContext=NULL;

	while(pCertContext= CertEnumCertificatesInStore(hStore, pCertContext)) {
	
		if(CertGetNameString(pCertContext, CERT_NAME_SIMPLE_DISPLAY_TYPE, 0, NULL, pszNameString, 128)) {

			dato = pszNameString;
			CertGetNameString(pCertContext, CERT_NAME_SIMPLE_DISPLAY_TYPE, CERT_NAME_ISSUER_FLAG, NULL, pszNameString, 128);
			dato.Append(" ");
			dato.Append(pszNameString);
			dato.Append(" ");
			CertGetNameString(pCertContext, CERT_NAME_FRIENDLY_DISPLAY_TYPE, 0, NULL, pszNameString, 128);
			dato.Append(pszNameString);

			index = m_comboClauer.InsertString(m_combo.GetCount(),dato.GetBuffer(0));

			unsigned char sha1Hash[20];

				CRYPT_HASH_BLOB hb;
				hb.pbData = NULL;
				hb.cbData = 20;

			if ( ! CertGetCertificateContextProperty ( pCertContext, CERT_HASH_PROP_ID, sha1Hash, &hb.cbData )) {
 
				// ERROR
 
			}else{

				/*certificadosClauer[index] = new unsigned char [20];
				if ( ! certificadosClauer[index] ) {
					//ERRROR
				}
				memcpy(certificadosClauer[index], sha1Hash,20);*/
				certificadosClauer[index] = sha1Hash;
				//A�adir fingerprint a la lista
				//certificadosIE[index] = hb;

			}


		}else {
	          
			//ERROR 

		}
	}

	}else{


		WCHAR store2[] = L"My\\ClauerStore";
		hStore = CertOpenStore(CERT_STORE_PROV_PHYSICAL, 0, NULL, CERT_SYSTEM_STORE_CURRENT_USER, (const void *) store2);
		if ( ! hStore ) {

			//ERROR

		}

		pCertContext=NULL;

		while(pCertContext = CertEnumCertificatesInStore(hStore, pCertContext)) {
		
			if(CertGetNameString(pCertContext, CERT_NAME_SIMPLE_DISPLAY_TYPE, 0, NULL, pszNameString, 128)) {

				dato = pszNameString;
				CertGetNameString(pCertContext, CERT_NAME_SIMPLE_DISPLAY_TYPE, CERT_NAME_ISSUER_FLAG, NULL, pszNameString, 128);
				dato.Append(" ");
				dato.Append(pszNameString);
				dato.Append(" ");
				CertGetNameString(pCertContext, CERT_NAME_FRIENDLY_DISPLAY_TYPE, 0, NULL, pszNameString, 128);
				dato.Append(pszNameString);

 				index = m_comboClauer.InsertString(m_comboClauer.GetCount(),dato.GetBuffer(0));

				unsigned char sha1Hash[20];

				CRYPT_HASH_BLOB hb;
				hb.pbData = NULL;
				hb.cbData = 20;

				if ( ! CertGetCertificateContextProperty ( pCertContext, CERT_HASH_PROP_ID, sha1Hash, &hb.cbData )) {
	 
					// ERROR


				}else{

					//A�adir lista
					//CString finger = fingerprint;
					//certificadosClauer[index] = finger.GetString();

    					certificadosClauer[index] = sha1Hash;
					//memcpy(certificadosClauer[index], sha1Hash,20);
					//certificadosClauer[index].cbData = hb.cbData;


				}

			}else {
		          
				//ERROR 

			}
		}
	}


	m_comboClauer.SetCurSel(0);

	//m_combo.SetCurSel(0);

	CertCloseStore(hStore, 0);
	hStore = 0;

	CFont *font = new CFont;
	font->CreatePointFont(12, "System");
	m_texto.SetFont(font);

	if(((CAdmin_CLAUERDlg *) this->GetParent())->import == 0 ) {
		m_texto.SetWindowText(IDIOMA_Get(IDIOMA_PASO_31_NEGRITA_EXP));
	}else{
		m_texto.SetWindowText(IDIOMA_Get(IDIOMA_PASO_31_NEGRITA_IMP));
	}


	if(((CAdmin_CLAUERDlg *) this->GetParent())->import == 0 ) {
		m_texto2.SetWindowText(IDIOMA_Get(IDIOMA_PASO_31_DESP_EXP));
	}else{
		m_texto2.SetWindowText(IDIOMA_Get(IDIOMA_PASO_31_DESP_IMP));
	}

	m_botoMover.SetWindowText(IDIOMA_Get(IDIOMA_BOTON_MOVER));
	m_botoCopiar.SetWindowText(IDIOMA_Get(IDIOMA_BOTON_COPIAR));

}

void DlgIE::OnCbnSelchangeCombo2()
{
	// TODO: Agregue aqu� su c�digo de controlador de notificaci�n de control
}

void DlgIE::OnCbnSelchangeCombo1()
{
	// TODO: Agregue aqu� su c�digo de controlador de notificaci�n de control
}

BOOL DlgIE::PreTranslateMessage(MSG* pMsg) 
{
	// TODO: Add your specialized code here and/or call the base class
	if(pMsg->message==WM_KEYDOWN)
	{
		switch (pMsg->wParam) {

		case VK_ESCAPE:
			pMsg->wParam=NULL ;
			break;

		case VK_RETURN:

			((CAdmin_CLAUERDlg *) this->GetParent())->OnBotonSiguiente();
			pMsg->wParam = NULL;
			return TRUE;
			break;

		}
	}
	return CDialog::PreTranslateMessage(pMsg);
}

//Copiar del Clauer al IE
void DlgIE::OnBnClickedButton4()
{
	// TODO: Agregue aqu� su c�digo de controlador de notificaci�n de control
	AlmacenCertificados::CPair* pCurVal;
	CString fingerprint;
	int encontrado = 0;
	CString pin;
	USBCERTS_HANDLE handle_certs;

	//	pin = ((CAdmin_CLAUERDlg *) this->GetParent())->Passwd();

	unsigned char * dispositius[MAX_DEVICES];
	int numDisp;

	if ( pin.IsEmpty() ) {
		DlgContras m_dlgContras;
		int nResponse = m_dlgContras.DoModal();
		if (nResponse == IDOK)
		{
			// TODO: Place code here to handle when the dialog is
			//  dismissed with OK
			if(m_dlgContras.m_pin.IsEmpty()){
				CWnd::MessageBox(IDIOMA_Get(IDIOMA_CONTRASENYA_NO_CORRECTA), IDIOMA_Get(IDIOMA_TITULO_CONTRASENYA_MAL), MB_ICONERROR);
				//				SetPaso(23);
				//				break;
				goto endDialog;
			}else{
				pin= m_dlgContras.m_pin;
			}
		}



		//LIBRT_RegenerarCache();


		LIBRT_ListarDispositivos(&numDisp,dispositius);

		if ( LIBRT_IniciarDispositivo(dispositius[0],pin.GetBuffer(0),&handle_certs) != 0 ) {
			CWnd::MessageBox(IDIOMA_Get(IDIOMA_CONTRASENYA_MAL), IDIOMA_Get(IDIOMA_TITULO_CONTRASENYA_MAL), MB_ICONERROR);
			goto endDialog;
		}

		//Pasar pin al padre

		((CAdmin_CLAUERDlg *) this->GetParent())->GetPasswd(pin);




		//		else if (nResponse == IDCANCEL)
		//		{
		// TODO: Place code here to handle when the dialog is
		//  dismissed with Cancel
		//SetPaso(23);
		//break;
	}

	//Comprobamos el PIN


	pCurVal = certificadosClauer.PGetFirstAssoc();

	CRYPT_HASH_BLOB hb;
	unsigned char lol[20];


	while ( (encontrado!=1) && (pCurVal != NULL) ) {

		int index = pCurVal->key;
		if (index == m_comboClauer.GetCurSel() ) {

			memcpy(lol,pCurVal->value.GetBuffer(0),20);
			//hb.cbData = pCurVal->value.cbData;
			//hb.pbData = pCurVal->value.pbData;
			//hb = pCurVal->value;
			encontrado = 1;

		}

		pCurVal = certificadosClauer.PGetNextAssoc(pCurVal);

	}
	if (!encontrado) {

		//Error, certificado no encontrado

	}else{ //Certificado encontrado




		char szCSP[]= "Microsoft Enhanced Cryptographic Provider v1.0"; 



		hb.pbData = lol;
		hb.cbData = 20;

		int err = 0;

		if( ((CAdmin_CLAUERDlg *) this->GetParent())->import == 0 ) {

			if ( ! CopyMoveCertFromClauer((char*)dispositius[0], pin.GetBuffer(0), hb.pbData, szCSP, 0, FALSE) ) {

				CWnd::MessageBox(IDIOMA_Get(IDIOMA_ERROR_IMP), "Gestor", MB_ICONINFORMATION);
				err = 1;
				//Error
			}
		}else{

			int err = 0;

			if ( ! CopyMoveCertToClauer((char*)dispositius[0], pin.GetBuffer(0), hb.pbData, FALSE) ) {

				CWnd::MessageBox(IDIOMA_Get(IDIOMA_ERROR_EXP), "Gestor", MB_ICONINFORMATION);
				err = 1;
				//Error
			}

		}
		if (err == 0) {

			if( ((CAdmin_CLAUERDlg *) this->GetParent())->import == 0 ) {

				CWnd::MessageBox(IDIOMA_Get(IDIOMA_OK_EXP), "Gestor", MB_ICONINFORMATION);

			}else{
				CWnd::MessageBox(IDIOMA_Get(IDIOMA_OK_IMP), "Gestor", MB_ICONINFORMATION);
			}



			m_combo.ResetContent();

			// TODO: Agregue aqu� su c�digo de controlador de mensajes
			HCERTSTORE hStore22 = 0;
			PCCERT_CONTEXT   pCertContext=NULL;
			//	HANDLE           hCertStore;

			unsigned char fingerprint[20];
			DWORD dwFinger;

			dwFinger = sizeof(fingerprint);

			int index = 0;


			char pszNameString[256];

			CString dato;

			DWORD dwClauerStoreParam = CLAUER_STORE_MY_PARAM;


			if(((CAdmin_CLAUERDlg *) this->GetParent())->import == 0 ) {
				hStore22 = CertOpenStore(CERT_STORE_PROV_PHYSICAL, 0, NULL, CERT_SYSTEM_STORE_CURRENT_USER, (const void *) L"My\\.Default");
			}else{
				hStore22 = CertOpenStore(CERT_STORE_PROV_PHYSICAL, 0, NULL, CERT_SYSTEM_STORE_CURRENT_USER, (const void *) L"My\\ClauerStore");
			}
			//hStore22 = CertOpenStore(CERT_STORE_PROV_PHYSICAL, 0, NULL, CERT_SYSTEM_STORE_CURRENT_USER, (const void *) store22);

			if ( ! hStore22 ) {

				//ERROR

			}

			pCertContext=NULL;

			while(pCertContext= CertEnumCertificatesInStore(hStore22, pCertContext)) {

				if(CertGetNameString(pCertContext, CERT_NAME_SIMPLE_DISPLAY_TYPE, 0, NULL, pszNameString, 128)) {

					dato = pszNameString;
					CertGetNameString(pCertContext, CERT_NAME_SIMPLE_DISPLAY_TYPE, CERT_NAME_ISSUER_FLAG, NULL, pszNameString, 128);
					dato.Append(" ");
					dato.Append(pszNameString);
					dato.Append(" ");
					CertGetNameString(pCertContext, CERT_NAME_FRIENDLY_DISPLAY_TYPE, 0, NULL, pszNameString, 128);
					dato.Append(pszNameString);

					index = m_combo.InsertString(m_combo.GetCount(),dato.GetBuffer(0));
				}
			}

			m_combo.SetCurSel(0);
		}
		LIBRT_FinalizarDispositivo(&handle_certs);



	}

endDialog:

	;

}

void DlgIE::OnBnClickedButton2()
{
	// TODO: Agregue aqu� su c�digo de controlador de notificaci�n de control

}

void DlgIE::OnStnClicked1083()
{
	// TODO: Agregue aqu� su c�digo de controlador de notificaci�n de control
}

//Boton mover
void DlgIE::OnBnClickedButton3()
{


	// TODO: Agregue aqu� su c�digo de controlador de notificaci�n de control
	AlmacenCertificados::CPair* pCurVal;
	CString fingerprint;
	int encontrado = 0;
	CString pin;
	USBCERTS_HANDLE handle_certs;

	//	pin = ((CAdmin_CLAUERDlg *) this->GetParent())->Passwd();

	unsigned char * dispositius[MAX_DEVICES];
	int numDisp;

	if ( pin.IsEmpty() ) {
		DlgContras m_dlgContras;
		int nResponse = m_dlgContras.DoModal();
		if (nResponse == IDOK)
		{
			// TODO: Place code here to handle when the dialog is
			//  dismissed with OK
			if(m_dlgContras.m_pin.IsEmpty()){
				CWnd::MessageBox(IDIOMA_Get(IDIOMA_CONTRASENYA_NO_CORRECTA), IDIOMA_Get(IDIOMA_TITULO_CONTRASENYA_MAL), MB_ICONERROR);
				//				SetPaso(23);
				//				break;
				goto endDialog;
			}else{
				pin= m_dlgContras.m_pin;
			}
		}



		//LIBRT_RegenerarCache();


		LIBRT_ListarDispositivos(&numDisp,dispositius);

		if ( LIBRT_IniciarDispositivo(dispositius[0],pin.GetBuffer(0),&handle_certs) != 0 ) {
			CWnd::MessageBox(IDIOMA_Get(IDIOMA_CONTRASENYA_MAL), IDIOMA_Get(IDIOMA_TITULO_CONTRASENYA_MAL), MB_ICONERROR);
			goto endDialog;
		}

		//Pasar pin al padre

		((CAdmin_CLAUERDlg *) this->GetParent())->GetPasswd(pin);




		//		else if (nResponse == IDCANCEL)
		//		{
		// TODO: Place code here to handle when the dialog is
		//  dismissed with Cancel
		//SetPaso(23);
		//break;
	}

	//Comprobamos el PIN


	pCurVal = certificadosClauer.PGetFirstAssoc();

	CRYPT_HASH_BLOB hb;
	unsigned char lol[20];


	while ( (encontrado!=1) && (pCurVal != NULL) ) {

		int index = pCurVal->key;
		if (index == m_comboClauer.GetCurSel() ) {

			memcpy(lol,pCurVal->value.GetBuffer(0),20);
			//hb.cbData = pCurVal->value.cbData;
			//hb.pbData = pCurVal->value.pbData;
			//hb = pCurVal->value;
			encontrado = 1;

		}

		pCurVal = certificadosClauer.PGetNextAssoc(pCurVal);

	}
	if (!encontrado) {

		//Error, certificado no encontrado

	}else{ //Certificado encontrado




		char szCSP[]= "Microsoft Enhanced Cryptographic Provider v1.0"; 



		hb.pbData = lol;
		hb.cbData = 20;

		int err = 0;

		if( ((CAdmin_CLAUERDlg *) this->GetParent())->import == 0 ) {

			if ( ! CopyMoveCertFromClauer((char*)dispositius[0], pin.GetBuffer(0), hb.pbData, szCSP, 0, TRUE) ) {

				CWnd::MessageBox(IDIOMA_Get(IDIOMA_ERROR_IMP), "Gestor", MB_ICONINFORMATION);
				err = 1;
				//Error
			}
		}else{

			if ( ! CopyMoveCertToClauer((char*)dispositius[0], pin.GetBuffer(0), hb.pbData, TRUE) ) {

				CWnd::MessageBox(IDIOMA_Get(IDIOMA_ERROR_EXP), "Gestor", MB_ICONINFORMATION);
				err = 1;
				//Error
			}

		}
		if (err == 0){
			if( ((CAdmin_CLAUERDlg *) this->GetParent())->import == 0 ) {

				CWnd::MessageBox(IDIOMA_Get(IDIOMA_OK_EXP), "Gestor", MB_ICONINFORMATION);

			}else{
				CWnd::MessageBox(IDIOMA_Get(IDIOMA_OK_IMP), "Gestor", MB_ICONINFORMATION);
			}


			m_comboClauer.ResetContent();

			// TODO: Agregue aqu� su c�digo de controlador de mensajes
			HCERTSTORE hStore22 = 0;
			PCCERT_CONTEXT   pCertContext=NULL;
			//	HANDLE           hCertStore;

			unsigned char fingerprint[20];
			DWORD dwFinger;

			dwFinger = sizeof(fingerprint);

			int index = 0;


			char pszNameString[256];

			CString dato;

			DWORD dwClauerStoreParam = CLAUER_STORE_MY_PARAM;
			if(((CAdmin_CLAUERDlg *) this->GetParent())->import == 0 ) {
				hStore22 = CertOpenStore(CERT_STORE_PROV_PHYSICAL, 0, NULL, CERT_SYSTEM_STORE_CURRENT_USER, (const void *) L"My\\ClauerStore");
			}else{
				hStore22 = CertOpenStore(CERT_STORE_PROV_PHYSICAL, 0, NULL, CERT_SYSTEM_STORE_CURRENT_USER, (const void *) L"My\\.Default");
			}
			//hStore22 = CertOpenStore(CERT_STORE_PROV_PHYSICAL, 0, NULL, CERT_SYSTEM_STORE_CURRENT_USER, (const void *) store22);

			if ( ! hStore22 ) {

				//ERROR

			}

			pCertContext=NULL;

			while(pCertContext= CertEnumCertificatesInStore(hStore22, pCertContext)) {

				if(CertGetNameString(pCertContext, CERT_NAME_SIMPLE_DISPLAY_TYPE, 0, NULL, pszNameString, 128)) {

					dato = pszNameString;
					CertGetNameString(pCertContext, CERT_NAME_SIMPLE_DISPLAY_TYPE, CERT_NAME_ISSUER_FLAG, NULL, pszNameString, 128);
					dato.Append(" ");
					dato.Append(pszNameString);
					dato.Append(" ");
					CertGetNameString(pCertContext, CERT_NAME_FRIENDLY_DISPLAY_TYPE, 0, NULL, pszNameString, 128);
					dato.Append(pszNameString);

					index = m_comboClauer.InsertString(m_comboClauer.GetCount(),dato.GetBuffer(0));

					unsigned char sha1Hash[20];

					CRYPT_HASH_BLOB hb;
					hb.pbData = NULL;
					hb.cbData = 20;

					if ( ! CertGetCertificateContextProperty ( pCertContext, CERT_HASH_PROP_ID, sha1Hash, &hb.cbData )) {

						// ERROR


					}else{

						//A�adir lista
						//CString finger = fingerprint;
						//certificadosClauer[index] = finger.GetString();
						certificadosClauer[index] = sha1Hash;
						//memcpy(certificadosClauer[index], sha1Hash,20);
						//certificadosClauer[index].cbData = hb.cbData;


					}
				}


			}

			m_comboClauer.SetCurSel(0);
		}
		LIBRT_FinalizarDispositivo(&handle_certs);



	}
endDialog:

	;
	// TODO: Agregue aqu� su c�digo de controlador de notificaci�n de control
}
