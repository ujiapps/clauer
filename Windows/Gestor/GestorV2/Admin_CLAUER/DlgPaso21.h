#if !defined(AFX_DLGPASO21_H__ABE532AB_0590_4997_B5B4_6708A8F8D1FD__INCLUDED_)
#define AFX_DLGPASO21_H__ABE532AB_0590_4997_B5B4_6708A8F8D1FD__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// DlgPaso21.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// DlgPaso21 dialog

class DlgPaso21 : public CDialog
{
// Construction
public:
	CString GetPIN(void);
	CString GetConfirmacion(void);
	CString GetViejo(void);
	DlgPaso21(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(DlgPaso21)
	enum { IDD = IDD_DIALOG_PASO_21 };
	CStatic	m_pin;
	CButton	m_cuadro2;
	CButton	m_cuadro1;
	CStatic	m_cont;
	CStatic	m_conf;
	CStatic	m_texto;
	CString	m_pinconf;
	CString	m_pinnuevo;
	CString	m_pinviejo;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(DlgPaso21)
	public:
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(DlgPaso21)
	afx_msg void OnShowWindow(BOOL bShow, UINT nStatus);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DLGPASO21_H__ABE532AB_0590_4997_B5B4_6708A8F8D1FD__INCLUDED_)
