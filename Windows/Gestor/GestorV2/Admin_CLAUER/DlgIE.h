#pragma once
#include "afxwin.h"

#include "DlgContras.h"

#include <wincrypt.h>

#define CLAUER_STORE_NAME      "CLAUERSTORE"
#define CLAUER_STORE_MY_PARAM 0


//typedef CMap<int, int,CString,CString> AlmacenCertificados;
//typedef CMap<int, int,CRYPT_HASH_BLOB,CRYPT_HASH_BLOB> AlmacenCertificados;
typedef CMap<int, int,CString,CString> AlmacenCertificados;

// Cuadro de di�logo de DlgIE

class DlgIE : public CDialog
{
	DECLARE_DYNAMIC(DlgIE)

public:
	DlgIE(CWnd* pParent = NULL);   // Constructor est�ndar
	virtual ~DlgIE();

// Datos del cuadro de di�logo
	enum { IDD = IDD_IE };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // Compatibilidad con DDX o DDV

	AlmacenCertificados certificadosIE;
	AlmacenCertificados certificadosClauer;

	DECLARE_MESSAGE_MAP()
public:
	CComboBox m_combo;
	afx_msg void OnShowWindow(BOOL bShow, UINT nStatus);
	afx_msg void OnCbnSelchangeCombo2();
	afx_msg void OnCbnSelchangeCombo1();
	CComboBox m_comboClauer;
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	CStatic m_texto;
	afx_msg void OnBnClickedButton4();
	afx_msg void OnBnClickedButton2();
public:
	afx_msg void OnStnClicked1083();
public:
	afx_msg void OnBnClickedButton3();
public:
	CStatic m_texto2;
public:
	CButton m_botoMover;
public:
	CButton m_botoCopiar;
};
