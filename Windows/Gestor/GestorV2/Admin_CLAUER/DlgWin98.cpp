// DlgWin98.cpp : implementation file
//

#include "stdafx.h"
#include "Admin_CLAUER.h"
#include "DlgWin98.h"
#include "IDIOMA.h"

#include <ctype.h>

#include "Excepciones.h"
#include <CRYPTOWrapper/CRYPTOWrap.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// DlgWin98 dialog


DlgWin98::DlgWin98(CWnd* pParent /*=NULL*/)
	: CDialog(DlgWin98::IDD, pParent)
{
	//{{AFX_DATA_INIT(DlgWin98)
	m_pinconf = _T("");
	m_pinuevo = _T("");
	m_pinviejo = _T("");
	//}}AFX_DATA_INIT
}


void DlgWin98::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(DlgWin98)
	DDX_Control(pDX, IDC_TXTPN, m_txtpn);
	DDX_Control(pDX, IDC_TXTPIN, m_txtpin);
	DDX_Control(pDX, IDC_TXTCONF, m_txtconf);
	DDX_Control(pDX, IDC_DESC2, m_cuadro2);
	DDX_Control(pDX, IDC_DESC1, m_cuadro1);
	DDX_Control(pDX, IDC_TEXT1, m_texto);
	DDX_Control(pDX, IDOK, m_ok);
	DDX_Control(pDX, IDCANCEL, m_cancel);
	DDX_Text(pDX, PIN_CONFIR, m_pinconf);
	DDX_Text(pDX, PIN_NUEVO, m_pinuevo);
	DDX_Text(pDX, PIN_VIEJO, m_pinviejo);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(DlgWin98, CDialog)
	//{{AFX_MSG_MAP(DlgWin98)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// DlgWin98 message handlers

BOOL DlgWin98::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here
	CFont *font = new CFont;
	font->CreatePointFont(12, "System");
	m_texto.SetFont(font);
	delete font;
	this->SetWindowText(IDIOMA_Get(IDIOMA_DIALOG_WIN98_TITULO));
	m_ok.SetWindowText(IDIOMA_Get(IDIOMA_DIALOG_WIN98_BOTON_MODIFICAR));
	m_texto.SetWindowText(IDIOMA_Get(IDIOMA_DIALOG_WIN98_NEGRITA));
	m_cancel.SetWindowText(IDIOMA_Get(IDIOMA_BOTON_CANCEL));
	m_cuadro1.SetWindowText(IDIOMA_Get(IDIOMA_DIALOG_WIN98_CUADRO1));
	m_cuadro2.SetWindowText(IDIOMA_Get(IDIOMA_DIALOG_WIN98_CUADRO2));
	m_txtpin.SetWindowText(IDIOMA_Get(IDIOMA_DIALOG_WIN98_PIN));
	m_txtpn.SetWindowText(IDIOMA_Get(IDIOMA_DIALOG_WIN98_NUEVO_PIN));
	m_txtconf.SetWindowText(IDIOMA_Get(IDIOMA_DIALOG_WIN98_CONF));

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void DlgWin98::OnOK() 
{
	// TODO: Add extra validation here
	int ok;

	FILE *fp;
	BYTE *pkcs12;
	DWORD tamPKCS12;

	CString pin,confirmacion,viejo;

	
	switch(1)
	{

	case 1:
			pin = GetPINuevo();
			confirmacion = GetConfirmacion();

			if ( pin.GetLength(
			) < 0 ) {
				AfxMessageBox(IDIOMA_Get(IDIOMA_LONGITUD_PIN), MB_OK,0);
				this->m_pinviejo.Empty();
				this->m_pinconf.Empty();
				this->m_pinuevo.Empty();
				this->UpdateData(FALSE);
				break;
			} else if ( pin != confirmacion ) {
				AfxMessageBox(IDIOMA_Get(IDIOMA_PIN_COINCIDENCIA), MB_OK,0);
				this->m_pinviejo.Empty();
				this->m_pinconf.Empty();
				this->m_pinuevo.Empty();
				this->UpdateData(FALSE);
				break;
			} else {
				if( !SeguridadPasswd(pin) )
				{
					this->m_pinviejo.Empty();
					this->m_pinconf.Empty();
					this->m_pinuevo.Empty();
					this->UpdateData(FALSE);
					break;
				}
				viejo=this->GetPIN();
				try {
				VerificarPKCS12(viejo);
				}
				catch (EPwdIncorrecta &e) {
					CWnd::MessageBox(e.GetMSG().c_str(), "Gestor Clauer :: ERROR", MB_ICONERROR);
					this->SetFocus();
					break;
				}
				catch (ENoPKCS12 &e) {
					CWnd::MessageBox(e.GetMSG().c_str(), "Gestor Clauer :: ERROR", MB_ICONERROR);
					break;
				}
				catch (EExcepcion &e) {
					CWnd::MessageBox(e.GetMSG().c_str(), "Gestor Clauer :: ERROR", MB_ICONERROR);
					break;
				}

				fp = fopen(this->cert1, "rb");
				fseek(fp, 0, SEEK_END);
				tamPKCS12 = ftell(fp);
				fseek(fp, 0, SEEK_SET);


				pkcs12 = new BYTE [tamPKCS12];


				fread(pkcs12, tamPKCS12, 1, fp);


				fclose(fp);

				ok = CRYPTO_PKCS12_CambiarPassword(pkcs12,tamPKCS12,viejo.GetBuffer(0),pin.GetBuffer(0));


				if(ok == 0)
				{
					this->nuevo_cert1=(string("A:\\") + string("OLD_") + string(this->cert1.Mid(3))).c_str();

					MoveFile(this->cert1.GetBuffer(0),this->nuevo_cert1.GetBuffer(0));

					FILE *f1;

					if ( ( f1 = fopen (this->cert1.GetBuffer(0), "wb")) == NULL ) {
						AfxMessageBox(IDIOMA_Get(IDIOMA_ABRIR_CERTIFICADO));
						CDialog::OnOK();
						break;
					}


					if( (tamPKCS12) > (fwrite(pkcs12, 1, tamPKCS12, f1) ) ) {
					
						AfxMessageBox(IDIOMA_Get(IDIOMA_ESCRIBIR_CERTIFICADOS));
						CDialog::OnOK();
						break;			  
					}

					SecureZeroMemory(pkcs12, tamPKCS12);
					delete [] pkcs12;
					pkcs12 = NULL;

					if(fclose(f1)) {
				
						AfxMessageBox(IDIOMA_Get(IDIOMA_CERRAR_CERTIFICADOS));
						CDialog::OnOK();
						break;
					}
					
					if(!this->cert2.IsEmpty()) {
						fp = fopen(this->cert2, "rb");
						fseek(fp, 0, SEEK_END);
						tamPKCS12 = ftell(fp);
						fseek(fp, 0, SEEK_SET);

						pkcs12 = new BYTE [tamPKCS12];

						fread(pkcs12, tamPKCS12, 1, fp);

						fclose(fp);
						ok = CRYPTO_PKCS12_CambiarPassword(pkcs12,tamPKCS12,viejo.GetBuffer(0),pin.GetBuffer(0));
						if(ok==0) {
							this->nuevo_cert2=(string("A:\\") + string("OLD_") + string(this->cert2.Mid(3))).c_str();
							MoveFile(this->cert2.GetBuffer(0),this->nuevo_cert2.GetBuffer(0));
							FILE *f2;
							if ( ( f2 = fopen (this->cert2.GetBuffer(0), "wb")) == NULL ) {
								AfxMessageBox(IDIOMA_Get(IDIOMA_ABRIR_CERTIFICADO));
								CDialog::OnOK();
								break;
							}

							if( (tamPKCS12) > (fwrite(pkcs12, 1, tamPKCS12, f2) ) ) {
					
								AfxMessageBox(IDIOMA_Get(IDIOMA_ESCRIBIR_CERTIFICADOS));
								CDialog::OnOK();
								break;
							}
							SecureZeroMemory(pkcs12, tamPKCS12);
							delete [] pkcs12;
							pkcs12 = NULL;

							if(fclose(f2)) {
				
								AfxMessageBox(IDIOMA_Get(IDIOMA_CERRAR_CERTIFICADOS));
								CDialog::OnOK();
								break;
							}

						}else{
							AfxMessageBox(IDIOMA_Get(IDIOMA_NO_MODIFICAR_PIN));
							this->m_pinviejo.Empty();
							this->m_pinconf.Empty();
							this->m_pinuevo.Empty();
							this->UpdateData(FALSE);
							CDialog::OnOK();
							break;
						}
						AfxMessageBox(IDIOMA_Get(IDIOMA_PIN_MODIFICADO));

						CDialog::OnOK();
						break;
					}
				}else{
					AfxMessageBox(IDIOMA_Get(IDIOMA_NO_MODIFICAR_PIN));
					this->m_pinviejo.Empty();
					this->m_pinconf.Empty();
					this->m_pinuevo.Empty();
					this->UpdateData(FALSE);
			
				}

			}
	}

}

void DlgWin98::OnCancel() 
{
	// TODO: Add extra cleanup here
	CDialog::OnCancel();
}

CString DlgWin98::GetPIN()
{
	UpdateData(TRUE);
	return m_pinviejo;
}

int DlgWin98::SeguridadPasswd(CString pwd)
{
	int i, lon;
	int num = 0;
	int letras = 0;
/*	int igual =0;
	int buscar = 0;*/

	lon=pwd.GetLength();

	for (i=0; i<lon;i++)
	{

		int igual =0;
		int buscar = 0;
		//int indice = 0;

		if( isdigit(pwd[i]) ) {
			buscar=pwd.Find(pwd[i],i+1);
			if(buscar!= -1) {
			//	indice=i+2;
				igual=igual+2;
				while (buscar !=-1) {
					buscar=pwd.Find(pwd[i],buscar+1);
					//indice=indice+1;
					if(buscar != -1)
						igual=igual+1;
				
				 }
			}
			if(lon <=10) {
				if(igual >= 4){
					AfxMessageBox(IDIOMA_Get(IDIOMA_PASS_NO_SEGURA), MB_OK,0);
					return 0;
				}
			}else{
				if(lon/2<=igual){
				AfxMessageBox(IDIOMA_Get(IDIOMA_PASS_NO_SEGURA), MB_OK,0);
				return 0;
				}
			}
			num=num+1;
		}else if( isalpha(pwd[i]) ) {
			buscar=pwd.Find(pwd[i],i+1);
			if(buscar!= -1) {
			//	indice=i+2;
				igual=igual+2;
				while (buscar !=-1) {
					buscar=pwd.Find(pwd[i],buscar+1);
				//	indice=indice+1;
					if(buscar != -1)
						igual=igual+1;
				
				 }
			}
			if(lon <=10) {
				if(igual >= 4){
					AfxMessageBox(IDIOMA_Get(IDIOMA_PASS_LETRAS), MB_OK,0);
					return 0;
				}
			}else{
				if(lon/2<=igual){
				AfxMessageBox(IDIOMA_Get(IDIOMA_PASS_LETRAS), MB_OK,0);
				return 0;
				}
			}
			letras=letras+1;
		}
	}

	if(letras<3){
		AfxMessageBox(IDIOMA_Get(IDIOMA_MIN_LETRAS), MB_OK,0);
		return 0;
	}else if(num<3){
		AfxMessageBox(IDIOMA_Get(IDIOMA_MIN_NUM), MB_OK,0);
		return 0;
	}else{
		return 1;
	}
}

CString DlgWin98::GetPINuevo()
{
	UpdateData(TRUE);
	return m_pinuevo;
}

CString DlgWin98::GetConfirmacion()
{
	UpdateData(TRUE);
	return m_pinconf;
}

void DlgWin98::VerificarPKCS12 (CString pwd)
{
	HANDLE hFind;
	WIN32_FIND_DATA findData;
	FILE *fp;
//	FILE *fp1;
//	FILE *fp2;
	BYTE *pkcs12;
	DWORD tamPKCS12, err;
	int ok, pkcs12Encontrados=0;
	BOOL masFicheros;
	char *filtro[2] = {"A:\\*.p12", "A:\\*.pfx"};


	/* Iteramos para ambas m�scaras
	 */

	for ( int f = 0 ; f < 2 ; f++ ) {

		hFind = FindFirstFile(filtro[f], &findData);

		if ( hFind == INVALID_HANDLE_VALUE ) 
			continue;
		
		do {
			
			if(this->cert1.IsEmpty()) {
				this->cert1 = (string("A:\\") + string(findData.cFileName)).c_str();
			}else{
				this->cert2 = (string("A:\\") + string(findData.cFileName)).c_str();
			}
			fp = fopen((string("A:\\") + string(findData.cFileName)).c_str(), "rb");
			if ( !fp ) {
				string errMsg;
				errMsg = IDIOMA_Get(IDIOMA_NO_CERT_DISQUET);
				ENoPKCS12 e(errMsg);
				this->m_pinviejo.Empty();
				this->m_pinconf.Empty();
				this->m_pinuevo.Empty();
				this->UpdateData(FALSE);
				
				throw e;
			}

			fseek(fp, 0, SEEK_END);
			tamPKCS12 = ftell(fp);
			fseek(fp, 0, SEEK_SET);


			pkcs12 = new BYTE [tamPKCS12];


			fread(pkcs12, tamPKCS12, 1, fp);


			fclose(fp);

			ok = CRYPTO_PKCS12_VerificarPassword (pkcs12, tamPKCS12, pwd.GetBuffer(0));


			SecureZeroMemory(pkcs12, tamPKCS12);
			delete [] pkcs12;
			pkcs12 = NULL;

			if ( ok == 0 ) {
				/* Password incorrecta */
	
					fclose(fp);
					FindClose(hFind);
					string errMsg;
					errMsg = IDIOMA_Get(IDIOMA_PIN_INCORRECTO);
					this->m_pinviejo.Empty();
					this->m_pinconf.Empty();
					this->m_pinuevo.Empty();
					this->UpdateData(FALSE);
					EPwdIncorrecta e(errMsg);
					throw e;
			} else if ( ok == 2 ) {
				/* Error */

				fclose(fp);
				FindClose(hFind);

				string errMsg;
				errMsg = IDIOMA_Get(IDIOMA_ERROR_VERIFICANDO_PASS);
				this->m_pinviejo.Empty();
				this->m_pinconf.Empty();
				this->m_pinuevo.Empty();
				this->UpdateData(FALSE);
				EExcepcion e(errMsg);
				throw e;
			}

			++pkcs12Encontrados;

			masFicheros = FindNextFile(hFind, &findData);

			if ( !masFicheros ) {
				err = GetLastError();
				if ( err == ERROR_NO_MORE_FILES )
					break;
				else {
					/* Cualquier otro error asumimos error de entrada salida
					 */

					fclose(fp);
					FindClose(hFind);
		
					string errMsg;
					errMsg = IDIOMA_Get(IDIOMA_ERROR_ENTRADA_SALIDA);
					this->m_pinviejo.Empty();
					this->m_pinconf.Empty();
					this->m_pinuevo.Empty();
					this->UpdateData(FALSE);
					EErrorES e(errMsg);
					throw e;
				}
			}
		
		} while (1);

		fclose(fp);
		FindClose(hFind);

	}

	if ( pkcs12Encontrados == 0 ) {
		string errMsg;
		errMsg = IDIOMA_Get(IDIOMA_NO_CERT_DISQUET);
		this->m_pinviejo.Empty();
		this->m_pinconf.Empty();
		this->m_pinuevo.Empty();
		this->UpdateData(FALSE);
		ENoPKCS12 e(errMsg);
		throw e;
	}

}

BOOL DlgWin98::PreTranslateMessage(MSG* pMsg) 
{
	// TODO: Add your specialized code here and/or call the base class
	if(pMsg->message==WM_KEYDOWN)
	{
		if ( (pMsg->wParam==VK_ESCAPE) )
			pMsg->wParam=NULL ;
	}
	return CDialog::PreTranslateMessage(pMsg);
}
