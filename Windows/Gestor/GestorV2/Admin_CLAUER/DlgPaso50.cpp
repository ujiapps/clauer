// DlgPaso50.cpp : implementation file
//

#include "stdafx.h"
#include "Admin_CLAUER.h"
#include "DlgPaso50.h"
#include "Admin_CLAUERDlg.h"
#include "IDIOMA.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// DlgPaso50 dialog


DlgPaso50::DlgPaso50(CWnd* pParent /*=NULL*/)
	: CDialog(DlgPaso50::IDD, pParent)
{
	//{{AFX_DATA_INIT(DlgPaso50)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}


void DlgPaso50::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(DlgPaso50)
	DDX_Control(pDX, IDC_TEXTO2, m_texto2);
	DDX_Control(pDX, IDC_TEXTO, m_texto);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(DlgPaso50, CDialog)
	//{{AFX_MSG_MAP(DlgPaso50)
	ON_WM_SHOWWINDOW()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// DlgPaso50 message handlers

void DlgPaso50::OnShowWindow(BOOL bShow, UINT nStatus) 
{
	CDialog::OnShowWindow(bShow, nStatus);
	
	// TODO: Add your message handler code here
	CFont *font = new CFont;
	font->CreatePointFont(12, "System");
	m_texto.SetFont(font);
	m_texto.SetWindowText(IDIOMA_Get(IDIOMA_PASO_50_NEGRITA));
	m_texto2.SetWindowText(IDIOMA_Get(IDIOMA_PASO_50_DESCRIP1));
	delete font;
}

BOOL DlgPaso50::PreTranslateMessage(MSG* pMsg) 
{
	// TODO: Add your specialized code here and/or call the base class
	if(pMsg->message==WM_KEYDOWN)
	{
		if ( (pMsg->wParam==VK_ESCAPE) )
			pMsg->wParam=NULL ;
		else if ( (pMsg->wParam==VK_RETURN) ) {
			((CAdmin_CLAUERDlg *) this->GetParent())->OnBotonSiguiente();
			//((CAdmin_CLAUERDlg *) AfxGetApp())->OnBotonSiguiente();
			return TRUE;
		}
	}
	return CDialog::PreTranslateMessage(pMsg);
}
