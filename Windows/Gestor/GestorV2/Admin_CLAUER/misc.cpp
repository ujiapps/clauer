#include "stdafx.h"
#include "misc.h"


// Reserva memoria con new para el objeto devuelto -->
// --> liberar con delete

BOOL MISC_CLAUER_Ini ( unsigned char *szDeviceName, char *szPwd, USBCERTS_HANDLE *hClauer )
{
	BOOL bFreeDevices = FALSE, ret = TRUE;
	unsigned char * d[MAX_DEVICES];
	int nDispositivos;

	if ( ! hClauer )
		return FALSE;

	if ( ! szDeviceName ) {
			memset(d,0,MAX_DEVICES * sizeof(unsigned char *));
			if ( LIBRT_ListarDispositivos(&nDispositivos,d) != 0 ) 
				return FALSE;
			if ( nDispositivos == 0 ) 
				return FALSE;

			bFreeDevices = TRUE;
			szDeviceName = d[0];
	}

	if ( LIBRT_IniciarDispositivo(szDeviceName, szPwd, hClauer) != 0 ) {
		ret = FALSE;
		goto endMISC_CLAUER_Ini;
	}
	

endMISC_CLAUER_Ini:

	if ( bFreeDevices ) {
		for ( int i = 0 ; *(d+i) ; i++ )
			free(d[i]);
	}

	return ret;

}
