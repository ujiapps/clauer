#pragma once
#include "afxwin.h"


// Cuadro de di�logo de DlgExportarPkcs12

class DlgExportarPkcs12 : public CDialog
{
	DECLARE_DYNAMIC(DlgExportarPkcs12)

public:
	DlgExportarPkcs12(CWnd* pParent = NULL);   // Constructor est�ndar
	virtual ~DlgExportarPkcs12();

	virtual BOOL PreTranslateMessage(MSG* pMsg);

// Datos del cuadro de di�logo
	enum { IDD = IDD_EXPORTAR_PKCS12 };

protected:
	// asocia posici�n en la lista con
	// bloque dentro del clauer

	CMap<int,int,unsigned long, unsigned long> m_mCboBlock;	


	virtual void DoDataExchange(CDataExchange* pDX);    // Compatibilidad con DDX/DDV

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedButton1();
public:
	// Fichero PKCS12
	CString m_editPkcs12File;
protected:
	// Carga la lista de certificados disponibles
	int LoadCertCombo(void);
	// Combo con certs a exportar
	CComboBox m_cboCerts;
public:
	afx_msg void OnShowWindow(BOOL bShow, UINT nStatus);

public:

	// juan: estos son p�blicos para obtener el n�mero de bloque seleccionado
	//       y el fichero destino

	BOOL GetSelectedCertBlock ( int &selCertBlock );
	BOOL GetSelectedFile      ( CString &strPkcs12 );

	// juan: fin
public:
	CStatic m_texto;
public:
	CStatic m_texto2;
public:
	CStatic n_texto3;
public:
	CStatic m_texto4;
};
