#if !defined(AFX_DLGPASO2_H__F57C3997_FC6B_4FE0_B062_C1281D8DBF71__INCLUDED_)
#define AFX_DLGPASO2_H__F57C3997_FC6B_4FE0_B062_C1281D8DBF71__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// DlgPaso2.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// DlgPaso2 dialog

class DlgPaso2 : public CDialog
{
// Construction
public:
	CString GetPIN(void);
	CString GetConfirmacion(void);
	DlgPaso2(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(DlgPaso2)
	enum { IDD = IDD_DIALOG_PASO_2 };
	CButton	m_cuadro;
	CStatic	m_pin;
	CStatic	m_conf;
	CStatic	m_texto;
	CString	m_editPIN;
	CString	m_editConfirmation;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(DlgPaso2)
	public:
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(DlgPaso2)
	afx_msg void OnShowWindow(BOOL bShow, UINT nStatus);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DLGPASO2_H__F57C3997_FC6B_4FE0_B062_C1281D8DBF71__INCLUDED_)
