#if !defined(AFX_DLGPASO50_H__EA20C2C9_1485_4E22_9B73_58878F4CF158__INCLUDED_)
#define AFX_DLGPASO50_H__EA20C2C9_1485_4E22_9B73_58878F4CF158__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// DlgPaso50.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// DlgPaso50 dialog

class DlgPaso50 : public CDialog
{
// Construction
public:
	DlgPaso50(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(DlgPaso50)
	enum { IDD = IDD_DIALOG_PASO_50 };
	CStatic	m_texto2;
	CStatic	m_texto;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(DlgPaso50)
	public:
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(DlgPaso50)
	afx_msg void OnShowWindow(BOOL bShow, UINT nStatus);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DLGPASO50_H__EA20C2C9_1485_4E22_9B73_58878F4CF158__INCLUDED_)
