
#include <windows.h>
#include <wincrypt.h>
#include <tchar.h>

#include "gestorimport.h"

#include <LIBRT/libRT.h>
#include <CRYPTOWrapper/CRYPTOWrap.h>

#include <stdio.h>
#include <stdlib.h>


#define CL_DEFAULT_IMPORT_CSP       "Microsoft Enhanced Cryptographic Provider v1.0"
#define WSZ_CL_DEFAULT_IMPORT_CSP	L"Microsoft Enhanced Cryptographic Provider v1.0"



void PrintError ( DWORD dw )
{
	LPVOID lpMsgBuf;
//	LPVOID lpDisplayBuf;

	FormatMessageA(
		FORMAT_MESSAGE_ALLOCATE_BUFFER | 
		FORMAT_MESSAGE_FROM_SYSTEM,
		NULL,
		dw,
		MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
		(LPTSTR) &lpMsgBuf,
		0, NULL );

	printf("%x: %s", dw, lpMsgBuf);

	LocalFree(lpMsgBuf);
	//LocalFree(lpDisplayBuf);
}

BOOL CopyMoveCertFromClauer ( char *szDevice, char *szPass, unsigned char sha1Hash[20], char *szCSP, DWORD dwFlags, BOOL bMove )
{
	USBCERTS_HANDLE hClauer;
	unsigned char blockCert[TAM_BLOQUE];
	long bnCert;
	unsigned char blockKey[TAM_BLOQUE];
	long bnKey;
	unsigned char blockContainer[TAM_BLOQUE];
	long bnContainer;

	unsigned char fingerPrint[TAM_SHA1];

	unsigned char *certDER = NULL;
	unsigned long tamDER;

	char szContainerName[35];

	unsigned char zeroId[20];
	WCHAR store[] = L"My\\.Default";

	HCRYPTPROV hProv = 0;
	HCRYPTKEY hKey = 0;
	HCERTSTORE hStore = 0;

	DWORD dwContainerName;
	LPWSTR wszContainerName = NULL;

	char *szTargetCSP;
	int i;

	PCCERT_CONTEXT pCertAux = NULL;
	BOOL ret = TRUE;
	DWORD dwAux;
	ALG_ID keySpec;

	CRYPT_KEY_PROV_INFO infoCSP;

	if ( ! szDevice )
		return FALSE;
	if ( ! szPass )
		return FALSE;
	if ( ! sha1Hash )
		return FALSE;

	if ( szCSP )
		szTargetCSP = szCSP;
	else
		szTargetCSP = CL_DEFAULT_IMPORT_CSP;


	/* Buscamos el certificado por hash
	*/


	if ( LIBRT_IniciarDispositivo((unsigned char *) szDevice, szPass, &hClauer) != 0 ) {
		ret = FALSE;
		goto endCopyMoveCertFromClauer;
	}
	if ( LIBRT_LeerTipoBloqueCrypto(&hClauer, BLOQUE_CERT_PROPIO, 1, blockCert, &bnCert) != 0 ) {
		LIBRT_FinalizarDispositivo(&hClauer);
		ret = FALSE;
		goto endCopyMoveCertFromClauer;
	}

	while ( bnCert != -1 ) {

		if ( CRYPTO_X509_FingerPrint(BLOQUE_CERTPROPIO_Get_Objeto(blockCert), BLOQUE_CERTPROPIO_Get_Tam(blockCert), ALGID_SHA1, fingerPrint) != 0 ) {

			LIBRT_FinalizarDispositivo(&hClauer);
			ret = FALSE;
			goto endCopyMoveCertFromClauer;
		}
		if ( memcmp(fingerPrint, sha1Hash, TAM_SHA1) == 0 )
			break;
		if ( LIBRT_LeerTipoBloqueCrypto(&hClauer, BLOQUE_CERT_PROPIO, 0, blockCert, &bnCert) != 0 ) {
			LIBRT_FinalizarDispositivo(&hClauer);
			ret = FALSE;
			goto endCopyMoveCertFromClauer;
		}

	}

	if ( bnCert == -1 ) {
		LIBRT_FinalizarDispositivo(&hClauer);
		ret = FALSE;
		goto endCopyMoveCertFromClauer;
	}

	/* Buscamos la llave privada asociada
	*/

	if ( LIBRT_LeerTipoBloqueCrypto(&hClauer, BLOQUE_PRIVKEY_BLOB, 1, blockKey, &bnKey) != 0 ) {
		LIBRT_FinalizarDispositivo(&hClauer);
		ret = FALSE;
		goto endCopyMoveCertFromClauer;
	}


	while ( bnKey != -1 ) {

		if ( memcmp(BLOQUE_PRIVKEYBLOB_Get_Id(blockKey), BLOQUE_CERTPROPIO_Get_Id(blockCert), 20) == 0 )
			break;

		SecureZeroMemory(blockKey, TAM_BLOQUE);

		if ( LIBRT_LeerTipoBloqueCrypto(&hClauer, BLOQUE_PRIVKEY_BLOB, 0, blockKey, &bnKey) != 0 ) {
			LIBRT_FinalizarDispositivo(&hClauer);
			ret = FALSE;
			goto endCopyMoveCertFromClauer;
		}
	}

	if ( bnKey == -1 ) {
		LIBRT_FinalizarDispositivo(&hClauer);
		ret = FALSE;
		goto endCopyMoveCertFromClauer;
	}

	LIBRT_FinalizarDispositivo(&hClauer);

	/* Creamos un container nuevo e importamos la llave
	*/


	strcpy(szContainerName, "climportedkey_");

	if ( ! CryptAcquireContextA(&hProv, NULL, NULL, PROV_RSA_FULL, CRYPT_VERIFYCONTEXT) ) {
		ret = FALSE;
		goto endCopyMoveCertFromClauer;
	}

	if ( ! CryptGenRandom(hProv, 20, (LPBYTE) (szContainerName+14)) ) {
		ret = FALSE;
		goto endCopyMoveCertFromClauer;
	}

	CryptReleaseContext(hProv, 0);
	hProv = 0;

	for ( i = 0 ; i < 20 ; i++ ) 
		*(szContainerName+14+i) = 97 + (unsigned char) *(szContainerName+14+i) % 26;
	*(szContainerName+34) = 0;

	if ( ! CryptAcquireContextA(&hProv, szContainerName, szTargetCSP, PROV_RSA_FULL, CRYPT_NEWKEYSET) ) {
		ret = FALSE;
		goto endCopyMoveCertFromClauer;
	}

	if ( ! CryptImportKey(hProv, 
		BLOQUE_PRIVKEYBLOB_Get_Objeto(blockKey),
		BLOQUE_PRIVKEYBLOB_Get_Tam(blockKey),
		0,
		dwFlags,
		&hKey) )
	{
		ret = FALSE;
		goto endCopyMoveCertFromClauer;
	}

	dwAux = sizeof(ALG_ID);
	if ( ! CryptGetKeyParam(hKey,KP_ALGID,(BYTE *) &keySpec, &dwAux,0) ) {
		ret = FALSE;
		goto endCopyMoveCertFromClauer;
	}
	CryptReleaseContext(hProv, 0);
	hProv = 0;

	if ( keySpec == CALG_RSA_KEYX )
		keySpec = AT_KEYEXCHANGE;
	else if ( keySpec == CALG_RSA_SIGN )
		keySpec = AT_SIGNATURE;

	/* Insertamos el certificado
	*
	*     1. Convertir a formato DER
	*     2. Insertar en store MY
	*/

	if ( CRYPTO_X509_PEM2DER(BLOQUE_CERTPROPIO_Get_Objeto(blockCert),
		BLOQUE_CERTPROPIO_Get_Tam(blockCert),
		NULL,
		&tamDER) != 0 )
	{
		ret = FALSE;
		goto endCopyMoveCertFromClauer;
	}

	certDER = ( unsigned char * ) malloc ( tamDER );
	if ( ! certDER ) {
		ret = FALSE;
		goto endCopyMoveCertFromClauer;
	}
	if ( CRYPTO_X509_PEM2DER(BLOQUE_CERTPROPIO_Get_Objeto(blockCert),
		BLOQUE_CERTPROPIO_Get_Tam(blockCert),
		certDER,
		&tamDER) != 0 )
	{
		ret = FALSE;
		goto endCopyMoveCertFromClauer;
	}

	if ( ! (hStore = CertOpenStore(CERT_STORE_PROV_PHYSICAL, 0, 0, CERT_SYSTEM_STORE_CURRENT_USER, (const void *) store)) ) {
		ret = FALSE;
		goto endCopyMoveCertFromClauer;
	}
	pCertAux = CertCreateCertificateContext(X509_ASN_ENCODING,certDER,tamDER);
	if ( ! pCertAux ) {
		ret = FALSE;
		goto endCopyMoveCertFromClauer;
	}
	dwContainerName = MultiByteToWideChar(CP_ACP, 0, szContainerName, strlen(szContainerName)+1, NULL, 0);
	if ( dwContainerName <= 0 ) {
		ret = FALSE;
		goto endCopyMoveCertFromClauer;
	}
	wszContainerName = ( LPWSTR ) malloc ( dwContainerName * sizeof(WCHAR) );
	if ( ! wszContainerName ) {
		ret = FALSE;
		goto endCopyMoveCertFromClauer;
	}
	if ( MultiByteToWideChar(CP_ACP, 0, szContainerName, strlen(szContainerName)+1, wszContainerName, dwContainerName) <= 0 ) {
		ret = FALSE;
		goto endCopyMoveCertFromClauer;
	}

	ZeroMemory(&infoCSP, sizeof infoCSP);
	infoCSP.pwszContainerName = wszContainerName;
	infoCSP.pwszProvName      = WSZ_CL_DEFAULT_IMPORT_CSP;
	infoCSP.dwProvType        = PROV_RSA_FULL;
	infoCSP.dwFlags           = 0;
	infoCSP.cProvParam        = 0;
	infoCSP.rgProvParam       = NULL;
	infoCSP.dwKeySpec         = keySpec;

	if ( ! CertSetCertificateContextProperty(pCertAux, CERT_KEY_PROV_INFO_PROP_ID, 0, &infoCSP) ) {
		ret = FALSE;
		goto endCopyMoveCertFromClauer;
	}


	if ( ! CertAddCertificateContextToStore(hStore,pCertAux,CERT_STORE_ADD_REPLACE_EXISTING,NULL) ) 
	{
		DWORD err = GetLastError();
		if ( err == E_INVALIDARG )
			ret = FALSE;
		goto endCopyMoveCertFromClauer;
	} 

	CertCloseStore(hStore, 0);
	hStore = 0;

	if ( bMove ) {

		/* Borramos el certificado, el blob, la llave pem y el container
		*/
		if ( LIBRT_IniciarDispositivo((unsigned char *)szDevice, szPass, &hClauer) != 0 ) {
			ret = FALSE;
			goto endCopyMoveCertFromClauer;
		}
		if ( LIBRT_BorrarBloqueCrypto(&hClauer, bnCert) != 0 ) {
			LIBRT_FinalizarDispositivo(&hClauer);
			ret = FALSE;
			goto endCopyMoveCertFromClauer;
		}
		if ( LIBRT_BorrarBloqueCrypto(&hClauer, bnKey) != 0 ) {
			LIBRT_FinalizarDispositivo(&hClauer);
			ret = FALSE;
			goto endCopyMoveCertFromClauer;
		}
		if ( LIBRT_LeerTipoBloqueCrypto(&hClauer, BLOQUE_KEY_CONTAINERS, 1, blockContainer, &bnContainer) != 0 ) {
			LIBRT_FinalizarDispositivo(&hClauer);
			ret = FALSE;
			goto endCopyMoveCertFromClauer;
		}

		memset(zeroId, 0, 20);

		while ( bnContainer != -1 ) {

			INFO_KEY_CONTAINER ikc[NUM_KEY_CONTAINERS];
			unsigned int ikcSize;
			BOOL bContainerDeleted;

			if ( BLOQUE_KeyContainer_Enumerar(blockContainer, ikc, &ikcSize) != 0 ) {
				LIBRT_FinalizarDispositivo(&hClauer);
				ret = FALSE;
				goto endCopyMoveCertFromClauer;
			}

			bContainerDeleted = FALSE;

			for ( i = 0 ; !bContainerDeleted && (i < ikcSize) ; i++ ) {

				if ( memcmp(ikc[i].idExchange, BLOQUE_PRIVKEYBLOB_Get_Id(blockKey), 20) == 0 ) {
					if ( memcmp(ikc[i].idSignature, BLOQUE_PRIVKEYBLOB_Get_Id(blockKey), 20 ) == 0 ) {
						BLOQUE_KeyContainer_Establecer_ID_Exchange(blockContainer, ikc[i].nombreKeyContainer, zeroId);
						if ( LIBRT_EscribirBloqueCrypto(&hClauer, bnContainer, blockContainer) != 0 ) {
							LIBRT_FinalizarDispositivo(&hClauer);
							ret = FALSE;
							goto endCopyMoveCertFromClauer;
						}
						bContainerDeleted = TRUE;
					} else {
						BLOQUE_KeyContainer_Borrar(blockContainer, ikc[i].nombreKeyContainer);
						if ( LIBRT_EscribirBloqueCrypto(&hClauer, bnContainer, blockContainer) != 0 ) {
							LIBRT_FinalizarDispositivo(&hClauer);
							ret = FALSE;
							goto endCopyMoveCertFromClauer;
						}
						bContainerDeleted = TRUE;

					}
				} else if ( memcmp(ikc[i].idSignature, BLOQUE_PRIVKEYBLOB_Get_Id(blockKey), 20) == 0 ) {
					if ( memcmp(ikc[i].idExchange, BLOQUE_PRIVKEYBLOB_Get_Id(blockKey), 20 ) == 0 ) {
						BLOQUE_KeyContainer_Establecer_ID_Signature(blockContainer, ikc[i].nombreKeyContainer, zeroId);
						if ( LIBRT_EscribirBloqueCrypto(&hClauer, bnContainer, blockContainer) != 0 ) {
							LIBRT_FinalizarDispositivo(&hClauer);
							ret = FALSE;
							goto endCopyMoveCertFromClauer;
						}
						bContainerDeleted = TRUE;
					} else {
						BLOQUE_KeyContainer_Borrar(blockContainer, ikc[i].nombreKeyContainer);
						if ( LIBRT_EscribirBloqueCrypto(&hClauer, bnContainer, blockContainer) != 0 ) {
							LIBRT_FinalizarDispositivo(&hClauer);
							ret = FALSE;
							goto endCopyMoveCertFromClauer;
						}
						bContainerDeleted = TRUE;
					}
				}
			}

			if ( ! bContainerDeleted ) {
				if ( LIBRT_LeerTipoBloqueCrypto(&hClauer, BLOQUE_KEY_CONTAINERS, 0, blockContainer, &bnContainer) != 0 ) {
					LIBRT_FinalizarDispositivo(&hClauer);
					ret = FALSE;
					goto endCopyMoveCertFromClauer;
				}
			}

		} /* bnContainer == -1 */


		/* Borramos la llave PEM
		*/

		if ( LIBRT_LeerTipoBloqueCrypto(&hClauer, BLOQUE_LLAVE_PRIVADA, 1, blockKey, &bnKey) != 0 ) {
			LIBRT_FinalizarDispositivo(&hClauer);
			ret = FALSE;
			goto endCopyMoveCertFromClauer;
		}

		while ( bnKey != -1 ) {
			if ( memcmp(BLOQUE_LLAVEPRIVADA_Get_Id(blockKey), BLOQUE_CERTPROPIO_Get_Id(blockCert), 20) == 0 ) {
				if ( LIBRT_BorrarBloqueCrypto(&hClauer, bnKey) != 0 ) {
					LIBRT_FinalizarDispositivo(&hClauer);
					ret = FALSE;
					goto endCopyMoveCertFromClauer;
				}
			}
			if ( LIBRT_LeerTipoBloqueCrypto(&hClauer, BLOQUE_LLAVE_PRIVADA, 0, blockKey, &bnKey) != 0 ) {
				LIBRT_FinalizarDispositivo(&hClauer);
				ret = FALSE;
				goto endCopyMoveCertFromClauer;
			}
		}

		LIBRT_FinalizarDispositivo(&hClauer);
	}

endCopyMoveCertFromClauer:


	SecureZeroMemory(blockKey, TAM_BLOQUE);

	if ( hStore ) 
		CertCloseStore(hStore, 0);
	if ( certDER )
		free(certDER);

	return ret;

}















BOOL CopyMoveCertToClauer ( char *szDevice,
						   char *szPass,
						   unsigned char sha1Hash[20],
						   BOOL bMove )
{
	HCERTSTORE hStore = 0;
	CRYPT_HASH_BLOB hb;
	PCCERT_CONTEXT certCtx = NULL;
//	char szAuxDevice[MAX_PATH_LEN+1];

	unsigned char *devs[MAX_DEVICES];
	int nDevs = 0, i;

	BOOL ret = TRUE;

	if ( ! sha1Hash )
		return FALSE;
	if ( ! szPass )
		return FALSE;
	if ( ! szDevice ) 
		return FALSE;


/*	
    hStore = CertOpenSystemStore(0, "MY");
	if ( ! hStore ) {
		ret = FALSE;
		goto endCopyMoveCertToClauer;
	}
*/
	WCHAR store2[] = L"My\\.Default";
	hStore = CertOpenStore(CERT_STORE_PROV_PHYSICAL, 0, NULL, CERT_SYSTEM_STORE_CURRENT_USER, (const void *) store2);

	if ( ! hStore ) {
		ret = FALSE;
		goto endCopyMoveCertToClauer;
	}


	hb.pbData = sha1Hash;
	hb.cbData = 20;

	certCtx = CertFindCertificateInStore(hStore, 
		X509_ASN_ENCODING,
		0,
		CERT_FIND_SHA1_HASH,
		&hb,
		NULL);

	if ( ! certCtx ) {
		ret = FALSE;
		goto endCopyMoveCertToClauer;
	}

	if ( CLIMPORT_CAPI_MY_Cert(szDevice, szPass, &certCtx, bMove) != 0 ) {
		ret = FALSE;
		goto endCopyMoveCertToClauer;
	}


endCopyMoveCertToClauer:

	if ( nDevs ) {
		for ( i = 0 ; i < nDevs ; i++ ) {
			free(devs[i]);
		}
	}

	if ( certCtx ) 
		CertFreeCertificateContext(certCtx);

	if ( hStore ) {
		CertCloseStore(hStore, 0);
	}

	return ret;

}








int CLIMPORT_CAPI_MY_Cert ( char *szDevice,
						   char *szPwd,
						   PCCERT_CONTEXT *certCtx,
						   BOOL bDelete )
{

	BYTE *pvkBlob = NULL;
	DWORD dwpvkBlobSize = 0;
	CRYPT_KEY_PROV_INFO *ki = NULL;
	DWORD kiSize, dwKeyExp, dwKeyExpSize;

	unsigned char *certPEM = NULL;
	unsigned long certPEMSize = 0;

	unsigned char *pemKey = NULL;
	unsigned long pemKeySize;

	unsigned char block[TAM_BLOQUE], block2[TAM_BLOQUE];
	long blockNumber;

	unsigned char idCert[20], id[20];

	LPWSTR wszFriendlyName = NULL;
	LPSTR lpszFriendlyName = NULL;
	DWORD dwwszFriendlyNameSize, dwlpszFriendlyNameSize;

	char containerName[257];
	int i, friendlyNameExists;

	unsigned char zeroId[20];

	int ret = CL_SUCCESS;

	HCRYPTPROV hProv = 0;
	HCRYPTKEY hKey = 0;

	USBCERTS_HANDLE hClauer;

	long bnCert=-1, bnPriv=-1, bnBlob=-1;


	if ( ! szDevice )
		return ERR_CL_BAD_PARAM;
	if ( ! szPwd ) 
		return ERR_CL_BAD_PARAM;
	if ( ! certCtx )
		return ERR_CL_BAD_PARAM;
	if ( ! *certCtx )
		return ERR_CL_BAD_PARAM;


	memset(zeroId, 0, 20);

	/* Compruebo si la llave asociada es exportable o no
	*/

	kiSize = sizeof(CRYPT_KEY_PROV_INFO);
	if ( ! CertGetCertificateContextProperty(*certCtx, CERT_KEY_PROV_INFO_PROP_ID, NULL, &kiSize) ) {
		ret = ERR_CL_CANNOT_GET_ASSOCIATED_KEY;
		goto endCLIMPORT_CAPI_MY_Cert;
	}
	ki = ( CRYPT_KEY_PROV_INFO * ) malloc ( kiSize );
	if ( ! ki ) {
		ret = ERR_CL_OUT_OF_MEMORY;
		goto endCLIMPORT_CAPI_MY_Cert;
	}
	if ( ! CertGetCertificateContextProperty(*certCtx, CERT_KEY_PROV_INFO_PROP_ID, ki, &kiSize) ) {
		ret = ERR_CL_CANNOT_GET_ASSOCIATED_KEY;
		goto endCLIMPORT_CAPI_MY_Cert;
	}  

	if ( ! CryptAcquireContextW(&hProv, ki->pwszContainerName, ki->pwszProvName, ki->dwProvType, ki->dwFlags) ) {
		ret = ERR_CL_CANNOT_GET_ASSOCIATED_KEY;
		goto endCLIMPORT_CAPI_MY_Cert;
	}

	if ( ! CryptGetUserKey(hProv, ki->dwKeySpec, &hKey) ) {
		if ( GetLastError() == NTE_NO_KEY )
			ret = ERR_CL_KEY_NOT_EXISTS;
		else
			ret = ERR_CL_CANNOT_GET_ASSOCIATED_KEY;
		goto endCLIMPORT_CAPI_MY_Cert;
	}


	dwKeyExpSize = sizeof(DWORD);
	if ( ! CryptGetKeyParam(hKey, KP_PERMISSIONS, (BYTE *) &dwKeyExp, &dwKeyExpSize, 0) ) {
		ret = ERR_CL;
		goto endCLIMPORT_CAPI_MY_Cert;
	}


	if ( ! (dwKeyExp & CRYPT_EXPORT) ) {
		ret = ERR_CL_KEY_NOT_EXPORTABLE;
		goto endCLIMPORT_CAPI_MY_Cert;
	}

	/* Ahora vamos a intentar exportar las llaves
	*/

	if ( ! CryptExportKey(hKey, 0, PRIVATEKEYBLOB, 0, NULL, &dwpvkBlobSize) ) {
		ret = ERR_CL;
		goto endCLIMPORT_CAPI_MY_Cert;
	}

	pvkBlob = ( unsigned char * ) malloc ( dwpvkBlobSize );
	if ( ! pvkBlob ) {
		ret = ERR_CL_OUT_OF_MEMORY;
		goto endCLIMPORT_CAPI_MY_Cert;
	}

	if ( ! CryptExportKey(hKey, 0, PRIVATEKEYBLOB, 0, pvkBlob, &dwpvkBlobSize) ) {
		ret = ERR_CL;
		goto endCLIMPORT_CAPI_MY_Cert;
	}

	/* Pasamos la llave a formato PEM
	*/

	if ( CRYPTO_BLOB2LLAVE(pvkBlob, dwpvkBlobSize, NULL, &pemKeySize) != 0 ) {
		ret = ERR_CL;
		goto endCLIMPORT_CAPI_MY_Cert;
	}

	pemKey = ( unsigned char *) malloc ( pemKeySize );
	if ( ! pemKey ) {
		ret = ERR_CL_OUT_OF_MEMORY;
		goto endCLIMPORT_CAPI_MY_Cert;
	}

	if ( CRYPTO_BLOB2LLAVE(pvkBlob, dwpvkBlobSize, pemKey, &pemKeySize) != 0 ) {
		ret = ERR_CL;
		goto endCLIMPORT_CAPI_MY_Cert;
	}

	/* Pasamos el certificado a formato PEM
	*/

	if ( CRYPTO_X509_DER2PEM((*certCtx)->pbCertEncoded, (*certCtx)->cbCertEncoded, NULL, &certPEMSize) != 0 ) {
		ret = ERR_CL;
		goto endCLIMPORT_CAPI_MY_Cert;
	}
	certPEM = ( unsigned char * ) malloc ( certPEMSize +1);
	if ( ! certPEM ) {
		ret = ERR_CL_OUT_OF_MEMORY;
		goto endCLIMPORT_CAPI_MY_Cert;
	}
	if ( CRYPTO_X509_DER2PEM((*certCtx)->pbCertEncoded, (*certCtx)->cbCertEncoded, certPEM, &certPEMSize) != 0 ) {
		ret = ERR_CL;
		goto endCLIMPORT_CAPI_MY_Cert;
	}

	if ( CRYPTO_CERT_PEM_Id(certPEM, certPEMSize, idCert) != 0 ) {
		ret = ERR_CL;
		goto endCLIMPORT_CAPI_MY_Cert;
	}

	/* Si los objetos ya han sido importados no los reimportamos
	*
	* 1. Insertamos el certificado
	*/

	if ( LIBRT_IniciarDispositivo((unsigned char *)szDevice, szPwd, &hClauer) != 0 ) {
		ret = ERR_CL_CANNOT_INITIALIZE;
		goto endCLIMPORT_CAPI_MY_Cert;
	}

	if ( LIBRT_LeerTipoBloqueCrypto(&hClauer, BLOQUE_CERT_PROPIO, 1, block, &blockNumber) != 0 ) {
		LIBRT_FinalizarDispositivo(&hClauer);
		ret = ERR_CL;
		goto endCLIMPORT_CAPI_MY_Cert;
	}

	while ( blockNumber != -1 ) {

		if ( memcmp(BLOQUE_CERTPROPIO_Get_Id(block), idCert, 20) == 0 ) 
			if ( CRYPTO_CERT_Cmp(BLOQUE_CERTPROPIO_Get_Objeto(block), BLOQUE_CERTPROPIO_Get_Tam(block), certPEM, certPEMSize) == 0 ) 
				break;

		if ( LIBRT_LeerTipoBloqueCrypto(&hClauer, BLOQUE_CERT_PROPIO, 0, block, &blockNumber) != 0 ) {
			LIBRT_FinalizarDispositivo(&hClauer);
			ret = ERR_CL;
			goto endCLIMPORT_CAPI_MY_Cert;
		}
	}


	if ( blockNumber == -1 ) {    

		memset(block, 0, TAM_BLOQUE);

		/* Obtengo el friendly name del certificado
		*/

		if ( ! CertGetCertificateContextProperty(*certCtx, CERT_FRIENDLY_NAME_PROP_ID, NULL, &dwwszFriendlyNameSize) ) {

			if ( GetLastError() == CRYPT_E_NOT_FOUND )
				friendlyNameExists = 0;
			else {
				LIBRT_FinalizarDispositivo(&hClauer);
				ret = ERR_CL;
				goto endCLIMPORT_CAPI_MY_Cert;
			}
		} else {
			friendlyNameExists = 1;
		}

		if ( friendlyNameExists ) {
			wszFriendlyName = ( LPWSTR ) malloc ( dwwszFriendlyNameSize );
			if ( ! wszFriendlyName ) {
				LIBRT_FinalizarDispositivo(&hClauer);
				ret = ERR_CL_OUT_OF_MEMORY;
				goto endCLIMPORT_CAPI_MY_Cert;
			}

			if ( ! CertGetCertificateContextProperty(*certCtx, CERT_FRIENDLY_NAME_PROP_ID, wszFriendlyName, &dwwszFriendlyNameSize) ) {
				LIBRT_FinalizarDispositivo(&hClauer);
				ret = ERR_CL;
				goto endCLIMPORT_CAPI_MY_Cert;
			}

			dwlpszFriendlyNameSize = WideCharToMultiByte(CP_ACP, 0, wszFriendlyName, dwwszFriendlyNameSize/sizeof(WCHAR), NULL, 0, NULL, NULL);
			if ( ! dwlpszFriendlyNameSize ) {
				LIBRT_FinalizarDispositivo(&hClauer);
				ret = ERR_CL;
				goto endCLIMPORT_CAPI_MY_Cert;
			}

			lpszFriendlyName = ( char * ) malloc ( dwlpszFriendlyNameSize );
			if ( ! lpszFriendlyName ) {
				LIBRT_FinalizarDispositivo(&hClauer);
				ret = ERR_CL_OUT_OF_MEMORY;
				goto endCLIMPORT_CAPI_MY_Cert;
			}

			if ( ! WideCharToMultiByte(CP_ACP, 0, wszFriendlyName, dwwszFriendlyNameSize/sizeof(WCHAR), lpszFriendlyName, dwlpszFriendlyNameSize, NULL, NULL) ) {
				LIBRT_FinalizarDispositivo(&hClauer);
				ret = ERR_CL;
				goto endCLIMPORT_CAPI_MY_Cert;
			}

			free(wszFriendlyName);
			wszFriendlyName = NULL;
		}

		/* Insertamos el bloque
		*/

		BLOQUE_Set_Claro(block);
		BLOQUE_CERTPROPIO_Nuevo(block);
		BLOQUE_CERTPROPIO_Set_Objeto(block, certPEM, certPEMSize);
		BLOQUE_CERTPROPIO_Set_Tam(block, certPEMSize);
		BLOQUE_CERTPROPIO_Set_Id(block, idCert);
		BLOQUE_CERTPROPIO_Set_FriendlyName(block, lpszFriendlyName);

		free(lpszFriendlyName);
		lpszFriendlyName = NULL;

		if ( LIBRT_InsertarBloqueCrypto(&hClauer, block, &blockNumber) != 0 ) {
			LIBRT_FinalizarDispositivo(&hClauer);
			ret = ERR_CL;
			goto endCLIMPORT_CAPI_MY_Cert;
		}

		bnCert = blockNumber;

	}

	/* 2. Insertamos la llave privada
	*/

	if ( LIBRT_LeerTipoBloqueCrypto(&hClauer, BLOQUE_LLAVE_PRIVADA, 1, block, &blockNumber) != 0 ) {
		LIBRT_FinalizarDispositivo(&hClauer);
		ret = ERR_CL;
		goto endCLIMPORT_CAPI_MY_Cert;
	}

	if ( CRYPTO_LLAVE_PEM_Id(pemKey, pemKeySize, 1, NULL, id) != 0 ) {
		LIBRT_FinalizarDispositivo(&hClauer);
		ret = ERR_CL;
		goto endCLIMPORT_CAPI_MY_Cert;
	}

	while ( blockNumber != -1 ) {

		if ( memcmp(BLOQUE_LLAVEPRIVADA_Get_Id(block), id, 20) == 0 ) 
			break;

		SecureZeroMemory(block, TAM_BLOQUE);
		if ( LIBRT_LeerTipoBloqueCrypto(&hClauer, BLOQUE_LLAVE_PRIVADA, 0, block, &blockNumber) != 0 ) {
			LIBRT_FinalizarDispositivo(&hClauer);
			ret = ERR_CL;
			goto endCLIMPORT_CAPI_MY_Cert;
		}

	}

	SecureZeroMemory(block, TAM_BLOQUE);
	if ( blockNumber == -1 ) {

		BLOQUE_Set_Cifrado(block);
		BLOQUE_LLAVEPRIVADA_Nuevo(block);
		BLOQUE_LLAVEPRIVADA_Set_Objeto(block, pemKey, pemKeySize);
		BLOQUE_LLAVEPRIVADA_Set_Tam(block, pemKeySize);
		BLOQUE_LLAVEPRIVADA_Set_Id(block, id);

		if ( LIBRT_InsertarBloqueCrypto(&hClauer, block, &blockNumber) != 0 ) {
			LIBRT_FinalizarDispositivo(&hClauer);
			ret = ERR_CL;
			goto endCLIMPORT_CAPI_MY_Cert;
		}

		bnPriv = blockNumber;
		SecureZeroMemory(block,TAM_BLOQUE);
	}

	/* 3. Insertamos container y blob
	*/

	if ( LIBRT_LeerTipoBloqueCrypto(&hClauer, BLOQUE_PRIVKEY_BLOB, 1, block, &blockNumber) != 0 ) {
		LIBRT_FinalizarDispositivo(&hClauer);
		ret = ERR_CL;
		goto endCLIMPORT_CAPI_MY_Cert;
	}

	while ( blockNumber != -1 ) {
		if ( memcmp(BLOQUE_PRIVKEYBLOB_Get_Id(block), id, 20) == 0 ) 
			break;

		SecureZeroMemory(block, TAM_BLOQUE);
		if ( LIBRT_LeerTipoBloqueCrypto(&hClauer, BLOQUE_PRIVKEY_BLOB, 0, block, &blockNumber) != 0 ) {
			LIBRT_FinalizarDispositivo(&hClauer);
			ret = ERR_CL;
			goto endCLIMPORT_CAPI_MY_Cert;
		}
	}

	SecureZeroMemory(block, TAM_BLOQUE);
	if ( blockNumber == -1 ) {

		BLOQUE_Set_Cifrado(block);
		BLOQUE_PRIVKEYBLOB_Nuevo(block);
		BLOQUE_PRIVKEYBLOB_Set_Tam(block, dwpvkBlobSize);
		BLOQUE_PRIVKEYBLOB_Set_Id(block, id);
		BLOQUE_PRIVKEYBLOB_Set_Objeto(block, pvkBlob, dwpvkBlobSize);
		
		if ( LIBRT_InsertarBloqueCrypto(&hClauer, block, &blockNumber) != 0 ) {
			LIBRT_FinalizarDispositivo(&hClauer);
			ret = ERR_CL;
			goto endCLIMPORT_CAPI_MY_Cert;
		}

		bnBlob = blockNumber;


		/* Insertamos un container nuevo
		*/

		if ( ! CryptGenRandom(hProv, 10, (LPBYTE) containerName) ) {
			LIBRT_FinalizarDispositivo(&hClauer);
			ret = ERR_CL;
			goto endCLIMPORT_CAPI_MY_Cert;
		}
		for ( i = 0 ; i < 10; i++ ) 
			containerName[i] = 97 + ( (unsigned char)containerName[i] % 26);
		containerName[10] = 0;

		if ( LIBRT_LeerTipoBloqueCrypto(&hClauer, BLOQUE_KEY_CONTAINERS, 1, block, &blockNumber) != 0 ) {
			LIBRT_FinalizarDispositivo(&hClauer);
			ret = ERR_CL;
			goto endCLIMPORT_CAPI_MY_Cert;
		}

		while ( blockNumber != -1 ) {

			unsigned int nContainers;

			if ( BLOQUE_KeyContainer_Enumerar(block, NULL, &nContainers) != 0 ) {
				LIBRT_FinalizarDispositivo(&hClauer);
				ret = ERR_CL;
				goto endCLIMPORT_CAPI_MY_Cert;
			}

			if ( nContainers < NUM_KEY_CONTAINERS ) 
				break;

			if ( LIBRT_LeerTipoBloqueCrypto(&hClauer, BLOQUE_KEY_CONTAINERS, 0, block, &blockNumber) != 0 ) {
				LIBRT_FinalizarDispositivo(&hClauer);
				ret = ERR_CL;
				goto endCLIMPORT_CAPI_MY_Cert;
			}
		}

		if ( blockNumber == -1 ) {
			memset(block, 0, TAM_BLOQUE);
			BLOQUE_Set_Claro(block);
			BLOQUE_KeyContainer_Nuevo(block);
		}

		BLOQUE_KeyContainer_Insertar(block, containerName);
		if ( ki->dwKeySpec == AT_SIGNATURE ) {

			BLOQUE_KeyContainer_EstablecerSIGNATURE(block, containerName, -1, 0);
			BLOQUE_KeyContainer_Establecer_ID_Signature(block, containerName, idCert);

			BLOQUE_KeyContainer_EstablecerEXCHANGE(block, containerName, -1, 0);
			BLOQUE_KeyContainer_Establecer_ID_Exchange(block, containerName, zeroId);

		} else if ( ki->dwKeySpec == AT_KEYEXCHANGE ) {

			BLOQUE_KeyContainer_EstablecerEXCHANGE(block, containerName, -1, 0);
			BLOQUE_KeyContainer_Establecer_ID_Exchange(block, containerName, idCert);

			BLOQUE_KeyContainer_EstablecerSIGNATURE(block, containerName, -1, 0);
			BLOQUE_KeyContainer_Establecer_ID_Signature(block, containerName, zeroId);

		} else {
			LIBRT_FinalizarDispositivo(&hClauer);
			ret = ERR_CL;
			goto endCLIMPORT_CAPI_MY_Cert;
		}

		if ( blockNumber == -1 ) {
			if ( LIBRT_InsertarBloqueCrypto(&hClauer, block, &blockNumber) != 0 ) {
				LIBRT_FinalizarDispositivo(&hClauer);
				ret = ERR_CL;
				goto endCLIMPORT_CAPI_MY_Cert;
			}
		} else {
			if ( LIBRT_EscribirBloqueCrypto(&hClauer, blockNumber, block) != 0 ) {
				LIBRT_FinalizarDispositivo(&hClauer);
				ret = ERR_CL;
				goto endCLIMPORT_CAPI_MY_Cert;
			}
		}
	}

	LIBRT_FinalizarDispositivo(&hClauer);


	/* Si bDelete es TRUE entonces el certificado y la llave del
	* framework
	*/

	if ( bDelete ) {

		/* Borramos el container. La pol�tica es la siguiente. Si la �nica llave
		* del container es la asociada con el certificado, borramos por completo
		* el container. Si hay una llave de tipo distinto, lo que hacemos es
		* generar una nueva llave para machacar la existente (podr�amos importar
		* un blob tambi�n... ser�a m�s eficiente)
		*/

		DWORD dwKeySpec = (ki->dwKeySpec == AT_SIGNATURE) ? AT_KEYEXCHANGE : AT_SIGNATURE;
		HCRYPTKEY hKeyAux;

		if ( ! CryptGetUserKey(hProv, dwKeySpec, &hKeyAux) ) {
			if ( GetLastError() == NTE_NO_KEY ) {	
				CryptReleaseContext(hProv, 0);
				if ( ! CryptAcquireContextW(&hProv, ki->pwszContainerName, ki->pwszProvName, ki->dwProvType, CRYPT_DELETEKEYSET) ) {
					hProv = 0;
					ret = ERR_CL_CANNOT_DELETE_KEY;
					goto endCLIMPORT_CAPI_MY_Cert;
				}
			} else {
				ret = ERR_CL_CANNOT_DELETE_KEY;
				goto endCLIMPORT_CAPI_MY_Cert;
			}
		} else {
			CryptDestroyKey(hKey);
			if ( ! CryptGenKey(hProv, dwKeySpec, 512 << 16, &hKey) ) {
				ret = ERR_CL_CANNOT_DELETE_KEY;
				goto endCLIMPORT_CAPI_MY_Cert;
			}
		}

		if ( ! CertDeleteCertificateFromStore(*certCtx) ) {
			*certCtx = NULL;
			ret = ERR_CL_CANNOT_DELETE_CERT;
			goto endCLIMPORT_CAPI_MY_Cert;
		} else
			*certCtx = NULL;
	}


endCLIMPORT_CAPI_MY_Cert:


	if ( hKey ) 
		CryptDestroyKey(hKey);

	if ( hProv ) 
		CryptReleaseContext(hProv, 0);

	if ( certPEM ) {
		SecureZeroMemory(certPEM, certPEMSize);
		free(certPEM);
	}

	if ( pemKey ) {
		SecureZeroMemory(pemKey, pemKeySize);
		free(pemKey);
	}

	SecureZeroMemory(block, TAM_BLOQUE);
	SecureZeroMemory(block2, TAM_BLOQUE);

	if ( wszFriendlyName ) 
		free(wszFriendlyName);

	if ( lpszFriendlyName )
		free(lpszFriendlyName);

	if ( ki )
		free(ki);

	/* Si hubo un error, borramos los bloques que hubi�ramos
	* podido insertar
	*/

	if ( ret != CL_SUCCESS && ret != ERR_CL_CANNOT_DELETE_KEY && ret != ERR_CL_CANNOT_DELETE_CERT ) {
//		USBCERTS_HANDLE h;

		if ( LIBRT_IniciarDispositivo((unsigned char *)szDevice, szPwd, &hClauer) == 0 ) {
			if ( bnBlob != -1 ) 
				LIBRT_BorrarBloqueCrypto(&hClauer, bnBlob);
			if ( bnPriv != -1 )
				LIBRT_BorrarBloqueCrypto(&hClauer, bnPriv);
			if ( bnCert != -1 )
				LIBRT_BorrarBloqueCrypto(&hClauer, bnCert);

			LIBRT_FinalizarDispositivo(&hClauer);
		}
	}


	return ret;
}





#ifdef __TEST__

int main ( void )
{
	char dev[] = "c:\\cryf_000.cla";
	char pwd[] = "123clauer";

	unsigned char sha1Hash[] = {0xCA2,0xCF,0xAC,0x77,0xE4,0x18,0x4C,0xE0,0x47,0xAE,0xB3,0x6A,0x11,0xBE,0x9E,0x01};


	LIBRT_Ini();
	CRYPTO_Ini();

	if ( ! CopyMoveCertFromClauer ( dev, pwd, sha1Hash, NULL, CRYPT_EXPORTABLE, FALSE ) ) {
		fprintf(stderr, "[ERROR] CopyMoveCertFromClauer\n");
		return 1;
	}


	return 0;
}

#endif

