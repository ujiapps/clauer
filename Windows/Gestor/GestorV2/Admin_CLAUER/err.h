#ifndef __CLAUER_ERR_H__
#define __CLAUER_ERR_H__

#define CL_SUCCESS                    0
#define ERR_CL                       -1
#define ERR_CL_OUT_OF_MEMORY         -2    /* No hay memoria suficiente (malloc devuelve NULL) */
#define ERR_CL_NO_KEY_FOUND          -3    /* No se encontr� llave privada asociada */
#define ERR_CL_CREATING_P12          -4    /* Error creando el pkcs12 */
#define ERR_CL_NOT_ENOUGH_BUFFER     -5    /* Cuando no hay suficiente buffer de salida reservado */
#define ERR_CL_BAD_PARAM             -6    /* Par�metro pasado incorrecto */
#define ERR_CL_CANNOT_INITIALIZE     -7    /* No se pudo iniciar dispositivo */
#define ERR_CL_BAD_BLOCK_TYPE        -8    /* El bloque le�do no es del tipo esperado  */

#define ERR_CL_KEY_NOT_EXPORTABLE           -9    /* La llave no es exportable */
#define ERR_CL_CANNOT_GET_ASSOCIATED_KEY   -10    /* No se pudo obtener llave asociada */
#define ERR_CL_KEY_NOT_EXISTS              -11    /* La llave indicada no existe */
#define ERR_CL_NO_PERM                     -12    /* No tiene privilegios suficientes para realizar operaci�n */
#define ERR_CL_CANNOT_OPEN_FILE            -13    /* No se puede abrir fichero */
#define ERR_CL_CANNOT_DELETE_KEY           -14    /* No se pudo borrar llave (Crypto API) */
#define ERR_CL_CANNOT_DELETE_CERT          -15    /* No se pudo borrar certificado (Crypto API) */

#endif
