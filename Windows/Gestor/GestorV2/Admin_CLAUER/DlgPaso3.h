#include "afxwin.h"
#if !defined(AFX_DLGPASO3_H__CC8E58C5_C5CF_4798_A3C8_85829EBEF0DD__INCLUDED_)
#define AFX_DLGPASO3_H__CC8E58C5_C5CF_4798_A3C8_85829EBEF0DD__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// DlgPaso3.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// DlgPaso3 dialog

class DlgPaso3 : public CDialog
{
// Construction
public:
	void Reset(void);
	void Paso (void);
	void SetPaso (int paso);
	void SetDescripcion(CString descripcion);
	DlgPaso3(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(DlgPaso3)
	enum { IDD = IDD_DIALOG_PASO_3 };
	CStatic	m_barra;
	CButton	m_descrip;
	CStatic	m_texto;
	CStatic	m_lblDescripcion;
	CProgressCtrl	m_prgProgreso;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(DlgPaso3)
	public:
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(DlgPaso3)
	afx_msg void OnShowWindow(BOOL bShow, UINT nStatus);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
	
public:
	afx_msg HBRUSH OnCtlColor(CDC* pDC, CWnd* pWnd, UINT nCtlColor);
public:
	CStatic m_advertencia;
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DLGPASO3_H__CC8E58C5_C5CF_4798_A3C8_85829EBEF0DD__INCLUDED_)
