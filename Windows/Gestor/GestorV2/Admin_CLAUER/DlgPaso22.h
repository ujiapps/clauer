#if !defined(AFX_DLGPASO22_H__0B2B71D2_02EC_4475_B1D3_A974544D04B1__INCLUDED_)
#define AFX_DLGPASO22_H__0B2B71D2_02EC_4475_B1D3_A974544D04B1__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// DlgPaso22.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// DlgPaso22 dialog

class DlgPaso22 : public CDialog
{
// Construction
public:
	DlgPaso22(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(DlgPaso22)
	enum { IDD = IDD_DIALOG_PASO_22 };
	CButton	m_cuadro;
	CStatic	m_imp;
	CStatic	m_exp;
	CStatic	m_elim;
	CStatic	m_disq;
	CStatic	m_texto;
	int		m_radio;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(DlgPaso22)
	public:
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(DlgPaso22)
	virtual BOOL OnInitDialog();
	afx_msg void OnShowWindow(BOOL bShow, UINT nStatus);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
public:
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DLGPASO22_H__0B2B71D2_02EC_4475_B1D3_A974544D04B1__INCLUDED_)
