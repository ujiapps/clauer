#pragma once
#include "afxwin.h"

#define CLAUER_STORE_NAME      "CLAUERSTORE"
#define CLAUER_STORE_MY_PARAM 0

typedef CMap<int, int,CString,CString> AlmacenCertificados;
// Cuadro de di�logo de DlgDelete

class DlgDelete : public CDialog
{
	DECLARE_DYNAMIC(DlgDelete)

public:
	DlgDelete(CWnd* pParent = NULL);   // Constructor est�ndar
	virtual ~DlgDelete();

// Datos del cuadro de di�logo
	enum { IDD = IDD_DIALOG_DELETE };

private:

	// asocia posici�n en la lista con
	// bloque dentro del clauer

	CMap<int,int,unsigned long, unsigned long> m_mListBlock;	

	// Carga la lista de certificados

	BOOL LoadCertList ( void );

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // Compatibilidad con DDX/DDV
    AlmacenCertificados certificadosClauer;
	DECLARE_MESSAGE_MAP()
public:
	CStatic m_texto;
public:
	CStatic m_texto2;
public:
	virtual BOOL OnInitDialog();
public:
	virtual BOOL PreTranslateMessage(MSG* pMsg);
public:
	CListBox m_ListBox;
public:
	afx_msg void OnBnClickedButton1();
public:
	afx_msg void OnShowWindow(BOOL bShow, UINT nStatus);
};
