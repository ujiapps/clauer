#if !defined(AFX_DLGPASO23_H__9402FFA4_BF52_479E_A967_6D5D65FDEE94__INCLUDED_)
#define AFX_DLGPASO23_H__9402FFA4_BF52_479E_A967_6D5D65FDEE94__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// DlgPaso23.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// DlgPaso23 dialog

class DlgPaso23 : public CDialog
{
// Construction
public:
	DlgPaso23(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(DlgPaso23)
	enum { IDD = IDD_DIALOG_PASO_23 };
	CStatic	m_ub;
	CStatic	m_ie;
	CStatic	m_disq;
	CButton	m_cuadro;
	CStatic	m_texto;
	int		m_radio;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(DlgPaso23)
	public:
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(DlgPaso23)
	afx_msg void OnShowWindow(BOOL bShow, UINT nStatus);
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DLGPASO23_H__9402FFA4_BF52_479E_A967_6D5D65FDEE94__INCLUDED_)
