; CLW file contains information for the MFC ClassWizard

[General Info]
Version=1
LastClass=DlgPaso28
LastTemplate=CDialog
NewFileInclude1=#include "stdafx.h"
NewFileInclude2=#include "Admin_CLAUER.h"

ClassCount=22
Class1=CAdmin_CLAUERApp
Class2=CAdmin_CLAUERDlg

ResourceCount=20
Resource2=IDD_DIALOG_PASO_3
Resource1=IDR_MAINFRAME
Class3=DlgPaso1
Resource3=IDD_DIALOG_PASO_4
Class4=DlgPaso2
Resource4=IDD_DIALOG_CONTRAS
Class5=DlgPaso3
Class6=Hilo
Class7=Thread
Class8=thread
Resource5=IDD_DIALOG_PASO_22
Class9=DlgPaso4
Resource6=IDD_DIALOG_PASO_23
Class10=DlgPaso5_6
Resource7=IDD_DIALOG_WIN98
Resource8=IDD_DIALOG_PASO_5_6
Class11=DlgPaso5_1
Resource9=IDD_DIALOG_PASO_21
Class12=DlgPaso5_2
Resource10=IDD_DIALOG_PASO_50
Class13=DlgPaso5_3
Class14=DlgPaso20
Resource11=IDD_DIALOG_PASO_2
Class15=DlgPaso21
Resource12=IDD_DIALOG_PASO_20
Class16=DlgWin98
Resource13=IDD_DIALOG_SEGURO
Class17=DlgPaso22
Resource14=IDD_ADMIN_CLAUER_DIALOG
Class18=DlgPaso50
Resource15=IDD_DIALOG_PASO_5_2
Class19=DlgPaso23
Resource16=IDD_DIALOG_PASO_1
Class20=DlgContras
Resource17=IDD_DIALOG_PASO_5_1
Class21=DlgPaso28
Resource18=IDD_DIALOG_PASO_28
Class22=DlgSeguro
Resource19=IDD_DIALOG_PASO_5_3
Resource20=IDD_MSG1

[CLS:CAdmin_CLAUERApp]
Type=0
HeaderFile=Admin_CLAUER.h
ImplementationFile=Admin_CLAUER.cpp
Filter=N
LastObject=CAdmin_CLAUERApp

[CLS:CAdmin_CLAUERDlg]
Type=0
HeaderFile=Admin_CLAUERDlg.h
ImplementationFile=Admin_CLAUERDlg.cpp
Filter=D
LastObject=CAdmin_CLAUERDlg
BaseClass=CDialog
VirtualFilter=dWC



[DLG:IDD_ADMIN_CLAUER_DIALOG]
Type=1
Class=CAdmin_CLAUERDlg
ControlCount=5
Control1=IDC_BOTON_SIGUIENTE,button,1342242817
Control2=IDC_BOTON_CANCEL,button,1342242816
Control3=IDB_BITMAP1,static,1342177294
Control4=IDC_FRAME,static,1342177287
Control5=IDC_STATIC,static,1342177296

[DLG:IDD_DIALOG_PASO_1]
Type=1
Class=DlgPaso1
ControlCount=2
Control1=IDC_LBL_explicacion,static,1342308352
Control2=IDC_LBL_PASO_1,static,1342308352

[CLS:DlgPaso1]
Type=0
HeaderFile=DlgPaso1.h
ImplementationFile=DlgPaso1.cpp
BaseClass=CDialog
Filter=D
LastObject=IDC_LBL_explicacion
VirtualFilter=dWC

[DLG:IDD_DIALOG_PASO_2]
Type=1
Class=DlgPaso2
ControlCount=6
Control1=IDC_EDIT_PIN,edit,1350631584
Control2=IDC_EDIT_CONFIRMACION,edit,1350631584
Control3=IDC_LBL_CREAR,static,1342308352
Control4=IDC_CUADRO,button,1342177287
Control5=IDC_PIN,static,1342308352
Control6=IDC_CONFIRMACION,static,1342308352

[CLS:DlgPaso2]
Type=0
HeaderFile=DlgPaso2.h
ImplementationFile=DlgPaso2.cpp
BaseClass=CDialog
Filter=D
LastObject=DlgPaso2
VirtualFilter=dWC

[DLG:IDD_DIALOG_PASO_3]
Type=1
Class=DlgPaso3
ControlCount=5
Control1=IDC_TEXTO,static,1342308352
Control2=IDC_DESCRIPCION,static,1342308352
Control3=IDC_PROGRESO,msctls_progress32,1350565888
Control4=IDC_DESCRIBIENDO,button,1342177287
Control5=IDC_LBL_DESCRIPCION,static,1342308352

[CLS:DlgPaso3]
Type=0
HeaderFile=DlgPaso3.h
ImplementationFile=DlgPaso3.cpp
BaseClass=CDialog
Filter=D
LastObject=DlgPaso3
VirtualFilter=dWC

[CLS:Hilo]
Type=0
HeaderFile=Hilo1.h
ImplementationFile=Hilo1.cpp
BaseClass=CWinThread
Filter=N

[CLS:Thread]
Type=0
HeaderFile=Thread.h
ImplementationFile=Thread.cpp
BaseClass=CWinThread
Filter=N

[CLS:thread]
Type=0
HeaderFile=thread.h
ImplementationFile=thread.cpp
BaseClass=CWinThread
Filter=N

[DLG:IDD_DIALOG_PASO_4]
Type=1
Class=DlgPaso4
ControlCount=10
Control1=IDC_TEXTO,static,1342308352
Control2=IDC_RADIO_IMP,button,1342308361
Control3=IDC_RADIO_IE,button,1342177289
Control4=IDC_RADIO_FIN,button,1342177289
Control5=IDC_CUADRO,button,1342177287
Control6=IDC_RADIO_DISQ,button,1342177289
Control7=IDC_DISQ,static,1342308352
Control8=IDC_UBI,static,1342308352
Control9=IDC_IE,static,1342308352
Control10=IDC_FIN,static,1342308352

[CLS:DlgPaso4]
Type=0
HeaderFile=DlgPaso4.h
ImplementationFile=DlgPaso4.cpp
BaseClass=CDialog
Filter=D
LastObject=DlgPaso4
VirtualFilter=dWC

[DLG:IDD_DIALOG_PASO_5_6]
Type=1
Class=DlgPaso5_6
ControlCount=3
Control1=IDC_TEXTO,static,1342308352
Control2=IDC_TEXTO2,static,1342308352
Control3=IDC_TEXTO3,static,1342308352

[CLS:DlgPaso5_6]
Type=0
HeaderFile=DlgPaso5_6.h
ImplementationFile=DlgPaso5_6.cpp
BaseClass=CDialog
Filter=D
VirtualFilter=dWC
LastObject=DlgPaso5_6

[DLG:IDD_DIALOG_PASO_5_1]
Type=1
Class=DlgPaso5_1
ControlCount=5
Control1=IDC_TEXT2,static,1342308352
Control2=IDC_TEXT1,static,1342308352
Control3=IDC_DESCRIPCION,button,1342177287
Control4=IDC_PASS_DISQ,edit,1350631584
Control5=IDC_PIN,static,1342308352

[CLS:DlgPaso5_1]
Type=0
HeaderFile=DlgPaso5_1.h
ImplementationFile=DlgPaso5_1.cpp
BaseClass=CDialog
Filter=D
LastObject=IDC_DESCRIPCION
VirtualFilter=dWC

[DLG:IDD_DIALOG_PASO_5_2]
Type=1
Class=DlgPaso5_2
ControlCount=4
Control1=IDC_RUTA,edit,1350631552
Control2=IDC_BOTON_RUTA,button,1342242816
Control3=IDC_DESCRIPCION,button,1342177287
Control4=IDC_TEXT,static,1342308352

[CLS:DlgPaso5_2]
Type=0
HeaderFile=DlgPaso5_2.h
ImplementationFile=DlgPaso5_2.cpp
BaseClass=CDialog
Filter=D
VirtualFilter=dWC
LastObject=DlgPaso5_2

[DLG:IDD_DIALOG_PASO_5_3]
Type=1
Class=DlgPaso5_3
ControlCount=4
Control1=IDC_TEXT,static,1342308352
Control2=IDC_DESCRIPCION,button,1342177287
Control3=IDC_CONTR,static,1342308352
Control4=IDC_PASSWORD,edit,1350631584

[CLS:DlgPaso5_3]
Type=0
HeaderFile=DlgPaso5_3.h
ImplementationFile=DlgPaso5_3.cpp
BaseClass=CDialog
Filter=D
LastObject=DlgPaso5_3
VirtualFilter=dWC

[DLG:IDD_DIALOG_PASO_20]
Type=1
Class=DlgPaso20
ControlCount=12
Control1=IDC_DESC,static,1342308352
Control2=IDC_OPCION,button,1342177287
Control3=IDC_RADIO1,button,1342308361
Control4=IDC_RADIO2,button,1342177289
Control5=IDC_RADIO3,button,1342177289
Control6=IDC_RADIO4,button,1342177289
Control7=IDC_RADIO5,button,1342177289
Control8=IDC_MOD,static,1342308352
Control9=IDC_GES,static,1342308352
Control10=IDC_ELIM,static,1342308352
Control11=IDC_ZONA,static,1342308352
Control12=IDC_FN,static,1342308352

[CLS:DlgPaso20]
Type=0
HeaderFile=DlgPaso20.h
ImplementationFile=DlgPaso20.cpp
BaseClass=CDialog
Filter=D
LastObject=DlgPaso20
VirtualFilter=dWC

[DLG:IDD_DIALOG_PASO_21]
Type=1
Class=DlgPaso21
ControlCount=9
Control1=IDC_TEXT,static,1342308352
Control2=IDC_DESC1,button,1342177287
Control3=IDC_DESC2,button,1342177287
Control4=IDC_CONT,static,1342308352
Control5=IDC_PIN,static,1342308352
Control6=IDC_CONF,static,1342308352
Control7=PIN_VIEJO,edit,1350631584
Control8=PIN_NUEVO,edit,1350631584
Control9=PIN_CONFIR,edit,1350631584

[CLS:DlgPaso21]
Type=0
HeaderFile=DlgPaso21.h
ImplementationFile=DlgPaso21.cpp
BaseClass=CDialog
Filter=D
VirtualFilter=dWC
LastObject=DlgPaso21

[DLG:IDD_DIALOG_WIN98]
Type=1
Class=DlgWin98
ControlCount=12
Control1=PIN_VIEJO,edit,1350631584
Control2=PIN_NUEVO,edit,1350631584
Control3=PIN_CONFIR,edit,1350631584
Control4=IDOK,button,1342242817
Control5=IDCANCEL,button,1342242816
Control6=IDC_DESC1,button,1342177287
Control7=IDC_DESC2,button,1342177287
Control8=IDC_TXTPIN,static,1342308352
Control9=IDC_TXTPN,static,1342308352
Control10=IDC_TXTCONF,static,1342308352
Control11=IDC_TEXT1,static,1342308352
Control12=IDC_STATIC,static,1342177296

[CLS:DlgWin98]
Type=0
HeaderFile=DlgWin98.h
ImplementationFile=DlgWin98.cpp
BaseClass=CDialog
Filter=D
VirtualFilter=dWC
LastObject=DlgWin98

[DLG:IDD_DIALOG_PASO_22]
Type=1
Class=DlgPaso22
ControlCount=10
Control1=IDC_RADIO1,button,1342308361
Control2=IDC_RADIO2,button,1342177289
Control3=IDC_RADIO3,button,1342177289
Control4=IDC_RADIO4,button,1342177289
Control5=IDC_CUADRO,button,1342177287
Control6=IDC_TEXTO,static,1342308352
Control7=IDC_IMP,static,1342308352
Control8=IDC_EXP,static,1342308352
Control9=IDC_ELIM,static,1342308352
Control10=IDC_DISQ,static,1342308352

[CLS:DlgPaso22]
Type=0
HeaderFile=DlgPaso22.h
ImplementationFile=DlgPaso22.cpp
BaseClass=CDialog
Filter=D
LastObject=IDC_RADIO4
VirtualFilter=dWC

[DLG:IDD_DIALOG_PASO_50]
Type=1
Class=DlgPaso50
ControlCount=2
Control1=IDC_TEXTO,static,1342308352
Control2=IDC_TEXTO2,static,1342308352

[CLS:DlgPaso50]
Type=0
HeaderFile=DlgPaso50.h
ImplementationFile=DlgPaso50.cpp
BaseClass=CDialog
Filter=D
LastObject=DlgPaso50
VirtualFilter=dWC

[DLG:IDD_DIALOG_PASO_23]
Type=1
Class=DlgPaso23
ControlCount=8
Control1=IDC_TEXTO,static,1342308352
Control2=IDC_CUADRO,button,1342177287
Control3=IDC_RADIO1,button,1342308361
Control4=IDC_RADIO2,button,1342177289
Control5=IDC_RADIO3,button,1342177289
Control6=IDC_DISQ,static,1342308352
Control7=IDC_UB,static,1342308352
Control8=IDC_IE,static,1342308352

[CLS:DlgPaso23]
Type=0
HeaderFile=DlgPaso23.h
ImplementationFile=DlgPaso23.cpp
BaseClass=CDialog
Filter=D
LastObject=DlgPaso23
VirtualFilter=dWC

[DLG:IDD_DIALOG_CONTRAS]
Type=1
Class=DlgContras
ControlCount=7
Control1=IDC_PIN,edit,1350631584
Control2=IDOK,button,1342242817
Control3=IDCANCEL,button,1342242816
Control4=IDC_TEXTO,static,1342308352
Control5=IDC_CUADRO,button,1342177287
Control6=IDC_STATIC,static,1342177283
Control7=IDC_CONTR,static,1342308352

[CLS:DlgContras]
Type=0
HeaderFile=DlgContras.h
ImplementationFile=DlgContras.cpp
BaseClass=CDialog
Filter=D
LastObject=DlgContras
VirtualFilter=dWC

[DLG:IDD_DIALOG_PASO_28]
Type=1
Class=DlgPaso28
ControlCount=3
Control1=IDC_TEXT,static,1342308352
Control2=IDC_TEXTO2,static,1342308352
Control3=IDC_TEXTO3,static,1342308352

[CLS:DlgPaso28]
Type=0
HeaderFile=DlgPaso28.h
ImplementationFile=DlgPaso28.cpp
BaseClass=CDialog
Filter=D
LastObject=DlgPaso28
VirtualFilter=dWC

[DLG:IDD_DIALOG_SEGURO]
Type=1
Class=DlgSeguro
ControlCount=4
Control1=IDCANCEL,button,1342242816
Control2=IDOK,button,1342242817
Control3=IDC_TEXTO,static,1342308352
Control4=IDC_PREG,static,1342308352

[CLS:DlgSeguro]
Type=0
HeaderFile=DlgSeguro.h
ImplementationFile=DlgSeguro.cpp
BaseClass=CDialog
Filter=D
LastObject=DlgSeguro
VirtualFilter=dWC

[DLG:IDD_MSG1]
Type=1
Class=?
ControlCount=10
Control1=IDC_STATIC,static,1342177283
Control2=1001,static,1342308352
Control3=IDC_PASO1,static,1342308352
Control4=IDC_PASO2,static,1342308352
Control5=IDC_URL,static,1342308352
Control6=IDC_STATIC,static,1342308352
Control7=IDC_STATIC,static,1342308352
Control8=IDC_STATIC,static,1342308352
Control9=IDOK,button,1342242816
Control10=IDC_STATIC,static,1342177296

