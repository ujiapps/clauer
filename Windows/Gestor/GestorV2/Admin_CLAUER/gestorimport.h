#ifndef __CLAUER_LIBIMPORT_H__
#define __CLAUER_LIBIMPORT_H__

#include <windows.h>
#include <wincrypt.h>
#include "err.h"



BOOL CopyMoveCertToClauer ( char *szDevice,
			    char *szPass,
			    unsigned char sha1Hash[20],
			    BOOL bMove );

BOOL CopyMoveCertFromClauer ( char *szDevice, 
			      char *szPass, 
			      unsigned char sha1Hash[20], 
			      char *szCSP, 
			      DWORD dwFlags, 
			      BOOL bMove );

int CLIMPORT_CAPI_MY_Cert ( char *szDevice,
			    char *szPwd,
			    PCCERT_CONTEXT *certCtx,
			    BOOL bDelete );



#endif
