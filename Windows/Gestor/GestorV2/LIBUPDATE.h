#ifndef __LIBUPDATE_H__
#define __LIBUPDATE_H__

#include <windows.h>
#include <stdio.h>
#include <stdlib.h>

#ifdef __cplusplus
extern "C" {
#endif

/* Datos a cambiar en cada programa
 */

#define PRG_VERSION			"2005062001"
#define PRG_REG_VALOR		"UP_USBPKIGESTOR"
#define PRG_NAME			"UJI - USBPKI Gestor"
#define PRG_NAME_WEB		"usbpki-gestor"
#define PRG_NAME_FILE		"setup-gestor.exe"

#define PRG_URL_EXPIRE		"http://expire.nisu.org/"
#define PRG_REG_KEY			"SOFTWARE\\Universitat Jaume I\\Projecte Clauer"

/* TRUE - Acabar el programa
 * FALSE - Sigue el programa
 */

BOOL LIBUPDATE_TerminarPrograma (HWND hPadre, HINSTANCE hDll);


#ifdef __cplusplus
}
#endif


/* C�digos devueltos por PRG_Caducado
 */

#define PRG_OK			0
#define PRG_CADUCADO	1
#define PRG_ERROR		2
#define PRG_AVISO		3





#endif
