//---------------------------------------------------------------------------
#include "usb_lowlevel.h"

#include <string.h>


// 02-04-2007 
// Obtiene los BytesPorSector.
int lowlevel_obtenerBytesSector(int dispositivo)
{

	HANDLE hDevice;               // handle to the drive to be examined 
	BOOL bResult;                 // results flag
	DWORD junk;                   // discard results
	TCHAR volumePath[MAX_PATH];
	DISK_GEOMETRY pdg;
	int res;

	_stprintf( volumePath, _T("\\\\.\\PHYSICALDRIVE%d"), dispositivo );
    hDevice = CreateFile(volumePath,  // drive 
                    0,                // no access to the drive
                    FILE_SHARE_READ | // share mode
                    FILE_SHARE_WRITE, 
                    NULL,             // default security attributes
                    OPEN_EXISTING,    // disposition
                    0,                // file attributes
                    NULL);            // do not copy file attributes

    if (hDevice == INVALID_HANDLE_VALUE) // cannot open the drive
    {
       return (FALSE);
    }

    bResult = DeviceIoControl(hDevice,  // device to be queried
						 IOCTL_DISK_GET_DRIVE_GEOMETRY,  // operation to perform
                         NULL, 0, // no input buffer
                         &pdg, sizeof(pdg),     // output buffer
                         &junk,                 // # bytes returned
                         (LPOVERLAPPED) NULL);  // synchronous I/O

	CloseHandle(hDevice);

	if ( !bResult )
		res= -1;
	else 
		res= pdg.BytesPerSector;

  return res;

}


/* Esta funci�n comprueba si es posible leer una unidad l�gica */

int lowlevel_is_readable ( IN char* unidad, IN int dispositivo ) {

	int se_puede_leer = 0;
	char cadena_unidad[20];
	HANDLE_DISPOSITIVO handle;
	int bytesSector;
	BYTE * sector;

	bytesSector= lowlevel_obtenerBytesSector(dispositivo);

	strcpy( cadena_unidad, "\\\\.\\" );
	strcat( cadena_unidad, unidad );

	handle = CreateFile(cadena_unidad,			     // leemos del dispositivo ndevice
						GENERIC_READ | GENERIC_WRITE,        // lo abrimos para leer y escribir
						FILE_SHARE_READ | FILE_SHARE_WRITE,  // compartido para leer y escribir
						NULL,								 // el handle no se puede heredar
						OPEN_EXISTING,						 // se supone que existe
						FILE_ATTRIBUTE_NORMAL | FILE_FLAG_WRITE_THROUGH | FILE_FLAG_NO_BUFFERING, // fichero normal
						NULL);								 // sin template file

	if ( handle != INVALID_HANDLE_VALUE ) {
		/* Probamos a leer un sector de la unidad */
		 DWORD dwBytesRead;
		 sector= (BYTE *) malloc(bytesSector);
		 if ( ReadFile(handle, sector, bytesSector, &dwBytesRead, NULL) )
			se_puede_leer = 1;
		 }
		 free(sector);

	CloseHandle( handle );

	return se_puede_leer;

}

//---------------------------------------------------------------------------
/*! \brief Enumera los dispositivos

             Obtiene los dispositivos de almacenamiento usb que se encuentran
             en el sistema.
             Si la funci�n se llama con los dos primeros argumentos a NULL,
             simplemente devuelve el n�mero total de dispositivos instalados
             en el sistema (tercer argumento).

			 NOTA: Bajo Win95/98/Me, no se devuelven las unidades l�gicas

   \param unidades
             Es un vector de las unidades l�gicas correspondientes a los dispositivos

   \param dispositivos
             Es un vector de n�meros enteros, correspondientes a los identificadores
             de cada dispositivo

   \param numDispositivos
             N�mero total de dispositivos en el sistema

   \retval ERR_LOWLEVEL_NO
             La funci�n se ejecut� correctamente

   \retval ERR_LOWLEVEL_SI
			 Ocurri� un error al enumerar los dispositivos

   \retval ERR_LOWLEVEL_SIST_OPERATIVO_INCORRECTO
            El sistema operativo utilizado no est� soportado por esta librer�a

   \todo
             Puede darse el caso de que un dispositivo contenga varias unidades
             l�gicas. En este caso, el n�mero de dispositivos devuelto no ser�a
             real, puesto que la funci�n devuelve un dispositivo por unidad
             l�gica existente.

             Es posible evitar este problema haciendo la comprobaci�n desde fuera,
             comprobando si hay dispositivos repetidos en la lista.

*/

int lowlevel_enumerar_dispositivos(OUT char **unidades, OUT int *dispositivos, OUT int *numDispositivos)
{
        int winver;
		LPTSTR driveLetters, deviceName;
		char drive[3];
		DWORD tam, i,j;
		PARTICION particiones[4];
		HANDLE_DISPOSITIVO handle;
		unsigned int n_disp = 1;
		int num_dispositivos=0;
		int result;

		HANDLE hDev=NULL;
        char auxDev[8];
        STORAGE_DEVICE_NUMBER sdn;
		DWORD dwBytesReturned;
		 

        /* Obtenemos la versi�n del Windows */

        //winver = lowlevel_get_winversion();
		/* Con WINXP va bien en WIN200, WINXP y VISTA!!!*/
		winver = WINXP;
        switch (winver) {

			case WIN95:
						if ( !SOPORTE_WIN98 ) return ERR_LOWLEVEL_SIST_OPERATIVO_INCORRECTO;

						/* Esta funci�n no devuelve las unidades l�gicas bajo WIN98 */
						unidades = NULL;

						/* Proceso de b�squeda de la partici�n 0x69 */

						for ( n_disp = 1; n_disp < 99 ; n_disp++ ) {

							result = lowlevel_abrir_dispositivo( n_disp, &handle );
							if ( result != ERR_LOWLEVEL_NO ) return ERR_LOWLEVEL_SI;
						
							result = lowlevel_leer_particiones( handle, particiones, -1 );
							if ( result == ERR_LOWLEVEL_NO ) {

								/* Comprobaci�n de partici�n cuarta */

								if ( particiones[3].id == 0x69 ) {

									if ( dispositivos ) dispositivos[num_dispositivos] = n_disp;
									num_dispositivos++;
								}

								result = lowlevel_cerrar_dispositivo( handle );
								if ( result != ERR_LOWLEVEL_NO ) return ERR_LOWLEVEL_SI;
							}
						}
						result = lowlevel_cerrar_dispositivo( handle );
						(*numDispositivos) = num_dispositivos;

				break;

			case WINXP:

				driveLetters = (LPTSTR) malloc (sizeof(TCHAR)*1000);
				deviceName = (LPTSTR) malloc (sizeof(TCHAR)*1000);

				*numDispositivos = 0;

				tam = GetLogicalDriveStrings(999,driveLetters);

				i=0;

				while ( i < (tam-1) ) {
					if ( GetDriveType(driveLetters+i) == DRIVE_REMOVABLE ) {
						strncpy(drive,driveLetters+i,2);
						drive[2] = 0;

						QueryDosDevice(drive, deviceName,999);

						/*
						 * Parseo el deviceName para comprobar que
						 * es un disco duro. Busco la cadena Harddisk
						 */

						j = 0;
						while ( j < strlen(deviceName) ) {
							while ( (deviceName[j] != 'H') && (j< strlen(deviceName)) )
								++j;

							if ( deviceName[j] == 'H' )
								if ( strncmp(deviceName+j, "Harddisk",8) == 0 ) {
									char num[3];
									num[0] = deviceName[j+8];
									if ( isdigit(deviceName[j+9]) ) {
										num[1] = deviceName[j+9];
										num[2] = 0;
									} 
									else
										num[1] = 0;
						
									n_disp= atoi(num);

									if ( lowlevel_is_readable(drive, n_disp) ) {		/* Si el dispositivo se puede leer lo a�adimos */
										if ( unidades ) {
											strcpy(unidades[*numDispositivos], drive);
										}

										if ( dispositivos) {
											 strncpy(auxDev, "\\\\.\\",4);
											 strncpy(auxDev+4,drive,2);
											 auxDev[6]='\0';
	    
										 
											 hDev = CreateFile(auxDev, 0, FILE_SHARE_READ|FILE_SHARE_WRITE, NULL, 
																OPEN_EXISTING, 0, NULL);

											if ( hDev == INVALID_HANDLE_VALUE ) {
												if ( GetLastError() == ERROR_ACCESS_DENIED ) {
													return ERR_LOWLEVEL_SI;
												} 
												else {
													return ERR_LOWLEVEL_SI;
												}
											}
    
								  	        if ( DeviceIoControl(hDev,
												IOCTL_STORAGE_GET_DEVICE_NUMBER,
												NULL,
												0,
												&sdn,
												sizeof(sdn),
												&dwBytesReturned,
												NULL) == 0 )
											{
												return ERR_LOWLEVEL_SI;
											}
	    
											CloseHandle(hDev);
											dispositivos[*numDispositivos] = sdn.DeviceNumber;
										}
										++(*numDispositivos);
									}

									break;
								}
						}

		        }

		        while ( (driveLetters[i] != '\0') && (i < (tam-1)))
			        ++i;
		        ++i;

				}

				free(driveLetters);
				free(deviceName);

                break;

                default: return ERR_LOWLEVEL_SIST_OPERATIVO_INCORRECTO;
      }

      return ERR_LOWLEVEL_NO;

}

//---------------------------------------------------------------------------
/*! \brief Comprueba los permisos del usuario

    Esta funci�n intenta acceder al disco duro como lectura, para comprobar
	si se tienen los suficientes permisos para ello. Devuelve en el par�metro
	soy si el usuario es administrador (1) o no (0).

    \param ndevice
            N�mero que identifica al dispositivo f�sico que debe abrirse.
            El 0 indica el primer dispositivo f�sico del sistema, el 1 el segundo
            dispositivo f�sico y as� sucesivamente.

    \param soy
			Par�metro en el que se indica si el usuario que llama a esta funci�n
			es administrador (1) o no (0).

    \retval ERR_LOWLEVEL_NO
            La funci�n se ejecut� correctamente.

    \retval ERR_LOWLEVEL_DISPOSITIVO_INCORRECTO
            El n�mero de dispositivo indicado no puede abrirse correctamente.

    \retval ERR_LOWLEVEL_SIST_OPERATIVO_INCORRECTO
            El sistema operativo utilizado no est� soportado por esta librer�a.
*/

int lowlevel_soy_admin( IN int ndevice, OUT int* soy ) {

	int winver;
    char drive[50];
    char ndevice_str[5];
    HANDLE hDevice;

	/* Obtenemos la versi�n del Windows */

	winver = lowlevel_get_winversion();

	switch (winver) {

			case WIN95:
						if ( !SOPORTE_WIN98 ) return ERR_LOWLEVEL_SIST_OPERATIVO_INCORRECTO;

			case WINXP: 
						(*soy) = 1;

                        strcpy( drive, "\\\\.\\PHYSICALDRIVE" );
                        if (( ndevice < 0 ) || ( ndevice >= 1000 )) return ERR_LOWLEVEL_DISPOSITIVO_INCORRECTO;

                        _itoa(ndevice, ndevice_str, 10);
                        strcat( drive, ndevice_str );

                        hDevice = CreateFile(drive,					 // leemos del dispositivo ndevice
                                GENERIC_READ,						 // lo abrimos para leer y escribir
                                FILE_SHARE_READ,					 // compartido para leer y escribir
                                NULL,								 // el handle no se puede heredar
                                OPEN_EXISTING,						 // se supone que existe
                                FILE_ATTRIBUTE_NORMAL | FILE_FLAG_WRITE_THROUGH | FILE_FLAG_NO_BUFFERING, // fichero normal
                                NULL);								 // sin template file

                        if ( hDevice == INVALID_HANDLE_VALUE ) {

							if ( GetLastError() == ERROR_ACCESS_DENIED ) {

								(*soy) = 0;
								return ERR_LOWLEVEL_NO;

							}
							else return ERR_LOWLEVEL_DISPOSITIVO_INCORRECTO;
						}
						else {
								if (CloseHandle(hDevice) == 0 ) return ERR_LOWLEVEL_DISPOSITIVO_INCORRECTO;
                                break;
						}

                        break;

			default: return ERR_LOWLEVEL_SIST_OPERATIVO_INCORRECTO;
	}

	return ERR_LOWLEVEL_NO;
}


int lowlevel_abrir_dispositivo_ex( IN char * device, OUT HANDLE_DISPOSITIVO* handle ) {                         

	char unidad[4];

	strncpy(unidad, device, 3);
	unidad[3] = 0;

	if ( GetDriveType(unidad) != DRIVE_CDROM ) {
		*handle = CreateFile(device,					 // leemos del dispositivo ndevice
           				GENERIC_READ | GENERIC_WRITE,        // lo abrimos para leer y escribir
						FILE_SHARE_READ | FILE_SHARE_WRITE,  // compartido para leer y escribir
						NULL,								 // el handle no se puede heredar
						OPEN_EXISTING,						 // se supone que existe
						FILE_ATTRIBUTE_NORMAL | FILE_FLAG_WRITE_THROUGH | FILE_FLAG_NO_BUFFERING, // fichero normal
						NULL);								 // sin template file
	} else {
		*handle = CreateFile(device,					 // leemos del dispositivo ndevice
           			GENERIC_READ,        // lo abrimos para leer y escribir
					FILE_SHARE_READ,     // compartido para leer y escribir
					NULL,								 // el handle no se puede heredar
					OPEN_EXISTING,						 // se supone que existe
					FILE_ATTRIBUTE_NORMAL, // fichero normal
					NULL);								 // sin template file
	}

	if ( *handle == INVALID_HANDLE_VALUE ){
		//printf("LOWLEVEL INTENTA ABRIR= %s devolviendo error\n", device);  
		return ERR_LOWLEVEL_DISPOSITIVO_INCORRECTO;
	}
	//printf("LOWLEVEL INTENTA ABRIR= %s devolviendo error NO \n", device);
	return ERR_LOWLEVEL_NO;

}



//---------------------------------------------------------------------------
/*! \brief Abre el dispositivo indicado

    Abre el dispositivo indicado mediante el par�metro ndevice, tanto
    para lectura como para escritura.

    \param ndevice
            N�mero que identifica al dispositivo f�sico que debe abrirse.
            El 0 indica el primer dispositivo f�sico del sistema, el 1 el segundo
            dispositivo f�sico y as� sucesivamente.

    \param handle
            Manejador que se obtiene al abrir el dispositivo, para hacer referencia
            al mismo desde funciones posteriores.

    \retval ERR_LOWLEVEL_NO
            La funci�n se ejecut� correctamente.

    \retval ERR_LOWLEVEL_DISPOSITIVO_INCORRECTO
            El n�mero de dispositivo indicado no puede abrirse correctamente.

    \retval ERR_LOWLEVEL_SIST_OPERATIVO_INCORRECTO
            El sistema operativo utilizado no est� soportado por esta librer�a.
*/

int lowlevel_abrir_dispositivo( IN int ndevice, OUT HANDLE_DISPOSITIVO* handle ) {

      int winver;
      char drive[50];
      char ndevice_str[5];
      HANDLE hDevice;

      /* Obtenemos la versi�n del Windows */

      winver = lowlevel_get_winversion();

      switch (winver) {

			 case WIN95:
						if ( !SOPORTE_WIN98 ) return ERR_LOWLEVEL_SIST_OPERATIVO_INCORRECTO;

						/* Enlazar con la VxD */

						hDevice = CreateFile( VXDNAME, 0, 0, 0, 0, FILE_FLAG_DELETE_ON_CLOSE, 0 );

						if ( hDevice == INVALID_HANDLE_VALUE )
								return ERR_LOWLEVEL_DISPOSITIVO_INCORRECTO;

						/* Devolvemos el handle */
						(*handle) = hDevice;

						break;

             case WINXP:

                        strcpy( drive, "\\\\.\\PHYSICALDRIVE" );
                        if (( ndevice < 0 ) || ( ndevice >= 1000 )) return ERR_LOWLEVEL_DISPOSITIVO_INCORRECTO;

                        _itoa(ndevice, ndevice_str, 10);
                        strcat( drive, ndevice_str );

                        hDevice = CreateFile(drive,					 // leemos del dispositivo ndevice
                                GENERIC_READ | GENERIC_WRITE,        // lo abrimos para leer y escribir
                                FILE_SHARE_READ | FILE_SHARE_WRITE,  // compartido para leer y escribir
                                NULL,								 // el handle no se puede heredar
                                OPEN_EXISTING,						 // se supone que existe
                                FILE_ATTRIBUTE_NORMAL | FILE_FLAG_WRITE_THROUGH | FILE_FLAG_NO_BUFFERING, // fichero normal
                                NULL);								 // sin template file

						if ( hDevice == INVALID_HANDLE_VALUE ){
							//printf("Error abriendo DRIVE= %s\n", drive );
							return ERR_LOWLEVEL_DISPOSITIVO_INCORRECTO;						
						}
                        /* Devolvemos el handle */
                        (*handle) = hDevice;

                        break;

             default: return ERR_LOWLEVEL_SIST_OPERATIVO_INCORRECTO;
      }

      return ERR_LOWLEVEL_NO;
}

//---------------------------------------------------------------------------
/*! \brief Cierra el dispositivo indicado

    Cierra el dispositivo indicado mediante el manejador que se le pasa

    \param handle
            Manejador que se obtiene al abrir el dispositivo. Permite hacer
			referencia al mismo para cerrarlo.

    \retval ERR_LOWLEVEL_NO
            La funci�n se ejecut� correctamente.

    \retval ERR_LOWLEVEL_DISPOSITIVO_INCORRECTO
            El n�mero de dispositivo indicado no puede cerrarse correctamente.

    \retval ERR_LOWLEVEL_SIST_OPERATIVO_INCORRECTO
            El sistema operativo utilizado no est� soportado por esta librer�a.
*/

int lowlevel_cerrar_dispositivo( IN HANDLE_DISPOSITIVO handle ) {

	int winver;

    /* Obtenemos la versi�n del Windows */

    winver = lowlevel_get_winversion();

    switch (winver) {

			 case WIN95:
							if ( !SOPORTE_WIN98 ) return ERR_LOWLEVEL_SIST_OPERATIVO_INCORRECTO;
             case WINXP:
							if (CloseHandle(handle) == 0 ) return ERR_LOWLEVEL_DISPOSITIVO_INCORRECTO;
                            break;

			 default: return ERR_LOWLEVEL_SIST_OPERATIVO_INCORRECTO;
    }

    return ERR_LOWLEVEL_NO;
}

//---------------------------------------------------------------------------
/*! \brief Obtiene informaci�n sobre las particiones

   Mediante la lectura del MBR, esta funci�n obtiene informaci�n acerca de los
   par�metros de cada partici�n existente en el dispositivo. Actualmente, s�lo
   se soportan una partici�n de datos (como partici�n 1).

   \param handle
            Manejador que se obtiene al abrir el dispositivo.

   \param particiones
            Vector que contiene la estructura de informaci�n obtenida para cada
            una de las particiones del dispositivo.

   \retval ERR_LOWLEVEL_NO
             La funci�n se ejecut� correctamente.

   \retval ERR_LOWLEVEL_LECTURA_MBR_INCORRECTA
             No se ha podido realizar la lectura del MBR de forma correcta

   \retval ERR_LOWLEVEL_MBR_NO_VALIDO
             La firma del MBR es incorrecta

   \retval ERR_LOWLEVEL_SIST_OPERATIVO_INCORRECTO
             El sistema operativo utilizado no est� soportado por esta librer�a

   \retval ERR_LOWLEVEL_SI
			 Ha ocurrido un error interno en la funci�n de salto

   \todo     Tener en cuenta m�s de una partici�n de datos para el formateo y nuestra propia partici�n?
*/

int lowlevel_leer_particiones( IN HANDLE_DISPOSITIVO handle, OUT PARTICION particiones[], IN int dispositivo ) {

      BYTE *data;
      BYTE *ptr;
      int result;
      char part1[16];
      char part2[16];
      char part3[16];
      char part4[16];
      unsigned char signat[2];
      long int cyl_up, cyl_down;
      long int temp1, temp2, temp3, temp4;
      int winver, bytesSector;

      /* Obtenemos la versi�n del Windows */
      winver = lowlevel_get_winversion();

      switch (winver) {

		case WIN95:
					if ( !SOPORTE_WIN98 ) return ERR_LOWLEVEL_SIST_OPERATIVO_INCORRECTO;
        case WINXP:

				bytesSector= lowlevel_obtenerBytesSector( dispositivo );
				if ( bytesSector < 0 )
					return ERR_LOWLEVEL_SI;

                /* Leemos el primer sector del disco, tabla de particiones */
                data = (BYTE *) malloc(bytesSector*sizeof(char));

				result = lowlevel_leer_sector( handle, 0, data, dispositivo );
				if ( result != ERR_LOWLEVEL_NO ) return ERR_LOWLEVEL_LECTURA_MBR_INCORRECTA;

                /* Analizamos el sector, fuente: http://www.boot-us.com/gloss03.htm */

                ptr=data;
                ptr+=446;    /* Saltamos el boot_code */
                memcpy(part1, ptr, 16);
                ptr+=16;
                memcpy(part2, ptr, 16);
                ptr+=16;
                memcpy(part3, ptr, 16);
                ptr+=16;
                memcpy(part4, ptr, 16);
                ptr+=16;
                memcpy(signat, ptr, 2);

                if ( memcmp(signat, "\x55\xAA", 2) != 0 ) return ERR_LOWLEVEL_MBR_NO_VALIDO;
                free(data);


                /* Id */

                particiones[0].id = (long int) part1[4];
                particiones[1].id = (long int) part2[4];
                particiones[2].id = (long int) part3[4];
                particiones[3].id = (long int) part4[4];

                /* El n�mero de sector se identifica por los bits 0-5 */

                /* Particion 1 -> particiones[0] */
                /* Particion 4 -> particiones[3] */

                particiones[0].sect_ini = (long int) part1[2] & 0x3F;
                particiones[0].sect_fin = (long int) part1[6] & 0x3F;
                particiones[3].sect_ini = (long int) part4[2] & 0x3F;
                particiones[3].sect_fin = (long int) part4[6] & 0x3F;

                /* El n�mero de cilindro son 10 bits */

                        /* Particion 1 */

                cyl_up = (long int) part1[2] & 0xC0;
                cyl_down = (long int) part1[3] & 0xFF;
                particiones[0].cyl_ini = (cyl_up << 2) + cyl_down;

                cyl_up = (long int) part1[6] & 0xC0;
                cyl_down = (long int) part1[7] & 0xFF;
                particiones[0].cyl_fin = (cyl_up << 2) + cyl_down;

                        /* Particion 4 */

                cyl_up = (long int) part4[2] & 0xC0;
                cyl_down = (long int) part4[3] & 0xFF;
                particiones[3].cyl_ini = (cyl_up << 2) + cyl_down;

                cyl_up = (long int) part4[6] & 0xC0;
                cyl_down = (long int) part4[7] & 0xFF;
                particiones[3].cyl_fin = (cyl_up << 2) + cyl_down;

                /* Cabeza */

                particiones[0].head_ini = (long int) part1[1];
                particiones[0].head_fin = (long int) part1[5];

                particiones[3].head_ini = (long int) part4[1];
                particiones[3].head_fin = (long int) part4[5];

                /* Rel Sectors */

                temp1 = (long int) part1[8] & 0xFF;
                temp2 = (long int) part1[9] & 0xFF;
                temp3 = (long int) part1[10] & 0xFF;
                temp4 = (long int) part1[11] & 0xFF;

                particiones[0].rel_sectors = (long int) (temp4<<24)+(temp3<<16)+(temp2<<8)+temp1;

                temp1 = (long int) part4[8] & 0xFF;
                temp2 = (long int) part4[9] & 0xFF;
                temp3 = (long int) part4[10] & 0xFF;
                temp4 = (long int) part4[11] & 0xFF;

                particiones[3].rel_sectors = (long int) (temp4<<24)+(temp3<<16)+(temp2<<8)+temp1;

                /* N�mero sectores en particion */

                temp1 = (long int) part1[12] & 0xFF;
                temp2 = (long int) part1[13] & 0xFF;
                temp3 = (long int) part1[14] & 0xFF;
                temp4 = (long int) part1[15] & 0xFF;

                particiones[0].num_sectors = (long int) (temp4<<24)+(temp3<<16)+(temp2<<8)+temp1;

                temp1 = (long int) part4[12] & 0xFF;
                temp2 = (long int) part4[13] & 0xFF;
                temp3 = (long int) part4[14] & 0xFF;
                temp4 = (long int) part4[15] & 0xFF;

                particiones[3].num_sectors = (long int) (temp4<<24)+(temp3<<16)+(temp2<<8)+temp1;

                break;

        default: return ERR_LOWLEVEL_SIST_OPERATIVO_INCORRECTO;
      }
      return ERR_LOWLEVEL_NO;
}

//---------------------------------------------------------------------------
/*! \brief Calcula la ubicaci�n de un sector absoluto en formato CHS

   Obtiene los par�metros numero de cabeza, cilindro y sector correspondientes
   a un n�mero de sector absoluto.

   \param num_sector
            N�mero de sector absoluto del que se quieren calcular los par�metros CHS

   \param total_cabezas
            N�mero de cabezas seg�n la geometr�a del dispositivo.

   \param total_cilindros
			N�mero de cilindros seg�n la geometr�a del dispositivo

   \param total_sectores
			N�mero de sectores seg�n la geometr�a del dispositivo

   \param n_cabeza
			N�mero de cabeza calculado en formato CHS

   \param n_pista
			N�mero de pista calculada en formato CHS

   \param n_sector
			N�mero de sector calculado en formato CHS

   \retval ERR_LOWLEVEL_NO
             La funci�n se ejecut� correctamente.

   \retval ERR_LOWLEVEL_SI
			 Ha ocurrido un error interno al llamar a la funci�n de salto
*/

int lowlevel_calcular_ubicacion( IN unsigned long num_sector, 
								 IN long int total_cabezas, IN long int total_cilindros, IN long int total_sectores, 
								 OUT unsigned int *n_cabeza, OUT unsigned int *n_pista, OUT unsigned int *n_sector ) {

	unsigned long temp;

	(*n_cabeza) = num_sector / (total_cilindros * total_sectores);
	temp = num_sector % (total_cilindros * total_sectores);

	(*n_pista) = temp / total_sectores;
	
	(*n_sector) = temp % total_sectores;


	return ERR_LOWLEVEL_NO;
}


//---------------------------------------------------------------------------
/*! \brief Lee un sector del dispositivo

   Accede al dispositivo en la posici�n indicada por el par�metro num_sector
   y obtiene el sector almacenado en esa posici�n.

   \param handle
            Manejador que se obtiene al abrir el dispositivo, para poder
			acceder al mismo.

   \param num_sector
            N�mero de sector a leer del dispositivo

   \param sector
            El sector le�do

   \retval ERR_LOWLEVEL_NO
             La funci�n se ejecut� correctamente.

   \retval ERR_LOWLEVEL_SI
			 Ha ocurrido un error interno al llamar a la funci�n de salto

   \retval ERR_LOWLEVEL_LECTURA_INCORRECTA
			 No se ha podido leer correctamente el sector especificado

   \retval ERR_LOWLEVEL_SIST_OPERATIVO_INCORRECTO
             El sistema operativo utilizado no est� soportado por esta librer�a
*/

int lowlevel_leer_sector( IN HANDLE_DISPOSITIVO handle, IN unsigned long num_sector, OUT BYTE* sector, IN int dispositivo ) {

	int winver;
	int result;
	unsigned long num_leidos;
	UBICACION lugar;
	PARTICION particiones[4];
	long int total_cabezas, total_sectores, total_cilindros;
	unsigned int n_cabeza, n_pista, n_sector;
	char ** unidades;
	int * dispositivos;
	int i;
	int num_dispositivos, bytesSector;


	/* Obtenemos la versi�n del Windows */
	winver = lowlevel_get_winversion();

	switch (winver) {


		case WIN95:

					if ( !SOPORTE_WIN98 ) return ERR_LOWLEVEL_SIST_OPERATIVO_INCORRECTO;

					/* Obtenemos la informaci�n del lugar desde el cual leer */

					/* Tomamos como geometr�a el final de la partici�n criptogr�fica */

			if ( num_sector == 0 ) {

					lugar.cabeza = 0;
					lugar.pista = 0;
					lugar.sector = 1;

					/* Obtenemos la unidad de la enumeraci�n */

					result = lowlevel_enumerar_dispositivos( NULL, NULL, &num_dispositivos );
					if ( result != ERR_LOWLEVEL_NO ) return ERR_LOWLEVEL_SI;

					unidades = (char **) malloc (num_dispositivos*sizeof(char)*5);

					for ( i=0; i<num_dispositivos; i++ ) {
						unidades[i] = (char *) malloc(5*sizeof(char));
					}

					dispositivos = (int *) malloc (num_dispositivos*sizeof(int));

					result = lowlevel_enumerar_dispositivos( unidades, dispositivos, &num_dispositivos );
					if ( result != ERR_LOWLEVEL_NO ) return ERR_LOWLEVEL_SI;

					lugar.unidad = dispositivos[0];

					/* Procedemos a la lectura */

					result = DeviceIoControl( handle, VXD_LEER, &lugar, sizeof(lugar), sector, 512, &num_leidos, NULL );
					if ( ( result == 0 ) || ( num_leidos != 512 ) ) return ERR_LOWLEVEL_LECTURA_INCORRECTA;
			}

			else {

					result = lowlevel_leer_particiones( handle, particiones, -1 );
					if ( result != ERR_LOWLEVEL_NO ) return ERR_LOWLEVEL_LECTURA_MBR_INCORRECTA;

					total_cabezas = particiones[3].head_fin + 1;
					total_sectores = particiones[3].sect_fin;
					total_cilindros = particiones[3].cyl_fin + 1;

					/* Obtenemos la unidad de la enumeraci�n */

					result = lowlevel_enumerar_dispositivos( NULL, NULL, &num_dispositivos );
					if ( result != ERR_LOWLEVEL_NO ) return ERR_LOWLEVEL_SI;

					unidades = (char **) malloc (num_dispositivos*sizeof(char)*5);

					for ( i=0; i<num_dispositivos; i++ ) {
						unidades[i] = (char *) malloc(5*sizeof(char));
					}

					dispositivos = (int *) malloc (num_dispositivos*sizeof(int));

					result = lowlevel_enumerar_dispositivos( unidades, dispositivos, &num_dispositivos );
					if ( result != ERR_LOWLEVEL_NO ) return ERR_LOWLEVEL_SI;

					lugar.unidad = dispositivos[0];

					/* Hacemos el c�lculo del resto de par�metros */

					result = lowlevel_calcular_ubicacion( num_sector, total_cabezas, total_cilindros, total_sectores, &n_cabeza, &n_pista, &n_sector );
					if ( result != ERR_LOWLEVEL_NO ) return ERR_LOWLEVEL_SI;

					lugar.cabeza = n_cabeza;
					lugar.pista = n_pista;
					lugar.sector = n_sector;

					/* Procedemos a la lectura */

					result = DeviceIoControl( handle, VXD_LEER, &lugar, sizeof(lugar), sector, 512, &num_leidos, NULL );
					if ( ( result == 0 ) || ( num_leidos != 512 ) ) return ERR_LOWLEVEL_LECTURA_INCORRECTA;

			}
			break;

        case WINXP:

			        bytesSector= lowlevel_obtenerBytesSector(dispositivo);	
					if ( bytesSector < 0 )
						return ERR_LOWLEVEL_SI;
					
					/* Nos posicionamos en el n�mero de sector a leer */

					result = lowlevel_saltar( handle, num_sector, FALSE, dispositivo );
					if ( result != ERR_LOWLEVEL_NO ) return ERR_LOWLEVEL_SI;

					/* Leemos el sector indicado */

					result = ReadFile( handle, sector, bytesSector, &num_leidos, NULL);
					if ( (result == 0) || (num_leidos != bytesSector) ) return ERR_LOWLEVEL_LECTURA_INCORRECTA;

					break;

		default: return ERR_LOWLEVEL_SIST_OPERATIVO_INCORRECTO;

	}

	return ERR_LOWLEVEL_NO;
}

//---------------------------------------------------------------------------
/*! \brief Escribe un sector en el dispositivo

   Accede al dispositivo en la posici�n indicada por el par�metro num_sector
   y almacena el sector proporcionado en esa posici�n.

   \param handle
            Manejador que se obtiene al abrir el dispositivo, para poder
			acceder al mismo.

   \param num_sector
            N�mero de sector a escribir en el dispositivo

   \param sector
            El sector a escribir en el dispositivo

   \retval ERR_LOWLEVEL_NO
             La funci�n se ejecut� correctamente

   \retval ERR_LOWLEVEL_SI
			 Ha ocurrido un error interno al llamar a la funci�n de salto

   \retval ERR_LOWLEVEL_ESCRITURA_INCORRECTA
			 No se ha podido escribir correctamente el sector especificado

   \retval ERR_LOWLEVEL_SIST_OPERATIVO_INCORRECTO
             El sistema operativo utilizado no est� soportado por esta librer�a
*/

int lowlevel_escribir_sector( IN HANDLE_DISPOSITIVO handle, IN unsigned long num_sector, IN BYTE *sector, IN int dispositivo) {

	int winver;
	int result;
	unsigned long num_escritos;
	UBICACION lugar;
	PARTICION particiones[4];
	long int total_cabezas, total_sectores, total_cilindros;
	int num_dispositivos;
	char ** unidades;
	int * dispositivos;
	int i, bytesSector;
	unsigned int n_cabeza, n_pista, n_sector;

	/* Obtenemos la versi�n del Windows */
	winver = lowlevel_get_winversion();

	switch (winver) {
		
		case WIN95:
					if ( !SOPORTE_WIN98 ) return ERR_LOWLEVEL_SIST_OPERATIVO_INCORRECTO;

					/* Obtenemos la informaci�n del lugar desde el cual leer */

					/* Tomamos como geometr�a el final de la partici�n criptogr�fica */

					result = lowlevel_leer_particiones( handle, particiones, -1 );
					if ( result != ERR_LOWLEVEL_NO ) return ERR_LOWLEVEL_LECTURA_MBR_INCORRECTA;

					total_cabezas = particiones[3].head_fin + 1;
					total_sectores = particiones[3].sect_fin;
					total_cilindros = particiones[3].cyl_fin + 1;

					/* Obtenemos la unidad de la enumeraci�n */

					result = lowlevel_enumerar_dispositivos( NULL, NULL, &num_dispositivos );
					if ( result != ERR_LOWLEVEL_NO ) return ERR_LOWLEVEL_SI;

					unidades = (char **) malloc (num_dispositivos*sizeof(char)*5);

					for ( i=0; i<num_dispositivos; i++ ) {
						unidades[i] = (char *) malloc(5*sizeof(char));
					}

					dispositivos = (int *) malloc (num_dispositivos*sizeof(int));

					result = lowlevel_enumerar_dispositivos( unidades, dispositivos, &num_dispositivos );
					if ( result != ERR_LOWLEVEL_NO ) return ERR_LOWLEVEL_SI;

					lugar.unidad = dispositivos[0];

					/* Hacemos el c�lculo del resto de par�metros */

					result = lowlevel_calcular_ubicacion( num_sector, total_cabezas, total_cilindros, total_sectores, &n_cabeza, &n_pista, &n_sector );
					if ( result != ERR_LOWLEVEL_NO ) return ERR_LOWLEVEL_SI;

					lugar.cabeza = n_cabeza;
					lugar.pista = n_pista;
					lugar.sector = n_sector;

					/* Procedemos a la lectura */

					/* Revisar los par�metros de entrada/salida? Lugar debe incluir el sector a escribir? */

					result = DeviceIoControl( handle, VXD_ESCRIBIR, &lugar, sizeof(lugar), sector, 512, &num_escritos, NULL );
					if ( ( result == 0 ) || ( num_escritos != 512 ) ) return ERR_LOWLEVEL_ESCRITURA_INCORRECTA;

					break;

        case WINXP:

					bytesSector= lowlevel_obtenerBytesSector(dispositivo);
					if ( bytesSector < 0 )
						return ERR_LOWLEVEL_SI;

					/* Nos posicionamos en el n�mero de sector a leer */

					result = lowlevel_saltar( handle, num_sector, FALSE, dispositivo );
					if ( result != ERR_LOWLEVEL_NO ) return ERR_LOWLEVEL_SI;

					/* Leemos el sector indicado */

					result = WriteFile( handle, sector, bytesSector, &num_escritos, NULL);
					if ( (result == 0) || (num_escritos != bytesSector) ) return ERR_LOWLEVEL_ESCRITURA_INCORRECTA;

					break;

		default: return ERR_LOWLEVEL_SIST_OPERATIVO_INCORRECTO;

	}

	return ERR_LOWLEVEL_NO;

}

//---------------------------------------------------------------------------
/*! \brief Bloquea una unidad l�gica

   Bloquea la unidad l�gica especificada como par�metro, para poder realizar
   el formateo con seguridad.

   \param unidad
            Car�cter que indica el nombre de unidad l�gica a bloquear (Ej: A,B,C,...)
   
   \param handle
            Manejador que se obtiene al abrir el dispositivo, para poder
			acceder al mismo.

   \retval ERR_LOWLEVEL_NO
            La funci�n se ejecut� correctamente.

   \retval ERR_LOWLEVEL_UNIDAD_LOGICA_INCORRECTA
			No se ha podido abrir la unidad l�gica especificada

   \retval ERR_LOWLEVEL_BLOQUEANDO_UNIDAD_LOGICA
			No se ha podido bloquear la unidad l�gica especificada

   \retval ERR_LOWLEVEL_SIST_OPERATIVO_INCORRECTO
            El sistema operativo utilizado no est� soportado por esta librer�a

   \todo Esta funci�n s�lo se soporta en Windows XP, Windows 2000 Professional, o Windows NT Workstation 3.5 y posteriores
*/

int lowlevel_bloquear_unidadlogica( IN char unidad, OUT HANDLE_DISPOSITIVO* handle ) {


	int winver;
	unsigned long bytes;
	char cadena[15];

	/* Obtenemos la versi�n del Windows */
	winver = lowlevel_get_winversion();

	switch (winver) {

        case WINXP:

					sprintf(cadena,"\\\\.\\%c:",unidad);

					(*handle) = CreateFile(cadena,
                                                GENERIC_READ | GENERIC_WRITE,        // lo abrimos para leer y escribir
                                                FILE_SHARE_READ | FILE_SHARE_WRITE,  // compartido para leer y escribir
                                                NULL,				     // el handle no se puede heredar
                                                OPEN_EXISTING,			     // se supone que existe
                                                FILE_ATTRIBUTE_NORMAL,		     // fichero normal
                                                NULL);				     // sin template file

					if ((*handle) == INVALID_HANDLE_VALUE) return ERR_LOWLEVEL_UNIDAD_LOGICA_INCORRECTA;

					if (! DeviceIoControl( (*handle), FSCTL_LOCK_VOLUME, NULL, 0, NULL, 0, &bytes, NULL ) )
						return ERR_LOWLEVEL_BLOQUEANDO_UNIDAD_LOGICA;

					break;

		default: return ERR_LOWLEVEL_SIST_OPERATIVO_INCORRECTO;
	}

	return ERR_LOWLEVEL_NO;
}

//---------------------------------------------------------------------------
/*! \brief Desbloquea una unidad l�gica

   Desbloquea la unidad l�gica especificada, como finalizaci�n de un proceso
   de acceso exclusivo a la misma.

   \param handle
            Manejador que se obtiene al abrir la unidad, para poder
			acceder a la misma.

   \retval ERR_LOWLEVEL_NO
             La funci�n se ejecut� correctamente.

   \retval ERR_LOWLEVEL_DESBLOQUEANDO_UNIDAD_LOGICA
			 No se ha podido desbloquear la unidad l�gica

   \retval ERR_LOWLEVEL_UNIDAD_LOGICA_INCORRECTA
			 No se ha podido cerrar el manejador de la unidad

   \retval ERR_LOWLEVEL_SIST_OPERATIVO_INCORRECTO
             El sistema operativo utilizado no est� soportado por esta librer�a

   \todo Esta funci�n s�lo se soporta en Windows XP, Windows 2000 Professional, o Windows NT Workstation 3.5 y posteriores

*/

int lowlevel_desbloquear_unidadlogica( IN HANDLE_DISPOSITIVO handle ) {

	int winver;
	unsigned long bytes;

	/* Obtenemos la versi�n del Windows */
	winver = lowlevel_get_winversion();

	switch (winver) {

        case WINXP:

					if (! DeviceIoControl( handle, FSCTL_UNLOCK_VOLUME, NULL, 0, NULL, 0, &bytes, NULL ) )
						return ERR_LOWLEVEL_DESBLOQUEANDO_UNIDAD_LOGICA;
					
					if (! DeviceIoControl( handle, FSCTL_DISMOUNT_VOLUME, NULL, 0, NULL, 0, &bytes, NULL ) )
						return ERR_LOWLEVEL_DESBLOQUEANDO_UNIDAD_LOGICA;

					if (! CloseHandle(handle)) return ERR_LOWLEVEL_UNIDAD_LOGICA_INCORRECTA;
					break;

		default: return ERR_LOWLEVEL_SIST_OPERATIVO_INCORRECTO;
	}

	return ERR_LOWLEVEL_NO;
}

//---------------------------------------------------------------------------
/*! \brief Comprueba si el dispositivo es un USB Cert

   Accede al dispositivo y lee su tabla de particiones. Si la cuarta partici�n
   primaria del mismo es de tipo 69h, se considera que el dispositivo es un
   USB Cert

   \param handle
            Manejador que se obtiene al abrir la unidad, para poder acceder a la misma.

   \param es
			Indica si el dispositivo es (1) o no (0) un USB Cert

   \retval ERR_LOWLEVEL_NO
            La funci�n se ha ejecutado correctamente

   \retval ERR_LOWLEVEL_SI
            No se ha podido leer correctamente la tabla de particiones
*/

int lowlevel_es_usbcert( IN HANDLE_DISPOSITIVO handle, OUT int *es )
{
        PARTICION particiones[4];

        /*
         * Es USB Cert si la cuarta partici�n primaria es
         * de tipo 69h
         */

		/* Si ocurre un error en la lectura de las particiones, el dispositivo no est� presente, no es un error cr�tico */

        if ( lowlevel_leer_particiones(handle, particiones, -1) != ERR_LOWLEVEL_NO ) {
				*es = 0;
                return ERR_LOWLEVEL_NO;
		}

        if ( particiones[3].id != 0x69 )
                *es = 0;
        else
                *es = 1;

	return ERR_LOWLEVEL_NO;
}

//---------------------------------------------------------------------------
/*! \brief Obtiene el salt del dispositivo
			
			Esta funci�n lee el salt del dispositivo, con el objetivo de
			realizar operaciones criptogr�ficas con el mismo. Actualmente,
			el salt coincide con el identificador del dispositivo.

   \param handle
			Manejador del dispositivo que se obtiene al abrir el mismo

   \param particion
			La partici�n criptogr�fica del dispositivo

   \param salt
			El salt del dispositivo

   \retval ERR_LOWLEVEL_NO
             La funci�n se ejecut� correctamente.

   \retval ERR_LOWLEVEL_SI
			 Ha ocurrido un error obteniendo el salt

*/

int lowlevel_leer_salt( IN HANDLE_DISPOSITIVO handle, IN PARTICION particion, OUT BYTE salt [20], IN int dispositivo ) {

	BYTE bloque[TAM_BLOQUE];
	int bytesSector;
		
	bytesSector= lowlevel_obtenerBytesSector(dispositivo);
	if ( bytesSector < 0 )
		return ERR_LOWLEVEL_SI;

	if ( lowlevel_leer_buffer(handle, bloque, particion.rel_sectors, TAM_BLOQUE/bytesSector, dispositivo) != ERR_LOWLEVEL_NO ) 
		return ERR_LOWLEVEL_SI;

	memcpy( salt, bloque+40, 20 );

	return ERR_LOWLEVEL_NO;

}

//---------------------------------------------------------------------------
/*! \brief Comprueba si el dispositivo tiene una tabla de particiones
		   en su primer sector
	
   Esta funci�n es capaz de determinar si el dispositivo contiene una tabla
   de particiones o no. Se considera que un dispositivo contiene tabla de
   particiones si se cumplen todas las condiciones que se exponen a continuaci�n:

   - Que el primer byte de las entradas de particiones contengan 00h o 80h (partici�n activa o no)
   - Que el byte de checksum del primer sector contenga 55h AAh

   \param handle
            Manejador que se obtiene al abrir la unidad, para poder acceder a la misma.

   \param tiene
			Indica si tiene (1) o no (0) una tabla de particiones v�lida en su primer sector

   \retval ERR_LOWLEVEL_NO
            La funci�n se ha ejecutado correctamente

   \retval ERR_LOWLEVEL_SI
            No se ha podido leer correctamente la tabla de particiones

   \retval ERR_LOWLEVEL_LECTURA_MBR_INCORRECTA
             No se ha podido realizar la lectura del MBR de forma correcta

   \retval ERR_LOWLEVEL_SIST_OPERATIVO_INCORRECTO
             Se est� intentando ejecutar la librer�a en un sistema operativo no soportado

*/

int lowlevel_tiene_mbr( IN HANDLE_DISPOSITIVO handle, OUT int *tiene, IN int dispositivo )
{

      int winver, result;
	  BYTE *data;
      BYTE part1[16];
      BYTE part2[16];
      BYTE part3[16];
      BYTE part4[16];
      unsigned char signat[2];
	  BYTE boot[446];
	  BYTE *ptr;

      /* Obtenemos la versi�n del Windows */
      winver = lowlevel_get_winversion();

      switch (winver) {

		case WIN95:
				if ( !SOPORTE_WIN98 ) return ERR_LOWLEVEL_SIST_OPERATIVO_INCORRECTO;

        case WINXP:

                /* Leemos el primer sector del disco, tabla de particiones */
                data = (BYTE *) malloc(512*sizeof(char));

				result = lowlevel_leer_sector( handle, 0, data, dispositivo );
				if ( result != ERR_LOWLEVEL_NO ) return ERR_LOWLEVEL_LECTURA_MBR_INCORRECTA;

                /* Analizamos el sector, fuente: http://www.boot-us.com/gloss03.htm */

                ptr=data;
				memcpy(boot, ptr, 446);
                ptr+=446;    /* Saltamos el boot_code */
                memcpy(part1, ptr, 16);
                ptr+=16;
                memcpy(part2, ptr, 16);
                ptr+=16;
                memcpy(part3, ptr, 16);
                ptr+=16;
                memcpy(part4, ptr, 16);
                ptr+=16;
                memcpy(signat, ptr, 2);

				/* Comprobaci�n de la signature */

                if ( memcmp(signat, "\x55\xAA", 2) != 0 ) {
					*tiene = 0;
					return ERR_LOWLEVEL_NO;
				}

				/* Comprobaci�n de las particiones activas */

					/* Partici�n 1 */
				if (( memcmp(part1,"\x00",1) != 0 ) && ( memcmp(part1,"\x80",1) != 0)) {
					*tiene = 0;
					return ERR_LOWLEVEL_NO;
				}
				

					/* Partici�n 2 */
				if (( memcmp(part2,"\x00",1) != 0 ) && ( memcmp(part2,"\x80",1) != 0)) {
					*tiene = 0;
					return ERR_LOWLEVEL_NO;
				}

					/* Partici�n 3 */
				if (( memcmp(part3,"\x00",1) != 0 ) && ( memcmp(part3,"\x80",1) != 0)) {
					*tiene = 0;
					return ERR_LOWLEVEL_NO;
				}

					/* Partici�n 4 */
				if (( memcmp(part4,"\x00",1) != 0 ) && ( memcmp(part4,"\x80",1) != 0)) {
					*tiene = 0;
					return ERR_LOWLEVEL_NO;
				}
				

				/* Si hemos llegado hasta aqu�, va a ser que tiene MBR */
				*tiene = 1;

                free(data);


				break;

		default: return ERR_LOWLEVEL_SIST_OPERATIVO_INCORRECTO;
	}

	return ERR_LOWLEVEL_NO;
}


//---------------------------------------------------------------------------
/*! \brief Obtiene la versi�n de Windows instalada en el sistema

   Devuelve la versi�n de Windows que est� ejecutando el sistema

   \retval WINNT351
             El sistema es Windows NT 3.51

   \retval WIN95
             El sistema es Windows 95/98/Me/NT 4.0

   \retval WINXP
             El sistema es Windows XP/2000/Server 2003 family

*/

int lowlevel_get_winversion( void ) {

      OSVERSIONINFO osv;

      osv.dwOSVersionInfoSize = sizeof(OSVERSIONINFO);
      GetVersionEx(&osv);

      return osv.dwMajorVersion;
}

//---------------------------------------------------------------------------
/*!
   \brief Salta a un sector concreto del dispositivo
 
    Realiza el salto o desplazamiento (bien relativo o bien absoluto) del 
	puntero del dispositivo el n�mero de sectores indicados.
 
	\param handle
			 Manejador para acceder al dispositivo
 
	\param nsectores
			 N�mero de sectores a saltar

	\param es_relativo
			 Indica si el salto debe realizarse desde la posici�n actual (salto
			 relativo) o desde el inicio del dispositivo (salto absoluto)

	\retval ERR_LOWLEVEL_NO
             La funci�n se ejecut� correctamente

	\retval ERR_LOWLEVEL_FIN_PARTICION
			 La posici�n destino del salto despu�s del desplazamiento es incorrecta,
			 posiblemente exceda del tama�o m�ximo de la partici�n

	\retval	ERR_LOWLEVEL_NSECTORES_INCORRECTO
			 El n�mero de sectores especificados para realizar el salto es incorrecto.

	\retval ERR_LOWLEVEL_SIST_OPERATIVO_INCORRECTO
             El sistema operativo utilizado no est� soportado por esta librer�a

	\todo Esta funci�n s�lo tiene sentido en Win XP, Win 2000 o Win NT
			
 */

int lowlevel_saltar ( HANDLE_DISPOSITIVO handle, unsigned long nsectores, BOOL es_relativo, int dispositivo ) {

    int result, winver, bytesSector;
    DWORD modo;
    DWORDLONG nbytes;
    LONG despl_low, despl_high, nbytes_corto;

	bytesSector= lowlevel_obtenerBytesSector(dispositivo);
	if ( bytesSector < 0 )
		return ERR_LOWLEVEL_SI;
	
    
	winver = lowlevel_get_winversion();

    switch (winver) {

      case WINXP:

			if ( nsectores >= 0 ) {

				if ( es_relativo ) modo = FILE_CURRENT;
				else modo = FILE_BEGIN;

				nbytes = nsectores * bytesSector;

				/* Necesitamos la parte alta del desplazamiento? */
				//if ( nbytes >= 4294967296 ) {
				if ( nbytes >= 2147483648LU ) {
						despl_low = (LONG) nbytes & 0xFFFFFFFF;
						nbytes = (nbytes & 0xFFFFFFFF00000000LLU) >> 32;
						despl_high = (LONG) nbytes;

						result = SetFilePointer( handle, despl_low, &despl_high, modo );

						if ( (result == INVALID_SET_FILE_POINTER) && (GetLastError != NO_ERROR) || (result == ERROR_NEGATIVE_SEEK) ) {
						return ERR_LOWLEVEL_FIN_PARTICION;
						}
				}
				else {

						nbytes_corto = (LONG) nbytes;

						result = SetFilePointer( handle, nbytes_corto, NULL, modo );

						if ( ( result == INVALID_SET_FILE_POINTER ) || ( result == ERROR_NEGATIVE_SEEK ) ) {
							return ERR_LOWLEVEL_FIN_PARTICION;
						}

				}
			}
			else if ( nsectores < 0 ) return ERR_LOWLEVEL_NSECTORES_INCORRECTO;

			break;

	  default: return ERR_LOWLEVEL_SIST_OPERATIVO_INCORRECTO;
	}

    return ERR_LOWLEVEL_NO;
}

//---------------------------------------------------------------------------
/*!
   \brief Obtiene una descripci�n del error indicado
 
    Dado un c�digo de error, obtiene una descripci�n textual del mismo.

	\param codigo_error
			 C�digo de error del que se quiere obtener la descripci�n

	\param descripcion
                         Cadena descriptiva del error indicado mediante el par�metro
                         codigo_error

	\retval ERR_LOWLEVEL_NO
             La funci�n se ejecut� correctamente

        \retval ERR_LOWLEVEL_SI
             Ha ocurrido un error al obtener la cadena descriptiva del error

 */

int lowlevel_error( IN int codigo_error, OUT char* descripcion ) {

        switch ( codigo_error ) {

                case ERR_LOWLEVEL_NO:
                                strcpy( descripcion, "No ha ocurrido ningun error");
                                break;
                case ERR_LOWLEVEL_SI:
                                strcpy( descripcion, "Ha ocurrido un error indefinido");
                                break;
                case ERR_LOWLEVEL_DISPOSITIVO_INCORRECTO:
                                strcpy( descripcion, "El dispositivo especificado es incorrecto");
                                break;
                case ERR_LOWLEVEL_SIST_OPERATIVO_INCORRECTO:
                                strcpy( descripcion, "El sistema operativo utilizado es incorrecto");
                                break;
                case ERR_LOWLEVEL_LECTURA_MBR_INCORRECTA:
                                strcpy( descripcion, "No se ha podido leer el MBR");
                                break;
                case ERR_LOWLEVEL_MBR_NO_VALIDO:
                                strcpy( descripcion, "El MBR no es valido, tiene una firma incorrecta");
                                break;
                case ERR_LOWLEVEL_UNIDAD_LOGICA_INCORRECTA:
                                strcpy( descripcion, "La unidad l�gica especificada es incorrecta");
                                break;
                case ERR_LOWLEVEL_LECTURA_INCORRECTA:
                                strcpy( descripcion, "La lectura no se ha realizado correctamente");
                                break;
                case ERR_LOWLEVEL_ESCRITURA_INCORRECTA:
                                strcpy( descripcion, "La escritura no se ha realizado correctamente");
                                break;
                case ERR_LOWLEVEL_BLOQUEANDO_UNIDAD_LOGICA:
                                strcpy( descripcion, "No se ha podido bloquear la unidad logica");
                                break;
                case ERR_LOWLEVEL_DESBLOQUEANDO_UNIDAD_LOGICA:
                                strcpy( descripcion, "No se ha podido desbloquear la unidad logica");
                                break;
                case ERR_LOWLEVEL_FIN_PARTICION:
                                strcpy( descripcion, "Se ha excedido de los limites de la particion");
                                break;
                case ERR_LOWLEVEL_PARTICION_INCORRECTA:
                                strcpy( descripcion, "Los datos que contiene la particion son incorrectos");
                                break;
                case ERR_LOWLEVEL_PARTICION_INEXISTENTE:
                                strcpy( descripcion, "La particion especificada no existe");
                                break;
                case ERR_LOWLEVEL_NSECTORES_INCORRECTO:
                                strcpy( descripcion, "El numero de sectores especificado es incorrecto");
                                break;

                default:        strcpy( descripcion, "Codigo de error desconocido");
                                return ERR_LOWLEVEL_SI;

        }

        return ERR_LOWLEVEL_NO;
}


//---------------------------------------------------------------------------
/*!
   \brief Obtiene un buffer o conjunto de sectores del dispositivo

        Devuelve un buffer del n�mero de sectores especificados mediante el
        par�metro nsectores, a partir de la posici�n indicada mediante sector_ini

	\param handle
			 Manejador para acceder al dispositivo

	\param buffer
			 Buffer que contiene la cantidad de sectores especificados
                         por el par�metro nsectores, le�dos del dispositivo

	\param sector_ini
			 N�mero de sector absoluto desde el que se quiere leer

        \param nsectores
                         N�mero de sectores a leer del dispositivo

	\retval ERR_LOWLEVEL_NO
                         La funci�n se ejecut� correctamente

	\retval ERR_LOWLEVEL_LECTURA_INCORRECTA
			 La lectura del dispositivo no se ha podido realizar correctamente

	\retval	ERR_LOWLEVEL_NSECTORES_INCORRECTO
			 El n�mero de sectores especificados para realizar la lectura es incorrecto

	\retval ERR_LOWLEVEL_SIST_OPERATIVO_INCORRECTO
             El sistema operativo utilizado no est� soportado por esta librer�a

 */

int lowlevel_leer_buffer( IN HANDLE_DISPOSITIVO handle, OUT BYTE* buffer, IN unsigned long sector_ini, IN unsigned long nsectores, IN int dispositivo ) {

	int winver, bytesSector;
	int result;
	unsigned long num_leidos;
	BYTE *ptr;
	unsigned long indice;
	unsigned long indice_sector;

	/* Obtenemos la versi�n del Windows */
	winver = lowlevel_get_winversion();

	switch (winver) {

				case WIN95:	/* Llamadas sucesivas a leer un sector */
					
					if ( !SOPORTE_WIN98 ) return ERR_LOWLEVEL_SIST_OPERATIVO_INCORRECTO;

					if ( nsectores <= 0 ) return ERR_LOWLEVEL_NSECTORES_INCORRECTO;

					ptr = buffer;
					indice_sector = sector_ini;

					for ( indice=0; indice < nsectores; indice++ ) {

						result = lowlevel_leer_sector( handle, indice_sector, ptr, -1 );
						if ( result != ERR_LOWLEVEL_NO ) return ERR_LOWLEVEL_LECTURA_INCORRECTA;

						ptr += 512;
						indice_sector++;
					}

					break;

                case WINXP:

					/* Nos posicionamos en el n�mero de sector inicial */

					bytesSector= lowlevel_obtenerBytesSector(dispositivo);
					if ( bytesSector < 0 ) 
						return ERR_LOWLEVEL_SI;

					result = lowlevel_saltar( handle, sector_ini, FALSE, dispositivo );
					if ( result != ERR_LOWLEVEL_NO ) return ERR_LOWLEVEL_SI;

					/* Leemos en el buffer */

                    if ( nsectores <= 0 ) return ERR_LOWLEVEL_NSECTORES_INCORRECTO;

					result = ReadFile( handle, buffer, 512*nsectores, &num_leidos, NULL);
					if ( (result == 0) || (num_leidos != 512*nsectores) ) return ERR_LOWLEVEL_LECTURA_INCORRECTA;

					break;

		default: return ERR_LOWLEVEL_SIST_OPERATIVO_INCORRECTO;

	}

	return ERR_LOWLEVEL_NO;
}

//---------------------------------------------------------------------------
/*!
   \brief Escribe un buffer o conjunto de sectores en el dispositivo

        Escribe el buffer del tama�o especificado mediante el par�metro nsectores
        en el dispositivo, a partir de la posici�n indicada mediante sector_ini

	\param handle
			 Manejador para acceder al dispositivo

	\param buffer
			 Buffer que contiene la cantidad de sectores especificados
                         por el par�metro nsectores, a escribir en el dispositivo

	\param sector_ini
			 N�mero de sector absoluto a partir del cual se quiere escribir

        \param nsectores
                         N�mero de sectores a escribir en el dispositivo

	\retval ERR_LOWLEVEL_NO
                         La funci�n se ejecut� correctamente

	\retval ERR_LOWLEVEL_ESCRITURA_INCORRECTA
			 La escritura del dispositivo no se ha podido realizar correctamente

	\retval	ERR_LOWLEVEL_NSECTORES_INCORRECTO
			 El n�mero de sectores especificados para realizar la lectura es incorrecto

	\retval ERR_LOWLEVEL_SIST_OPERATIVO_INCORRECTO
             El sistema operativo utilizado no est� soportado por esta librer�a

 */

int lowlevel_escribir_buffer( IN HANDLE_DISPOSITIVO handle, IN BYTE* buffer, IN unsigned long sector_ini, IN unsigned long nsectores, IN int dispositivo ) {

	int winver, bytesSector;
	int result;
	unsigned long num_escritos;
	BYTE *ptr;
	unsigned long indice_sector;
	unsigned long indice;

	/* Obtenemos la versi�n del Windows */
	winver = lowlevel_get_winversion();

	switch (winver) {

				case WIN95:

					if ( !SOPORTE_WIN98 ) return ERR_LOWLEVEL_SIST_OPERATIVO_INCORRECTO;

								/* Llamadas sucesivas a leer un sector */

					if ( nsectores <= 0 ) return ERR_LOWLEVEL_NSECTORES_INCORRECTO;

					ptr = buffer;
					indice_sector = sector_ini;

					for ( indice=0; indice < nsectores; indice++ ) {

						result = lowlevel_escribir_sector( handle, indice_sector, ptr, -1 );
						if ( result != ERR_LOWLEVEL_NO ) return ERR_LOWLEVEL_LECTURA_INCORRECTA;

						ptr += 512;
						indice_sector++;
					}

					break;

                case WINXP:

					bytesSector = lowlevel_obtenerBytesSector(dispositivo);
					if ( bytesSector < 0 )
						return ERR_LOWLEVEL_SI;

					/* Nos posicionamos en el n�mero de sector inicial */
					result = lowlevel_saltar( handle, sector_ini, FALSE, dispositivo );
					if ( result != ERR_LOWLEVEL_NO ) return ERR_LOWLEVEL_SI;

					/* Escribimos el buffer en el dispositivo */

                    if ( nsectores <= 0 ) return ERR_LOWLEVEL_NSECTORES_INCORRECTO;

					result = WriteFile( handle, buffer, bytesSector*nsectores, &num_escritos, NULL);
					if ( (result == 0) || (num_escritos != bytesSector*nsectores) ) return ERR_LOWLEVEL_ESCRITURA_INCORRECTA;

					break;

		default: return ERR_LOWLEVEL_SIST_OPERATIVO_INCORRECTO;

	}

        return ERR_LOWLEVEL_NO;
}

//---------------------------------------------------------------------------


    
int lowlevel_enumerar_dispositivos_ex ( OUT char *dispositivos[MAX_DEVICES], OUT int *numDispositivos)
{
	char *driveLetters = NULL, deviceName[MAX_DEVICE_LEN], drive[3], fileName[16];
	DWORD tam, deviceNameSize, i, j;
	UINT driveType;
	HANDLE hFindFile = NULL, hFile = NULL;
	WIN32_FIND_DATA findData;
	BOOL esClauer;
	char cdromDeviceName[10];
	int err, n_disp;
	
	memset((void *) dispositivos, 0, MAX_DEVICES * sizeof(char *));

	*numDispositivos = 0;

	tam = GetLogicalDriveStrings(0, NULL);
	if ( ! tam ) {
		err = ERR_LOWLEVEL_SI;
		goto err_lowlevel_enumerar_dispositivos_ex;
	}

	driveLetters = ( char * ) malloc ( sizeof(char) * tam );
	if ( ! driveLetters ) {
		err = ERR_LOWLEVEL_SI;
		goto err_lowlevel_enumerar_dispositivos_ex;
	}

	if ( ! GetLogicalDriveStrings(tam, driveLetters) ) {
		err = ERR_LOWLEVEL_SI;
		goto err_lowlevel_enumerar_dispositivos_ex;
	}

	i=0;
	while ( i < (tam-1) ) {

		if ( *numDispositivos >= MAX_DEVICES ) {
			err = ERR_LOWLEVEL_TOO_MUCH_DEVICES;
			goto err_lowlevel_enumerar_dispositivos_ex;
		}


		driveType = GetDriveType(driveLetters+i);

		if ( driveType == DRIVE_CDROM ) {

			/* We've a CD, so lookup for CRYF_xxx.cla files
			 */

			strncpy(fileName, driveLetters+i, 3);
			fileName[3] = 0;
			strcat(fileName, "CRYF_???.cla");
			
			strncpy(drive,driveLetters+i,2);
			drive[2] = 0;
			deviceNameSize = QueryDosDevice(drive, deviceName, MAX_DEVICE_LEN);
			strncpy(cdromDeviceName,"\\\\.\\",8);
			strcat(cdromDeviceName,drive);
			cdromDeviceName[9]='\0';		
			
			// Bajo ciertas condiciones, windows 2000 (XP no se ha probado)
			// no actualiza el estado del cdrom frente a una extracci�n y la
			// funci�n FindFirstFile hace que aparezca una ventana indicando 
			// que debemos insertar el cd. 
			// Este comportamiento se puede evitar mediante el siguiente 
			// CreateFile, que de alg�n modo actualiza el estado 
			// interno del Windows con respecto a la unidad de cdrom.


    		hFile = CreateFile(cdromDeviceName,
								GENERIC_READ,
								FILE_SHARE_READ,
								NULL,								 
								OPEN_EXISTING,						
								FILE_ATTRIBUTE_NORMAL,
								NULL);													 
			
			// Podr�amos hacer despues un IO_Control, pero no es necesario, el hecho de 
			// abrir y cerrar el handle es suficiente.
			// err = DeviceIoControl( hFile, IOCTL_DISK_UPDATE_PROPERTIES, NULL, 0, NULL, 0, &nbytes, NULL );	
			// if ( err == 0 )  
			// 	return ERR_LOWLEVEL_SI;
		
			if ( hFile != INVALID_HANDLE_VALUE ) {
				CloseHandle(hFile);

				hFindFile = FindFirstFile(fileName, &findData);	

				if ( hFindFile != INVALID_HANDLE_VALUE ) {

					do {
						/* Test if ??? are numbers. It's up to the caller
						 * application to test if it's a Clauer or not.
						 */

						if ( isdigit(*(findData.cFileName + 5)) && 
							 isdigit(*(findData.cFileName + 6)) &&
							 isdigit(*(findData.cFileName + 7)) ) {

							dispositivos[*numDispositivos] = (char *) malloc ( 16 );
							if ( ! dispositivos[*numDispositivos] ) {
								err = ERR_LOWLEVEL_SI;
								goto err_lowlevel_enumerar_dispositivos_ex;
							}

							strcpy(dispositivos[*numDispositivos], driveLetters+i);
							strcat(dispositivos[*numDispositivos], findData.cFileName);
							(*numDispositivos)++;
						}

					} while ( FindNextFile(hFindFile, &findData) );

					FindClose(hFindFile);
					hFindFile = NULL;
				}
			}			

		} else if ( GetDriveType(driveLetters+i) == DRIVE_REMOVABLE ) {

			strncpy(drive,driveLetters+i,2);
			drive[2] = 0;

			deviceNameSize = QueryDosDevice(drive, deviceName, MAX_DEVICE_LEN);
			if ( ! deviceNameSize ) {
				err = ERR_LOWLEVEL_SI;
				goto err_lowlevel_enumerar_dispositivos_ex;
			}

			/* Test if deviceName is a HardDisk by parsing
			 * it ( we look for the Harddisk substring)
			 */

			j = 0;
			while ( *(deviceName+j) ) {

				if ( *(deviceName+j) == 'H' ) {

					if ( strncmp(deviceName+j, "Harddisk", 8) == 0 ) {
						
						/* Si el dispositivo se puede leer se a�ade
						 */
						char num[3];
						int k = j+8;

						num[0] = deviceName[j+8];
						if ( isdigit(deviceName[j+9]) ) {
							num[1] = deviceName[j+9];
							num[2] = 0;
						} 
						else
							num[1] = 0;
						
						n_disp= atoi(num);
						if ( lowlevel_is_readable(drive, n_disp) ) {

							dispositivos[*numDispositivos] = (char *) malloc ( strlen("\\\\.\\PHYSICALDRIVE")+strlen(num)+1 );
							if ( ! dispositivos[*numDispositivos] ) {
								err = ERR_LOWLEVEL_SI;
								goto err_lowlevel_enumerar_dispositivos_ex;
							}

							strcpy(dispositivos[*numDispositivos], "\\\\.\\PHYSICALDRIVE");
							strcat(dispositivos[*numDispositivos], num);

							// Ojo aqu� con los par�metros, debemos abrir con los 
							// FILE_SHARE_* adecuados, ya que esto influir� en futuras 
							// aperturas mientras mantengamos ese handle abierto, i.e.
							// si abrimos un file sin FILE_SHARE_WRITE y sin cerrar el handle 
							// abrimos otra vez el fichero para escritura, CreateFile fallar�,

							 hFile = CreateFile(dispositivos[*numDispositivos],					 // leemos del dispositivo ndevice
										GENERIC_READ | GENERIC_WRITE,        // lo abrimos para leer y escribir
										FILE_SHARE_READ | FILE_SHARE_WRITE,  // compartido para leer y escribir
										NULL,								 // el handle no se puede heredar
										OPEN_EXISTING,						 // se supone que existe
										FILE_ATTRIBUTE_NORMAL | FILE_FLAG_WRITE_THROUGH | FILE_FLAG_NO_BUFFERING, // fichero normal
										NULL);								 // sin template file

								if ( hFile != INVALID_HANDLE_VALUE ) {
								if ( lowlevel_es_usbcert( hFile, &esClauer ) != ERR_LOWLEVEL_SI )
									(*numDispositivos)++;
								CloseHandle(hFile);
							}
							
							
						}
					} 

				} /* if Harddisk */

				++j;

			} /* while */

		} /* driveRemovable */

		while ( (driveLetters[i] != '\0') && (i < (tam-1)))
			++i;
		++i;

	} /* while */

	free(driveLetters);

	return ERR_LOWLEVEL_NO;

err_lowlevel_enumerar_dispositivos_ex:

	if ( driveLetters )
		free(driveLetters);

	if ( hFindFile )
		CloseHandle(hFindFile);

	for ( i = 0 ; (i < MAX_DEVICES) && dispositivos[i] ; i++ ) 
		free(dispositivos[i]);

	return err;

}

