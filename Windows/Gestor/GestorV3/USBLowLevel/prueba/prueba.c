#include <stdio.h>
#include <stdlib.h>

#include <usb_lowlevel.h>

void main() {

//	int result;
//	HANDLE_DISPOSITIVO handle;
//	BYTE sector[512];

//	printf("Lectura y escritura de sectores bajo Windows 98\n");
//	printf("-----------------------------------------------\n\n");

//	printf("Por favor, introduce el dispositivo USB y pulsa una tecla para continuar...\n");
//	fgetc(stdin);

	//result = lowlevel_abrir_dispositivo( 1, &handle );
	//if ( result != ERR_LOWLEVEL_NO ) printf("Error abriendo dispositivo, GetLastError=<%d>\n", GetLastError());

//	result = lowlevel_leer_sector( handle, 678, sector );
//	if ( result != ERR_LOWLEVEL_NO ) printf("Error leyendo sector, GetLastError=<%d>\n", GetLastError());


	int result;
	int num_disp;
	char ** unidades;
	char *dispositivos[MAX_DEVICES];
	//int * dispositivos;
	int i;
	int tiene;
	HANDLE_DISPOSITIVO handle;
	PARTICION particiones[4];

	printf("Prueba de enumeracion de dispositivos\n");
	printf("-------------------------------------\n\n");

	printf("Por favor, introduce el dispositivo USB y pulsa una tecla para continuar...\n");
	fgetc(stdin);

	result = lowlevel_enumerar_dispositivos( NULL, NULL, &num_disp );
	if ( result != ERR_LOWLEVEL_NO ) {
		printf("Error enumerando dispositivos!\n");
		return;
	}

		
	printf("Numero de dispositivos conectados=<%d>\n", num_disp);
		
	result = lowlevel_enumerar_dispositivos_ex( dispositivos, &num_disp );
	if ( result != ERR_LOWLEVEL_NO ) {
		printf("Error enumerando dispositivos!\n");
		return;
	}

	for (  i=0; i< num_disp; i++)
		printf("%s\n",dispositivos[i]);
	


	unidades = (char **) malloc (num_disp*sizeof(char)*5);

	for ( i=0; i<num_disp; i++ ) {
		unidades[i] = (char *) malloc(5*sizeof(char));
	}

	//dispositivos = ( *) malloc (num_disp*sizeof(int));


	result = lowlevel_enumerar_dispositivos( unidades, dispositivos, &num_disp );
	if ( result != ERR_LOWLEVEL_NO ) {
		printf("Error 2 enumerando dispositivos!\n");
		return;
	}

	for ( i=0; i<num_disp; i++ ) {
		printf("Dispositivo numero %d -> unidad=<%s>, dispositivo=<%d>\n", i, unidades[i], dispositivos[i]);
	}

	result = lowlevel_abrir_dispositivo( 1, &handle );
	if ( result != ERR_LOWLEVEL_NO ) {
		printf("Error abriendo dispositivo!\n");
		return;
	}

	result = lowlevel_tiene_mbr( handle, &tiene);
	if ( result != ERR_LOWLEVEL_NO ) {

		printf("Error comprobando si tiene mbr!\n");
		return;
	}

	if ( tiene ) printf("Tiene MBR!\n");
	else printf("No tiene MBR!\n");

}