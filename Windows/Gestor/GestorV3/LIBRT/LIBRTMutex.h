#ifndef __LIBRT_MUTEX_H__
#define __LIBRT_MUTEX_H__

#if defined(WIN32)

#include <windows.h>

#elif defined(LINUX)

#include <pthread.h>

#endif


#ifdef __cplusplus
extern "C" {
#endif

#if defined(WIN32)
typedef HANDLE LIBRT_MUTEX;
#elif defined(LINUX)
typedef pthread_mutex_t LIBRT_MUTEX;
#endif

#define LIBRT_MUTEX_TIMEOUT		20000	/* Tiempo m�ximo de espera en un bloqueo */


int LIBRT_MUTEX_Crear    (LIBRT_MUTEX *m);
int LIBRT_MUTEX_Destruir (LIBRT_MUTEX m);
int LIBRT_MUTEX_Lock     (LIBRT_MUTEX m);
int LIBRT_MUTEX_Unlock   (LIBRT_MUTEX m);


#ifdef __cplusplus
}
#endif

#endif
