#ifndef __LIB_MSG_H__
#define __LIB_MSG_H__

#if defined(WIN32)

#include <windows.h>
#include <winsock.h>

#elif defined(LINUX)

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>

#define SOCKET          int
#define INVALID_SOCKET  -1
#define SOCKET_ERROR    -1
#define closesocket     close

#endif

int     LIBMSG_Ini      (void);
int     LIBMSG_Fin      (void);
int     LIBMSG_Enviar   (SOCKET socket, unsigned char *buffer, int tamBuffer);
int     LIBMSG_Recibir  (SOCKET socket, unsigned char *buffer, int tamBuffer);
int     LIBMSG_Cerrar   (SOCKET socket);
int     LIBMSG_Conectar (const char *ip, unsigned short portnum, SOCKET *s);

#define ERR_LIBMSG_NO		 0
#define ERR_LIBMSG_SI		 1
#define ERR_LIBMSG_TIMEOUT	 2
#define ERR_LIBMSG_CONNRESET 3

#endif
