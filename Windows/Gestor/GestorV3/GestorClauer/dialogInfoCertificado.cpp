/****************************************************************************
**
** 	Created using Edyuk 1.0.0
**
** File : dialogInfoCertificado.cpp
** Date : lun 27. oct 18:32:33 2008
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
****************************************************************************/

/*!
	\file dialogInfoCertificado.cpp
	
	\brief Implementation of Diálogo de información sobre el certificado del Clauer
*/

#include <QtGui>
#include "dialogInfoCertificado.h"

DialogInfoCertificado::DialogInfoCertificado(CertificadoX509 &cert, QWidget *parent):QDialog(parent)
{
	setupUi(this);
	
	titular->setText(cert.getTitular());
	friendlyName->setText(cert.getFriendlyName());
	emisor->setText(cert.getEmisor());
	inicioValidez->setText(cert.getInicioValidez().toString(Qt::SystemLocaleLongDate));
	finValidez->setText(cert.getFinValidez().toString(Qt::DefaultLocaleLongDate));
}

