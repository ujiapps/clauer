/****************************************************************************
**
** 	Created using Edyuk 1.0.0
**
** File : HLClauer.cpp
** Date : lun 6. oct 18:52:27 2008
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
****************************************************************************/

/*!
	\file HLClauer.cpp
	
	\brief Implementation of Funciones de alto nivel para manejar el Clauer
*/
#include "HLClauer.h"
#include "CRYPTOWrapper/CRYPTOWrap.h"
#include "LIBIMPORT.h"
#include "auxiliares.h" //borrar
#include <string.h>

#ifdef DEBUG
#include <iostream>
using namespace std;
#endif /*DEBUG*/

int iniciaClauer(void)
{
	if(LIBRT_Ini()!=0)
		return -1;
	LIBRT_RegenerarCache();
	return 0;
}

int finalizaClauer(void)
{
	return LIBRT_Fin();
}

char *rutaClauer(int nDispositivo)
{
	char *dispositivos[MAX_DEVICES],*vuelta=NULL;
	int nDispositivos;
	size_t len;

	if(LIBRT_ListarUSBs(&nDispositivos,(unsigned char **)dispositivos)!=0)
		return NULL;
	if(nDispositivo>=nDispositivos) {
		goto bailout;
	}
	
	len=strlen(dispositivos[nDispositivo])+1;
	if((vuelta=new(std::nothrow) char[len])!=NULL) {
		memcpy(vuelta,dispositivos[nDispositivo],len);
	}
	
bailout:
	for(int i=0;i<nDispositivos;i++)
		free(dispositivos[i]);
	return vuelta;
}

void liberaRutaClauer(char *ruta)
{
	if(ruta!=NULL)
		delete [] ruta;
}

int nClauersConectados(bool soloFormateados) {
	int nDispositivos,i;
	unsigned char *dispositivos[MAX_DEVICES];
	
	if((soloFormateados?
		LIBRT_ListarDispositivos(&nDispositivos,dispositivos):
		LIBRT_ListarUSBs(&nDispositivos,dispositivos))!=0)
		return -1;
	for(i=0;i<nDispositivos;i++)
		free(dispositivos[i]);
#ifdef DEBUG
	cerr << "N�mero de Clauers conectados: " << nDispositivos << endl;
#endif /*DEBUG*/

	return nDispositivos;
}

int clauerExigeCambioPassword(int nDispositivo)
{
	int nDispositivos,i;
	unsigned char *dispositivos[MAX_DEVICES];
	int vuelta=0;
	USBCERTS_HANDLE handle_certs;
	unsigned char bloque[TAM_BLOQUE];
	long numBloque;

	if(LIBRT_ListarDispositivos(&nDispositivos,dispositivos)!=0)
		return -1;
	if(nDispositivo>=nDispositivos) {
		vuelta=-2;
		goto bailout;
	}
	if(LIBRT_IniciarDispositivo(dispositivos[nDispositivo],"",&handle_certs)!=0) {
		vuelta=-3;
		goto bailout;
	}
	numBloque=-1;
	if(LIBRT_LeerTipoBloqueCrypto(&handle_certs,BLOQUE_CHANGE_PASSWORD,1,bloque,&numBloque)!=0) {
		vuelta=-4;
		goto bailout;
	}
	if(LIBRT_FinalizarDispositivo(&handle_certs)!=0) {
		vuelta=-5;
		goto bailout;
	}
	vuelta=(numBloque!=-1?1:0);

bailout:
	for(i=0;i<nDispositivos;i++)
		free(dispositivos[i]);
	return vuelta;
}

int formateaClauer(int nDispositivo, const QString &password)
{
	char *dispositivos[MAX_DEVICES];
	int nDispositivos,vuelta=0,i;
	
	if(LIBRT_ListarDispositivos(&nDispositivos,(unsigned char **)dispositivos)!=0)
		return -1;
	if(nDispositivo>=nDispositivos) {
		vuelta=-2;
		goto bailout;
	}
	
	vuelta=LIBRT_FormatearClauerCrypto(dispositivos[nDispositivo],(char *)password.toLatin1().constData());

bailout:
#ifdef DEBUG
	cerr << "formateaClauer: " << vuelta << endl;
#endif /*DEBUG*/
	for(i=0;i<nDispositivos;i++)
		free(dispositivos[i]);
	return vuelta;		
}

int particionaClauer(int nDispositivo, int porcentaje)
{
	char *dispositivos[MAX_DEVICES];
	int nDispositivos,vuelta=0,i;

	if(LIBRT_ListarUSBs(&nDispositivos,(unsigned char **)dispositivos)!=0)
		return -1;
	if(nDispositivo>=nDispositivos) {
		vuelta=-2;
		goto bailout;
	}
	
	vuelta=LIBRT_CrearClauer(dispositivos[nDispositivo],porcentaje);

bailout:
#ifdef DEBUG
	cerr << "creaClauer: " << vuelta << endl;
#endif /*DEBUG*/
	for(i=0;i<nDispositivos;i++)
		free(dispositivos[i]);
	return vuelta;
}

int destruyeClauer(int nDispositivo)
{
	char *dispositivos[MAX_DEVICES];
	int nDispositivos,vuelta=0,i;

	if(LIBRT_ListarUSBs(&nDispositivos,(unsigned char **)dispositivos)!=0)
		return -1;
	if(nDispositivo>=nDispositivos) {
		vuelta=-2;
		goto bailout;
	}
	
	vuelta=LIBRT_EliminarClauer(dispositivos[nDispositivo]);

bailout:
#ifdef DEBUG
	cerr << "destruyeClauer: " << vuelta << endl;
#endif /*DEBUG*/
	for(i=0;i<nDispositivos;i++)
		free(dispositivos[i]);
	return vuelta;
}

int creaClauer(int nDispositivo, const QString &password, int porcentaje)
{
	int vuelta=0;
	char *dev;
	
	if((dev=rutaClauer(nDispositivo))==NULL)
		return -1;
	
	/*particionamos*/
	if(LIBRT_CrearClauer(dev,porcentaje)!=0) {
		vuelta=-2;
		goto bailout;
	}
	/*formateamos partici�n de datos*/
	if(LIBRT_FormatearClauerDatos(dev)!=0) {
		vuelta=-3;
		goto bailout;
	}	
	/*formateamos almac�n criptogr�fico*/
	if(LIBRT_FormatearClauerCrypto(dev,(char *)password.toLatin1().constData())!=0) {
		vuelta=-4;
		goto bailout;
	}
	
bailout:
	liberaRutaClauer(dev);
#ifdef DEBUG
	cerr << "creaClauer: " << vuelta << endl;
#endif /*DEBUG*/
	return vuelta;

}

//Paul: We'll check here for the new password, trying again and giving an 
//      error against failure. 
int cambiaPasswordClauer(int nDispositivo, const QString &old_password, const QString &new_password)
{
	int vuelta=0;
	char *dev;
	USBCERTS_HANDLE certs;
	
	if((dev=rutaClauer(nDispositivo))==NULL)
		return -1;
	
	const char *c_old_password=old_password.toLatin1().constData();
	if(LIBRT_IniciarDispositivo((unsigned char *)dev,(char *)c_old_password,&certs)!=0) {
		vuelta=-2;
		goto bailout0;
	}
	
	if(LIBRT_CambiarPassword(&certs,(char *)new_password.toLatin1().constData())!=0) {
		vuelta=-3;
		goto bailout;
	}


	//Check the password is ok in order to init a session, otherwise, try to 
        //set it up again.
        LIBRT_FinalizarDispositivo(&certs);
        #ifdef DEBUG
        cerr << "Comprobando la nueva  password: " << vuelta << endl;
	#endif /*DEBUG*/

        if(LIBRT_IniciarDispositivo((unsigned char *)dev,(char *)new_password.toLatin1().constData(),&certs)!=0) {
                 #ifdef DEBUG
        	cerr << "(ERROR) password KO" << vuelta << endl;
	        #endif /*DEBUG*/

		vuelta=-3;
                goto bailout0;
        }
	#ifdef DEBUG
        cerr << "password OK." << vuelta << endl;
	#endif /*DEBUG*/


	unsigned char bloque[TAM_BLOQUE];
	long numBloque;
	if(LIBRT_LeerTipoBloqueCrypto(&certs,BLOQUE_CHANGE_PASSWORD,1,bloque,&numBloque)!=-1) {
		LIBRT_BorrarBloqueCrypto(&certs,numBloque);
	}
	
bailout:
	LIBRT_FinalizarDispositivo(&certs);
bailout0:
	liberaRutaClauer(dev);
#ifdef DEBUG
	cerr << "cambiaPassword: " << vuelta << endl;
#endif /*DEBUG*/
	return vuelta;

}

long buscaBloqueClauer(int nDispositivo, const QString *password, TIPO_BLOQUE tipo, CertificadoX509 &cert)
{
	int vuelta=0;
	char *dev;
	USBCERTS_HANDLE certs;
	
	if((dev=rutaClauer(nDispositivo))==NULL)
		return -1;
	
	const char *c_password=password?password->toLatin1().constData():NULL;
	if(LIBRT_IniciarDispositivo((unsigned char *)dev,(char *)c_password,&certs)!=0) {
		vuelta=-2;
		goto bailout;
	}
	
	vuelta=buscaBloqueClauer(&certs,tipo,cert);
	LIBRT_FinalizarDispositivo(&certs);
	
bailout:
	liberaRutaClauer(dev);
#ifdef DEBUG
	cerr << "buscaBloqueClauer: " << vuelta << endl;
#endif /*DEBUG*/
	return vuelta;

}

long buscaBloqueClauer(USBCERTS_HANDLE *usbcerts, TIPO_BLOQUE tipo, CertificadoX509 &cert)
{
	int vuelta=0;
	unsigned char bloque[TAM_BLOQUE];
	long nBloque;
	QByteArray id;
	
	if(LIBRT_LeerTipoBloqueCrypto(usbcerts,tipo,1,bloque,&nBloque)!=0) {
		vuelta=-3;
		goto bailout;
	}
	
	while(nBloque!=-1) {
		unsigned char *pID;
		switch(tipo) {
			case BLOQUE_LLAVE_PRIVADA:
				pID=BLOQUE_LLAVEPRIVADA_Get_Id(bloque);
				break;
			case BLOQUE_CERT_PROPIO:
				pID=BLOQUE_CERTPROPIO_Get_Id(bloque);
				break;
			case BLOQUE_PRIVKEY_BLOB:
				pID=BLOQUE_PRIVKEYBLOB_Get_Id(bloque);
				break;
			case BLOQUE_PUBKEY_BLOB:
				pID=BLOQUE_PUBKEYBLOB_Get_Id(bloque);
				break;
			case BLOQUE_CIPHER_PRIVKEY_BLOB:
				pID=BLOQUE_CIPHPRIVKEYBLOB_Get_Id(bloque);
				break;
			case BLOQUE_CIPHER_PRIVKEY_PEM:
				pID=BLOQUE_CIPHER_PRIVKEY_PEM_Get_Id(bloque);
				break;
			default:
				vuelta=-4;
				goto bailout;
		}
		id=QByteArray((const char *)pID,20);
		if(id==cert.getCertID()) {
			vuelta=nBloque;
			break;
		} else {
			//Avanzamos al siguiente certificado		
			if(LIBRT_LeerTipoBloqueCrypto(usbcerts,tipo,0,bloque,&nBloque)!=0) {
				vuelta=-5;
				goto bailout;
			}
		}
	}
	
bailout:
	return vuelta;

}


int leeCertificadosClauer(int nDispositivo, const QString *password, QList<CertificadoX509> &salida)
{
	int vuelta=0;
	char *dev;
	USBCERTS_HANDLE certs;
	unsigned char bloque[TAM_BLOQUE];
	long nBloque;
	
	if((dev=rutaClauer(nDispositivo))==NULL)
		return -1;
	
	const char *c_password=password?password->toLatin1().constData():NULL;
	if(LIBRT_IniciarDispositivo((unsigned char *)dev,(char *)c_password,&certs)!=0) {
		vuelta=-2;
		goto bailout0;
	}
	
	if(LIBRT_LeerTipoBloqueCrypto(&certs,BLOQUE_CERT_PROPIO,1,bloque,&nBloque)!=0) {
		vuelta=-3;
		goto bailout;
	}
	
	unsigned char *cert;
	unsigned long tamCert;
	while(nBloque!=-1) {
		//Certificado X.509 en Base64
		cert=BLOQUE_CERTPROPIO_Get_Objeto(bloque);
		tamCert=BLOQUE_CERTPROPIO_Get_Tam(bloque);
#ifdef DEBUG
		cerr << cert << " - " << tamCert <<  " Es claro: " << BLOQUE_Es_Claro(bloque) << endl;
	#endif /*DEBUG*/
		
		try  {
			CertificadoX509 x509(bloque,nBloque);
	#ifdef DEBUG
			cerr << "Huella: " << x509.getCertID().toHex().constData() << endl;
	#endif /*DEBUG*/
			salida.append(x509);
		} catch(...) {
		}

		//Avanzamos al siguiente certificado		
		if(LIBRT_LeerTipoBloqueCrypto(&certs,BLOQUE_CERT_PROPIO,0,bloque,&nBloque)!=0) {
			vuelta=-4;
			goto bailout;
		}
	}
	
bailout:
	LIBRT_FinalizarDispositivo(&certs);
bailout0:
	liberaRutaClauer(dev);
#ifdef DEBUG
	cerr << "leeCertificadosClauer: " << vuelta << endl;
#endif /*DEBUG*/
	return vuelta;

}

int leePrivadaClauer(int nDispositivo, const QString &password, CertificadoX509 &cert)
{
	int vuelta=-5;
	char *dev;
	USBCERTS_HANDLE certs;
	unsigned char bloque[TAM_BLOQUE];
	long nBloque;
	QByteArray keyID;
	
	if((dev=rutaClauer(nDispositivo))==NULL)
		return -1;
	
	const char *c_password=password.toLatin1().constData();
	if(LIBRT_IniciarDispositivo((unsigned char *)dev,(char *)c_password,&certs)!=0) {
		vuelta=-2;
		goto bailout0;
	}
	
	if(LIBRT_LeerTipoBloqueCrypto(&certs,BLOQUE_LLAVE_PRIVADA,1,bloque,&nBloque)!=0) {
		vuelta=-3;
		goto bailout;
	}
	
	while(nBloque!=-1) {
#ifdef DEBUG
		cerr << "iter privada" << endl;
#endif /*DEBUG*/
		keyID=QByteArray((const char *)BLOQUE_LLAVEPRIVADA_Get_Id(bloque),20);
		if(keyID==cert.getCertID()) {
#ifdef DEBUG
			cerr << "Encontrada clave privada solicitada!!" << endl;
#endif /*DEBUG*/
			cert.setPrivateKey(BLOQUE_LLAVEPRIVADA_Get_Objeto(bloque),BLOQUE_LLAVEPRIVADA_Get_Tam(bloque));
			//////cert.setPrivateBLOB(BLOQUE_PRIVKEYBLOB_Get_Objeto(bloque),BLOQUE_PRIVKEYBLOB_Get_Tam(bloque));
			vuelta=0;
			break;
		} else {
			CRYPTO_SecureZeroMemory(bloque,TAM_BLOQUE);
			//Avanzamos al siguiente certificado		
			if(LIBRT_LeerTipoBloqueCrypto(&certs,BLOQUE_LLAVE_PRIVADA,0,bloque,&nBloque)!=0) {
				vuelta=-4;
				goto bailout;
			}
		}
	}
	
bailout:
	CRYPTO_SecureZeroMemory(bloque,TAM_BLOQUE);
	LIBRT_FinalizarDispositivo(&certs);
bailout0:
	liberaRutaClauer(dev);
#ifdef DEBUG
	cerr << "leePrivadasClauer: " << vuelta << endl;
#endif /*DEBUG*/
	return vuelta;
}

int leeBLOBClauer(int nDispositivo, const QString &password, CertificadoX509 &cert)
{
	int vuelta=-5;
	char *dev;
	USBCERTS_HANDLE certs;
	unsigned char bloque[TAM_BLOQUE];
	long nBloque;
	QByteArray keyID;
	
	if((dev=rutaClauer(nDispositivo))==NULL)
		return -1;
	
	const char *c_password=password.toLatin1().constData();
	if(LIBRT_IniciarDispositivo((unsigned char *)dev,(char *)c_password,&certs)!=0) {
		vuelta=-2;
		goto bailout0;
	}
	
	if(LIBRT_LeerTipoBloqueCrypto(&certs,BLOQUE_PRIVKEY_BLOB,1,bloque,&nBloque)!=0) {
		vuelta=-3;
		goto bailout;
	}
	
	while(nBloque!=-1) {
#ifdef DEBUG
		cerr << "iter blob privado" << endl;
#endif /*DEBUG*/
		keyID=QByteArray((const char *)BLOQUE_PRIVKEYBLOB_Get_Id(bloque),20);
		if(keyID==cert.getCertID()) {
#ifdef DEBUG
			cerr << "Encontrado blob privado solicitado!!" << endl;
#endif /*DEBUG*/
			cert.setPrivateBLOB(BLOQUE_PRIVKEYBLOB_Get_Objeto(bloque),BLOQUE_PRIVKEYBLOB_Get_Tam(bloque));
			vuelta=0;
			break;
		} else {
			CRYPTO_SecureZeroMemory(bloque,TAM_BLOQUE);
			//Avanzamos al siguiente certificado		
			if(LIBRT_LeerTipoBloqueCrypto(&certs,BLOQUE_PRIVKEY_BLOB,0,bloque,&nBloque)!=0) {
				vuelta=-4;
				goto bailout;
			}
		}
	}
	
bailout:
	CRYPTO_SecureZeroMemory(bloque,TAM_BLOQUE);
	LIBRT_FinalizarDispositivo(&certs);
bailout0:
	liberaRutaClauer(dev);
#ifdef DEBUG
	cerr << "leeBLOBClauer: " << vuelta << endl;
#endif /*DEBUG*/
	return vuelta;
}

int borraCertificadoClauer(int nDispositivo, const QString &password, CertificadoX509 &cert)
{
	int vuelta;
	char *dev;
	USBCERTS_HANDLE certs;
	
	unsigned long nBloques;
	unsigned char *bloques;
	long *hBloques;
	unsigned char zeroID[20]={0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
	bool found=false,rewrite=false;
	
	unsigned int lstContainerSize=NUM_KEY_CONTAINERS;
    INFO_KEY_CONTAINER lstContainer[NUM_KEY_CONTAINERS];
	
	if((dev=rutaClauer(nDispositivo))==NULL)
		return -1;
	
	const char *c_password=password.toLatin1().constData();
	if(LIBRT_IniciarDispositivo((unsigned char *)dev,(char *)c_password,&certs)!=0) {
		vuelta=-2;
		goto bailout0;
	}
	
	//Leemos todos los bloques ocupados del Clauer
	if (LIBRT_LeerTodosBloquesOcupados(&certs,NULL,NULL,&nBloques)!=0) {
		vuelta=-3;
		goto bailout1;
	}
	
	if((bloques=new(std::nothrow) unsigned char[nBloques*TAM_BLOQUE])==NULL) {
		vuelta=-4;
		goto bailout1;
	}
	if((hBloques=new(std::nothrow) long[nBloques])==NULL) {
		vuelta=-5;
		goto bailout2;
	}
	
	if(LIBRT_LeerTodosBloquesOcupados(&certs,hBloques,bloques,&nBloques)!=0) {
		vuelta=-6;
		goto bailout3;
	}
	
	//Borramos todos los bloques con nuestro identificador
	for(unsigned long i=0;i<nBloques;i++) {
		unsigned char *bloque=&bloques[i*TAM_BLOQUE];
		unsigned char tipoBloque=bloque[1];
		if(tipoBloque==BLOQUE_KEY_CONTAINERS) {
			rewrite=false;
			//En los key containers solo hay que modificar el puntero a la clave
			if(BLOQUE_KeyContainer_Enumerar(bloque,lstContainer,&lstContainerSize)!=0) {
				vuelta=-7;
				goto bailout3;
			}
			for(unsigned int j=0;j<lstContainerSize;j++) {
				if(memcmp(lstContainer[j].idExchange,cert.getCertID().constData(),20)==0) {
					if((memcmp(lstContainer[j].idSignature,cert.getCertID().constData(),20)==0)||(memcmp(lstContainer[j].idSignature,zeroID,20)==0)) {
						if(BLOQUE_KeyContainer_Borrar(bloque,lstContainer[j].nombreKeyContainer )!=0) {
							vuelta=-8;
							goto bailout3;
						}
						rewrite=true;
					} else {
						if(BLOQUE_KeyContainer_Establecer_ID_Exchange(bloque,lstContainer[j].nombreKeyContainer,zeroID)!=0) {
							vuelta=-9;
							goto bailout3;
						}
						rewrite=true;
					}
				} else if(memcmp(lstContainer[j].idSignature,cert.getCertID().constData(),20)==0) {
					if((memcmp(lstContainer[j].idExchange,cert.getCertID().constData(),20)==0)||(memcmp(lstContainer[j].idExchange,zeroID,20)==0)) {
						if(BLOQUE_KeyContainer_Borrar(bloque,lstContainer[j].nombreKeyContainer)!=0) {
							vuelta=-10;
							goto bailout3;
						}
						rewrite=true;
					
					} else {
						if (BLOQUE_KeyContainer_Establecer_ID_Signature(bloque,lstContainer[j].nombreKeyContainer,zeroID)!=0) {
							vuelta=-11;
							goto bailout3;
						}
						rewrite=true;
					}
				}
			} /*for j*/
			
			if(rewrite) {
				if(BLOQUE_KeyContainer_Enumerar(bloque,lstContainer,&lstContainerSize)!=0) {
					break;
				}
				if(lstContainerSize==0) {
					if (LIBRT_BorrarBloqueCrypto(&certs,hBloques[i])!=0) {
						vuelta=-12;
						goto bailout3;
					}
				} else {
					if(LIBRT_EscribirBloqueCrypto(&certs,hBloques[i],bloque)!=0) {
						vuelta=-13;
						goto bailout3;
					}
				}
			}
		} else {
			found=false;
			switch(tipoBloque) {
				case BLOQUE_LLAVE_PRIVADA:
					found=memcmp(cert.getCertID().constData(),BLOQUE_LLAVEPRIVADA_Get_Id(bloque),20)==0;
					break;
				case BLOQUE_CIPHER_PRIVKEY_PEM:
					found=memcmp(cert.getCertID().constData(),BLOQUE_CIPHER_PRIVKEY_PEM_Get_Id(bloque),20)==0;
					break;
				case BLOQUE_PRIVKEY_BLOB:
					found=memcmp(cert.getCertID().constData(),BLOQUE_PRIVKEYBLOB_Get_Id(bloque),20)==0;
					break;
				case BLOQUE_CIPHER_PRIVKEY_BLOB:
					found=memcmp(cert.getCertID().constData(),BLOQUE_CIPHPRIVKEYBLOB_Get_Id(bloque),20)==0;
					break;
				default:
					found=false;
					break;
			}
			
			if(found&&(LIBRT_BorrarBloqueCrypto(&certs,hBloques[i])!=0)) {
				vuelta=-14;
				goto bailout3;
			}
		}
	} /*for i*/
	
	//Finalmente borramos la parte p�blica del certificado
	if(LIBRT_BorrarBloqueCrypto(&certs,cert.getBloque())!=0) {
		vuelta=-15;
		goto bailout3;
	}
	vuelta=0;
	
bailout3:
	delete [] hBloques;
bailout2:
	CRYPTO_SecureZeroMemory(bloques,nBloques*TAM_BLOQUE);
	delete [] bloques;	
bailout1:
	LIBRT_FinalizarDispositivo(&certs);
bailout0:
	liberaRutaClauer(dev);
#ifdef DEBUG
	cerr << "borraCertificadoClauer: " << vuelta << endl;
#endif /*DEBUG*/
	return vuelta;
}


int importaPKCS12(int nDispositivo, const QString *passwordClauer, const QString &archivo, const QString &passwordCert)
{
	int vuelta=-400;
	char *dev;
	USBCERTS_HANDLE certs;
	
	QByteArray pkcs12;
	if(!leeFichero(archivo,pkcs12))
		return -1;
	
	if((dev=rutaClauer(nDispositivo))==NULL)
		return -2;
	
	const char *c_passwordClauer=passwordClauer?passwordClauer->toLatin1().constData():NULL;
	if(LIBRT_IniciarDispositivo((unsigned char *)dev,(char *)c_passwordClauer,&certs)!=0) {
		vuelta=-3;
		goto bailout;
	}
	
	//if(LIBIMPORT_ImportarPKCS12(archivo.toLocal8Bit().constData(),passwordCert.toLatin1().constData(),&certs,PRIVKEY_DEFAULT_CIPHER,NULL)!=FALSE)
	if(LIBIMPORT_ImportarPKCS12deBuffer((BYTE *)pkcs12.constData(),pkcs12.size(),passwordCert.toLatin1().constData(),&certs,PRIVKEY_DEFAULT_CIPHER,NULL,1))
		vuelta=0;
	LIBRT_FinalizarDispositivo(&certs);
	
bailout:
	liberaRutaClauer(dev);
#ifdef DEBUG
	cerr << "importaPKCS12: " << vuelta << endl;
#endif /*DEBUG*/
	return vuelta;

}
