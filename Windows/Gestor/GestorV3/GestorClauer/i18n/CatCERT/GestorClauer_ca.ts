<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS><TS version="1.1" language="ca">
<defaultcodec></defaultcodec>
<context>
    <name>DialogImpl</name>
    <message>
        <location filename="../src/dialogimpl.cpp" line="631"/>
        <source>Todos los datos del sitck USB se borrar&#xe1;n. &#xbf;Est&#xe1; seguro que desea continuar?</source>
        <translation>Totes les dades de l&apos;sitck USB s&apos;esborraran. Esteu segur que voleu continuar?</translation>
    </message>
    <message>
        <location filename="../src/dialogimpl.cpp" line="660"/>
        <source>No se ha podido formatear el Clauer.</source>
        <translation>No s&apos;ha pogut formatar el clauer.</translation>
    </message>
    <message>
        <location filename="../src/dialogimpl.cpp" line="528"/>
        <source>Titular del certificado</source>
        <translation>Titular del certificat</translation>
    </message>
    <message>
        <location filename="../src/dialogimpl.cpp" line="529"/>
        <source>Organizaci&#xf3;n que ha emitido el certificado</source>
        <translation>Organització que ha emès el certificat</translation>
    </message>
    <message>
        <location filename="../src/dialogimpl.cpp" line="530"/>
        <source>Fecha de caducidad del certificado</source>
        <translation>Data de caducitat del certificat</translation>
    </message>
    <message>
        <location filename="../src/dialogimpl.cpp" line="200"/>
        <source>Visualizar informaci&#xf3;n del certificado</source>
        <translation>Visualitzar informació del certificat</translation>
    </message>
    <message>
        <location filename="../src/dialogimpl.cpp" line="201"/>
        <source>Exportar el certificado a un fichero PKCS#12</source>
        <translation>Exportar el certificat a un fitxer PKCS#12</translation>
    </message>
    <message>
        <location filename="../src/dialogimpl.cpp" line="203"/>
        <source>Exportar el certificado al Internet Explorer</source>
        <translation>Exportar el certificat a l&apos;Internet Explorer</translation>
    </message>
    <message>
        <location filename="../src/dialogimpl.cpp" line="269"/>
        <source>Seleccione archivo en donde se exportar&#xe1; el certificado</source>
        <translation>Seleccioneu l&apos;arxiu on s&apos;exportarà el certificat</translation>
    </message>
    <message>
        <location filename="../src/dialogimpl.cpp" line="271"/>
        <source>Contenedor criptogr&#xe1;fico PKCS#12 (*.p12)</source>
        <translation>Contenidor criptogràfic PKCS#12 (*.p12)</translation>
    </message>
    <message>
        <location filename="../src/dialogimpl.cpp" line="285"/>
        <source>El certificado se ha exportado correctamente a la ruta: %1.</source>
        <translation>El certificat s’ha exportat correctament a la ruta: %1.</translation>
    </message>
    <message>
        <location filename="../src/dialogimpl.cpp" line="345"/>
        <source>No se pudo exportar el certificado.</source>
        <translation>No s&apos;ha pogut exportar el certificat.</translation>
    </message>
    <message>
        <location filename="../src/dialogimpl.cpp" line="340"/>
        <source>El certificado se ha exportado correctamente.</source>
        <translation>El certificat s&apos;ha exportat correctament.</translation>
    </message>
    <message>
        <location filename="../src/dialogimpl.cpp" line="361"/>
        <source>Se encuentra a punto de eliminar permanentemente el certificado del clauer.
&#xbf;Desea continuar?</source>
        <translation>Esteu a punt d&apos;eliminar permanentment el certificat del clauer.
Voleu continuar?</translation>
    </message>
    <message>
        <location filename="../src/dialogimpl.cpp" line="384"/>
        <source>El certificado se ha eliminado correctamente.</source>
        <translation>El certificat s’ha eliminat correctament.</translation>
    </message>
    <message>
        <location filename="../src/dialogimpl.cpp" line="389"/>
        <source>No se ha podido eliminar el certificado.</source>
        <translation>No s&apos;ha pogut eliminar el certificat.</translation>
    </message>
    <message>
        <location filename="../src/dialogimpl.cpp" line="469"/>
        <source>Seleccione el archivo que contenga el certificado a importar</source>
        <translation>Seleccioneu l&apos;arxiu que contingui el certificat a importar</translation>
    </message>
    <message>
        <location filename="../src/dialogimpl.cpp" line="471"/>
        <source>Certificados digitales (*.p12; *.pfx)</source>
        <translation>Certificats digitals (*.p12; *.pfx)</translation>
    </message>
    <message>
        <location filename="../src/dialogimpl.cpp" line="492"/>
        <source>El certificado se ha importado correctamente dentro del clauer.</source>
        <translation>El certificat s&apos;ha importat correctament dins del clauer.</translation>
    </message>
    <message>
        <location filename="../src/dialogimpl.cpp" line="585"/>
        <source>La operaci&#xf3;n no se ha podido realizar porque el certificado elegido no es exportable.</source>
        <translation>La operació no s&apos;ha pogut realizar perquè el certificat escollit no és exportable.</translation>
    </message>
    <message>
        <location filename="../src/dialogimpl.cpp" line="633"/>
        <source>A continuaci&#xf3;n se va a formatear la zona criptogr&#xe1;fica, todos los certificados que contenga el clauer ser&#xe1;n eliminados.
&#xbf;Est&#xe1; seguro de que desea formatear la zona criptogr&#xe1;fica?</source>
        <translation>A continuació es formatarà la zona criptogràfica, tots els certificats que contingui el clauer seran eliminats.
Esteu segur que voleu formatar la zona criptogràfica?</translation>
    </message>
    <message>
        <location filename="../src/dialogimpl.cpp" line="692"/>
        <source>Hay m&#xe1;s de un Stick USB insertado en el sistema
Por favor, deje insertado
&#xfa;nicamente el dispostivo que desee inicializar y pulse el bot&#xf3;n Aceptar.</source>
        <translation type="unfinished">Hi ha més d&apos;un Stick USB inserit al sistema.
Si us plau, deixeu inserit únicament el dispostiu que vulgueu inicializar i premeu el botó Acceptar.</translation>
    </message>
    <message>
        <location filename="../src/dialogimpl.cpp" line="205"/>
        <source>Elimina el certificado del clauer</source>
        <translation>Elimina el certificat del clauer</translation>
    </message>
    <message>
        <location filename="../src/dialogimpl.cpp" line="428"/>
        <source>La contrase&#xf1;a del clauer se ha cambiado correctamente.</source>
        <translation>La paraula de pas del clauer s’ha canviat correctament.</translation>
    </message>
    <message>
        <location filename="../src/dialogimpl.cpp" line="435"/>
        <source>No se ha podido cambiar la contrase&#xf1;a del clauer. &#xbf;Es correcta la contrase&#xf1;a actual?</source>
        <translation>No s&apos;ha pogut canviar la paraula de pas del clauer. És correcta la paraula de pas actual?</translation>
    </message>
    <message>
        <location filename="../src/dialogimpl.cpp" line="651"/>
        <source>El clauer se ha formateado correctamente.</source>
        <translation>El clauer s’ha formatat correctament.</translation>
    </message>
    <message>
        <location filename="../src/dialogimpl.cpp" line="577"/>
        <source>El certificado se ha movido correctamente.</source>
        <translation>El certificat s&apos;ha mogut correctament.</translation>
    </message>
    <message>
        <location filename="../src/dialogimpl.cpp" line="578"/>
        <source>El certificado se ha copiado correctamente.</source>
        <translation>El certificat s&apos;ha copiat correctament.</translation>
    </message>
</context>
<context>
    <name>DialogPrincipal</name>
    <message>
        <location filename="../ui/principal.ui" line="515"/>
        <source>Nombre del certificado</source>
        <translation>Nom del certificat</translation>
    </message>
    <message>
        <location filename="../ui/principal.ui" line="525"/>
        <source>Caducidad</source>
        <translation>Caducitat</translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../ui/principal.ui" line="145"/>
        <source>Contraseña</source>
        <translation>Paraula de pas</translation>
    </message>
    <message>
        <location filename="../ui/principal.ui" line="309"/>
        <source>Importar fichero</source>
        <translation>Importar fitxer</translation>
    </message>
    <message>
        <location filename="../ui/principal.ui" line="681"/>
        <source>Formatear</source>
        <translation>Formatar</translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../ui/principal.ui" line="615"/>
        <source>Contraseña:</source>
        <translation>Paraula de pas:</translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../ui/principal.ui" line="691"/>
        <source>Información</source>
        <translation>Informació</translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../ui/principal.ui" line="299"/>
        <source>Cambiar contraseña</source>
        <translation>Canviar paraula de pas</translation>
    </message>
    <message>
        <location filename="../ui/principal.ui" line="22"/>
        <source>Gestor del Clauer</source>
        <translation>Gestor del Clauer</translation>
    </message>
    <message>
        <location filename="../ui/principal.ui" line="54"/>
        <source>Certificados dentro del clauer</source>
        <translation>Certificats dins del Clauer</translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../ui/principal.ui" line="151"/>
        <source>Desde esta pestaña puede cambiar la contraseña de su clauer. Para hacerlo, debe introducir la contraseña actual y a continuación la nueva dos veces.</source>
        <translation>Des d&apos;aquesta pestanya podeu canviar la paraula de pas del vostre clauer. Per a fer-ho, heu d&apos;introduir la paraula de pas actual i a continuació la nova dues vegades.</translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../ui/principal.ui" line="177"/>
        <source>Introduzca la contraseña actual</source>
        <translation>Introduiu la paraula de pas actual</translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../ui/principal.ui" line="191"/>
        <source>Contraseña actual del Clauer</source>
        <translation>Paraula de pas actual del Clauer</translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../ui/principal.ui" line="219"/>
        <source>Introduzca la nueva contraseña</source>
        <translation>Introduïu la nova paraula de pas</translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../ui/principal.ui" line="230"/>
        <source>Nueva contraseña:</source>
        <translation>Nova paraula de pas:</translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../ui/principal.ui" line="240"/>
        <source>Nueva contraseña del Clauer</source>
        <translation>Nova paraula de pas del Clauer</translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../ui/principal.ui" line="266"/>
        <source>Confirmación de la nueva contraseña:</source>
        <translation>Confirmació nova paraula de pas:</translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../ui/principal.ui" line="315"/>
        <source>Desde esta pestaña puede importar dentro del clauer cualquier certificado en formato fichero (extensiones .pfx y .p12).</source>
        <translation>Des d&apos;aquesta pestanya podeu importar dins del clauer qualsevol certificat en format fitxer (extensions .pfx y .p12).</translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../ui/principal.ui" line="341"/>
        <source>Ubicación del fichero a importar dentro del clauer</source>
        <translation>Ubicació del fitxer a importar dins del clauer</translation>
    </message>
    <message>
        <location filename="../ui/principal.ui" line="355"/>
        <source>Buscar el certficiado en el sistema de ficheros</source>
        <translation>Buscar el certificat al sistema de fitxers</translation>
    </message>
    <message>
        <location filename="../ui/principal.ui" line="358"/>
        <source>Examinar</source>
        <translation>Examinar</translation>
    </message>
    <message>
        <location filename="../ui/principal.ui" line="371"/>
        <source>Ruta completa del fichero que contiene el certificado digital a importar dentro del clauer</source>
        <translation>Ruta completa del fitxer que conté el certificat digital a importar dins del clauer</translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../ui/principal.ui" line="410"/>
        <source>Contraseña que protege el fichero que desea importar al clauer</source>
        <translation>Paraula de pas que protegeix el fitxer que voleu importar al clauer</translation>
    </message>
    <message>
        <location filename="../ui/principal.ui" line="443"/>
        <source>Importar</source>
        <translation>Importar</translation>
    </message>
    <message>
        <location filename="../ui/principal.ui" line="453"/>
        <source>Importar desde Internet Explorer</source>
        <translation>Importar des d’Internet Explorer</translation>
    </message>
    <message>
        <location filename="../ui/principal.ui" line="548"/>
        <source>Mover dentro del clauer</source>
        <translation>Moure dins del Clauer</translation>
    </message>
    <message>
        <location filename="../ui/principal.ui" line="555"/>
        <source>Copiar dentro del clauer</source>
        <translation>Copiar dins del clauer</translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../ui/principal.ui" line="604"/>
        <source>Introduzca la contraseña que protegerá al clauer idCAT</source>
        <translation>Introduïu la paraula de pas que protegirà el clauer idCAT</translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../ui/principal.ui" line="625"/>
        <source>Contraseña que tendrá el clauer una vez formateado</source>
        <translation>Paraula de pas que tindrà el clauer un cop formatat</translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../ui/principal.ui" line="638"/>
        <source>Confirmación contraseña:</source>
        <translation>Confirmació paraula de pas:</translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../ui/principal.ui" line="648"/>
        <source>Confirmación de la contraseña</source>
        <translation>Confirmació de la paraula de pas</translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../ui/principal.ui" line="459"/>
        <source>Seleccione el certificado digital que desee copiar o mover desde el almacén de certificados de Internet Explorer hacia la zona criptográfica del clauer:</source>
        <translation>Seleccioneu el certificat digital que voleu copiar o moure des del magatzem de certificats d&apos;Internet Explorer cap a la zona criptogràfica del clauer:</translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../ui/principal.ui" line="389"/>
        <source>Contraseña que protege al fichero</source>
        <translation>Paraula de pas que protegeix el fitxer</translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../ui/principal.ui" line="253"/>
        <source>Confirmación de la nueva contraseña del Clauer</source>
        <translation>Confirmació de la nova paraula de pas del clauer</translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../ui/principal.ui" line="520"/>
        <source>Entidad de certificación</source>
        <translation>Entitat de certificació</translation>
    </message>
</context>
<context>
    <name>InfoCertificado</name>
    <message encoding="UTF-8">
        <location filename="../ui/infoCertificado.ui" line="16"/>
        <source>Información del certificado</source>
        <translation>Informació del certificat</translation>
    </message>
    <message>
        <location filename="../ui/infoCertificado.ui" line="30"/>
        <source>Titular:</source>
        <translation>Titular:</translation>
    </message>
    <message>
        <location filename="../ui/infoCertificado.ui" line="70"/>
        <source>Entidad emisora:</source>
        <translation>Entitat emisora:</translation>
    </message>
    <message>
        <location filename="../ui/infoCertificado.ui" line="82"/>
        <source>Validez del certificado</source>
        <translation>Validesa del certificat</translation>
    </message>
    <message>
        <location filename="../ui/infoCertificado.ui" line="113"/>
        <source>Hasta:</source>
        <translation>Fins a:</translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../ui/infoCertificado.ui" line="123"/>
        <source>Válido desde:</source>
        <translation>Vàlid des de:</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../auxiliares.h" line="35"/>
        <source>Gestor del Clauer</source>
        <translation>Gestor del Clauer</translation>
    </message>
    <message>
        <location filename="../auxiliares.cpp" line="43"/>
        <source>Imposible comunicar con el clauer. &#xbf;Tenemos privilegios suficientes?</source>
        <translation>Impossible comunicar amb el Clauer. Teniu privilegis suficients?</translation>
    </message>
    <message>
        <location filename="../auxiliares.cpp" line="80"/>
        <source>Hay m&#xe1;s de un Stick USB insertado en el sistema
Por favor, pulse el bot&#xf3;n Aceptar y deje insertado
&#xfa;nicamente el dispostivo que desee inicializar.</source>
        <translation type="unfinished">Hi ha més d&apos;un Stick USB inserit al sistema.
Si us plau, premeu el botó Acceptar y deixeu inserit únicament el dispostiu que voleu inicialitzar.</translation>
    </message>
    <message>
        <location filename="../auxiliares.cpp" line="99"/>
        <source>No hay ning&#xfa;n Stick USB insertado en el sistema
Por favor, pulse el bot&#xf3;n Aceptar e inserte el dispositivo
que desee inicializar.</source>
        <translation>No hi ha cap Stick USB inserit al sistema.
Si us plau, premeu el botó Acceptar i inseriu el dispositiu que voleu inicialitzar.</translation>
    </message>
    <message>
        <location filename="../auxiliares.cpp" line="164"/>
        <source>La nueva contrase&#xf1;a y su confirmaci&#xf3;n deben coincidir.</source>
        <translation>La nova paraula de pas i la seva confirmació han de coincidir.</translation>
    </message>
    <message>
        <location filename="../auxiliares.cpp" line="184"/>
        <source>La contrase&#xf1;a es demasiado corta, ha de tener un m&#xed;nimo de 4 caracteres.</source>
        <translation>La paraula de pas és massa curta, ha de tenir un mínim de 4 caràcters.</translation>
    </message>
    <message>
        <location filename="../auxiliares.cpp" line="193"/>
        <source>La nueva contrase&#xf1;a es demasiado simple.</source>
        <translation>La nova paraula de pas és massa simple.</translation>
    </message>
    <message>
        <location filename="../src/dialogimpl.cpp" line="500"/>
        <source>Contrase&#xf1;a del certificado incorrecta.</source>
        <translation>Paraula de pas del certificat incorrecta.</translation>
    </message>
    <message>
        <location filename="../src/dialogimpl.cpp" line="94"/>
        <source>Introduzca la contrase&#xf1;a del clauer:</source>
        <translation>Introdueixi la paraula de pas del clauer:</translation>
    </message>
    <message>
        <location filename="../src/dialogimpl.cpp" line="111"/>
        <source>No se ha podido completar la operaci&#xf3;n. &#xbf;Es correcta la contrase&#xf1;a del clauer que ha introducido?</source>
        <translation>No s’ha pogut completar l’operació. És correcta la paraula de pas del clauer que heu introduït?</translation>
    </message>
    <message>
        <location filename="../auxiliares.cpp" line="173"/>
        <source>La contrase&#xf1;a contiene alg&#xfa;n car&#xe1;cter no permitido.</source>
        <translation type="unfinished">La paraula de pas conté algun caràcter no permès.</translation>
    </message>
</context>
</TS>
