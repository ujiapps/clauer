<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS><TS version="1.1" language="es">
<defaultcodec></defaultcodec>
<context>
    <name>DialogImpl</name>
    <message>
        <location filename="../src/dialogimpl.cpp" line="631"/>
        <source>Todos los datos del sitck USB se borrar&#xe1;n. &#xbf;Est&#xe1; seguro que desea continuar?</source>
        <translation>Totes es donades dera sitck USB s&apos;esborraràn. Ètz segur que voletz contunhar?</translation>
    </message>
    <message>
        <location filename="../src/dialogimpl.cpp" line="660"/>
        <source>No se ha podido formatear el Clauer.</source>
        <translation>Non s&apos;a pogut formatar eth clauer.</translation>
    </message>
    <message>
        <location filename="../src/dialogimpl.cpp" line="528"/>
        <source>Titular del certificado</source>
        <translation>Titular deth certificat</translation>
    </message>
    <message>
        <location filename="../src/dialogimpl.cpp" line="529"/>
        <source>Organizaci&#xf3;n que ha emitido el certificado</source>
        <translation>Organizacion qu&apos;a emetut eth certificat</translation>
    </message>
    <message>
        <location filename="../src/dialogimpl.cpp" line="530"/>
        <source>Fecha de caducidad del certificado</source>
        <translation>Date de caducitat deth certificat</translation>
    </message>
    <message>
        <location filename="../src/dialogimpl.cpp" line="200"/>
        <source>Visualizar informaci&#xf3;n del certificado</source>
        <translation>Visualizar informacion deth certificat</translation>
    </message>
    <message>
        <location filename="../src/dialogimpl.cpp" line="201"/>
        <source>Exportar el certificado a un fichero PKCS#12</source>
        <translation>Exportar eth certificat a un fichèr PKCS#12</translation>
    </message>
    <message>
        <location filename="../src/dialogimpl.cpp" line="203"/>
        <source>Exportar el certificado al Internet Explorer</source>
        <translation>Exportar eth certificat ena Internet Explorer</translation>
    </message>
    <message>
        <location filename="../src/dialogimpl.cpp" line="269"/>
        <source>Seleccione archivo en donde se exportar&#xe1; el certificado</source>
        <translation>Seleccionatz er archiu a on s&apos;exportarà eth certificat</translation>
    </message>
    <message>
        <location filename="../src/dialogimpl.cpp" line="271"/>
        <source>Contenedor criptogr&#xe1;fico PKCS#12 (*.p12)</source>
        <translation>Contienedor criptogràfic PKCS#12 (*.P12)</translation>
    </message>
    <message>
        <location filename="../src/dialogimpl.cpp" line="285"/>
        <source>El certificado se ha exportado correctamente a la ruta: %1.</source>
        <translation>Eth certificat s&apos;a exportat corrèctaments ara rota: %1.</translation>
    </message>
    <message>
        <location filename="../src/dialogimpl.cpp" line="345"/>
        <source>No se pudo exportar el certificado.</source>
        <translation>Non s&apos;a pogut exportar eth certificat.</translation>
    </message>
    <message>
        <location filename="../src/dialogimpl.cpp" line="340"/>
        <source>El certificado se ha exportado correctamente.</source>
        <translation>Eth certificat s&apos;a exportat corrèctaments.</translation>
    </message>
    <message>
        <location filename="../src/dialogimpl.cpp" line="361"/>
        <source>Se encuentra a punto de eliminar permanentemente el certificado del clauer.
&#xbf;Desea continuar?</source>
        <translation>Ètz prèste d&apos;eliminar permanentaments eth certificat deth clauer.
Voletz contunhar?</translation>
    </message>
    <message>
        <location filename="../src/dialogimpl.cpp" line="384"/>
        <source>El certificado se ha eliminado correctamente.</source>
        <translation>Eth certificat s&apos;a eliminat corrèctaments.</translation>
    </message>
    <message>
        <location filename="../src/dialogimpl.cpp" line="389"/>
        <source>No se ha podido eliminar el certificado.</source>
        <translation>Non s&apos;a pogut eliminar eth certificat.</translation>
    </message>
    <message>
        <location filename="../src/dialogimpl.cpp" line="469"/>
        <source>Seleccione el archivo que contenga el certificado a importar</source>
        <translation>Seleccionatz er archiu que contengue eth certificat a importar</translation>
    </message>
    <message>
        <location filename="../src/dialogimpl.cpp" line="471"/>
        <source>Certificados digitales (*.p12; *.pfx)</source>
        <translation>Certificadi digitaus (*.P12; *.pfx)</translation>
    </message>
    <message>
        <location filename="../src/dialogimpl.cpp" line="492"/>
        <source>El certificado se ha importado correctamente dentro del clauer.</source>
        <translation>Eth certificat s&apos;a importat corrèctaments laguens deth Clauer.</translation>
    </message>
    <message>
        <location filename="../src/dialogimpl.cpp" line="585"/>
        <source>La operaci&#xf3;n no se ha podido realizar porque el certificado elegido no es exportable.</source>
        <translation>Era operacion non s&apos;a pogut realizar pr&apos;amor qu&apos;eth certificat escuelhut non ei exportable.</translation>
    </message>
    <message>
        <location filename="../src/dialogimpl.cpp" line="633"/>
        <source>A continuaci&#xf3;n se va a formatear la zona criptogr&#xe1;fica, todos los certificados que contenga el clauer ser&#xe1;n eliminados.
&#xbf;Est&#xe1; seguro de que desea formatear la zona criptogr&#xe1;fica?</source>
        <translation>De contunh se formatarà era zòna criptogràfica, toti es certificadi que contengue eth clauer seràn eliminadi.￼Ètz segur que voletz formatar era zòna criptogràfica?</translation>
    </message>
    <message>
        <location filename="../src/dialogimpl.cpp" line="692"/>
        <source>Hay m&#xe1;s de un Stick USB insertado en el sistema
Por favor, deje insertado
&#xfa;nicamente el dispostivo que desee inicializar y pulse el bot&#xf3;n Aceptar.</source>
        <translation type="unfinished">I a mès d&apos;un Stick USB inserit ath sistèma.
Se vos platz, deishatz inserit unicaments eth dispostiu que volgatz inicializar e premetz eth boton Acceptar.</translation>
    </message>
    <message>
        <location filename="../src/dialogimpl.cpp" line="205"/>
        <source>Elimina el certificado del clauer</source>
        <translation>Elimine eth certificat deth clauer</translation>
    </message>
    <message>
        <location filename="../src/dialogimpl.cpp" line="428"/>
        <source>La contrase&#xf1;a del clauer se ha cambiado correctamente.</source>
        <translation>Era paraula de pas deth clauer s&apos;a cambiat corrèctaments.</translation>
    </message>
    <message>
        <location filename="../src/dialogimpl.cpp" line="435"/>
        <source>No se ha podido cambiar la contrase&#xf1;a del clauer. &#xbf;Es correcta la contrase&#xf1;a actual?</source>
        <translation>Non s&apos;a pogut cambiar era paraula de pas deth clauer. Ei corrècta era paraula de pas actuau?</translation>
    </message>
    <message>
        <location filename="../src/dialogimpl.cpp" line="651"/>
        <source>El clauer se ha formateado correctamente.</source>
        <translation>Eth clauer s&apos;a formatat corrèctaments.</translation>
    </message>
    <message>
        <location filename="../src/dialogimpl.cpp" line="577"/>
        <source>El certificado se ha movido correctamente.</source>
        <translation>Eth certificat s&apos;a moigut corrèctaments.</translation>
    </message>
    <message>
        <location filename="../src/dialogimpl.cpp" line="578"/>
        <source>El certificado se ha copiado correctamente.</source>
        <translation>Çò de certificat s&apos;a copiat corrèctaments.</translation>
    </message>
</context>
<context>
    <name>DialogPrincipal</name>
    <message>
        <location filename="../ui/principal.ui" line="515"/>
        <source>Nombre del certificado</source>
        <translation>Nòm deth certificat</translation>
    </message>
    <message>
        <location filename="../ui/principal.ui" line="525"/>
        <source>Caducidad</source>
        <translation>Caducitat</translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../ui/principal.ui" line="145"/>
        <source>Contraseña</source>
        <translation>Paraula de pas</translation>
    </message>
    <message>
        <location filename="../ui/principal.ui" line="309"/>
        <source>Importar fichero</source>
        <translation>Importar fichèr</translation>
    </message>
    <message>
        <location filename="../ui/principal.ui" line="681"/>
        <source>Formatear</source>
        <translation>Formatar</translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../ui/principal.ui" line="615"/>
        <source>Contraseña:</source>
        <translation>Paraula de pas:</translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../ui/principal.ui" line="691"/>
        <source>Información</source>
        <translation>Informacion</translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../ui/principal.ui" line="299"/>
        <source>Cambiar contraseña</source>
        <translation>Cambiar paraula de pas</translation>
    </message>
    <message>
        <location filename="../ui/principal.ui" line="22"/>
        <source>Gestor del Clauer</source>
        <translation>Gestor deth Clauer</translation>
    </message>
    <message>
        <location filename="../ui/principal.ui" line="54"/>
        <source>Certificados dentro del clauer</source>
        <translation>Certificada laguens deth Clauer</translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../ui/principal.ui" line="151"/>
        <source>Desde esta pestaña puede cambiar la contraseña de su clauer. Para hacerlo, debe introducir la contraseña actual y a continuación la nueva dos veces.</source>
        <translation>Dempús d&apos;aguesta pestanha podetz cambiar era paraula de pas deth vòste clauer. Entà hè&apos;c, vos cau introdusir era paraula de pas actuau e de contunh era naua dus còps.</translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../ui/principal.ui" line="177"/>
        <source>Introduzca la contraseña actual</source>
        <translation>Introduiu era paraula de pas actuau</translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../ui/principal.ui" line="191"/>
        <source>Contraseña actual del Clauer</source>
        <translation>Paraula de pas actuau deth Clauer</translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../ui/principal.ui" line="219"/>
        <source>Introduzca la nueva contraseña</source>
        <translation>Introduiu era naua paraula de pas</translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../ui/principal.ui" line="230"/>
        <source>Nueva contraseña:</source>
        <translation>Naua paraula de pas:</translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../ui/principal.ui" line="240"/>
        <source>Nueva contraseña del Clauer</source>
        <translation>Naua paraula de pas deth clauer</translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../ui/principal.ui" line="266"/>
        <source>Confirmación de la nueva contraseña:</source>
        <translation>Confirmacion dera naua paraula de pas:</translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../ui/principal.ui" line="315"/>
        <source>Desde esta pestaña puede importar dentro del clauer cualquier certificado en formato fichero (extensiones .pfx y .p12).</source>
        <translation>Dempús d&apos;aguesta pestanha podetz importar laguens deth clauer quin certificat que sigue eth format fichèr (estendudes .pfx y .P12).</translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../ui/principal.ui" line="341"/>
        <source>Ubicación del fichero a importar dentro del clauer</source>
        <translation>Ubicacion deth fichèr a importar laguens deth Clauer</translation>
    </message>
    <message>
        <location filename="../ui/principal.ui" line="355"/>
        <source>Buscar el certficiado en el sistema de ficheros</source>
        <translation>Cercar eth certificat ath sistèma de fichèrs</translation>
    </message>
    <message>
        <location filename="../ui/principal.ui" line="358"/>
        <source>Examinar</source>
        <translation>Examinar</translation>
    </message>
    <message>
        <location filename="../ui/principal.ui" line="371"/>
        <source>Ruta completa del fichero que contiene el certificado digital a importar dentro del clauer</source>
        <translation>Rota complèta deth fichèr que conten eth certificat digitau a importar laguens deth clauer</translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../ui/principal.ui" line="410"/>
        <source>Contraseña que protege el fichero que desea importar al clauer</source>
        <translation>Paraula de pas que protegís eth fichèr que voletz importar ath clauer</translation>
    </message>
    <message>
        <location filename="../ui/principal.ui" line="443"/>
        <source>Importar</source>
        <translation>Importar</translation>
    </message>
    <message>
        <location filename="../ui/principal.ui" line="453"/>
        <source>Importar desde Internet Explorer</source>
        <translation>Importar dempús d&apos;Internet Explorer</translation>
    </message>
    <message>
        <location filename="../ui/principal.ui" line="548"/>
        <source>Mover dentro del clauer</source>
        <translation>Mòir laguens deth clauer</translation>
    </message>
    <message>
        <location filename="../ui/principal.ui" line="555"/>
        <source>Copiar dentro del clauer</source>
        <translation>Copiar laguens deth clauer</translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../ui/principal.ui" line="604"/>
        <source>Introduzca la contraseña que protegerá al clauer idCAT</source>
        <translation>Introduiu era paraula de pas que protegirà eth clauer idCAT</translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../ui/principal.ui" line="625"/>
        <source>Contraseña que tendrá el clauer una vez formateado</source>
        <translation>Paraula de pas qu&apos;aurà eth clauer un còp formatat</translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../ui/principal.ui" line="638"/>
        <source>Confirmación contraseña:</source>
        <translation>Confirmacion paraula de pas:</translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../ui/principal.ui" line="648"/>
        <source>Confirmación de la contraseña</source>
        <translation>Confirmacion dera paraula de pas</translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../ui/principal.ui" line="459"/>
        <source>Seleccione el certificado digital que desee copiar o mover desde el almacén de certificados de Internet Explorer hacia la zona criptográfica del clauer:</source>
        <translation>Seleccionatz eth certificat digitau que voletz copiar o mòir dempús deth magasèm de certificadi d&apos;Internet Explorer cap ara zòna criptogràfica deth clauer:</translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../ui/principal.ui" line="389"/>
        <source>Contraseña que protege al fichero</source>
        <translation>Paraula de pas que protegís eth fichèr</translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../ui/principal.ui" line="253"/>
        <source>Confirmación de la nueva contraseña del Clauer</source>
        <translation>Confirmacion dera naua paraula de pas deth clauer</translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../ui/principal.ui" line="520"/>
        <source>Entidad de certificación</source>
        <translation>Entitat de certificacion</translation>
    </message>
</context>
<context>
    <name>InfoCertificado</name>
    <message encoding="UTF-8">
        <location filename="../ui/infoCertificado.ui" line="16"/>
        <source>Información del certificado</source>
        <translation>Informacion deth certificat</translation>
    </message>
    <message>
        <location filename="../ui/infoCertificado.ui" line="30"/>
        <source>Titular:</source>
        <translation>Titular:</translation>
    </message>
    <message>
        <location filename="../ui/infoCertificado.ui" line="70"/>
        <source>Entidad emisora:</source>
        <translation>Entitat emisora:</translation>
    </message>
    <message>
        <location filename="../ui/infoCertificado.ui" line="82"/>
        <source>Validez del certificado</source>
        <translation>Validesa deth certificat</translation>
    </message>
    <message>
        <location filename="../ui/infoCertificado.ui" line="113"/>
        <source>Hasta:</source>
        <translation>Enquia:</translation>
    </message>
    <message encoding="UTF-8">
        <location filename="../ui/infoCertificado.ui" line="123"/>
        <source>Válido desde:</source>
        <translation>Valid dempús de:</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../auxiliares.h" line="35"/>
        <source>Gestor del Clauer</source>
        <translation>Gestor deth Clauer</translation>
    </message>
    <message>
        <location filename="../auxiliares.cpp" line="43"/>
        <source>Imposible comunicar con el clauer. &#xbf;Tenemos privilegios suficientes?</source>
        <translation>Impossible comunicar damb eth Clauer. Auetz privilègis sufisents?</translation>
    </message>
    <message>
        <location filename="../auxiliares.cpp" line="80"/>
        <source>Hay m&#xe1;s de un Stick USB insertado en el sistema
Por favor, pulse el bot&#xf3;n Aceptar y deje insertado
&#xfa;nicamente el dispostivo que desee inicializar.</source>
        <translation type="unfinished">I a mès d&apos;un Stick USB inserit ath sistèma
 Se vos platz, premetz eth boton Acceptar y deishatz inserit unicaments eth dispostiu que voletz inicialitzar.</translation>
    </message>
    <message>
        <location filename="../auxiliares.cpp" line="99"/>
        <source>No hay ning&#xfa;n Stick USB insertado en el sistema
Por favor, pulse el bot&#xf3;n Aceptar e inserte el dispositivo
que desee inicializar.</source>
        <translation type="unfinished">Non i a cap Stick USB inserit ath sistèma.
Se vos platz, premetz eth boton Acceptar e inseriu eth dispositiu que voletz inicialitzar.</translation>
    </message>
    <message>
        <location filename="../auxiliares.cpp" line="164"/>
        <source>La nueva contrase&#xf1;a y su confirmaci&#xf3;n deben coincidir.</source>
        <translation>Era naua paraula de pas e era sua confirmacion les cau coïncidir.</translation>
    </message>
    <message>
        <location filename="../auxiliares.cpp" line="184"/>
        <source>La contrase&#xf1;a es demasiado corta, ha de tener un m&#xed;nimo de 4 caracteres.</source>
        <translation>Era paraula de pas ei massa cuerta, li cau auer aumens 4 caracters.</translation>
    </message>
    <message>
        <location filename="../auxiliares.cpp" line="193"/>
        <source>La nueva contrase&#xf1;a es demasiado simple.</source>
        <translation>Era naua paraula de pas ei massa simpla.</translation>
    </message>
    <message>
        <location filename="../src/dialogimpl.cpp" line="500"/>
        <source>Contrase&#xf1;a del certificado incorrecta.</source>
        <translation>Paraula de pas deth certificat incorrècta.</translation>
    </message>
    <message>
        <location filename="../src/dialogimpl.cpp" line="94"/>
        <source>Introduzca la contrase&#xf1;a del clauer:</source>
        <translation>Introdusitz era paraula de pas deth clauer:</translation>
    </message>
    <message>
        <location filename="../src/dialogimpl.cpp" line="111"/>
        <source>No se ha podido completar la operaci&#xf3;n. &#xbf;Es correcta la contrase&#xf1;a del clauer que ha introducido?</source>
        <translation>Non s&apos;a pogut completar era operacion. Ei corrècta era paraula de pas deth Clauer qu&apos;auetz introdusit?</translation>
    </message>
    <message>
        <location filename="../auxiliares.cpp" line="173"/>
        <source>La contrase&#xf1;a contiene alg&#xfa;n car&#xe1;cter no permitido.</source>
        <translation type="unfinished">La paraula de pas conté algun caràcter no permès.</translation>
    </message>
</context>
</TS>
