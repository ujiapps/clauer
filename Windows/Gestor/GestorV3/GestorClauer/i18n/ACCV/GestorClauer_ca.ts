<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="es_ES">
<context>
    <name>Dialog</name>
    <message>
        <source>Dialog</source>
        <translation>Diàlog</translation>
    </message>
    <message>
        <source>Remove this widget and insert your own widgets</source>
        <translation>Esborra el widget i insereix els teus propis widgets</translation>
    </message>
</context>
<context>
    <name>DialogImpl</name>
    <message>
        <source>Titular del certificado</source>
        <translation>Titular del certificat</translation>
    </message>
    <message>
        <source>Organización que ha emitido el certificado</source>
        <translation>Organització que ha emès el certificat</translation>
    </message>
    <message>
        <source>Fecha de caducidad del certificado</source>
        <translation>Data de caducitat del certificat</translation>
    </message>
    <message>
        <source>Visualizar información del certificado</source>
        <translation>Visualitzar informació del certificat</translation>
    </message>
    <message>
        <source>Exportar el certificado a un fichero PKCS#12</source>
        <translation>Exportar el certificat a un fitxer PKCS #12</translation>
    </message>
    <message>
        <source>Exportar el certificado al Internet Explorer</source>
        <translation>Exportar el certificat a l&apos;Internet Explorer</translation>
    </message>
    <message>
        <source>Elimina el certificado del clauer</source>
        <translation>Elimina el certificat de la ClauACCV</translation>
    </message>
    <message>
        <source>Seleccione archivo en donde se exportará el certificado</source>
        <translation>Seleccioni l&apos;arxiu on s&apos;exportarà el certificat</translation>
    </message>
    <message>
        <source>Contenedor criptográfico PKCS#12 (*.p12)</source>
        <translation>Contenidor criptogràfic PKCS#12 (*.p12) </translation>
    </message>
    <message>
        <source>El certificado se ha exportado correctamente a la ruta: %1.</source>
        <translation>El certificat s&apos;ha exportat correctament a la ruta: %1.</translation>
    </message>
    <message>
        <source>No se pudo exportar el certificado.</source>
        <translation>No es va poder exportar el certificat.</translation>
    </message>
    <message>
        <source>El certificado se ha exportado correctamente.</source>
        <translation>El certificat s&apos;ha exportat correctament.</translation>
    </message>
    <message>
        <source>Se encuentra a punto de eliminar permanentemente el certificado del clauer.
¿Desea continuar?</source>
        <translation>Es troba a punt d&apos;eliminar permanentment el certificat de la ClauACCV.
 Desitja continuar?</translation>
    </message>
    <message>
        <source>El certificado se ha eliminado correctamente.</source>
        <translation>El certificat s&apos;ha eliminat correctament.</translation>
    </message>
    <message>
        <source>No se ha podido eliminar el certificado.</source>
        <translation>No s&apos;ha pogut eliminar el certificat.</translation>
    </message>
    <message>
        <source>La contraseña del clauer se ha cambiado correctamente.</source>
        <translation>La contrasenya de la ClauACCV s&apos;ha canviat correctament.</translation>
    </message>
    <message>
        <source>Ha ocurrido un error grave cambiando la contraseña de su clauer. Deberá formatearlo de nuevo.</source>
        <translation>Ha ocorregut un error greu canviant la contrasenya de la seva ClauACCV. Haurà de formatar-lo de nou.</translation>
    </message>
    <message>
        <source>No se ha podido cambiar la contraseña del clauer. ¿Es correcta la contraseña actual?</source>
        <translation>No s&apos;ha pogut canviar la contrasenya de la seva ClauACCV. És correcta la contrasenya actual?</translation>
    </message>
    <message>
        <source>Seleccione el archivo que contenga el certificado a importar</source>
        <translation>Seleccioni l&apos;arxiu que contingui el certificat a importar</translation>
    </message>
    <message>
        <source>Certificados digitales (*.p12; *.pfx)</source>
        <translation>Certificats digitals (*.p12;*.pfx)</translation>
    </message>
    <message>
        <source>El certificado se ha importado correctamente dentro del clauer.</source>
        <translation>El certificat s&apos;ha importat correctament dins de la ClauACCV.</translation>
    </message>
    <message>
        <source>El certificado se ha movido correctamente.</source>
        <translation>El certificat s&apos;ha mogut correctament.</translation>
    </message>
    <message>
        <source>El certificado se ha copiado correctamente.</source>
        <translation>El certificat s&apos;ha copiat correctament.</translation>
    </message>
    <message>
        <source>La operación no se ha podido realizar porque el certificado elegido no es exportable.</source>
        <translation>L&apos;operació no s&apos;ha pogut realitzar perquè el certificat triat no és exportable.</translation>
    </message>
    <message>
        <source>Todos los datos del sitck USB se borrarán. ¿Está seguro que desea continuar?</source>
        <translation>Tots les dades de l&apos;estic USB s&apos;esborraran. Està segur que desitja continuar? </translation>
    </message>
    <message>
        <source>A continuación se va a formatear la zona criptográfica, todos los certificados que contenga el clauer serán eliminados.
¿Está seguro de que desea formatear la zona criptográfica?</source>
        <translation>A continuació es va a formatar la zona criptogràfica, tots els certificats que contingui la ClauACCV seran eliminats.&lt;br&gt;Estàs segur que desitges formatar la zona criptogràfica? </translation>
    </message>
    <message>
        <source>El clauer se ha formateado correctamente.</source>
        <translation>La ClauACCV s&apos;ha formatat correctament.</translation>
    </message>
    <message>
        <source>No se ha podido formatear el Clauer.</source>
        <translation>No s&apos;ha pogut formatar la ClauACCV.</translation>
    </message>
    <message>
        <source>Hay más de un Stick USB insertado en el sistema
Por favor, deje insertado
únicamente el dispostivo que desee inicializar y pulse el botón Aceptar.</source>
        <translation>Hi ha més d&apos;un estic USB inserit en el sistema,
per favor, deixi inserit
únicament el dispositiu que desitgi inicialitzar i premi el botó Acceptar. </translation>
    </message>
</context>
<context>
    <name>DialogPrincipal</name>
    <message>
        <source>Gestor de la ClauACCV</source>
        <translation></translation>
    </message>
    <message>
        <source>Certificados en la ClauACCV</source>
        <translation>Certificats en la ClauACCV</translation>
    </message>
    <message utf8="true">
        <source>Desde esta pestaña puede visualizar los certificados digitales que contiene su ClauACCV.&lt;br&gt;Pulsando los iconos a la derecha de cada certificado puede obtener información de cada uno de ellos, exportarlos a un fichero, exportarlos a Internet Explorer o Eliminarlos.</source>
        <translation>Des d&apos;aquesta pestanya podeu visualitzar els certificats digitals que conté la vostra ClauACCV.&lt;br&gt;Polsant les icones a la dreta de cada certificat podeu obtindre informació de cada un d&apos;aquests, exportar-los a un fitxer, exportar-los a Internet Explorer o &lt;b&gt;eliminar-los&lt;/b&gt;.</translation>
    </message>
    <message>
        <source>Nombre del certificado</source>
        <translation>Nom del certificat</translation>
    </message>
    <message utf8="true">
        <source>Entidad de certificación</source>
        <translation>Autoritat de Certificació</translation>
    </message>
    <message>
        <source>Caducidad</source>
        <translation>Caducitat</translation>
    </message>
    <message utf8="true">
        <source>Contraseña</source>
        <translation>Contrasenya</translation>
    </message>
    <message utf8="true">
        <source>Desde esta pestaña puede cambiar la contraseña de la clauACCV. Para ello debe introducir la contraseña actual y a continuación la nueva dos veces.</source>
        <translation>Des d&apos;aquesta pestanya podeu canviar la contrasenya de la ClauACCV. Per a això heu d&apos;introduir la contrasenya actual i tot seguit la nova dues vegades.</translation>
    </message>
    <message utf8="true">
        <source>Introduzca la contraseña actual</source>
        <translation>Introdueix la contrasenya actual</translation>
    </message>
    <message utf8="true">
        <source>Contraseña actual del Clauer</source>
        <translation>Contrasenya actual de la ClauACCV</translation>
    </message>
    <message utf8="true">
        <source>Contraseña:</source>
        <translation>Contrasenya:</translation>
    </message>
    <message utf8="true">
        <source>Introduzca la nueva contraseña</source>
        <translation>Introdueix la nova contrasenya</translation>
    </message>
    <message utf8="true">
        <source>Nueva contraseña:</source>
        <translation>Nova contrasenya:</translation>
    </message>
    <message utf8="true">
        <source>Nueva contraseña del Clauer</source>
        <translation>Nova contrasenya de la ClauACCV</translation>
    </message>
    <message utf8="true">
        <source>Confirmación de la nueva contraseña del Clauer</source>
        <translation>Confirmació de la nova contrasenya de la ClauACCV</translation>
    </message>
    <message utf8="true">
        <source>Confirmación de la nueva contraseña:</source>
        <translation>Confirmació de la nova contrasenya:</translation>
    </message>
    <message utf8="true">
        <source>Cambiar contraseña</source>
        <translation>Canviar contrasenya</translation>
    </message>
    <message>
        <source>Importar fichero</source>
        <translation>Importar des de fitxer</translation>
    </message>
    <message utf8="true">
        <source>Desde esta pestaña puede importar dentro de la clauACCV cualquier certificado en fichero (extensiones .pfx y .p12). Para ello debe introducir la contraseña que protege el fichero.</source>
        <translation>Des d&apos;aquesta pestanya podeu importar dins de la ClauACCV qualsevol certificat digital en fitxer (extensions .pfx i .p12). Per a això heu d&apos;introduir la contrasenya que protegeix el fitxer.</translation>
    </message>
    <message utf8="true">
        <source>Ubicación del fichero:</source>
        <translation>Ubicació del fitxer:</translation>
    </message>
    <message>
        <source>Buscar el certficiado en el sistema de ficheros</source>
        <translation>Buscar el certificat en el sistema de fitxers</translation>
    </message>
    <message>
        <source>Examinar</source>
        <translation>Examinar</translation>
    </message>
    <message>
        <source>Ruta completa del fichero que contiene el certificado digital a importar dentro del clauer</source>
        <translation>Ruta completa del fitxer que conté el certificat digital a importar dins de la ClauACCV</translation>
    </message>
    <message utf8="true">
        <source>Contraseña del fichero:</source>
        <translation>Contrasenya del fitxer:</translation>
    </message>
    <message utf8="true">
        <source>Contraseña que protege el fichero que desea importar al clauer</source>
        <translation>Contrasenya que protegeix el fitxer que vol importar des de la ClauACCV</translation>
    </message>
    <message>
        <source>Importar</source>
        <translation>Importar</translation>
    </message>
    <message>
        <source>Importar desde Internet Explorer</source>
        <translation>Importar des d&apos;Internet Explorer</translation>
    </message>
    <message utf8="true">
        <source>Desde esta pestaña puede importar dentro de la ClauACCV cualquier certificado registrado en el almacén de certificados digitales de Internet Explorer.&lt;br&gt;&lt;br&gt;Existen dos alternativas, Mover el certificado definitivamente a la ClauACCV o Copiar el certificado en la ClauACCV.</source>
        <translation>Des d&apos;aquesta pestanya podeu importar dins de la ClauACCV qualsevol certificat registrat en el magatzem de certificats digitals &lt;b&gt;d&apos;Internet Explorer&lt;/b&gt;.&lt;br&gt;&lt;br&gt;Hi ha dues alternatives, moure el certificat definitivament a la ClauACCV o copiar el certificat en la ClauACCV.</translation>
    </message>
    <message utf8="true">
        <source>Desde esta pestaña puede dar formato a la partición criptográfica de la ClauACCV y definir una nueva contraseña para la misma.&lt;br&gt;&lt;br&gt;&lt;b&gt;ATENCIÓN:&lt;/b&gt; Debe tener precaución pues la operación eliminará los certificados digitales que actualmente contenga la ClauACCV. Sólo podrá volver a importarlos a la ClauACCV si dispone de ellos en un fichero.&lt;br&gt;&lt;br&gt;El formateo no afectará a los ficheros que se encuentran en la partición de memoria estándar de la ClauACCV.</source>
        <translation>Des d&apos;aquesta pestanya podeu donar format a la ClauACCV i definir una nova contrasenya per a aquesta.&lt;br&gt;&lt;br&gt;&lt;b&gt;ATENCIÓ&lt;/b&gt;: Heu de tindre precaució perquè l&apos;operació eliminarà els certificats digitals que actualment continga la ClauACCV. Només podreu tornar a importar-los a la ClauACCV si en disposeu en un fitxer.&lt;br&gt;&lt;br&gt;El formatatge nomes afectarà els fitxers que es troben en la partició de memòria estàndard de la ClauACCV si aquesta no ha sigut mai formatada com a ClauACCV.</translation>
    </message>
    <message>
        <source>Mover dentro de la ClauACCV</source>
        <translation>Moure dins de la ClauACCV</translation>
    </message>
    <message>
        <source>Copiar dentro de la ClauACCV</source>
        <translation>Copiar dins de la ClauACCV</translation>
    </message>
    <message>
        <source>Formatear</source>
        <translation>Formatar</translation>
    </message>
    <message utf8="true">
        <source>Introduzca la contraseña que protegerá a la ClauACCV:</source>
        <translation>Introdueix la contrasenya que protegirà a la ClauACCV:</translation>
    </message>
    <message utf8="true">
        <source>Contraseña que tendrá el clauer una vez formateado</source>
        <translation>Contrasenya que tindrà la ClauACCV una volta formatat</translation>
    </message>
    <message utf8="true">
        <source>Confirmación contraseña:</source>
        <translation>Confirmació contrasenya:</translation>
    </message>
    <message utf8="true">
        <source>Confirmación de la contraseña</source>
        <translation>Confirmació de la contrasenya</translation>
    </message>
    <message utf8="true">
        <source>Información</source>
        <translation>Informació</translation>
    </message>
    <message utf8="true">
        <source>Bienvenido al Gestor de la ClauACCV. A través de este programa puede gestionar sus certificados en memoria USB. Para ello seleccione la función deseada entre las pestañas superiores.</source>
        <translation>Benvingut al Gestor de la ClauACCV. A través d&apos;este programa pot gestionar els seus certificats en memòria USB. Per a això seleccione la funció desitjada entre les pestanyes superiors.</translation>
    </message>
</context>
<context>
    <name>InfoCertificado</name>
    <message utf8="true">
        <source>Información del certificado</source>
        <translation>Informació del certificat</translation>
    </message>
    <message>
        <source>Titular:</source>
        <translation>Titular:</translation>
    </message>
    <message>
        <source>Entidad emisora:</source>
        <translation>Entitat emissora:</translation>
    </message>
    <message>
        <source>Validez del certificado</source>
        <translation>Validesa del certificat</translation>
    </message>
    <message>
        <source>Hasta:</source>
        <translation>Fins a:</translation>
    </message>
    <message utf8="true">
        <source>Válido desde:</source>
        <translation>Vàlid des de:</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <source>Imposible comunicar con el clauer. ¿Tenemos privilegios suficientes?</source>
        <translation type="unfinished">Impossible comunicar amb la ClauACCV. Tenim privilegis suficients?</translation>
    </message>
    <message>
        <source>Hay más de un Stick USB insertado en el sistema
Por favor, pulse el botón Aceptar y deje insertado
únicamente el dispostivo que desee inicializar.</source>
        <translation>Hi ha més d&apos;un Stick USB inserit en el sistema,
per favor, premi el botó Acceptar i deixi inserit
únicament el dispositiu que desitgi inicialitzar. </translation>
    </message>
    <message>
        <source>No hay ningún Stick USB insertado en el sistema
Por favor, pulse el botón Aceptar e inserte el dispositivo
que desee inicializar.</source>
        <translation>No hi ha cap Stick USB inserit en el sistema,
per favor, premi el botó Acceptar i insereixi el dispositiu
que desitgi inicialitzar.</translation>
    </message>
    <message>
        <source>La nueva contraseña y su confirmación deben coincidir.</source>
        <translation>La nova contrasenya i la seva confirmació han de coincidir.</translation>
    </message>
    <message>
        <source>La contraseña contiene algún carácter no permitido.</source>
        <translation>La contrasenya conté algun caracter no permès.</translation>
    </message>
    <message>
        <source>La contraseña es demasiado corta, ha de tener un mínimo de 4 caracteres.</source>
        <translation>La contrasenya es molt curta, ha de tindre com a mínim 4 caràcters.</translation>
    </message>
    <message>
        <source>La nueva contraseña es demasiado simple.</source>
        <translation>La nova contrasenya es molt simple.</translation>
    </message>
    <message>
        <source>Gestor del Clauer</source>
        <translation>Gestor de la ClauACCV</translation>
    </message>
    <message>
        <source>Introduzca la contraseña del clauer:</source>
        <translation>Introdueix la contrasenya de la ClauACCV:</translation>
    </message>
    <message>
        <source>No se ha podido completar la operación. ¿Es correcta la contraseña del clauer que ha introducido?</source>
        <translation>No s&apos;ha pogut completar l&apos;operació. Es correcta la contrasenya de la ClauACCV introduïda?</translation>
    </message>
    <message>
        <source>Contraseña del certificado incorrecta.</source>
        <translation>Contrasenya del certificat incorrecta.</translation>
    </message>
    <message>
        <source>Imposible comunicar con #CLAUERNAME. ¿Tenemos privilegios suficientes?</source>
        <translation type="obsolete">Impossible comunicar-se amb la ClauACCV. Tens privilegis suficients?</translation>
    </message>
</context>
</TS>
