<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="es_ES">
<context>
    <name>Dialog</name>
    <message>
        <source>Dialog</source>
        <translation>Diálogo</translation>
    </message>
    <message>
        <source>Remove this widget and insert your own widgets</source>
        <translation>Elimina este widget e inserta tus propios widgets</translation>
    </message>
</context>
<context>
    <name>DialogImpl</name>
    <message>
        <source>Titular del certificado</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Organización que ha emitido el certificado</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Fecha de caducidad del certificado</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Visualizar información del certificado</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Exportar el certificado a un fichero PKCS#12</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Exportar el certificado al Internet Explorer</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Elimina el certificado del clauer</source>
        <translation>Elimina el certificado de la ClauACCV</translation>
    </message>
    <message>
        <source>Seleccione archivo en donde se exportará el certificado</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Contenedor criptográfico PKCS#12 (*.p12)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>El certificado se ha exportado correctamente a la ruta: %1.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>No se pudo exportar el certificado.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>El certificado se ha exportado correctamente.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Se encuentra a punto de eliminar permanentemente el certificado del clauer.
¿Desea continuar?</source>
        <translation>Se encuentra a punto de eliminar permanentemente el certificado seleccionado de la ClauACCV y sólo podrá volver a importarlo si dispone de él en un fichero. &lt;br&gt;¿Desea continuar?</translation>
    </message>
    <message>
        <source>El certificado se ha eliminado correctamente.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>No se ha podido eliminar el certificado.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>La contraseña del clauer se ha cambiado correctamente.</source>
        <translation>La contraseña de la ClauACCV se ha cambiado correctamente.</translation>
    </message>
    <message>
        <source>Ha ocurrido un error grave cambiando la contraseña de su clauer. Deberá formatearlo de nuevo.</source>
        <translation>Ha ocurrido un error grave cambiando la contraseña de su ClauACCV. Deberá formatearlo de nuevo.</translation>
    </message>
    <message>
        <source>No se ha podido cambiar la contraseña del clauer. ¿Es correcta la contraseña actual?</source>
        <translation>No se ha podido cambiar la contraseña de su ClauACCV. ¿Es correcta la contraseña actual?</translation>
    </message>
    <message>
        <source>Seleccione el archivo que contenga el certificado a importar</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Certificados digitales (*.p12; *.pfx)</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>El certificado se ha importado correctamente dentro del clauer.</source>
        <translation>El certificado se ha importado correctamente dentro de la ClauACCV.</translation>
    </message>
    <message>
        <source>El certificado se ha movido correctamente.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>El certificado se ha copiado correctamente.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>La operación no se ha podido realizar porque el certificado elegido no es exportable.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Todos los datos del sitck USB se borrarán. ¿Está seguro que desea continuar?</source>
        <translation>Todos los datos del stick USB se borrarán. ¿Está seguro que desea continuar?</translation>
    </message>
    <message>
        <source>A continuación se va a formatear la zona criptográfica, todos los certificados que contenga el clauer serán eliminados.
¿Está seguro de que desea formatear la zona criptográfica?</source>
        <translation>A continuación se va a formatear la zona criptográfica, todos los certificados que contenga la ClauACCV serán eliminados.&lt;br&gt;¿Está seguro que desea formatear la zona criptográfica?</translation>
    </message>
    <message>
        <source>El clauer se ha formateado correctamente.</source>
        <translation>La ClauACCV se ha formateado correctamente.</translation>
    </message>
    <message>
        <source>No se ha podido formatear el Clauer.</source>
        <translation>No se ha podido formatear la ClauACCV.</translation>
    </message>
    <message>
        <source>Hay más de un Stick USB insertado en el sistema
Por favor, deje insertado
únicamente el dispostivo que desee inicializar y pulse el botón Aceptar.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>DialogPrincipal</name>
    <message>
        <source>Gestor de la ClauACCV</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Certificados en la ClauACCV</source>
        <translation type="unfinished"></translation>
    </message>
    <message utf8="true">
        <source>Desde esta pestaña puede visualizar los certificados digitales que contiene su ClauACCV.&lt;br&gt;Pulsando los iconos a la derecha de cada certificado puede obtener información de cada uno de ellos, exportarlos a un fichero, exportarlos a Internet Explorer o Eliminarlos.</source>
        <translation>Desde esta pestaña puede visualizar los certificados digitales que contiene su ClauACCV. &lt;br&gt;Pulsando los iconos a la derecha de cada certificado puede obtener información de cada uno de ellos, exportarlos a un fichero, exportarlos a Internet Explorer o &lt;b&gt;eliminarlos&lt;/b&gt;.</translation>
    </message>
    <message>
        <source>Nombre del certificado</source>
        <translation type="unfinished"></translation>
    </message>
    <message utf8="true">
        <source>Entidad de certificación</source>
        <translation>Autoridad de Certificación</translation>
    </message>
    <message>
        <source>Caducidad</source>
        <translation type="unfinished"></translation>
    </message>
    <message utf8="true">
        <source>Contraseña</source>
        <translation type="unfinished"></translation>
    </message>
    <message utf8="true">
        <source>Desde esta pestaña puede cambiar la contraseña de la clauACCV. Para ello debe introducir la contraseña actual y a continuación la nueva dos veces.</source>
        <translation>Desde esta pestaña puede cambiar la contraseña de la ClauACCV. Para ello debe introducir la contraseña actual y a continuación la nueva dos veces.</translation>
    </message>
    <message utf8="true">
        <source>Introduzca la contraseña actual</source>
        <translation type="unfinished"></translation>
    </message>
    <message utf8="true">
        <source>Contraseña actual del Clauer</source>
        <translation>Contraseña actual de la ClauACCV</translation>
    </message>
    <message utf8="true">
        <source>Contraseña:</source>
        <translation type="unfinished"></translation>
    </message>
    <message utf8="true">
        <source>Introduzca la nueva contraseña</source>
        <translation type="unfinished"></translation>
    </message>
    <message utf8="true">
        <source>Nueva contraseña:</source>
        <translation type="unfinished"></translation>
    </message>
    <message utf8="true">
        <source>Nueva contraseña del Clauer</source>
        <translation>Nueva contraseña de la ClauACCV</translation>
    </message>
    <message utf8="true">
        <source>Confirmación de la nueva contraseña del Clauer</source>
        <translation>Confirmación de la nueva contraseña de la ClauACCV</translation>
    </message>
    <message utf8="true">
        <source>Confirmación de la nueva contraseña:</source>
        <translation type="unfinished"></translation>
    </message>
    <message utf8="true">
        <source>Cambiar contraseña</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Importar fichero</source>
        <translation type="unfinished"></translation>
    </message>
    <message utf8="true">
        <source>Desde esta pestaña puede importar dentro de la clauACCV cualquier certificado en fichero (extensiones .pfx y .p12). Para ello debe introducir la contraseña que protege el fichero.</source>
        <translation>Desde esta pestaña puede importar dentro de la ClauACCV cualquier certificado en fichero (extensiones .pfx y .p12). Para ello debe introducir la contraseña que protege el fichero.</translation>
    </message>
    <message utf8="true">
        <source>Ubicación del fichero:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Buscar el certficiado en el sistema de ficheros</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Examinar</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Ruta completa del fichero que contiene el certificado digital a importar dentro del clauer</source>
        <translation>Ruta completa del fichero que contiene el certificado digital a importar dentro de la ClauACCV</translation>
    </message>
    <message utf8="true">
        <source>Contraseña del fichero:</source>
        <translation type="unfinished"></translation>
    </message>
    <message utf8="true">
        <source>Contraseña que protege el fichero que desea importar al clauer</source>
        <translation>Contraseña que protege el fichero que desea importar a la ClauACCV</translation>
    </message>
    <message>
        <source>Importar</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Importar desde Internet Explorer</source>
        <translation type="unfinished"></translation>
    </message>
    <message utf8="true">
        <source>Desde esta pestaña puede importar dentro de la ClauACCV cualquier certificado registrado en el almacén de certificados digitales de Internet Explorer.&lt;br&gt;&lt;br&gt;Existen dos alternativas, Mover el certificado definitivamente a la ClauACCV o Copiar el certificado en la ClauACCV.</source>
        <translation>Desde esta pestaña puede importar dentro de la ClauACCV cualquier certificado registrado en el almacén de certificados digitales de &lt;b&gt;Internet Explorer&lt;/b&gt;.&lt;br&gt;&lt;br&gt;Existen dos alternativas, Mover el certificado definitivamente a la ClauACCV o Copiar el certificado en la ClauACCV.</translation>
    </message>
    <message>
        <source>Mover dentro de la ClauACCV</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Copiar dentro de la ClauACCV</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Formatear</source>
        <translation type="unfinished"></translation>
    </message>
    <message utf8="true">
        <source>Desde esta pestaña puede dar formato a la partición criptográfica de la ClauACCV y definir una nueva contraseña para la misma.&lt;br&gt;&lt;br&gt;&lt;b&gt;ATENCIÓN:&lt;/b&gt; Debe tener precaución pues la operación eliminará los certificados digitales que actualmente contenga la ClauACCV. Sólo podrá volver a importarlos a la ClauACCV si dispone de ellos en un fichero.&lt;br&gt;&lt;br&gt;El formateo no afectará a los ficheros que se encuentran en la partición de memoria estándar de la ClauACCV.</source>
        <translation>Desde esta pestaña puede dar formato a la ClauACCV y definir una nueva contraseña para la misma.&lt;br&gt;&lt;br&gt;&lt;b&gt;ATENCIÓN:&lt;/b&gt; Debe tener precaución pues la operación eliminará los certificados digitales que actualmente contenga la ClauACCV. Sólo podrá volver a importarlos a la ClauACCV si dispone de ellos en un fichero.&lt;br&gt;&lt;br&gt;El formateo solo afectará a los ficheros que se encuentran en la partición de memoria estándar de la ClauACCV si el dispositvo USB no ha sido formateado nunca como ClauACCV.</translation>
    </message>
    <message utf8="true">
        <source>Introduzca la contraseña que protegerá a la ClauACCV:</source>
        <translation type="unfinished"></translation>
    </message>
    <message utf8="true">
        <source>Contraseña que tendrá el clauer una vez formateado</source>
        <translation>Contraseña que tendrá la ClauACCV una vez formateada</translation>
    </message>
    <message utf8="true">
        <source>Confirmación contraseña:</source>
        <translation type="unfinished"></translation>
    </message>
    <message utf8="true">
        <source>Confirmación de la contraseña</source>
        <translation type="unfinished"></translation>
    </message>
    <message utf8="true">
        <source>Información</source>
        <translation type="unfinished"></translation>
    </message>
    <message utf8="true">
        <source>Bienvenido al Gestor de la ClauACCV. A través de este programa puede gestionar sus certificados en memoria USB. Para ello seleccione la función deseada entre las pestañas superiores.</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>InfoCertificado</name>
    <message utf8="true">
        <source>Información del certificado</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Titular:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Entidad emisora:</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Validez del certificado</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Hasta:</source>
        <translation type="unfinished"></translation>
    </message>
    <message utf8="true">
        <source>Válido desde:</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <source>Gestor del Clauer</source>
        <translation>Gestor de la ClauACCV</translation>
    </message>
    <message>
        <source>Hay más de un Stick USB insertado en el sistema
Por favor, pulse el botón Aceptar y deje insertado
únicamente el dispostivo que desee inicializar.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>No hay ningún Stick USB insertado en el sistema
Por favor, pulse el botón Aceptar e inserte el dispositivo
que desee inicializar.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>La nueva contraseña y su confirmación deben coincidir.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>La contraseña contiene algún carácter no permitido.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>La contraseña es demasiado corta, ha de tener un mínimo de 4 caracteres.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>La nueva contraseña es demasiado simple.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Introduzca la contraseña del clauer:</source>
        <translation>Introduzca la contraseña de la ClauACCV:</translation>
    </message>
    <message>
        <source>No se ha podido completar la operación. ¿Es correcta la contraseña del clauer que ha introducido?</source>
        <translation>No se ha podido completar la operación. Reinicie el Gestor de la ClauACCV utilizando la contraseña actual.</translation>
    </message>
    <message>
        <source>Contraseña del certificado incorrecta.</source>
        <translation type="unfinished"></translation>
    </message>
    <message>
        <source>Imposible comunicar con el clauer. ¿Tenemos privilegios suficientes?</source>
        <translation>Imposible comunicar con la ClauACCV. ¿Tenemos privilegios suficientes?</translation>
    </message>
</context>
</TS>
