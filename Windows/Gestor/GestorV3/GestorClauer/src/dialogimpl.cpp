#include "dialogimpl.h"
#include <QInputDialog>
#include <QMessageBox>
#include <QFileDialog>
#include <QHeaderView>
#include "HLClauer.h"
#include "auxiliares.h"
#include "dialogInfoCertificado.h"
#include <algorithm>


#include <iostream>
using namespace std;

//
DialogImpl::DialogImpl( QWidget * parent, Qt::WFlags f) 
	: QDialog(parent, f)
{
	formatearClauerCompleto=false;
	forzarCambioPassword=false;
	
	setupUi(this);
	menuSetExclusivo(Acciones::informacion);
	widgetAcciones->setCurrentIndex(Acciones::informacion);	//por defecto mostramos la pesta�a Informaci�n
	
	//Men� Principal
	connect(widgetAcciones,SIGNAL(currentChanged(int)),this,SLOT(opcionMenuPrincipal(int)));
	
	//Pesta�a Certificados
	connect(tablaCertificadosClauer,SIGNAL(cellClicked(int,int)),this,SLOT(clickVentanaCertificados(int,int)));
	
	//Pesta�a Contrase�a
	connect(botonCambiarPass,SIGNAL(clicked(bool)),this,SLOT(clickCambiarPass(bool)));
	connect(C_passwordACT,SIGNAL(textChanged(const QString &)),this,SLOT(setStatusCambiarPass(const QString &)));
	connect(C_password1,SIGNAL(textChanged(const QString &)),this,SLOT(setStatusCambiarPass(const QString &)));
	connect(C_password2,SIGNAL(textChanged(const QString &)),this,SLOT(setStatusCambiarPass(const QString &)));
	//setPasswordValidator(C_password1,this);
	//setPasswordValidator(C_password2,this);
	
	//Pesta�a Importar Fichero
	connect(botonBuscarFichero,SIGNAL(clicked(bool)),this,SLOT(clickBuscarFichero(bool)));
	connect(botonImportarFichero,SIGNAL(clicked(bool)),this,SLOT(clickImportarFichero(bool)));
	connect(IF_ruta,SIGNAL(textChanged(const QString &)),this,SLOT(setStatusImportarFichero(const QString &)));
	
	//Pesta�a Importar Internet Explorer
	connect(tablaCertificadosIE,SIGNAL(itemSelectionChanged()),this,SLOT(setStatusBotones()));
	connect(botonImportarIEMover,SIGNAL(clicked(bool)),this,SLOT(clickMoverIE(bool)));
	connect(botonImportarIECopiar,SIGNAL(clicked(bool)),this,SLOT(clickCopiarIE(bool)));
	
	//Pesta�a Formatear
	connect(botonFormatear,SIGNAL(clicked(bool)),this,SLOT(clickFormatear(bool)));
	connect(F_password1,SIGNAL(textChanged(const QString &)),this,SLOT(setStatusFormatear(const QString &)));
	connect(F_password2,SIGNAL(textChanged(const QString &)),this,SLOT(setStatusFormatear(const QString &)));
	//setPasswordValidator(F_password1,this);
	//setPasswordValidator(F_password2,this);
	
	//Pesta�a Informaci�n
	connect(botonesInformacion,SIGNAL(clicked(QAbstractButton *)),this,SLOT(clickInfo(QAbstractButton *)));
	
	/*
	switch(localizaClauer(this)) {
		case unico:	//�nico clauer formateado
			break;
		case formatear:	//�nico clauer sin formatear
			formatearClauerCompleto=true;
			widgetAcciones->setCurrentIndex(4);
			menuSetExclusivo(Acciones::formatear);
			break;
		case abortar:	//abortar
			QCoreApplication::exit(3);
			//return;
		default:	//imposible
			bailoutClauer(this);
	}
	*/
	recalcularEstado();
	//bailoutClauer(this);
	//destruyeClauer(0);
	//creaClauer(0,"1234");
}
//

bool DialogImpl::pidePassword(QWidget *parent, bool cachear)
{
	if(!cachear)
		this->password.clear();
	
	if(!this->password.isEmpty())
		return true;
	
	bool res;
    QString nPassword=QInputDialog::getText(parent,
    	tituloDialogos,
    	QObject::tr("Introduzca la contrase�a del clauer:"),
    	QLineEdit::Password,
    	"",
    	&res);
    if(res) {
	    nPassword.truncate(127);
	    this->password=nPassword;
	}
    return res;
}

void DialogImpl::passwordIncorrecta(QWidget *parent)
{
	//if(this->password.isEmpty())
	//	return;
	QMessageBox::critical(parent,
		tituloDialogos,
		QObject::tr("No se ha podido completar la operaci�n. �Es correcta la contrase�a del clauer que ha introducido?"));
	this->password.clear();
}

//Men� Principal
void DialogImpl::opcionMenuPrincipal(int index)
{
	int ancho= 0;
#ifdef DEBUG
	cerr << "Cambio a la pesta�a " << index << endl;
#endif /*DEBUG*/
	switch(index) {
		case Acciones::certificados: /*Certificados*/
			limpiaVentanaCertificados();
			break;
		case Acciones::password:	/*Cambio de password*/
			limpiaVentanaPassword();
			//Alineamos en vertical los campos de las contrase�as
			ancho=std::max(LC_passwordACT->width(),std::max(LC_password1->width(),LC_password2->width()));
			LC_passwordACT->setMinimumWidth(ancho);
			LC_password1->setMinimumWidth(ancho);
			LC_password2->setMinimumWidth(ancho);
			break;
		case Acciones::importarFichero:	/*Importar fichero*/
			limpiaVentanaImportarFichero();
			break;
		case Acciones::importarIE:	/*Importar Internet Explorer*/
			limpiaVentanaImportarIExplorer();
			break;
		case Acciones::formatear:	/*Formatear*/
			limpiaVentanaFormatear();
			break;
		case Acciones::informacion:	/*Informaci�n*/
		default:
			return;
	}
}

void DialogImpl::menuSetExclusivo(int index)
{
	for(int i=0;i<widgetAcciones->count();i++) {
		widgetAcciones->setTabEnabled(i,(index<0?true:(i==index)));
	}
	
	//Si no estamos en Windows, inhabilitar siempre la pesta�a de IExplorer
#ifndef WIN32
	widgetAcciones->setTabEnabled(Acciones::importarIE,false);
#endif /*WIN32*/
}


//Pesta�a Certificados
void DialogImpl::limpiaVentanaCertificados(void)
{
	this->setCursor(Qt::WaitCursor);
	tablaCertificadosClauer->setRowCount(0);
	
	int h=tablaCertificadosClauer->width()-30;//tablaCertificadosClauer->verticalHeader()->width()*2;
#ifdef DEBUG
	cerr << "Anchura Certificados: " << h << endl;
#endif /*DEBUG*/
	const int icoH=30;
	tablaCertificadosClauer->setColumnWidth(CertificadosClauer::borrar,icoH);
	h-=icoH;
#ifdef WIN32
	tablaCertificadosClauer->setColumnWidth(CertificadosClauer::exportarIE,icoH);
	h-=icoH;
#else
	tablaCertificadosClauer->setColumnWidth(CertificadosClauer::exportarIE,0);
#endif /*WIN32*/
	tablaCertificadosClauer->setColumnWidth(CertificadosClauer::exportarFichero,icoH);
	h-=icoH;
	tablaCertificadosClauer->setColumnWidth(CertificadosClauer::ver,icoH);
	h-=icoH;
	tablaCertificadosClauer->setColumnWidth(CertificadosClauer::caducidad,h*0.2);
	tablaCertificadosClauer->setColumnWidth(CertificadosClauer::emisor,h*0.3);
	tablaCertificadosClauer->setColumnWidth(CertificadosClauer::titular,h*0.5);
	//tablaCertificadosClauer->horizontalHeader()->setResizeMode(QHeaderView::ResizeToContents);
	
	certificadosClauer.clear();
	
	leeCertificadosClauer(0,NULL,certificadosClauer);
	qSort(certificadosClauer);
	tablaCertificadosClauer->setRowCount(certificadosClauer.count());
	for(int i=0;i<certificadosClauer.count();i++) {
		tablaCertificadosClauer->setItem(i,CertificadosClauer::titular,celda(certificadosClauer[i].getTitularFriendly(),tr("Titular del certificado")));
		tablaCertificadosClauer->setItem(i,CertificadosClauer::emisor,celda(certificadosClauer[i].getEmisor(),tr("Organizaci�n que ha emitido el certificado")));
		tablaCertificadosClauer->setItem(i,CertificadosClauer::caducidad,celda(certificadosClauer[i].getFinValidez().toString(Qt::SystemLocaleShortDate),tr("Fecha de caducidad del certificado")));
		tablaCertificadosClauer->setItem(i,CertificadosClauer::ver,celda(/*"NFO"*/QIcon(":/iconos/cert_info.png"),tr("Visualizar informaci�n del certificado")));
		tablaCertificadosClauer->setItem(i,CertificadosClauer::exportarFichero,celda(/*">P12"*/QIcon(":/iconos/export_p12.png"),tr("Exportar el certificado a un fichero PKCS#12")));
#ifdef WIN32
		tablaCertificadosClauer->setItem(i,CertificadosClauer::exportarIE,celda(/*">IE"*/QIcon(":/iconos/export_ie.png"),tr("Exportar el certificado al Internet Explorer")));
#endif /*WIN32*/
		tablaCertificadosClauer->setItem(i,CertificadosClauer::borrar,celda(/*"DEL"*/QIcon(":/iconos/del_cert.png"),tr("Elimina el certificado del clauer")));
	}
	this->setCursor(Qt::ArrowCursor);
}

void DialogImpl::clickVentanaCertificados(int row, int column)
{
#ifdef DEBUG
	cerr << "Click Certificados: " << row << "," << column << endl;
#endif /*DEBUG*/
	switch(column) {
		case CertificadosClauer::ver:		//Visualizar el certificado
			informacionCertificado(row);
			break;
		case CertificadosClauer::exportarFichero:	//Copia el certificado a un fichero
			exportaPKCS12(row);
			break;
		case CertificadosClauer::exportarIE:		//Copiar el certificado a Internet Explorer
			exportaIExplorer(row);
			break;
		case CertificadosClauer::borrar:			//Eliminar el certificado
			borraCertificado(row);
			break;
		default:
			return;
	}
}

bool DialogImpl::informacionCertificado(int nCert)
{
	DialogInfoCertificado info(certificadosClauer[nCert],this);
	info.exec();
	return true;
}

bool DialogImpl::exportaPKCS12(int nCert)
{
#ifdef DEBUG
	cerr << "exportaPKCS12(" << nCert <<")" << endl;
#endif /*DEBUG*/

	while(true) {
		//Pedimos la contrase�a del Clauer
		if(!pidePassword(this)) {
			//Abortado
			return false;
		}
		//Obtenemos la clave privada del certificado
		if(leePrivadaClauer(0,password,certificadosClauer[nCert])==0)
			break;
		passwordIncorrecta(this);
	}
		
	//Generamos PKCS#12
	QByteArray pkcs12;
	//Por el momento lo protegemos con la misma contrase�a del Clauer
	if(!certificadosClauer[nCert].buildPKCS12(pkcs12,this->password)) {
		//Error generando
		return false;
	}
	
	bool vuelta=false;
	//Pedimos ruta del fichero destino
	QString destino=QFileDialog::getSaveFileName(this,
		tr("Seleccione archivo en donde se exportar� el certificado"),
		".",
		tr("Contenedor criptogr�fico PKCS#12 (*.p12)"));
	if(destino.isEmpty())
		goto bailout;

	//Exportamos al fichero
	if(escribeFichero(destino,pkcs12))
		vuelta=true;
	
bailout:
	//CRYPTO_SecureZeroMemory((void *)pkcs12.constData(),pkcs12.length());
	SecureZeroMemory(pkcs12);
	certificadosClauer[nCert].deletePrivateKey();
	if(vuelta) {
		QMessageBox::information(this,tituloDialogos,
			tr("El certificado se ha exportado correctamente a la ruta: %1.").arg(destino));
	} else {
		QMessageBox::critical(this,tituloDialogos,
			tr("No se pudo exportar el certificado."));
	}
	return vuelta;
}

bool DialogImpl::exportaIExplorer(int nCert)
{
#ifdef DEBUG
	cerr << "exportaIEXplorer(" << nCert <<")" << endl;
#endif /*DEBUG*/

	while(true) {
		//Pedimos la contrase�a del Clauer
		if(!pidePassword(this)) {
			//Abortado
			return false;
		}
		this->setCursor(Qt::WaitCursor);
		qApp->processEvents(QEventLoop::AllEvents);
		//Obtenemos la clave privada del certificado
		if(leePrivadaClauer(0,password,certificadosClauer[nCert])==0) {
			this->setCursor(Qt::ArrowCursor);
			break;
		}
		passwordIncorrecta(this);
	}
	
	//Obtenemos el blob privado del certificado
	if(leeBLOBClauer(0,password,certificadosClauer[nCert])!=0) {
		this->setCursor(Qt::ArrowCursor);
		return false;
	}
	
	bool vuelta=false;
	//cerr << "privada: " << certificadosClauer[nCert].getPrivateKey().toHex().constData() << endl;
	
	int res=exportaIE(certificadosClauer[nCert]);
	this->setCursor(Qt::ArrowCursor);
#ifdef DEBUG
	cerr << "exportaIE: " << res << endl;
#endif /*DEBUG*/
	if(res==0)
		vuelta=true;
	
	//SecureZeroMemory(pkcs12);
	certificadosClauer[nCert].deletePrivateKey();
	if(vuelta) {
		QMessageBox::information(this,tituloDialogos,
			tr("El certificado se ha exportado correctamente."));
	} else {
		QMessageBox::critical(this,tituloDialogos,
			tr("No se pudo exportar el certificado."));
	}
	
	return vuelta;
}

bool DialogImpl::borraCertificado(int nCert)
{
	bool vuelta;
#ifdef DEBUG
	cerr << "borraCertificado(" << nCert <<")" << endl;
#endif /*DEBUG*/
	
	if(QMessageBox::question(this,tituloDialogos,
		tr("Se encuentra a punto de eliminar permanentemente el certificado del clauer.\n�Desea continuar?"),
		QMessageBox::Yes|QMessageBox::No,
		QMessageBox::No)!=QMessageBox::Yes)
		return false;
	while(true) {
		//Pedimos la contrase�a del Clauer
		if(!pidePassword(this)) {
			//Abortado
			return false;
		}
		this->setCursor(Qt::WaitCursor);
		qApp->processEvents(QEventLoop::AllEvents);
		vuelta=borraCertificadoClauer(0,password,certificadosClauer[nCert])==0;
		this->setCursor(Qt::ArrowCursor);
		if(vuelta) {
			limpiaVentanaCertificados();
			break;
		}
		passwordIncorrecta(this);
	}
	
	if(vuelta) {
		QMessageBox::information(this,tituloDialogos,
			tr("El certificado se ha eliminado correctamente."));
	} else {
		QMessageBox::critical(this,tituloDialogos,
			tr("No se ha podido eliminar el certificado."));
	}
	return vuelta;
}



//Pesta�a Password
void DialogImpl::limpiaVentanaPassword(void)
{
	C_passwordACT->clear();
	C_password1->clear();
	C_password2->clear();
	botonCambiarPass->setEnabled(false);
}

void DialogImpl::clickCambiarPass(bool checked)
{
	//Comprobamos que haya contrase�a antigua
	if(C_passwordACT->text().isEmpty())
		return;
	
	//Comprobamos que haya contrase�a nueva
	if(C_password1->text().isEmpty())
		return;
	
	//Comprobamos que las passwords coincidan
	if(!validaPasswords(this,C_password1->text(),C_password2->text())) {
		C_password1->clear();
		C_password2->clear();
		C_password1->setFocus();
		return;
	}
	
	int res;
	this->setCursor(Qt::WaitCursor);
	qApp->processEvents(QEventLoop::AllEvents);
	res=cambiaPasswordClauer(0,C_passwordACT->text(),C_password1->text());
	this->setCursor(Qt::ArrowCursor);
	if(res==0) {
		QMessageBox::information(this,tituloDialogos,
			tr("La contrase�a del clauer se ha cambiado correctamente."));
			limpiaVentanaPassword();
			menuSetExclusivo(-1);
	} 
 	else if(res=-3){
		QMessageBox::critical(this,tituloDialogos,
                tr("Ha ocurrido un error grave cambiando la contrase�a de su clauer. Deber� formatearlo de nuevo."));
 	} 
        else {
		QMessageBox::critical(this,tituloDialogos,
		tr("No se ha podido cambiar la contrase�a del clauer. �Es correcta la contrase�a actual?"));
	}
}

void DialogImpl::setStatusCambiarPass(const QString &texto)
{
	botonCambiarPass->setEnabled(
		((C_passwordACT->text().isEmpty())||
		(C_password1->text().isEmpty())||
		(C_password2->text().isEmpty()))?false:true);
}

//Pesta�a Importar Fichero
void DialogImpl::limpiaVentanaImportarFichero(void)
{
	IF_ruta->clear();
	IF_password->clear();
	botonImportarFichero->setEnabled(false);
}

void DialogImpl::setStatusImportarFichero(const QString &texto)
{
	botonImportarFichero->setEnabled(
		(!IF_ruta->text().isEmpty())
		&&
		isFileReadable(IF_ruta->text())
	);
}

void DialogImpl::clickBuscarFichero(bool checked)
{
	QString ruta=QFileDialog::getOpenFileName(this,
		tr("Seleccione el archivo que contenga el certificado a importar"),
		".",
		tr("Certificados digitales (*.p12; *.pfx)"));
	
	if(!ruta.isEmpty())
		IF_ruta->setText(ruta);
}

void DialogImpl::clickImportarFichero(bool checked)
{
	while(true) {
		//Pedimos la contrase�a del Clauer
		if(!pidePassword(this)) {
			//Abortado
			return;
		}
		int res;
		this->setCursor(Qt::WaitCursor);
		qApp->processEvents(QEventLoop::AllEvents);
		res=importaPKCS12(0,&password,IF_ruta->text(),IF_password->text());
		this->setCursor(Qt::ArrowCursor);
		if(res==0) {
			QMessageBox::information(this,tituloDialogos,
			tr("El certificado se ha importado correctamente dentro del clauer."));
			limpiaVentanaImportarFichero();
			return;
		}
		if(res==-400) {
			QMessageBox::critical(this,
				tituloDialogos,
				QObject::tr("Contrase�a del certificado incorrecta."));
			return;
		} else {
			passwordIncorrecta(this);
		}
	}	
}

//Pesta�a Importar Internet Explorer
void DialogImpl::limpiaVentanaImportarIExplorer(void)
{
	this->setCursor(Qt::WaitCursor);
	tablaCertificadosIE->setRowCount(0);
	int h=tablaCertificadosIE->width()-30;//tablaCertificadosIE->verticalHeader()->width()*2;
	tablaCertificadosIE->setColumnWidth(2,h*0.2);
	tablaCertificadosIE->setColumnWidth(1,h*0.3);
	tablaCertificadosIE->setColumnWidth(0,h*0.5);
	//tablaCertificadosIE->horizontalHeader()->setResizeMode(QHeaderView::ResizeToContents);
	setStatusBotones();
	certificadosIE.clear();
	
	leeCertificadosIE(certificadosIE);
	qSort(certificadosIE);
	tablaCertificadosIE->setRowCount(certificadosIE.count());
	for(int i=0;i<certificadosIE.count();i++) {
		//cerr << i << ": " << certificadosIE[i].getTitular().toLatin1().constData() << " / " << certificadosIE[i].getCertID().toHex().constData() << endl;
		tablaCertificadosIE->setItem(i,CertificadosIE::titular,celda(certificadosIE[i].getTitularFriendly(),tr("Titular del certificado")));
		tablaCertificadosIE->setItem(i,CertificadosIE::emisor,celda(certificadosIE[i].getEmisor(),tr("Organizaci�n que ha emitido el certificado")));
		tablaCertificadosIE->setItem(i,CertificadosIE::caducidad,celda(certificadosIE[i].getFinValidez().toString(Qt::SystemLocaleShortDate),tr("Fecha de caducidad del certificado")));
	}
	this->setCursor(Qt::ArrowCursor);
}

void DialogImpl::setStatusBotones(void)
{
	if(tablaCertificadosIE->selectedItems().isEmpty()) {
		botonImportarIEMover->setEnabled(false);
		botonImportarIECopiar->setEnabled(false);
	} else {
		botonImportarIEMover->setEnabled(true);
		botonImportarIECopiar->setEnabled(true);
	}
}

void DialogImpl::clickMoverIE(bool checked)
{
	importarIE(true);
}

void DialogImpl::clickCopiarIE(bool checked)
{
	importarIE(false);
}

bool DialogImpl::importarIE(bool mover)
{
	bool fin=false;
	while(!fin) {
		//Pedimos la contrase�a del Clauer
		if(!pidePassword(this)) {
			//Abortado
			return false;
		}
		int res;
		this->setCursor(Qt::WaitCursor);
		qApp->processEvents(QEventLoop::AllEvents);
		res=importaIE(0,password,certificadosIE[tablaCertificadosIE->currentRow()],mover);
		this->setCursor(Qt::ArrowCursor);
#ifdef DEBUG
		cerr << "importaIE: " << res << endl;
#endif /*DEBUG*/
		switch(res) {
			case 0:	//todo correcto
				QMessageBox::information(this,tituloDialogos,
					mover?
						tr("El certificado se ha movido correctamente."):
						tr("El certificado se ha copiado correctamente."));
				fin=true;
				break;
			case ERR_CL_KEY_NOT_EXPORTABLE:
				QMessageBox::critical(this,tituloDialogos,
					tr("La operaci�n no se ha podido realizar porque el certificado elegido no es exportable."));
				password.clear(); //porque no tenemos la certeza de que fuese correcta
				return false;
			default:
				passwordIncorrecta(this);
				break;
		}
	}
	
	if(mover)
		limpiaVentanaImportarIExplorer();
	
	return true;
}

	
//Pesta�a Formatear
void DialogImpl::limpiaVentanaFormatear(void)
{
	F_password1->clear();
	F_password2->clear();
	/*txtFormatear->setText(
		formatearClauerCompleto?
		tr("El Clauer no tiene formato criptogr�fico.\nA continuaci�n se va a preparar el Clauer. Por favor, no retire la memoria USB mientras dure la operaci�n.")
		:
		tr("Esta operaci�n va a eliminar todos los certificados, claves y credenciales que contiene el Clauer. No afectar� a la zona de datos.\nDespues podr� volver a importar los certificados si dispone de una copia.")
	);*/
	if(formatearClauerCompleto)
		rellenaIntQLabel(txtFormatear,":/i18n/firstFormat");
	else
		rellenaIntQLabel(txtFormatear,":/i18n/doFormat");
	botonFormatear->setEnabled(false);
}

void DialogImpl::clickFormatear(bool checked)
{
	//Comprobamos que las passwords coincidan
	if(!validaPasswords(this,F_password1->text(),F_password2->text())) {
		F_password1->clear();
		F_password2->clear();
		F_password1->setFocus();
		return;
	}
		
	//Advertencia final antes de formatear
	if(QMessageBox::question(this,tituloDialogos,
		"<font color=\"red\">"+(formatearClauerCompleto?
			tr("Todos los datos del sitck USB se borrar�n. �Est� seguro que desea continuar?")
			:
			tr("A continuaci�n se va a formatear la zona criptogr�fica, todos los certificados que contenga el clauer ser�n eliminados.\n�Est� seguro de que desea formatear la zona criptogr�fica?")
			)+"</font>"
			,
		QMessageBox::Ok|QMessageBox::Cancel,
		QMessageBox::Cancel)!=QMessageBox::Ok)
		return;
	
	int res;
	this->setCursor(Qt::WaitCursor);
	qApp->processEvents(QEventLoop::AllEvents);
	res=(formatearClauerCompleto?
		creaClauer(0,F_password1->text())
		:
		formateaClauer(0,F_password1->text())
		);
	this->setCursor(Qt::ArrowCursor);
	if(res==0) {
		formatearClauerCompleto=false;
		QMessageBox::information(this,tituloDialogos,
			tr("El clauer se ha formateado correctamente."));
		limpiaVentanaFormatear();
		menuSetExclusivo(-4);
		//Abrimos la pesta�a Informaci�n
		widgetAcciones->setCurrentIndex(Acciones::informacion);
	} else {
		QMessageBox::critical(this,tituloDialogos,
			tr("No se ha podido formatear el Clauer."));
	}
}

void DialogImpl::setStatusFormatear(const QString &texto)
{
	botonFormatear->setEnabled(
		((F_password1->text().isEmpty())||
		(F_password2->text().isEmpty()))?false:true);
}


//Pesta�a Informaci�n
void DialogImpl::limpiaVentanaInformacion(void)
{
#ifdef DEBUG
	cerr << "Estado informaci�n: " << estado << endl;
#endif /*DEBUG*/
	switch(estado) {
		case Estados::bienvenida:	//Mensaje bienvenida - Un unico Clauer conectado
			//txtInformacion->setText(tr("Gestor del Clauer - Universitat Jaume I"));
			rellenaIntQLabel(txtInformacion,":/i18n/welcome");
			botonesInformacion->setVisible(false);
			break;
		case Estados::noClauer: 		//Ning�n Clauer conectado
			//txtInformacion->setText(tr("No hay ning�n Stick USB insertado en el sistema\nPor favor, inserte el dispositivo\nque desee inicializar y pulse el bot�n Aceptar."));
			rellenaIntQLabel(txtInformacion,":/i18n/noClauer");
			botonesInformacion->setVisible(true);
			break;
		case Estados::variosClauers: //Varios Clauers/USB conectados
			txtInformacion->setText(tr("Hay m�s de un Stick USB insertado en el sistema\nPor favor, deje insertado\n�nicamente el dispostivo que desee inicializar y pulse el bot�n Aceptar."));
			botonesInformacion->setVisible(true);
			break;
		default:
			break;
	}
}

void DialogImpl::recalcularEstado(void)
{
	switch(localizaClauer(this,false)) {
		case LClauers::unico:	//�nico clauer formateado
			estado=Estados::bienvenida;
			if(clauerExigeCambioPassword(0)>0) {
				//Si tiene marca especial, forzar cambio de password
				widgetAcciones->setCurrentIndex(Acciones::password);
				menuSetExclusivo(Acciones::password);
			} else {
				//widgetAcciones->setCurrentIndex(Acciones::informacion);
				menuSetExclusivo(-1);
			}
			break;
		case LClauers::formatear:	//�nico clauer sin formatear
			estado=Estados::bienvenida;
			this->password.clear();
			formatearClauerCompleto=true;
			widgetAcciones->setCurrentIndex(Acciones::formatear);
			menuSetExclusivo(Acciones::formatear);
			break;
		case LClauers::ninguno:	//ningun Clauer ni USB conectado
			estado=Estados::noClauer;
			this->password.clear();
			widgetAcciones->setCurrentIndex(Acciones::informacion);
			menuSetExclusivo(Acciones::informacion);
			break;
		case LClauers::varios:	//m�s de un Clauer o USB conectado
			estado=Estados::variosClauers;
			this->password.clear();
			widgetAcciones->setCurrentIndex(Acciones::informacion);
			menuSetExclusivo(Acciones::informacion);
			break;
		case LClauers::abortar:	//abortar
			QCoreApplication::exit(254);
			//return;
		default:	//imposible
#ifdef DEBUG
			cerr << "recalcularEstado: opci�n imposible" << endl;
#endif /*DEBUG*/
			bailoutClauer(this);
	}
	limpiaVentanaInformacion();
}

void DialogImpl::clickInfo(QAbstractButton *button)
{
#ifdef DEBUG
	cerr << "clickInfo: " << botonesInformacion->buttonRole(button) << endl;
#endif /*DEBUG*/
	switch(botonesInformacion->buttonRole(button)) {
		case QDialogButtonBox::AcceptRole:
			recalcularEstado();
			break;
		case QDialogButtonBox::RejectRole:
			QApplication::exit(253);
			break;
		default:
			break;
	}
}
