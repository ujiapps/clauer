#ifndef DIALOGIMPL_H
#define DIALOGIMPL_H
//
#include <QDialog>
#include <QList>
#include "ui_principal.h"
#include "certificados.h"
#include "iexplorer.h"
//

namespace Acciones {
	/*ATENCI�N: mantener sincronizado con QTDesigner (widgetAcciones)*/
	enum Acciones {
			certificados=0,
			password=1,
			importarFichero=2,
			importarIE=3,
			formatear=4,
			informacion=5
	};
}

namespace CertificadosClauer {
	/*ATENCI�N: mantener sincronizado con QTDesigner (tablaCertificadosClauer)*/
	enum CertificadosClauer {
		titular=0,
		emisor=1,
		caducidad=2,
		ver=3,
		exportarFichero=4,
		exportarIE=5,
		borrar=6
	};
}

namespace CertificadosIE {
	/*ATENCI�N: mantener sincronizado con QTDesigner (tablaCertificadosIE)*/
	enum CertificadosIE {
		titular=0,
		emisor=1,
		caducidad=2,
	};
}

namespace Estados {
	enum Estado {bienvenida, noClauer, variosClauers};
}

class DialogImpl : public QDialog, public Ui::DialogPrincipal
{
Q_OBJECT
public:
	DialogImpl( QWidget * parent = 0, Qt::WFlags f = Qt::WindowMinMaxButtonsHint );

protected:
	//Datos
	QString password;
	QList<CertificadoX509> certificadosClauer;
	QList<CertificadoIE> certificadosIE;
	
	Estados::Estado estado;	//M�quina de estados para la pesta�a Informaci�n
	
	//Opciones de configuraci�n
	bool formatearClauerCompleto;
	bool forzarCambioPassword;
	
	//Men� Principal
	void limpiaVentanaFormatear(void);
	void menuSetExclusivo(int index);
	
private slots:
	//Pesta�a Certificados
	void limpiaVentanaCertificados(void);
	void clickVentanaCertificados(int row, int column);
	bool informacionCertificado(int nCert);
	bool exportaPKCS12(int nCert);
	bool exportaIExplorer(int nCert);
	bool borraCertificado(int nCert);
	//Pesta�a Contrase�a
	void limpiaVentanaPassword(void);
	void clickCambiarPass(bool checked);
	void setStatusCambiarPass(const QString &texto);
	//Pesta�a Importar Fichero
	void limpiaVentanaImportarFichero(void);
	void setStatusImportarFichero(const QString &texto);
	void clickBuscarFichero(bool checked);
	void clickImportarFichero(bool checked);
	//Pesta�a Importar Internet Explorer
	void limpiaVentanaImportarIExplorer(void);
	void setStatusBotones(void);
	void clickMoverIE(bool checked);
	void clickCopiarIE(bool checked);
	bool importarIE(bool mover);
	//Pesta�a Formatear
	void opcionMenuPrincipal(int index);
	void clickFormatear(bool checked);
	void setStatusFormatear(const QString &texto);
	bool pidePassword(QWidget *parent, bool cachear=true);
	void passwordIncorrecta(QWidget *parent);
	//Pesta�a Informaci�n
	void limpiaVentanaInformacion(void);
	void recalcularEstado(void);
	void clickInfo(QAbstractButton *button);
};
#endif
