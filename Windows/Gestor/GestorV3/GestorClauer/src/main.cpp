#include <QApplication>
#include <QTextCodec>
#include <QMessageBox>
#include <QTranslator>
#include <QLibraryInfo>
#include <QPlastiqueStyle>
#include "single_application.h"
#include "dialogimpl.h"
#include <unistd.h>
#include "HLClauer.h"
#include "CRYPTOWrapper/CRYPTOWrap.h"
#include "iexplorer.h"
#include "auxiliares.h"
#include "configuracion.h"
//

#ifdef DEBUG
#include <iostream>
using namespace std;
#endif /*DEBUG*/

int main(int argc, char ** argv)
{
	int opt,vuelta=0;
	
	QCoreApplication::setOrganizationName("Universitat Jaume I");
	QCoreApplication::setOrganizationDomain("uji.es");
	QCoreApplication::setApplicationName("QTClauer");
	SingleApplication app(argc,argv,QCoreApplication::applicationName());
	if(app.isRunning())
		return 1;
	
	QApplication::setStyle(new QPlastiqueStyle);
	
	ConfiguracionClauer config;
	
    //Traducciones del Gestor del Clauer
    QTranslator clauerTranslator;
    clauerTranslator.load("i18n/GestorClauer_"+config.idioma,
    	":/"/*app.applicationDirPath()*/);
    app.installTranslator(&clauerTranslator);


	//Traducciones de QT
	QTranslator qtTranslator;
	qtTranslator.load("i18n/qt_"+/*QLocale::system().name()*/config.idioma,
		":/"/*QLibraryInfo::location(QLibraryInfo::TranslationsPath)*/);
    app.installTranslator(&qtTranslator);
    


    //Hoja de estilos
    app.setStyleSheet(config.stylesheet);


	mingw_load_crypto_func();
	CRYPTO_Ini();
	iniciaClauer();
	while((opt=getopt(argc,argv,"ip"))!=-1) {
		switch(opt) {
			case 'i': /*no forzar cambio de password al comienzo*/
#ifdef DEBUG
				cerr << "Parámetro i" << endl;
#endif /*DEBUG*/
				break;
			case 'p':
#ifdef DEBUG
				cerr << "Parámetro p" << endl;
#endif /*DEBUG*/
				break;
			default:
				break;
		}
	}
	/*libRT codifica en ISO8859-1*/
	QTextCodec::setCodecForCStrings(QTextCodec::codecForName("ISO 8859-1"));
	
	//cerr << "Instancia unica: " << isSingleInstance("dfdf") << endl;
	//nClauersConectados(1);
#ifdef DEBUG
	cerr << "Clauer exige cambio de password: " << clauerExigeCambioPassword(0) << endl;
#endif /*DEBUG*/
	
	DialogImpl win;
	win.show();
	//app.connect( &app, SIGNAL( lastWindowClosed() ), &app, SLOT( quit() ) );
	
	
	vuelta=app.exec();
//bailout:
#ifdef DEBUG
	cerr << "Finalizando clauer" << endl;
#endif /*DEBUG*/
	finalizaClauer();
	CRYPTO_Fin();
	return vuelta;
}
