/****************************************************************************
**
** 	Created using Edyuk 1.0.0
**
** File : iexplorer.cpp
** Date : mi� 22. oct 18:19:44 2008
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
****************************************************************************/

/*!
	\file iexplorer.cpp
	
	\brief Implementation of Utilidades para Internet Explorer
*/

#include "iexplorer.h"
#include <string.h>
#include <QByteArray>

#ifdef WIN32
#include <stdlib.h>
#include <wchar.h>
#include "HLClauer.h"
#include "LIBRT/libRT.h"
#include "CRYPTOWrapper/CRYPTOWrap.h"

#ifdef DEBUG
#include <iostream>
using namespace std;
#endif /*DEBUG*/

#ifdef __MINGW32__
typedef struct _CRYPT_KEY_PROV_PARAM {
  DWORD dwParam;
  BYTE* pbData;
  DWORD cbData;
  DWORD dwFlags;
} CRYPT_KEY_PROV_PARAM, *PCRYPT_KEY_PROV_PARAM;

typedef struct _CRYPT_KEY_PROV_INFO {
  LPWSTR pwszContainerName;
  LPWSTR pwszProvName;
  DWORD dwProvType;
  DWORD dwFlags;
  DWORD cProvParam;
  PCRYPT_KEY_PROV_PARAM rgProvParam;
  DWORD dwKeySpec;
} CRYPT_KEY_PROV_INFO, 
 *PCRYPT_KEY_PROV_INFO;

#define CERT_STORE_PROV_PHYSICAL_W           ((LPCSTR)14)
#define CERT_STORE_PROV_PHYSICAL             CERT_STORE_PROV_PHYSICAL_W
#define sz_CERT_STORE_PROV_PHYSICAL_W        "Physical"
#define sz_CERT_STORE_PROV_PHYSICAL          sz_CERT_STORE_PROV_PHYSICAL_W

#define CERT_KEY_PROV_INFO_PROP_ID                 2
#define CERT_STORE_ADD_REPLACE_EXISTING                    3
#define CERT_FRIENDLY_NAME_PROP_ID                 11



#define CERT_NAME_SIMPLE_DISPLAY_TYPE   4
#define CERT_NAME_FRIENDLY_DISPLAY_TYPE 5

#define CERT_NAME_ISSUER_FLAG           0x00000001

#define CERT_SHA1_HASH_PROP_ID                     3
#define CERT_HASH_PROP_ID                          CERT_SHA1_HASH_PROP_ID

#define CERT_SYSTEM_STORE_LOCATION_SHIFT 16
#define CERT_SYSTEM_STORE_CURRENT_USER_ID               1
#define CERT_SYSTEM_STORE_CURRENT_USER \
 (CERT_SYSTEM_STORE_CURRENT_USER_ID << CERT_SYSTEM_STORE_LOCATION_SHIFT)

#define CRYPT_E_NOT_FOUND               (0x80092004L)


typedef PCCERT_CONTEXT (WINAPI *_CertCreateCertificateContext)(DWORD, const BYTE *, DWORD);
typedef BOOL (WINAPI *_CertSetCertificateContextProperty)(PCCERT_CONTEXT,DWORD,DWORD,const void *);
typedef BOOL (WINAPI *_CertGetCertificateContextProperty)(PCCERT_CONTEXT,DWORD,void *,DWORD *);
/*
typedef BOOL (WINAPI *_CertAddCertificateContextToStore)(HCERTSTORE,PCCERT_CONTEXT,DWORD,PCCERT_CONTEXT *);
typedef BOOL (WINAPI *_CertDeleteCertificateFromStore)(PCCERT_CONTEXT);
*/
static _CertCreateCertificateContext CertCreateCertificateContext=NULL;
static _CertSetCertificateContextProperty CertSetCertificateContextProperty=NULL;
static _CertGetCertificateContextProperty CertGetCertificateContextProperty=NULL;
/*
static _CertAddCertificateContextToStore CertAddCertificateContextToStore;=NULL;
static _CertDeleteCertificateFromStore CertDeleteCertificateFromStore=NULL;
*/

int mingw_load_crypto_func(void)
{
	HINSTANCE dll;

	if(CertCreateCertificateContext)
		return 0;

	if((dll=LoadLibraryW(L"crypt32"))==NULL)
		return -1;

	if((CertCreateCertificateContext=(_CertCreateCertificateContext)
		GetProcAddress(dll,"CertCreateCertificateContext"))==NULL)
		return -2;
	
	if((CertSetCertificateContextProperty=(_CertSetCertificateContextProperty)
	GetProcAddress(dll,"CertSetCertificateContextProperty"))==NULL)
		return -3;
	
	if((CertGetCertificateContextProperty=(_CertGetCertificateContextProperty)
	GetProcAddress(dll,"CertGetCertificateContextProperty"))==NULL)
		return -4;
	
	/*
	if((CertAddCertificateContextToStore=(_CertAddCertificateContextToStore)
	GetProcAddress(dll,"CertAddCertificateContextToStore"))==NULL)
		return -5;
	
	if((CertDeleteCertificateFromStore=(_CertDeleteCertificateFromStore)
	GetProcAddress(dll,"CertDeleteCertificateFromStore"))==NULL)
		return -6;
	*/
	
	return 0;
}
#else
int mingw_load_crypto_func(void)
{
	return 0;
}
#endif /*__MINGW32__*/


int exportaIE(CertificadoX509 &x509)
{
	int vuelta;
	
	CRYPT_HASH_BLOB hb;
	HCRYPTPROV hProv=0;
	HCRYPTKEY hKey=0;
	HCERTSTORE hStore=0;
	DWORD dwAux;
	ALG_ID keySpec;
	WCHAR *store=L"My\\.Default";
	
	QByteArray der;
	PCCERT_CONTEXT pCertAux;
	
	DWORD dwContainerName;
	LPWSTR wszContainerName = NULL;
	CRYPT_KEY_PROV_INFO infoCSP;


	
	char *szCSP="Microsoft Enhanced Cryptographic Provider v1.0";
	hb.pbData=(BYTE *)x509.getCertID().constData();
	hb.cbData=x509.getCertID().length();
	
	//Creamos un contaniner nuevo e importamos la clave
	char szContainerName[35];
	strcpy(szContainerName, "climportedkey_");
	
	if(!CryptAcquireContextA(&hProv,NULL,NULL,PROV_RSA_FULL,CRYPT_VERIFYCONTEXT)) {
		return -1;
	}
	if(!CryptGenRandom(hProv,20,(LPBYTE)(szContainerName+14))) {
		CryptReleaseContext(hProv,0);
		return -2;
	}
	CryptReleaseContext(hProv,0);
	//hProv=0;
	
	for(int i=0;i<20;i++ )
		szContainerName[14+i]=97+(szContainerName[14+i]%26);
		//*(szContainerName+14+i)=97+(unsigned char)*(szContainerName+14+i)%26;
	//*(szContainerName+34)=0;
	szContainerName[34]=0;
	
	if(!CryptAcquireContextA(&hProv,szContainerName,szCSP,PROV_RSA_FULL,CRYPT_NEWKEYSET)) {
		return -3;
	}
	
	if(!CryptImportKey(hProv,
		(BYTE *)x509.getPrivateBLOB().constData(),
		x509.getPrivateBLOB().length(),
		0,
		0,
		&hKey))
	{
		vuelta=-4;
		goto bailout;
	}
	
	dwAux=sizeof(ALG_ID);
	if(!CryptGetKeyParam(hKey,KP_ALGID,(BYTE *)&keySpec,&dwAux,0)) {
		vuelta=-5;
		goto bailout;
	}
	CryptDestroyKey(hKey);
	
	if(keySpec==CALG_RSA_KEYX)
		keySpec=AT_KEYEXCHANGE;
	else if(keySpec==CALG_RSA_SIGN )
		keySpec=AT_SIGNATURE;
	
	
	//Convertimos el certificado a formato DER
	if(!x509.buildDER(der)) {
		vuelta=-6;
		goto bailout;
	}
	
	//Insertamos el certificado
	if(!(hStore=CertOpenStore(CERT_STORE_PROV_PHYSICAL/*"Physical"*/,0,0,/*CERT_SYSTEM_STORE_CURRENT_USER*/1<<16,(const void *)store))) {
		vuelta=-7;
		goto bailout;
	}
	
	if((pCertAux=CertCreateCertificateContext(X509_ASN_ENCODING,(const BYTE *)der.constData(),der.length()))==NULL) {
		vuelta=-8;
		goto bailout;
	}
	
	if((dwContainerName=MultiByteToWideChar(CP_ACP,0,szContainerName, strlen(szContainerName)+1,NULL,0))<=0) {
		vuelta=-9;
		goto bailout;
	}
	if((wszContainerName=new(std::nothrow) WCHAR[dwContainerName])==NULL) {
		vuelta=-10;
		goto bailout;
	}
	if(MultiByteToWideChar(CP_ACP,0,szContainerName,strlen(szContainerName)+1,wszContainerName,dwContainerName)<=0) {
		vuelta=-11;
		goto bailout1;
	}
	
	ZeroMemory(&infoCSP, sizeof infoCSP);
	infoCSP.pwszContainerName = wszContainerName;
	infoCSP.pwszProvName      = WSZ_CL_DEFAULT_IMPORT_CSP;
	infoCSP.dwProvType        = PROV_RSA_FULL;
	infoCSP.dwFlags           = 0;
	infoCSP.cProvParam        = 0;
	infoCSP.rgProvParam       = NULL;
	infoCSP.dwKeySpec         = keySpec;

	if(!CertSetCertificateContextProperty(pCertAux,CERT_KEY_PROV_INFO_PROP_ID,0,&infoCSP)) {
		vuelta=-12;
		goto bailout;
	}
	
	if(!CertAddCertificateContextToStore(hStore,pCertAux,CERT_STORE_ADD_REPLACE_EXISTING,NULL)) {
		DWORD err=GetLastError();
		vuelta=(err==E_INVALIDARG)?-13:0;
		goto bailout;
	}
	
	CertCloseStore(hStore,0);
	vuelta=0;
	
bailout1:
	delete [] wszContainerName;
bailout:
	CryptReleaseContext(hProv,0);
	return vuelta;
}

////

QDateTime FILETIME2QDateTime(const FILETIME *ft)
{
	SYSTEMTIME st;
	FileTimeToSystemTime(ft,&st);
	
	QDate fecha(st.wYear,st.wMonth,st.wDay);
	QTime hora(st.wHour,st.wMinute,st.wSecond,st.wMilliseconds);
	return QDateTime(fecha,hora);
}

CertificadoIE::CertificadoIE(PCCERT_CONTEXT pCertContext)
{
	WCHAR tmp[128];
	DWORD tamID=20;
	
	if(!CertGetCertificateContextProperty(pCertContext,CERT_HASH_PROP_ID,tmp,&tamID))
		throw(-1);
	this->certID=QByteArray((const char *)tmp,20);
	
	CertGetNameString(pCertContext,CERT_NAME_SIMPLE_DISPLAY_TYPE,0,NULL,tmp,128);
	this->titular=QString::fromUtf16((const ushort *)tmp);

	CertGetNameString(pCertContext,CERT_NAME_FRIENDLY_DISPLAY_TYPE,0,NULL,tmp,128);
	this->friendlyName=QString::fromUtf16((const ushort *)tmp);
	
	this->titularFriendly=this->titular;
	if((!this->friendlyName.isEmpty())&&(this->titular!=this->friendlyName))
		this->titularFriendly+=" ("+this->friendlyName+")";

	CertGetNameString(pCertContext,CERT_NAME_SIMPLE_DISPLAY_TYPE,CERT_NAME_ISSUER_FLAG,NULL,tmp,128);
	this->emisor=QString::fromUtf16((const ushort *)tmp);
	
	inicioValidez=FILETIME2QDateTime(&pCertContext->pCertInfo->NotBefore);
	finValidez=FILETIME2QDateTime(&pCertContext->pCertInfo->NotAfter);
}

CertificadoIE::~CertificadoIE()
{
}


////
int leeCertificadosIE(QList<CertificadoIE> &salida)
{
	WCHAR *store=L"My\\.Default";
	HCERTSTORE hStore;
	PCCERT_CONTEXT pCertContext=NULL;
	
	if((hStore=CertOpenStore(CERT_STORE_PROV_PHYSICAL,0,/*NULL*/0,CERT_SYSTEM_STORE_CURRENT_USER,(const void *)store))==NULL)
		return -1;
	
	while(pCertContext=CertEnumCertificatesInStore(hStore,pCertContext)) {
		try {
			CertificadoIE cie(pCertContext);
			salida.append(cie);
		} catch(...) {
		}
	}
	CertCloseStore(hStore,0);
	return 0;
}


int CLIMPORT_CAPI_MY_Cert(char *szDevice, char *szPwd, PCCERT_CONTEXT *certCtx, BOOL bDelete )
{

	BYTE *pvkBlob = NULL;
	DWORD dwpvkBlobSize = 0;
	CRYPT_KEY_PROV_INFO *ki = NULL;
	DWORD kiSize, dwKeyExp, dwKeyExpSize;

	unsigned char *certPEM = NULL;
	unsigned long certPEMSize = 0;

	unsigned char *pemKey = NULL;
	unsigned long pemKeySize;

	unsigned char block[TAM_BLOQUE], block2[TAM_BLOQUE];
	long blockNumber;

	unsigned char idCert[20], id[20];

	LPWSTR wszFriendlyName = NULL;
	LPSTR lpszFriendlyName = NULL;
	DWORD dwwszFriendlyNameSize, dwlpszFriendlyNameSize;

	char containerName[257];
	int i, friendlyNameExists;

	unsigned char zeroId[20];

	int ret = CL_SUCCESS;

	HCRYPTPROV hProv = 0;
	HCRYPTKEY hKey = 0;

	USBCERTS_HANDLE hClauer;

	long bnCert=-1, bnPriv=-1, bnBlob=-1;


	if ( ! szDevice )
	return ERR_CL_BAD_PARAM;
	if ( ! szPwd ) 
	return ERR_CL_BAD_PARAM;
	if ( ! certCtx )
	return ERR_CL_BAD_PARAM;
	if ( ! *certCtx )
	return ERR_CL_BAD_PARAM;


	memset(zeroId, 0, 20);

	/* Compruebo si la llave asociada es exportable o no
	*/

	kiSize = sizeof(CRYPT_KEY_PROV_INFO);
	if ( ! CertGetCertificateContextProperty(*certCtx, CERT_KEY_PROV_INFO_PROP_ID, NULL, &kiSize) ) {
		ret = ERR_CL_CANNOT_GET_ASSOCIATED_KEY;
		goto endCLIMPORT_CAPI_MY_Cert;
	}
	ki = ( CRYPT_KEY_PROV_INFO * ) malloc ( kiSize );
	if ( ! ki ) {
		ret = ERR_CL_OUT_OF_MEMORY;
		goto endCLIMPORT_CAPI_MY_Cert;
	}
	if ( ! CertGetCertificateContextProperty(*certCtx, CERT_KEY_PROV_INFO_PROP_ID, ki, &kiSize) ) {
		ret = ERR_CL_CANNOT_GET_ASSOCIATED_KEY;
		goto endCLIMPORT_CAPI_MY_Cert;
	}  

	if ( ! CryptAcquireContextW(&hProv, ki->pwszContainerName, ki->pwszProvName, ki->dwProvType, ki->dwFlags) ) {
		ret = ERR_CL_CANNOT_GET_ASSOCIATED_KEY;
		goto endCLIMPORT_CAPI_MY_Cert;
	}

	if ( ! CryptGetUserKey(hProv, ki->dwKeySpec, &hKey) ) {
		if ( GetLastError() == NTE_NO_KEY )
		ret = ERR_CL_KEY_NOT_EXISTS;
		else
		ret = ERR_CL_CANNOT_GET_ASSOCIATED_KEY;
		goto endCLIMPORT_CAPI_MY_Cert;
	}


	dwKeyExpSize = sizeof(DWORD);
	if ( ! CryptGetKeyParam(hKey, KP_PERMISSIONS, (BYTE *) &dwKeyExp, &dwKeyExpSize, 0) ) {
		ret = ERR_CL;
		goto endCLIMPORT_CAPI_MY_Cert;
	}


	if ( ! (dwKeyExp & CRYPT_EXPORT) ) {
		ret = ERR_CL_KEY_NOT_EXPORTABLE;
		goto endCLIMPORT_CAPI_MY_Cert;
	}

	/* Ahora vamos a intentar exportar las llaves
	*/

	if ( ! CryptExportKey(hKey, 0, PRIVATEKEYBLOB, 0, NULL, &dwpvkBlobSize) ) {
		ret = ERR_CL;
		goto endCLIMPORT_CAPI_MY_Cert;
	}

	pvkBlob = ( unsigned char * ) malloc ( dwpvkBlobSize );
	if ( ! pvkBlob ) {
		ret = ERR_CL_OUT_OF_MEMORY;
		goto endCLIMPORT_CAPI_MY_Cert;
	}

	if ( ! CryptExportKey(hKey, 0, PRIVATEKEYBLOB, 0, pvkBlob, &dwpvkBlobSize) ) {
		ret = ERR_CL;
		goto endCLIMPORT_CAPI_MY_Cert;
	}

	/* Pasamos la llave a formato PEM
	*/

	if ( CRYPTO_BLOB2LLAVE(pvkBlob, dwpvkBlobSize, NULL, &pemKeySize) != 0 ) {
		ret = ERR_CL;
		goto endCLIMPORT_CAPI_MY_Cert;
	}

	pemKey = ( unsigned char *) malloc ( pemKeySize );
	if ( ! pemKey ) {
		ret = ERR_CL_OUT_OF_MEMORY;
		goto endCLIMPORT_CAPI_MY_Cert;
	}

	if ( CRYPTO_BLOB2LLAVE(pvkBlob, dwpvkBlobSize, pemKey, &pemKeySize) != 0 ) {
		ret = ERR_CL;
		goto endCLIMPORT_CAPI_MY_Cert;
	}

	/* Pasamos el certificado a formato PEM
	*/

	if ( CRYPTO_X509_DER2PEM((*certCtx)->pbCertEncoded, (*certCtx)->cbCertEncoded, NULL, &certPEMSize) != 0 ) {
		ret = ERR_CL;
		goto endCLIMPORT_CAPI_MY_Cert;
	}
	certPEM = ( unsigned char * ) malloc ( certPEMSize +1);
	if ( ! certPEM ) {
		ret = ERR_CL_OUT_OF_MEMORY;
		goto endCLIMPORT_CAPI_MY_Cert;
	}
	if ( CRYPTO_X509_DER2PEM((*certCtx)->pbCertEncoded, (*certCtx)->cbCertEncoded, certPEM, &certPEMSize) != 0 ) {
		ret = ERR_CL;
		goto endCLIMPORT_CAPI_MY_Cert;
	}

	if ( CRYPTO_CERT_PEM_Id(certPEM, certPEMSize, idCert) != 0 ) {
		ret = ERR_CL;
		goto endCLIMPORT_CAPI_MY_Cert;
	}

	/* Si los objetos ya han sido importados no los reimportamos
	*
	* 1. Insertamos el certificado
	*/

	if ( LIBRT_IniciarDispositivo((unsigned char *)szDevice, szPwd, &hClauer) != 0 ) {
		ret = ERR_CL_CANNOT_INITIALIZE;
		goto endCLIMPORT_CAPI_MY_Cert;
	}

	if ( LIBRT_LeerTipoBloqueCrypto(&hClauer, BLOQUE_CERT_PROPIO, 1, block, &blockNumber) != 0 ) {
		LIBRT_FinalizarDispositivo(&hClauer);
		ret = ERR_CL;
		goto endCLIMPORT_CAPI_MY_Cert;
	}

	while ( blockNumber != -1 ) {

		if ( memcmp(BLOQUE_CERTPROPIO_Get_Id(block), idCert, 20) == 0 ) 
		if ( CRYPTO_CERT_Cmp(BLOQUE_CERTPROPIO_Get_Objeto(block), BLOQUE_CERTPROPIO_Get_Tam(block), certPEM, certPEMSize) == 0 ) 
		break;

		if ( LIBRT_LeerTipoBloqueCrypto(&hClauer, BLOQUE_CERT_PROPIO, 0, block, &blockNumber) != 0 ) {
			LIBRT_FinalizarDispositivo(&hClauer);
			ret = ERR_CL;
			goto endCLIMPORT_CAPI_MY_Cert;
		}
	}


	if ( blockNumber == -1 ) {    

		memset(block, 0, TAM_BLOQUE);

		/* Obtengo el friendly name del certificado
		*/

		if ( ! CertGetCertificateContextProperty(*certCtx, CERT_FRIENDLY_NAME_PROP_ID, NULL, &dwwszFriendlyNameSize) ) {

			if ( GetLastError() == CRYPT_E_NOT_FOUND )
			friendlyNameExists = 0;
			else {
				LIBRT_FinalizarDispositivo(&hClauer);
				ret = ERR_CL;
				goto endCLIMPORT_CAPI_MY_Cert;
			}
		} else {
			friendlyNameExists = 1;
		}

		if ( friendlyNameExists ) {
			wszFriendlyName = ( LPWSTR ) malloc ( dwwszFriendlyNameSize );
			if ( ! wszFriendlyName ) {
				LIBRT_FinalizarDispositivo(&hClauer);
				ret = ERR_CL_OUT_OF_MEMORY;
				goto endCLIMPORT_CAPI_MY_Cert;
			}

			if ( ! CertGetCertificateContextProperty(*certCtx, CERT_FRIENDLY_NAME_PROP_ID, wszFriendlyName, &dwwszFriendlyNameSize) ) {
				LIBRT_FinalizarDispositivo(&hClauer);
				ret = ERR_CL;
				goto endCLIMPORT_CAPI_MY_Cert;
			}

			dwlpszFriendlyNameSize = WideCharToMultiByte(CP_ACP, 0, wszFriendlyName, dwwszFriendlyNameSize/sizeof(WCHAR), NULL, 0, NULL, NULL);
			if ( ! dwlpszFriendlyNameSize ) {
				LIBRT_FinalizarDispositivo(&hClauer);
				ret = ERR_CL;
				goto endCLIMPORT_CAPI_MY_Cert;
			}

			lpszFriendlyName = ( char * ) malloc ( dwlpszFriendlyNameSize );
			if ( ! lpszFriendlyName ) {
				LIBRT_FinalizarDispositivo(&hClauer);
				ret = ERR_CL_OUT_OF_MEMORY;
				goto endCLIMPORT_CAPI_MY_Cert;
			}

			if ( ! WideCharToMultiByte(CP_ACP, 0, wszFriendlyName, dwwszFriendlyNameSize/sizeof(WCHAR), lpszFriendlyName, dwlpszFriendlyNameSize, NULL, NULL) ) {
				LIBRT_FinalizarDispositivo(&hClauer);
				ret = ERR_CL;
				goto endCLIMPORT_CAPI_MY_Cert;
			}

			free(wszFriendlyName);
			wszFriendlyName = NULL;
		}

		/* Insertamos el bloque
		*/

		BLOQUE_Set_Claro(block);
		BLOQUE_CERTPROPIO_Nuevo(block);
		BLOQUE_CERTPROPIO_Set_Objeto(block, certPEM, certPEMSize);
		BLOQUE_CERTPROPIO_Set_Tam(block, certPEMSize);
		BLOQUE_CERTPROPIO_Set_Id(block, idCert);
		BLOQUE_CERTPROPIO_Set_FriendlyName(block, lpszFriendlyName);

		free(lpszFriendlyName);
		lpszFriendlyName = NULL;

		if ( LIBRT_InsertarBloqueCrypto(&hClauer, block, &blockNumber) != 0 ) {
			LIBRT_FinalizarDispositivo(&hClauer);
			ret = ERR_CL;
			goto endCLIMPORT_CAPI_MY_Cert;
		}

		bnCert = blockNumber;

	}

	/* 2. Insertamos la llave privada
	*/

	if ( LIBRT_LeerTipoBloqueCrypto(&hClauer, BLOQUE_LLAVE_PRIVADA, 1, block, &blockNumber) != 0 ) {
		LIBRT_FinalizarDispositivo(&hClauer);
		ret = ERR_CL;
		goto endCLIMPORT_CAPI_MY_Cert;
	}

	if ( CRYPTO_LLAVE_PEM_Id(pemKey, pemKeySize, 1, NULL, id) != 0 ) {
		LIBRT_FinalizarDispositivo(&hClauer);
		ret = ERR_CL;
		goto endCLIMPORT_CAPI_MY_Cert;
	}

	while ( blockNumber != -1 ) {

		if ( memcmp(BLOQUE_LLAVEPRIVADA_Get_Id(block), id, 20) == 0 ) 
		break;

		CRYPTO_SecureZeroMemory(block, TAM_BLOQUE);
		if ( LIBRT_LeerTipoBloqueCrypto(&hClauer, BLOQUE_LLAVE_PRIVADA, 0, block, &blockNumber) != 0 ) {
			LIBRT_FinalizarDispositivo(&hClauer);
			ret = ERR_CL;
			goto endCLIMPORT_CAPI_MY_Cert;
		}

	}

	CRYPTO_SecureZeroMemory(block, TAM_BLOQUE);
	if ( blockNumber == -1 ) {

		BLOQUE_Set_Cifrado(block);
		BLOQUE_LLAVEPRIVADA_Nuevo(block);
		BLOQUE_LLAVEPRIVADA_Set_Objeto(block, pemKey, pemKeySize);
		BLOQUE_LLAVEPRIVADA_Set_Tam(block, pemKeySize);
		BLOQUE_LLAVEPRIVADA_Set_Id(block, id);

		if ( LIBRT_InsertarBloqueCrypto(&hClauer, block, &blockNumber) != 0 ) {
			LIBRT_FinalizarDispositivo(&hClauer);
			ret = ERR_CL;
			goto endCLIMPORT_CAPI_MY_Cert;
		}

		bnPriv = blockNumber;
		CRYPTO_SecureZeroMemory(block,TAM_BLOQUE);
	}

	/* 3. Insertamos container y blob
	*/

	if ( LIBRT_LeerTipoBloqueCrypto(&hClauer, BLOQUE_PRIVKEY_BLOB, 1, block, &blockNumber) != 0 ) {
		LIBRT_FinalizarDispositivo(&hClauer);
		ret = ERR_CL;
		goto endCLIMPORT_CAPI_MY_Cert;
	}

	while ( blockNumber != -1 ) {
		if ( memcmp(BLOQUE_PRIVKEYBLOB_Get_Id(block), id, 20) == 0 ) 
		break;

		CRYPTO_SecureZeroMemory(block, TAM_BLOQUE);
		if ( LIBRT_LeerTipoBloqueCrypto(&hClauer, BLOQUE_PRIVKEY_BLOB, 0, block, &blockNumber) != 0 ) {
			LIBRT_FinalizarDispositivo(&hClauer);
			ret = ERR_CL;
			goto endCLIMPORT_CAPI_MY_Cert;
		}
	}

	CRYPTO_SecureZeroMemory(block, TAM_BLOQUE);
	if ( blockNumber == -1 ) {

		BLOQUE_Set_Cifrado(block);
		BLOQUE_PRIVKEYBLOB_Nuevo(block);
		BLOQUE_PRIVKEYBLOB_Set_Tam(block, dwpvkBlobSize);
		BLOQUE_PRIVKEYBLOB_Set_Id(block, id);
		BLOQUE_PRIVKEYBLOB_Set_Objeto(block, pvkBlob, dwpvkBlobSize);
		
		if ( LIBRT_InsertarBloqueCrypto(&hClauer, block, &blockNumber) != 0 ) {
			LIBRT_FinalizarDispositivo(&hClauer);
			ret = ERR_CL;
			goto endCLIMPORT_CAPI_MY_Cert;
		}

		bnBlob = blockNumber;


		/* Insertamos un container nuevo
		*/

		if ( ! CryptGenRandom(hProv, 10, (LPBYTE) containerName) ) {
			LIBRT_FinalizarDispositivo(&hClauer);
			ret = ERR_CL;
			goto endCLIMPORT_CAPI_MY_Cert;
		}
		for ( i = 0 ; i < 10; i++ ) 
		containerName[i] = 97 + ( (unsigned char)containerName[i] % 26);
		containerName[10] = 0;

		if ( LIBRT_LeerTipoBloqueCrypto(&hClauer, BLOQUE_KEY_CONTAINERS, 1, block, &blockNumber) != 0 ) {
			LIBRT_FinalizarDispositivo(&hClauer);
			ret = ERR_CL;
			goto endCLIMPORT_CAPI_MY_Cert;
		}

		while ( blockNumber != -1 ) {

			unsigned int nContainers;

			if ( BLOQUE_KeyContainer_Enumerar(block, NULL, &nContainers) != 0 ) {
				LIBRT_FinalizarDispositivo(&hClauer);
				ret = ERR_CL;
				goto endCLIMPORT_CAPI_MY_Cert;
			}

			if ( nContainers < NUM_KEY_CONTAINERS ) 
			break;

			if ( LIBRT_LeerTipoBloqueCrypto(&hClauer, BLOQUE_KEY_CONTAINERS, 0, block, &blockNumber) != 0 ) {
				LIBRT_FinalizarDispositivo(&hClauer);
				ret = ERR_CL;
				goto endCLIMPORT_CAPI_MY_Cert;
			}
		}

		if ( blockNumber == -1 ) {
			memset(block, 0, TAM_BLOQUE);
			BLOQUE_Set_Claro(block);
			BLOQUE_KeyContainer_Nuevo(block);
		}

		BLOQUE_KeyContainer_Insertar(block, containerName);
		if ( ki->dwKeySpec == AT_SIGNATURE ) {

			BLOQUE_KeyContainer_EstablecerSIGNATURE(block, containerName, -1, 0);
			BLOQUE_KeyContainer_Establecer_ID_Signature(block, containerName, idCert);

			BLOQUE_KeyContainer_EstablecerEXCHANGE(block, containerName, -1, 0);
			BLOQUE_KeyContainer_Establecer_ID_Exchange(block, containerName, zeroId);

		} else if ( ki->dwKeySpec == AT_KEYEXCHANGE ) {

			BLOQUE_KeyContainer_EstablecerEXCHANGE(block, containerName, -1, 0);
			BLOQUE_KeyContainer_Establecer_ID_Exchange(block, containerName, idCert);

			BLOQUE_KeyContainer_EstablecerSIGNATURE(block, containerName, -1, 0);
			BLOQUE_KeyContainer_Establecer_ID_Signature(block, containerName, zeroId);

		} else {
			LIBRT_FinalizarDispositivo(&hClauer);
			ret = ERR_CL;
			goto endCLIMPORT_CAPI_MY_Cert;
		}

		if ( blockNumber == -1 ) {
			if ( LIBRT_InsertarBloqueCrypto(&hClauer, block, &blockNumber) != 0 ) {
				LIBRT_FinalizarDispositivo(&hClauer);
				ret = ERR_CL;
				goto endCLIMPORT_CAPI_MY_Cert;
			}
		} else {
			if ( LIBRT_EscribirBloqueCrypto(&hClauer, blockNumber, block) != 0 ) {
				LIBRT_FinalizarDispositivo(&hClauer);
				ret = ERR_CL;
				goto endCLIMPORT_CAPI_MY_Cert;
			}
		}
	}

	LIBRT_FinalizarDispositivo(&hClauer);


	/* Si bDelete es TRUE entonces el certificado y la llave del
	* framework
	*/

	if ( bDelete ) {

		/* Borramos el container. La pol�tica es la siguiente. Si la �nica llave
		* del container es la asociada con el certificado, borramos por completo
		* el container. Si hay una llave de tipo distinto, lo que hacemos es
		* generar una nueva llave para machacar la existente (podr�amos importar
		* un blob tambi�n... ser�a m�s eficiente)
		*/

		DWORD dwKeySpec = (ki->dwKeySpec == AT_SIGNATURE) ? AT_KEYEXCHANGE : AT_SIGNATURE;
		HCRYPTKEY hKeyAux;

		if ( ! CryptGetUserKey(hProv, dwKeySpec, &hKeyAux) ) {
			if ( GetLastError() == NTE_NO_KEY ) {	
				CryptReleaseContext(hProv, 0);
				if ( ! CryptAcquireContextW(&hProv, ki->pwszContainerName, ki->pwszProvName, ki->dwProvType, CRYPT_DELETEKEYSET) ) {
					hProv = 0;
					ret = ERR_CL_CANNOT_DELETE_KEY;
					goto endCLIMPORT_CAPI_MY_Cert;
				}
			} else {
				ret = ERR_CL_CANNOT_DELETE_KEY;
				goto endCLIMPORT_CAPI_MY_Cert;
			}
		} else {
			CryptDestroyKey(hKey);
			if ( ! CryptGenKey(hProv, dwKeySpec, 512 << 16, &hKey) ) {
				ret = ERR_CL_CANNOT_DELETE_KEY;
				goto endCLIMPORT_CAPI_MY_Cert;
			}
		}

		if ( ! CertDeleteCertificateFromStore(*certCtx) ) {
			*certCtx = NULL;
			ret = ERR_CL_CANNOT_DELETE_CERT;
			goto endCLIMPORT_CAPI_MY_Cert;
		} else
		*certCtx = NULL;
	}


	endCLIMPORT_CAPI_MY_Cert:


	if ( hKey ) 
	CryptDestroyKey(hKey);

	if ( hProv ) 
	CryptReleaseContext(hProv, 0);

	if ( certPEM ) {
		CRYPTO_SecureZeroMemory(certPEM, certPEMSize);
		free(certPEM);
	}

	if ( pemKey ) {
		CRYPTO_SecureZeroMemory(pemKey, pemKeySize);
		free(pemKey);
	}

	CRYPTO_SecureZeroMemory(block, TAM_BLOQUE);
	CRYPTO_SecureZeroMemory(block2, TAM_BLOQUE);

	if ( wszFriendlyName ) 
	free(wszFriendlyName);

	if ( lpszFriendlyName )
	free(lpszFriendlyName);

	if ( ki )
	free(ki);

	/* Si hubo un error, borramos los bloques que hubi�ramos
	* podido insertar
	*/

	if ( ret != CL_SUCCESS && ret != ERR_CL_CANNOT_DELETE_KEY && ret != ERR_CL_CANNOT_DELETE_CERT ) {
		//		USBCERTS_HANDLE h;

		if ( LIBRT_IniciarDispositivo((unsigned char *)szDevice, szPwd, &hClauer) == 0 ) {
			if ( bnBlob != -1 ) 
			LIBRT_BorrarBloqueCrypto(&hClauer, bnBlob);
			if ( bnPriv != -1 )
			LIBRT_BorrarBloqueCrypto(&hClauer, bnPriv);
			if ( bnCert != -1 )
			LIBRT_BorrarBloqueCrypto(&hClauer, bnCert);

			LIBRT_FinalizarDispositivo(&hClauer);
		}
	}
	
	return ret;
}


int importaIE(int nDispositivo, const QString &password, CertificadoIE &cie, bool mover)
{
	int vuelta=0;
	char *ruta;
	
	//Conectamos con el almacen criptogr�fico de Windows
	WCHAR store[] = L"My\\.Default";
	HCERTSTORE hStore;
	if((hStore=CertOpenStore(CERT_STORE_PROV_PHYSICAL,0,0,CERT_SYSTEM_STORE_CURRENT_USER,(const void *)store))==NULL) {
		return -1;
	}
	
	//Buscamos el certificado en el almac�n del Internet Explorer
	CRYPT_HASH_BLOB hb;
	hb.pbData=(BYTE *)cie.getCertID().constData();
	hb.cbData=20;
	PCCERT_CONTEXT certCtx;
	if((certCtx=CertFindCertificateInStore(hStore,X509_ASN_ENCODING,
		0,CERT_FIND_SHA1_HASH,&hb,NULL))==NULL) {
		vuelta=-2;
		goto bailout0;
	}
	
	if((ruta=rutaClauer(nDispositivo))==NULL) {
		vuelta=-3;
		goto bailout1;
	}
	if((vuelta=CLIMPORT_CAPI_MY_Cert(ruta,(char *)password.toLatin1().constData(),&certCtx,mover))!=0 ) {
		//vuelta=-4;
		goto bailout2;
	}
	
bailout2:
	liberaRutaClauer(ruta);
bailout1:
	CertFreeCertificateContext(certCtx);
bailout0:
	CertCloseStore(hStore,0);
	return vuelta;
}


////////////////////////////////////////
#else /*!WIN32*/
int exportaIE(CertificadoX509 &x509)
{
	return -100000;
}

int leeCertificadosIE(QList<CertificadoIE> &salida)
{
	return -100000;
}

int importaIE(int nDispositivo, const QString &password, CertificadoIE &cie, bool mover)
{
	return -100000;
}
#endif /*WIN32*/

bool CertificadoIE::operator<(const CertificadoIE &cert) const
{
	//return this->titularFriendly.localeAwareCompare(cert.titularFriendly)<0;
	return this->titularFriendly.compare(cert.titularFriendly,Qt::CaseInsensitive)<0;
}
