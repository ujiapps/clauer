/****************************************************************************
**
** 	Created using Edyuk 1.0.0
**
** File : configuracion.cpp
** Date : dom 26. oct 10:53:47 2008
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
****************************************************************************/

/*!
	\file configuracion.cpp
	
	\brief Implementation of Configuración del Clauer
*/

#include "configuracion.h"

static const char
	*Kidioma="language",
	*Kstylesheet="stylesheet",
	*KweakPasswords="allowweakpasswords";


ConfiguracionClauer::ConfiguracionClauer()
{
	leer();
	escribir();
}

void ConfiguracionClauer::leer(void)
{
#ifdef WIN32
	QSettings instconfig("HKEY_LOCAL_MACHINE\\SOFTWARE\\Universitat Jaume I\\Projecte Clauer",QSettings::NativeFormat);
	idioma=instconfig.value("Idioma","1034").toString();
	bool ok;
	int idioma_code=idioma.toInt(&ok);
	if(ok) {
		switch(idioma_code) {
			case 1027:	//Catalán
				idioma="ca";
				break;
			case 1072:	//Aranés
				idioma="oc";
				break;
			case 1034:	//Castellando
			default:
				idioma="es";
		}
	}
#else
	idioma=config.value(Kidioma,"ca").toString();
#endif /*WIN32*/
	idioma=idioma.toLower();
	stylesheet=config.value(Kstylesheet,"QDialog { background-color: white }").toString();
	weakPasswords=config.value(KweakPasswords,true).toBool();
}

void ConfiguracionClauer::escribir(void)
{
#ifndef WIN32
	config.setValue(Kidioma,idioma);
#endif /*WIN32*/
	config.setValue(Kstylesheet,stylesheet);
	config.setValue(KweakPasswords,weakPasswords);
	config.sync();
}

void ConfiguracionClauer::reiniciar(void)
{
	config.clear();
	leer();
	escribir();
}
