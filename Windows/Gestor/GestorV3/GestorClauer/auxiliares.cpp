/****************************************************************************
**
** 	Created using Edyuk 1.0.0
**
** File : auxiliares.cpp
** Date : lun 6. oct 22:04:47 2008
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
****************************************************************************/

/*!
	\file auxiliares.cpp
	
	\brief Implementation of funciones auxiliares
*/

#include <QMessageBox>
#include <QInputDialog>
#include <QLabel>
#include <QFile>
#include <QFileInfo>
#include <QString>
#include <QTextStream>
#include <QRegExp>
#include <QRegExpValidator>
//#include <QCoreApplication>
#include <stdlib.h>	/*por exit()*/
#include "auxiliares.h"
#include "HLClauer.h"
#include "configuracion.h"

#ifdef DEBUG
#include <iostream>
using namespace std;
#endif /*DEBUG*/

void bailoutClauer(QWidget *parent)
{
	QMessageBox::critical(parent,
		tituloDialogos,
		QObject::tr("Imposible comunicar con el clauer. �Tenemos privilegios suficientes?"),
		QMessageBox::Ok,
		QMessageBox::Ok);
		/*QCoreApplication::*/exit(254);
}

LClauers::LClauers localizaClauer(QWidget *parent, bool retry)
{
	int nClauers,nUSBs;
	
	while(1) {
		/*
		if(QMessageBox::information(parent,
			tituloDialogos,
			"Aseg�rse de tener un �nio clauer conectado al ordenador",
			QMessageBox::Ok|QMessageBox::Cancel,
			QMessageBox::Ok)!=QMessageBox::Ok)
			return abortar;
		*/
		nClauers=nClauersConectados(true);
		nUSBs=nClauersConectados(false);
		//nUSBs=nClauers;////
		if((nClauers<0)||(nUSBs<0)) {
#ifdef DEBUG
			cerr << "localizaClauer, problema: nClauers: " << nClauers << ", nUSBs: " << nUSBs << endl;
#endif /*DEBUG*/
			bailoutClauer(parent);
		}
#ifdef DEBUG
		cerr << "localizaClauer; nClauers: " << nClauers << ", nUSBs: " << nUSBs << endl;
#endif /*DEBUG*/
		if(nUSBs>1) {
			/*Hay m�s de un clauer conectado*/
			if(!retry)
				return LClauers::varios;
			if(QMessageBox::warning(parent,
				tituloDialogos,
				QObject::tr("Hay m�s de un Stick USB insertado en el sistema\nPor favor, pulse el bot�n Aceptar y deje insertado\n�nicamente el dispostivo que desee inicializar."),
				QMessageBox::Ok|QMessageBox::Cancel,
				QMessageBox::Ok)!=QMessageBox::Ok)
				return LClauers::abortar;
		} else if(nClauers==1) {
			return LClauers::unico;
		} else {
			/*Ning�n clauer formateado*/
			/*Comprobamos si hay algun clauer sin formatear*/
#ifdef WIN32
			//Por el momento, libRT solo puede formatear en Windows
			if(nUSBs==1)
				return LClauers::formatear;
#endif /*WIN32*/
			/*Ninguna memoria USB conectada*/
			if(!retry)
				return LClauers::ninguno;
			if(QMessageBox::information(parent,
				tituloDialogos,
				QObject::tr("No hay ning�n Stick USB insertado en el sistema\nPor favor, pulse el bot�n Aceptar e inserte el dispositivo\nque desee inicializar."),
				QMessageBox::Ok|QMessageBox::Cancel,
				QMessageBox::Ok)!=QMessageBox::Ok)
				return LClauers::abortar;
		}
	}

}

void setPasswordValidator(QLineEdit *le, QObject *parent)
{
	QRegExp rx("[-.+_;:,*@#%|~$\\!?()=&/A-Za-z0-9]+");
	QValidator *validator=new QRegExpValidator(rx,parent);
	le->setValidator(validator);
}

bool isPasswordAllowed(const QString &pass)
{
	QRegExp rx("^[-.+_;:,*@#%|~$\\!?()=&/A-Za-z0-9]+");
	return rx.indexIn(pass)==0;
}

int pwdStrength(const QString &pass)
{
	int strength=0;
	
	if(pass.length()>0)
		strength+=10;
	
	if(pass.length()>4)
		strength+=10;
	
	if(pass.length()>8) {
		strength+=20;
		
		QRegExp alphaRe("[a-z]+|[A-Z]+");
		if(alphaRe.indexIn(pass)>=0) {
			strength+=20;
		}
		
		QRegExp caseRe("([A-Z].*[a-z])|([a-z].*[A-Z])");
		if(caseRe.indexIn(pass)>=0) {
			strength+=20;
		}
		
		QRegExp numRe("[0-9]");
		if(numRe.indexIn(pass)>=0) {
			strength+=10;
		}
		
		QRegExp otherRe("[-.+_;:,*/]");
		if(otherRe.indexIn(pass)>=0) {
			strength+=20;
		}
	}
	
	return strength;
}


bool validaPasswords(QWidget *parent, const QString &p1, const QString &p2)
{
	//Comprobamos que las passwords coincidan
	if(p1!=p2) {
		QMessageBox::critical(parent,tituloDialogos,
			QObject::tr("La nueva contrase�a y su confirmaci�n deben coincidir."),
			QMessageBox::Ok,
			QMessageBox::Ok);
		return false;
	}
	
	//Comprobamos que la contrase�a no contenga caracteres inv�lidos
	if(!isPasswordAllowed(p1)) {
		QMessageBox::critical(parent,tituloDialogos,
			QObject::tr("La contrase�a contiene alg�n car�cter no permitido."),
			QMessageBox::Ok,
			QMessageBox::Ok);
		return false;
	}
	
	ConfiguracionClauer config;
	
	if(config.weakPasswords) {
		if(p1.length()<4) {
			QMessageBox::critical(parent,tituloDialogos,
				QObject::tr("La contrase�a es demasiado corta, ha de tener un m�nimo de 4 caracteres."),
				QMessageBox::Ok,
				QMessageBox::Ok);
			return false;
		}
	} else {
	//Comprobamos que la password sea segura
		if(pwdStrength(p1)<42) {
			QMessageBox::critical(parent,tituloDialogos,
				QObject::tr("La nueva contrase�a es demasiado simple."),
				QMessageBox::Ok,
				QMessageBox::Ok);
			return false;
		}
	}
	
	return true;
}

/*
bool pidePassword(QWidget *parent, QString &password)
{
	bool res;
    password=QInputDialog::getText(parent,
    	tituloDialogos,
    	QObject::tr("Introduzca la contrase�a del clauer:"),
    	QLineEdit::Password,
    	"",
    	&res);
    if(res)
	    password.truncate(127);
	    
	//TODO: verificamos la password
    return res;
}
*/

QTableWidgetItem *celda(const QString &texto, const QString &tooltip)
{
	QTableWidgetItem *item=new QTableWidgetItem;
	item->setFlags(Qt::ItemIsSelectable|Qt::ItemIsEnabled);
	item->setText(texto);
	if(!tooltip.isEmpty())
		item->setToolTip(tooltip);
	return item;
}

QTableWidgetItem *celda(const QIcon &icono, const QString &tooltip)
{
	QTableWidgetItem *item=new QTableWidgetItem;
	item->setFlags(Qt::ItemIsSelectable|Qt::ItemIsEnabled);
	item->setIcon(icono);
	if(!tooltip.isEmpty())
		item->setToolTip(tooltip);
	return item;
}

bool escribeFichero(const QString &ruta, const QByteArray &datos)
{
	QFile fd(ruta);
	if(!fd.open(QIODevice::WriteOnly))
		goto bailout;
	
	if(fd.write(datos)!=datos.length())
		goto bailout;
	
	return fd.flush();
	
bailout:
	fd.remove();
	return false;
}

bool leeFichero(const QString &ruta, QByteArray &datos)
{
	QFile fd(ruta);
	if(!fd.open(QIODevice::ReadOnly))
		return false;
	
	datos=fd.readAll();
	
	QFileInfo info(ruta);
	return info.size()==datos.size();
}

bool isFileReadable(const QString &path)
{
	QFileInfo info(path);
	return info.isFile()&&info.isReadable();
}

void SecureZeroMemory(QByteArray &buffer)
{
	//CRYPTO_SecureZeroMemory((void *)buffer.constData(),buffer.length());
	buffer.fill(0x55);
	buffer.fill(0xaa);
	buffer.fill(0x00);
	buffer.clear();
}

QDateTime tm2QDateTime(const struct tm *tm)
{
	QDate fecha(tm->tm_year+1900,tm->tm_mon+1,tm->tm_mday);
	QTime hora(tm->tm_hour,tm->tm_min,tm->tm_sec);
	return QDateTime(fecha,hora,Qt::UTC);
}

/*
bool rellenaQLabel(QLabel *label, const QString &fichero)
{
	QFile fd(fichero);
	if(!fd.open(QIODevice::ReadOnly)) {
		label->setText("#ERROR#");
		return false;
	}
	
	QTextStream txt(&fd);
	txt.setCodec("UTF-8");
	label->setText(txt.readAll());
	return true;
}
*/
bool rellenaQLabel(QLabel *label, const QString &fichero)
{
	QByteArray contenido;
	if(!leeFichero(fichero,contenido)) {
		label->setText("#ERROR#");
		return false;
	}
	
	QTextStream txt(contenido);
	txt.setCodec("UTF-8");
	label->setText(txt.readAll());
	return true;
}

bool rellenaIntQLabel(QLabel *label, const QString &pfichero)
{
	ConfiguracionClauer config;
	return rellenaQLabel(label,pfichero+"_"+config.idioma+".txt");
}
