/****************************************************************************
**
** 	Created using Edyuk 1.0.0
**
** File : dialogInfoCertificado.h
** Date : lun 27. oct 18:32:51 2008
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
****************************************************************************/

#ifndef _DIALOGINFOCERTIFICADO_H_
#define _DIALOGINFOCERTIFICADO_H_

/*!
	\file dialogInfoCertificado.h
	
	\brief Definition of Diálogo de información sobre el certificado del Clauer
*/

#include <QDialog>
#include "ui_infoCertificado.h"
#include "certificados.h"

class DialogInfoCertificado:public QDialog, public Ui::InfoCertificado
{
	Q_OBJECT

public:
	DialogInfoCertificado(CertificadoX509 &cert, QWidget *parent=0);
};


#endif // !_DIALOGINFOCERTIFICADO_H_
