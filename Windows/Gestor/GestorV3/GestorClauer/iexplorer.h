/****************************************************************************
**
** 	Created using Edyuk 1.0.0
**
** File : iexplorer.h
** Date : mi� 22. oct 18:19:36 2008
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
****************************************************************************/

#ifndef _IEXPLORER_H_
#define _IEXPLORER_H_

/*!
	\file iexplorer.h
	
	\brief Definition of Utilidades para Internet Explorer
*/

#include <QString>
#include <QByteArray>
#include <QList>
#include <QDateTime>
#include "certificados.h"
#ifdef WIN32
#include <windows.h>
#include <wincrypt.h>
#define CL_DEFAULT_IMPORT_CSP       "Microsoft Enhanced Cryptographic Provider v1.0"
#define WSZ_CL_DEFAULT_IMPORT_CSP	L"Microsoft Enhanced Cryptographic Provider v1.0"
int mingw_load_crypto_func(void);
#endif /*WIN32*/

#define CL_SUCCESS                    0
#define ERR_CL                       -1000
#define ERR_CL_OUT_OF_MEMORY         -2000    /* No hay memoria suficiente (malloc devuelve NULL) */
#define ERR_CL_NO_KEY_FOUND          -3000    /* No se encontr� llave privada asociada */
#define ERR_CL_CREATING_P12          -4000    /* Error creando el pkcs12 */
#define ERR_CL_NOT_ENOUGH_BUFFER     -5000    /* Cuando no hay suficiente buffer de salida reservado */
#define ERR_CL_BAD_PARAM             -6000    /* Par�metro pasado incorrecto */
#define ERR_CL_CANNOT_INITIALIZE     -7000    /* No se pudo iniciar dispositivo */
#define ERR_CL_BAD_BLOCK_TYPE        -8000    /* El bloque le�do no es del tipo esperado  */

#define ERR_CL_KEY_NOT_EXPORTABLE           -9000    /* La llave no es exportable */
#define ERR_CL_CANNOT_GET_ASSOCIATED_KEY   -10000    /* No se pudo obtener llave asociada */
#define ERR_CL_KEY_NOT_EXISTS              -11000    /* La llave indicada no existe */
#define ERR_CL_NO_PERM                     -12000    /* No tiene privilegios suficientes para realizar operaci�n */
#define ERR_CL_CANNOT_OPEN_FILE            -13000    /* No se puede abrir fichero */
#define ERR_CL_CANNOT_DELETE_KEY           -14000    /* No se pudo borrar llave (Crypto API) */
#define ERR_CL_CANNOT_DELETE_CERT          -15000    /* No se pudo borrar certificado (Crypto API) */



//Clauer -> IExplorer
int exportaIE(CertificadoX509 &x509);

class CertificadoIE {
public:
#ifdef WIN32
	CertificadoIE(PCCERT_CONTEXT pCertContext);
	~CertificadoIE();
#endif /*WIN32*/
	bool operator<(const CertificadoIE &cert) const;
	
	inline const QByteArray &getCertID(void){return certID;};
	inline const QString &getTitular(void){return titular;};
	inline const QString &getFriendlyName(void){return friendlyName;};
	inline const QString &getTitularFriendly(void){return titularFriendly;};
	inline const QString &getEmisor(void){return emisor;};
	inline const QDateTime &getInicioValidez(void){return inicioValidez;};
	inline const QDateTime &getFinValidez(void){return finValidez;};
	
protected:
	QByteArray certID;
	QString titular;
	QString friendlyName;
	QString titularFriendly;
	QString emisor;
	
	QDateTime inicioValidez;
	QDateTime finValidez;
};

int leeCertificadosIE(QList<CertificadoIE> &salida);

int importaIE(int nDispositivo, const QString &password, CertificadoIE &cie, bool mover);

#endif // !_IEXPLORER_H_
