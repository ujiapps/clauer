/****************************************************************************
**
** 	Created using Edyuk 1.0.0
**
** File : certificados.h
** Date : s�b 18. oct 23:20:23 2008
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
****************************************************************************/

#ifndef _CERTIFICADOS_H_
#define _CERTIFICADOS_H_

/*!
	\file certificados.h
	
	\brief Clase contenedora de certificados X.509 Base64
*/

#include <QString>
#include <QByteArray>
#include <string.h>
#include <QDateTime>


//El constructor lanza una exception int(-1) en caso de error.
//Es imprescindible capturarla
class CertificadoX509 {
public:
	CertificadoX509(unsigned char *bloque, unsigned long numBloque);
	~CertificadoX509();
	bool operator<(const CertificadoX509 &cert) const;
	void setPrivateKey(unsigned char *key, int tamKey);
	void setPrivateBLOB(unsigned char *blob, int tamBlob);
	void deletePrivateKey(void);
	
	
	inline const QByteArray &getx509B64(void){return x509B64;};
	inline const QString &getTitular(void){return titular;};
	inline const QString &getFriendlyName(void){return friendlyName;};
	inline const QString &getTitularFriendly(void){return titularFriendly;};
	inline const QString &getEmisor(void){return emisor;};
	inline const QByteArray &getCertID(void){return certID;};
	inline const QByteArray &getPrivateKey(void){return key;};
	inline const QByteArray &getPrivateBLOB(void){return blob;};
	inline const unsigned long getBloque(void){return numBloque;};
	inline const QDateTime &getInicioValidez(void){return inicioValidez;};
	inline const QDateTime &getFinValidez(void){return finValidez;};
	
	bool buildPKCS12(QByteArray &destino, const QString &password);
	bool buildDER(QByteArray &destino);
	
private:
	/*unsigned char *cert;
	unsigned long tamCert;*/
	QByteArray x509B64;
	QByteArray key;
	QByteArray blob;
	
	QString titular;
	QString friendlyName;
	QString titularFriendly;
	QString emisor;
	
	QDateTime inicioValidez;
	QDateTime finValidez;
	
	QByteArray certID;
	unsigned long numBloque;
};


#endif // !_CERTIFICADOS_H_
