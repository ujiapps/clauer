/****************************************************************************
**
** 	Created using Edyuk 1.0.0
**
** File : HLClauer.h
** Date : lun 6. oct 18:50:41 2008
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
****************************************************************************/

#ifndef _HLCLAUER_H_
#define _HLCLAUER_H_

/*!
	\file HLClauer.h
	
	\brief Funciones de alto nivel para manejar el Clauer
	NOTA: En el programa principal se debe llamar a iniciaClauer() y finalizaClauer()
*/

#include <QString>
#include <QList>
#include "certificados.h"
#include "LIBRT/libRT.h"
#include "UtilBloques/UtilBloques.h"


/*Inicializa la libreria RT del Clauer
 *devuelve 0 en caso de �xito
 */
int iniciaClauer(void);

/*Finaliza la libreria RT del clauer
 *devuelve 0 en caso de �xito
 */
int finalizaClauer(void);

char *rutaClauer(int nDispositivo);
void liberaRutaClauer(char *);


/*Devuelve el n�mero de clauers conectados en el sistema, o negtivo en caso de error
 *
 *Par�metros:
 *	soloFormateados:
 *		0: busca todos los llaveros USB
 *		!=0: contabiliza unicamenet los clauers formateados
 */
int nClauersConectados(bool soloFormateados);

/*Determina si el clauer indicado exige cambio de password
 *Retorno:
 *	<0 en caso de error
 *	 0 no hay que cambiar el password
 *	>0 hay que cambiar el password
*/
int clauerExigeCambioPassword(int nDispositivo);


/*Formatea la partici�n criptogr�fica del clauer indicado
 *
 *Par�metros:
 *	nDispositivo: n�mero de dispositivo
 *	password: contrase�a
 *
 *Retorno:
 *	<0 en caso de error
 *	0 en caso de �xito
*/
int formateaClauer(int nDispositivo, const QString &password);


/*Particiona el clauer indicado
 *
 *Parametros:
 * nDispositivo: n�mero de dispositivo
 * porcentaje: tama�o porcentual de la partici�n de usuario.
 *		Si se indica 0, se asignan unos 5MB
 *
 *Retorno:
 *	<0 en caso de error
 *	0 en caso de �xito
*/
int particionaClauer(int nDispositivo, int porcentaje=0);

/*Elimina la partici�n criptogr�fica del Clauer extendiendo
 *la partici�n de datos
 *
 *Par�metros:
 *	nDispositivo: n�mero de dispositivo
 *
 *Retorno:
 *	<0 en caso de error
 *	0 en caso de �xito
*/
int destruyeClauer(int nDispositivo);


/*Prepara un clauer eliminando todo el contenido de la memoria USB indicada
 *
 *Par�metros:
 *	nDispositivo: n�mero de dispositivo
 *	porcentaje: tama�o porcentual de la partici�n de usuario.
 *		Si se indica 0, se asignan unos 5MB
 *
 *Retorno:
 *	<0 en caso de error
 *	0 en caso de �xito
*/
int creaClauer(int nDispositivo, const QString &password, int porcentaje=0);

/*Cambia la contrase�a del Clauer
 *
 *Parametros:
 *	nDispositivo: n�mero de dispositivo
 *	old_password: contrase�a antigua
 *	new_password: nueva contrase�a
 *
 *Retorno:
 *	<0 en caso de error
 *	0 en caso de �xito
*/
int cambiaPasswordClauer(int nDispositivo, const QString &old_password, const QString &new_password);

long buscaBloqueClauer(int nDispositivo, TIPO_BLOQUE tipo, const QString *password, CertificadoX509 &cert);
long buscaBloqueClauer(USBCERTS_HANDLE *usbcerts, TIPO_BLOQUE tipo, CertificadoX509 &cert);

int leeCertificadosClauer(int nDispositivo, const QString *password, QList<CertificadoX509> &salida);

int leePrivadaClauer(int nDispositivo, const QString &password, CertificadoX509 &cert);

int leeBLOBClauer(int nDispositivo, const QString &password, CertificadoX509 &cert);

int borraCertificadoClauer(int nDispositivo, const QString &password, CertificadoX509 &cert);


/////

int importaPKCS12(int nDispositivo, const QString *passwordClauer, const QString &archivo, const QString &passwordCert);



#endif // !_HLCLAUER_H_
