/****************************************************************************
**
** 	Created using Edyuk 1.0.0
**
** File : auxiliares.h
** Date : lun 6. oct 22:04:30 2008
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
****************************************************************************/

#ifndef _AUXILIARES_H_
#define _AUXILIARES_H_

/*!
	\file auxiliares.h
	
	\brief Definition of funciones auxiliares
*/

#include <QWidget>
#include <QString>
#include <QTableWidgetItem>
#include <QIcon>
#include <QDateTime>
#include <QLineEdit>
#include <QLabel>
#include "time.h"

#define tituloDialogos QObject::tr("Gestor del Clauer")



/*Para llamar cuando haya alg�n problema critico con el Clauer
 *Muestra un mensaje de error al usuario y finaliza la aplicaci�n
*/
void bailoutClauer(QWidget *parent);

namespace LClauers {
	enum LClauers {unico, formatear, ninguno, varios, abortar};
}

/*Para llamar al comienzo del programa: determina qu� hacer con los clauers
 *Retorno:
 *	0: un �nico clauer identifciado y localizado
 *	1: hay que formatear el clauer
 * -1: abortar
*/
LClauers::LClauers localizaClauer(QWidget *parent, bool retry=true);

void setPasswordValidator(QLineEdit *le, QObject *parent);

bool isPasswordAllowed(const QString &pass);

int pwdStrength(const QString &pass);

/*Determina la validez de la contrase�a, mostrando en caso de error
 *un QMessageBox apropiado
 *
 *Par�metros:
 *	parent:
 *	p1: contrase�a
 *	p2: confirmaci�n de la contrase�a
 *
 *Retorno:
 *	true en caso de ser aceptada, false en caso contrario
*/
bool validaPasswords(QWidget *parent, const QString &p1, const QString &p2);

/*Muestra un di�logo pidiendo al usuario la contrase�a del Clauer
 *
 *Par�metros:
 *	parent:
 *	password: QString que almacenara la contrase�a
 *
 *Retorno:
 *	true si el usuario ha seleccionado Aceptar, false en caso contrario
*/
//bool pidePassword(QWidget *parent, QString &password);

/*Crea un nuevo QTableWidgetItem con el texto indicado*/
QTableWidgetItem *celda(const QString &texto, const QString &tooltip="");

/*Crea un nuevo QTableWidgetItem con el icono indicado*/
QTableWidgetItem *celda(const QIcon &icono, const QString &tooltip="");

/*Graba el contenido en el fichero indicado
 *
 *Par�metros:
 *	ruta: ruta al fichero
 *	datos: buffer con el contenido a grabar
 *
 *Retorno:
 *	true en caso de �xito, false en caso de error
 *
*/

bool escribeFichero(const QString &ruta, const QByteArray &datos);
/*Lee el contenido en el fichero indicado
 *
 *Par�metros:
 *	ruta: ruta al fichero
 *	datos: buffer que albergara el contenido del fichero
 *
 *Retorno:
 *	true en caso de �xito, false en caso de error
 *
*/
bool leeFichero(const QString &ruta, QByteArray &datos);

bool isFileReadable(const QString &path);

void SecureZeroMemory(QByteArray &buffer);

QDateTime tm2QDateTime(const struct tm *tm);

bool rellenaQLabel(QLabel *label, const QString &fichero);

bool rellenaIntQLabel(QLabel *label, const QString &pfichero);

#endif // !_AUXILIARES_H_
