/****************************************************************************
**
** 	Created using Edyuk 1.0.0
**
** File : certificados.cpp
** Date : s�b 18. oct 23:22:47 2008
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
****************************************************************************/

/*!
	\file certificados.cpp
	
	\brief Implementation of Clase contenedora de certificados X.509 Base64
*/

#include "certificados.h"
#include "CRYPTOWrapper/CRYPTOWrap.h"
#include "auxiliares.h"
#include "UtilBloques/UtilBloques.h"
#include <string.h>

#ifdef DEBUG
#include <iostream>
using namespace std;
#endif /*DEBUG*/

CertificadoX509::CertificadoX509(unsigned char *bloque, unsigned long numBloque)
{
	unsigned char *cert=BLOQUE_CERTPROPIO_Get_Objeto(bloque);
	unsigned long tamCert=BLOQUE_CERTPROPIO_Get_Tam(bloque);
	unsigned char *certID=BLOQUE_CERTPROPIO_Get_Id(bloque);
	char *friendlyName=BLOQUE_CERTPROPIO_Get_FriendlyName(bloque);
	
	this->certID=QByteArray((const char *)certID,20);
	this->numBloque=numBloque;
	this->x509B64=QByteArray((const char *)cert,tamCert);
	
	DN *subject,*issuer;
	if((subject=CRYPTO_DN_New())==NULL)
		goto bailout0;
	if((issuer=CRYPTO_DN_New())==NULL)
		goto bailout1;
	if(!CRYPTO_CERT_SubjectIssuer(cert,tamCert,subject,issuer))
		goto bailout2;
	
	//char *friendlyName=BLOQUE_CERTPROPIO_Get_FriendlyName(bloque);
	this->titular=QString(subject->CN);
	this->titularFriendly=this->titular;
	if(*friendlyName) {
		this->friendlyName=QString(friendlyName);
		this->titularFriendly+=" ("+this->friendlyName+")";
	}
	this->emisor=QString(issuer->O);
	 
	CRYPTO_DN_Free(issuer);
	CRYPTO_DN_Free(subject);
	
	struct tm tinicio,tfin;
	if(!CRYPTO_CERT_Dates(cert,tamCert,&tinicio,&tfin))
		goto bailout0;
	inicioValidez=tm2QDateTime(&tinicio);
	finValidez=tm2QDateTime(&tfin);
	
	//cerr << ":: " << titular.toLatin1().constData() << " / " << subject->O << endl;
	return;

bailout2:
	CRYPTO_DN_Free(issuer);
bailout1:
	CRYPTO_DN_Free(subject);
bailout0:
	throw(-1);
}

CertificadoX509::~CertificadoX509()
{
	/*if(this->cert!=NULL)
		delete [] this->cert;*/
	this->deletePrivateKey();
}

bool CertificadoX509::operator<(const CertificadoX509 &cert) const
{
	//return this->titular.localeAwareCompare(cert.titular)<0;
	return this->titular.compare(cert.titular,Qt::CaseInsensitive)<0;
}

void CertificadoX509::setPrivateKey(unsigned char *key, int tamKey)
{
	this->deletePrivateKey();
	this->key=QByteArray((const char *)key,tamKey);
}

void CertificadoX509::deletePrivateKey(void)
{
	/*Esto no es nada ortodoxo, pero bueno*/
	SecureZeroMemory(this->key);
	SecureZeroMemory(this->blob);
}

void CertificadoX509::setPrivateBLOB(unsigned char *blob, int tamBlob)
{
	this->blob=QByteArray((const char *)blob,tamBlob);
}

bool CertificadoX509::buildPKCS12(QByteArray &destino, const QString &password)
{
	if(this->key.isEmpty())
		return false;
	
	//Determinamos cu�nto ocupar� el objeto PKCS#12
	unsigned long pk12Tam;
	if(CRYPTO_PKCS12_Crear(
		(unsigned char *)this->key.constData(),
		this->key.length(),
		NULL,
		(unsigned char *)this->x509B64.constData(),
		this->x509B64.length(),
		NULL,
		NULL,
		0,
		(char *)password.toLatin1().constData(),
		(char *)this->friendlyName.toLatin1().constData(),
		NULL,
		&pk12Tam)!=0) {
 	    return false;
 	}
#ifdef DEBUG
 	cerr << "buildPKCS12 tam: " << pk12Tam << endl;
#endif /*DEBUG*/
 	
 	unsigned char *pk12;
 	if((pk12=new(std::nothrow) unsigned char[pk12Tam])==NULL)
 		return false;

	//Componemos el PKCS#12
	bool vuelta=false;
 	if(CRYPTO_PKCS12_Crear(
		(unsigned char *)this->key.constData(),
		this->key.length(),
		NULL,
		(unsigned char *)this->x509B64.constData(),
		this->x509B64.length(),
		NULL,
		NULL,
		0,
		(char *)password.toLatin1().constData(),
		(char *)this->friendlyName.toLatin1().constData(),
		pk12,
		&pk12Tam)==0) {
	 	destino=QByteArray((const char *)pk12,pk12Tam);
 	    vuelta=true;
 	}
 	
 	CRYPTO_SecureZeroMemory(pk12,pk12Tam);
 	delete [] pk12;

	return vuelta;
}

bool CertificadoX509::buildDER(QByteArray &destino)
{
	//Determinamos cu�nto ocupara el DER
	unsigned long derTam;
	if(CRYPTO_X509_PEM2DER(
	(unsigned char *)this->x509B64.constData(),
		this->x509B64.length(),
		NULL,
		&derTam)!=0) {
		return false;
	}
	unsigned char *der;
	if((der=new(std::nothrow) unsigned char[derTam])==NULL)
 		return false;
 	
 	//Componemos el DER
 	bool vuelta=false;
 	if(CRYPTO_X509_PEM2DER(
 		(unsigned char *)this->x509B64.constData(),
		this->x509B64.length(),
		der,
		&derTam)==0) {
		destino=QByteArray((const char *)der,derTam);
 	    vuelta=true;
		}
	
	delete [] der;
	
	return vuelta;
}