/****************************************************************************
**
** 	Created using Edyuk 1.0.0
**
** File : configuracion.h
** Date : dom 26. oct 10:53:39 2008
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
****************************************************************************/

#ifndef _CONFIGURACION_H_
#define _CONFIGURACION_H_

/*!
	\file configuracion.h
	
	\brief Definition of Configuraci�n del Clauer
*/

#include <QSettings>
#include <QString>

class ConfiguracionClauer {
public:
	ConfiguracionClauer();
	void leer(void);
	void escribir(void);
	void reiniciar(void);
	
	
	QString idioma; //C�digo de idioma de 2 letras: es, ca, oc, ...
	QString stylesheet;	//Hoja de Estilo QT
	bool weakPasswords;	//�Permitir contrase�as d�biles?
	
protected:
	QSettings config;
};

#endif // !_CONFIGURACION_H_
