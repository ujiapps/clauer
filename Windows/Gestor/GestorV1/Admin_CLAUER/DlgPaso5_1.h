#if !defined(AFX_DLGPASO5_1_H__EC139F9F_16CB_46BC_8959_1B7B42C4E79D__INCLUDED_)
#define AFX_DLGPASO5_1_H__EC139F9F_16CB_46BC_8959_1B7B42C4E79D__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// DlgPaso5_1.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// DlgPaso5_1 dialog

class DlgPaso5_1 : public CDialog
{
// Construction
public:
	CString GetPIN(void);
	DlgPaso5_1(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(DlgPaso5_1)
	enum { IDD = IDD_DIALOG_PASO_5_1 };
	CButton	m_cuadro;
	CStatic	m_pin;
	CStatic	m_texto2;
	CStatic	m_texto;
	CString	m_editPIN;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(DlgPaso5_1)
	public:
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(DlgPaso5_1)
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DLGPASO5_1_H__EC139F9F_16CB_46BC_8959_1B7B42C4E79D__INCLUDED_)
