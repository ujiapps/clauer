// DlgPaso4.cpp : implementation file
//

#include "stdafx.h"
#include "Admin_CLAUER.h"
#include "DlgPaso4.h"
#include "Admin_CLAUERDlg.h"
#include "IDIOMA.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// DlgPaso4 dialog


DlgPaso4::DlgPaso4(CWnd* pParent /*=NULL*/)
	: CDialog(DlgPaso4::IDD, pParent)
{
	//{{AFX_DATA_INIT(DlgPaso4)
	m_radio = -1;
	//}}AFX_DATA_INIT
}


void DlgPaso4::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(DlgPaso4)
	DDX_Control(pDX, IDC_UBI, m_ubi);
	DDX_Control(pDX, IDC_IE, m_ie);
	DDX_Control(pDX, IDC_FIN, m_fin);
	DDX_Control(pDX, IDC_DISQ, m_disq);
	DDX_Control(pDX, IDC_CUADRO, m_cuadro);
	DDX_Control(pDX, IDC_TEXTO, m_texto);
	DDX_Radio(pDX, IDC_RADIO_IMP, m_radio);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(DlgPaso4, CDialog)
	//{{AFX_MSG_MAP(DlgPaso4)
	ON_WM_SHOWWINDOW()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// DlgPaso4 message handlers

BOOL DlgPaso4::OnInitDialog() 
{
	CDialog::OnInitDialog();
	m_radio = 3;
	UpdateData(FALSE);
	// TODO: Add extra initialization here

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void DlgPaso4::OnShowWindow(BOOL bShow, UINT nStatus) 
{
	CDialog::OnShowWindow(bShow, nStatus);
	
	// TODO: Add your message handler code here

	CFont *font = new CFont;
	font->CreatePointFont(12, "System");
	m_texto.SetFont(font);
	m_texto.SetWindowText(IDIOMA_Get(IDIOMA_PASO4_NEGRITA));
	m_cuadro.SetWindowText(IDIOMA_Get(IDIOMA_PASO4_OPC));
	m_ubi.SetWindowText(IDIOMA_Get(IDIOMA_PASO4_UBIC));
	m_ie.SetWindowText(IDIOMA_Get(IDIOMA_PASO4_IE));
	m_fin.SetWindowText(IDIOMA_Get(IDIOMA_PASO4_FIN));
	m_disq.SetWindowText(IDIOMA_Get(IDIOMA_PASO4_DISQ));

	delete font;
}

BOOL DlgPaso4::PreTranslateMessage(MSG* pMsg) 
{
	// TODO: Add your specialized code here and/or call the base class
	if(pMsg->message==WM_KEYDOWN)
	{
		if ( (pMsg->wParam==VK_ESCAPE) )
			pMsg->wParam=NULL ;
		else if ( (pMsg->wParam==VK_RETURN) ) {
			((CAdmin_CLAUERDlg *) this->GetParent())->OnBotonSiguiente();
			return TRUE;
		}
	}
	return CDialog::PreTranslateMessage(pMsg);
}
