// DlgSeguro.cpp : implementation file
//

#include "stdafx.h"
#include "Admin_CLAUER.h"
#include "DlgSeguro.h"
#include "IDIOMA.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// DlgSeguro dialog


DlgSeguro::DlgSeguro(CWnd* pParent /*=NULL*/)
	: CDialog(DlgSeguro::IDD, pParent)
{
	//{{AFX_DATA_INIT(DlgSeguro)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}


void DlgSeguro::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(DlgSeguro)
	DDX_Control(pDX, IDOK, m_ok);
	DDX_Control(pDX, IDCANCEL, m_cancel);
	DDX_Control(pDX, IDC_TEXTO, m_text);
	DDX_Control(pDX, IDC_PREG, m_preg);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(DlgSeguro, CDialog)
	//{{AFX_MSG_MAP(DlgSeguro)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// DlgSeguro message handlers

BOOL DlgSeguro::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here

	m_text.SetWindowText(IDIOMA_Get(IDIOMA_DIALOG_SEGURO_TEXTO));
	m_preg.SetWindowText(IDIOMA_Get(IDIOMA_DIALOG_SEGURO_PREGUNTA));
	m_ok.SetWindowText(IDIOMA_Get(IDIOMA_DIALOG_SEGURO_SI));
	m_cancel.SetWindowText(IDIOMA_Get(IDIOMA_DIALOG_SEGURO_NO));

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}
