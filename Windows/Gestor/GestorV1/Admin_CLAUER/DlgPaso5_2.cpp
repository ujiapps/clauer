// DlgPaso5_2.cpp : implementation file
//

#include "stdafx.h"
#include "Admin_CLAUER.h"
#include "DlgPaso5_2.h"
#include "Admin_CLAUERDlg.h"
#include "IDIOMA.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// DlgPaso5_2 dialog


DlgPaso5_2::DlgPaso5_2(CWnd* pParent /*=NULL*/)
	: CDialog(DlgPaso5_2::IDD, pParent)
{
	//{{AFX_DATA_INIT(DlgPaso5_2)
	m_ruta = _T("");
	//}}AFX_DATA_INIT
}


void DlgPaso5_2::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(DlgPaso5_2)
	DDX_Control(pDX, IDC_DESCRIPCION, m_desc);
	DDX_Control(pDX, IDC_TEXT, m_texto);
	DDX_Text(pDX, IDC_RUTA, m_ruta);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(DlgPaso5_2, CDialog)
	//{{AFX_MSG_MAP(DlgPaso5_2)
	ON_BN_CLICKED(IDC_BOTON_RUTA, OnBotonRuta)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// DlgPaso5_2 message handlers

BOOL DlgPaso5_2::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here
	CFont *font = new CFont;
	font->CreatePointFont(12, "System");
	m_texto.SetFont(font);

	m_texto.SetWindowText(IDIOMA_Get(IDIOMA_PASO5_2_NEGRITA));
	m_desc.SetWindowText(IDIOMA_Get(IDIOMA_PASO5_2_CUADRO));

	delete font;
	m_ruta.Empty();
	UpdateData(FALSE);
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

BOOL DlgPaso5_2::PreTranslateMessage(MSG* pMsg) 
{
	// TODO: Add your specialized code here and/or call the base class
	if(pMsg->message==WM_KEYDOWN)
	{
		if ( (pMsg->wParam==VK_ESCAPE) )
			pMsg->wParam=NULL ;
		else if ( (pMsg->wParam==VK_RETURN) ) {
			if (m_ruta.IsEmpty()) {
				pMsg->wParam=NULL ;
				AfxMessageBox("Debe seleccionar un certificado");
			}else {

				((CAdmin_CLAUERDlg *) this->GetParent())->OnBotonSiguiente();
			}
			//((CAdmin_CLAUERDlg *) AfxGetApp())->OnBotonSiguiente();
			return TRUE;
		}
	}
	return CDialog::PreTranslateMessage(pMsg);
}

void DlgPaso5_2::OnBotonRuta() 
{

	// TODO: Add your control notification handler code here

	/*	the only real sulution (without recompiling the MFC) is change in
..\VC98\MFC\Include\AFXDLGS.H the OPENFILENAME to somthing like this:

#ifdef _WIN32_WINNT>0x0400
OPENFILENAME_NT4  m_ofn;
#else
OPENFILENAME  m_ofn;
#endif
*/

	CFileDialog dialog( TRUE, NULL, NULL, OFN_HIDEREADONLY, "PKCS12 (*.p12)|*.p12| Personal Information Exchange (*.pfx)|*.pfx||", this);
	dialog.m_ofn.lpstrTitle = "Elija el certificado";
	int iRet = dialog.DoModal();
	if ( iRet == IDOK)
	{
		m_ruta = dialog.GetPathName();
		UpdateData(FALSE);
		}else {
		SetFocus();
	}
}
