// Admin_CLAUER.cpp : Defines the class behaviors for the application.
//

#include "stdafx.h"
#include "Admin_CLAUER.h"
#include "Admin_CLAUERDlg.h"
//#include <LIBUPDATE.h>
#include "IDIOMA.h"

#include <USBLowLevel/usb_lowlevel.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CAdmin_CLAUERApp

BEGIN_MESSAGE_MAP(CAdmin_CLAUERApp, CWinApp)
	//{{AFX_MSG_MAP(CAdmin_CLAUERApp)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG
	ON_COMMAND(ID_HELP, CWinApp::OnHelp)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CAdmin_CLAUERApp construction

CAdmin_CLAUERApp::CAdmin_CLAUERApp()
{
	// TODO: add construction code here,
	// Place all significant initialization in InitInstance
}

/////////////////////////////////////////////////////////////////////////////
// The one and only CAdmin_CLAUERApp object

CAdmin_CLAUERApp theApp;

/////////////////////////////////////////////////////////////////////////////
// CAdmin_CLAUERApp initialization

BOOL CAdmin_CLAUERApp::InitInstance()
{
	// Standard initialization
	// If you are not using these features and wish to reduce the size
	//  of your final executable, you should remove from the following
	//  the specific initialization routines you do not need.

#ifdef _AFXDLL
	Enable3dControls();			// Call this when using MFC in a shared DLL
#else
	Enable3dControlsStatic();	// Call this when linking to MFC statically
#endif
    
	int version;
	if(!IDIOMA_Cargar()) {
		AfxMessageBox(IDIOMA_Get(IDIOMA_NO_DETERMINAR_IDIOMA));
		return FALSE;
	}
//	if(LIBUPDATE_TerminarPrograma (GetForegroundWindow(), GetModuleHandle(NULL)))
//			return FALSE;
	version=lowlevel_get_winversion();
	if (version == 3){
		AfxMessageBox(IDIOMA_Get(IDIOMA_NO_NT_VIEJO));
	}else if(version == 4){
		DlgWin98 dlg_win98;
		m_pMainWnd = &dlg_win98;
		AfxMessageBox(IDIOMA_Get(IDIOMA_ADVERTENCIA_CAMBIO_PIN));
		int nResponse = dlg_win98.DoModal();
			if (nResponse == IDOK)
			{
				// TODO: Place code here to handle when the dialog is
				//  dismissed with OK
			}
			else if (nResponse == IDCANCEL)
			{
				// TODO: Place code here to handle when the dialog is
				//  dismissed with Cancel
			}
	}else{
	CAdmin_CLAUERDlg dlg;
	m_pMainWnd = &dlg;
	int nResponse = dlg.DoModal();
	if (nResponse == IDOK)
	{
		// TODO: Place code here to handle when the dialog is
		//  dismissed with OK
	}
	else if (nResponse == IDCANCEL)
	{
		// TODO: Place code here to handle when the dialog is
		//  dismissed with Cancel
	}

	// Since the dialog has been closed, return FALSE so that we exit the
	//  application, rather than start the application's message pump.
	}
	return FALSE;
}
