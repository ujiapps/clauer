#if !defined(AFX_DLGCONTRAS_H__7E79C655_D90F_4561_86CC_39F7315C9DC3__INCLUDED_)
#define AFX_DLGCONTRAS_H__7E79C655_D90F_4561_86CC_39F7315C9DC3__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// DlgContras.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// DlgContras dialog

class DlgContras : public CDialog
{
// Construction
public:
	DlgContras(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(DlgContras)
	enum { IDD = IDD_DIALOG_CONTRAS };
	CStatic	m_texto;
	CButton	m_cuadro;
	CStatic	m_contr;
	CButton	m_ok;
	CButton	m_cancel;
	CString	m_pin;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(DlgContras)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(DlgContras)
	afx_msg void OnShowWindow(BOOL bShow, UINT nStatus);
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedOk();
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DLGCONTRAS_H__7E79C655_D90F_4561_86CC_39F7315C9DC3__INCLUDED_)
