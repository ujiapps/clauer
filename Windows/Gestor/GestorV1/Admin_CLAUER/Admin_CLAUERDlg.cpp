// Admin_CLAUERDlg.cpp : implementation file
//

#include "stdafx.h"
#include "Admin_CLAUER.h"
#include "Admin_CLAUERDlg.h"
#include "thread.h"
#include "IDIOMA.h"

#include <CRYPTOWrapper/CRYPTOWrap.h>
#include <libFormat/usb_format.h>
#include <USBLowLevel/usb_lowlevel.h>
#include "Excepciones.h"
#include <LIBIMPORT/LIBIMPORT.h>
#include <LIBRT/LIBRT.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CAdmin_CLAUERDlg dialog

CAdmin_CLAUERDlg::CAdmin_CLAUERDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CAdmin_CLAUERDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CAdmin_CLAUERDlg)
	//}}AFX_DATA_INIT
	// Note that LoadIcon does not require a subsequent DestroyIcon in Win32
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CAdmin_CLAUERDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CAdmin_CLAUERDlg)
	DDX_Control(pDX, IDB_BITMAP1, m_pnlIzquierdo);
	DDX_Control(pDX, IDC_BOTON_CANCEL, m_botonCancelar);
	DDX_Control(pDX, IDC_BOTON_SIGUIENTE, m_botonSiguiente);
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CAdmin_CLAUERDlg, CDialog)
	//{{AFX_MSG_MAP(CAdmin_CLAUERDlg)
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDC_BOTON_CANCEL, OnBotonCancel)
	ON_BN_CLICKED(IDC_BOTON_SIGUIENTE, OnBotonSiguiente)
	ON_WM_TIMER()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CAdmin_CLAUERDlg message handlers

BOOL CAdmin_CLAUERDlg::OnInitDialog()
{
	CDialog::OnInitDialog();


//myButton.Create(_T("My button"), WS_CHILD|WS_VISIBLE|BS_ICON, CRect(10,10,60,50), , 1);
	//myButton.SetIcon( ::LoadIcon(NULL, IDI_ERROR) );


	//** ANTES DE QUE EMPIECE A EJECUTARSE LA APLICACI�N			**//
	//** HAY QUE TENER EN CUENTA QUE NO ESTE YA EN EJECUCION		**//
	//** Para esto crearemos un fichero temporal, 					**//
	//** y al acabar lo borraremos									**//
	//** De esta forma antes de crearlo, comprobaremos si el 		**//
	//** fichero existe, si no existe, no hay problema, continuamos	**//
	//** pero si existe puede ser por 2 razones,					**//
	//** 1. que la apli se este ejecutando o						**//
	//** 2. que haya fallado y no se ha podido borrar				**//
	//** Para saber cual de las 2 opciones es lo intentamos borrar	**//
	//** Si se puede borrar es que la apli no est� en ejecuci�n		**//										**//
	//** Si no se puede borrar es que ya tenemos la apli abierta.	**//
	//** Por tanto, si el fichero existe y no se puede borrar:		**//
	//** No podemos entrar de forma correcta, FALLO					**//
	
	char * id;

	id=getenv( "UserProfile" );

	fich_temp = (string(id) + string("\\fich_temporal_lock.tmp")).c_str();

	hFile=CreateFile(TEXT(fich_temp.GetBuffer(0)), GENERIC_WRITE,0,NULL, CREATE_NEW,FILE_ATTRIBUTE_NORMAL, NULL);
	if (GetLastError()==80)
	{
		if (DeleteFile(fich_temp.GetBuffer(0))==0)
		{
			MessageBox(IDIOMA_Get(IDIOMA_FICHERO_ABIERTO), NULL, MB_ICONERROR|MB_OK);
			exit(1);
		}
		else
		{
			hFile = CreateFile(TEXT(fich_temp.GetBuffer(0)),GENERIC_WRITE,FILE_SHARE_WRITE,						
                   NULL,CREATE_ALWAYS,			
                   FILE_ATTRIBUTE_NORMAL,	
                   NULL);
			if (hFile == INVALID_HANDLE_VALUE)
			{
				MessageBox(IDIOMA_Get(IDIOMA_CREAR_FICHERO), NULL, MB_ICONERROR|MB_OK);
			}
		}

	}



	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon
	
	// TODO: Add extra initialization here


	
		// Esto lo he adaptado de: http://www.codeproject.com/dialog/ChangeContDlg.asp

	CWnd* pWnd = GetDlgItem( IDC_FRAME );
    CRect rect;
    pWnd->GetWindowRect( &rect );
    ScreenToClient( &rect );

	// Estas ventanas siempre estar�n creadas mientras la apliaci�n est� en
	// ejecuci�n

	m_dlgPaso1.Create(IDD_DIALOG_PASO_1, this);
	m_dlgPaso1.ShowWindow(WS_VISIBLE|WS_CHILD);
	m_dlgPaso1.SetWindowPos(pWnd, rect.left, rect.top, rect.right, rect.bottom, SWP_SHOWWINDOW);
	m_dlgPaso1.ShowWindow(SW_SHOW);

	m_dlgPaso3.Create(IDD_DIALOG_PASO_3, this);
	m_dlgPaso3.ShowWindow(WS_VISIBLE|WS_CHILD);
	m_dlgPaso3.SetWindowPos(pWnd, rect.left, rect.top, rect.right, rect.bottom, SWP_HIDEWINDOW);
	
	m_dlgPaso4.Create(IDD_DIALOG_PASO_4, this);
	m_dlgPaso4.ShowWindow(WS_VISIBLE|WS_CHILD);
	m_dlgPaso4.SetWindowPos(pWnd, rect.left, rect.top, rect.right, rect.bottom, SWP_HIDEWINDOW);

	m_dlgPaso5_6.Create(IDD_DIALOG_PASO_5_6, this);
	m_dlgPaso5_6.ShowWindow(WS_VISIBLE|WS_CHILD);
	m_dlgPaso5_6.SetWindowPos(pWnd, rect.left, rect.top, rect.right, rect.bottom, SWP_HIDEWINDOW);
	// Inicializamos el resto de variables

	m_paso = 1;

	this->SetWindowText("Gestor del Clauer");

	m_botonSiguiente.SetWindowText(IDIOMA_Get(IDIOMA_BOTON_SIGUIENTE));
	m_botonCancelar.SetWindowText(IDIOMA_Get(IDIOMA_BOTON_CANCEL));

	return TRUE;  // return TRUE  unless you set the focus to a control
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CAdmin_CLAUERDlg::OnPaint() 
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, (WPARAM) dc.GetSafeHdc(), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

// The system calls this to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CAdmin_CLAUERDlg::OnQueryDragIcon()
{
	return (HCURSOR) m_hIcon;
}

void CAdmin_CLAUERDlg::OnCancel() 
{
	// TODO: Add extra cleanup here
	
	CDialog::OnCancel();
}

void CAdmin_CLAUERDlg::OnOK() 
{
	// TODO: Add extra validation here
	
	CDialog::OnOK();

}

void CAdmin_CLAUERDlg::OnBotonCancel() 
{
	// TODO: Add your control notification handler code here
	if(m_paso == 7 || m_paso == 8 || m_paso == 9){
		m_botonSiguiente.SetWindowText(IDIOMA_Get(IDIOMA_BOTON_SIGUIENTE));
		SetPaso(4);
	}else if(m_paso == 3){
		m_botonSiguiente.SetWindowText(IDIOMA_Get(IDIOMA_BOTON_SIGUIENTE));
		SetPaso(2);
	}else if (m_paso == 21 || m_paso == 22 || m_paso == 28 || m_paso == 29){
		SetPaso(20);
	}else if(m_paso == 23){
		SetPaso(22);
	}else if(m_paso == 24 || m_paso == 25 || m_paso == 26){
		SetPaso(23);
	}else {
		EndDialog(IDOK);
	}
}

void CAdmin_CLAUERDlg::OnBotonSiguiente() 
{
	// TODO: Add your control notification handler code here
	int numDispositivos;
	CString pin, pin_nuevo, confirmacion, opcion;
			
	int es, result;
	int j=0;

	HANDLE_DISPOSITIVO handle;
	USBCERTS_HANDLE handle_certs;

	char ** unidades = NULL;
	int *dispositivos = NULL;

	byte *dispos = NULL;

	int nDispositivos;

	int nResponse;

	switch ( m_paso ) {

	case 1:

		/* Comprobamos que el n�mero de sticks insertado sea el correcto (exactamente 1)
		 */

		LIBRT_Ini();

		LIBRT_ListarDispositivos(&nDispositivos,&dispos);
		
		LIBRT_Fin ();

		if(nDispositivos >= 1){
			this->numDispositivos=nDispositivos;
			SetPaso(20);
			break;
		}
		
		if ( format_enumerar_dispositivos(NULL, NULL, &numDispositivos) != 0 ) {
			CWnd::MessageBox(IDIOMA_Get(IDIOMA_NO_ENCONTRAR_STICK), "Clauer :: ERROR :: Buscando sticks", MB_ICONERROR);
			break;
		}

		if ( numDispositivos == 0 ) {
			CString msg;
			msg = "No hay ning�n Stick USB insertado en el sistema\n";
			msg += "Por favor, pulse el bot�n Aceptar e inserte el dispositivo\n";
			msg += "que desee inicializar";
			CWnd::MessageBox(IDIOMA_Get(IDIOMA_NO_HAY_STICK), "Clauer :: ERROR :: Inserte Stick USB", MB_ICONERROR);
			break;
		} else if ( numDispositivos > 1 ) {
			CString msg;
			msg = "Hay m�s de un Stick USB insertado en el sistema\n";
			msg += "Por favor, pulse el bot�n Aceptar y deje insertado\n";
			msg += "�nicamente el dispostivo que desee inicializar";
			CWnd::MessageBox(IDIOMA_Get(IDIOMA_MUCHOS_STICKS), "Clauer :: ERROR :: M�s de un Stick USB insertado", MB_ICONERROR);
			break;
		}


		this->numDispositivos=numDispositivos;

		unidades = new char * [numDispositivos];

		for (j; j<numDispositivos; j++ ) 
			unidades[j] = new char[5];
			
		dispositivos = new int [numDispositivos];

		if ( lowlevel_enumerar_dispositivos( unidades, dispositivos, &numDispositivos) != 0 ) {
			CWnd::MessageBox(IDIOMA_Get(IDIOMA_NO_INICIALIZAR), "Clauer :: ERROR :: Buscando sticks", MB_ICONERROR);
			break;
		}

		int soy;
		result = lowlevel_soy_admin( dispositivos[0], &soy );
		if ( result != ERR_LOWLEVEL_NO ) {
			CWnd::MessageBox(IDIOMA_Get(IDIOMA_COMPROBANDO_ADMINISTRADOR), "Clauer :: ERROR :: Buscando sticks", MB_ICONERROR);
			break;
		}

		if (!soy)
		{
			CWnd::MessageBox(IDIOMA_Get(IDIOMA_NO_ADMINISTRADOR), "Clauer :: ERROR :: Buscando sticks", MB_ICONERROR);	
			break;
		}

		if (lowlevel_abrir_dispositivo( dispositivos[0], &handle ) !=0 ) {
			CWnd::MessageBox(IDIOMA_Get(IDIOMA_NO_INICIALIZAR), "Clauer :: ERROR :: Buscando sticks", MB_ICONERROR);
			break;
		}

		if (lowlevel_es_usbcert(handle, &es ) != 0 ) {
			CWnd::MessageBox(IDIOMA_Get(IDIOMA_NO_INICIALIZAR), "Clauer :: ERROR :: Buscando sticks", MB_ICONERROR);
			break;
		}
		
		if (lowlevel_cerrar_dispositivo(handle) != 0) {
			CWnd::MessageBox(IDIOMA_Get(IDIOMA_IMPOSIBLE_CERRAR_DISP), "Clauer :: ERROR :: Buscando sticks", MB_ICONERROR);
			break;
		}
es=0;
		if (!es){
			SetPaso(m_paso+1);
		}else{
			SetPaso(20);
		}
		break;

	case 2:

		// Comprobamos que la password es correcta

		pin = m_dlgPaso2.GetPIN();
		confirmacion = m_dlgPaso2.GetConfirmacion();

		if ( pin.GetLength(
		) < 8 ) {
			AfxMessageBox(IDIOMA_Get(IDIOMA_LONGITUD_PIN), MB_OK,0);
			m_dlgPaso3.SetFocus();
			m_dlgPaso2.m_editPIN.Empty();
			m_dlgPaso2.m_editConfirmation.Empty();
			m_dlgPaso2.UpdateData(FALSE);
		} else if ( pin != confirmacion ) {
			AfxMessageBox(IDIOMA_Get(IDIOMA_PIN_COINCIDENCIA), MB_OK,0);
			m_dlgPaso3.SetFocus();
			m_dlgPaso2.m_editPIN.Empty();
			m_dlgPaso2.m_editConfirmation.Empty();
			m_dlgPaso2.UpdateData(FALSE);
		} else {
			if( !SeguridadPasswd(pin) ) {
				m_dlgPaso3.SetFocus();
				m_dlgPaso2.m_editPIN.Empty();
				m_dlgPaso2.m_editConfirmation.Empty();
				m_dlgPaso2.UpdateData(FALSE);
			}else{
				this->SetFocus();
				m_dlgPaso2.m_editPIN.Empty();
				m_dlgPaso2.m_editConfirmation.Empty();
				this->pin=pin;
				SetPaso(m_paso+1);
			}
		}
		m_dlgPaso2.m_editPIN.Empty();
		m_dlgPaso2.m_editConfirmation.Empty();

		break;
	case 3:

			m_botonSiguiente.SetWindowText(IDIOMA_Get(IDIOMA_BOTON_SIGUIENTE));
			m_botonSiguiente.EnableWindow(FALSE);
			m_botonCancelar.EnableWindow(FALSE);
			// Creamos hilo
			thrPaso3 = new thread;
			thrPaso3->SetDlg(&m_dlgPaso3);
			thrPaso3->SetPINClauer(this->pin);
			thrPaso3->CreateThread(CREATE_SUSPENDED);
			thrPaso3->ResumeThread();
			SetTimer(69,200,NULL);

		break;

	case 4:
		LIBRT_Ini();
		// Obtiene valores de variables
		m_dlgPaso4.UpdateData(TRUE);
		switch (m_dlgPaso4.m_radio)
		{
		//Ubicaci�n
		case 0:
		if(LIBRT_HayRuntime()){
			LIBRT_Fin ();
			SetPaso(m_paso+4);
		}else{
			AfxMessageBox(IDIOMA_Get(IDIOMA_NO_BASE));
		}
			break;
		//Internet Explorer
		case 1:
			AfxMessageBox(IDIOMA_Get(IDIOMA_NO_IMPLEMENTADO));
			break;
		//Fin
		case 2:
			SetPaso(m_paso+1);
			break;
		//Disquete
		case 3:
			if(LIBRT_HayRuntime()){
				LIBRT_Fin ();
				SetPaso(m_paso+3);
			}else{
				AfxMessageBox(IDIOMA_Get(IDIOMA_NO_BASE));
			}
			break;
		}
		break;
	case 5:
		EndDialog(IDOK);
		break;
	case 6:
		EndDialog(IDOK);
		break;
	case 7:
		m_botonCancelar.EnableWindow(FALSE);
		m_botonSiguiente.EnableWindow(FALSE);
		m_dlgPaso5_1.m_editPIN.Empty();
		CRYPTO_Ini();
		try {
			VerificarPKCS12(m_dlgPaso5_1.GetPIN());
		}
		catch (EPwdIncorrecta &e) {
			CWnd::MessageBox(e.GetMSG().c_str(),IDIOMA_Get(IDIOMA_TITULO_PIN_INCORRECTO), MB_ICONERROR);
			m_dlgPaso5_1.SetFocus();
			m_botonCancelar.EnableWindow(TRUE);
			m_botonSiguiente.EnableWindow(TRUE);
			break;
		}
		catch (ENoPKCS12 &e) {
			CWnd::MessageBox(e.GetMSG().c_str(), IDIOMA_Get(IDIOMA_TITULO_NO_CERTIFICADOS), MB_ICONERROR);
			m_botonCancelar.EnableWindow(TRUE);
			m_botonSiguiente.EnableWindow(TRUE);
			SetPaso(7);
			break;
		}
		catch (EExcepcion &e) {
			CWnd::MessageBox(e.GetMSG().c_str(), "Gestor Clauer :: ERROR", MB_ICONERROR);
			m_botonSiguiente.EnableWindow(TRUE);
			SetPaso(4);
			break;
		}
		if(ImpDisqPKCS12 (m_dlgPaso5_1.GetPIN(),this->numDispositivos,this->pin)) {

			m_botonSiguiente.SetWindowText(IDIOMA_Get(IDIOMA_BOTON_SIGUIENTE));
			SetPaso(6);
		}else {
			AfxMessageBox(IDIOMA_Get(IDIOMA_NO_IMPORTAR_CERTIFICADO));
			m_dlgPaso5_1.m_editPIN.Empty();
			SetPaso(4);
		}
		break;
	case 8: if (m_dlgPaso5_2.m_ruta.IsEmpty()) {
				AfxMessageBox(IDIOMA_Get(IDIOMA_SELECCIONAR_CERTIFICADO));
			}else{
				ruta=m_dlgPaso5_2.m_ruta;
				SetPaso(9);
			}
		break;
	case 9:
		m_botonCancelar.EnableWindow(FALSE);
		m_botonSiguiente.EnableWindow(FALSE);
		m_dlgPaso5_3.m_pin.Empty();
		CRYPTO_Ini();
		try {
			Verificar(m_dlgPaso5_3.GetPIN(),ruta);
		}
		catch (EPwdIncorrecta &e) {
			CWnd::MessageBox(e.GetMSG().c_str(), IDIOMA_Get(IDIOMA_TITULO_PIN_INCORRECTO), MB_ICONERROR);
			m_dlgPaso5_3.SetFocus();
			m_botonCancelar.EnableWindow(TRUE);
			m_botonSiguiente.EnableWindow(TRUE);
			break;
		}
		catch (ENoPKCS12 &e) {
			CWnd::MessageBox(e.GetMSG().c_str(), IDIOMA_Get(IDIOMA_TITULO_NO_CERTIFICADOS), MB_ICONERROR);
			m_botonCancelar.EnableWindow(TRUE);
			m_botonSiguiente.EnableWindow(TRUE);
			SetPaso(8);
			break;
		}
		catch (EExcepcion &e) {
			CWnd::MessageBox(e.GetMSG().c_str(), "Gestor Clauer :: ERROR", MB_ICONERROR);
			m_botonSiguiente.EnableWindow(TRUE);
			SetPaso(4);
			break;
		}
		if(Importar(m_dlgPaso5_3.GetPIN(),this->numDispositivos,this->pin,this->ruta)) {
			m_botonSiguiente.SetWindowText(IDIOMA_Get(IDIOMA_BOTON_SIGUIENTE));
			SetPaso(6);
		}else{
			AfxMessageBox(IDIOMA_Get(IDIOMA_NO_IMPORTAR_CERTIFICADO));
			m_dlgPaso5_3.m_pin.Empty();
			SetPaso(4);
		}
		break;
	case 20:
		// Obtiene valores de variables
		m_dlgPaso20.UpdateData(TRUE);
		switch (m_dlgPaso20.m_radio)
		{
		//Modificar PIN
		case 0:
			LIBRT_Ini();
			if(LIBRT_HayRuntime()){
				LIBRT_Fin ();
				SetPaso(21);
				break;
			}else{
				LIBRT_Fin ();
				AfxMessageBox(IDIOMA_Get(IDIOMA_NO_BASE));
				break;
			}
		//Gestionar certificados
		case 1:
			LIBRT_Ini();
			if(LIBRT_HayRuntime()){
				LIBRT_Fin ();
				SetPaso(22);
				break;
			}else{
				LIBRT_Fin ();
				AfxMessageBox(IDIOMA_Get(IDIOMA_NO_BASE));
				break;
			}
		//Eliminar zona criptografica
		case 2:
			LIBRT_Ini();
			if(LIBRT_HayRuntime()){
				LIBRT_Fin ();
				SetPaso(28);
				break;
			}else{
				LIBRT_Fin ();
				AfxMessageBox(IDIOMA_Get(IDIOMA_NO_BASE));
				break;
			}
		//Modificar particiones
		case 3:
			LIBRT_Ini();
			if(LIBRT_HayRuntime()){
				LIBRT_Fin ();
				AfxMessageBox(IDIOMA_Get(IDIOMA_NO_IMPLEMENTADO));
				//SetPaso();
				break;
			}else{
				LIBRT_Fin ();
				AfxMessageBox(IDIOMA_Get(IDIOMA_NO_BASE));
				break;
			}
		//Fin
		case 4:
			SetPaso(50);
			break;	
		}
		break;
	case 21:
		// Comprobamos que la password es correcta
		pin_nuevo = m_dlgPaso21.GetPIN();
		confirmacion = m_dlgPaso21.GetConfirmacion();

		if ( pin_nuevo.GetLength(
		) < 8 ) {
			AfxMessageBox(IDIOMA_Get(IDIOMA_LONGITUD_PIN), MB_OK,0);
			m_dlgPaso21.SetFocus();
		} else if ( pin_nuevo != confirmacion ) {
			AfxMessageBox(IDIOMA_Get(IDIOMA_PIN_COINCIDENCIA), MB_OK,0);
			m_dlgPaso21.SetFocus();
		} else {
			if( !SeguridadPasswd(pin_nuevo) ) {
				m_dlgPaso21.m_pinnuevo.Empty();
				m_dlgPaso21.m_pinconf.Empty();
				m_dlgPaso21.m_pinviejo.Empty();
				m_dlgPaso21.UpdateData(FALSE);
				SetPaso(20);
				break;
			}
			this->pin = m_dlgPaso21.GetViejo();

			if(this->pin.GetLength()==0){
				CWnd::MessageBox(IDIOMA_Get(IDIOMA_CONTRASENYA_MAL), IDIOMA_Get(IDIOMA_TITULO_CONTRASENYA_MAL), MB_ICONERROR);
				m_dlgPaso21.m_pinnuevo.Empty();
				m_dlgPaso21.m_pinconf.Empty();
				m_dlgPaso21.m_pinviejo.Empty();
				LIBRT_Fin ();
				SetPaso(20);
				break;
			}

			//Iniciar dispositivo
			LIBRT_Ini();

/*			unidades = new char * [this->numDispositivos];

			for ( int j=0; j<this->numDispositivos; j++ ) 
				unidades[j] = new char[5];
		
			dispositivos = new int [this->numDispositivos];

			if ( lowlevel_enumerar_dispositivos( unidades, dispositivos, &numDispositivos ) != ERR_LOWLEVEL_NO ) {
				CWnd::MessageBox(IDIOMA_Get(IDIOMA_NO_ENCONTRAR_STICK), "Clauer :: ERROR :: Buscando sticks", MB_ICONERROR);
				m_dlgPaso21.m_pinnuevo.Empty();
				m_dlgPaso21.m_pinconf.Empty();
				m_dlgPaso21.m_pinviejo.Empty();
				break;
			}*/

			unsigned char * dispositius[MAX_DEVICES];
			int numDisp;
			
			LIBRT_ListarDispositivos(&numDisp,dispositius);
			if ( LIBRT_IniciarDispositivo(dispositius[0],this->pin.GetBuffer(0),&handle_certs) != 0 ) {
			//if ( LIBRT_IniciarDispositivo(dispositivos[0],this->pin.GetBuffer(0),&handle_certs) != 0 ) {
				CWnd::MessageBox(IDIOMA_Get(IDIOMA_CONTRASENYA_MAL), IDIOMA_Get(IDIOMA_TITULO_CONTRASENYA_MAL), MB_ICONERROR);
				m_dlgPaso21.m_pinnuevo.Empty();
				m_dlgPaso21.m_pinconf.Empty();
				m_dlgPaso21.m_pinviejo.Empty();
				LIBRT_Fin ();
				SetPaso(20);
				break;
			}

			if ( LIBRT_CambiarPassword(&handle_certs,pin_nuevo.GetBuffer(0)) != 0 ) {
				CWnd::MessageBox(IDIOMA_Get(IDIOMA_NO_MODIFICAR_CONTRASENYA), IDIOMA_Get(IDIOMA_TITULO_CONTRASENYA_MAL), MB_ICONERROR);
				m_dlgPaso21.m_pinnuevo.Empty();
				m_dlgPaso21.m_pinconf.Empty();
				m_dlgPaso21.m_pinviejo.Empty();
				LIBRT_Fin ();
				SetPaso(20);
				break;
			}
			//Cerramos dispositivo
			if ( LIBRT_FinalizarDispositivo(&handle_certs) != 0 ) {
				CWnd::MessageBox(IDIOMA_Get(IDIOMA_NO_FINALIZADO), IDIOMA_Get(IDIOMA_TITULO_CONTRASENYA_MAL), MB_ICONERROR);
				m_dlgPaso21.m_pinnuevo.Empty();
				m_dlgPaso21.m_pinconf.Empty();
				m_dlgPaso21.m_pinviejo.Empty();
				LIBRT_Fin ();
				SetPaso(20);
				break;
			}
			this->SetFocus();
			m_dlgPaso21.m_pinnuevo.Empty();
			m_dlgPaso21.m_pinconf.Empty();
			m_dlgPaso21.m_pinviejo.Empty();
			CWnd::MessageBox(IDIOMA_Get(IDIOMA_CONTRASENYA_MODIFICADA), IDIOMA_Get(IDIOMA_TITULO_CONTRASENYA_MAL), MB_ICONINFORMATION);
			this->pin=pin_nuevo;
			SetPaso(20);
		}
		m_dlgPaso21.m_pinnuevo.Empty();
		m_dlgPaso21.m_pinconf.Empty();
		m_dlgPaso21.m_pinviejo.Empty();
		break;
	case 22:
// Obtiene valores de variables
		m_dlgPaso22.UpdateData(TRUE);
		switch (m_dlgPaso22.m_radio)
		{
		//Importar
		case 0:
			SetPaso(23);
			break;
		//Exportar
		case 1:
			AfxMessageBox(IDIOMA_Get(IDIOMA_NO_IMPLEMENTADO));
		//	SetPaso(22);
			break;
		//Eliminar
		case 2:
			AfxMessageBox(IDIOMA_Get(IDIOMA_NO_IMPLEMENTADO));
		//	SetPaso();
			break;
		//Modificar PIN disquete UJI
		case 3:
			DlgWin98 dlg_win98;
			int nResponse = dlg_win98.DoModal();
			if (nResponse == IDOK)
				{
					// TODO: Place code here to handle when the dialog is
					//  dismissed with OK
				}
				else if (nResponse == IDCANCEL)
				{
					// TODO: Place code here to handle when the dialog is
					//  dismissed with Cancel
				}
			break;
		}
		break;
	case 23:
		// Obtiene valores de variables
		m_dlgPaso23.UpdateData(TRUE);
		switch (m_dlgPaso23.m_radio)
		{
		//Disquete
		case 0:
			SetPaso(24);
			break;
		//Ubicaci�n
		case 1:
			SetPaso(25);
			break;
		//IE
		case 2:
			AfxMessageBox(IDIOMA_Get(IDIOMA_NO_IMPLEMENTADO));
		//	SetPaso();
			break;
		}
		break;
	case 24:
		m_botonCancelar.EnableWindow(FALSE);
		m_botonSiguiente.EnableWindow(FALSE);
		m_dlgPaso5_1.m_editPIN.Empty();
		if (this->pin.IsEmpty()){
			nResponse = m_dlgContras.DoModal();
			if (nResponse == IDOK)
			{
				// TODO: Place code here to handle when the dialog is
				//  dismissed with OK
				if(m_dlgContras.m_pin.IsEmpty()){
					CWnd::MessageBox(IDIOMA_Get(IDIOMA_CONTRASENYA_NO_CORRECTA), IDIOMA_Get(IDIOMA_TITULO_CONTRASENYA_MAL), MB_ICONERROR);
					SetPaso(23);
					break;
				}else{
					pin= m_dlgContras.m_pin;
				}
			}
			else if (nResponse == IDCANCEL)
			{
				// TODO: Place code here to handle when the dialog is
				//  dismissed with Cancel
				SetPaso(23);
				break;
			}
		}else{
			pin=this->pin;
		}
		//Iniciar dispositivo
		LIBRT_Ini();

/*		unidades = new char * [this->numDispositivos];

		for ( j=0; j<this->numDispositivos; j++ ) 
			unidades[j] = new char[5];
		
			dispositivos = new int [this->numDispositivos];

		if ( lowlevel_enumerar_dispositivos( unidades, dispositivos, &numDispositivos ) != ERR_LOWLEVEL_NO ) {
			CWnd::MessageBox(IDIOMA_Get(IDIOMA_NO_ENCONTRAR_STICK),IDIOMA_Get(IDIOMA_TITULO_ERROR_BUSCANDO), MB_ICONERROR);
			m_dlgContras.m_pin.Empty();
			LIBRT_Fin ();
			SetPaso(23);
			break;
		}
*/
		unsigned char * dispositius[MAX_DEVICES];
		int numDisp;
			
		LIBRT_ListarDispositivos(&numDisp,dispositius);

		if ( LIBRT_IniciarDispositivo(dispositius[0],this->pin.GetBuffer(0),&handle_certs) != 0 ) {
		//if ( LIBRT_IniciarDispositivo(dispositivos[0],pin.GetBuffer(0),&handle_certs) != 0 ) {
			CWnd::MessageBox(IDIOMA_Get(IDIOMA_CONTRASENYA_NO_CORRECTA), IDIOMA_Get(IDIOMA_TITULO_CONTRASENYA_MAL), MB_ICONERROR);
			m_dlgContras.m_pin.Empty();
			LIBRT_Fin ();
			SetPaso(23);
			break;
		}
		this->pin=pin;
		CRYPTO_Ini();
		try {
			VerificarPKCS12(m_dlgPaso5_1.GetPIN());
		}
		catch (EPwdIncorrecta &e) {
			CWnd::MessageBox(e.GetMSG().c_str(),IDIOMA_Get(IDIOMA_TITULO_PIN_INCORRECTO), MB_ICONERROR);
			m_dlgPaso5_1.SetFocus();
			m_botonCancelar.EnableWindow(TRUE);
			m_botonSiguiente.EnableWindow(TRUE);
			SetPaso(23);
			break;
		}
		catch (ENoPKCS12 &e) {
			CWnd::MessageBox(e.GetMSG().c_str(),IDIOMA_Get(IDIOMA_NO_ENCONTRADOS_CERTIFICADOS), MB_ICONERROR);
			m_botonCancelar.EnableWindow(TRUE);
			m_botonSiguiente.EnableWindow(TRUE);
			SetPaso(23);
			break;
		}
		catch (EExcepcion &e) {
			CWnd::MessageBox(e.GetMSG().c_str(), "Gestor Clauer :: ERROR", MB_ICONERROR);
			m_botonSiguiente.EnableWindow(TRUE);
			SetPaso(23);
			break;
		}
		if(ImpDisqPKCS12(m_dlgPaso5_1.GetPIN(),this->numDispositivos,this->pin)) {

			m_botonSiguiente.SetWindowText(IDIOMA_Get(IDIOMA_BOTON_SIGUIENTE));
			CWnd::MessageBox(IDIOMA_Get(IDIOMA_CERTIFICADO_OK),IDIOMA_Get(IDIOMA_TITULOS_CERTIFICADOS), MB_ICONINFORMATION);
			SetPaso(20);
		}else {
			AfxMessageBox(IDIOMA_Get(IDIOMA_NO_IMPORTAR_CERTIFICADO));
			m_dlgPaso5_1.m_editPIN.Empty();
			SetPaso(23);
		}
		break;
	case 25:
		
		if (m_dlgPaso5_2.m_ruta.IsEmpty()) {
			AfxMessageBox(IDIOMA_Get(IDIOMA_SELECCIONAR_CERTIFICADO));
		}else{

			if (this->pin.IsEmpty()){
			nResponse = m_dlgContras.DoModal();
				if (nResponse == IDOK)
				{
					// TODO: Place code here to handle when the dialog is
					//  dismissed with OK
					if (m_dlgContras.m_pin.IsEmpty()){
						CWnd::MessageBox(IDIOMA_Get(IDIOMA_CONTRASENYA_NO_CORRECTA),IDIOMA_Get(IDIOMA_TITULO_CONTRASENYA_MAL), MB_ICONERROR);
						SetPaso(23);
						break;
					}else{
						pin= m_dlgContras.m_pin;
					}
				}
				else if (nResponse == IDCANCEL)
				{
					// TODO: Place code here to handle when the dialog is
				//  dismissed with Cancel
					SetPaso(23);
					break;
				}
			}else{
			pin=this->pin;
			}

			//Iniciar dispositivo
			LIBRT_Ini();

/*			unidades = new char * [this->numDispositivos];

			for (j=0; j<this->numDispositivos; j++ ) 
			unidades[j] = new char[5];
		
			dispositivos = new int [this->numDispositivos];

			if ( lowlevel_enumerar_dispositivos( unidades, dispositivos, &numDispositivos ) != ERR_LOWLEVEL_NO ) {
				CWnd::MessageBox(IDIOMA_Get(IDIOMA_NO_ENCONTRAR_STICK),IDIOMA_Get(IDIOMA_TITULO_ERROR_BUSCANDO), MB_ICONERROR);
				m_dlgContras.m_pin.Empty();
				LIBRT_Fin();
				SetPaso(23);
				break;
			}
*/
			unsigned char * dispositius[MAX_DEVICES];
			int numDisp;
			
			LIBRT_ListarDispositivos(&numDisp,dispositius);

			if ( LIBRT_IniciarDispositivo(dispositius[0],this->pin.GetBuffer(0),&handle_certs) != 0 ) {
			//if ( LIBRT_IniciarDispositivo(dispositivos[0],this->pin.GetBuffer(0),&handle_certs) != 0 ) {
				CWnd::MessageBox(IDIOMA_Get(IDIOMA_CONTRASENYA_NO_CORRECTA), IDIOMA_Get(IDIOMA_TITULO_CONTRASENYA_MAL), MB_ICONERROR);
				m_dlgContras.m_pin.Empty();
				LIBRT_Fin();
				SetPaso(23);
				break;
			}
			this->pin=pin;
			ruta=m_dlgPaso5_2.m_ruta;
			SetPaso(26);
			break;
		}
		break;
	case 26:
		m_botonCancelar.EnableWindow(FALSE);
		m_botonSiguiente.EnableWindow(FALSE);
		m_dlgPaso5_3.m_pin.Empty();
		CRYPTO_Ini();
		try {
			Verificar(m_dlgPaso5_3.GetPIN(),ruta);
		}
		catch (EPwdIncorrecta &e) {
			CWnd::MessageBox(e.GetMSG().c_str(),IDIOMA_Get(IDIOMA_TITULO_PIN_INCORRECTO), MB_ICONERROR);
			m_dlgPaso5_3.SetFocus();
			m_botonCancelar.EnableWindow(TRUE);
			m_botonSiguiente.EnableWindow(TRUE);
			break;
		}
		catch (ENoPKCS12 &e) {
			CWnd::MessageBox(e.GetMSG().c_str(),IDIOMA_Get(IDIOMA_TITULO_NO_CERTIFICADOS), MB_ICONERROR);
			m_botonCancelar.EnableWindow(TRUE);
			m_botonSiguiente.EnableWindow(TRUE);
			SetPaso(23);
			break;
		}
		catch (EExcepcion &e) {
			CWnd::MessageBox(e.GetMSG().c_str(), "Gestor Clauer :: ERROR", MB_ICONERROR);
			m_botonSiguiente.EnableWindow(TRUE);
			SetPaso(20);
			break;
		}
		if(Importar(m_dlgPaso5_3.GetPIN(),this->numDispositivos,this->pin,this->ruta)) {
			m_botonSiguiente.SetWindowText(IDIOMA_Get(IDIOMA_BOTON_SIGUIENTE));
			CWnd::MessageBox(IDIOMA_Get(IDIOMA_CERTIFICADO_OK),IDIOMA_Get(IDIOMA_TITULOS_CERTIFICADOS), MB_ICONINFORMATION);
			SetPaso(20);
		}else{
			AfxMessageBox(IDIOMA_Get(IDIOMA_NO_IMPORTAR_CERTIFICADO));
			m_dlgPaso5_3.m_pin.Empty();
			SetPaso(20);
		}
		break;
	case 28:
		m_botonSiguiente.EnableWindow(FALSE);
		m_botonCancelar.EnableWindow(FALSE);
		nResponse = m_dlgSeguro.DoModal();
		if (nResponse == IDOK)
		{
			// TODO: Place code here to handle when the dialog is
			//  dismissed with OK
/*			if (this->pin.IsEmpty()){
			nResponse = m_dlgContras.DoModal();
				if (nResponse == IDOK)
				{
					// TODO: Place code here to handle when the dialog is
					//  dismissed with OK
					if(m_dlgContras.m_pin.IsEmpty()){
						CWnd::MessageBox(IDIOMA_Get(IDIOMA_CONTRASENYA_MAL),IDIOMA_Get(IDIOMA_TITULO_CONTRASENYA_MAL), MB_ICONERROR);
						SetPaso(20);
						break;
					}else{
						pin= m_dlgContras.m_pin;
					}
				}
				else if (nResponse == IDCANCEL)
				{
					// TODO: Place code here to handle when the dialog is
				//  dismissed with Cancel
					SetPaso(20);
					break;
				}
			}else{
			pin=this->pin;
			}*/
			SetPaso(29);
			break;
		}
		else if (nResponse == IDCANCEL)
		{
			// TODO: Place code here to handle when the dialog is
		    //  dismissed with Cancel
			SetPaso(20);
			break;
		}
		break;
	case 29:

		m_botonSiguiente.EnableWindow(FALSE);
		m_botonCancelar.EnableWindow(FALSE);
		pin = m_dlgPaso2.GetPIN();
		confirmacion = m_dlgPaso2.GetConfirmacion();

		if ( pin.GetLength(
		) < 8 ) {
			AfxMessageBox(IDIOMA_Get(IDIOMA_LONGITUD_PIN), MB_OK,0);
			m_dlgPaso3.SetFocus();
			m_dlgPaso2.m_editPIN.Empty();
			m_dlgPaso2.m_editConfirmation.Empty();
			m_dlgPaso2.UpdateData(FALSE);
			m_botonSiguiente.EnableWindow(TRUE);
			m_botonCancelar.EnableWindow(TRUE);
		} else if ( pin != confirmacion ) {
			AfxMessageBox(IDIOMA_Get(IDIOMA_PIN_COINCIDENCIA), MB_OK,0);
			m_dlgPaso3.SetFocus();
			m_dlgPaso2.m_editPIN.Empty();
			m_dlgPaso2.m_editConfirmation.Empty();
			m_dlgPaso2.UpdateData(FALSE);
			m_botonSiguiente.EnableWindow(TRUE);
			m_botonCancelar.EnableWindow(TRUE);
		} else {
			if( !SeguridadPasswd(pin) ) {
				m_dlgPaso3.SetFocus();
				m_dlgPaso2.m_editPIN.Empty();
				m_dlgPaso2.m_editConfirmation.Empty();
				m_dlgPaso2.UpdateData(FALSE);
				m_botonSiguiente.EnableWindow(TRUE);
				m_botonCancelar.EnableWindow(TRUE);
			}else{
				this->SetFocus();
				m_dlgPaso2.m_editPIN.Empty();
				m_dlgPaso2.m_editConfirmation.Empty();
				this->pin=pin;
				unidades = new char * [this->numDispositivos];

				for ( j=0; j<this->numDispositivos; j++ ) 
				unidades[j] = new char[5];
		
				dispositivos = new int [this->numDispositivos];

				if ( lowlevel_enumerar_dispositivos( unidades, dispositivos, &numDispositivos ) != ERR_LOWLEVEL_NO ) {
					CWnd::MessageBox(IDIOMA_Get(IDIOMA_NO_ENCONTRAR_STICK), IDIOMA_Get(IDIOMA_TITULO_ERROR_BUSCANDO), MB_ICONERROR);
					m_dlgContras.m_pin.Empty();
					LIBRT_Fin();
					SetPaso(20);
					break;
				}


				this->pin=pin;

				PARTICION particiones[4];

				result = lowlevel_abrir_dispositivo( dispositivos[0], &handle );

				if ( lowlevel_leer_particiones(handle, particiones) != ERR_FORMAT_NO ) {
					CWnd::MessageBox(IDIOMA_Get(IDIOMA_NO_ENCONTRAR_STICK), IDIOMA_Get(IDIOMA_TITULO_ERROR_BUSCANDO), MB_ICONERROR);
					SetPaso(20);
					break;
				}

				result = format_crear_zona_cripto( handle, particiones[3], this->pin.GetBuffer(0), NULL );
				if ( result != ERR_FORMAT_NO ) {
					CWnd::MessageBox(IDIOMA_Get(IDIOMA_ERROR_FORMATEANDO), IDIOMA_Get(IDIOMA_TITULO_CRIPTO), MB_ICONERROR);
					SetPaso(20);
					break;
				}

				CWnd::MessageBox(IDIOMA_Get(IDIOMA_ELIMINADO_CRIPTO), IDIOMA_Get(IDIOMA_TITULO_CRIPTO), MB_ICONINFORMATION);
				SetPaso(20);
				break;
			}
		}
		break;
	case 50:
		EndDialog(IDOK);
		break;
	}
}

void CAdmin_CLAUERDlg::SetPaso(int paso)
{

	CWnd* pWnd = GetDlgItem( IDC_FRAME );
    CRect rect;
    pWnd->GetWindowRect( &rect );
    ScreenToClient( &rect );

	switch ( m_paso ) {

	case 1:
		m_dlgPaso1.ShowWindow(SW_HIDE);
		break;
	case 2:
		m_dlgPaso2.ShowWindow(SW_HIDE);
		m_dlgPaso2.DestroyWindow();
		break;
	case 3:
		m_dlgPaso3.ShowWindow(SW_HIDE);
		break;
	case 4:
		m_dlgPaso4.ShowWindow(SW_HIDE);
		break;
	case 5:
		m_dlgPaso5_6.ShowWindow(SW_HIDE);
		break;
	case 6:
		m_dlgPaso5_6.ShowWindow(SW_HIDE);
		break;
	case 7:
		m_dlgPaso5_1.ShowWindow(SW_HIDE);
		m_dlgPaso5_1.DestroyWindow();
		break;
	case 8:
		m_dlgPaso5_2.ShowWindow(SW_HIDE);
		m_dlgPaso5_2.DestroyWindow();
		break;
	case 9:
		m_dlgPaso5_3.ShowWindow(SW_HIDE);
		m_dlgPaso5_3.DestroyWindow();
		break;
	case 20:
		m_dlgPaso20.ShowWindow(SW_HIDE);
		m_dlgPaso20.DestroyWindow();
		break;
	case 21:
		m_dlgPaso21.ShowWindow(SW_HIDE);
		m_dlgPaso21.DestroyWindow();
		break;
	case 22:
		m_dlgPaso22.ShowWindow(SW_HIDE);
		m_dlgPaso22.DestroyWindow();
		break;
	case 23:
		m_dlgPaso23.ShowWindow(SW_HIDE);
		m_dlgPaso23.DestroyWindow();
		break;
	case 24:
		m_dlgPaso5_1.ShowWindow(SW_HIDE);
		m_dlgPaso5_1.DestroyWindow();
		break;
	case 25:
		m_dlgPaso5_2.ShowWindow(SW_HIDE);
		m_dlgPaso5_2.DestroyWindow();
		break;
	case 26:
		m_dlgPaso5_3.ShowWindow(SW_HIDE);
		m_dlgPaso5_3.DestroyWindow();
		break;
	case 28:
		m_dlgPaso28.ShowWindow(SW_HIDE);
		m_dlgPaso28.DestroyWindow();
		break;
	case 29:
		m_dlgPaso2.ShowWindow(SW_HIDE);
		m_dlgPaso2.DestroyWindow();
		break;
	case 50:
		m_dlgPaso50.ShowWindow(SW_HIDE);
		m_dlgPaso50.DestroyWindow();
		break;
	}

	switch ( paso ) {
	case 1:
		//	m_pnlIzquierdo.SetBitmap(::LoadBitmap(AfxGetResourceHandle(),MAKEINTRESOURCE(IDB_BITMAP1)));
		m_botonSiguiente.SetWindowText(IDIOMA_Get(IDIOMA_BOTON_SIGUIENTE));
		m_botonCancelar.SetWindowText(IDIOMA_Get(IDIOMA_BOTON_CANCEL));
		m_botonSiguiente.EnableWindow(TRUE);
		m_botonCancelar.EnableWindow(TRUE);
		m_dlgPaso1.ShowWindow(SW_SHOW);
		break;
	case 2:
	//	m_pnlIzquierdo.SetBitmap(::LoadBitmap(AfxGetResourceHandle(),MAKEINTRESOURCE(IDB_BITMAP2)));
		m_dlgPaso2.Create(IDD_DIALOG_PASO_2, this);
		m_dlgPaso2.ShowWindow(WS_VISIBLE|WS_CHILD);
		m_dlgPaso2.SetWindowPos(pWnd, rect.left, rect.top, rect.right, rect.bottom, SWP_HIDEWINDOW);
		m_dlgPaso2.ShowWindow(SW_SHOW);
		m_dlgPaso2.SetFocus();
		break;

	case 3:
	//	m_pnlIzquierdo.SetBitmap(::LoadBitmap(AfxGetResourceHandle(),MAKEINTRESOURCE(IDB_BITMAP3)));
		m_botonSiguiente.SetWindowText(IDIOMA_Get(IDIOMA_BOTON_EMPEZAR));
		m_dlgPaso3.ShowWindow(SW_SHOW);
		m_dlgPaso3.SetFocus();
		break;
		
	case 4:
	//	m_pnlIzquierdo.SetBitmap(::LoadBitmap(AfxGetResourceHandle(),MAKEINTRESOURCE(IDB_BITMAP4)));
		m_dlgPaso4.ShowWindow(SW_SHOW);
		m_dlgPaso4.SetFocus();
		m_botonSiguiente.SetWindowText(IDIOMA_Get(IDIOMA_BOTON_SIGUIENTE));
		m_botonSiguiente.EnableWindow(TRUE);
		m_botonCancelar.EnableWindow(FALSE);
		break;

	case 5:
		m_botonSiguiente.EnableWindow(TRUE);
		m_botonCancelar.EnableWindow(FALSE);
		this->SetFocus();
		m_botonSiguiente.SetWindowText(IDIOMA_Get(IDIOMA_BOTON_FINALIZAR));
		m_dlgPaso5_6.ShowWindow(SW_SHOW);
		break;

	case 6:
		m_botonSiguiente.EnableWindow(TRUE);
		m_botonCancelar.EnableWindow(FALSE);
		this->SetFocus();
		m_botonSiguiente.SetWindowText(IDIOMA_Get(IDIOMA_BOTON_FINALIZAR));
		m_dlgPaso5_6.ShowWindow(SW_SHOW);
		break;

	case 7:
		m_dlgPaso5_1.Create(IDD_DIALOG_PASO_5_1, this);
		m_dlgPaso5_1.ShowWindow(WS_VISIBLE|WS_CHILD);
		m_dlgPaso5_1.SetWindowPos(pWnd, rect.left, rect.top, rect.right, rect.bottom, SWP_HIDEWINDOW);
		m_dlgPaso5_1.ShowWindow(SW_SHOW);
		m_botonSiguiente.SetWindowText(IDIOMA_Get(IDIOMA_BOTON_IMPORTAR));
		m_botonCancelar.EnableWindow(TRUE);
		m_botonSiguiente.EnableWindow(TRUE);
		m_dlgPaso5_1.SetFocus();
		break;
	case 8:
		m_dlgPaso5_2.Create(IDD_DIALOG_PASO_5_2, this);
		m_dlgPaso5_2.ShowWindow(WS_VISIBLE|WS_CHILD);
		m_dlgPaso5_2.SetWindowPos(pWnd, rect.left, rect.top, rect.right, rect.bottom, SWP_HIDEWINDOW);
		m_dlgPaso5_2.ShowWindow(SW_SHOW);
		m_botonSiguiente.SetWindowText(IDIOMA_Get(IDIOMA_BOTON_SIGUIENTE));
		m_botonCancelar.EnableWindow(TRUE);
		m_botonSiguiente.EnableWindow(TRUE);
		m_dlgPaso5_2.SetFocus();
		break;
	case 9:
		m_dlgPaso5_3.Create(IDD_DIALOG_PASO_5_3, this);
		m_dlgPaso5_3.ShowWindow(WS_VISIBLE|WS_CHILD);
		m_dlgPaso5_3.SetWindowPos(pWnd, rect.left, rect.top, rect.right, rect.bottom, SWP_HIDEWINDOW);
		m_dlgPaso5_3.ShowWindow(SW_SHOW);
		m_botonSiguiente.SetWindowText(IDIOMA_Get(IDIOMA_BOTON_IMPORTAR));
		m_botonCancelar.EnableWindow(TRUE);
		m_botonSiguiente.EnableWindow(TRUE);
		m_dlgPaso5_3.SetFocus();
		break;
	case 20:
		m_dlgPaso20.Create(IDD_DIALOG_PASO_20, this);
		m_dlgPaso20.ShowWindow(WS_VISIBLE|WS_CHILD);
		m_dlgPaso20.SetWindowPos(pWnd, rect.left, rect.top, rect.right, rect.bottom, SWP_HIDEWINDOW);
		m_dlgPaso20.ShowWindow(SW_SHOW);
		m_botonSiguiente.SetWindowText(IDIOMA_Get(IDIOMA_BOTON_SIGUIENTE));
		m_botonCancelar.SetWindowText(IDIOMA_Get(IDIOMA_BOTON_CANCEL));
		m_botonCancelar.EnableWindow(FALSE);
		m_botonSiguiente.EnableWindow(TRUE);
		m_dlgPaso20.SetFocus();
		break;
	case 21:
		m_dlgPaso21.Create(IDD_DIALOG_PASO_21, this);
		m_dlgPaso21.ShowWindow(WS_VISIBLE|WS_CHILD);
		m_dlgPaso21.SetWindowPos(pWnd, rect.left, rect.top, rect.right, rect.bottom, SWP_HIDEWINDOW);
		m_dlgPaso21.ShowWindow(SW_SHOW);
		//m_botonSiguiente.SetWindowText("Siguiente");
		m_botonCancelar.EnableWindow(TRUE);
		//m_botonSiguiente.EnableWindow(TRUE);
		m_dlgPaso21.SetFocus();
		break;
	case 22:
		m_dlgPaso22.Create(IDD_DIALOG_PASO_22, this);
		m_dlgPaso22.ShowWindow(WS_VISIBLE|WS_CHILD);
		m_dlgPaso22.SetWindowPos(pWnd, rect.left, rect.top, rect.right, rect.bottom, SWP_HIDEWINDOW);
		m_dlgPaso22.ShowWindow(SW_SHOW);
		//m_botonSiguiente.SetWindowText("Siguiente");
		m_botonCancelar.EnableWindow(TRUE);
		m_botonCancelar.SetWindowText(IDIOMA_Get(IDIOMA_BOTON_CANCEL));
		//m_botonSiguiente.EnableWindow(TRUE);
		m_dlgPaso22.SetFocus();
		break;
	case 23:
		m_dlgPaso23.Create(IDD_DIALOG_PASO_23, this);
		m_dlgPaso23.ShowWindow(WS_VISIBLE|WS_CHILD);
		m_dlgPaso23.SetWindowPos(pWnd, rect.left, rect.top, rect.right, rect.bottom, SWP_HIDEWINDOW);
		m_dlgPaso23.ShowWindow(SW_SHOW);
		m_botonSiguiente.SetWindowText(IDIOMA_Get(IDIOMA_BOTON_SIGUIENTE));
		m_botonCancelar.SetWindowText(IDIOMA_Get(IDIOMA_BOTON_ANTERIOR));
		m_botonCancelar.EnableWindow(TRUE);
		m_botonSiguiente.EnableWindow(TRUE);
		m_dlgPaso23.SetFocus();
		break;
	case 24:
		m_dlgPaso5_1.Create(IDD_DIALOG_PASO_5_1, this);
		m_dlgPaso5_1.ShowWindow(WS_VISIBLE|WS_CHILD);
		m_dlgPaso5_1.SetWindowPos(pWnd, rect.left, rect.top, rect.right, rect.bottom, SWP_HIDEWINDOW);
		m_dlgPaso5_1.ShowWindow(SW_SHOW);
		m_botonSiguiente.SetWindowText(IDIOMA_Get(IDIOMA_BOTON_IMPORTAR));
		m_botonCancelar.EnableWindow(TRUE);
		m_botonSiguiente.EnableWindow(TRUE);
		m_dlgPaso5_1.SetFocus();
		break;
	case 25:
		m_dlgPaso5_2.Create(IDD_DIALOG_PASO_5_2, this);
		m_dlgPaso5_2.ShowWindow(WS_VISIBLE|WS_CHILD);
		m_dlgPaso5_2.SetWindowPos(pWnd, rect.left, rect.top, rect.right, rect.bottom, SWP_HIDEWINDOW);
		m_dlgPaso5_2.ShowWindow(SW_SHOW);
		m_botonSiguiente.SetWindowText(IDIOMA_Get(IDIOMA_BOTON_SIGUIENTE));
		m_botonCancelar.EnableWindow(TRUE);
		m_botonSiguiente.EnableWindow(TRUE);
		m_dlgPaso5_2.SetFocus();
		break;
	case 26:
		m_dlgPaso5_3.Create(IDD_DIALOG_PASO_5_3, this);
		m_dlgPaso5_3.ShowWindow(WS_VISIBLE|WS_CHILD);
		m_dlgPaso5_3.SetWindowPos(pWnd, rect.left, rect.top, rect.right, rect.bottom, SWP_HIDEWINDOW);
		m_dlgPaso5_3.ShowWindow(SW_SHOW);
		m_botonSiguiente.SetWindowText(IDIOMA_Get(IDIOMA_BOTON_IMPORTAR));
		m_botonCancelar.EnableWindow(TRUE);
		m_botonCancelar.SetWindowText(IDIOMA_Get(IDIOMA_BOTON_CANCEL));
		m_botonSiguiente.EnableWindow(TRUE);
		m_dlgPaso5_3.SetFocus();
		break;
	case 28:
		m_dlgPaso28.Create(IDD_DIALOG_PASO_28, this);
		m_dlgPaso28.ShowWindow(WS_VISIBLE|WS_CHILD);
		m_dlgPaso28.SetWindowPos(pWnd, rect.left, rect.top, rect.right, rect.bottom, SWP_HIDEWINDOW);
		m_dlgPaso28.ShowWindow(SW_SHOW);
		m_botonSiguiente.SetWindowText(IDIOMA_Get(IDIOMA_BOTON_ELIMINAR));
		m_botonCancelar.SetWindowText(IDIOMA_Get(IDIOMA_BOTON_CANCEL));
		m_botonCancelar.EnableWindow(TRUE);
		m_botonSiguiente.EnableWindow(TRUE);
		m_dlgPaso28.SetFocus();
		break;
	case 29:
	//	m_pnlIzquierdo.SetBitmap(::LoadBitmap(AfxGetResourceHandle(),MAKEINTRESOURCE(IDB_BITMAP2)));
		m_dlgPaso2.Create(IDD_DIALOG_PASO_2, this);
		m_dlgPaso2.ShowWindow(WS_VISIBLE|WS_CHILD);
		m_dlgPaso2.SetWindowPos(pWnd, rect.left, rect.top, rect.right, rect.bottom, SWP_HIDEWINDOW);
		m_dlgPaso2.ShowWindow(SW_SHOW);
		m_dlgPaso2.SetFocus();
		m_botonSiguiente.EnableWindow(TRUE);
		m_botonCancelar.EnableWindow(TRUE);
		m_dlgPaso2.m_texto.SetWindowText(IDIOMA_Get(IDIOMA_PASO2_NEGR_FOR));
		break;
	case 50:
		m_dlgPaso50.Create(IDD_DIALOG_PASO_50, this);
		m_dlgPaso50.ShowWindow(WS_VISIBLE|WS_CHILD);
		m_dlgPaso50.SetWindowPos(pWnd, rect.left, rect.top, rect.right, rect.bottom, SWP_HIDEWINDOW);
		m_dlgPaso50.ShowWindow(SW_SHOW);
		m_botonSiguiente.SetWindowText(IDIOMA_Get(IDIOMA_BOTON_FINALIZAR));
		m_botonCancelar.EnableWindow(FALSE);
		m_botonSiguiente.EnableWindow(TRUE);
		m_dlgPaso50.SetFocus();
		break;

	}

	m_paso = paso;

//	char num[2];
//	itoa(m_paso, num, 10);
//	this->SetWindowText("Gestor del Clauer" + CString(" :: Paso ") + CString(num));

}

void CAdmin_CLAUERDlg::OnTimer(UINT nIDEvent) 
{
	// TODO: Add your message handler code here and/or call default

	if ( nIDEvent == 69 ) {

		if ( thrPaso3->Terminado() ) {
			KillTimer(69);

			if ( !thrPaso3->GetError() ) {
				CWnd::MessageBox(IDIOMA_Get(IDIOMA_PROCESO_FINALIZADO), IDIOMA_Get(IDIOMA_TITULO_FINALIZADO),MB_ICONINFORMATION);
				m_botonSiguiente.EnableWindow(TRUE);
				SetPaso(m_paso+1);
			} else {
				CWnd::MessageBox(thrPaso3->GetMsgError().GetBuffer(0), "Clauer :: ERROR", MB_ICONERROR);
				SetPaso(1);
			}
		}
	}
	
	CDialog::OnTimer(nIDEvent);
}

BOOL CAdmin_CLAUERDlg::PreTranslateMessage(MSG* pMsg) 
{
	// TODO: Add your specialized code here and/or call the base class
	if(pMsg->message==WM_KEYDOWN)
	{
		if ( (pMsg->wParam==VK_ESCAPE) )
			pMsg->wParam=NULL ;
		else if ( (pMsg->wParam==VK_RETURN) ) {
			OnBotonSiguiente();
			return TRUE;
		}
	}
	return CDialog::PreTranslateMessage(pMsg);
}

void CAdmin_CLAUERDlg::VerificarPKCS12(CString pwd)
{
	HANDLE hFind;
	WIN32_FIND_DATA findData;
	FILE *fp;
	BYTE *pkcs12;
	DWORD tamPKCS12, err;
	int ok, pkcs12Encontrados=0;
	BOOL masFicheros;
	char *filtro[2] = {"A:\\*.p12", "A:\\*.pfx"};

	/* Iteramos para ambas m�scaras
	 */
	for ( int f = 0 ; f < 2 ; f++ ) {

		hFind = FindFirstFile(filtro[f], &findData);

		if ( hFind == INVALID_HANDLE_VALUE ) 
			continue;
		
		do {
		
			fp = fopen((string("A:\\") + string(findData.cFileName)).c_str(), "rb");
			if ( !fp ) {
				string errMsg;
				errMsg = IDIOMA_Get(IDIOMA_NO_CERT_DISQUET);
				ENoPKCS12 e(errMsg);
				m_dlgPaso5_1.m_editPIN.Empty();
				m_dlgPaso5_1.UpdateData(FALSE);
				
				throw e;
			}

			fseek(fp, 0, SEEK_END);
			tamPKCS12 = ftell(fp);
			fseek(fp, 0, SEEK_SET);

			pkcs12 = new BYTE [tamPKCS12];
			fread(pkcs12, tamPKCS12, 1, fp);
			fclose(fp);

			ok = CRYPTO_PKCS12_VerificarPassword (pkcs12, tamPKCS12, pwd.GetBuffer(0));

			SecureZeroMemory(pkcs12, tamPKCS12);
			delete [] pkcs12;
			pkcs12 = NULL;

			if ( ok == 0 ) {
				/* Password incorrecta */
	
					fclose(fp);
					FindClose(hFind);
					string errMsg;
					errMsg = IDIOMA_Get(IDIOMA_PIN_INCORRECTO);
					m_dlgPaso5_1.m_editPIN.Empty();
					m_dlgPaso5_1.UpdateData(FALSE);
					EPwdIncorrecta e(errMsg);
					throw e;
			} else if ( ok == 2 ) {
				/* Error */

				fclose(fp);
				FindClose(hFind);

				string errMsg;
				errMsg =IDIOMA_Get(IDIOMA_ERROR_VERIFICANDO_PASS);
				m_dlgPaso5_1.m_editPIN.Empty();
				m_dlgPaso5_1.UpdateData(FALSE);
				EExcepcion e(errMsg);
				throw e;
			}

			++pkcs12Encontrados;

			masFicheros = FindNextFile(hFind, &findData);

			if ( !masFicheros ) {
				err = GetLastError();
				if ( err == ERROR_NO_MORE_FILES )
					break;
				else {
					/* Cualquier otro error asumimos error de entrada salida
					 */

					fclose(fp);
					FindClose(hFind);
		
					string errMsg;
					errMsg =IDIOMA_Get(IDIOMA_ERROR_ENTRADA_SALIDA);
					m_dlgPaso5_1.m_editPIN.Empty();
					m_dlgPaso5_1.UpdateData(FALSE);
					EErrorES e(errMsg);
					throw e;
				}
			}
		
		} while (1);

		fclose(fp);
		FindClose(hFind);

	}

	if ( pkcs12Encontrados == 0 ) {
		string errMsg;
		errMsg = IDIOMA_Get(IDIOMA_NO_CERT_DISQUET);
		m_dlgPaso5_1.m_editPIN.Empty();
		m_dlgPaso5_1.UpdateData(FALSE);
		ENoPKCS12 e(errMsg);
		throw e;
	}

}

int CAdmin_CLAUERDlg::ImpDisqPKCS12(CString pwd,int numDispositivos,CString pin)
{
	/* Importar los certificados del disquete
	 */
//	int result;
	HANDLE hFind;
	WIN32_FIND_DATA findData;
	int pkcs12Encontrados=0;
	BOOL masFicheros;
	char *filtro[2] = {"A:\\*.p12", "A:\\*.pfx"};
	USBCERTS_HANDLE hClauer;

	CString m_strErrMsg;

	int *dispositivos = NULL;
	char ** unidades = NULL;
	/* Iteramos para ambas m�scaras
	 */

	LIBRT_Ini();

/*
	unidades = new char * [numDispositivos];

	for ( int j=0; j<numDispositivos; j++ ) 
		unidades[j] = new char[5];
		
	dispositivos = new int [numDispositivos];

	result = lowlevel_enumerar_dispositivos( unidades, dispositivos, &numDispositivos );

	if ( result != ERR_LOWLEVEL_NO ) {
		m_strErrMsg = IDIOMA_Get(IDIOMA_NO_INICIALIZAR);
		string errMsg;
		ENoPKCS12 e(errMsg);
	//	throw e;
		return 0;
	}
	*/

	unsigned char * dispositius[MAX_DEVICES];
	int numDisp;
			
	LIBRT_ListarDispositivos(&numDisp,dispositius);

	if ( LIBRT_IniciarDispositivo(dispositius[0],this->pin.GetBuffer(0),&hClauer) != 0 ) {
	//if ( LIBRT_IniciarDispositivo ( dispositivos[0], pin.GetBuffer(0), &hClauer ) != 0 ) {
		m_strErrMsg = IDIOMA_Get(IDIOMA_NO_INICIALIZAR);
		string errMsg;
		ENoPKCS12 e(errMsg);
	//	throw e;
		return 0;

	}

	for ( int f = 0 ; f < 2 ; f++ ) {

		hFind = FindFirstFile(filtro[f], &findData);

		if ( hFind == INVALID_HANDLE_VALUE ) {

			if ( f == 0 )
				continue;
			else   {
				FindClose(hFind);
				break;
			}
		}
		
		do {
		
			if ( !LIBIMPORT_ImportarPKCS12((string("A:\\") + string(findData.cFileName)).c_str(), 
										   pwd.GetBuffer(0), 
										   &hClauer) ) 
			{

				LIBRT_FinalizarDispositivo(&hClauer);

				m_strErrMsg = IDIOMA_Get(IDIOMA_NO_IMPORTAR_PKCS12);
				string errMsg;
				ENoPKCS12 e(errMsg);
				//throw e;
				return 0;
			}

			++pkcs12Encontrados;

			masFicheros = FindNextFile(hFind, &findData);

			if ( !masFicheros ) {
				DWORD err;
				err = GetLastError();
				if ( err == ERROR_NO_MORE_FILES )
					break;
				else {
					m_strErrMsg = IDIOMA_Get(IDIOMA_ERROR_ENTRADA_SALIDA);
					string errMsg;
					ENoPKCS12 e(errMsg);
					//throw e;
					return 0;
				}
			}
		
		} while (1);

		FindClose(hFind);
	}

	if ( pkcs12Encontrados == 0 ) {
		
		m_strErrMsg = IDIOMA_Get(IDIOMA_NO_CERT_DISQUET);
		string errMsg;
		ENoPKCS12 e(errMsg);
	//	throw e;
		return 0;
	}
	return 1;
}


void CAdmin_CLAUERDlg::Verificar(CString pwd, CString ruta)
{

	FILE *fp;
	BYTE *pkcs12;
	DWORD tamPKCS12;
	int ok;
	fp = fopen(ruta.GetBuffer(0), "rb");
	if ( !fp ) {
		string errMsg;
		errMsg = IDIOMA_Get(IDIOMA_NO_LEER_CERTIFICADO);
		ENoPKCS12 e(errMsg);
		m_dlgPaso5_3.m_pin.Empty();
		m_dlgPaso5_3.UpdateData(FALSE);
		throw e;
	}

	fseek(fp, 0, SEEK_END);
	tamPKCS12 = ftell(fp);
	fseek(fp, 0, SEEK_SET);

	pkcs12 = new BYTE [tamPKCS12];
	fread(pkcs12, tamPKCS12, 1, fp);
	fclose(fp);

	ok = CRYPTO_PKCS12_VerificarPassword (pkcs12, tamPKCS12, pwd.GetBuffer(0));

	SecureZeroMemory(pkcs12, tamPKCS12);
	delete [] pkcs12;
	pkcs12 = NULL;

	if ( ok == 0 ) {
	/* Password incorrecta */
	
		fclose(fp);
		string errMsg;
		errMsg = IDIOMA_Get(IDIOMA_PIN_INCORRECTO);
		m_dlgPaso5_3.m_pin.Empty();
		m_dlgPaso5_3.UpdateData(FALSE);
		EPwdIncorrecta e(errMsg);
		throw e;
	} else if ( ok == 2 ) {
	/* Error */

		fclose(fp);
		string errMsg;
		errMsg = IDIOMA_Get(IDIOMA_ERROR_VERIFICANDO_PASS);
		m_dlgPaso5_3.m_pin.Empty();
		m_dlgPaso5_3.UpdateData(FALSE);
		EExcepcion e(errMsg);
		throw e;
	}
}

int CAdmin_CLAUERDlg::Importar(CString pwd,int numDispositivos,CString pin,CString ruta)
{
	/* Importar los certificados del disquete
	 */
	USBCERTS_HANDLE hClauer;

	CString m_strErrMsg;
	int result;
	int *dispositivos = NULL;
	char ** unidades = NULL;


	LIBRT_Ini();


	unidades = new char * [numDispositivos];

	for ( int j=0; j<numDispositivos; j++ ) 
		unidades[j] = new char[5];
		
	dispositivos = new int [numDispositivos];

	result = lowlevel_enumerar_dispositivos( unidades, dispositivos, &numDispositivos );

	if ( result != ERR_LOWLEVEL_NO ) {
		m_strErrMsg = IDIOMA_Get(IDIOMA_NO_INICIALIZAR);
		string errMsg;
		ENoPKCS12 e(errMsg);
	//	throw e;
		return 0;
	}

	unsigned char * dispositius[MAX_DEVICES];
	int numDisp;
			
	LIBRT_ListarDispositivos(&numDisp,dispositius);

	if ( LIBRT_IniciarDispositivo(dispositius[0],this->pin.GetBuffer(0),&hClauer) != 0 ) {
	//if ( LIBRT_IniciarDispositivo ( dispositivos[0], pin.GetBuffer(0), &hClauer ) != 0 ) {
		m_strErrMsg =IDIOMA_Get(IDIOMA_NO_INICIALIZAR);
		string errMsg;
		ENoPKCS12 e(errMsg);
//		throw e;
		return 0;
	}


		
	if ( !LIBIMPORT_ImportarPKCS12(ruta.GetBuffer(0),pwd.GetBuffer(0), &hClauer) ) 
	{

		LIBRT_FinalizarDispositivo(&hClauer);

		m_strErrMsg =IDIOMA_Get(IDIOMA_NO_IMPORTAR_PKCS12);
		string errMsg;
		ENoPKCS12 e(errMsg);
	//	throw e;
		return 0;
	}
	return 1;
}

int CAdmin_CLAUERDlg::SeguridadPasswd(CString pwd)
{
	int i, lon;
	int num = 0;
	int letras = 0;
/*	int igual =0;
	int buscar = 0;*/

	lon=pwd.GetLength();

	for (i=0; i<lon;i++)
	{

		int igual =0;
		int buscar = 0;
		//int indice = 0;

		if( isdigit(pwd[i]) ) {
			buscar=pwd.Find(pwd[i],i+1);
			if(buscar!= -1) {
			//	indice=i+2;
				igual=igual+2;
				while (buscar !=-1) {
					buscar=pwd.Find(pwd[i],buscar+1);
					//indice=indice+1;
					if(buscar != -1)
						igual=igual+1;
				
				 }
			}
			if(lon <=10) {
				if(igual >= 4){
					AfxMessageBox(IDIOMA_Get(IDIOMA_PASS_NO_SEGURA), MB_OK,0);
					return 0;
				}
			}else{
				if(lon/2<=igual){
				AfxMessageBox(IDIOMA_Get(IDIOMA_PASS_NO_SEGURA), MB_OK,0);
				return 0;
				}
			}
			num=num+1;
		}else if( isalpha(pwd[i]) ) {
			buscar=pwd.Find(pwd[i],i+1);
			if(buscar!= -1) {
			//	indice=i+2;
				igual=igual+2;
				while (buscar !=-1) {
					buscar=pwd.Find(pwd[i],buscar+1);
				//	indice=indice+1;
					if(buscar != -1)
						igual=igual+1;
				
				 }
			}
			if(lon <=10) {
				if(igual >= 4){
					AfxMessageBox(IDIOMA_Get(IDIOMA_PASS_LETRAS), MB_OK,0);
					return 0;
				}
			}else{
				if(lon/2<=igual){
				AfxMessageBox(IDIOMA_Get(IDIOMA_PASS_LETRAS), MB_OK,0);
				return 0;
				}
			}
			letras=letras+1;
		}
	}

	if(letras<3){
		AfxMessageBox(IDIOMA_Get(IDIOMA_MIN_LETRAS), MB_OK,0);
		return 0;
	}else if(num<3){
		AfxMessageBox(IDIOMA_Get(IDIOMA_MIN_NUM), MB_OK,0);
		return 0;
	}else{
		return 1;
	}
}

