#if !defined(AFX_DLGPASO5_2_H__C8F6C5D9_0ED7_450E_9AB2_39C91EEEC53A__INCLUDED_)
#define AFX_DLGPASO5_2_H__C8F6C5D9_0ED7_450E_9AB2_39C91EEEC53A__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// DlgPaso5_2.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// DlgPaso5_2 dialog

class DlgPaso5_2 : public CDialog
{
// Construction
public:
	DlgPaso5_2(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(DlgPaso5_2)
	enum { IDD = IDD_DIALOG_PASO_5_2 };
	CButton	m_desc;
	CStatic	m_texto;
	CString	m_ruta;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(DlgPaso5_2)
	public:
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(DlgPaso5_2)
	virtual BOOL OnInitDialog();
	afx_msg void OnBotonRuta();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DLGPASO5_2_H__C8F6C5D9_0ED7_450E_9AB2_39C91EEEC53A__INCLUDED_)
