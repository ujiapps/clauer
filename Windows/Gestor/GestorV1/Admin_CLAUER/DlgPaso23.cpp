// DlgPaso23.cpp : implementation file
//

#include "stdafx.h"
#include "Admin_CLAUER.h"
#include "DlgPaso23.h"
#include "Admin_CLAUERDlg.h"
#include "IDIOMA.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// DlgPaso23 dialog


DlgPaso23::DlgPaso23(CWnd* pParent /*=NULL*/)
	: CDialog(DlgPaso23::IDD, pParent)
{
	//{{AFX_DATA_INIT(DlgPaso23)
	m_radio = -1;
	//}}AFX_DATA_INIT
}


void DlgPaso23::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(DlgPaso23)
	DDX_Control(pDX, IDC_UB, m_ub);
	DDX_Control(pDX, IDC_IE, m_ie);
	DDX_Control(pDX, IDC_DISQ, m_disq);
	DDX_Control(pDX, IDC_CUADRO, m_cuadro);
	DDX_Control(pDX, IDC_TEXTO, m_texto);
	DDX_Radio(pDX, IDC_RADIO1, m_radio);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(DlgPaso23, CDialog)
	//{{AFX_MSG_MAP(DlgPaso23)
	ON_WM_SHOWWINDOW()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// DlgPaso23 message handlers

void DlgPaso23::OnShowWindow(BOOL bShow, UINT nStatus) 
{
	CDialog::OnShowWindow(bShow, nStatus);
	
	// TODO: Add your message handler code here
	CFont *font = new CFont;
	font->CreatePointFont(12, "System");
	m_texto.SetFont(font);
	delete font;

	m_texto.SetWindowText(IDIOMA_Get(IDIOMA_PASO_23_NEGRITA));
	m_cuadro.SetWindowText(IDIOMA_Get(IDIOMA_PASO_23_CUADRO));
	m_disq.SetWindowText(IDIOMA_Get(IDIOMA_PASO_23_DISQ));
	m_ub.SetWindowText(IDIOMA_Get(IDIOMA_PASO_23_UB));
	m_ie.SetWindowText(IDIOMA_Get(IDIOMA_PASO_23_IE));
}

BOOL DlgPaso23::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here
	m_radio = 0;
	UpdateData(FALSE);
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

BOOL DlgPaso23::PreTranslateMessage(MSG* pMsg) 
{
	// TODO: Add your specialized code here and/or call the base class
	if(pMsg->message==WM_KEYDOWN)
	{
		if ( (pMsg->wParam==VK_ESCAPE) )
			pMsg->wParam=NULL ;
		else if ( (pMsg->wParam==VK_RETURN) ) {
			((CAdmin_CLAUERDlg *) this->GetParent())->OnBotonSiguiente();
			return TRUE;
		}
	}
	return CDialog::PreTranslateMessage(pMsg);
}
