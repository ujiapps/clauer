// DlgContras.cpp : implementation file
//

#include "stdafx.h"
#include "Admin_CLAUER.h"
#include "DlgContras.h"
#include "IDIOMA.h"
#include ".\dlgcontras.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// DlgContras dialog



DlgContras::DlgContras(CWnd* pParent /*=NULL*/)
	: CDialog(DlgContras::IDD, pParent)
{
	//{{AFX_DATA_INIT(DlgContras)
	m_pin = _T("");
	//}}AFX_DATA_INIT
}


void DlgContras::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(DlgContras)
	DDX_Control(pDX, IDC_TEXTO, m_texto);
	DDX_Control(pDX, IDC_CUADRO, m_cuadro);
	DDX_Control(pDX, IDC_CONTR, m_contr);
	DDX_Control(pDX, IDOK, m_ok);
	DDX_Control(pDX, IDCANCEL, m_cancel);
	DDX_Text(pDX, IDC_PIN, m_pin);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(DlgContras, CDialog)
	//{{AFX_MSG_MAP(DlgContras)
	ON_WM_SHOWWINDOW()
	//}}AFX_MSG_MAP
	ON_BN_CLICKED(IDOK, OnBnClickedOk)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// DlgContras message handlers




void DlgContras::OnShowWindow(BOOL bShow, UINT nStatus) 
{
	CDialog::OnShowWindow(bShow, nStatus);
	
	// TODO: Add your message handler code here
}

BOOL DlgContras::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here

	m_texto.SetWindowText(IDIOMA_Get(IDIOMA_DIALOG_CONTRAS_TEXTO));
	m_cuadro.SetWindowText(IDIOMA_Get(IDIOMA_DIALOG_CONTRAS_CUADRO));
	m_contr.SetWindowText(IDIOMA_Get(IDIOMA_DIALOG_CONTRAS_CONTR));
	m_ok.SetWindowText(IDIOMA_Get(IDIOMA_BOTON_ACEPTAR));
	m_cancel.SetWindowText(IDIOMA_Get(IDIOMA_BOTON_CANCEL));
	this->SetWindowText(IDIOMA_Get(IDIOMA_DIALOG_CONTRAS_TITULO));
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void DlgContras::OnBnClickedOk()
{
	// TODO: Agregue aqu� su c�digo de controlador de notificaci�n de control
	OnOK();
}
