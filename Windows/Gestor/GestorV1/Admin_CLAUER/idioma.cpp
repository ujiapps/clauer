#include "stdafx.h"
#include "IDIOMA.h"

#include <stdio.h>

static char *idioma = NULL;
static char *etiquetas[NUM_ETIQUETAS];


/*! TRUE Ok
 *  FALSE error
 */

BOOL IDIOMA_Cargar (void)
{

	HKEY hKey;
	DWORD tamValue;

	if ( idioma != NULL )
		return FALSE;

	if ( RegOpenKeyEx(HKEY_LOCAL_MACHINE, 
			     "SOFTWARE\\Universitat Jaume I\\Projecte Clauer", 
				 0,
				 KEY_QUERY_VALUE,
				 &hKey) != ERROR_SUCCESS ) 
	{
		return FALSE;
	}

	if ( RegQueryValueEx(hKey,"Idioma", NULL, NULL, NULL, &tamValue) != ERROR_SUCCESS ) {
		RegCloseKey(hKey);
		return FALSE;
	}

	if ( tamValue > 5 ) {
		RegCloseKey(hKey);
		return FALSE;
	}

	idioma = new char [tamValue];

	if ( !idioma ) {
		RegCloseKey(hKey);
		return FALSE;
	}

	if ( RegQueryValueEx(hKey,"Idioma", NULL, NULL, (BYTE *)idioma, &tamValue) != ERROR_SUCCESS ) {
		delete [] idioma;
		idioma = NULL;
		RegCloseKey(hKey);
		return FALSE;
	}

	RegCloseKey(hKey);

	/* Cargamos la tabla de etiquetas
	 */

	if ( strcmp(idioma, "1027") == 0 )
	{
		/* Valenci�
		 */
		
		//BOTONES
		etiquetas[IDIOMA_BOTON_CANCEL]						= "Cancel�lar";
		etiquetas[IDIOMA_BOTON_SIGUIENTE]					= "Seg�ent >";
		etiquetas[IDIOMA_BOTON_FINALIZAR]					= "Finalitzar";
		etiquetas[IDIOMA_BOTON_EMPEZAR]						= "Comen�ar";	
		etiquetas[IDIOMA_BOTON_IMPORTAR]					= "Importar";		
		etiquetas[IDIOMA_BOTON_ELIMINAR]					= "Formatejar";
		etiquetas[IDIOMA_BOTON_ANTERIOR]					= "< Anterior";
		etiquetas[IDIOMA_BOTON_ACEPTAR]						= "Acceptar";
		
		
		etiquetas[IDIOMA_PASO1_NEGRITA]						= "Introdueix el stick USB si no ho ha fet";
		etiquetas[IDIOMA_PASO1_EXPLICACION]					= "Aquest programa li permetr� modificar o preparar un nou Clauer, per favor segueix les instruccions que es mostren en pantalla.";

		etiquetas[IDIOMA_PASO2_NEGRITA]						= "El Clauer no t� format criptogr�fic, es procedir� a formatejar-lo. Per a aix� deu pensar una contrasenya suficientment segura.";
		etiquetas[IDIOMA_PASO2_CUADRO1]						= "Contrasenya del Clauer";
		etiquetas[IDIOMA_PASO2_CONTRAS]						= "Contrasenya:";
		etiquetas[IDIOMA_PASO2_CONF]						= "Confirmaci�:";
		etiquetas[IDIOMA_PASO2_CUADRO]						="Contrasenya del Clauer";
		etiquetas[IDIOMA_PASO2_NEGR_FOR]			        ="Es va a procedir a formatejar la zona criptogr�fica. Per a aix� deu pensar una contrasenya suficientment segura."; 

		etiquetas[IDIOMA_PASO3_NEGRITA]						= "Es va a preparar el Clauer. Per favor no retiri l'estic USB mentre duri l'operaci�.";
		etiquetas[IDIOMA_PASO3_BARRA]						= "Progr�s de l'operaci�";
		etiquetas[IDIOMA_PASO3_DESCRIP]						= "Descripci�";

		etiquetas[IDIOMA_PASO4_NEGRITA]						= "El Clauer ha estat particionat correctament, seleccioni una opci�.";
		etiquetas[IDIOMA_PASO4_OPC]							= "Opcions";
		etiquetas[IDIOMA_PASO4_DISQ]						= "Importar certificat del disquet de la UJI";
		etiquetas[IDIOMA_PASO4_UBIC]						= "Importar certificat des d'altra ubicaci�";
		etiquetas[IDIOMA_PASO4_IE]							= "Importar certificat instal�lat en l'Internet Explorer";
		etiquetas[IDIOMA_PASO4_FIN]							= "Finalitzar";

		etiquetas[IDIOMA_PASO5_1_NEGRITA1]					= "1. Introdueix el disquet amb els certificats que li va proporcionar el personal de registre de la UJI";
		etiquetas[IDIOMA_PASO5_1_NEGRITA2]					= "2 .Introdueixi el PIN del disquet";
		etiquetas[IDIOMA_PASO5_1_CUADRO]					= "PIN del disquet";
		etiquetas[IDIOMA_PASO5_1_PIN]						= "PIN:";

		etiquetas[IDIOMA_PASO5_2_NEGRITA]					= "Introdueixi la ubicaci� del certificat que desitja importar";
		etiquetas[IDIOMA_PASO5_2_CUADRO]					= "Ubicaci�";

		etiquetas[IDIOMA_PASO5_3_NEGRITA]					= "Introdueixi la contrasenya del certificat";
		etiquetas[IDIOMA_PASO5_3_CUADRO]					= "Contrasenya del certificat";
		etiquetas[IDIOMA_PASO5_3_CONTR]						= "Contrasenya:";

		etiquetas[IDIOMA_PASO5_6_NEGRITA]					= "El Gestor del Clauer ha finalitzat correctament.";
		etiquetas[IDIOMA_PASO5_6_DESCRIP1]					= "El Clauer ha sigut formatejat i particionat correctament. Si tamb� ha triat importar un certificat podr� usar el Clauer com dispositiu criptogr�fic.";
		etiquetas[IDIOMA_PASO5_6_DESCRIP2]					= "Per a modificar el Clauer �nicament deus tornar a utilitzar aquest programa.";

		etiquetas[IDIOMA_DIALOG_CONTRAS_TEXTO]				= "Introdueix la contrasenya que protegeix el seu Clauer";
		etiquetas[IDIOMA_DIALOG_CONTRAS_CUADRO]				= "Contrasenya";
		etiquetas[IDIOMA_DIALOG_CONTRAS_CONTR]				= "Contrasenya:";
		etiquetas[IDIOMA_DIALOG_CONTRAS_TITULO]				= "Contrasenya del Clauer";

		etiquetas[IDIOMA_DIALOG_WIN98_NEGRITA]				= "Introdueix el disquet amb els certificats que se li va proporcionar juntament amb el Clauer.";
		etiquetas[IDIOMA_DIALOG_WIN98_CUADRO1]				= "Introdueixi PIN actual";
		etiquetas[IDIOMA_DIALOG_WIN98_CUADRO2]				= "Introdueixi nou PIN";
		etiquetas[IDIOMA_DIALOG_WIN98_PIN]					= "PIN:";
		etiquetas[IDIOMA_DIALOG_WIN98_NUEVO_PIN]			= "Nou PIN:";
		etiquetas[IDIOMA_DIALOG_WIN98_CONF]					= "Confirmaci�";
		etiquetas[IDIOMA_DIALOG_WIN98_BOTON_MODIFICAR]		= "Modificar";
		etiquetas[IDIOMA_DIALOG_WIN98_TITULO]				= "Gestor del Clauer:: Modificar PIN del disquet";

		etiquetas[IDIOMA_DIALOG_SEGURO_TEXTO]				= "Es va a procedir a formatejar la zona criptogr�fica i tots els certificats que contingui el Clauer seran eliminats.";
		etiquetas[IDIOMA_DIALOG_SEGURO_PREGUNTA]			= "Esta segur de formatejar la zona criptogr�fica? ";
		etiquetas[IDIOMA_DIALOG_SEGURO_SI]					= "Si";
		etiquetas[IDIOMA_DIALOG_SEGURO_NO]					= "No";

		etiquetas[IDIOMA_PASO_50_NEGRITA]					= "El Gestor del Clauer ha finalitzat correctament.";
		etiquetas[IDIOMA_PASO_50_DESCRIP1]					= "Per a modificar el Clauer �nicament deus tornar a emprar aquest programa.";

		etiquetas[IDIOMA_PASO_20_NEGRITA]					= "S'ha detectat un Clauer que cont� una zona criptogr�fica, seleccioni una opci�.";
		etiquetas[IDIOMA_PASO_20_MODIFICAR_PASSWORD]		= "Modificar contrasenya";
		etiquetas[IDIOMA_PASO_20_GESTIONAR_CERTIFICADOS]	= "Gestionar certificats";
		etiquetas[IDIOMA_PASO_20_ELIMINAR_CRIPTO]			= "Formatejar zona criptogr�fica";
		etiquetas[IDIOMA_PASO_20_MODIFICAR_ZONA]			= "Modificar grand�ria de la zona criptogr�fica";
		etiquetas[IDIOMA_PASO_20_FIN]						= "Finalitzar";

		etiquetas[IDIOMA_PASO_21_NEGRITA]					= "Modificar contrasenya";
		etiquetas[IDIOMA_PASO_21_CUADRO1]					= "Introdueix la contrasenya actual";
		etiquetas[IDIOMA_PASO_21_CUADRO2]					= "Introdueix la nova contrasenya";
		etiquetas[IDIOMA_PASO_21_CONTRA]					= "Contrasenya:";
		etiquetas[IDIOMA_PASO_21_NUEVA]						= "Nova contrasenya:";
		etiquetas[IDIOMA_PASO_21_CONF]						= "Confirmaci�:";

		etiquetas[IDIOMA_PASO_22_NEGRITA]					= "Seleccioni una opci�";
		etiquetas[IDIOMA_PASO_22_CUADRO]					= "Gestionar certificats";
		etiquetas[IDIOMA_PASO_22_IMP]						= "Importar certificats al clauer";	
		etiquetas[IDIOMA_PASO_22_EXP]						= "Exportar certificats des del clauer";
		etiquetas[IDIOMA_PASO_22_ELIM]						= "Eliminar certificats del clauer";
		etiquetas[IDIOMA_PASO_22_DISQ]						= "Modificar PIN del disquet de la UJI";

		etiquetas[IDIOMA_PASO_23_NEGRITA]					= "Seleccioni una opci�";
		etiquetas[IDIOMA_PASO_23_CUADRO]					= "Opcions";
		etiquetas[IDIOMA_PASO_23_DISQ]						= "Importar certificat del disquet de la UJI";
		etiquetas[IDIOMA_PASO_23_UB]						= "Importar certificat des d'altra ubicaci�";
		etiquetas[IDIOMA_PASO_23_IE]						= "Importar certificat instal�lat en l'Internet Explorer";

		etiquetas[IDIOMA_PASO_28_TEXT1]						= "Formatejar zona criptogr�fica";
		etiquetas[IDIOMA_PASO_28_TEXT2]						= "Si elimina la zona criptogr�fica no podr� utilitzar el clauer per a l'emmagatzematge, transport i �s de certificats digitals i les seves claus privades associades.";
		etiquetas[IDIOMA_PASO_28_TEXT3]						= "Solament podr� utilitzar el clauer com disc dur de butxaca.";

		etiquetas[IDIOMA_FICHERO_ABIERTO]					= "L'aplicaci� ja est� oberta";

		etiquetas[IDIOMA_CREAR_FICHERO]						= "Error al crear el fitxer de bloqueig";

		etiquetas[IDIOMA_NO_ENCONTRAR_STICK]				= "No es va poder determinar la pres�ncia de sticks en el sistema";
		
		etiquetas[IDIOMA_NO_HAY_STICK]						= "No hi ha cap stick USB inserit en el sistema\nPer favor, premi el bot� Acceptar i insereix el dispositiu\nque desitgi inicialitzar";

		etiquetas[IDIOMA_MUCHOS_STICKS]						= "Hi ha m�s d'un stick USB inserit en el sistema\nPer favor, premi el bot� Acceptar i deixi inserit\n�nicament el dispostiu que desitgi inicialitzar";

		etiquetas[IDIOMA_NO_INICIALIZAR]					= "Impossible realitzar la inicialitzaci� del dispositiu.\n";
	
		etiquetas[IDIOMA_COMPROBANDO_ADMINISTRADOR]			= "Impossible comprovar si ets administrador.\n";

		etiquetas[IDIOMA_NO_ADMINISTRADOR]					= "Deus ser administrador per a poder preparar un nou Clauer";

		etiquetas[IDIOMA_IMPOSIBLE_CERRAR_DISP]				= "Impossible tancar dispositiu.\n";

		etiquetas[IDIOMA_PIN_COINCIDENCIA]					= "El PIN i la seva confirmaci� deuen coincidir";
		
		etiquetas[IDIOMA_LONGITUD_PIN]						= "Deu introduir un PIN d'un m�nim de 8 car�cters";

		etiquetas[IDIOMA_NO_BASE]							= "Necessites tenir instal�lat el programari base";

		etiquetas[IDIOMA_NO_IMPLEMENTADO]					= "Opci� no implementada.";
		
		etiquetas[IDIOMA_TITULO_PIN_INCORRECTO]				= "Gestor Clauer :: ERROR :: PIN Incorrecte";

		etiquetas[IDIOMA_TITULO_NO_CERTIFICADOS]			= "Gestor Clauer :: ERROR :: No es van trobar certificats";

		etiquetas[IDIOMA_NO_IMPORTAR_CERTIFICADO]			= "No es va poder importar el certificat";

		etiquetas[IDIOMA_SELECCIONAR_CERTIFICADO]			= "Deu seleccionar un certificat";

		etiquetas[IDIOMA_CONTRASENYA_MAL]					= "Contrasenya del Clauer incorrecta\n";

		etiquetas[IDIOMA_TITULO_CONTRASENYA_MAL]			= "Clauer :: ERROR :: Contrasenya Clauer";

		etiquetas[IDIOMA_NO_MODIFICAR_CONTRASENYA]			= "No ha estat possible modificar la contrasenya del Clauer\n";

		etiquetas[IDIOMA_NO_FINALIZADO]						= "Error finalitzant dispositiu\n";

		etiquetas[IDIOMA_CONTRASENYA_MODIFICADA]			= "La contrasenya s'ha modificat correctament.\n";

		etiquetas[IDIOMA_CONTRASENYA_NO_CORRECTA]			= "Contrasenya del Clauer incorrecta\n";

		etiquetas[IDIOMA_TITULO_ERROR_BUSCANDO]				= "Clauer :: ERROR :: Buscant sticks";

		etiquetas[IDIOMA_NO_ENCONTRADOS_CERTIFICADOS]		= "Gestor Clauer :: ERROR :: No es van trobar certificats ";

		etiquetas[IDIOMA_CERTIFICADO_OK]					= "El certificat s'ha importat correctament.\n";

		etiquetas[IDIOMA_TITULOS_CERTIFICADOS]				= "Gestor Clauer :: Certificats";

		etiquetas[IDIOMA_NO_MODIFICAR_MBR]					="Impossible canviar particions del mbr.\n";

		etiquetas[IDIOMA_RETIRAR_STICK]						= "Per favor, retiri el stick USB i premi el bot� Acceptar\n";

		etiquetas[IDIOMA_TITULO_RETIRAR]					= "Gestor Clauer :: Extregui el stick";

		etiquetas[IDIOMA_INSERTAR_STICK]					= "Per favor, inserti de nou el stick i premi el bot� Acceptar\n";

		etiquetas[IDIOMA_TITULO_INSERTAR]					= "Gestor Clauer :: Inserti el stick";

		etiquetas[IDIOMA_TITULO_ELIMINAR_ZONA_CRIPTO]		= "Gestor Clauer :: Formatejar zona criptogr�fica";

		etiquetas[IDIOMA_NO_INSERTADO]						= "No ha insertit encara el seu stick USB. Per favor,\n inserti de nou i premi el bot� Acceptar";

		etiquetas[IDIOMA_MUCHOS_STICKS_INSERT]				= "El programa ha detectat m�s de un stick USB insertat en el sistema.\n Per favor, deixi insertat �nicament el stick amb el qual va comen�ar el proc�s\n de inicialitzaci�.";

		etiquetas[IDIOMA_IMPOSIBLE_FORMATEAR_LOGICA]		= "Impossible formatar unitat l�gica del dispositiu.\n";

		etiquetas[IDIOMA_ERROR_FORMATEANDO]					= "Clauer :: ERROR :: Formatejant";

		etiquetas[IDIOMA_ELIMINADO_CRIPTO]					= "S'ha eliminat la zona criptogr�fica correctament.\n"; 

		etiquetas[IDIOMA_TITULO_CRIPTO]						= "Gestor Clauer :: Zona criptogr�fica";

		etiquetas[IDIOMA_PROCESO_FINALIZADO]				= "El proc�s ha acabar amb �xit\nPulse Acceptar";

		etiquetas[IDIOMA_TITULO_FINALIZADO]					= "Clauer :: Finalitzat";

		etiquetas[IDIOMA_NO_CERT_DISQUET]					= "No es va poder llegir certificat en la unitat A:\n";

		etiquetas[IDIOMA_PIN_INCORRECTO]					= "PIN incorrecte. Torni a introduir el PIN.\nComproba que ha respetat maj�scules i min�scules (si les hagu�s)\nPulse el bot� Acceptar";

		etiquetas[IDIOMA_ERROR_VERIFICANDO_PASS]			= "Va oc�rrer un error verificant el seu password. Provi de nou. \n";

		etiquetas[IDIOMA_ERROR_ENTRADA_SALIDA]				= "Va oc�rrer un error E/S.\n";

		etiquetas[IDIOMA_NO_IMPORTAR_PKCS12]				= "No es van poder importar els PKCS12";

		etiquetas[IDIOMA_PASS_NO_SEGURA]					= "La nova contrasenya no �s prou segura, prova amb una altra.\nHi ha massa nombres repetits.";

		etiquetas[IDIOMA_PASS_LETRAS]						= "La nova contrasenya no �s prou segura, prova amb una altra.\nHi ha massa lletres repetides.";

		etiquetas[IDIOMA_MIN_LETRAS]						= "La nova contrasenya no �s prou segura, prova amb una altra.\nDeu emprar com a m�nim 3 lletres.";
	
		etiquetas[IDIOMA_MIN_NUM]							= "La nova contrasenya no �s prou segura, prova amb una altra.\nDeu emprar com a m�nim 3 nombres.";

		etiquetas[IDIOMA_INICIALIZANDO]						= "Inicialitzant dispositiu";

		etiquetas[IDIOMA_FORMATEANDO]						= "Formatejant dispositiu...";

		etiquetas[IDIOMA_ERROR_GRAVE]						= "Va oc�rrer un error greu en l'aplicaci�.\n";

		etiquetas[IDIOMA_NO_DETERMINAR_IDIOMA]				= "No s'ha pogut determinar l'idioma.";

		etiquetas[IDIOMA_NO_NT_VIEJO]						= "El Gestor no suporta Windows NT 3.51";

		etiquetas[IDIOMA_ADVERTENCIA_CAMBIO_PIN]			= "Esta utilitzant Windows 95/98/Me/NT 4.0\\nEl Gestor solament li permitira modificar el PIN del disquet de la UJI";

		etiquetas[IDIOMA_ABRIR_CERTIFICADO]					= "No es va poder obrir el certificat.";
	
		etiquetas[IDIOMA_ESCRIBIR_CERTIFICADOS]				= "No es va poder escriure el certificat.";
		
		etiquetas[IDIOMA_CERRAR_CERTIFICADOS]				= "No es va poder tancar el certificat.";

		etiquetas[IDIOMA_NO_MODIFICAR_PIN]					= "No es va poder modificar el PIN del disquet.\n";

		etiquetas[IDIOMA_PIN_MODIFICADO]					= "PIN del disquet modificat correctament.";

		etiquetas[IDIOMA_NO_LEER_CERTIFICADO]				= "No es va poder llegir certificat\n";

//	} else if ( strcmp(idioma, "1033") == 0 ){
		/* Idioma no reconocido 
		Entonces Ingles
		 */
/*		etiquetas[IDIOMA_BOTON_CANCEL]						= "Cancel";
		etiquetas[IDIOMA_BOTON_SIGUIENTE]					= "Next >";
		etiquetas[IDIOMA_BOTON_FINALIZAR]					= "Finalize";
		etiquetas[IDIOMA_BOTON_EMPEZAR]						= "Begin";
		etiquetas[IDIOMA_BOTON_IMPORTAR]					= "Importar";
		etiquetas[IDIOMA_BOTON_ELIMINAR]					= "Eliminate";
		etiquetas[IDIOMA_BOTON_ANTERIOR]					= "< Previous";
		etiquetas[IDIOMA_BOTON_ACEPTAR]						= "Accept";
		
		
		etiquetas[IDIOMA_PASO1_NEGRITA]						= "Introduce USB stick if it has not done it";
		etiquetas[IDIOMA_PASO1_EXPLICACION]					= "This program will allow him to modify or to prepare a new Clauer, please follows the instructions that are in screen.";

		etiquetas[IDIOMA_PASO2_NEGRITA]						= ***"The Clauer does not have cryptographic format, is going away to come to *formatear it. For it it must think a sufficiently safe password.";
		etiquetas[IDIOMA_PASO2_CUADRO1]						= "Password of the Clauer";
		etiquetas[IDIOMA_PASO2_CONTRAS]						= "Password";
		etiquetas[IDIOMA_PASO2_CONF]						= "Confirmation:";
		etiquetas[IDIOMA_PASO2_CUADRO]						= "Password of the Clauer";
		etiquetas[IDIOMA_PASO2_NEGR_FOR]			        = "";

		etiquetas[IDIOMA_PASO3_NEGRITA]						= "One is going away to prepare the Clauer.  Please it does not withdraw USB stick while the operation lasts.";
		etiquetas[IDIOMA_PASO3_BARRA]						= "Progress of the operation";
		etiquetas[IDIOMA_PASO3_DESCRIP]						= "Description";

		etiquetas[IDIOMA_PASO4_NEGRITA]						= "El Clauer ha sido particionado correctamente, elija una opci�n.";
		etiquetas[IDIOMA_PASO4_OPC]							= "Options";
		etiquetas[IDIOMA_PASO4_DISQ]						= "Importar certificado del disquete de la UJI";
		etiquetas[IDIOMA_PASO4_UBIC]						= "Importar certificado desde otra ubicaci�n";
		etiquetas[IDIOMA_PASO4_IE]							= "Importar certificado instalado en el  Internet Explorer";
		etiquetas[IDIOMA_PASO4_FIN]							= "Finalize";

		etiquetas[IDIOMA_PASO5_1_NEGRITA1]					= "1. Introduzca el disquete con los certificados que le proporciono el personal de registro de la UJI";
		etiquetas[IDIOMA_PASO5_1_NEGRITA2]					= "2 .Introduce floppy PIN";
		etiquetas[IDIOMA_PASO5_1_CUADRO]					= "The floppy PIN ";
		etiquetas[IDIOMA_PASO5_1_PIN]						= "PIN:";

		etiquetas[IDIOMA_PASO5_2_NEGRITA]					= "Introduce certificate's location that wishes to import";
		etiquetas[IDIOMA_PASO5_2_CUADRO]					= "Location";

		etiquetas[IDIOMA_PASO5_3_NEGRITA]					= "Introduce certificate's password";
		etiquetas[IDIOMA_PASO5_3_CUADRO]					= "Certificate's password";
		etiquetas[IDIOMA_PASO5_3_CONTR]						= "Password:";
	
		etiquetas[IDIOMA_PASO5_6_NEGRITA]					= "Manager has finalized correctly.";
		etiquetas[IDIOMA_PASO5_6_DESCRIP1]					= "El Clauer ha sido formateado y particionado correctamente. Si tambi�n ha elegido importar un certificado podr� usar el Clauer como dispositivo criptogr�fico.";
		etiquetas[IDIOMA_PASO5_6_DESCRIP2]					= "In order to modify the Clauer you must return to use this program.";

		etiquetas[IDIOMA_DIALOG_CONTRAS_TEXTO]				= "Introduce the password that protects its Clauer";
		etiquetas[IDIOMA_DIALOG_CONTRAS_CUADRO]				= "Password:";
		etiquetas[IDIOMA_DIALOG_CONTRAS_CONTR]				= "Password:";
		etiquetas[IDIOMA_DIALOG_CONTRAS_TITULO]				= "Clauer's password";

		etiquetas[IDIOMA_DIALOG_WIN98_NEGRITA]				= "Introduce the diskette with the certificates that was provided to him.";
		etiquetas[IDIOMA_DIALOG_WIN98_CUADRO1]				= "Introduce present PIN";
		etiquetas[IDIOMA_DIALOG_WIN98_CUADRO2]				= "Introduce new PIN";
		etiquetas[IDIOMA_DIALOG_WIN98_PIN]					= "PIN:";
		etiquetas[IDIOMA_DIALOG_WIN98_NUEVO_PIN]			= "New Pin:";
		etiquetas[IDIOMA_DIALOG_WIN98_CONF]					= "Confirmation";
		etiquetas[IDIOMA_DIALOG_WIN98_BOTON_MODIFICAR]		= "Modify";
		etiquetas[IDIOMA_DIALOG_WIN98_TITULO]				= "Clauer's Manager :: To modify PIN floppy";

		etiquetas[IDIOMA_DIALOG_SEGURO_TEXTO]				= "One is going away to come to eliminate the cryptographic zone and all the certificates that clauer contains will be eliminated.";
		etiquetas[IDIOMA_DIALOG_SEGURO_PREGUNTA]			= "Wish you to eliminate the cryptographic zone?";
		etiquetas[IDIOMA_DIALOG_SEGURO_SI]					= "Yes";
		etiquetas[IDIOMA_DIALOG_SEGURO_NO]					= "No";

		etiquetas[IDIOMA_PASO_50_NEGRITA]					= "Clauer's manager has finalized correctly.";
		etiquetas[IDIOMA_PASO_50_DESCRIP1]					= "In order to modify the Clauer you must return to use this program.";
		
		etiquetas[IDIOMA_PASO_20_NEGRITA]					= "A Clauer has been detected that contains a cryptographic zone, chooses an option.";
		etiquetas[IDIOMA_PASO_20_MODIFICAR_PASSWORD]		= "Modify password";
		etiquetas[IDIOMA_PASO_20_GESTIONAR_CERTIFICADOS]	= "Manage certificates";
		etiquetas[IDIOMA_PASO_20_ELIMINAR_CRIPTO]			= "Eliminate cryptographic zone";
		etiquetas[IDIOMA_PASO_20_MODIFICAR_ZONA]			= "Modify size of the cryptographic zone";
		etiquetas[IDIOMA_PASO_20_FIN]						= "Finalize";

		etiquetas[IDIOMA_PASO_21_NEGRITA]					= "Modify password";
		etiquetas[IDIOMA_PASO_21_CUADRO1]					= "Introduce the present password";
		etiquetas[IDIOMA_PASO_21_CUADRO2]					= "Introduce the new password";
		etiquetas[IDIOMA_PASO_21_CONTRA]					= "Password";
		etiquetas[IDIOMA_PASO_21_NUEVA]						= "New password";
		etiquetas[IDIOMA_PASO_21_CONF]						= "Confirmation:";

		etiquetas[IDIOMA_PASO_22_NEGRITA]					= "Choose an option";
		etiquetas[IDIOMA_PASO_22_CUADRO]					= "Manage certificates";
		etiquetas[IDIOMA_PASO_22_IMP]						= "Importar certificados al clauer";	
		etiquetas[IDIOMA_PASO_22_EXP]						= "Export certificates from clauer";
		etiquetas[IDIOMA_PASO_22_ELIM]						= "Eliminate certificates of clauer";
		etiquetas[IDIOMA_PASO_22_DISQ]						= "Modify PIN of the UJI floppy";
		
		etiquetas[IDIOMA_PASO_23_NEGRITA]					= "Choose an option";
		etiquetas[IDIOMA_PASO_23_CUADRO]					= "Opcions";
		etiquetas[IDIOMA_PASO_23_DISQ]						= "Import certificate of the UJI floppy";
		etiquetas[IDIOMA_PASO_23_UB]						= "Import certificate from another location";
		etiquetas[IDIOMA_PASO_23_IE]						= "Import certificate installed in the Internet Explorer";

		etiquetas[IDIOMA_PASO_28_TEXT1]						= "Eliminate cryptographic zone";
		etiquetas[IDIOMA_PASO_28_TEXT2]						= "If it eliminates the cryptographic zone will not be able to use clauer for the storage, it transports and digital certificate use and its private keys associated.";
		etiquetas[IDIOMA_PASO_28_TEXT3]						= "Single it will be able to use clauer as hard disk of pocket.";

		etiquetas[IDIOMA_FICHERO_ABIERTO]					= "The application already is open";

		etiquetas[IDIOMA_CREAR_FICHERO]						= "Error when creating the file of blockade";

		etiquetas[IDIOMA_NO_ENCONTRAR_STICK]				= "The presence of sticks in the system could not be determined";
		
		etiquetas[IDIOMA_NO_HAY_STICK]						= "There is no USB stick inserted in the system\nPlease, it presses the button Accept and it inserts the device\nhat wishes to initialize";

		etiquetas[IDIOMA_MUCHOS_STICKS]						= "It has more of a USB stick inserted in the system\nPlease, it presses the button Accept and it leaves inserted the device that wishes to initialize";
	
		etiquetas[IDIOMA_NO_INICIALIZAR]					= "Impossible to make the boot of the device.\n";
		
		etiquetas[IDIOMA_COMPROBANDO_ADMINISTRADOR]			= "Impossible to verify if you are administrator.\n";
		
		etiquetas[IDIOMA_NO_ADMINISTRADOR]					= "You must be administrator to be able to prepare a new Clauer";
		
		etiquetas[IDIOMA_IMPOSIBLE_CERRAR_DISP]				= "Impossible to close device.\n";

		etiquetas[IDIOMA_PIN_COINCIDENCIA]					= "The PIN and its confirmation must agree";
		
		etiquetas[IDIOMA_LONGITUD_PIN]						= "It must introduce a PIN of a minimum of 8 characters";

		etiquetas[IDIOMA_NO_BASE]							= "You need to have installed software base";
		
		etiquetas[IDIOMA_NO_IMPLEMENTADO]					= "Option non implemented.";
		
		etiquetas[IDIOMA_TITULO_PIN_INCORRECTO]				= "Clauer Manager:: ERROR:: Incorrect PIN";
		
		etiquetas[IDIOMA_TITULO_NO_CERTIFICADOS]			= "Clauer manager:: ERROR:: Was not certificates";
		
		etiquetas[IDIOMA_NO_IMPORTAR_CERTIFICADO]			= "The certificate could not be import";
		
		etiquetas[IDIOMA_SELECCIONAR_CERTIFICADO]			= "You must select a certificate";

		etiquetas[IDIOMA_CONTRASENYA_MAL]					= "Clauer's password incorrect\n";

		etiquetas[IDIOMA_TITULO_CONTRASENYA_MAL]			= "Clauer :: ERROR :: Password Clauer";

		etiquetas[IDIOMA_NO_MODIFICAR_CONTRASENYA]			= "It has not been possible to modify the password of the Clauer\n";

		etiquetas[IDIOMA_NO_FINALIZADO]						= "Error finalizing device\n";

		etiquetas[IDIOMA_CONTRASENYA_MODIFICADA]			= "Password has been modified correctly.\n";

		etiquetas[IDIOMA_CONTRASENYA_NO_CORRECTA]			= "Clauer's password incorrect\n";

		etiquetas[IDIOMA_TITULO_ERROR_BUSCANDO]				= "Clauer:: ERROR:: Looking for sticks";

		etiquetas[IDIOMA_NO_ENCONTRADOS_CERTIFICADOS]		= "Clauer manager:: ERROR:: Was not certificates";

		etiquetas[IDIOMA_CERTIFICADO_OK]					= "The certificate has been imported correctly.\n";

		etiquetas[IDIOMA_TITULOS_CERTIFICADOS]				= "Manager Clauer:: Certificates";

		etiquetas[IDIOMA_NO_MODIFICAR_MBR]					= "Impossible to change to partitions of mbr.\n";

		etiquetas[IDIOMA_RETIRAR_STICK]						= "Please, it retires stick USB and it presses the button Accept\n";

		etiquetas[IDIOMA_TITULO_RETIRAR]					= "Clauer manager:: Extract stick";

		etiquetas[IDIOMA_INSERTAR_STICK]					= "Please, it inserts stick again and it presses the button Accept\n";

		etiquetas[IDIOMA_TITULO_INSERTAR]					= "Clauer Manager :: Insert stick";

		etiquetas[IDIOMA_TITULO_ELIMINAR_ZONA_CRIPTO]		= "Clauer Manager :: Eliminate cryptographic zone";

		etiquetas[IDIOMA_NO_INSERTADO]						= "Its USB stick has still not inserted. Please,\n insert it and press the button Accept";

		etiquetas[IDIOMA_MUCHOS_STICKS_INSERT]				= "The program has detected more of a USB stick inserted in the system.\n Please, it leaves inserted the Stick solely with which the boot process began.";
		etiquetas[IDIOMA_IMPOSIBLE_FORMATEAR_LOGICA]		= "Imposible formatear unidad l�gica del dispositivo.\n";

		etiquetas[IDIOMA_ERROR_FORMATEANDO]					= "Clauer :: ERROR Formating";

		etiquetas[IDIOMA_ELIMINADO_CRIPTO]					= "The cryptographic zone has been eliminated correctly.\n";

		etiquetas[IDIOMA_TITULO_CRIPTO]						= "Clauer manager:: Cryptographic zone";

		etiquetas[IDIOMA_PROCESO_FINALIZADO]				= "The process finished successfully\nPress Accept";

		etiquetas[IDIOMA_TITULO_FINALIZADO]					= "Clauer :: Finalized";

		etiquetas[IDIOMA_NO_CERT_DISQUET]					= "Certificate in unit A: could not be read\n";

		etiquetas[IDIOMA_PIN_INCORRECTO]					= "Incorrect PIN.";

		etiquetas[IDIOMA_ERROR_VERIFICANDO_PASS]			= "Error verifying his password.\n";

		etiquetas[IDIOMA_ERROR_ENTRADA_SALIDA]				= "E/S error.\n";

		etiquetas[IDIOMA_NO_IMPORTAR_PKCS12]				= "The PKCS12 could not be import";

		etiquetas[IDIOMA_PASS_NO_SEGURA]					= "The new password is not the sufficiently safe thing.\nNo must use but of 3 equal numbers.";

		etiquetas[IDIOMA_PASS_LETRAS]						= "The new password is not the sufficiently safe thing.\nNo must use but of 3 equal letters.";

		etiquetas[IDIOMA_MIN_LETRAS]						= "The new password is not the sufficiently safe thing,.\n Must use like minimum 3 letters.";
		
		etiquetas[IDIOMA_MIN_NUM]							= "The new password is not the sufficiently safe thing.\n Must use like minimum 3 numbers.";

		etiquetas[IDIOMA_INICIALIZANDO]						= "Initializing device";

		etiquetas[IDIOMA_FORMATEANDO]						= "Formating device...";

		etiquetas[IDIOMA_ERROR_GRAVE]						= "Serious error in the application.\n";

		etiquetas[IDIOMA_NO_DETERMINAR_IDIOMA]				= "It has not been possible to determine the language.";

		etiquetas[IDIOMA_NO_NT_VIEJO]						= "Manager does not support Windows NT 3.51";

		etiquetas[IDIOMA_ADVERTENCIA_CAMBIO_PIN]			= "You are using Windows 95/98/Me/NT 4.0\nThis program will only allow you to change the PIN of the UJI floppy";

		etiquetas[IDIOMA_ABRIR_CERTIFICADO]					= "The certificate could not be opened.";
		
		etiquetas[IDIOMA_ESCRIBIR_CERTIFICADOS]				= "The certificate could not be written.";
		
		etiquetas[IDIOMA_CERRAR_CERTIFICADOS]				= "The certificate could not be closed.";

		etiquetas[IDIOMA_NO_MODIFICAR_PIN]					= "The floppy PIN could not be modified.\n";

		etiquetas[IDIOMA_PIN_MODIFICADO]					= "The floppy PIN modified correctly.";

		etiquetas[IDIOMA_NO_LEER_CERTIFICADO]				= "Certificate could not be read";

*/
	}else
	{
		/* Idioma no reconocido 
		Entonces Espa�ol
		 */

		//BOTONES
		etiquetas[IDIOMA_BOTON_CANCEL]						= "Cancelar";
		etiquetas[IDIOMA_BOTON_SIGUIENTE]					= "Siguiente >";
		etiquetas[IDIOMA_BOTON_FINALIZAR]					= "Finalizar";
		etiquetas[IDIOMA_BOTON_EMPEZAR]						= "Empezar";
		etiquetas[IDIOMA_BOTON_IMPORTAR]					= "Importar";
		etiquetas[IDIOMA_BOTON_ELIMINAR]					= "Formatear";
		etiquetas[IDIOMA_BOTON_ANTERIOR]					= "< Anterior";
		etiquetas[IDIOMA_BOTON_ACEPTAR]						= "Aceptar";
		
		
		etiquetas[IDIOMA_PASO1_NEGRITA]						= "Introduzca el stick USB si no lo ha hecho";
		etiquetas[IDIOMA_PASO1_EXPLICACION]					= "Este programa le permitir� modificar o preparar un nuevo Clauer, por favor sigue las instrucciones que se muestran en pantalla.";

		etiquetas[IDIOMA_PASO2_NEGRITA]						= "El Clauer no tiene formato criptogr�fico, se va a proceder a formatearlo. Para ello debe pensar una contrase�a suficientemente segura.";
		etiquetas[IDIOMA_PASO2_CUADRO1]						= "Contrase�a del Clauer";
		etiquetas[IDIOMA_PASO2_CONTRAS]						= "Contrase�a:";
		etiquetas[IDIOMA_PASO2_CONF]						= "Confirmaci�n:";
		etiquetas[IDIOMA_PASO2_CUADRO]						= "Contrase�a del Clauer";
		etiquetas[IDIOMA_PASO2_NEGR_FOR]					= "Se va a proceder a formatear la zona criptogr�fica. Para ello debe pensar una contrase�a suficientemente segura.";

		etiquetas[IDIOMA_PASO3_NEGRITA]						= "Se va a preparar el Clauer. Por favor no retire el stick USB mientras dure la operaci�n.";
		etiquetas[IDIOMA_PASO3_BARRA]						= "Progreso de la operaci�n";
		etiquetas[IDIOMA_PASO3_DESCRIP]						= "Descripci�n";

		etiquetas[IDIOMA_PASO4_NEGRITA]						= "El Clauer ha sido particionado correctamente, elija una opci�n.";
		etiquetas[IDIOMA_PASO4_OPC]							= "Opciones";
		etiquetas[IDIOMA_PASO4_DISQ]						= "Importar certificado del disquete de la UJI";
		etiquetas[IDIOMA_PASO4_UBIC]						= "Importar certificado desde otra ubicaci�n";
		etiquetas[IDIOMA_PASO4_IE]							= "Importar certificado instalado en el  Internet Explorer";
		etiquetas[IDIOMA_PASO4_FIN]							= "Finalizar";

		etiquetas[IDIOMA_PASO5_1_NEGRITA1]					= "1. Introduzca el disquete con los certificados que le proporciono el personal de registro de la UJI";
		etiquetas[IDIOMA_PASO5_1_NEGRITA2]					= "2 .Introduzca el PIN del disquete";
		etiquetas[IDIOMA_PASO5_1_CUADRO]					= "PIN del disquete";
		etiquetas[IDIOMA_PASO5_1_PIN]						= "PIN:";

		etiquetas[IDIOMA_PASO5_2_NEGRITA]					= "Introduzca la ubicaci�n del certificado que desea importar";
		etiquetas[IDIOMA_PASO5_2_CUADRO]					= "Ubicaci�n";

		etiquetas[IDIOMA_PASO5_3_NEGRITA]					= "Introduzca la contrase�a del certificado";
		etiquetas[IDIOMA_PASO5_3_CUADRO]					= "Contrase�a del certificado";
		etiquetas[IDIOMA_PASO5_3_CONTR]						= "Contrase�a:";
	
		etiquetas[IDIOMA_PASO5_6_NEGRITA]					= "El Gestor del Clauer ha finalizado correctamente.";
		etiquetas[IDIOMA_PASO5_6_DESCRIP1]					= "El Clauer ha sido formateado y particionado correctamente. Si tambi�n ha elegido importar un certificado podr� usar el Clauer como dispositivo criptogr�fico.";
		etiquetas[IDIOMA_PASO5_6_DESCRIP2]					= "Para modificar el Clauer �nicamente debes volver a emplear este programa.";

		etiquetas[IDIOMA_DIALOG_CONTRAS_TEXTO]				= "Introduzca la contrase�a que protege su Clauer";
		etiquetas[IDIOMA_DIALOG_CONTRAS_CUADRO]				= "Contrase�a";
		etiquetas[IDIOMA_DIALOG_CONTRAS_CONTR]				= "Contrase�a:";
		etiquetas[IDIOMA_DIALOG_CONTRAS_TITULO]				= "Contrase�a del Clauer";

		etiquetas[IDIOMA_DIALOG_WIN98_NEGRITA]				= "Introduzca el disquete con los certificados que se le proporcion� junto con el Clauer.";
		etiquetas[IDIOMA_DIALOG_WIN98_CUADRO1]				= "Introduzca PIN actual";
		etiquetas[IDIOMA_DIALOG_WIN98_CUADRO2]				= "Introduzca nuevo PIN";
		etiquetas[IDIOMA_DIALOG_WIN98_PIN]					= "PIN:";
		etiquetas[IDIOMA_DIALOG_WIN98_NUEVO_PIN]			= "Nuevo PIN:";
		etiquetas[IDIOMA_DIALOG_WIN98_CONF]					= "Confirmaci�n";
		etiquetas[IDIOMA_DIALOG_WIN98_BOTON_MODIFICAR]		= "Modificar";
		etiquetas[IDIOMA_DIALOG_WIN98_TITULO]				= "Gestor del Clauer:: Modificar PIN del disquete";

		etiquetas[IDIOMA_DIALOG_SEGURO_TEXTO]				= "Se va a proceder a formatear la zona criptogr�fica y todos los certificados que contenga el clauer ser�n eliminados.";
		etiquetas[IDIOMA_DIALOG_SEGURO_PREGUNTA]			= "�Esta seguro de formatear la zona criptogr�fica?";
		etiquetas[IDIOMA_DIALOG_SEGURO_SI]					= "S�";
		etiquetas[IDIOMA_DIALOG_SEGURO_NO]					= "No";

		etiquetas[IDIOMA_PASO_50_NEGRITA]					= "El Gestor del Clauer ha finalizado correctamente.";
		etiquetas[IDIOMA_PASO_50_DESCRIP1]					= "Para modificar el Clauer �nicamente debes volver a emplear este programa.";
		
		etiquetas[IDIOMA_PASO_20_NEGRITA]					= "Se ha detectado un Clauer que contiene una zona criptogr�fica, elija una opci�n.";
		etiquetas[IDIOMA_PASO_20_MODIFICAR_PASSWORD]		= "Modificar contrase�a";
		etiquetas[IDIOMA_PASO_20_GESTIONAR_CERTIFICADOS]	= "Gestionar certificados";
		etiquetas[IDIOMA_PASO_20_ELIMINAR_CRIPTO]			= "Formatear zona criptogr�fica";
		etiquetas[IDIOMA_PASO_20_MODIFICAR_ZONA]			= "Modificar tama�o de la zona criptogr�fica";
		etiquetas[IDIOMA_PASO_20_FIN]						= "Finalizar";

		etiquetas[IDIOMA_PASO_21_NEGRITA]					= "Modificar contrase�a";
		etiquetas[IDIOMA_PASO_21_CUADRO1]					= "Introduzca la contrase�a actual";
		etiquetas[IDIOMA_PASO_21_CUADRO2]					= "Introduzca la nueva contrase�a";
		etiquetas[IDIOMA_PASO_21_CONTRA]					= "Contrase�a:";
		etiquetas[IDIOMA_PASO_21_NUEVA]						= "Nueva contrase�a:";
		etiquetas[IDIOMA_PASO_21_CONF]						= "Confirmaci�n:";

		etiquetas[IDIOMA_PASO_22_NEGRITA]					= "Elija una opci�n";
		etiquetas[IDIOMA_PASO_22_CUADRO]					= "Gestionar certificados";
		etiquetas[IDIOMA_PASO_22_IMP]						= "Importar certificados al clauer";	
		etiquetas[IDIOMA_PASO_22_EXP]						= "Exportar certificados desde el clauer";
		etiquetas[IDIOMA_PASO_22_ELIM]						= "Eliminar certificados del clauer";
		etiquetas[IDIOMA_PASO_22_DISQ]						= "Modificar PIN del disquete de la UJI";
		
		etiquetas[IDIOMA_PASO_23_NEGRITA]					= "Elija una opci�n";
		etiquetas[IDIOMA_PASO_23_CUADRO]					= "Opciones";
		etiquetas[IDIOMA_PASO_23_DISQ]						= "Importar certificado del disquete de la UJI";
		etiquetas[IDIOMA_PASO_23_UB]						= "Importar certificado desde otra ubicaci�n";
		etiquetas[IDIOMA_PASO_23_IE]						= "Importar certificado instalado en el  Internet Explorer";

		etiquetas[IDIOMA_PASO_28_TEXT1]						= "Formatear zona criptogr�fica";
		etiquetas[IDIOMA_PASO_28_TEXT2]						= "Si elimina la zona criptogr�fica no podr� utilizar el clauer para el almacenamiento, transporte y uso de certificados digitales y sus llaves privadas asociadas.";
		etiquetas[IDIOMA_PASO_28_TEXT3]						= "Solo podr� utilizar el clauer como disco duro de bolsillo.";

		etiquetas[IDIOMA_FICHERO_ABIERTO]					= "La aplicaci�n ya est� abierta";

		etiquetas[IDIOMA_CREAR_FICHERO]						= "Error al crear el fichero de bloqueo";

		etiquetas[IDIOMA_NO_ENCONTRAR_STICK]				= "No se pudo determinar la presencia de sticks en el sistema";
		
		etiquetas[IDIOMA_NO_HAY_STICK]						= "No hay ning�n Stick USB insertado en el sistema\nPor favor, pulse el bot�n Aceptar e inserte el dispositivo\nque desee inicializar";

		etiquetas[IDIOMA_MUCHOS_STICKS]						= "Hay m�s de un Stick USB insertado en el sistema\nPor favor, pulse el bot�n Aceptar y deje insertado\n�nicamente el dispostivo que desee inicializar";
	
		etiquetas[IDIOMA_NO_INICIALIZAR]					= "Imposible realizar la inicializaci�n del dispositivo.\n";
		
		etiquetas[IDIOMA_COMPROBANDO_ADMINISTRADOR]			= "Imposible comprobar si eres administrador.\n";
		
		etiquetas[IDIOMA_NO_ADMINISTRADOR]					= "Debes ser administrador para poder preparar un nuevo Clauer";
		
		etiquetas[IDIOMA_IMPOSIBLE_CERRAR_DISP]				= "Imposible cerrar dispositivo.\n";

		etiquetas[IDIOMA_PIN_COINCIDENCIA]					= "El PIN y su confirmaci�n deben coincidir";
		
		etiquetas[IDIOMA_LONGITUD_PIN]						= "Debe introducir un PIN de un m�nimo de 8 caracteres";

		etiquetas[IDIOMA_NO_BASE]							= "Necesitas tener instalado el software base";
		
		etiquetas[IDIOMA_NO_IMPLEMENTADO]					= "Opci�n no implementada.";
		
		etiquetas[IDIOMA_TITULO_PIN_INCORRECTO]				= "Gestor Clauer :: ERROR :: PIN Incorrecto";
		
		etiquetas[IDIOMA_TITULO_NO_CERTIFICADOS]			= "Gestor Clauer :: ERROR :: No se encontraron certificados";
		
		etiquetas[IDIOMA_NO_IMPORTAR_CERTIFICADO]			= "No se pudo importar el certificado";
		
		etiquetas[IDIOMA_SELECCIONAR_CERTIFICADO]			= "Debe seleccionar un certificado";

		etiquetas[IDIOMA_CONTRASENYA_MAL]					= "Contrase�a del Clauer incorrecta\n";

		etiquetas[IDIOMA_TITULO_CONTRASENYA_MAL]			= "Clauer :: ERROR :: Contrase�a Clauer";

		etiquetas[IDIOMA_NO_MODIFICAR_CONTRASENYA]			= "No ha sido posible modificar la contrase�a del Clauer\n";

		etiquetas[IDIOMA_NO_FINALIZADO]						= "Error finalizando dispositivo\n";

		etiquetas[IDIOMA_CONTRASENYA_MODIFICADA]			= "La contrase�a se ha modificado correctamente.\n";

		etiquetas[IDIOMA_CONTRASENYA_NO_CORRECTA]			= "Contrase�a del Clauer incorrecta\n";

		etiquetas[IDIOMA_TITULO_ERROR_BUSCANDO]				= "Clauer :: ERROR :: Buscando sticks";

		etiquetas[IDIOMA_NO_ENCONTRADOS_CERTIFICADOS]		= "Gestor Clauer :: ERROR :: No se encontraron certificados";

		etiquetas[IDIOMA_CERTIFICADO_OK]					= "El certificado se ha importado correctamente.\n";

		etiquetas[IDIOMA_TITULOS_CERTIFICADOS]				= "Gestor Clauer :: Certificados";

		etiquetas[IDIOMA_NO_MODIFICAR_MBR]					="Imposible cambiar particiones del mbr.\n";

		etiquetas[IDIOMA_RETIRAR_STICK]						= "Por favor, retire el stick USB y pulse el bot�n Aceptar\n";

		etiquetas[IDIOMA_TITULO_RETIRAR]					= "Gestor Clauer :: Extraiga el stick";

		etiquetas[IDIOMA_INSERTAR_STICK]					= "Por favor, inserte de nuevo el stick y pulse el bot�n Aceptar\n";

		etiquetas[IDIOMA_TITULO_INSERTAR]					= "Gestor Clauer :: Inserte el stick";

		etiquetas[IDIOMA_TITULO_ELIMINAR_ZONA_CRIPTO]		= "Gestor Clauer :: Formatear zona criptogr�fica";

		etiquetas[IDIOMA_NO_INSERTADO]						= "No ha insertado todav�a su Stick USB. Por favor,\n ins�rtelo de nuevo y pulse el bot�n Aceptar";

		etiquetas[IDIOMA_MUCHOS_STICKS_INSERT]				= "El programa ha detectado m�s de un Stick USB insertado en el sistema.\n Por favor, deje insertado �nicamente el Stick con el que empez� el proceso\n de inicializaci�n.";

		etiquetas[IDIOMA_IMPOSIBLE_FORMATEAR_LOGICA]		= "Imposible formatear unidad l�gica del dispositivo.\n";

		etiquetas[IDIOMA_ERROR_FORMATEANDO]					= "Clauer :: ERROR :: Formateando";

		etiquetas[IDIOMA_ELIMINADO_CRIPTO]					= "Se ha eliminado la zona criptogr�fica correctamente.\n";

		etiquetas[IDIOMA_TITULO_CRIPTO]						= "Gestor Clauer :: Zona criptogr�fica";

		etiquetas[IDIOMA_PROCESO_FINALIZADO]				= "El proceso termin� con �xito\nPulse Aceptar";

		etiquetas[IDIOMA_TITULO_FINALIZADO]					= "Clauer :: Finalizado";

		etiquetas[IDIOMA_NO_CERT_DISQUET]					= "No se pudo leer certificado en la unidad A:\n";

		etiquetas[IDIOMA_PIN_INCORRECTO]					= "PIN incorrecto. Vuelva a introducir el PIN.\nComprueba que respet� may�sculas y min�sculas (si las hubiere)\nPulse el bot�n Aceptar";

		etiquetas[IDIOMA_ERROR_VERIFICANDO_PASS]			= "Ocurri� un error verificando su password. Int�ntelo de nuevo. \n";

		etiquetas[IDIOMA_ERROR_ENTRADA_SALIDA]				= "Ocurri� un error de E/S.\n";

		etiquetas[IDIOMA_NO_IMPORTAR_PKCS12]				= "No se pudieron importar los PKCS12.";

		etiquetas[IDIOMA_PASS_NO_SEGURA]					= "La nueva contrase�a no es lo suficientemente segura, intentelo con otra.\nHay demasiados n�meros repetidos.";

		etiquetas[IDIOMA_PASS_LETRAS]						= "La nueva contrase�a no es lo suficientemente segura, intentelo con otra.\nHay demasiadas letras repetidas.";

		etiquetas[IDIOMA_MIN_LETRAS]						= "La nueva contrase�a no es lo suficientemente segura, intentelo con otra.\nDebe emplear como m�nimo 3 letras.";
		
		etiquetas[IDIOMA_MIN_NUM]							= "La nueva contrase�a no es lo suficientemente segura, intentelo con otra.\nDebe emplear como m�nimo 3 n�meros.";

		etiquetas[IDIOMA_INICIALIZANDO]						= "Inicializando dispositivo";

		etiquetas[IDIOMA_FORMATEANDO]						= "Formateando dispositivo...";

		etiquetas[IDIOMA_ERROR_GRAVE]						= "Ocurri� un error grave en la aplicaci�n.\n";

		etiquetas[IDIOMA_NO_DETERMINAR_IDIOMA]				= "No se ha podido determinar el idioma.";

		etiquetas[IDIOMA_NO_NT_VIEJO]						= "El Gestor no soporta Windows NT 3.51";

		etiquetas[IDIOMA_ADVERTENCIA_CAMBIO_PIN]			= "Esta utilizando Windows 95/98/Me/NT 4.0\nEl Gestor solo le permitira modificar el PIN del disquete de la UJI";

		etiquetas[IDIOMA_ABRIR_CERTIFICADO]					= "No se pudo abrir el certificado.";
		
		etiquetas[IDIOMA_ESCRIBIR_CERTIFICADOS]				= "No se pudo escribir el certificado.";
		
		etiquetas[IDIOMA_CERRAR_CERTIFICADOS]				= "No se pudo cerrar el certificado.";

		etiquetas[IDIOMA_NO_MODIFICAR_PIN]					= "No se pudo modificar el PIN del disquete.\n";

		etiquetas[IDIOMA_PIN_MODIFICADO]					= "PIN del disquete modificado correctamente.";

		etiquetas[IDIOMA_NO_LEER_CERTIFICADO]				= "No se pudo leer certificado\n";


	}
	return TRUE;

}


BOOL IDIOMA_Descargar (void)
{
	if ( idioma ) {
		delete [] idioma;
		idioma = NULL;
	}

	return TRUE;
}




LPSTR IDIOMA_Get (DWORD etiqueta)
{
	if ( !idioma )
		return NULL;

	if ( etiqueta > (NUM_ETIQUETAS - 1) )
		return NULL;

	return etiquetas[etiqueta];
}



BOOL IDIOMA_Cargado (void)
{

	return idioma != NULL;


}

