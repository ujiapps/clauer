// DlgPaso28.cpp : implementation file
//

#include "stdafx.h"
#include "Admin_CLAUER.h"
#include "DlgPaso28.h"
#include "Admin_CLAUERDlg.h"
#include "IDIOMA.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// DlgPaso28 dialog


DlgPaso28::DlgPaso28(CWnd* pParent /*=NULL*/)
	: CDialog(DlgPaso28::IDD, pParent)
{
	//{{AFX_DATA_INIT(DlgPaso28)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}


void DlgPaso28::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(DlgPaso28)
	DDX_Control(pDX, IDC_TEXTO3, m_texto3);
	DDX_Control(pDX, IDC_TEXTO2, m_texto2);
	DDX_Control(pDX, IDC_TEXT, m_texto);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(DlgPaso28, CDialog)
	//{{AFX_MSG_MAP(DlgPaso28)
	ON_WM_SHOWWINDOW()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// DlgPaso28 message handlers

void DlgPaso28::OnShowWindow(BOOL bShow, UINT nStatus) 
{
	CDialog::OnShowWindow(bShow, nStatus);
	
	// TODO: Add your message handler code here
	CFont *font = new CFont;
	font->CreatePointFont(12, "System");
	m_texto.SetFont(font);
	delete font;
	
}

BOOL DlgPaso28::PreTranslateMessage(MSG* pMsg) 
{
	// TODO: Add your specialized code here and/or call the base class
	if(pMsg->message==WM_KEYDOWN)
	{
		if ( (pMsg->wParam==VK_ESCAPE) )
			pMsg->wParam=NULL ;
		else if ( (pMsg->wParam==VK_RETURN) ) {
			((CAdmin_CLAUERDlg *) this->GetParent())->OnBotonSiguiente();
			return TRUE;
		}
	}
	return CDialog::PreTranslateMessage(pMsg);
}
