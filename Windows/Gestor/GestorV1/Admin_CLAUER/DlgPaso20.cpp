// DlgPaso20.cpp : implementation file
//

#include "stdafx.h"
#include "Admin_CLAUER.h"
#include "DlgPaso20.h"
#include "Admin_CLAUERDlg.h"
#include "IDIOMA.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// DlgPaso20 dialog


DlgPaso20::DlgPaso20(CWnd* pParent /*=NULL*/)
	: CDialog(DlgPaso20::IDD, pParent)
{
	//{{AFX_DATA_INIT(DlgPaso20)
	m_radio = -1;
	//}}AFX_DATA_INIT
}


void DlgPaso20::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(DlgPaso20)
	DDX_Control(pDX, IDC_ZONA, m_zona);
	DDX_Control(pDX, IDC_MOD, m_mod);
	DDX_Control(pDX, IDC_GES, m_ges);
	DDX_Control(pDX, IDC_FN, m_fn);
	DDX_Control(pDX, IDC_ELIM, m_elim);
	DDX_Control(pDX, IDC_DESC, m_texto);
	DDX_Radio(pDX, IDC_RADIO1, m_radio);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(DlgPaso20, CDialog)
	//{{AFX_MSG_MAP(DlgPaso20)
	ON_WM_SHOWWINDOW()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// DlgPaso20 message handlers

BOOL DlgPaso20::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here
	m_radio = 0;
	UpdateData(FALSE);
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void DlgPaso20::OnShowWindow(BOOL bShow, UINT nStatus) 
{
	CDialog::OnShowWindow(bShow, nStatus);
	
	// TODO: Add your message handler code here
	CFont *font = new CFont;
	font->CreatePointFont(12, "System");
	m_texto.SetFont(font);
	delete font;

	m_texto.SetWindowText(IDIOMA_Get(IDIOMA_PASO_20_NEGRITA));
	m_mod.SetWindowText(IDIOMA_Get(IDIOMA_PASO_20_MODIFICAR_PASSWORD));
	m_ges.SetWindowText(IDIOMA_Get(IDIOMA_PASO_20_GESTIONAR_CERTIFICADOS));
	m_elim.SetWindowText(IDIOMA_Get(IDIOMA_PASO_20_ELIMINAR_CRIPTO));
	m_zona.SetWindowText(IDIOMA_Get(IDIOMA_PASO_20_MODIFICAR_ZONA));
	m_fn.SetWindowText(IDIOMA_Get(IDIOMA_PASO_20_FIN));
}

BOOL DlgPaso20::PreTranslateMessage(MSG* pMsg) 
{
	// TODO: Add your specialized code here and/or call the base class
		// TODO: Add your specialized code here and/or call the base class
	if(pMsg->message==WM_KEYDOWN)
	{
		if ( (pMsg->wParam==VK_ESCAPE) )
			pMsg->wParam=NULL ;
		else if ( (pMsg->wParam==VK_RETURN) ) {
			((CAdmin_CLAUERDlg *) this->GetParent())->OnBotonSiguiente();
			return TRUE;
		}
	}
	return CDialog::PreTranslateMessage(pMsg);
}
