// Admin_CLAUERDlg.h : header file
//

#if !defined(AFX_ADMIN_CLAUERDLG_H__1A4BDCA4_00F0_406D_8630_EE220F459A02__INCLUDED_)
#define AFX_ADMIN_CLAUERDLG_H__1A4BDCA4_00F0_406D_8630_EE220F459A02__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "DlgPaso1.h"
#include "DlgPaso2.h"
#include "DlgPaso3.h"
#include "DlgPaso4.h"
#include "DlgPaso5_1.h"
#include "DlgPaso5_2.h"
#include "DlgPaso5_3.h"
#include "DlgPaso5_6.h"
#include "DlgPaso20.h"
#include "DlgPaso21.h"
#include "DlgPaso22.h"
#include "DlgPaso23.h"
#include "DlgPaso28.h"

#include "DlgContras.h"
#include "DlgSeguro.h"


#include "DlgPaso50.h"

#include "DlgWin98.h"

#include "thread.h"


/////////////////////////////////////////////////////////////////////////////
// CAdmin_CLAUERDlg dialog

class CAdmin_CLAUERDlg : public CDialog
{
// Construction
public:
	CAdmin_CLAUERDlg(CWnd* pParent = NULL);	// standard constructor

	friend class DlgPaso1;
	friend class DlgPaso2;
	friend class DlgPaso3;
	friend class DlgPaso4;
	friend class DlgPaso5_1;
	friend class DlgPaso5_2;
	friend class DlgPaso5_3;
	friend class DlgPaso5_6;
	friend class DlgPaso20;
	friend class DlgPaso21;
	friend class DlgPaso22;
	friend class DlgPaso23;
	friend class DlgPaso28;
	friend class DlgSeguro;
	friend class DlgContras;


	friend class DlgPaso50;
// Dialog Data
	//{{AFX_DATA(CAdmin_CLAUERDlg)
	enum { IDD = IDD_ADMIN_CLAUER_DIALOG };
	CStatic	m_pnlIzquierdo;
	CButton	m_botonCancelar;
	CButton	m_botonSiguiente;
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CAdmin_CLAUERDlg)
	public:
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	void VerificarPKCS12 (CString pwd);
	int ImpDisqPKCS12 (CString pwd,int numDispositivos,CString pin);
	void Verificar (CString pwd, CString ruta);
	int Importar (CString pwd,int numDispositivos,CString pin,CString ruta);
	void SetPaso (int paso);
	int SeguridadPasswd (CString pwd);
	thread * thrPaso3;
	int m_paso; // Indica en qu� paso estamos actualmente
	int numDispositivos;
	CString pin;
	CString ruta;
	
//	int version;		/*3 Windows NT 3.51 */
						/*4 Windows 95/98/Me/NT 4.0 */
						/*5 Windows XP/2000/Server 2003 family */
	
	//FILE *temporal;
	CString fich_temp;
	HANDLE hFile;

	DlgContras m_dlgContras;
	DlgSeguro m_dlgSeguro;

	DlgPaso1 m_dlgPaso1;
	DlgPaso2 m_dlgPaso2;
	DlgPaso3 m_dlgPaso3;
	DlgPaso4 m_dlgPaso4;
	DlgPaso5_1 m_dlgPaso5_1;
	DlgPaso5_2 m_dlgPaso5_2;
	DlgPaso5_3 m_dlgPaso5_3;
	DlgPaso5_6 m_dlgPaso5_6;
	DlgPaso20 m_dlgPaso20;
	DlgPaso21 m_dlgPaso21;
	DlgPaso22 m_dlgPaso22;
	DlgPaso23 m_dlgPaso23;
	DlgPaso28 m_dlgPaso28;


	DlgPaso50 m_dlgPaso50;

//	DlgWin98 m_dlgWin98;

	HICON m_hIcon;
	// Generated message map functions
	//{{AFX_MSG(CAdmin_CLAUERDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	virtual void OnCancel();
	virtual void OnOK();
	afx_msg void OnBotonCancel();
	afx_msg void OnBotonSiguiente();
	afx_msg void OnTimer(UINT nIDEvent);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_ADMIN_CLAUERDLG_H__1A4BDCA4_00F0_406D_8630_EE220F459A02__INCLUDED_)
