#if !defined(AFX_DLGPASO20_H__8B6ECED4_A0BE_4027_977F_C4A166DA523A__INCLUDED_)
#define AFX_DLGPASO20_H__8B6ECED4_A0BE_4027_977F_C4A166DA523A__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// DlgPaso20.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// DlgPaso20 dialog

class DlgPaso20 : public CDialog
{
// Construction
public:
	DlgPaso20(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(DlgPaso20)
	enum { IDD = IDD_DIALOG_PASO_20 };
	CStatic	m_zona;
	CStatic	m_mod;
	CStatic	m_ges;
	CStatic	m_fn;
	CStatic	m_elim;
	CStatic	m_texto;
	int		m_radio;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(DlgPaso20)
	public:
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(DlgPaso20)
	virtual BOOL OnInitDialog();
	afx_msg void OnShowWindow(BOOL bShow, UINT nStatus);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DLGPASO20_H__8B6ECED4_A0BE_4027_977F_C4A166DA523A__INCLUDED_)
