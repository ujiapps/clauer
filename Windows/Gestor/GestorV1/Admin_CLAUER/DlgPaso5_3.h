#if !defined(AFX_DLGPASO5_3_H__22127801_821D_41F2_9837_F3FA1DED9059__INCLUDED_)
#define AFX_DLGPASO5_3_H__22127801_821D_41F2_9837_F3FA1DED9059__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// DlgPaso5_3.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// DlgPaso5_3 dialog

class DlgPaso5_3 : public CDialog
{
// Construction
public:
	CString GetPIN(void);
	DlgPaso5_3(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(DlgPaso5_3)
	enum { IDD = IDD_DIALOG_PASO_5_3 };
	CButton	m_cuadro;
	CStatic	m_contr;
	CStatic	m_texto;
	CString	m_pin;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(DlgPaso5_3)
	public:
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(DlgPaso5_3)
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DLGPASO5_3_H__22127801_821D_41F2_9837_F3FA1DED9059__INCLUDED_)
