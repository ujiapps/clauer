// DlgPaso22.cpp : implementation file
//

#include "stdafx.h"
#include "Admin_CLAUER.h"
#include "DlgPaso22.h"
#include "Admin_CLAUERDlg.h"
#include "IDIOMA.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// DlgPaso22 dialog


DlgPaso22::DlgPaso22(CWnd* pParent /*=NULL*/)
	: CDialog(DlgPaso22::IDD, pParent)
{
	//{{AFX_DATA_INIT(DlgPaso22)
	m_radio = -1;
	//}}AFX_DATA_INIT
}


void DlgPaso22::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(DlgPaso22)
	DDX_Control(pDX, IDC_CUADRO, m_cuadro);
	DDX_Control(pDX, IDC_IMP, m_imp);
	DDX_Control(pDX, IDC_EXP, m_exp);
	DDX_Control(pDX, IDC_ELIM, m_elim);
	DDX_Control(pDX, IDC_DISQ, m_disq);
	DDX_Control(pDX, IDC_TEXTO, m_texto);
	DDX_Radio(pDX, IDC_RADIO1, m_radio);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(DlgPaso22, CDialog)
	//{{AFX_MSG_MAP(DlgPaso22)
	ON_WM_SHOWWINDOW()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// DlgPaso22 message handlers

BOOL DlgPaso22::OnInitDialog() 
{
	CDialog::OnInitDialog();
	m_radio = 0;
	UpdateData(FALSE);
	// TODO: Add extra initialization here
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

void DlgPaso22::OnShowWindow(BOOL bShow, UINT nStatus) 
{
	CDialog::OnShowWindow(bShow, nStatus);
	
	// TODO: Add your message handler code here
	CFont *font = new CFont;
	font->CreatePointFont(12, "System");
	m_texto.SetFont(font);
	delete font;

	m_texto.SetWindowText(IDIOMA_Get(IDIOMA_PASO_22_NEGRITA));
	m_cuadro.SetWindowText(IDIOMA_Get(IDIOMA_PASO_22_CUADRO));
	m_imp.SetWindowText(IDIOMA_Get(IDIOMA_PASO_22_IMP));
	m_exp.SetWindowText(IDIOMA_Get(IDIOMA_PASO_22_EXP));
	m_elim.SetWindowText(IDIOMA_Get(IDIOMA_PASO_22_ELIM));
	m_disq.SetWindowText(IDIOMA_Get(IDIOMA_PASO_22_DISQ));
	
}

BOOL DlgPaso22::PreTranslateMessage(MSG* pMsg) 
{
	// TODO: Add your specialized code here and/or call the base class
	if(pMsg->message==WM_KEYDOWN)
	{
		if ( (pMsg->wParam==VK_ESCAPE) )
			pMsg->wParam=NULL ;
		else if ( (pMsg->wParam==VK_RETURN) ) {
			((CAdmin_CLAUERDlg *) this->GetParent())->OnBotonSiguiente();
			return TRUE;
		}
	}
	return CDialog::PreTranslateMessage(pMsg);
}
