// thread.cpp : implementation file
//

#include "stdafx.h"
#include "Admin_CLAUER.h"
#include "thread.h"
#include "IDIOMA.h"
#include "Admin_CLAUERDlg.h"

#include <USBLowLevel/usb_lowlevel.h>
#include <libFormat/usb_format.h>
#include <LIBIMPORT/LIBIMPORT.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// thread

IMPLEMENT_DYNCREATE(thread, CWinThread)

thread::thread()
{
	this->m_bAutoDelete = FALSE;
}

thread::~thread()
{
}

BOOL thread::InitInstance()
{
	// TODO:  perform and per-thread initialization here
	m_bFinished=FALSE;
	m_bError=FALSE;
	return TRUE;
}

int thread::ExitInstance()
{
	// TODO:  perform any per-thread cleanup here
	return CWinThread::ExitInstance();
}

BEGIN_MESSAGE_MAP(thread, CWinThread)
	//{{AFX_MSG_MAP(thread)
		// NOTE - the ClassWizard will add and remove mapping macros here.
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// thread message handlers

void thread::SetDlg(DlgPaso3 *dlgPaso3)
{
	this->m_dlgPaso3 = dlgPaso3;
}

int thread::Run()
{
	int result, retval = 0;
	int numDispositivos;
	char ** unidades = NULL;
	int *dispositivos = NULL;
	HANDLE_DISPOSITIVO handle;
	CString msg;
	//int *soy = NULL;

	try {

		m_bError = FALSE;

		m_dlgPaso3->SetPaso(100/5);

		m_dlgPaso3->SetDescripcion(IDIOMA_Get(IDIOMA_INICIALIZANDO));

		/* Formateamos
		 */

		while ( 1 ) {

			m_dlgPaso3->Paso();

			result = lowlevel_enumerar_dispositivos( NULL, NULL, &numDispositivos );

			if ( result != ERR_LOWLEVEL_NO ) {
				m_bError = TRUE;
				m_strErrMsg = IDIOMA_Get(IDIOMA_NO_INICIALIZAR);

				retval = 1;
				goto finRun;
			}

			if ( numDispositivos > 1 ) {
				MessageBox(AfxGetApp()->GetMainWnd()->m_hWnd, 
					IDIOMA_Get(IDIOMA_MUCHOS_STICKS_INSERT),
					"Gestor Clauer",
					MB_ICONEXCLAMATION);
			}
			else if ( numDispositivos == 0 ) {
				MessageBox(AfxGetApp()->GetMainWnd()->m_hWnd, 
					IDIOMA_Get(IDIOMA_NO_INSERTADO),
					"Gestor Clauer",
					MB_ICONEXCLAMATION);
			} else if ( numDispositivos == 1 )
				break;

		}

		unidades = new char * [numDispositivos];

		for ( int j=0; j<numDispositivos; j++ ) 
			unidades[j] = new char[5];
			
		dispositivos = new int [numDispositivos];

		result = lowlevel_enumerar_dispositivos( unidades, dispositivos, &numDispositivos );

		if ( result != ERR_LOWLEVEL_NO ) {
			m_bError = TRUE;
			m_strErrMsg = IDIOMA_Get(IDIOMA_NO_INICIALIZAR);

			retval = 1;
			goto finRun;
		}
		
		m_dlgPaso3->Paso();

		int soy;
		result = lowlevel_soy_admin( dispositivos[0], &soy );
		if ( result != ERR_LOWLEVEL_NO ) {
			result = GetLastError();
			m_bError = TRUE;
			m_strErrMsg = IDIOMA_Get(IDIOMA_COMPROBANDO_ADMINISTRADOR);

			retval = 1;
			goto finRun;
		}
		if (!soy)
		{
			m_bError = TRUE;
			m_strErrMsg = IDIOMA_Get(IDIOMA_NO_ADMINISTRADOR);
			retval = 1;
			goto finRun;
		}
			
	/*	result = lowlevel_abrir_dispositivo( dispositivos[0], &handle );
		if ( result != ERR_LOWLEVEL_NO ) {
			m_bError = TRUE;
			m_strErrMsg = IDIOMA_Get(IDIOMA_NO_INICIALIZAR);

			retval = 1;
			goto finRun;
		}

*/
		m_dlgPaso3->Paso();	


	//	long C,H,S,C1,H1,S1,C2,H2,S2;

//		PARTICION particiones[4], nuevas_particiones[4];

/*		if ( format_obtener_geometria_optima(dispositivos[0], &H1, &S1, &C1) != ERR_FORMAT_NO ) {//handle por unidades[0]
			m_bError = TRUE;
			m_strErrMsg = IDIOMA_Get(IDIOMA_NO_INICIALIZAR);

			retval = 1;
			goto finRun;
		}
		
		int tiene;

		if( lowlevel_tiene_mbr(handle,&tiene) !=ERR_LOWLEVEL_NO) {
			m_bError = TRUE;
			m_strErrMsg = IDIOMA_Get(IDIOMA_NO_INICIALIZAR);

			retval = 1;
			goto finRun;	
		}

		if (tiene) {
			if ( format_obtener_geometria(handle, &H2, &S2, &C2) != ERR_FORMAT_NO ) {
				m_bError = TRUE;
				m_strErrMsg = IDIOMA_Get(IDIOMA_NO_INICIALIZAR);

				retval = 1;
				goto finRun;
			}

			if( (H1*S1*C1) > (H2*S2*C2) ) {
				H=H1;
				S=S1;
				C=C1;
			}else{
				H=H2;
				S=S2;
				C=C2;
			}
		}else{
			H=H1;
			S=S1;
			C=C1;
		}

		if ( format_crear_mbr(handle, H,C,S) != ERR_FORMAT_NO ) {
			m_bError = TRUE;
			m_strErrMsg = IDIOMA_Get(IDIOMA_NO_INICIALIZAR);

			retval = 1;
			goto finRun;
		}


		if ( lowlevel_leer_particiones(handle, particiones) != ERR_FORMAT_NO ) {
			m_bError = TRUE;
			m_strErrMsg = IDIOMA_Get(IDIOMA_NO_INICIALIZAR);

			retval = 1;
			goto finRun;
		}

		int cap=((H*S*C*512)/1000)/1024;
		float total=100-((100*5.0)/cap);
		if (cap<=128){
		
			if ( format_cambiar_particiones_mbr(handle, 96, particiones, nuevas_particiones) != ERR_FORMAT_NO ) {
				m_bError = TRUE;
				m_strErrMsg = IDIOMA_Get(IDIOMA_NO_INICIALIZAR);

				retval = 1;
				goto finRun;
			}

		}else{
			if ( format_cambiar_particiones_mbr(handle, total, particiones, nuevas_particiones) != ERR_FORMAT_NO ) {
				m_bError = TRUE;
				m_strErrMsg = IDIOMA_Get(IDIOMA_NO_INICIALIZAR);

				retval = 1;
				goto finRun;
			}

		}

		result = format_releer_mbr( handle );

		if ( result == ERR_FORMAT_FUNCION_NO_DISPONIBLE ) {

			MessageBox(AfxGetApp()->GetMainWnd()->m_hWnd, IDIOMA_Get(IDIOMA_RETIRAR_STICK),
					IDIOMA_Get(IDIOMA_TITULO_RETIRAR), MB_ICONEXCLAMATION);

			msg = IDIOMA_Get(IDIOMA_INSERTAR_STICK);

			while (1) {

				MessageBox(AfxGetApp()->GetMainWnd()->m_hWnd, msg.GetBuffer(0),
					"Gestor Clauer", MB_ICONEXCLAMATION);

				result = lowlevel_enumerar_dispositivos( NULL, NULL, &numDispositivos );

				if ( result != 0 ) {
					m_bError = TRUE;
					m_strErrMsg = IDIOMA_Get(IDIOMA_NO_INICIALIZAR);

					retval = 1;
					goto finRun;
				}

				if ( numDispositivos == 0 ) {
					msg = IDIOMA_Get(IDIOMA_NO_INSERTADO);
				} else if ( numDispositivos > 1 ) {
					msg = IDIOMA_Get(IDIOMA_MUCHOS_STICKS_INSERT);
				} else if ( numDispositivos == 1 )
					break;
			}
			result = lowlevel_cerrar_dispositivo( handle );
			if ( result != ERR_LOWLEVEL_NO ) {
				m_bError = TRUE;
				m_strErrMsg = IDIOMA_Get(IDIOMA_NO_INICIALIZAR);

				retval = 1;
				goto finRun;
			}

			result = lowlevel_abrir_dispositivo( dispositivos[0], &handle );
			if ( result != ERR_LOWLEVEL_NO ) {
				m_bError = TRUE;
				m_strErrMsg = IDIOMA_Get(IDIOMA_NO_INICIALIZAR);

				retval = 1;
				goto finRun;
			}

		}
*/
		m_dlgPaso3->SetDescripcion(IDIOMA_Get(IDIOMA_FORMATEANDO));
/*
		result = format_formatear_unidad_logica_win( unidades[0] );
		if ( result != ERR_FORMAT_NO ) {
			m_bError = TRUE;
			m_strErrMsg = IDIOMA_Get(IDIOMA_NO_INICIALIZAR);

			retval = 1;
			goto finRun;
		}
*/
			
/*

		result = format_crear_zona_cripto( handle, nuevas_particiones[3], m_strPINClauer.GetBuffer(0),NULL );
		if ( result != ERR_FORMAT_NO ) {
			m_bError = TRUE;
			m_strErrMsg = IDIOMA_Get(IDIOMA_NO_INICIALIZAR);

			retval = 1;
			goto finRun;
		}
*/
		

/*		result = lowlevel_cerrar_dispositivo( handle );
		if ( result != ERR_LOWLEVEL_NO ) {
			m_bError = TRUE;
			m_strErrMsg = IDIOMA_Get(IDIOMA_IMPOSIBLE_CERRAR_DISP);

			retval = 1;
			goto finRun;
		}
*/
		m_dlgPaso3->Paso();	

		result=format_crear_clauer(dispositivos[0],unidades[0],m_strPINClauer.GetBuffer(0),0);

		if ( result != ERR_FORMAT_NO ) {
			m_bError = TRUE;
			m_strErrMsg = IDIOMA_Get(IDIOMA_NO_INICIALIZAR);

			retval = 1;
			goto finRun;
		}
		m_dlgPaso3->Paso();	
	}

	catch (...) {
		// Error desconocido

		m_bError = TRUE;
		m_strErrMsg = IDIOMA_Get(IDIOMA_ERROR_GRAVE);

		retval = 1;
		goto finRun;
	}

finRun:

	if ( unidades ) {
		for ( int j = 0 ; j < numDispositivos; j++ ) 
			if (unidades[j])
				delete [] unidades[j];

		delete [] unidades;
	}

	if (dispositivos)
		delete [] dispositivos;

	m_bFinished = TRUE;

	return retval;
}

BOOL thread::Terminado()
{
	return m_bFinished;
}


BOOL thread::GetError()
{
	return m_bError;
}

CString thread::GetMsgError()
{
	return m_strErrMsg;
}

void thread::SetPINClauer(CString pin)
{
	m_strPINClauer = pin;
}