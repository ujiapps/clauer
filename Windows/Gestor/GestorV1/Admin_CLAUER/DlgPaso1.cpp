// DlgPaso1.cpp : implementation file
//

#include "stdafx.h"
#include "Admin_CLAUER.h"
#include "Admin_CLAUERDlg.h"
#include "DlgPaso1.h"
#include "IDIOMA.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// DlgPaso1 dialog


DlgPaso1::DlgPaso1(CWnd* pParent /*=NULL*/)
	: CDialog(DlgPaso1::IDD, pParent)
{
	//{{AFX_DATA_INIT(DlgPaso1)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}


void DlgPaso1::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(DlgPaso1)
	DDX_Control(pDX, IDC_LBL_explicacion, m_texto2);
	DDX_Control(pDX, IDC_LBL_PASO_1, m_texto1);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(DlgPaso1, CDialog)
	//{{AFX_MSG_MAP(DlgPaso1)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// DlgPaso1 message handlers

BOOL DlgPaso1::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here

	CFont *font = new CFont;
	font->CreatePointFont(12, "System");
	m_texto1.SetFont(font);
	m_texto1.SetWindowText(IDIOMA_Get(IDIOMA_PASO1_NEGRITA));
	m_texto2.SetWindowText(IDIOMA_Get(IDIOMA_PASO1_EXPLICACION));
	delete font;

	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

BOOL DlgPaso1::PreTranslateMessage(MSG* pMsg) 
{
	// TODO: Add your specialized code here and/or call the base class
	if(pMsg->message==WM_KEYDOWN)
	{
		if ( (pMsg->wParam==VK_ESCAPE) )
			pMsg->wParam=NULL ;
		else if ( (pMsg->wParam==VK_RETURN) ) {
			((CAdmin_CLAUERDlg *) this->GetParent())->OnBotonSiguiente();
			return TRUE;
		}
	}
	return CDialog::PreTranslateMessage(pMsg);
}
