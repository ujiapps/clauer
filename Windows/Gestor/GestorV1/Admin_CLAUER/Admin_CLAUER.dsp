# Microsoft Developer Studio Project File - Name="Admin_CLAUER" - Package Owner=<4>
# Microsoft Developer Studio Generated Build File, Format Version 6.00
# ** DO NOT EDIT **

# TARGTYPE "Win32 (x86) Application" 0x0101

CFG=Admin_CLAUER - Win32 Debug
!MESSAGE This is not a valid makefile. To build this project using NMAKE,
!MESSAGE use the Export Makefile command and run
!MESSAGE 
!MESSAGE NMAKE /f "Admin_CLAUER.mak".
!MESSAGE 
!MESSAGE You can specify a configuration when running NMAKE
!MESSAGE by defining the macro CFG on the command line. For example:
!MESSAGE 
!MESSAGE NMAKE /f "Admin_CLAUER.mak" CFG="Admin_CLAUER - Win32 Debug"
!MESSAGE 
!MESSAGE Possible choices for configuration are:
!MESSAGE 
!MESSAGE "Admin_CLAUER - Win32 Release" (based on "Win32 (x86) Application")
!MESSAGE "Admin_CLAUER - Win32 Debug" (based on "Win32 (x86) Application")
!MESSAGE 

# Begin Project
# PROP AllowPerConfigDependencies 0
# PROP Scc_ProjName ""
# PROP Scc_LocalPath ""
CPP=cl.exe
MTL=midl.exe
RSC=rc.exe

!IF  "$(CFG)" == "Admin_CLAUER - Win32 Release"

# PROP BASE Use_MFC 5
# PROP BASE Use_Debug_Libraries 0
# PROP BASE Output_Dir "Release"
# PROP BASE Intermediate_Dir "Release"
# PROP BASE Target_Dir ""
# PROP Use_MFC 5
# PROP Use_Debug_Libraries 0
# PROP Output_Dir "Release"
# PROP Intermediate_Dir "Release"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MT /W3 /GX /O2 /D "WIN32" /D "NDEBUG" /D "_WINDOWS" /Yu"stdafx.h" /FD /c
# ADD CPP /nologo /MT /W3 /GX /O2 /I ".." /D "NDEBUG" /D "WIN32" /D "_WINDOWS" /D "_MBCS" /D _WIN32_WINNT=0x500 /Yu"stdafx.h" /FD /c
# ADD BASE MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "NDEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0xc0a /d "NDEBUG"
# ADD RSC /l 0xc0a /d "NDEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 /nologo /subsystem:windows /machine:I386
# ADD LINK32 /nologo /subsystem:windows /machine:I386 /out:"Release/Gestor Clauer.exe"

!ELSEIF  "$(CFG)" == "Admin_CLAUER - Win32 Debug"

# PROP BASE Use_MFC 5
# PROP BASE Use_Debug_Libraries 1
# PROP BASE Output_Dir "Debug"
# PROP BASE Intermediate_Dir "Debug"
# PROP BASE Target_Dir ""
# PROP Use_MFC 5
# PROP Use_Debug_Libraries 1
# PROP Output_Dir "Debug"
# PROP Intermediate_Dir "Debug"
# PROP Ignore_Export_Lib 0
# PROP Target_Dir ""
# ADD BASE CPP /nologo /MTd /W3 /Gm /GX /ZI /Od /D "WIN32" /D "_DEBUG" /D "_WINDOWS" /Yu"stdafx.h" /FD /GZ /c
# ADD CPP /nologo /MTd /W3 /Gm /GX /ZI /Od /I ".." /D "_DEBUG" /D "WIN32" /D "_WINDOWS" /D "_MBCS" /D _WIN32_WINNT=0x500 /Yu"stdafx.h" /FD /GZ /c
# ADD BASE MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD MTL /nologo /D "_DEBUG" /mktyplib203 /win32
# ADD BASE RSC /l 0xc0a /d "_DEBUG"
# ADD RSC /l 0xc0a /d "_DEBUG"
BSC32=bscmake.exe
# ADD BASE BSC32 /nologo
# ADD BSC32 /nologo
LINK32=link.exe
# ADD BASE LINK32 /nologo /subsystem:windows /debug /machine:I386 /pdbtype:sept
# ADD LINK32 /nologo /subsystem:windows /debug /machine:I386 /out:"Debug/Gestor Clauer.exe" /pdbtype:sept

!ENDIF 

# Begin Target

# Name "Admin_CLAUER - Win32 Release"
# Name "Admin_CLAUER - Win32 Debug"
# Begin Group "Source Files"

# PROP Default_Filter "cpp;c;cxx;rc;def;r;odl;idl;hpj;bat"
# Begin Source File

SOURCE=.\Admin_CLAUER.cpp
# End Source File
# Begin Source File

SOURCE=.\Admin_CLAUER.rc
# End Source File
# Begin Source File

SOURCE=.\Admin_CLAUERDlg.cpp
# End Source File
# Begin Source File

SOURCE=.\DlgContras.cpp
# End Source File
# Begin Source File

SOURCE=.\DlgPaso1.cpp
# End Source File
# Begin Source File

SOURCE=.\DlgPaso2.cpp
# End Source File
# Begin Source File

SOURCE=.\DlgPaso20.cpp
# End Source File
# Begin Source File

SOURCE=.\DlgPaso21.cpp
# End Source File
# Begin Source File

SOURCE=.\DlgPaso22.cpp
# End Source File
# Begin Source File

SOURCE=.\DlgPaso23.cpp
# End Source File
# Begin Source File

SOURCE=.\DlgPaso28.cpp
# End Source File
# Begin Source File

SOURCE=.\DlgPaso3.cpp
# End Source File
# Begin Source File

SOURCE=.\DlgPaso4.cpp
# End Source File
# Begin Source File

SOURCE=.\DlgPaso50.cpp
# End Source File
# Begin Source File

SOURCE=.\DlgPaso5_1.cpp
# End Source File
# Begin Source File

SOURCE=.\DlgPaso5_2.cpp
# End Source File
# Begin Source File

SOURCE=.\DlgPaso5_3.cpp
# End Source File
# Begin Source File

SOURCE=.\DlgPaso5_6.cpp
# End Source File
# Begin Source File

SOURCE=.\DlgSeguro.cpp
# End Source File
# Begin Source File

SOURCE=.\DlgWin98.cpp
# End Source File
# Begin Source File

SOURCE=.\Excepciones.cpp
# End Source File
# Begin Source File

SOURCE=.\IDIOMA.cpp
# End Source File
# Begin Source File

SOURCE=.\StdAfx.cpp
# ADD CPP /Yc"stdafx.h"
# End Source File
# Begin Source File

SOURCE=.\thread.cpp
# End Source File
# End Group
# Begin Group "Header Files"

# PROP Default_Filter "h;hpp;hxx;hm;inl"
# Begin Source File

SOURCE=.\Admin_CLAUER.h
# End Source File
# Begin Source File

SOURCE=.\Admin_CLAUERDlg.h
# End Source File
# Begin Source File

SOURCE=.\DlgContras.h
# End Source File
# Begin Source File

SOURCE=.\DlgPaso1.h
# End Source File
# Begin Source File

SOURCE=.\DlgPaso2.h
# End Source File
# Begin Source File

SOURCE=.\DlgPaso20.h
# End Source File
# Begin Source File

SOURCE=.\DlgPaso21.h
# End Source File
# Begin Source File

SOURCE=.\DlgPaso22.h
# End Source File
# Begin Source File

SOURCE=.\DlgPaso23.h
# End Source File
# Begin Source File

SOURCE=.\DlgPaso28.h
# End Source File
# Begin Source File

SOURCE=.\DlgPaso3.h
# End Source File
# Begin Source File

SOURCE=.\DlgPaso4.h
# End Source File
# Begin Source File

SOURCE=.\DlgPaso50.h
# End Source File
# Begin Source File

SOURCE=.\DlgPaso5_1.h
# End Source File
# Begin Source File

SOURCE=.\DlgPaso5_2.h
# End Source File
# Begin Source File

SOURCE=.\DlgPaso5_3.h
# End Source File
# Begin Source File

SOURCE=.\DlgPaso5_6.h
# End Source File
# Begin Source File

SOURCE=.\DlgSeguro.h
# End Source File
# Begin Source File

SOURCE=.\DlgWin98.h
# End Source File
# Begin Source File

SOURCE=.\Excepciones.h
# End Source File
# Begin Source File

SOURCE=.\IDIOMA.h
# End Source File
# Begin Source File

SOURCE=.\Resource.h
# End Source File
# Begin Source File

SOURCE=.\StdAfx.h
# End Source File
# Begin Source File

SOURCE=.\thread.h
# End Source File
# End Group
# Begin Group "Resource Files"

# PROP Default_Filter "ico;cur;bmp;dlg;rc2;rct;bin;rgs;gif;jpg;jpeg;jpe"
# Begin Source File

SOURCE=.\res\Admin_CLAUER.ico
# End Source File
# Begin Source File

SOURCE=.\res\Admin_CLAUER.rc2
# End Source File
# Begin Source File

SOURCE=.\res\bitmap1.bmp
# End Source File
# Begin Source File

SOURCE=.\res\bitmap2.bmp
# End Source File
# Begin Source File

SOURCE=.\res\bitmap3.bmp
# End Source File
# Begin Source File

SOURCE=.\res\bitmap4.bmp
# End Source File
# Begin Source File

SOURCE=.\res\bitmap5.bmp
# End Source File
# Begin Source File

SOURCE=.\res\fotoClauer.bmp
# End Source File
# Begin Source File

SOURCE=".\imagenes\uji-2.ico"
# End Source File
# Begin Source File

SOURCE=.\uji.ico
# End Source File
# Begin Source File

SOURCE=..\LIBUPDATE\uji.ico
# End Source File
# End Group
# Begin Source File

SOURCE=.\ReadMe.txt
# End Source File
# Begin Source File

SOURCE=..\LIBIMPORT\Salida\LIBIMPORT.lib
# End Source File
# Begin Source File

SOURCE=..\LIBRT\Salida\LibRT.lib
# End Source File
# Begin Source File

SOURCE=..\libFormat\Salida\libformat.lib
# End Source File
# Begin Source File

SOURCE=..\CRYPTOWrapper\Salida\CryptoWrapper.lib
# End Source File
# Begin Source File

SOURCE=..\LIBUPDATE\Salida\LIBUPDATE.lib
# End Source File
# End Target
# End Project
