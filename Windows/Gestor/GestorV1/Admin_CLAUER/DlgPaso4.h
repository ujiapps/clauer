#if !defined(AFX_DLGPASO4_H__E7B8A6F5_D528_4AA8_A3A1_46F78D92C67C__INCLUDED_)
#define AFX_DLGPASO4_H__E7B8A6F5_D528_4AA8_A3A1_46F78D92C67C__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// DlgPaso4.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// DlgPaso4 dialog

class DlgPaso4 : public CDialog
{
// Construction
public:
	DlgPaso4(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(DlgPaso4)
	enum { IDD = IDD_DIALOG_PASO_4 };
	CStatic	m_ubi;
	CStatic	m_ie;
	CStatic	m_fin;
	CStatic	m_disq;
	CButton	m_cuadro;
	CStatic	m_texto;
	int		m_radio;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(DlgPaso4)
	public:
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(DlgPaso4)
	virtual BOOL OnInitDialog();
	afx_msg void OnShowWindow(BOOL bShow, UINT nStatus);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DLGPASO4_H__E7B8A6F5_D528_4AA8_A3A1_46F78D92C67C__INCLUDED_)
