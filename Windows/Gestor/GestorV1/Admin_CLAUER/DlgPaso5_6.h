#if !defined(AFX_DLGPASO5_6_H__E64E6C0C_8B43_4A96_88A5_32744D454CFE__INCLUDED_)
#define AFX_DLGPASO5_6_H__E64E6C0C_8B43_4A96_88A5_32744D454CFE__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// DlgPaso5_6.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// DlgPaso5_6 dialog

class DlgPaso5_6 : public CDialog
{
// Construction
public:
	DlgPaso5_6(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(DlgPaso5_6)
	enum { IDD = IDD_DIALOG_PASO_5_6 };
	CStatic	m_texto3;
	CStatic	m_texto2;
	CStatic	m_texto;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(DlgPaso5_6)
	public:
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(DlgPaso5_6)
	afx_msg void OnShowWindow(BOOL bShow, UINT nStatus);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DLGPASO5_6_H__E64E6C0C_8B43_4A96_88A5_32744D454CFE__INCLUDED_)
