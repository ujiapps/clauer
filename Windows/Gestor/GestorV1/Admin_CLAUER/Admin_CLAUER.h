// Admin_CLAUER.h : main header file for the ADMIN_CLAUER application
//

#if !defined(AFX_ADMIN_CLAUER_H__3D740AB1_0510_4432_BA7A_3D153FCE65F5__INCLUDED_)
#define AFX_ADMIN_CLAUER_H__3D740AB1_0510_4432_BA7A_3D153FCE65F5__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"		// main symbols
#include "DlgWin98.h"

/////////////////////////////////////////////////////////////////////////////
// CAdmin_CLAUERApp:
// See Admin_CLAUER.cpp for the implementation of this class
//

class CAdmin_CLAUERApp : public CWinApp
{
public:
	CAdmin_CLAUERApp();
	DlgWin98 m_dlgWin98;

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CAdmin_CLAUERApp)
	public:
	virtual BOOL InitInstance();
	//}}AFX_VIRTUAL

// Implementation

	//{{AFX_MSG(CAdmin_CLAUERApp)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_ADMIN_CLAUER_H__3D740AB1_0510_4432_BA7A_3D153FCE65F5__INCLUDED_)
