// DlgPaso3.cpp : implementation file
//

#include "stdafx.h"
#include "Admin_CLAUER.h"
#include "DlgPaso3.h"
#include "Admin_CLAUERDlg.h"
#include "IDIOMA.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// DlgPaso3 dialog


DlgPaso3::DlgPaso3(CWnd* pParent /*=NULL*/)
	: CDialog(DlgPaso3::IDD, pParent)
{
	//{{AFX_DATA_INIT(DlgPaso3)

	//}}AFX_DATA_INIT
}


void DlgPaso3::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(DlgPaso3)
	DDX_Control(pDX, IDC_DESCRIPCION, m_barra);
	DDX_Control(pDX, IDC_DESCRIBIENDO, m_descrip);
	DDX_Control(pDX, IDC_TEXTO, m_texto);
	DDX_Control(pDX, IDC_LBL_DESCRIPCION, m_lblDescripcion);
	DDX_Control(pDX, IDC_PROGRESO, m_prgProgreso);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(DlgPaso3, CDialog)
	//{{AFX_MSG_MAP(DlgPaso3)
	ON_WM_SHOWWINDOW()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// DlgPaso3 message handlers

void DlgPaso3::SetDescripcion(CString descripcion)
{
	//UpdateData(TRUE);
	m_lblDescripcion.SetWindowText(descripcion);
}


/*! \brief Establece el paso de la barra de progreso
 *
 * Establece el paso de la barra de progreso.
 */

void DlgPaso3::SetPaso(int paso)
{
	//UpdateData(TRUE);
	m_prgProgreso.SetStep(paso);
}



void DlgPaso3::Paso()
{
//	UpdateData(TRUE);
	m_prgProgreso.StepIt();
}



void DlgPaso3::Reset()
{
	UpdateData(TRUE);
	m_prgProgreso.SetPos(0);
}

void DlgPaso3::OnShowWindow(BOOL bShow, UINT nStatus) 
{
	CDialog::OnShowWindow(bShow, nStatus);
	
	// TODO: Add your message handler code here
	this->Reset();
	this->SetDescripcion("Pulse en el bot�n Empezar");
	CFont *font = new CFont;
	font->CreatePointFont(12, "System");
	m_texto.SetFont(font);
	m_texto.SetWindowText(IDIOMA_Get(IDIOMA_PASO3_NEGRITA));
	m_barra.SetWindowText(IDIOMA_Get(IDIOMA_PASO3_BARRA));
	m_descrip.SetWindowText(IDIOMA_Get(IDIOMA_PASO3_DESCRIP));

	delete font;
}

BOOL DlgPaso3::PreTranslateMessage(MSG* pMsg) 
{
	// TODO: Add your specialized code here and/or call the base class
//	bool a,b;
	if(pMsg->message==WM_KEYDOWN)
	{
		if ( (pMsg->wParam==VK_ESCAPE) )
			pMsg->wParam=NULL ;
		else if ( (pMsg->wParam==VK_RETURN) ) {
			if ( ((CAdmin_CLAUERDlg *) this->GetParent())->m_botonSiguiente.IsWindowEnabled()){
				((CAdmin_CLAUERDlg *) this->GetParent())->OnBotonSiguiente();
			}
			return TRUE;
		}
	}
	return CDialog::PreTranslateMessage(pMsg);
}
