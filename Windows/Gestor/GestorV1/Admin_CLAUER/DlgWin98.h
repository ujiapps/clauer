#if !defined(AFX_DLGWIN98_H__146BDAA3_CC16_44F1_BBF6_07B0CF91EE00__INCLUDED_)
#define AFX_DLGWIN98_H__146BDAA3_CC16_44F1_BBF6_07B0CF91EE00__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// DlgWin98.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// DlgWin98 dialog

class DlgWin98 : public CDialog
{
// Construction
public:
	DlgWin98(CWnd* pParent = NULL);   // standard constructor
	BYTE *pkcs12_1;
	DWORD tamPKCS12_1;
	BYTE *pkcs12_2;
	DWORD tamPKCS12_2;
	CString cert1;
	CString cert2;
	CString nuevo_cert1;
	CString nuevo_cert2;
// Dialog Data
	//{{AFX_DATA(DlgWin98)
	enum { IDD = IDD_DIALOG_WIN98 };
	CStatic	m_txtpn;
	CStatic	m_txtpin;
	CStatic	m_txtconf;
	CButton	m_cuadro2;
	CButton	m_cuadro1;
	CStatic	m_texto;
	CButton	m_ok;
	CButton	m_cancel;
	CString	m_pinconf;
	CString	m_pinuevo;
	CString	m_pinviejo;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(DlgWin98)
	public:
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	CString GetPIN(void);
	CString GetPINuevo(void);
	CString GetConfirmacion(void);
	void VerificarPKCS12 (CString pwd);
	int SeguridadPasswd (CString pwd);
	// Generated message map functions
	//{{AFX_MSG(DlgWin98)
	virtual BOOL OnInitDialog();
	virtual void OnOK();
	virtual void OnCancel();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DLGWIN98_H__146BDAA3_CC16_44F1_BBF6_07B0CF91EE00__INCLUDED_)
