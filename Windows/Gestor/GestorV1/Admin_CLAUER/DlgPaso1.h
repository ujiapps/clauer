#if !defined(AFX_DLGPASO1_H__EC5A923A_6BBA_4AA5_82D3_AA1010EA04AD__INCLUDED_)
#define AFX_DLGPASO1_H__EC5A923A_6BBA_4AA5_82D3_AA1010EA04AD__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// DlgPaso1.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// DlgPaso1 dialog

class DlgPaso1 : public CDialog
{
// Construction
public:
	DlgPaso1(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(DlgPaso1)
	enum { IDD = IDD_DIALOG_PASO_1 };
	CStatic	m_texto2;
	CStatic	m_texto1;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(DlgPaso1)
	public:
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(DlgPaso1)
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DLGPASO1_H__EC5A923A_6BBA_4AA5_82D3_AA1010EA04AD__INCLUDED_)
