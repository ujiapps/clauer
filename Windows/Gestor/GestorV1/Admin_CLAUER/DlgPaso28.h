#if !defined(AFX_DLGPASO28_H__D340A7C4_FF1E_4A35_8F5A_122C23CBB392__INCLUDED_)
#define AFX_DLGPASO28_H__D340A7C4_FF1E_4A35_8F5A_122C23CBB392__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// DlgPaso28.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// DlgPaso28 dialog

class DlgPaso28 : public CDialog
{
// Construction
public:
	DlgPaso28(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(DlgPaso28)
	enum { IDD = IDD_DIALOG_PASO_28 };
	CStatic	m_texto3;
	CStatic	m_texto2;
	CStatic	m_texto;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(DlgPaso28)
	public:
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(DlgPaso28)
	afx_msg void OnShowWindow(BOOL bShow, UINT nStatus);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DLGPASO28_H__D340A7C4_FF1E_4A35_8F5A_122C23CBB392__INCLUDED_)
