#if !defined(AFX_DLGSEGURO_H__CA34646F_6CB7_4E0E_816A_E3AF89CB3881__INCLUDED_)
#define AFX_DLGSEGURO_H__CA34646F_6CB7_4E0E_816A_E3AF89CB3881__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// DlgSeguro.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// DlgSeguro dialog

class DlgSeguro : public CDialog
{
// Construction
public:
	DlgSeguro(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(DlgSeguro)
	enum { IDD = IDD_DIALOG_SEGURO };
	CButton	m_ok;
	CButton	m_cancel;
	CStatic	m_text;
	CStatic	m_preg;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(DlgSeguro)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(DlgSeguro)
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DLGSEGURO_H__CA34646F_6CB7_4E0E_816A_E3AF89CB3881__INCLUDED_)
