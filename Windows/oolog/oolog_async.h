#ifndef __OOLOG_ASYNC_H__
#define __OOLOG_ASYNC_H__

#include <fstream>
#include <iostream>
#include <string>
#include <sstream>
#include <stack>

#include "eoolog.h"


using namespace std;

/* Lo chulo hubiera sido heredar del COOLOG, pero es una kk
 */

class COOLOG_ASYNC {

 protected:

  int tabSpaces;

  string fileName;
  ofstream fsOut;           // The output stream
  ostringstream strStream; 
  string tab;               // Spaces to put before the text

  stack<string> stFuncs;	// Function names stack

  int opened;

 public:

  
  COOLOG_ASYNC(void);
  ~COOLOG_ASYNC(void);

  virtual COOLOG_ASYNC &operator<< (string msg);
  virtual COOLOG_ASYNC &operator<< (long n);
  virtual COOLOG_ASYNC &operator<< (unsigned long n);
  virtual COOLOG_ASYNC &operator<< (int n);
  virtual COOLOG_ASYNC &operator<< (unsigned int n);

  virtual COOLOG_ASYNC &operator<< (ostream& (__cdecl * func)(ostream&));
  virtual COOLOG_ASYNC &operator<< (ios& (__cdecl * func)(ios& )); 

  void Open (string fileName);
  void Close(void);

  virtual void Tab   (void);
  virtual void UnTab (void);

  void EnterFunc  (string name);
  void ExitFunc   (string errMsg="EXITO");

  virtual void Flush (void);

};





#endif
