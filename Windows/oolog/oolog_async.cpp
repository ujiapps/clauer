#include "oolog_async.h"

#include <time.h>
#include <sstream>


/////////////////////////////////////////////////////////////////
// COOLOG_ASYNC methods
//

COOLOG_ASYNC::COOLOG_ASYNC(void)
{
	this->tabSpaces = 4;
	this->opened = 0;
}


void COOLOG_ASYNC::Open (string fileName)
{
  this->fsOut.open(fileName.c_str(), ios::app);
  if ( !this->fsOut ) {
    EOOLOG e;
    e << "Failed to open the output file: " << fileName;
    throw e;
  }
  this->opened = 1;
}



void COOLOG_ASYNC::Close(void)
{
	try {
		this->fsOut.close();
		this->opened = 0;
	}
	catch (...)
	{

	}
}


COOLOG_ASYNC::~COOLOG_ASYNC (void)
{
	if ( this->opened )
		this->Close();
}




COOLOG_ASYNC & COOLOG_ASYNC::operator<< (string msg)
{
	try {
		strStream << msg;
	}
	catch (...)
	{

	}

	return *this;
}





COOLOG_ASYNC &COOLOG_ASYNC::operator<<(long n)
{
	try {
		strStream << n;
	}
	catch (...) {

	}
	return *this;
}


COOLOG_ASYNC &COOLOG_ASYNC::operator<< (ostream& (__cdecl * func)(ostream&))
{
	char now[30];
	time_t auxTime;
	stringstream strAux;

	try {
		auxTime = time(NULL);

		strftime(now, 24, "[%d/%m/%Y - %H:%M:%S]", localtime(&auxTime));
		now[24] = '\0';

		strAux << strStream.str();
		strAux << now << " " << this->tab << strStream.str();
		(*func)(strAux);
		cout << strAux.str() << endl;
		strStream.str(strAux.str());
	}
	catch (...)
	{

	}

	return *this;
}


COOLOG_ASYNC & COOLOG_ASYNC::operator<< (ios& (__cdecl * func)(ios& ))
{
	try {
		char now[30];
		time_t auxTime;
		stringstream strAux;

		auxTime = time(NULL);

		strftime(now, 24, "[%d/%m/%Y - %H:%M:%S]", localtime(&auxTime));
		now[24] = '\0';

		strAux << now << " " << this->tab << strStream.str();
		(*func)(strAux);
		strStream.str(strAux.str());
	}
	catch (...)
	{


	}

  return *this;
}


COOLOG_ASYNC & COOLOG_ASYNC::operator<<(unsigned long n)
{
	try {
		strStream << n;
	}
	catch (...)
	{

	}

	return *this;
}

COOLOG_ASYNC & COOLOG_ASYNC::operator<<(int n)
{
	try {
		strStream << n;
	}
	catch (...)
	{


	}

	return *this;
}

COOLOG_ASYNC & COOLOG_ASYNC::operator<<(unsigned int n)
{
	try {
		strStream << n;
	}
	catch (...)
	{

	}

	return *this;

}



void COOLOG_ASYNC::Tab (void)
{
	try {
		for ( int i = 0 ; i < COOLOG_ASYNC::tabSpaces ; i++ )
			this->tab += " ";
	}
	catch (...)
	{

	}
}



void COOLOG_ASYNC::UnTab (void)
{
	try {
		if (!this->tab.empty()) 
			this->tab.erase(0, COOLOG_ASYNC::tabSpaces); 
	}
	catch (...)
	{


	}
}



void COOLOG_ASYNC::EnterFunc  (string name)
{
	try {
		this->stFuncs.push(name);
		*this << "INICIO funci�n: " 
			  << name 
			  << "()" 
			  << endl;

		this->Tab();
	}
	catch (...)
	{

	}
	
}



void COOLOG_ASYNC::ExitFunc (string errMsg/*="EXITO"*/)
{
	try {
		string name;
		name = this->stFuncs.top();
		this->stFuncs.pop();
		this->UnTab();

		*this << "FIN funci�n: " 
			  << name 
			  << "()"
			  << ": " 
			  << errMsg 
			  << endl;

	}
	catch (...)
	{

	}
}




void COOLOG_ASYNC::Flush (void)
{
	this->fsOut << this->strStream.str();
	this->strStream.str("");
}

