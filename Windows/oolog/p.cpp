#include <iostream>
#include "oolog.h"


using namespace std;


int main (void)
{
  COOLOG log("yo.log");

  log << "Hola caracola";
  log << ". Me llamo Juan" 
      << endl;

  log << "Ahora vamos a seguir" 
      << " a�adiendo cosas" 
      << endl;

  log.Tab();

  log << "Y ahora seguimos a�adiendo cosas pero identando."
      << " Lo mejor es que esto es muy c�modo."
      << endl;
  
  log << "Ahora vamos a probar con n�meros: " 
      << 1 
      << " " 
      << 12045 
      << endl;

  log.UnTab();
  log << "Esto ya est�" << endl;




}
