#include "eoolog.h"

///////////////////////////////////////////////////////////////7
// Exception methods

EOOLOG & EOOLOG::operator<< (string msg) throw ()
{
	try {
		this->msg += msg;
	}
	catch (...) {

	}
	return *this;

}


ostream & operator<< (ostream &os, const EOOLOG &e) throw ()
{
	try {
		os << e.msg.c_str() << endl;
	}
	catch (...) {

	}

	return os;
}

