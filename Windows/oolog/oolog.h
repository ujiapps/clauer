#ifndef __OOLOG_H__
#define __OOLOG_H__

#include <fstream>
#include <iostream>
#include <string>
#include <sstream>
#include <stack>

#include "eoolog.h"

using namespace std;

// Object Oriented Log Class :-P

class COOLOG {

 protected:

  int tabSpaces;

  string fileName;
  ofstream fsOut;           // The output stream
  ostringstream strStream; 
  string tab;               // Spaces to put before the text

  stack<string> stFuncs;	// Function names stack

  int opened;

 public:

  //COOLOG(void) throw () { };
  COOLOG(void);
  ~COOLOG(void);

  virtual COOLOG &operator<< (string msg);
  virtual COOLOG &operator<< (long n);
  virtual COOLOG &operator<< (unsigned long n);
  virtual COOLOG &operator<< (int n);
  virtual COOLOG &operator<< (unsigned int n);

  virtual COOLOG &operator<< (ostream& (__cdecl * func)(ostream&));
  virtual COOLOG &operator<< (ios& (__cdecl * func)(ios& )); 

  void Open (string fileName);
  void Close(void);

  virtual void Tab   (void);
  virtual void UnTab (void);

  void EnterFunc  (string name);
  void ExitFunc   (string errMsg="EXITO");

  virtual void Flush (void) {};

};


#endif
