#include "oolog.h"

#include <time.h>
#include <sstream>


/////////////////////////////////////////////////////////////////
// COOLOG methods
//

COOLOG::COOLOG(void)
{
#ifdef NO_DUMMY
  this->tabSpaces = 4;
  this->opened = 0;
#endif
}


void COOLOG::Open (string fileName)
{
#ifdef NO_DUMMY
  this->fsOut.open(fileName.c_str(), ios::app);
  if ( !this->fsOut ) {
    EOOLOG e;
    e << "Failed to open the output file: " << fileName;
    throw e;
  }
  this->opened = 1;
#endif
}



void COOLOG::Close(void)
{
#ifdef NO_DUMMY
	try {
		this->fsOut.close();
		this->opened = 0;
	}
	catch (...)
	{

	}
#endif
}


COOLOG::~COOLOG (void)
{
#ifdef NO_DUMMY
	if ( this->opened )
		this->Close();
#endif
}




COOLOG & COOLOG::operator<< (string msg)
{
#ifdef NO_DUMMY
	try {
		strStream << msg;
	}
	catch (...)
	{

	}
#endif
	return *this;
}





COOLOG &COOLOG::operator<<(long n)
{
#ifdef NO_DUMMY
	try {
		strStream << n;
	}
	catch (...) {

	}
#endif
	return *this;

}


COOLOG &COOLOG::operator<< (ostream& (__cdecl * func)(ostream&))
{
#ifdef NO_DUMMY
	char now[30];
	time_t auxTime;
	char threadID[30];

	try {
		auxTime = time(NULL);

		strftime(now, 24, "[%d/%m/%Y - %H:%M:%S]", localtime(&auxTime));
		now[24] = '\0';

		sprintf(threadID, "[%ld]", GetCurrentThreadId());

		this->fsOut << now << " " << threadID << " " << this->tab << strStream.str() << flush;
		(*func)(this->fsOut);

		strStream.str("");
	}
	catch (...)
	{

	}

#endif

	return *this;

}


COOLOG & COOLOG::operator<< (ios& (__cdecl * func)(ios& ))
{
#ifdef NO_DUMMY
	try {
		char now[30];
		time_t auxTime;
		char threadID[30];

		auxTime = time(NULL);

		strftime(now, 24, "[%d/%m/%Y - %H:%M:%S]", localtime(&auxTime));
		now[24] = '\0';

		sprintf(threadID, "[%ld]", GetCurrentThreadId());

		this->fsOut << now << " " << threadID << " " << this->tab << strStream.str() << flush;
		(*func)(this->fsOut);

		strStream.str();
	}
	catch (...)
	{


	}


#endif
  return *this;

}


COOLOG & COOLOG::operator<<(unsigned long n)
{
#ifdef NO_DUMMY
	try {
		strStream << n;
	}
	catch (...)
	{

	}

#endif
	return *this;

}

COOLOG & COOLOG::operator<<(int n)
{
#ifdef NO_DUMMY
	try {
		strStream << n;
	}
	catch (...)
	{


	}
#endif
	return *this;

}

COOLOG & COOLOG::operator<<(unsigned int n)
{
#ifdef NO_DUMMY
	try {
		strStream << n;
	}
	catch (...)
	{

	}

#endif
	return *this;

}



void COOLOG::Tab (void)
{
#ifdef NO_DUMMY
	try {
		for ( int i = 0 ; i < COOLOG::tabSpaces ; i++ )
			this->tab += " ";
	}
	catch (...)
	{

	}
#endif
}



void COOLOG::UnTab (void)
{
#ifdef NO_DUMMY
	try {
		if (!this->tab.empty()) 
			this->tab.erase(0, COOLOG::tabSpaces); 
	}
	catch (...)
	{


	}
#endif
}






void COOLOG::EnterFunc  (string name)
{
#ifdef NO_DUMMY
	try {
		this->stFuncs.push(name);
		*this << "INICIO funci�n: " 
			  << name 
			  << "()" 
			  << endl;

		this->Tab();
	}
	catch (...)
	{

	}
#endif
}



void COOLOG::ExitFunc (string errMsg/*="EXITO"*/)
{
#ifdef NO_DUMMY
	try {
		string name;
		name = this->stFuncs.top();
		this->stFuncs.pop();
		this->UnTab();

		*this << "FIN funci�n: " 
			  << name 
			  << "()"
			  << ": " 
			  << errMsg 
			  << endl;

	}
	catch (...)
	{

	}
#endif
}



