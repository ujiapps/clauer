#ifndef __EOOLOG_H__
#define __EOOLOG_H__

#include <windows.h>
#include <iostream>

using namespace std;

// An exception

class EOOLOG {

 protected:

  string msg;

 public:

  EOOLOG(void) {};

  EOOLOG & operator<< (const string msg);
  friend ostream & operator<< (ostream &os, const EOOLOG &e);

};



#endif
