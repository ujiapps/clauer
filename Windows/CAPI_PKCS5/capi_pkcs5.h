/*
 * PKCS#5 implementation of key derivation function 2: PBKDF2
 *
 *    Author: Juan Segarra Montesinos
 *
 */

#ifndef __CAPI_PKCS5_H__
#define __CAPI_PKCS5_H__

#ifndef _WIN32_WINNT
#error CAPI_PKCS5: Debe definir _WIN32_WINNT para poder compilar correctamente
#endif

#ifndef WINVER
#error CAPI_PKCS5: Debe definir WINVER para poder compilar correctamente
#endif

#include <windows.h>
#include <wincrypt.h>

#ifdef __cplusplus
extern "C" {
#endif

	/*
#ifndef SecureZeroMemory
#define SecureZeroMemory	ZeroMemory
#endif
*/

BOOL CAPI_PKCS5_3DES_PBE_Init (LPCSTR pwd, 
							   BYTE *salt, 
							   DWORD saltLen, 
							   DWORD iterCount, 
							   HCRYPTPROV *hProv,
							   HCRYPTKEY *hKey);

/* No crea un nuevo contexto del CSP */

BOOL CAPI_PKCS5_3DES_PBE_Init_Ex (LPCSTR pwd, 
								  BYTE *salt, 
								  DWORD saltLen, 
								  DWORD iterCount, 
								  HCRYPTPROV *hProv, 
								  HCRYPTKEY *hKey);



#ifdef __cplusplus
}
#endif

#endif
