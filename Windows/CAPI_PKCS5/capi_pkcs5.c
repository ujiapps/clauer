#include "capi_pkcs5.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define SHA1_BLOCK_LEN	64			/* block size of SHA1 in bytes */
#define SHA1_LEN		20			/* size of SHA1 digest in bytes */
#define HMAC_SHA1_LEN	SHA1_LEN    /* size of HMAC-SHA1 MAC in bytes */

#define DES3_BLOCK_LEN	8
#define DES3_IV_LEN		DES3_BLOCK_LEN
#define DES3_KEY_LEN	24

#define CONTAINER_NAME	"capi-pkcs5-temporary-container-dont-delete"
#define CSP				MS_ENHANCED_PROV



/*! \brief Implements HMAC-SHA1 as described in RFC2104. It uses MS Crypto API to perform
 *		   SHA1 digests
 *
 * Implementes HMAC-SHA1 as described in RFC2104. It uses MS Crypto API to perform SHA1
 * digests
 *
 * \param text
 *		  The text to calcule the HMAC
 *
 * \param textLen
 *		  The size of text in bytes
 *
 * \param key
 *		  The key to use
 *
 * \param keyLen
 *		  The size of key in bytes
 *
 * \param out
 *		  The HMAC. It must be a buffer of at least HMAC_SHA1_LEN bytes
 *
 * \retval TRUE
 *		   No error
 *
 * \retval FALSE
 *		   Error
 */

BOOL P5_HMACSHA1 (BYTE *text, DWORD textLen, BYTE *key, DWORD keyLen, BYTE *out, HCRYPTPROV hProv)
{

	HCRYPTHASH hHash;
	BYTE ipad[SHA1_BLOCK_LEN], opad[SHA1_BLOCK_LEN], tk[SHA1_LEN];
	DWORD aux = SHA1_LEN;
	register DWORD i;

	/* Use MS CryptoAPI to perform the hash
     */

	if ( keyLen > SHA1_BLOCK_LEN ) {

		if ( !CryptCreateHash(hProv, CALG_SHA1, 0, 0, &hHash) ) {
			CryptReleaseContext(hProv,0);
			return FALSE;
		}

		if ( !CryptHashData(hHash, (const BYTE *) key, keyLen, 0) ) {
			CryptDestroyHash(hHash);
			CryptReleaseContext(hProv,0);
			return FALSE;
		}

		keyLen = SHA1_LEN;

		if ( !CryptGetHashParam(hHash, HP_HASHVAL,tk,&keyLen,0) ) {
			CryptDestroyHash(hHash);
			CryptReleaseContext(hProv,0);
			return FALSE;
		}

		if ( !CryptDestroyHash(hHash) ) {
			CryptReleaseContext(hProv,0);
			return FALSE;
		}

		key = tk;
	}

	memset(ipad+keyLen, 0x36, SHA1_BLOCK_LEN-keyLen);
	memset(opad+keyLen, 0x5c, SHA1_BLOCK_LEN-keyLen);
	memcpy(ipad, key, keyLen);
	memcpy(opad, key, keyLen);

	for ( i = 0 ; i < keyLen ; i++ ) {
		*(ipad+i) ^= 0x36;
		*(opad+i) ^= 0x5c;
	}


	/* Perform inner SHA1
	 */

	if ( !CryptCreateHash(hProv, CALG_SHA1, 0, 0, &hHash ) ) {
		SecureZeroMemory(ipad,SHA1_BLOCK_LEN);
		SecureZeroMemory(opad,SHA1_BLOCK_LEN);
		CryptReleaseContext(hProv,0);
		return FALSE;
	}

	if ( !CryptHashData(hHash, ipad, SHA1_BLOCK_LEN, 0) ) {
		SecureZeroMemory(ipad,SHA1_BLOCK_LEN);
		SecureZeroMemory(opad,SHA1_BLOCK_LEN);
		CryptDestroyHash(hHash);
		CryptReleaseContext(hProv,0);
		return FALSE;
	}

	if ( !CryptHashData(hHash, text, textLen, 0) ) {
		SecureZeroMemory(ipad,SHA1_BLOCK_LEN);
		SecureZeroMemory(opad,SHA1_BLOCK_LEN);
		CryptDestroyHash(hHash);
		CryptReleaseContext(hProv,0);
		return FALSE;
	}


	if ( !CryptGetHashParam(hHash, HP_HASHVAL, out, &aux, 0) ) {
		SecureZeroMemory(ipad,SHA1_BLOCK_LEN);
		SecureZeroMemory(opad,SHA1_BLOCK_LEN);
		CryptDestroyHash(hHash);
		CryptReleaseContext(hProv,0);
		return FALSE;
	}

	if ( !CryptDestroyHash(hHash) ) {
		SecureZeroMemory(ipad,SHA1_BLOCK_LEN);
		SecureZeroMemory(opad,SHA1_BLOCK_LEN);
		CryptReleaseContext(hProv,0);
		return FALSE;
	}

	/* Perform the outer hash
	 */

	if ( !CryptCreateHash(hProv, CALG_SHA1, 0, 0, &hHash ) ) {
		SecureZeroMemory(ipad,SHA1_BLOCK_LEN);
		SecureZeroMemory(opad,SHA1_BLOCK_LEN);
		CryptReleaseContext(hProv,0);
		return FALSE;
	}

	if ( !CryptHashData(hHash, opad, SHA1_BLOCK_LEN, 0) ) {
		SecureZeroMemory(ipad,SHA1_BLOCK_LEN);
		SecureZeroMemory(opad,SHA1_BLOCK_LEN);
		CryptDestroyHash(hHash);
		CryptReleaseContext(hProv,0);
		return FALSE;
	}

	if ( !CryptHashData(hHash, out, SHA1_LEN, 0) ) {
		SecureZeroMemory(ipad,SHA1_BLOCK_LEN);
		SecureZeroMemory(opad,SHA1_BLOCK_LEN);
		CryptDestroyHash(hHash);
		CryptReleaseContext(hProv,0);
		return FALSE;
	}

	if ( !CryptGetHashParam(hHash, HP_HASHVAL, out, &aux, 0) ) {
		SecureZeroMemory(ipad,SHA1_BLOCK_LEN);
		SecureZeroMemory(opad,SHA1_BLOCK_LEN);
		CryptDestroyHash(hHash);
		CryptReleaseContext(hProv,0);
		return FALSE;
	}

	if ( !CryptDestroyHash(hHash) ) {
		SecureZeroMemory(ipad,SHA1_BLOCK_LEN);
		SecureZeroMemory(opad,SHA1_BLOCK_LEN);
		CryptReleaseContext(hProv,0);
		return FALSE;
	}

	SecureZeroMemory(ipad, SHA1_BLOCK_LEN);
	SecureZeroMemory(opad, SHA1_BLOCK_LEN);

	return TRUE;

}

/*! \brief Implements PBKDF2 as described in PKCS#5 v. 2.0.
 *
 * Implements PBKDF2 as described in PKCS#5.
 *
 * \param pwd
 *		  The password to derive a key from. It must be a null terminated ASCII string
 *
 * \param salt
 *		  The salt to use
 *
 * \param saltLen
 *		  The size of salt in bytes
 *
 * \param iterCount
 *		  The iteration count
 *
 * \param key
 *		  The output key. It must be dkLen-byte-size buffer.
 *
 * \param dkLen
 *		  The size of key buffer in bytes.
 *
 * \retval TRUE
 *		   No error
 *
 * \retval FALSE
 *		   Error
 */

BOOL P5_PBKDF2 (LPCSTR pwd, BYTE *salt, DWORD saltLen, DWORD iterCount, BYTE *key, DWORD dkLen)
{
	BYTE *T, U[HMAC_SHA1_LEN], *salt_INT, *auxT;
	register DWORD i, j, k, l, r, pwdLen;
	DWORD aux;
	HCRYPTPROV hProv;

	r = (dkLen % HMAC_SHA1_LEN);
	l = (dkLen / HMAC_SHA1_LEN) + (r ? 1 : 0 );

	if ( !CryptAcquireContext(&hProv, NULL,CSP,PROV_RSA_FULL, CRYPT_VERIFYCONTEXT) ) 
		return FALSE;

	T = (BYTE *) malloc (HMAC_SHA1_LEN * l);
	if ( !T ) {
		CryptReleaseContext(hProv,0);
		return FALSE;
	}

	salt_INT = (BYTE *) malloc (saltLen + 4);
	if ( !salt_INT ) {
		CryptReleaseContext(hProv,0);
		free(T);
		return FALSE;
	}

	pwdLen = strlen(pwd);

	memcpy(salt_INT, salt, saltLen);

	auxT = T;
	for ( i = 1 ; i <= l ; i++ ) {
		
		/* Calculamos U1
		 */

		aux = ( (i & 0x000000ff) << 24 ) |
			  ( (i & 0x0000ff00) << 8 ) |
			  ( (i & 0x00ff0000) >> 8 ) |
			  ( (i & 0xff000000) >> 24 );

		memcpy(salt_INT+saltLen, &aux, 4);

		if ( !P5_HMACSHA1( salt_INT, saltLen+4, (BYTE *) pwd, pwdLen, U, hProv) ) {
			SecureZeroMemory(T, HMAC_SHA1_LEN*l);
			SecureZeroMemory(U, HMAC_SHA1_LEN);
			SecureZeroMemory(salt_INT, saltLen+4);
			CryptReleaseContext(hProv,0);
			free(T);
			free(salt_INT);
			return FALSE;
		}

		memcpy(auxT, U, HMAC_SHA1_LEN);

		/* Calculamos Un/n>1 y hacemos el XOR a la vez
		 */

		for ( j = 2 ; j <= iterCount ; j++ ) {
			/* Calculate U_j
			 */
			if ( !P5_HMACSHA1( U, HMAC_SHA1_LEN, (BYTE *) pwd, pwdLen, U, hProv ) ) {
				CryptReleaseContext(hProv,0);
				SecureZeroMemory(T, HMAC_SHA1_LEN*l);
				SecureZeroMemory(U, HMAC_SHA1_LEN);
				SecureZeroMemory(salt_INT, saltLen+4);
				free(T);
				free(salt_INT);
				T = NULL;
				return FALSE;
			}
			/* Do the XOR
			 */
			for ( k = 0 ; k < HMAC_SHA1_LEN ; k++ )
				(*(auxT+k)) ^= (*(U+k));
		}

		auxT += HMAC_SHA1_LEN;
	}
	
	if ( r )
		memcpy(key, T, (l-1)*HMAC_SHA1_LEN+r);
	else
		memcpy(key, T, l*HMAC_SHA1_LEN);

	SecureZeroMemory(U, HMAC_SHA1_LEN);
	SecureZeroMemory(T, HMAC_SHA1_LEN*l);
	SecureZeroMemory(salt_INT, saltLen+4);
	CryptReleaseContext(hProv,0);

	free(T);
	free(salt_INT);

	return TRUE;
}


/*! \brief Returns an initialized HCRYPTPROV and HCRYPTKEY with the session key specified
 *		   in key.
 *
 * Returns an initialized HCRYPTPROV and HCRYPTKEY with the session key specified
 * in key.
 *
 * \param key
 *		  The session key to be imported in the CSP.
 *
 * \param hProv
 *		  The CSP context returned.
 *
 * \param hKey
 *		  The session key handle imported.
 *
 * \retval TRUE
 *		   La funci�n termin� con �xito
 *
 * \retval FALSE
 *		   La funci�n termin� con error
 *
 * \warning El par�metro key se machaca si la funci�n tuvo �xito.
 */

BOOL CAPI_PBE_3DES_Get_HCRYPTKEY (BYTE *key, HCRYPTPROV *hProv, HCRYPTKEY *hKey)
{
	HCRYPTKEY hKeyEx;
	BYTE *blob, *auxBlob;
	DWORD blobLen, keyLen = DES3_KEY_LEN;

	/* Adquiero el contexto al container que contiene la llave AT_KEYEXCHANGE
	 * para crear el SIMPLEBLOB e Importar la llave. Si el container no existe
	 * entonces lo creo. Si no hay llave AT_KEYEXCHANGE, genero una nueva
	 */

	if ( !CryptAcquireContext(hProv, CONTAINER_NAME, CSP, PROV_RSA_FULL, 0) ) {
		if ( GetLastError() == NTE_BAD_KEYSET ) {
			if  (!CryptAcquireContext(hProv, CONTAINER_NAME, CSP, PROV_RSA_FULL, CRYPT_NEWKEYSET) ) 
				return FALSE;
		} else
			return FALSE;
	}

	if ( !CryptGetUserKey(*hProv, AT_KEYEXCHANGE, &hKeyEx) ) {
		if ( GetLastError() == NTE_NO_KEY ) {
			if (!CryptGenKey(*hProv, AT_KEYEXCHANGE, 0x02000000, &hKeyEx)) {
				CryptReleaseContext(*hProv,0);
				return FALSE;
			}
		} else {
			CryptReleaseContext(*hProv, 0);
			return FALSE;
		}
	}

	/* Creamos un simple blob para importarlo en el CSP
	 */

	blobLen = sizeof(BLOBHEADER)+sizeof(ALG_ID)+64;
	blob    = (BYTE *) malloc (blobLen);
	if ( !blob ) {
		CryptDestroyKey(hKeyEx);
		CryptReleaseContext(*hProv,0);
		return FALSE;
	}

	memset(blob,0,blobLen);

	auxBlob = blob;

	((BLOBHEADER *) auxBlob)->bType    = SIMPLEBLOB;
	((BLOBHEADER *) auxBlob)->bVersion = 0x02;
	((BLOBHEADER *) auxBlob)->aiKeyAlg = CALG_3DES;

	auxBlob += sizeof(BLOBHEADER);

	*((ALG_ID *) auxBlob) = CALG_RSA_KEYX;

	auxBlob += sizeof(ALG_ID);

	memcpy(auxBlob, key, DES3_KEY_LEN);

	if ( !CryptEncrypt(hKeyEx, 0, TRUE, 0, auxBlob, &keyLen, 64) ) {
		SecureZeroMemory(blob, blobLen);
		free(blob);
		blob = NULL;
		CryptDestroyKey(hKeyEx);
		CryptReleaseContext(*hProv,0);
		return FALSE;
	}

	/* Importamos la llave en el contexto que nos pasan
	 */

	if ( !CryptImportKey(*hProv, blob, blobLen, hKeyEx, 0, hKey) ) {
		SecureZeroMemory(blob, blobLen);
		free(blob);
		blob = NULL;
		CryptDestroyKey(hKeyEx);
		CryptReleaseContext(*hProv,0);
		return FALSE;
	}

	SecureZeroMemory(key, DES3_KEY_LEN);

	SecureZeroMemory(blob, blobLen);
	free(blob);
	blob = NULL;
	CryptDestroyKey(hKeyEx);

	return TRUE;
}


















/*! \brief Context initialization of a provider for PBE with 3DES with three keys. It uses
 *		   PKCS#5 version 2.0 (PBKDF2) as the key derivation function. It's part of PBES2.
 * 
 * Context initialization of a provider for PBE with 3DES with three keys.
 *
 * \param pwd
 *		  The password to use as a NULL-terminated ASCII string.
 *
 * \param salt
 *		  The salt to use.
 *
 * \param iterCount
 *		  The iteration count to use.
 *
 * \param hProv
 *		  The CSP context returned.
 *
 * \param hKey
 *		  The handle to the key derived.
 *
 * \retval TRUE
 *		   No error.
 *
 * \retval ERROR
 *		   The function terminated with errors.
 */

BOOL CAPI_PKCS5_3DES_PBE_Init (LPCSTR pwd, BYTE *salt, DWORD saltLen, DWORD iterCount, HCRYPTPROV *hProv, HCRYPTKEY *hKey)
{
	BYTE *key;
	
	/* Obtain the key
	 */

	key = (BYTE *) malloc (DES3_KEY_LEN);
	if (!key)
		return FALSE;

	if ( ! P5_PBKDF2(pwd, salt, saltLen, iterCount, key, DES3_KEY_LEN) ) {
		free(key);
		key = NULL;
		return FALSE;
	}

	/* Obtain the CSP handles
	 */

	if ( !CAPI_PBE_3DES_Get_HCRYPTKEY(key, hProv, hKey) ) {
		SecureZeroMemory(key, DES3_KEY_LEN);
		free(key);
		key = NULL;
		return FALSE;
	}

	SecureZeroMemory(key, DES3_KEY_LEN);
	free(key);
	key = NULL;

	return TRUE;
}












/*! \brief Implements PBKDF1 as described in PKCS#5.
 *
 * Implements PBKDF1 as described in PKCS#5. PBKDF1 is a key derivation function that must
 * not be used only for compatibility with older implementations. New applications must use
 * PBKDF2.
 *
 * \param pwd
 *		  The password to derive a key from. It must be a null terminated ASCII string
 *
 * \param salt
 *		  The salt to use.
 *
 * \param iterCount
 *		  The iteration count
 *
 * \param key
 *		  The output key. It must be dkLen-byte-size buffer.
 *
 * \param keyLen
 *		  The size of key buffer in bytes.
 *
 * \retval TRUE
 *		   No error
 *
 * \retval FALSE
 *		   Error
 */

BOOL P5_PBKDF1 (LPCSTR pwd, BYTE salt[8], DWORD iterCount, BYTE *key, DWORD keyLen)
{
	HCRYPTPROV hProv;
	HCRYPTHASH hHash;
	BYTE *P_S, T[SHA1_LEN];
	DWORD pwdLen, len=SHA1_LEN, i;

	if ( (keyLen > SHA1_LEN) || (iterCount == 0) )
		return FALSE;

	pwdLen = strlen(pwd);

	P_S = (BYTE *) malloc (8+pwdLen);
	if ( !P_S )
		return FALSE;

	memcpy(P_S, pwd, pwdLen);
	memcpy(P_S+pwdLen, salt, 8);

	if (!CryptAcquireContext(&hProv, NULL, CSP, PROV_RSA_FULL, CRYPT_VERIFYCONTEXT)) {
		free(P_S);
		return FALSE;
	}

	if ( !CryptCreateHash(hProv, CALG_SHA1, 0, 0, &hHash) ) {
		free(P_S);
		CryptReleaseContext(hProv,0);
		return FALSE;
	}
	
	if ( !CryptHashData(hHash, P_S, pwdLen+8, 0) ) {
		free(P_S);
		CryptDestroyHash(hHash);
		CryptReleaseContext(hProv,0);
		return FALSE;
	}

	if ( !CryptGetHashParam(hHash, HP_HASHVAL, T, &len, 0) ) {
		free(P_S);
		CryptDestroyHash(hHash);
		CryptReleaseContext(hProv,0);
		return FALSE;
	}

	if ( !CryptDestroyHash(hHash) ) {
		free(P_S);
		CryptDestroyHash(hHash);
		CryptReleaseContext(hProv,0);
		return FALSE;
	}

	SecureZeroMemory(P_S, pwdLen+8);
	free(P_S);

	for ( i = 1 ; i < iterCount ; i++ ) {

		if ( !CryptCreateHash(hProv, CALG_SHA1, 0, 0, &hHash) ) {
			CryptReleaseContext(hProv,0);
			return FALSE;
		}
	
		if ( !CryptHashData(hHash, T, SHA1_LEN, 0) ) {
			CryptDestroyHash(hHash);
			CryptReleaseContext(hProv,0);
			return FALSE;
		}

		if ( !CryptGetHashParam(hHash, HP_HASHVAL, T, &len, 0) ) {
			CryptDestroyHash(hHash);
			CryptReleaseContext(hProv,0);
			return FALSE;
		}

		if ( !CryptDestroyHash(hHash) ) {
			CryptDestroyHash(hHash);
			CryptReleaseContext(hProv,0);
			return FALSE;
		}
	}

	SecureZeroMemory(T,SHA1_LEN);
	memcpy(key, T, keyLen);

	return FALSE;

}






/* Obtiene el handle a la llave en el contexto que se pasa
 */

BOOL CAPI_PBE_3DES_Get_HCRYPTKEY_Ex (BYTE *key, HCRYPTPROV hProv, HCRYPTKEY *hKey)
{
	HCRYPTKEY hKeyEx;
	BYTE *blob, *auxBlob;
	DWORD blobLen, keyLen = DES3_KEY_LEN;
	DWORD keyExLen, auxSize;

	/* Obtengo la llave AT_KEYEXCHANGE.
	 * Si no la hay genero un par de llave nuevo
	 */


	if ( !CryptGetUserKey(hProv, AT_KEYEXCHANGE, &hKeyEx) ) {
		if ( GetLastError() == NTE_NO_KEY ) {
			/* Activo el flag exportable para no interferir con el CSP del CLauer
			 */
			if (!CryptGenKey(hProv, AT_KEYEXCHANGE, 0x02000000|CRYPT_EXPORTABLE, &hKeyEx)) {
				return FALSE;
			}
		} else {
			return FALSE;
		}
	}

	auxSize = sizeof keyLen;
	if ( ! CryptGetKeyParam(hKeyEx, KP_KEYLEN, &keyExLen, &auxSize, 0) ) 
		return FALSE;
	keyExLen /= 8;

	/* Creamos un simple blob para importarlo en el CSP
	 */

	blobLen = sizeof(BLOBHEADER) + sizeof(ALG_ID) + keyExLen;
	blob    = (BYTE *) malloc (blobLen);
	if ( ! blob ) {
		CryptDestroyKey(hKeyEx);
		return FALSE;
	}

	memset(blob,0,blobLen);

	auxBlob = blob;

	((BLOBHEADER *) auxBlob)->bType    = SIMPLEBLOB;
	((BLOBHEADER *) auxBlob)->bVersion = 0x02;
	((BLOBHEADER *) auxBlob)->aiKeyAlg = CALG_3DES;

	auxBlob += sizeof(BLOBHEADER);

	*((ALG_ID *) auxBlob) = CALG_RSA_KEYX;

	auxBlob += sizeof(ALG_ID);

	memcpy(auxBlob, key, DES3_KEY_LEN);

	if ( ! CryptEncrypt(hKeyEx, 0, TRUE, 0, auxBlob, &keyLen, keyExLen) ) {

		SecureZeroMemory(blob, blobLen);
		free(blob);
		blob = NULL;
		CryptDestroyKey(hKeyEx);
		return FALSE;
	}

	/* Importamos la llave en el contexto que nos pasan
	 */

	if ( ! CryptImportKey(hProv, blob, blobLen, hKeyEx, 0, hKey) ) {
		SecureZeroMemory(blob, blobLen);
		free(blob);
		blob = NULL;
		CryptDestroyKey(hKeyEx);
		return FALSE;
	}

	SecureZeroMemory(key, DES3_KEY_LEN);

	SecureZeroMemory(blob, blobLen);
	free(blob);
	blob = NULL;
	CryptDestroyKey(hKeyEx);

	return TRUE;
}






BOOL CAPI_PKCS5_3DES_PBE_Init_Ex (LPCSTR pwd, BYTE *salt, DWORD saltLen, DWORD iterCount, HCRYPTPROV *hProv, HCRYPTKEY *hKey)
{
	BYTE *key;
	
	/* Obtain the key
	 */

	key = (BYTE *) malloc (DES3_KEY_LEN);
	if (!key)
		return FALSE;




	if ( ! P5_PBKDF2(pwd, salt, saltLen, iterCount, key, DES3_KEY_LEN) ) {
		free(key);
		key = NULL;
		return FALSE;
	}



	/* Obtenemos el handle a la llave
	 */

	if ( ! CAPI_PBE_3DES_Get_HCRYPTKEY_Ex(key, *hProv, hKey) ) {
		SecureZeroMemory(key, DES3_KEY_LEN);
		free(key);
		key = NULL;
		return FALSE;
	}



	SecureZeroMemory(key, DES3_KEY_LEN);
	free(key);
	key = NULL;

	return TRUE;
}



