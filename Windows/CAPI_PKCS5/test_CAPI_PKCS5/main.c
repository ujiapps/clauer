#ifndef _WIN32_WINNT
#error ERROR. Debe definir _WIN32_WINNT para poder compilar correctamente
#endif

#ifndef WINVER
#error ERROR. Debe definir WINVER para poder compilar correctamente
#endif

#include <windows.h>
#include <wincrypt.h>

#include "../capi_pkcs5.h"

#include <openssl/evp.h>
#include <openssl/rand.h>
#include <openssl/x509.h>


#define DES3_KEY_LEN	24
#define DES3_BLOCK_LEN	8



void Test_OSSL_YO (void)
{
	HCRYPTPROV hProv;
	HCRYPTKEY hKey;
	BYTE bufCifrado[1024], salt[8], iv[DES3_BLOCK_LEN];
	DWORD lenTexto,i, pwdLen, saltLen=8, iterCount=1000,aux, tamCifrado;
	LPCSTR texto = "Soy el texto a cifrar";
	LPCSTR pwd = "juanjuan";

	BYTE descifrado[1024];
	DWORD tamDescifrado;

	X509_ALGOR *alg;
	EVP_CIPHER_CTX ctx;
	
	OpenSSL_add_all_algorithms();

	RAND_pseudo_bytes(salt,8);
	pwdLen = strlen("juanjuan");
	lenTexto = strlen(texto);

	/* Ciframos con OpenSSL
	 */

	memset(iv,0,DES3_BLOCK_LEN);

	alg = PKCS5_pbe2_set(EVP_des_ede3_cbc(), iterCount, salt, saltLen);

	EVP_CIPHER_CTX_init(&ctx);
	EVP_PBE_CipherInit(alg->algorithm, pwd, pwdLen, alg->parameter, &ctx, 1);
	EVP_CipherInit_ex(&ctx, NULL, NULL, NULL, iv, 1);
	EVP_CipherUpdate(&ctx, bufCifrado, &aux, texto, lenTexto);
	EVP_CipherFinal_ex(&ctx, bufCifrado+aux, &tamCifrado);
	EVP_CIPHER_CTX_cleanup(&ctx);

	tamCifrado += aux;

	printf("OSSL: ");
	for ( i = 0 ; i < tamCifrado ; i++ ) 
		printf("%x ", bufCifrado[i]);
	printf("\n");
	fflush(stdout);

	memset(bufCifrado, 0, 1024);

	/* Ciframos con nuestro m�todo
	 */

	if ( !CAPI_PKCS5_3DES_PBE_Init(pwd,salt,saltLen,iterCount, &hProv, &hKey) ) {
		fprintf(stderr, "ERROR CAPI_PKCS5_3DES_PBE_Init\n");
		return;
	}

	lenTexto = strlen(texto);
	memcpy(bufCifrado,texto,lenTexto);
	if ( !CryptEncrypt(hKey, 0, TRUE, 0, bufCifrado, &lenTexto, 1024) ) {
		fprintf(stderr, "ERROR Encrypt\n");
		return;
	}

	printf("YO:   ");
	for ( i = 0 ; i < lenTexto ; i++ ) {
		printf("%x ", bufCifrado[i]);
	}
	printf("\n");

	CryptDestroyKey(hKey);
	CryptReleaseContext(hProv,0);

	alg = PKCS5_pbe2_set(EVP_des_ede3_cbc(), iterCount, salt, saltLen);

	EVP_CIPHER_CTX_init(&ctx);
	EVP_PBE_CipherInit(alg->algorithm, pwd, pwdLen, alg->parameter, &ctx, 0);
	EVP_CipherInit_ex(&ctx, NULL, NULL, NULL, iv, 0);
	EVP_CipherUpdate(&ctx, descifrado, &tamDescifrado, bufCifrado, lenTexto);
	EVP_CipherFinal_ex(&ctx, descifrado+tamDescifrado, &lenTexto);
	EVP_CIPHER_CTX_cleanup(&ctx);

	tamDescifrado += lenTexto;

	for ( i = 0 ; i < tamDescifrado ; i++ )
		printf("%c", descifrado[i]);

}



void Test_Padding (void)
{

	HCRYPTPROV hProv;
	HCRYPTKEY hKey;
	BYTE bufCifrado[1024], salt[20], iv[DES3_BLOCK_LEN];
	DWORD lenTexto,i, pwdLen, saltLen=8, iterCount=1000,aux, tamCifrado;
	char texto[33];
	LPCSTR pwd = "juanjuan";
	DWORD padding;

	if ( !CAPI_PKCS5_3DES_PBE_Init(pwd,salt,saltLen,iterCount, &hProv, &hKey) ) {
		fprintf(stderr, "ERROR CAPI_PKCS5_3DES_PBE_Init\n");
		return;
	}

	for ( i = 0 ; i < 32 ; i++ ) 
		texto[i] = '1';
	texto[32] = '\0';

	lenTexto = strlen(texto);

	printf("lenTexto: %d\n", lenTexto);
	memcpy(bufCifrado,texto,lenTexto);

	if ( !CryptEncrypt(hKey, 0, TRUE, 0, bufCifrado, &lenTexto, 1024) ) {
		fprintf(stderr, "ERROR Encrypt\n");
		return;
	}

	printf("lenTexto: %ld\n", lenTexto);
	fflush(stdout);

	CryptDestroyKey(hKey);
	CryptReleaseContext(hProv,0);

	if ( !CAPI_PKCS5_3DES_PBE_Init(pwd,salt,saltLen,iterCount, &hProv, &hKey) ) {
		fprintf(stderr, "ERROR CAPI_PKCS5_3DES_PBE_Init\n");
		return;
	}


	if (!CryptDecrypt(hKey, 0, TRUE, 0, bufCifrado, &lenTexto) ) {
		fprintf(stderr, "ERROR Decrypt\n");
		return;
	}

	printf("lenTexto: %ld\n", lenTexto);
	fflush(stdout);

	for ( i = 0 ; i < lenTexto ; i++ ) {
		printf("%c", bufCifrado[i]);
	}
	fflush(stdout);

}




void main ()
{
	Test_OSSL_YO();
}