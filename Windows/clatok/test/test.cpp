// test.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include "test.h"

#include "../clatok/ClatokErr.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// The one and only application object

CWinApp theApp; 

using namespace std;




void ErrorTest ( void ) 
{

	try {

		try {

			CLERR_THROW(ERR_CLATOK_OUT_OF_MEMORY);

		}
		catch ( CClatokErr &err ) {
			CLERR_PROPAGATE(err);
		}

	} catch ( CClatokErr &err ){	
		CLERR_PROPAGATE(err);
	}
}





int _tmain(int argc, TCHAR* argv[], TCHAR* envp[])
{
	// initialize MFC and print and error on failure

	if (!AfxWinInit(::GetModuleHandle(NULL), NULL, ::GetCommandLine(), 0))
	{
		// TODO: change error code to suit your needs
		_tprintf(_T("Fatal Error: MFC initialization failed\n"));
		return 1;
	}


	CClatokErr err;

	ErrorTest();

	return 0;
}
