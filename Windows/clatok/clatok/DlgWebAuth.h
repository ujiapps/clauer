#pragma once
#include "CExplorer.h"

#include "TokenWallet.h"


// CDlgWebAuth dialog

class CDlgWebAuth : public CDialog
{
	DECLARE_DYNAMIC(CDlgWebAuth)

public:
	CDlgWebAuth(CWnd* pParent = NULL);   // standard constructor
	virtual ~CDlgWebAuth();

// Dialog Data
	enum { IDD = IDD_DIALOG1 };

	afx_msg void OnBnClickedOk();

	DECLARE_EVENTSINK_MAP()
	void DocumentCompleteExplorer1(LPDISPATCH pDisp, VARIANT* URL);
	afx_msg void OnTimer(UINT_PTR nIDEvent);

public:
	void    SetToken  ( TOKEN_WALLET tok );
	CString GetURL ( void );

	BSTR GetResult ( void );


protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL OnInitDialog ( void );

	BOOL m_bDownloadComplete;		// indica si se carg� completamente la p�gina o no

	TOKEN_WALLET m_tok;
	BSTR m_result;

	CString m_urlRetorno;			// url de retorno

	DECLARE_MESSAGE_MAP()
	CExplorer1 m_ie;

public:
};
