// DlgClatok.cpp: archivo de implementaci�n
//

#include "stdafx.h"
#include "clatok.h"
#include "DlgClatok.h"
#include "ClatokErr.h"
#include "DlgWebAuth.h"

#include <clio/clio.h>
#include <clui/clui.h>
#include <capi_pkcs5/capi_pkcs5.h>


#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// Cuadro de di�logo de CDlgClatok


CDlgClatok::CDlgClatok(CWnd* pParent /*=NULL*/)
	: CDialog(CDlgClatok::IDD, pParent)
{
	m_hIcon         = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
	m_clauerPassLen = 0;
}





void CDlgClatok::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_BUTTON_AUTH, m_btnAuth);
	DDX_Control(pDX, IDC_COMBO_DEVICES, m_cboDevices);
	DDX_Control(pDX, IDC_LIST1, m_lstTokens);
}



BEGIN_MESSAGE_MAP(CDlgClatok, CDialog)
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	//}}AFX_MSG_MAP
	ON_BN_CLICKED(IDC_BUTTON_ACEPTAR, &CDlgClatok::OnBnClickedButtonAceptar)
	ON_BN_CLICKED(IDC_BUTTON_SALIR, &CDlgClatok::OnBnClickedButtonSalir)
	ON_BN_CLICKED(IDC_BUTTON_AUTH, &CDlgClatok::OnBnClickedButtonAuth)
	ON_CBN_SELCHANGE(IDC_COMBO_DEVICES, &CDlgClatok::OnCbnSelchangeComboDevices)
	ON_NOTIFY(NM_DBLCLK, IDC_LIST1, &CDlgClatok::OnNMDblclkList1)
END_MESSAGE_MAP()


// Controladores de mensaje de CDlgClatok

BOOL CDlgClatok::OnInitDialog()
{

	try {

		CDialog::OnInitDialog();

		// Establecer el icono para este cuadro de di�logo. El marco de trabajo realiza esta operaci�n
		// autom�ticamente cuando la ventana principal de la aplicaci�n no es un cuadro de di�logo

		SetIcon(m_hIcon, TRUE);			// Establecer icono grande
		SetIcon(m_hIcon, FALSE);		// Establecer icono peque�o

		// Creamos el tru�o-contexto para el descifrado de los clauers

		if ( ! CryptAcquireContext(&m_hProv, 
									NULL, 
									_T("Microsoft Enhanced Cryptographic Provider v1.0"), 
									PROV_RSA_FULL, 
									CRYPT_VERIFYCONTEXT) )
		{
			CLERR_THROW(ERR_CLATOK_CANNOT_INITIALIZE_DIALOG);
		}

		m_clauerKeys = NULL;

		// Inicializamos combo con dispositivos

		EnumDevices();

		// Inicializamos la lista de tokens

		this->m_lstTokens.SetExtendedStyle(m_lstTokens.GetExtendedStyle() |
												LVS_EX_FULLROWSELECT |
												LVS_EX_GRIDLINES );

		if ( m_lstTokens.InsertColumn(0, _T("Nombre")) == -1 )
			CLERR_THROW(ERR_CLATOK_CANNOT_INITIALIZE_DIALOG);
		if ( m_lstTokens.InsertColumn(1, _T("URL")) == -1 )
			CLERR_THROW(ERR_CLATOK_CANNOT_INITIALIZE_DIALOG);

		if ( m_cboDevices.GetCount() == 0 ) 
			m_lstTokens.SetColumnWidth(0, LVSCW_AUTOSIZE_USEHEADER);
		else
			m_lstTokens.SetColumnWidth(0,LVSCW_AUTOSIZE);

		m_lstTokens.SetColumnWidth(1, LVSCW_AUTOSIZE_USEHEADER);
		
		EnumTokens();

		/* Cambio la imagen
		 */

		CStatic *imgHeader = (CStatic *) GetDlgItem(IDC_STATIC_CLATOK_HEADER);
		CButton *btnAuth = (CButton *) GetDlgItem(IDC_BUTTON_AUTH);
		HBITMAP hBmp;
		HICON hBmpButton;

		hBmp = LoadBitmap(theApp.m_hInstance, MAKEINTRESOURCE(IDB_BITMAP_CLATOK_HEADER));
		hBmpButton = LoadIcon(theApp.m_hInstance, MAKEINTRESOURCE(IDI_CANDADO_CERRADO));

		imgHeader->SetBitmap(hBmp);
		btnAuth->SetIcon(hBmpButton);

		if ( m_cboDevices.GetCount() == 0 ) {
			CComboBox *cbo = (CComboBox *) GetDlgItem(IDC_COMBO_DEVICES);
			CButton *btn = (CButton *) GetDlgItem(IDC_BUTTON_ACEPTAR);
			btn->EnableWindow(FALSE);
			cbo->EnableWindow(FALSE);
			btnAuth->EnableWindow(FALSE);
		}
		
	}
	catch ( CClatokErr &err ) {
		CLERR_SHOW(err);
		EndDialog(-1);
	}


	return TRUE;  // Devuelve TRUE  a menos que establezca el foco en un control
}

// Si agrega un bot�n Minimizar al cuadro de di�logo, necesitar� el siguiente c�digo
//  para dibujar el icono. Para aplicaciones MFC que utilicen el modelo de documentos y vistas,
//  esta operaci�n la realiza autom�ticamente el marco de trabajo.

void CDlgClatok::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // Contexto de dispositivo para dibujo

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Centrar icono en el rect�ngulo de cliente
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Dibujar el icono
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

// El sistema llama a esta funci�n para obtener el cursor que se muestra mientras el usuario arrastra
// la ventana minimizada.

HCURSOR CDlgClatok::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}


void CDlgClatok::OnBnClickedButtonAceptar()
{
	USES_CONVERSION;

	if ( m_cboDevices.GetCount() == 0 ) {
		AfxMessageBox(_T("CLATOK no detect� ficheros CRYF"), MB_ICONINFORMATION);
		return;
	}

	if ( m_lstTokens.GetItemCount() == 0 ) {
		if ( this->m_clauerKeys[m_cboDevices.GetCurSel()] )
			AfxMessageBox(_T("El dispositivo seleccionado no contiene tokens"), MB_ICONINFORMATION);
		else
			AfxMessageBox(_T("El dispositivo seleccionado no contiene tokens p�blicos. Pruebe a autenticarse"), MB_ICONINFORMATION);
		return;
	}

	/* S�lo puede haber un token seleccionado
	 */

	POSITION pos = m_lstTokens.GetFirstSelectedItemPosition();
	if ( pos == NULL ) {
		AfxMessageBox(_T("Imposible obtener token seleccionado. Por favor, seleccione un token de la lista"), MB_ICONERROR);
		return;
	}

	int nItem = m_lstTokens.GetNextSelectedItem(pos);

	/* Mostramos la ventana de autenticaci�n
	 */

	ShowWindow(SW_HIDE);
	CDlgWebAuth dlg;
	dlg.SetToken(m_tokens[nItem]);
	INT_PTR ret;
	ret = dlg.DoModal();
	if ( ret == -1 || ret == IDABORT ) {
		AfxMessageBox(_T("Error mostrando ventana de petici�n de credenciales"), MB_ICONERROR);
		ShowWindow(SW_SHOW);
		return;
	} else if ( ret == IDCANCEL ) {
		ShowWindow(SW_SHOW);
		return;
	}

	CString url;
	url.Format(dlg.GetURL().GetString(), W2A(dlg.GetResult()));
	
	HINSTANCE shell_ret;
	shell_ret = ShellExecute(this->m_hWnd, _T("open"), url.GetString(), NULL, NULL, SW_SHOWNORMAL);
	if ( (int)shell_ret <= 32 ) {
		CString msg;
		msg = _T("Error mostrando ventana de redirecci�n\r\n");
		msg += _T("URL: ");
		msg += url;

		AfxMessageBox(msg.GetString(), MB_ICONERROR);
		ShowWindow(SW_SHOW);
		return;
	}

	CDialog::OnOK();

}



BOOL CDlgClatok::EnumDevices ( void )
{

	/* Enumeramos ficheros CRYF. Se asume que el programa est�
	 * en el mismo directorio que los ficheros. De esta forma, 
	 * no se restringe �nicamente a CDs
	 */

	HANDLE hFindFile;
	WIN32_FIND_DATA findData;
	DWORD err, ret;
	TCHAR szFileName[MAX_PATH];
	TCHAR *dirs[3];

	TCHAR szCurrentDirectory[MAX_PATH];
	DWORD size;

	int i;

	++m_clauerPassLen;

	try {

		if ( ! (size = GetCurrentDirectory(MAX_PATH, szCurrentDirectory)) )
			dirs[0] = _T(".\\");
		else {
			dirs[0] = szCurrentDirectory;
			dirs[0][size] = _T('\\');
			dirs[0][size+1] = _T('\0');
		}

		if ( ! GetModuleFileName(NULL,szFileName, MAX_PATH*sizeof(TCHAR)) ) {
			dirs[1] = NULL;
		} else {
			for ( i = _tcsclen(szFileName)-1 ; szFileName[i] != _T('\\') && i>= 0 ; i--);
			if ( i>=0 )
				szFileName[i+1] = _T('\0');
			if ( _tcscmp(szFileName, szCurrentDirectory) == 0 )
				dirs[1] = NULL;
			else {
				dirs[1] = szFileName;
				dirs[2] = NULL;
			}
		}

		for ( i = 0 ; dirs[i] ; i++ ) {
			CString aux;
			CString aux2;
			aux.Format("%s%s", dirs[i], _T("cryf_???.cla"));
			hFindFile = FindFirstFile(aux.GetString(), &findData);
			if ( hFindFile == INVALID_HANDLE_VALUE ) {
				err = GetLastError();
				if ( err != ERROR_FILE_NOT_FOUND && !dirs[i+1] )
					CLERR_THROW(ERR_CLATOK_COULD_NOT_DETERMINE_CRYF_FILES);
				else if ( err == ERROR_FILE_NOT_FOUND && dirs[i+1] )
					continue;

				return TRUE;
			}

			do {

				aux2.Format("%s%s", dirs[i], findData.cFileName);
				ret = IO_Is_Clauer(aux2.GetString());
				if ( ret == IO_IS_CLAUER ) {
					++m_clauerPassLen;
					m_cboDevices.AddString(findData.cFileName);
					m_vDeviceDirs.push_back(CString(dirs[i]));
				} else if ( ret != IO_IS_NOT_CLAUER ) 
					CLERR_THROW(ERR_CLATOK_COULD_NOT_DETERMINE_CRYF_FILES);
				
			} while ( FindNextFile(hFindFile, &findData) != 0 );

			err = GetLastError();
			if ( err != ERROR_NO_MORE_FILES ) {
				FindClose(hFindFile);
				CLERR_THROW(ERR_CLATOK_COULD_NOT_DETERMINE_CRYF_FILES);
			} else {
				FindClose(hFindFile);
			}
		}

		m_cboDevices.SetCurSel(0);

		m_clauerKeys = new HCRYPTKEY[m_clauerPassLen];
		if ( ! m_clauerKeys ) 
			CLERR_THROW(ERR_CLATOK_OUT_OF_MEMORY);
		memset(m_clauerKeys, 0, sizeof(HCRYPTKEY)*m_clauerPassLen);

		if ( m_clauerPassLen == 1 ) {
			m_cboDevices.EnableWindow(FALSE);
			UpdateData(TRUE);
		}
	}
	catch ( CClatokErr &err ) {

		m_clauerPassLen = 0;
		if ( m_clauerKeys ) {
			delete [] m_clauerKeys;
			m_clauerKeys = NULL;
		}
		
		CLERR_PROPAGATE(err);
	}

	UpdateData();

	return TRUE;
}



void CDlgClatok::OnBnClickedButtonSalir()
{
	EndDialog(IDOK);
}



#define BLOCK_IS_CLEAR(b)	 ((*(b) >= 85) && (*(b) <= 169) )
#define BLOCK_IS_EMPTY(b)	 ( *(b) <= 84 )
#define BLOCK_IS_CIPHERED(b) ((*(b) >= 170) && (*(b) <= 254))

#define TOKENS_PER_BLOCK	48	// (10240 - 16) / 213 
#define TOKEN_SIZE			( sizeof(TOKEN_WALLET) )	// 213 bytes

/*! \brief Rellena el list control con los tokens presentes
 *
 * Rellena el list control con los tokens presentes del dispositivo seleccionado
 * actualmente
 */

BOOL CDlgClatok::EnumTokens ( void )
{
	try {

		CString aux;
		CString device;
		clauer_handle_t hClauer;

		if ( ! m_lstTokens.DeleteAllItems() )
			CLERR_THROW(IDS_ERR_CLATOK_CANNOT_INSERT_TOKEN_TO_LIST);

		m_tokens.clear();

		if ( m_cboDevices.GetCount() == 0 )
			return TRUE;

		m_cboDevices.GetLBText(m_cboDevices.GetCurSel(), aux);
		device.Format(_T("%s%s"), m_vDeviceDirs[m_cboDevices.GetCurSel()].GetString(), aux.GetString());

		if ( IO_Open(device.GetString(), &hClauer, IO_RD, 0) != IO_SUCCESS )
			CLERR_THROW(ERR_CLATOK_CANNOT_OPEN_DEVICE);

		block_info_t ib;
		if ( IO_ReadInfoBlock(hClauer, &ib) != IO_SUCCESS )
			CLERR_THROW(ERR_CLATOK_CANNOT_READ_INFORMATION_BLOCK);

		int ret;
		if ( (ret = IO_Seek(hClauer, 0, IO_SEEK_OBJECT)) != IO_SUCCESS )
			CLERR_THROW(ERR_CLATOK_CANNOT_MOVE_TO_OBJECT_ZONE);

		unsigned char block[BLOCK_SIZE], *b;
		TOKEN_WALLET *tw;
		int nItem;			// El item number dentro del list control
		long i, to;			

		nItem = 0;
		b     = block;
		i     = 0;
		to    = ( ib.cb == -1 ) ? ib.totalBlocks : ib.cb;

		while ( i < to ) {
	
			if ( IO_Read(hClauer, block) != IO_SUCCESS )
				CLERR_THROW(ERR_CLATOK_CANNOT_READ);

			if ( ! BLOCK_IS_EMPTY(block) ) {

				if ( *(block+1) == 0x0c ) {

					/* Si el bloque est� cifrado y ya he derivado la password lo descifro,
					 * si no nada
					 */

					if ( BLOCK_IS_CIPHERED(block) && m_clauerKeys[m_cboDevices.GetCurSel()] ) {
						DWORD dwSize;
						dwSize = BLOCK_SIZE-8;
						if ( ! CryptDecrypt(m_clauerKeys[m_cboDevices.GetCurSel()], 0, TRUE, 0, block+8, &dwSize) ) {
							DWORD err = GetLastError();
							CryptDestroyKey(m_clauerKeys[m_cboDevices.GetCurSel()]);
							m_clauerKeys[m_cboDevices.GetCurSel()] = NULL;
							CLERR_THROW(ERR_CLATOK_CANNOT_DECIPHER_BLOCK);
						}
						//++i;
						//continue;
					}

					/* Los bloques de tipo cartera siempre est�n compactos internamente
					 */

					if (  BLOCK_IS_CLEAR(block) || ( BLOCK_IS_CIPHERED(block) && m_clauerKeys[m_cboDevices.GetCurSel()] ) ) {

						for ( int tokenNumber = 0 ; tokenNumber < TOKENS_PER_BLOCK ; tokenNumber++ ) {

							tw = (TOKEN_WALLET *) (block+8+tokenNumber*TOKEN_SIZE);
			
							if ( *(tw->name) == 0 )
								break;

							if ( tw->attributes & 0x02 ) {

								if ( m_lstTokens.InsertItem(nItem, tw->name) == -1 ) 
									CLERR_THROW(ERR_CLATOK_CANNOT_INSERT_TOKEN_TO_LIST);
								if ( ! m_lstTokens.SetItemText(nItem, 1, (char *) tw->condition) )
									CLERR_THROW(ERR_CLATOK_CANNOT_INSERT_TOKEN_TO_LIST);

								m_tokens.push_back(*tw);

								++nItem;
							}
						}

					} // for

				} // if es 0x0c

			} // if es claro

			++i;

		} // while

		if ( nItem == 0 )
			m_lstTokens.SetColumnWidth(0, LVSCW_AUTOSIZE_USEHEADER);
		else
			m_lstTokens.SetColumnWidth(0,LVSCW_AUTOSIZE);

		m_lstTokens.SetColumnWidth(1, LVSCW_AUTOSIZE_USEHEADER);
		m_lstTokens.SetItemState(0, LVIS_SELECTED, LVIS_SELECTED);

	}
	catch ( CClatokErr &err ) {
		CLERR_PROPAGATE(err);
	}

	return TRUE;
}

void CDlgClatok::OnBnClickedButtonAuth()
{
	CString device;
	char szPassword[CLUI_MAX_PASS_LEN];
	int ret, sel;

	try {

		if ( m_cboDevices.GetCount() == 0 ) {
			AfxMessageBox(_T("CLATOK no detect� ficheros CRYF"), MB_ICONINFORMATION);
			return;
		}

		sel = m_cboDevices.GetCurSel();
		if ( sel == -1 ) {
			AfxMessageBox(_T("No se seleccion� dispositivo en la lista de dispositivos"), MB_ICONERROR);
			return;
		}

		CString aux;
		m_cboDevices.GetLBText(m_cboDevices.GetCurSel(), aux);
		device.Format(_T("%s%s"), m_vDeviceDirs[m_cboDevices.GetCurSel()].GetString(), aux.GetString());

		if ( ! m_clauerKeys[m_cboDevices.GetCurSel()] ) {

			while ( 1 ) {

				ret = CLUI_AskGlobalPassphrase(this->m_hWnd, FALSE, FALSE, FALSE, NULL, (char *)device.GetString(), szPassword);
				switch ( ret ) {
				case CLUI_ERR:
					CLERR_THROW(ERR_CLATOK_CANNOT_ASK_FOR_PASSWORD);

				case CLUI_CANCEL:
					return;
				}

				/* Compruebo la validez de la contrase�a
				 */
				
				HCRYPTPROV hProv;
				HCRYPTKEY hKey;
				clauer_handle_t hClauer;
				block_info_t ib;

				if ( IO_Open(device.GetString(), &hClauer, IO_RD, 0) != IO_SUCCESS ) 
					CLERR_THROW(ERR_CLATOK_CANNOT_OPEN_DEVICE);
				
				if ( IO_ReadInfoBlock(hClauer, &ib) != IO_SUCCESS ) {
					IO_Close(hClauer);
					CLERR_THROW(ERR_CLATOK_CANNOT_READ_INFORMATION_BLOCK);
				}

				IO_Close(hClauer);
				
				if ( ! CAPI_PKCS5_3DES_PBE_Init_Ex(szPassword,
												ib.id,
												20,
												1000,
												&m_hProv,
												&(m_clauerKeys[m_cboDevices.GetCurSel()])) )
				{	
					CLERR_THROW(ERR_CLATOK_CANNOT_VERIFY_PASSWORD);
				}

				SecureZeroMemory(szPassword, sizeof szPassword);

				DWORD dwDataLen = 40;
				CryptDecrypt(m_clauerKeys[m_cboDevices.GetCurSel()], 0, TRUE, 0, ib.idenString, &dwDataLen);

				if ( strncmp("UJI - Clauer PKI storage system", (const char *)ib.idenString, 31) != 0 ) {
					CString aux;
					aux.LoadString(IDS_WRONG_PASSWORD);
					CryptDestroyKey(m_clauerKeys[m_cboDevices.GetCurSel()]);
					m_clauerKeys[m_cboDevices.GetCurSel()] = NULL;
					AfxMessageBox(aux.GetString(), MB_ICONERROR);
				}else
					break;
			
			} // while(1)

			/* Reenumeramos la lista de tokens
			 */

			EnumTokens();

			/* Cambio la imagen
			 */

			CStatic *imgHeader = (CStatic *) GetDlgItem(IDC_STATIC_CLATOK_HEADER);
			CButton *btnAuth = (CButton *) GetDlgItem(IDC_BUTTON_AUTH);
			HBITMAP hBmp;
			HICON hBmpButton;

			if ( m_clauerKeys[m_cboDevices.GetCurSel()] ) {
				hBmp = LoadBitmap(theApp.m_hInstance, MAKEINTRESOURCE(IDB_BITMAP_CLATOK_HEADER_AUTH));	
				hBmpButton = LoadIcon(theApp.m_hInstance, MAKEINTRESOURCE(IDI_CANDADO_ABIERTO));
				btnAuth->EnableWindow(FALSE);
			} else {
				hBmp = LoadBitmap(theApp.m_hInstance, MAKEINTRESOURCE(IDB_BITMAP_CLATOK_HEADER));
				hBmpButton = LoadIcon(theApp.m_hInstance, MAKEINTRESOURCE(IDI_CANDADO_CERRADO));
				btnAuth->EnableWindow(TRUE);
			}

			imgHeader->SetBitmap(hBmp);
			btnAuth->SetIcon(hBmpButton);


		}
	}
	catch ( CClatokErr &err ) {
		SecureZeroMemory(szPassword, sizeof szPassword);
		CLERR_SHOW(err);
	}

}




void CDlgClatok::OnCbnSelchangeComboDevices()
{
	EnumTokens();

	/* Cambio la imagen
	 */

	CStatic *imgHeader = (CStatic *) GetDlgItem(IDC_STATIC_CLATOK_HEADER);
	CButton *btnAuth = (CButton *) GetDlgItem(IDC_BUTTON_AUTH);
	HBITMAP hBmp;
	HICON hBmpButton;

	if ( m_clauerKeys[m_cboDevices.GetCurSel()] ) {
		hBmp = LoadBitmap(theApp.m_hInstance, MAKEINTRESOURCE(IDB_BITMAP_CLATOK_HEADER_AUTH));	
		hBmpButton = LoadIcon(theApp.m_hInstance, MAKEINTRESOURCE(IDI_CANDADO_ABIERTO));
		btnAuth->EnableWindow(FALSE);
	} else {
		hBmp = LoadBitmap(theApp.m_hInstance, MAKEINTRESOURCE(IDB_BITMAP_CLATOK_HEADER));
		hBmpButton = LoadIcon(theApp.m_hInstance, MAKEINTRESOURCE(IDI_CANDADO_CERRADO));
		btnAuth->EnableWindow(TRUE);
	}

	imgHeader->SetBitmap(hBmp);
	btnAuth->SetIcon(hBmpButton);
}



void CDlgClatok::OnNMDblclkList1(NMHDR *pNMHDR, LRESULT *pResult)
{
	OnBnClickedButtonAceptar();

	*pResult = 0;
}
