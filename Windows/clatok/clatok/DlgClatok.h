// DlgClatok.h: archivo de encabezado
//

#pragma once
#include "afxwin.h"
#include "afxcmn.h"

#include <wincrypt.h>

#include "TokenWallet.h"

#include <vector>

using namespace std;


// Cuadro de di�logo de CDlgClatok
class CDlgClatok : public CDialog
{
// Construcci�n
public:
	CDlgClatok(CWnd* pParent = NULL);	// Constructor est�ndar

// Datos del cuadro de di�logo
	enum { IDD = IDD_CLATOK_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// Compatibilidad con DDX/DDV


protected:

	HICON m_hIcon;

	HCRYPTPROV m_hProv;
	HCRYPTKEY *m_clauerKeys;

	int m_clauerPassLen;     // Indica el n�mero de elementos del m_clauerPass
	vector<TOKEN_WALLET> m_tokens;
	
	// Funciones de asignaci�n de mensajes generadas

	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	afx_msg void OnBnClickedButtonAceptar();

	BOOL EnumDevices ( void );
	BOOL EnumTokens ( void );

	DECLARE_MESSAGE_MAP()

	// Button to authenticate the currently selected device

	CButton m_btnAuth;

	// Directorios donde se encuentran ubicados los dispositivos. En principio
	// �nicamente admitimos, como mucho, dos directorios (el de trabajo y aquel en el
	// que se encuentra ubicado el programa). El �ltimo elemento del vector debe ser
	// NULL

	vector<CString> m_vDeviceDirs;

public:
	CComboBox m_cboDevices;
public:
	afx_msg void OnBnClickedButtonSalir();
protected:
	CListCtrl m_lstTokens;
public:
	afx_msg void OnBnClickedButtonAuth();
public:
	afx_msg void OnCbnSelchangeComboDevices();
public:
	afx_msg void OnNMDblclkList1(NMHDR *pNMHDR, LRESULT *pResult);
};

