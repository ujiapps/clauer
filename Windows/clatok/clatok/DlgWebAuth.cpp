// DlgWebAuth.cpp : implementation file
//

#include "stdafx.h"
#include "clatok.h"
#include "DlgWebAuth.h"
#include "TokenWallet.h"
#include "urls.h"
#include <Mshtml.h>



#define TIMER_PUSH_BUTTON	1


// CDlgWebAuth dialog

IMPLEMENT_DYNAMIC(CDlgWebAuth, CDialog)

CDlgWebAuth::CDlgWebAuth(CWnd* pParent /*=NULL*/)
	: CDialog(CDlgWebAuth::IDD, pParent)
{
#ifndef _WIN32_WCE
	EnableActiveAccessibility();
#endif

	m_bDownloadComplete = FALSE;
	m_result = NULL;

}

CDlgWebAuth::~CDlgWebAuth()
{
}

void CDlgWebAuth::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_EXPLORER1, m_ie);
}


BOOL CDlgWebAuth::OnInitDialog ( void )
{
	CDialog::OnInitDialog();

	CString caption;
	caption = "CLATOK :: ";
	caption += (char *) m_tok.condition;
	SetWindowText(caption.GetString());

	m_ie.Navigate((char *)m_tok.condition, NULL, NULL, NULL,NULL);

	return TRUE;
}


BEGIN_MESSAGE_MAP(CDlgWebAuth, CDialog)
	ON_BN_CLICKED(IDOK, &CDlgWebAuth::OnBnClickedOk)
	ON_WM_TIMER()
END_MESSAGE_MAP()


// CDlgWebAuth message handlers

void CDlgWebAuth::OnBnClickedOk()
{
	
	//OnOK();
}
BEGIN_EVENTSINK_MAP(CDlgWebAuth, CDialog)
	ON_EVENT(CDlgWebAuth, IDC_EXPLORER1, 259, CDlgWebAuth::DocumentCompleteExplorer1, VTS_DISPATCH VTS_PVARIANT)
END_EVENTSINK_MAP()




void CDlgWebAuth::DocumentCompleteExplorer1(LPDISPATCH pDisp, VARIANT* URL)
{
	m_bDownloadComplete = TRUE;
	SetTimer(TIMER_PUSH_BUTTON, 1000, NULL);
}



const char *bin2hex(unsigned char *bin, int l)
{
	unsigned char *hex;
	char digitos[] = "0123456789ABCDEF";

	hex= (unsigned char *) malloc (l*2+1);

	for (byte i = 0; i < l; i++) {
		hex[2*i]=  digitos[bin[i] >> 4];
		hex[2*i+1]=digitos[bin[i] & 0xf];
	}
	hex[2*l]= 0;
	return (char *)hex;
}

void arc4(char *key, int keySize, unsigned char *msg, int msgSize)
{
	int i, j, l, x;
	char S[256], K[256];

	for (i=0; i<256; i++) {
		S[i] = i;
		K[i] = key[i % keySize];
	}
	for (i=0, j=0; i<256; i++) {
		j = (j + S[i] + K[i]) & 255;
		x = S[i]; S[i] = S[j]; S[j] = x;
	}
	for (i=0 , j=0, l=0; l<msgSize; l++) {
		i = (i + 1) & 255;
		j = (j + S[i]) & 255;
		x = S[i]; S[i] = S[j]; S[j] = x;
		msg[l] = msg[l] ^ S[(S[i] + S[j]) & 255];
	}
}



#define lNom 24
#define lAttr 1
#define lCond 164
#define lTok 24
#define lCred sizeof(TOKEN_WALLET)
#define ntk 10224/sizeof(TOKEN_WALLET)



/* Se ejecuta peri�dicamente para parsear el documento que devuelve la 
 * p�gina
 */

void CDlgWebAuth::OnTimer(UINT_PTR nIDEvent)
{
	USES_CONVERSION;

	if ( (nIDEvent == TIMER_PUSH_BUTTON) && m_bDownloadComplete ) {

		HRESULT hr;
		IHTMLDocument3 *pDoc = NULL;
		IHTMLElement *pElem = NULL;
		LPDISPATCH lpDispatch = NULL;
		BSTR auxStr = NULL, strValue = NULL;
		VARIANT attrValue;
		VARIANT passAutoToken;
		VARIANT challAutoToken;

		lpDispatch = m_ie.get_Document();
		if ( ! lpDispatch ) {
			AfxMessageBox(_T("Imposible obtener documento de la p�gina"), MB_ICONERROR);
			CDialog::OnCancel();
			return;
		}

		hr = lpDispatch->QueryInterface(IID_IHTMLDocument3, (void **) &pDoc);
		if ( ! SUCCEEDED(hr) ) {
			AfxMessageBox(_T("Imposible obtener documento de la p�gina"), MB_ICONERROR);
			CDialog::OnCancel();
			return;
		}

		auxStr = ::SysAllocString(L"authDoneAutoToken");
		if ( ! auxStr ) {
			AfxMessageBox(_T("No hay memoria suficiente para completar la operaci�n"), MB_ICONERROR);
			CDialog::OnCancel();
			return;
		}
		hr = pDoc->getElementById(auxStr, &pElem);
		if ( ! SUCCEEDED(hr) ) {
			AfxMessageBox(_T("No se pudo determinar el env�o de password"), MB_ICONERROR);
			::SysFreeString(auxStr);
			CDialog::OnCancel();
			return;
		}
		::SysFreeString(auxStr);
		auxStr = NULL;

		if ( ! pElem ) {
			AfxMessageBox(_T("No se pudo determinar el env�o de password"), MB_ICONERROR);
			CDialog::OnCancel();
			return;
		}
		strValue = ::SysAllocString(L"value");
		if ( ! strValue ) {
			AfxMessageBox(_T("No hay memoria suficiente para completar la operaci�n"), MB_ICONERROR);
			CDialog::OnCancel();
			return;
		}
		hr = pElem->getAttribute(strValue, 0, &attrValue);
		if ( ! SUCCEEDED(hr) ) {
			AfxMessageBox(_T("No se pudo determinar el env�o de password"), MB_ICONERROR);
			CDialog::OnCancel();
			return;
		}
		::SysFreeString(strValue);
		strValue = NULL;

		if ( attrValue.vt == VT_NULL ) {
			AfxMessageBox(_T("No se pudo determinar el env�o de password"), MB_ICONERROR);
			CDialog::OnCancel();
			return;
		}

		CString aux;
		aux.Format(_T("%s"), attrValue.bstrVal);

		pElem->Release();
		pElem = NULL;

		/* Si se ha enviado la password, lo que hacemos es recoger el reto
		 */

		if ( aux == "1" ) {

			/* Obtengo el reto
			 */

			auxStr = ::SysAllocString(L"challAutoToken");
			if ( ! auxStr ) {
				AfxMessageBox(_T("Imposible obtener reto"), MB_ICONERROR);
				CDialog::OnCancel();
				return;
			}
			hr = pDoc->getElementById(auxStr, &pElem);
			::SysFreeString(auxStr);
			auxStr = NULL;
			if ( SUCCEEDED(hr) && pElem ) {
				
				hr = pElem->getAttribute(strValue, 0, &challAutoToken);
				if ( ! SUCCEEDED(hr) ) {
					AfxMessageBox(_T("Imposible obtener reto\r\nNo se pudo extraer atributo value"), MB_ICONERROR);
					CDialog::OnCancel();
				}
				if (  challAutoToken.vt == VT_NULL ) {
					challAutoToken.vt = VT_BSTR;
					challAutoToken.bstrVal = NULL;
				}
			} else {
				challAutoToken.vt = VT_BSTR;
				challAutoToken.bstrVal = NULL;
			}

			/* urlAutoToken
			 */

			VARIANT urlAutoToken;

			auxStr = ::SysAllocString(L"urlAutoToken");
			if ( ! auxStr ) {
				AfxMessageBox(_T("Imposible obtener URL de retorno"), MB_ICONERROR);
				CDialog::OnCancel();
				return;
			}
			hr = pDoc->getElementById(auxStr, &pElem);
			::SysFreeString(auxStr);
			auxStr = NULL;
			if ( ! SUCCEEDED(hr) || !pElem ) {
				AfxMessageBox(_T("Imposible obtener urlAutoToken"), MB_ICONERROR);
				CDialog::OnCancel();
				return;
			}

			hr = pElem->getAttribute(strValue,0, &urlAutoToken);
			if ( ! SUCCEEDED(hr) ) {
				AfxMessageBox(_T("Imposible obtener urlAutoToken"), MB_ICONERROR);
				CDialog::OnCancel();
				return;
			}
			if ( urlAutoToken.vt == VT_NULL ) {
				AfxMessageBox(_T("Imposible obtener urlAutoToken"), MB_ICONERROR);
				CDialog::OnCancel();
				return;
			}
			if ( ! urlAutoToken.bstrVal ) {
				AfxMessageBox(_T("Imposible obtener URL de retorno\r\nP�gina incorrecta"), MB_ICONERROR);
				CDialog::OnCancel();
				return;
			}

			m_urlRetorno = W2A(urlAutoToken.bstrVal);

			/* Obtengo la password
			 */

			auxStr = ::SysAllocString(L"passAutoToken");
			if ( ! auxStr ) {
				AfxMessageBox(_T("Imposible obtener contrase�a"), MB_ICONERROR);
				CDialog::OnCancel();
				return;
			}
			hr = pDoc->getElementById(auxStr, &pElem);
			::SysFreeString(auxStr);
			auxStr = NULL;
			if ( SUCCEEDED(hr) && pElem ) {

				hr = pElem->getAttribute(strValue, 0, &passAutoToken);
				if ( ! SUCCEEDED(hr) ) {
					AfxMessageBox(_T("Imposible obtener contrase�a"), MB_ICONERROR);
					CDialog::OnCancel();
					return;
				}

				if ( passAutoToken.vt == VT_NULL ) {
					passAutoToken.vt = VT_BSTR;
					passAutoToken.bstrVal = NULL;
				} else if ( passAutoToken.vt != VT_BSTR ) {
					passAutoToken.vt = VT_BSTR;
					passAutoToken.bstrVal = NULL;
				}

			} else {
				passAutoToken.vt = VT_BSTR;
				passAutoToken.bstrVal = NULL;
			}
	
			/* Calculamos el churro que tenemos que enviar
			 */

			char aux[lCred+1];
			BSTR churro;
			int j;

			if (m_tok.attributes > (BYTE)128) {
				unsigned char uniqIdD[16], uniqIdS[16];
				/* No puedo tirar de librt para calcular el identificador hardware (se asume
				 * que no hay runtime ni nada or el estilo, de modo que el c�lculo del identificador
				 * tendr� que hacerlo en alguna funci�n de clatok)
				 *
				 * TODO: De momento se asignan ambos identificadores a cero
				 */
				/*if (LIBRT_ObtenerHardwareId(&hClauer, uniqIdD, uniqIdS) || memcmp(uniqIdD,uniqIdS,16)) {
					*pVal = SysAllocString(L"");
					return S_FALSE;
				}*/
				memset(uniqIdD,0,sizeof uniqIdD);
				memset(uniqIdS,0,sizeof uniqIdS);
				arc4((char *)uniqIdD,sizeof uniqIdD, m_tok.value,lTok);
			}

			if ( passAutoToken.bstrVal ) {
				strncpy(aux, W2A(passAutoToken.bstrVal), lTok);
				aux[lTok]=0; // por si pwd tiene long=lTok
			} else
				*aux = 0;

			if (aux[0] != 0)
				arc4(aux, strlen(aux), m_tok.value, lTok);

			if ( challAutoToken.bstrVal ) {
				strncpy(aux, W2A(challAutoToken.bstrVal), lCred);
				aux[lCred]=0; j=strlen(aux);
			} else
				*aux = 0;

			if (aux[0] != 0) {
				arc4((char *)m_tok.value,lTok,(unsigned char*)aux,j);
				m_result = SysAllocString(A2W(bin2hex((unsigned char*)aux,j)));
			}
			else
				m_result = SysAllocString(A2W(bin2hex(m_tok.value,lTok)));

			char *p = (char *) bin2hex(m_tok.value,lTok);

			if ( ! m_result ) {
				AfxMessageBox(_T("No se pudo calcular resultado\r\nNo hay suficiente memoria"), MB_ICONERROR);
				CDialog::OnCancel();
			}

			CDialog::OnOK();
		
#if 0
			if (tok->attr > (BYTE)128) {
				unsigned char uniqIdD[16], uniqIdS[16];
				if (LIBRT_ObtenerHardwareId(&hClauer, uniqIdD, uniqIdS) || memcmp(uniqIdD,uniqIdS,16)) {
					*pVal = SysAllocString(L"");
					return S_FALSE;
				}
				arc4((char *)uniqIdD,sizeof(uniqIdD),tok->tok,lTok);
			}
			LIBRT_FinalizarDispositivo(&hClauer);

			strncpy(aux, W2A(pwd), lTok);
			aux[lTok]=0; // por si pwd tiene long=lTok
			if (aux[0] != 0)
				arc4(aux,strlen(aux),tok->tok,lTok);

			strncpy(aux, W2A(chal), lCred);
			aux[lCred]=0; j=strlen(aux);
			if (aux[0] != 0) {
				arc4((char *)tok->tok,lTok,(unsigned char*)aux,j);
				*pVal = SysAllocString(A2W(bin2hex((unsigned char*)aux,j)));
			}
			else
				*pVal = SysAllocString(A2W(bin2hex(tok->tok,lTok)));
#endif
		} else 
			SetTimer(TIMER_PUSH_BUTTON, 1000, NULL);
	}

	CDialog::OnTimer(nIDEvent);
}




void CDlgWebAuth::SetToken ( TOKEN_WALLET tok )
{
	m_tok = tok;
}




BSTR CDlgWebAuth::GetResult ( void )
{
	return m_result;
}



CString CDlgWebAuth::GetURL ( void )
{
	return m_urlRetorno;
}