#pragma once

/* Este struct define la estructura de la cartera de
 * tokens
 */

struct TOKEN_WALLET {
	char name[24];
	BYTE attributes;
	BYTE condition[164];
	BYTE value[24];
};
