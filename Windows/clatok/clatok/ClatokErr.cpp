#include "StdAfx.h"
#include "ClatokErr.h"

#include "DlgError.h"

/*! \brief Asigna valores por defecto a los atributos de la clase.
 *
 * Asigna valores por defecto a los atributos de la clase. En particular
 *    -# m_errCode queda inicializado a cero
 *    -# m_mfcException queda inicializado a NULL
 */

CClatokErr::CClatokErr(void)
{
	m_errCode       = 0;
	m_mfcException = NULL;
}

/*! \brief Borra la excepci�n mfc si la hubiere
 *
 * Borra la excepci�n mfc si la hubiere
 */

CClatokErr::~CClatokErr(void)
{
	if ( m_mfcException )
		m_mfcException->Delete();
}

/*! \brief Construye un objeto inicializando con los atributos especificados
 *
 * Construye un objeto inicializando con los atributos especificados
 *
 * \param errCode
 *        C�digo del error lanzado
 *
 * \param mfcException
 *        Excepci�n mfc en caso de que se necesite
 *
 * \param lineNumber
 *        N�mero de l�nea donde se lanz� la excepci�n
 *
 * \param szFileName
 *        Cadena ASCII que contiene el nombre del fichero donde se lanz�
 *        la excepci�n
 *
 * \param szFuncName
 *        Cadena ASCII que contiene el nombre de la funci�n desde donde
 *        se lanz� la excepci�n
 */

CClatokErr::CClatokErr ( int errCode, 
		                 CException *mfcException /*= NULL*/, 
				         long lineNumber /*=0*/, 
				         const char *szFileName /*= NULL*/,
				         const char *szFuncName /*= NULL*/ )
{
	m_errCode      = errCode;
	m_mfcException = mfcException;
	Push(lineNumber, szFileName, szFuncName);
}

/*! \brief A�ade nuevos datos a la pila de llamadas del objeto
 *
 * A�ade nuevos datos a la pila de llamadas del objeto. �til cuando queremos
 * propagar una excepci�n y seguirle el rastro a la llamada.
 *
 * \param lineNumber
 *        N�mero de l�nea donde se lanz� la excepci�n
 *
 * \param szFileName
 *        Cadena ASCII que contiene el nombre del fichero donde se lanz�
 *        la excepci�n
 *
 * \param szFuncName
 *        Cadena ASCII que contiene el nombre de la funci�n desde donde
 *        se lanz� la excepci�n
 */

void CClatokErr::Push ( long lineNumber, const char *szFileName, const char *szFuncName )
{
	STStackInfo stInfo;
	stInfo.lineNumber = lineNumber;
	strncpy(stInfo.szFileName, szFileName, ERR_MAX_FILE_NAME_LEN);
	stInfo.szFileName[ERR_MAX_FILE_NAME_LEN-1] = 0;
	strncpy(stInfo.szFuncName, szFuncName, ERR_MAX_FUNC_NAME_LEN);
	stInfo.szFuncName[ERR_MAX_FUNC_NAME_LEN-1] = 0;

	m_callStack.push(stInfo);
}



void CClatokErr::Show ( CWnd *pParent /*=NULL*/ )
{
	CString pilaLlamadas, descripcion;
	
	if ( ! descripcion.LoadString(m_errCode) )
		descripcion = "Descrition not available";
	
	/* Ahora constru�mos la pila de llamadas
	 */

	while ( ! m_callStack.empty()) {
		STStackInfo llamada;
		CString aux;

		llamada = m_callStack.top();
		aux.Format(_T("%ld::%s::%s\r\n"), llamada.lineNumber, llamada.szFuncName, llamada.szFileName);
		pilaLlamadas += aux;

		m_callStack.pop();
		if ( m_callStack.size() == 2 )
			m_callStack.pop();
	}

	CDlgError errDlg(m_errCode, descripcion, pilaLlamadas, pParent);
	errDlg.DoModal();
}

