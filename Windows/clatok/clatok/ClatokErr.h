#pragma once

#include <stack>

#define CLERR_THROW(errCode)   (throw CClatokErr(errCode,NULL,__LINE__,__FILE__,__FUNCTION__))
#define CLERR_PROPAGATE(err)   err.Push(__LINE__,__FILE__,__FUNCTION__); throw err
#define CLERR_SHOW(err)		   err.Push(__LINE__,__FILE__,__FUNCTION__); err.Show()

#define ERR_MAX_FILE_NAME_LEN	100
#define ERR_MAX_FUNC_NAME_LEN	100

class CClatokErr
{
protected:
	int m_errCode;				  // C�digo de error producido

	CException *m_mfcException;   // Una excepci�n lanzada por las MFCs
	                              // S�lo tiene sentido cuando errCode vale
								  // ERR_CLATOK_MFC_EXCEPTION

	struct STStackInfo {
		long lineNumber;
		char szFileName[ERR_MAX_FILE_NAME_LEN];
		char szFuncName[ERR_MAX_FUNC_NAME_LEN];
	};

	std::stack<STStackInfo> m_callStack;	// La pila de llamadas hasta que se
										    // trata la excepci�n


public:

	CClatokErr (void);

	CClatokErr (int errCode, 
		        CException *mfcException = NULL, 
				long lineNumber=0, 
				const char *szFileName = NULL,
				const char *szFuncName = NULL);

	~CClatokErr (void);

	void Push ( long lineNumber, const char *szFileName, const char *szFuncName );
	void Show ( CWnd *pParent = NULL );
};

/* C�digos de error posibles
 *
 * ATENCI�N Los valores de las constantes TIENEN que ser iguales a las
 * correspondientes valores del string table.
 *
 */

#define ERR_CLATOK_OUT_OF_MEMORY					5000
#define ERR_CLATOK_MFC_EXCEPTION					5001
#define ERR_CLATOK_COULD_NOT_DETERMINE_CRYF_FILES	5002
#define ERR_CLATOK_CANNOT_INITIALIZE_DIALOG			5003
#define ERR_CLATOK_CANNOT_OPEN_DEVICE				5004
#define ERR_CLATOK_CANNOT_READ_INFORMATION_BLOCK	5005
#define ERR_CLATOK_CANNOT_MOVE_TO_OBJECT_ZONE		5006
#define ERR_CLATOK_CANNOT_READ						5007
#define ERR_CLATOK_CANNOT_INSERT_TOKEN_TO_LIST		5008
#define ERR_CLATOK_CANNOT_ASK_FOR_PASSWORD			5009
#define ERR_CLATOK_CANNOT_VERIFY_PASSWORD			5010
#define ERR_CLATOK_WRONG_PASSWORD					5011
#define ERR_CLATOK_CANNOT_DECIPHER_BLOCK			5012

