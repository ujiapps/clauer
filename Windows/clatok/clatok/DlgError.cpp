// DlgError.cpp : implementation file
//

#include "stdafx.h"
#include "clatok.h"
#include "DlgError.h"


// CDlgError dialog

IMPLEMENT_DYNAMIC(CDlgError, CDialog)

CDlgError::CDlgError(CWnd* pParent /*=NULL*/)
	: CDialog(CDlgError::IDD, pParent)
{
}


CDlgError::CDlgError(int codigo, CString descripcion, CString pilaLlamadas, CWnd *pParent /*= NULL*/)
	: CDialog(CDlgError::IDD, pParent)
{
	CString strCodigo;

	m_codigo.Format(_T("%d"), codigo);
	m_descripcion = descripcion;
	m_pilaLlamadas = pilaLlamadas;
	
}


CDlgError::~CDlgError()
{
}

void CDlgError::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}

BOOL CDlgError::OnInitDialog ( void )
{
	/* Realizamos la traducción de las etiquetas
	 */

	try {

		CString aux;

		CWnd *item = GetDlgItem(IDC_STATIC_LBL_CODIGO);
		if ( ! item )
			return FALSE;
		if ( ! aux.LoadString(IDS_DLG_ERR_DIALOG_LBL_CODIGO) )
			return FALSE;
		item->SetWindowText(aux.GetString());

		item = GetDlgItem(IDC_STATIC_LBL_DESCRIPCION);
		if ( ! item )
			return FALSE;
		if ( ! aux.LoadString(IDS_DLG_ERR_DIALOG_LBL_DESCRIPCION) )
			return FALSE;
		item->SetWindowText(aux.GetString());

		item = GetDlgItem(IDC_STATIC_LBL_PILA_LLAMADAS);
		if ( ! item )
			return FALSE;
		if ( ! aux.LoadString(IDS_DLG_ERR_DIALOG_LBL_PILA_LLAMADAS) )
			return FALSE;
		item->SetWindowText(aux.GetString());

		item = GetDlgItem(IDC_STATIC_LBL_DESC_DIALOG);
		if ( ! item )
			return FALSE;
		if ( ! aux.LoadString(IDS_DLG_ERR_DIALOG_LBL_DESC_DIALOG) )
			return FALSE;
		item->SetWindowText(aux.GetString());



		item = GetDlgItem(IDC_STATIC_CODIGO);
		if ( ! item ) 
			throw 1;
		item->SetWindowText(m_codigo.GetString());

		item = GetDlgItem(IDC_EDIT_DESCRIPCION);
		if ( ! item )
			throw 1;
		item->SetWindowText(m_descripcion.GetString());

		item = GetDlgItem(IDC_EDIT_PILA_LLAMADAS);
		if ( ! item )
			throw 1;
		item->SetWindowText(m_pilaLlamadas);
	}
	catch ( DWORD i ) {
		AfxMessageBox(_T("Clatok Error. Cannot construct Error Dialog"), MB_ICONERROR);
	}

	UpdateData();


	return TRUE;
}


BEGIN_MESSAGE_MAP(CDlgError, CDialog)
END_MESSAGE_MAP()


// CDlgError message handlers
