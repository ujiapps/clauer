#pragma once

#include "resource.h"

// CDlgError dialog

class CDlgError : public CDialog
{
	DECLARE_DYNAMIC(CDlgError)

public:
	CDlgError(CWnd* pParent = NULL);   // standard constructor
	CDlgError(int codigo, CString descripcion, CString pilaLlamadas, CWnd *pParent = NULL);
	virtual ~CDlgError();

// Dialog Data
	enum { IDD = IDD_CLATOK_ERR_DIALOG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL OnInitDialog ( void );

	CString m_codigo, m_descripcion, m_pilaLlamadas;

	DECLARE_MESSAGE_MAP()
};


