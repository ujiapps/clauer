UNIVERSITAT JAUME I � CONDICIONES DE USO, COPIA Y DISTRIBUCI�N DEL SOFTWARE DEL PROYECTO CLAUER.


1.Este programa puede ser ejecutado sin ninguna restricci�n por parte del usuario final del mismo.

2.La Universitat Jaume I autoriza la copia y distribuci�n del programa con fines no comerciales por 
cualquier medio con la �nica limitaci�n de que, de forma apropiada, se haga constar en cada una de 
las copias la autor�a de esta Universidad y una reproducci�n exacta de las presentes condiciones y 
de la declaraci�n de exenci�n de responsabilidad.

3.El hecho en s� del uso, copia o distribuci�n del presente programa implica la aceptaci�n de estas 
condiciones.

4.Cualquier distribuci�n del programa para usos comerciales o empresariales requerir� la autorizaci�n 
expresa de la Universitat Jaume I.

5.La copia y distribuci�n del programa supone la extensi�n de las presentes condiciones al destinatario.
El distribuidor no puede imponer condiciones adicionales que limiten las aqu� establecidas.


DECLARACI�N DE EXENCI�N DE RESPONSABILIDAD
Este programa se distribuye gratuitamente. La Universitat Jaume I no ofrece ning�n tipo de garant�a sobre
el mismo ni acepta ninguna responsabilidad por su uso o imposibilidad de uso.

"This product includes software developed by the OpenSSL Project
for use in the OpenSSL Toolkit (http://www.openssl.org/)"
Copyright (c) 1998-2005 The OpenSSL Project.  All rights reserved. 