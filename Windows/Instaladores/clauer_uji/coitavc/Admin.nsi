# set the name of the installer
outfile "Admin.exe"
SilentInstall silent
AutoCloseWindow true
ShowInstDetails nevershow
# default section start; every NSIS script has at least one section.
section
  UserInfo::GetAccountType
  Pop $0
  StrCmp $0 "Admin" Exit Fin
Fin:
 Abort
Exit:

# default section end
sectionEnd