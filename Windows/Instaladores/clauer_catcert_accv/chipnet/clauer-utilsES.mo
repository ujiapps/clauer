��    �      �  �   l      0  	   1     ;     I     V     k  '   �  1   �     �  #   �  .      >   O  J   �  <   �  5     C   L  F   �  G   �  C     ^   c  8   �  ;   �  <   7  <   t  3   �  1   �  6     5   N  :   �  ?   �  D   �  C   D  n   �  @   �  n   8  s   �  r     8   �  C   �  B        N  '   h  9   �     �     �           "     C  <   c  >   �  6   �  =     "   T  D   w      �  &   �  3        8     M     l     �  !   �  "   �     �     �           #     D     b     r     ~     �     �  (   �     �     
           :     N     l     �     �  "   �     �  4   �     0     I     W     g  ?   n     �     �     �     �  %   �  !     +   =  0   i     �     �  "   �     �     �              9   ,   '   f   ;   �   F   �   #   !  0   5!  P   f!     �!  Y   �!  I   !"  2   k"     �"  N   �"  .   #  '   ;#  0   c#  o   �#  )   $  2   .$  6   a$  !   �$  %   �$     �$     �$  (   %  &   C%  #   j%  #   �%  #   �%     �%  &   �%  4   &     N&  F   g&  6   �&  K   �&  +   1'     ]'     {'     �'     �'     �'  X   �'  &   B(  Z   i(  I   �(  ?   )  G   N)  $   �)  -   �)  $   �)  ,   *  &   ;*  "   b*  #   �*  >   �*  .   �*  1   +  *   I+  3   t+  3   �+  +   �+  &   ,  9   /,  )   i,  &   �,  R   �,  H   -  .   V-  B   �-  4   �-  M   �-  -   K.     y.  �   �.     L/  a  g/  	   �0     �0     �0     �0     1  '   !1  5   I1     1  '   �1  1   �1  E   �1  H   ;2  :   �2  :   �2  H   �2  O   C3  E   �3  B   �3  U   4  6   r4  9   �4  :   �4  <   5  7   [5  6   �5  ;   �5  ;   6  =   B6  C   �6  C   �6  C   7  �   L7  D   �7  y   8  }   �8  }   9  :   �9  P   �9  <   :     V:  '   r:  6   �:     �:     �:  !   ;  #   0;      T;  9   u;  ?   �;  5   �;  A   %<  (   g<  K   �<  "   �<  (   �<  8   (=     a=     w=     �=     �=  #   �=      �=     >     >     9>  !   M>     o>     �>     �>     �>     �>     �>  -   �>     -?     C?      R?     s?     �?     �?     �?  !   �?  &   @  &   3@  )   Z@  "   �@     �@     �@     �@  E   �@      A     .A     CA     `A  /   rA  #   �A  5   �A  2   �A     /B     KB  &   \B     �B     �B     �B     �B  D   �B  -   #C  A   QC  L   �C  $   �C  9   D  Y   ?D     �D  _   �D  E   	E  -   OE  %   }E  T   �E  4   �E  .   -F  0   \F  }   �F  2   G  7   >G  8   vG  "   �G  .   �G     H     H  5   =H  &   sH  "   �H  "   �H  *   �H  $   I  +   0I  0   \I     �I  >   �I  ;   �I  P   &J  0   wJ  #   �J     �J  #   �J     K     !K  h   ?K  "   �K  f   �K  K   2L  A   ~L  L   �L  &   M  ;   4M  '   pM  3   �M  *   �M  *   �M  %   "N  A   HN  2   �N  5   �N  ,   �N  M    O  M   nO  2   �O  ,   �O  D   P  2   aP  *   �P  f   �P  V   &Q  5   }Q  P   �Q  7   R  T   <R  6   �R  '   �R  �   �R     �S            `   �       Q   �   ?   )   E   .   9   i   �   &          m      /                 k   L   d   �   �   �           B   [   ^   �   �       <   2       �   F   H   7       �   !   �   |                      \       �              z   �                             8   0   v   �   y       �   (   
   �   	   =   �   p   O       N               �   e   }       �          5   a   A   +       �   J   ;   w   �   S          $   �   4           �   �   �              K   �   �   �   b           �   t   �      X               c   �   R       �   h   �   �   o   P      r   ~   �   C   "   g   �   �   q   �   %   �      �   �   D   �          f           I           �   '       _   V             W   s          Z   j   x   #   n          �      >       @   :   T   �   l   �   -                   6       ]      1   u   �   M             �   U   G              �   �      ,   �       �   3   {         Y   *       �      �    	%22.22s  	%22.22s %ld
 	%22.22s %s
 	Clauer's password:  	Type the password for %s: 	[ERROR] Inserting the certificate: %s
 	[ERROR] Unable to guess the size of the file %s
 	[ERROR] Unable to open %s
 	[ERROR] Unable to open the device
 	[ERROR] Unable to read the information block
 	[ERROR] You have to be permission (admin) to open the device
 
The identifier NUM is the first number than appears when executing clls

     -d, --device            Selects the clauer to be listed
     -h, --help              Prints this help message
     -l, --list              List the clauers plugged on the system
     If the password is not indicated it will be prompted with getpass
    -b, --block NUM          Deletes the block number NUM from Clauer.

    -c, --CA                  imports a intermediate CA certificate
    -c, --certificate NUM    Deletes the certificate and their associated blocks in the Clauer
    -d,  --device        Selects the clauer to be listed
    -d, --device              Selects the clauer to be used
    -d, --device             Selects the clauer to be listed
    -d, --device DEVICE      Selects the clauer to work with
    -f, --fp12                imports a pkcs12 file
    -h,  --help          Prints this help message
    -h, --help                Prints this help message
    -h, --help               Prints this help message
    -i, --import-password     Password for the pkcs12 file
    -l,  --list          List the clauers plugged on the system
    -l, --list                List the clauers plugged on the system
    -l, --list               List the clauers plugged on the system
    -np, --new-password  Insecure mode of give the password, if not given, the password is asked with getpass.
    -o, --other               imports a other person certificate
    -op, --old-password  Insecure mode of give the password, if not given, the password is asked with getpass.
    -p, --password password   Insecure mode of give the password, if not given, the password is asked with getpass.
    -p, --password password  Insecure mode of give the password, if not given, the password is asked with getpass.
    -r, --root                imports a root certificate
    -t TYPE, --type TYPE     Lists all the objects of a given type

    -y, --yes                No ask for confirmation when deleting
    ALL.  All the objects
    CA.   For intermediate certificates
    CERT. For certificates with an associated private key
    CONT. For key containers
    PRIV. For private keys
    ROOT. For root certificates
    TOK.  For the token's wallet
    WEB.  For web certificates.
   -b, --block block   exports the object in the block block
   -c, --crypto        dumps the whole criptographic partition
   -d, --device        Selects the clauer to be listed
   -l, --list          List the clauers plugged on the system
   -o, --out file      output file
   -p, --p12 block     exports a pkcs#12 with the given block number
 -b o --block must be given alone -c o --certfiicate must be given alone -l o --list must be given alone. Invalid option: %s Are you sure (y/n)?  Block %d successfully deleted
 Changes the Clauer's password

 Clauer's password:  Clauers inserted on the system:

 Container %s successfully deleted
 Current Block: Delete blocks from Clauer

 Deleted block:     Elements that are no options: %s Exports objects from Clauer

 Format version: Identifier: Importing Certificates...
 Importing PKCS12...
 Imports objects to the Clauer

 Impossible to get certificate's subject
 Invalid option: %s Key Containers List the objects in the Clauer

 M$ Private Key Blob More than one password given
 New Clauer's password:  New password (confirmation):  No block to delete specified No certificate to delete specified No device name specified No more options allowed when -h or --help are given
 No object type specified Options are:
 Own Certificate Owner: Prints the information block content of the specified clauers

 Private Key Repeat the password:  Reserved zone size: Root Certificate TYPE can be one of the next values:

 The block is empty!, exiting ...
 The password is longer than 127 characters
 The passwords do not  match, write them again!.
 The possible options are:
 Total Blocks: Unable to generate random numbers
 Unable to guess it Unknown Unknown object type: %s Unknown option: %s Usage Mode: clexport OPTIONS {block num of certificate}?
 Usage mode: %s -d device [-p password]
 Usage mode: cldel [-h] | [-d device]  ( -b NUM | -c NUM ) 
 Usage mode: climport -h | -l | [-d device] ([-p pkcs12]* [-c cert]*)*
 Usage mode: clls [-h] | (-t TYPE)*
 Usage mode: clpasswd -h | -l | -d (device|file)
 Usage mode: clview (-h|--help) | (-l|--list) | (-d|--device (device|file|ALL))+
 Web Certificate Without arguments, it prints  the information block of all clauers plugged on the system
 You are going to delete all the blocks associated with the certificate:
	 You are going to delete the block that contains:
	 You can only select one device [ATENTION] More than a Clauer plugged on the system.
           Use -d option
 [ATENTION] No Clauers detected on the system.
 [ERROR] %s is not a valid block number
 [ERROR] An error happened changing the password
 [ERROR] An error happened when dumping crypto partition
        The dumped file countains an invalid partition
 [ERROR] Associated private key not found
 [ERROR] Block %ld contains an unexportable object
 [ERROR] Block %ld does not contains a own certificate
 [ERROR] Ciphering the identifier. [ERROR] Closing Clauer's connectiono
 [ERROR] Deleting block = %d
 [ERROR] Deleting block = %d  [ERROR] Device name longer than allowed
 [ERROR] Enumerating blocks of type %s
 [ERROR] Enumerating key Containers
 [ERROR] Enumerating key containers
 [ERROR] Error getting confirmation
 [ERROR] Getting size of %s
 [ERROR] Impossible to finalize device
 [ERROR] Impossible to get the CN of the certificate
 [ERROR] Listing devices
 [ERROR] Looking for the private key associated to the certificate %ld
 [ERROR] Maximum number of exportable objects exceeded
 [ERROR] More than a Clauer plugged on the system.
           Use -d option
 [ERROR] No Clauers detected on the system.
 [ERROR] No memory available 
 [ERROR] Opening %s
 [ERROR] Passwords do not match
 [ERROR] Reading from Clauer
 [ERROR] Reading the file %s
 [ERROR] This password has less future than Chewaka on a depilation challenge. Try again
 [ERROR] Unable to build up the pkcs12
 [ERROR] Unable to connect with the Clauer %s
        Correct device? Try to use -l option
 [ERROR] Unable to connect with the Clauer %s
        Incorrect password?
 [ERROR] Unable to connect with the Clauer. Incorrect password?
 [ERROR] Unable to delete the associated key entry on the key container
 [ERROR] Unable to enumerate objects
 [ERROR] Unable to get Clauers on the system.
 [ERROR] Unable to get the block %ld
 [ERROR] Unable to get the information block
 [ERROR] Unable to get the new password [ERROR] Unable to get the password [ERROR] Unable to get the password
 [ERROR] Unable to import the certificate, incorrect password?
 [ERROR] Unable to open/create the output file
 [ERROR] Unable to open/create the output file %s
 [ERROR] Unable to read the specified block [ERROR] Unable to unlock newPass memory errno= %d.
 [ERROR] Unable to unlock oldPass memory errno= %d.
 [ERROR] Unable to write in the output file
 [ERROR] Unable to write on the device
 [ERROR] Unknown block type, Really is it a Certificate? 
 [ERROR] Unknown error opening the device
 [ERROR] Writing changes to the Clauer
 [ERROR] You have to had permission (admin) to open the device to dump crypto zone
 [WARNING] Maximum number of certificates exceeded. Next will be ignored
 [WARNING] Maximum number of devices execeeded
 [WARNING] Maximum number of pkcs12 exceeded. Next will be ignored
 [WARNING] More than a Clauer plugged on the system.
 [WARNING] More than a Clauer plugged on the system.
           Use -d option
 [WARNING] No Clauers detected on the system.
 [WARNING] Unknown block type
 [WARNING] You have given more than a pkcs12 file with --import-password option or -i
           the password for all the pkcs12 files must be the same if this option is indicated
. pkcs12's password for %s:  Project-Id-Version: clauer-utils 0.1
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2006-08-18 12:58+0200
PO-Revision-Date: 2006-08-18 13:03+0200
Last-Translator: paul <psantapau@sg.uji.es>
Language-Team: Spanish <es@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
 	%22.22s  	%22.22s %ld
 	%22.22s %s
 	Password del Clauer 	Escribe el password para %s: 	[ERROR] Insertando el certificado: %s
 	[ERROR] Imposible obtener el tamaño del fichero %s
 	[ERROR] No puedo abrir %s
 	[ERROR] No puedo abrir el dispositivo
 	[ERROR] No puedo leer el bloque de información
 	[ERROR] Tienes que tener permisos (admin) para abrir el dispositivo
 
El identificador NUM es el primer numero que aparace al ejecutar clls

     -d, --device            Selecciona el clauer a listar
     -h, --help              Imprime este mensaje de ayuda
     -l, --list              Lista los caluaers insertados en el sistema
     Si no se indica el password, será solicitado mediante la función getpass
    -b, --block NUM          Borra el bloque número NUM del Clauer.

    -c, --CA                  importa un certificado CA intermedio
    -c, --certificate NUM    Borra el certificado y sus bloquesasociados en el Clauer
    -d,  --device        Selecciona el Clauer a listar
    -d, --device              Selecciona el clauer a usar
    -d, --device             Selecciona el clauer a listar
    -d, --device DEVICE      Selecciona el clauer a utilizar
    -f, --fp12                importa un fichero pkcs12
    -h,  --help          Imprime este mensaje de ayuda
    -h, --help                Imprime este mensaje de ayuda
    -h, --help                Imprime este mensaje de ayuda
    -i, --import-password     Password para el fichero pkcs12
    -l,  --list          Lista los clauers conectados a la máquina
    -l,  --list          Lista los clauers conectados a la máquina
    -l,  --list          Lista los clauers conectados a la máquina
    -np, --new-password  Modo inseguro de indicar la password, si no se indica, el password se solicita mediante la función getpass.
    -o, --other               importa un certificado de otra persona
    -op, --old-password  Modo inseguro de indicar la password, si no se indica, el password se solicita mediante getpass.
    -p, --password password   Modo inseguro de indicar la password, si no se indica la password se solicita mediante getpass.
    -p, --password password   Modo inseguro de indicar la password, si no se indica la password se solicita mediante getpass.
    -r, --root                importa un certificado raíz
    -t TYPE, --type TYPE     Lista todos los objetos del tipo indicado con TYPE

    -y, --yes                No pide confirmaicón al borrar
    ALL.  Todos los objetos
    CA.   Para certificados intermedios
    CERT. Para certificados con llave privada asociada
    CONT. Para key containers
    PRIV. Para llaves privadas
    ROOT. Para certificados raíz
    TOK.  Para la cartera de tokens
    WEB.  Para certificados web.
   -b, --block block   exporta el objeto del bloque block
   -c, --crypto        vuelca toda la partición criptográfica
   -d, --device        Selecciona ael clauer a listar
   -l, --list          Lista los clauers conectados en el sistema
   -o, --out file      fichero de salida
   -p, --p12 block     exporta un pkcs#12 con el número de bloque indicado
 -b o --block deben indicarse solas -c o --certfiicate deben indicarse solas -l o --list deben indicarse solas. Opción inválida: %s ¿Esta seguro (y/n)?  Bloque %d borrado con éxito
 Cambia el password del Clauer

 Password del Clauer:  Clauers insertados en el sistema:

 Container %s borrado con éxito
 Bloque actual: Borra bloque del Clauer

 Borrado bloque:     Elementos que no son opciones: %s Exporta objetos del Clauer

 Versión del formato: Identificador: Importando certificados...
 Importando PKCS12...
 Importa objetos al Clauer

 Imposible obtener el subject del certificado
 Opción inválida: %s Key Containers Lista los objetos en el Clauer

 M$ Blob de llave privada Se indico más de un password
 Nueva password para el Clauer:  Nueva password (confirmación) No se especificó bloque a borrar No se especificó certificado a borrar No se especifico nombre de dispositivo No se admiten opciones junto -h o --help
 No se especifico el tipo de objeto Las opciones son:
 Certificado propio Propietario: Imprime el contenido del bloque de información del Clauer indicado

 Llave privada Repite el password:  Tamaño de la zona reservada Certificado Raíz TIPO puede ser uno de los siguientes valores:

 El bloque está vacio, saliendo...
 La longitud del password es mayor que 127 caracteres
 Los passwords no coindiden, vuelva a teclearlos!.
 Las opciones posibles son:
 Bloques totales: Imposible generar números aleatorios
 No se ha podido obtener Desconocido Tipo de bloque desconocido: %s Opción desconocida: %s Modo de uso: clexport OPCIONES {número de bloque del certificado}?
 Modo de uso: %s -d dispositivo [-p password]
 Modo de uso: cldel [-h] | [-d dispositivo]  ( -b NUM | -c NUM ) 
 Modo de uso: climport -h | -l | [-d dispositivo] ([-p pkcs12]* [-c cert]*)*
 Modo de uso: clls [-h] | (-t TIPO)*
 Modo de uso: clpasswd -h | -l | -d (dispositivo|fichero)
 Modo de uso: clview (-h|--help) | (-l|--list) | (-d|--device (disopsitivo|fichero|ALL))+
 Certificado Web Sin argumentos, imprime el bloque de información de todos los clauers conectadosen el sistema
 Se dispone a borrar todos los bloques asociados con el certificado:
	 Se dispone a borrar el bloque que contiene:
	 Solo puede seleccionar un dispositivo [Atención] Más de un clauer conectado en el sistema.
           Use la opción -d
 [Atención] No se detectaron Clauers en el sistema.
 [ERROR] %s no es un número de bloque válido
 [ERROR] Ocurrió un error cambiando la password
 [ERROR] Ocurrió un error volcando la partición criptográfica
        el fichero volcado contiene una partición inválida
 [ERROR] No se encontró la llave privada asociada
 [ERROR] El bloque %ld contiene un bloque no exportable
 [ERROR] El bloque %ld no contiene un certificado propio
 [ERROR] Cifrando el identificador. [ERROR] Terminando la conexión con el Clauer
 [ERROR] Borrando bloque = %d
 [ERROR] Borrando bloque = %d  [ERROR] Nombre del dispositivo mayor de lo permitido
 [ERROR] Enumerando bloques de typo %s
 [ERROR] Enumerando key Containers
 [ERROR] Enumerando key containers
 [ERROR] Error obteniendo la confirmación
 [ERROR] Obteniendo el tamaño de %s
 [ERROR] Imposible finalizar el dispositivo
 [ERROR] Imposible obtener el CN del certificado
 [ERROR] Listado dispositivos
 [ERROR] Buscando la llave privada asociada al certificado %ld
 [ERROR] Excedido el máximo número de objetos exportables
 [ERROR] Más de un Clauer conectado en el sistema.
           Use la opción -d
 [ERROR] No se detectaron Clauers en el sistema.
 [ERROR] No hay memoria disponible 
 [ERROR] Abrinedo %s
 [ERROR] Los passwords no coinciden
 [ERROR] Leyendo del Clauer
 [ERROR] Leendo el fichero %s
 [ERROR] Este password tiene menos futuro que Chewaka en un concurson de depilación. Intentalo de nuevo
 [ERROR] Imposible crear el pkcs12
 [ERROR] Imposible conectar con el Clauer %s
        ¿dipositivo correcto? Intenta usar la opción -l
 [ERROR] Imposible conectar con el Clauer %s
        ¿password incorrecto?
 [ERROR] Imposible conectar con el Clauer. ¿Password incorrecto?
 [ERROR] No puedo obtener la entrada asociada a la llave en el key container
 [ERROR] No puedo enumerar los objetos
 [ERROR] No puedo obtener los Clauer conectados al sistema.
 [ERROR] No puedo obtener el bloque %ld
 [ERROR] No puedo obtener el bloque de información
 [ERROR] No puedo obtener la nueva password [ERROR] No puedo obtener la nueva password [ERROR] No puedo obtener la password
 [ERROR] No puedo importar el certificado, ¿password incorrecta?
 [ERROR] No puedo abrir/crear el fichero de salida
 [ERROR] no puedo abrir/crear el fichero de salida %s
 [ERROR] No puedo leer el bloque especificado [ERROR] no puedo desbloquear la memoria correspondiente a newPass errno= %d.
 [ERROR] No puedo desbloquear la memoria correspondiente a oldPass errno= %d.
 [ERROR] No puedo escribir en el fichero de salida
 [ERROR] No puedo escribir en el dispositivo
 [ERROR] Tipo de bloque desconocido, ¿Es realmente un certificado? 
 [ERROR] Error desconocido abriendo el dispositivo
 [ERROR] Escribiendo los cambios al Clauer
 [ERROR] Tienes que tener permiso (admin) para abrir el dispositivo para volcar la zona criptográfica
 [Atención] Excedido número máximo de certificados. Los siguientes serán ignorados
 [Atención] Número máximo de dispositivos superado
 [Atención] Número máximo de pkcs12 superado. Los siguientes serán ignorados
 [Atención] Más de un Clauer conectado en el sistema.
 [Atención] Más de un Clauer conectado en el sistema.
           Use la opción -d
 [Atención] No se de detectaron Clauer en el sistema.
 [Atención] Tipo de bloque desconocido
 [Atención] Ha indicado más de un fichero pkcs12 con la opción --import-password o -i
           el password para todos los ficheros pkcs12 debe ser el mismo si se indica esta opción
. Password del pkcs12 para %s:  