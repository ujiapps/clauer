// DlgPaso2.cpp: archivo de implementaci�n
//

#include "stdafx.h"
#include "GestorCD.h"
#include "DlgPaso2.h"
#include ".\dlgpaso2.h"

#include "GestorCDDlg.h"


// Cuadro de di�logo de DlgPaso2

IMPLEMENT_DYNAMIC(DlgPaso2, CDialog)
DlgPaso2::DlgPaso2(CWnd* pParent /*=NULL*/)
	: CDialog(DlgPaso2::IDD, pParent)
	, m_pin(_T(""))
	, m_confirmacion(_T(""))
	, m_Texto2(_T(""))
	, m_textocuadro(_T(""))
	, m_TextoPass(_T(""))
{
	foco=0;
}

DlgPaso2::~DlgPaso2()
{
}

void DlgPaso2::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_EDIT_PASSWD, m_pin);
	DDX_Text(pDX, IDC_CONFIRMACION, m_confirmacion);
	DDX_Text(pDX, IDC_TEXTO_PASO_2, m_Texto2);
	DDX_Text(pDX, IDC_CUADRO, m_textocuadro);
	DDX_Text(pDX, IDC_PASSWORD, m_TextoPass);
	DDX_Text(pDX, IDC_CONF_PASS, m_TextoConfirmacion);
}

CString DlgPaso2::GetConfirmacion()
{
	UpdateData(TRUE);
	return m_confirmacion;
}

CString DlgPaso2::GetPIN()
{
	UpdateData(TRUE);
	return m_pin;
}


BEGIN_MESSAGE_MAP(DlgPaso2, CDialog)
	ON_EN_CHANGE(IDC_EDIT_PASSWD, OnEnChangeEditPasswd)
	ON_EN_SETFOCUS(IDC_CONFIRMACION, OnEnSetfocusConfirmacion)
	ON_EN_KILLFOCUS(IDC_CONFIRMACION, OnEnKillfocusConfirmacion)
END_MESSAGE_MAP()


// Controladores de mensajes de DlgPaso2

BOOL DlgPaso2::PreTranslateMessage(MSG* pMsg) 
{
	// TODO: Add your specialized code here and/or call the base class
	if(pMsg->message==WM_KEYDOWN)
	{
		if ( (pMsg->wParam==VK_ESCAPE) )
			pMsg->wParam=NULL ;
		else if ( (pMsg->wParam==VK_RETURN) ) {
			((CGestorCDDlg *) this->GetParent())->OnBnClickedSiguiente();
			return TRUE;
		}else if ( (pMsg->wParam==VK_TAB)  && (foco) ) {
			((CGestorCDDlg *) this->GetParent())->SetFocus();
			return TRUE;
		}
	}
	return CDialog::PreTranslateMessage(pMsg);
}

void DlgPaso2::OnEnChangeEditPasswd()
{
	// TODO:  Si �ste es un control RICHEDIT, el control no
	// enviar� esta notificaci�n a menos que se anule la funci�n CDialog::OnInitDialog()
	// y se llame a CRichEditCtrl().SetEventMask()
	// con el indicador ENM_CHANGE ORed en la m�scara.

	// TODO:  Agregue aqu� el controlador de notificaci�n de controles
}

void DlgPaso2::OnEnSetfocusConfirmacion()
{
	// TODO: Agregue aqu� su c�digo de controlador de notificaci�n de control
	foco=1;
}

void DlgPaso2::OnEnKillfocusConfirmacion()
{
	// TODO: Agregue aqu� su c�digo de controlador de notificaci�n de control
	foco=0;
}
