#pragma once


// Cuadro de di�logo de DlgPaso1

class DlgPaso1 : public CDialog
{
	DECLARE_DYNAMIC(DlgPaso1)

public:
	DlgPaso1(CWnd* pParent = NULL);   // Constructor est�ndar
	virtual ~DlgPaso1();

// Datos del cuadro de di�logo
	enum { IDD = IDD_DIALOG_PASO_1 };

	virtual BOOL PreTranslateMessage(MSG* pMsg);

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // Compatibilidad con DDX o DDV

	DECLARE_MESSAGE_MAP()
public:
	CString m_Texto1;
};
