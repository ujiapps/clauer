// GestorCDDlg.cpp: archivo de implementaci�n
//

#include "stdafx.h"
#include "GestorCD.h"
#include "GestorCDDlg.h"
#include ".\gestorcddlg.h"

#include <LIBRT/LIBRT.h>

//#include ".\Es_language/resource.h"
#include "Defines.h"

#include <LIBIMPORT/LIBIMPORT.h>

#include <CRYPTOWrapper/CRYPTOWrap.h>
#include "afxwin.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

// Cuadro de di�logo CAboutDlg utilizado para el comando Acerca de

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// Datos del cuadro de di�logo
	enum { IDD = IDD_ABOUTBOX };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // Compatibilidad con DDX/DDV

// Implementaci�n
protected:
	DECLARE_MESSAGE_MAP()
public:
	CButton m_bottonAbout;
	afx_msg void OnBnClickedOk();
	afx_msg void OnActivate(UINT nState, CWnd* pWndOther, BOOL bMinimized);
	CString m_textoVersion;
	CString m_textoCopyright;
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
, m_textoVersion(_T(""))
, m_textoCopyright(_T(""))
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDOK, m_bottonAbout);
	DDX_Text(pDX, IDC_TEXTO_VERSION, m_textoVersion);
	DDX_Text(pDX, IDC_COPYRIGHT, m_textoCopyright);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
	ON_BN_CLICKED(IDOK, OnBnClickedOk)
	ON_WM_SETFOCUS()
	ON_WM_ACTIVATE()
END_MESSAGE_MAP()


// Cuadro de di�logo de CGestorCDDlg



CGestorCDDlg::CGestorCDDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CGestorCDDlg::IDD, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
	foco=0;
	m_paso = 1;
}

void CGestorCDDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDCANTERIOR, m_buttonAnterior);
	DDX_Control(pDX, IDSIGUIENTE, m_buttonSiguiente);
	DDX_Control(pDX, IDABOUT, m_buttonAbout);
}

BEGIN_MESSAGE_MAP(CGestorCDDlg, CDialog)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	//}}AFX_MSG_MAP
	ON_BN_CLICKED(IDSIGUIENTE, OnBnClickedSiguiente)
	ON_BN_CLICKED(IDABOUT, OnBnClickedAbout)
	ON_BN_CLICKED(IDCANTERIOR, OnBnClickedCanterior)
	ON_BN_KILLFOCUS(IDABOUT, OnBnKillfocusAbout)
	ON_BN_SETFOCUS(IDABOUT, OnBnSetfocusAbout)
	ON_WM_ACTIVATE()
	ON_WM_CLOSE()
END_MESSAGE_MAP()


// Controladores de mensaje de CGestorCDDlg

BOOL CGestorCDDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// Agregar el elemento de men� "Acerca de..." al men� del sistema.

	// IDM_ABOUTBOX debe estar en el intervalo de comandos del sistema.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		CString strAboutMenu;
		strAboutMenu.LoadString(IDS_ABOUTBOX);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// Establecer el icono para este cuadro de di�logo. El marco de trabajo realiza esta operaci�n
	//  autom�ticamente cuando la ventana principal de la aplicaci�n no es un cuadro de di�logo
	SetIcon(m_hIcon, TRUE);			// Establecer icono grande
	SetIcon(m_hIcon, FALSE);		// Establecer icono peque�o

	// TODO: agregar aqu� inicializaci�n adicional

    CWnd* pWnd = GetDlgItem( IDC_FRAME );
    CRect rect;
    pWnd->GetWindowRect( &rect );
    ScreenToClient( &rect );

	//Cargamos idiomas
	hLangDLL = LoadLanguageDLL();

	LoadString(hLangDLL, ID_BOTON_SIGUIENTE, cadena, MAX_LOADSTRING);
	m_buttonSiguiente.SetWindowText(cadena);

	LoadString(hLangDLL, ID_BOTON_ACERCA, cadena, MAX_LOADSTRING);
	m_buttonAbout.SetWindowText(cadena);

	LoadString(hLangDLL, ID_BOTON_ANTERIOR, cadena, MAX_LOADSTRING);
	m_buttonAnterior.SetWindowText(cadena);

	this->UpdateData(FALSE);


    //Creating a m_Paso1
    m_Paso1.Create( IDD_DIALOG_PASO_1, this );

	LoadString(hLangDLL, ID_TEXT_PASO_1, cadena, MAX_LOADSTRING);

	m_Paso1.m_Texto1=cadena;

	m_Paso1.UpdateData(FALSE);

    m_Paso1.ShowWindow( WS_VISIBLE | WS_CHILD );
    m_Paso1.SetWindowPos(pWnd, rect.left, rect.top, rect.right, rect.bottom, SWP_SHOWWINDOW);

	//Creating a m_Paso2
	m_Paso2.Create( IDD_DIALOG_PASO_2, this );

	LoadString(hLangDLL, ID_TEXT_PASO_2, cadena, MAX_LOADSTRING);

	m_Paso2.m_Texto2 = cadena;

	LoadString(hLangDLL, ID_PASO2_CUADRO, cadena, MAX_LOADSTRING);

	m_Paso2.m_textocuadro = cadena;

	LoadString(hLangDLL, ID_PASO2_PASS, cadena, MAX_LOADSTRING);

	m_Paso2.m_TextoPass = cadena;

	LoadString(hLangDLL, ID_PASO2_CONF, cadena, MAX_LOADSTRING);

	m_Paso2.m_TextoConfirmacion = cadena;

	m_Paso2.UpdateData(FALSE);

    m_Paso2.ShowWindow( WS_VISIBLE | WS_CHILD );
    m_Paso2.SetWindowPos(pWnd, rect.left, rect.top, rect.right, rect.bottom, SWP_SHOWWINDOW);

	m_Paso2.ShowWindow(SW_HIDE);

	//Creating a m_Paso3
	m_Paso3.Create( IDD_DIALOG_PASO_3, this );

	LoadString(hLangDLL, ID_PASO3_CUADRO, cadena, MAX_LOADSTRING);

	m_Paso3.m_CuadroTexto3 = cadena;

	LoadString(hLangDLL, ID_PASO3_ADD, cadena, MAX_LOADSTRING);

	m_Paso3.m_buttonAdd.SetWindowText(cadena);

	LoadString(hLangDLL, ID_PASO3_DELETE, cadena, MAX_LOADSTRING);

	m_Paso3.m_buttonBorrar.SetWindowText(cadena);

	LoadString(hLangDLL, ID_PASO3_DELALL, cadena, MAX_LOADSTRING);

	m_Paso3.m_buttonBorrarTodo.SetWindowText(cadena);

	m_Paso3.UpdateData(FALSE);

    m_Paso3.ShowWindow( WS_VISIBLE | WS_CHILD );
    m_Paso3.SetWindowPos(pWnd, rect.left, rect.top, rect.right, rect.bottom, SWP_SHOWWINDOW);

	m_Paso3.ShowWindow(SW_HIDE);

	//Creating a m_Paso4
	m_Paso4.Create( IDD_DIALOG_PASO_4, this );

	LoadString(hLangDLL, ID_TEXT_PASO_4, cadena, MAX_LOADSTRING);

	m_Paso4.m_Texto4 = cadena;

	LoadString(hLangDLL, ID_PASO4_CUADRO, cadena, MAX_LOADSTRING);

	m_Paso4.m_CuadroTexto4 = cadena;

	LoadString(hLangDLL, ID_PASO4_BURN, cadena, MAX_LOADSTRING);

	m_Paso4.m_buttonBurn.SetWindowText(cadena);

	LoadString(hLangDLL, ID_PASO4_SAVE, cadena, MAX_LOADSTRING);

	m_Paso4.m_buttonSave.SetWindowText(cadena);

	LoadString(hLangDLL, ID_NUM_CERT, cadena, MAX_LOADSTRING);

	m_Paso4.m_TotalCert = cadena;

	m_Paso4.UpdateData(FALSE);

    m_Paso4.ShowWindow( WS_VISIBLE | WS_CHILD );
    m_Paso4.SetWindowPos(pWnd, rect.left, rect.top, rect.right, rect.bottom, SWP_SHOWWINDOW);

	m_paso=1;

	m_Paso4.ShowWindow(SW_HIDE);

	m_buttonAnterior.EnableWindow(FALSE);

	return TRUE;  // Devuelve TRUE  a menos que establezca el foco en un control
}

void CGestorCDDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialog::OnSysCommand(nID, lParam);
	}
}

// Si agrega un bot�n Minimizar al cuadro de di�logo, necesitar� el siguiente c�digo
//  para dibujar el icono. Para aplicaciones MFC que utilicen el modelo de documentos y vistas,
//  esta operaci�n la realiza autom�ticamente el marco de trabajo.

void CGestorCDDlg::OnPaint() 
{
	if (IsIconic())
	{
		CPaintDC dc(this); // Contexto de dispositivo para dibujo

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Centrar icono en el rect�ngulo de cliente
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Dibujar el icono
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

// El sistema llama a esta funci�n para obtener el cursor que se muestra mientras el usuario arrastra
//  la ventana minimizada.
HCURSOR CGestorCDDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

void CGestorCDDlg::OnBnClickedSiguiente()
{
	// TODO: Agregue aqu� su c�digo de controlador de notificaci�n de control

	int total=0;
	int i;
	CString confirmacion;
	CString m_path;

	switch ( m_paso ) {

	case 1:
		SetPaso(2);
		break;
	case 2:
		
		pin = m_Paso2.GetPIN();
		confirmacion = m_Paso2.GetConfirmacion();

		if ( pin != confirmacion ){
			LoadString(hLangDLL, ID_ERROR_PASS_CONF, cadena, MAX_LOADSTRING);
			AfxMessageBox(cadena, MB_OK,0);
			//AfxMessageBox("La contrase�a y su confirmaci�n deben coincidir", MB_OK,0);
			m_Paso2.m_pin.Empty();
			m_Paso2.m_confirmacion.Empty();
			m_Paso2.UpdateData(FALSE);
			m_Paso2.SetFocus();
		}else if ( !SeguridadPasswd(m_Paso2.m_pin) ) {
			m_Paso2.m_pin.Empty();
			m_Paso2.m_confirmacion.Empty();
			m_Paso2.UpdateData(FALSE);
		}else{
			m_Paso2.m_pin.Empty();
			m_Paso2.m_confirmacion.Empty();
			m_Paso2.UpdateData(FALSE);
			if(m_Paso3.m_ListaCert1.GetCount() == 0){
				m_Paso3.m_buttonBorrar.EnableWindow(FALSE);
				m_Paso3.m_buttonBorrarTodo.EnableWindow(FALSE);
			}	
			SetPaso(3);
		}
		break;
	case 3:
		m_Paso4.m_ListaCert2.ResetContent();
		total=m_Paso3.m_ListaCert1.GetCount();
		if(total > 0){
			for (i=0;i<total;i++){
				m_Paso3.m_ListaCert1.GetText(i,m_path);
				m_Paso4.m_ListaCert2.AddString(m_path);
			}
		}
		SetPaso(4);
		break;
	case 4:
		EndDialog(IDOK);
		break;
	}

}

void CGestorCDDlg::OnBnClickedAbout()
{
	// TODO: Agregue aqu� su c�digo de controlador de notificaci�n de control
	CAboutDlg aboutDlg;
	aboutDlg.DoModal();
/*
	hLangDLL = LoadLanguageDLL(1);
	LoadString(hLangDLL, ID_BOTON_ACEPTAR, cadena, MAX_LOADSTRING);
	aboutDlg.m_bottonAbout.SetWindowText("LLL");
	aboutDlg.UpdateData(FALSE);
*/
}

void CGestorCDDlg::SetPaso(int paso)
{
	CWnd* pWnd = GetDlgItem( IDC_FRAME );
    CRect rect;
    pWnd->GetWindowRect( &rect );
    ScreenToClient( &rect );

	switch ( m_paso ) {

	case 1:
		m_Paso1.ShowWindow(SW_HIDE);
		m_buttonAnterior.EnableWindow(TRUE);
		break;
	case 2:
		m_Paso2.ShowWindow(SW_HIDE);
		break;
	case 3:
		m_Paso3.ShowWindow(SW_HIDE);
		break;
	case 4:
		m_Paso4.ShowWindow(SW_HIDE);
		break;
	}
	switch ( paso ) {
	case 1:
		//	m_pnlIzquierdo.SetBitmap(::LoadBitmap(AfxGetResourceHandle(),MAKEINTRESOURCE(IDB_BITMAP1)));
		m_buttonAnterior.EnableWindow(FALSE);
		m_Paso1.ShowWindow(SW_SHOW);
		m_Paso1.SetFocus();
		//LoadString(IDC_TEXTO_PASO_1);
		break;
	case 2:
	//	m_pnlIzquierdo.SetBitmap(::LoadBitmap(AfxGetResourceHandle(),MAKEINTRESOURCE(IDB_BITMAP2)));
		m_Paso2.ShowWindow(SW_SHOW);
		m_Paso2.SetFocus();
		break;

	case 3:
	//	m_pnlIzquierdo.SetBitmap(::LoadBitmap(AfxGetResourceHandle(),MAKEINTRESOURCE(IDB_BITMAP3)));
		m_Paso3.ShowWindow(SW_SHOW);
		LoadString(hLangDLL, ID_BOTON_SIGUIENTE, cadena, MAX_LOADSTRING);
		m_buttonSiguiente.SetWindowText(cadena);
		//m_buttonSiguiente.SetWindowText("Siguiente >");
		m_Paso3.SetFocus();
		break;
		
	case 4:
	//	m_pnlIzquierdo.SetBitmap(::LoadBitmap(AfxGetResourceHandle(),MAKEINTRESOURCE(IDB_BITMAP4)));
		LoadString(hLangDLL, ID_BOTON_FIN, cadena, MAX_LOADSTRING);
		m_buttonSiguiente.SetWindowText(cadena);
		//m_buttonSiguiente.SetWindowText("Finalizar");
		m_Paso4.ShowWindow(SW_SHOW);
		m_Paso4.SetFocus();
		char buffer [1024];
		itoa(m_Paso4.m_ListaCert2.GetCount(),buffer,10);
		m_Paso4.m_numCert = buffer;
		m_Paso4.UpdateData(FALSE);
		break;
	}

	m_paso = paso;

	char num[2];

	TCHAR cadena2[MAX_LOADSTRING];

	itoa(m_paso, num, 10);

	//hLangDLL = LoadLanguageDLL(1);

	LoadString(hLangDLL, ID_TITULO_PRINCIPAL, cadena, MAX_LOADSTRING);

	LoadString(hLangDLL, ID_TITULO_PASO, cadena2, MAX_LOADSTRING);

	this->SetWindowText(CString(AfxGetAppName()) + (" :: ") + cadena + CString(" :: ") + cadena2 + " " + CString(num));

}

int CGestorCDDlg::SeguridadPasswd(CString pwd)
{
	int i, lon;
	int num = 0;
	int letras = 0;
/*	int igual =0;
	int buscar = 0;*/

	if ( pwd.GetLength() < 8 ){
		LoadString(hLangDLL, ID_ERROR_PASS, cadena, MAX_LOADSTRING);
		AfxMessageBox(cadena, MB_OK,0);
		//AfxMessageBox("La nueva contrase�a no es lo suficientemente segura, intentelo con otra.\nLa longitud es demasido corta, debe emplear m�nimo 8 caracteres alfanum�ricos.", MB_OK,0);
		return 0;
	}

	lon=pwd.GetLength();

	for (i=0; i<lon;i++)
	{

		int igual =0;
		int buscar = 0;
		//int indice = 0;

		if( isdigit(pwd[i]) ) {
			buscar=pwd.Find(pwd[i],i+1);
			if(buscar!= -1) {
			//	indice=i+2;
				igual=igual+2;
				while (buscar !=-1) {
					buscar=pwd.Find(pwd[i],buscar+1);
					//indice=indice+1;
					if(buscar != -1)
						igual=igual+1;
				
				 }
			}
			if(lon <=10) {
				if(igual >= 4){
					LoadString(hLangDLL, ID_ERROR_PASS_NUMBER, cadena, MAX_LOADSTRING);
					AfxMessageBox(cadena, MB_OK,0);
					//AfxMessageBox("La nueva contrase�a no es lo suficientemente segura, intentelo con otra.\nHay demasiados n�meros repetidos.", MB_OK,0);
					return 0;
				}
			}else{
				if(lon/2<=igual){
				LoadString(hLangDLL, ID_ERROR_PASS_NUMBER, cadena, MAX_LOADSTRING);
				AfxMessageBox(cadena, MB_OK,0);
				//AfxMessageBox("La nueva contrase�a no es lo suficientemente segura, intentelo con otra.\nHay demasiados n�meros repetidos.", MB_OK,0);
				return 0;
				}
			}
			num=num+1;
		}else if( isalpha(pwd[i]) ) {
			buscar=pwd.Find(pwd[i],i+1);
			if(buscar!= -1) {
			//	indice=i+2;
				igual=igual+2;
				while (buscar !=-1) {
					buscar=pwd.Find(pwd[i],buscar+1);
				//	indice=indice+1;
					if(buscar != -1)
						igual=igual+1;
				
				 }
			}
			if(lon <=10) {
				if(igual >= 4){
					LoadString(hLangDLL, ID_ERROR_PASS_CARACT, cadena, MAX_LOADSTRING);
					AfxMessageBox(cadena, MB_OK,0);
					//AfxMessageBox("La nueva contrase�a no es lo suficientemente segura, intentelo con otra.\nHay demasiadas letras repetidas.", MB_OK,0);
					return 0;
				}
			}else{
				if(lon/2<=igual){
				LoadString(hLangDLL, ID_ERROR_PASS_CARACT, cadena, MAX_LOADSTRING);
				AfxMessageBox(cadena, MB_OK,0);
				//AfxMessageBox("La nueva contrase�a no es lo suficientemente segura, intentelo con otra.\nHay demasiadas letras repetidas.", MB_OK,0);
				return 0;
				}
			}
			letras=letras+1;
		}
	}

	if(letras<3){
		LoadString(hLangDLL, ID_ERROR_REP_CARACT, cadena, MAX_LOADSTRING);
		AfxMessageBox(cadena, MB_OK,0);
		//AfxMessageBox("La nueva contrase�a no es lo suficientemente segura, intentelo con otra.\nDebe emplear como m�nimo 3 letras.", MB_OK,0);
		return 0;
	}else if(num<3){
		LoadString(hLangDLL, ID_ERROR_REP_NUM, cadena, MAX_LOADSTRING);
		AfxMessageBox(cadena, MB_OK,0);
		//AfxMessageBox("La nueva contrase�a no es lo suficientemente segura, intentelo con otra.\nDebe emplear como m�nimo 3 n�meros.", MB_OK,0);
		return 0;
	}else{
		return 1;
	}
}
void CGestorCDDlg::OnBnClickedCanterior()
{
	// TODO: Agregue aqu� su c�digo de controlador de notificaci�n de control

	if (m_paso!=1)
		SetPaso(m_paso-1);
}

BOOL CGestorCDDlg::PreTranslateMessage(MSG* pMsg) 
{
	// TODO: Add your specialized code here and/or call the base class
	if(pMsg->message==WM_KEYDOWN) {
		if ( (pMsg->wParam==VK_ESCAPE) ){
			pMsg->wParam=NULL ;
		}
	}else if ( (pMsg->wParam==VK_TAB) && (foco) ) {
		foco=0;
		if(m_paso==2)
			m_Paso2.SetFocus();
		else if(m_paso==3)
			m_Paso3.SetFocus();
		else if(m_paso==4)
			m_Paso4.SetFocus();

		return TRUE;
	}

	return CDialog::PreTranslateMessage(pMsg);
}

void CGestorCDDlg::OnBnKillfocusAbout()
{
	// TODO: Agregue aqu� su c�digo de controlador de notificaci�n de control
	foco=1;
}

void CGestorCDDlg::OnBnSetfocusAbout()
{
	// TODO: Agregue aqu� su c�digo de controlador de notificaci�n de control
	foco=0;
}

CString CGestorCDDlg::GetPin()
{
	return pin;
}

void CGestorCDDlg::AddAlmacen(CString ruta, CString passwd)
{
	almacen[ruta]=passwd;
}

int CGestorCDDlg::GetNumBloque()
{ 
	int count = almacen.GetCount();
	int num_bloque = 0;
	AlmacenPKCS12::CPair* pCurVal;

	FILE *fp;
	
	unsigned char *pkcs12 = NULL;
	unsigned long tamPkcs12;

//	unsigned char * llavePrivada = NULL;
	unsigned long tamLlavePrivada = 0;

//	unsigned char * certLlave = NULL;
	unsigned long tamCertLlave = 0;

//	unsigned char * certs = NULL;
	unsigned long tamCerts = 0;

	unsigned long numCerts = 0;

//	char * friendlyNameCert = NULL;
	unsigned long tamFriendlyNameCert = 0;

	if ( count == 0 ) {
		goto Fin;
	}

	pCurVal = almacen.PGetFirstAssoc();

	CRYPTO_Ini();

	while (pCurVal != NULL) {
			
			CString path = pCurVal->key;
			CString passwd = pCurVal->value;

			fp = fopen(path.GetBuffer(0), "rb");
			fseek(fp, 0, SEEK_END);
			tamPkcs12 = ftell(fp);
			fseek(fp, 0, SEEK_SET);

			pkcs12 = new BYTE [tamPkcs12];

			fread(pkcs12, tamPkcs12, 1, fp);

			fclose(fp);



			if ( CRYPTO_ParsePKCS12(pkcs12,tamPkcs12,passwd.GetBuffer(0),NULL,
			&tamLlavePrivada, NULL, &tamCertLlave, NULL, &tamCerts, &numCerts,
			NULL, &tamFriendlyNameCert) == ERR_CW_SI) {

				num_bloque = -1;
				goto Fin;

			}
	
			//N�mero de certificados personales + 2 * llaves privadas
			num_bloque = num_bloque + 1 + 2;

			// N�mero CA's
			num_bloque = num_bloque + numCerts;


			SecureZeroMemory(pkcs12, tamPkcs12);
			delete [] pkcs12;
			pkcs12 = NULL;

/*			if(llavePrivada) {
				SecureZeroMemory(llavePrivada,tamLlavePrivada);
				llavePrivada = NULL;
			}

			if(certLlave) {
				SecureZeroMemory(certLlave,tamCertLlave);
				certLlave = NULL;
			}

			if(certs) {
				SecureZeroMemory(certs,tamCerts);
				certs = NULL;
			}
*/
			pCurVal = almacen.PGetNextAssoc(pCurVal);

		}

	num_bloque = num_bloque + ( count % NUM_KEY_CONTAINERS ) + 1;

Fin:
	CRYPTO_Fin();
	if(pkcs12) {
		SecureZeroMemory(pkcs12, tamPkcs12);
		delete [] pkcs12;
		pkcs12 = NULL;
	}
	CRYPTO_Fin();
	return num_bloque;
}

void CGestorCDDlg::RemoveAlmacen(CString path)
{
	almacen.RemoveKey(path);
}
void CGestorCDDlg::RemoveAll()
{
	almacen.RemoveAll();
}

int CGestorCDDlg::Importar(CString ruta)
{
	USBCERTS_HANDLE hClauer;
	AlmacenPKCS12::CPair* pCurVal;

	pCurVal = almacen.PGetFirstAssoc();


		LIBRT_Ini();

		if ( ! LIBRT_HayRuntime()) {
			LoadString(hLangDLL, ID_ERROR_RUNTIME, cadena, MAX_LOADSTRING);
			AfxMessageBox(cadena);
			//AfxMessageBox("ERROR: El servicio USBPKI Clauer no esta disponible");
			return 0;
		}

		if ( LIBRT_IniciarDispositivo ((unsigned char *)ruta.GetBuffer(0), pin.GetBuffer(0), &hClauer ) != ERR_LIBRT_NO) {
			LoadString(hLangDLL, ID_ERROR_FILE_INI, cadena, MAX_LOADSTRING);
			AfxMessageBox(cadena);
			//AfxMessageBox("ERROR: No se pudo inicializar el fichero");
			return 0;
		}

		//AfxMessageBox("DESPUES DE INICIAR");

		while (pCurVal != NULL) {
			// Importamos
			CString path = pCurVal->key;
			CString passwd = pCurVal->value;
CRYPTO_Ini();
			if ( ! LIBIMPORT_ImportarPKCS12(path.GetBuffer(0),passwd.GetBuffer(0), &hClauer) ) {
				LoadString(hLangDLL, ID_ERROR_IMPORT, cadena, MAX_LOADSTRING);
				CString error= cadena + pCurVal->key;
				MessageBox(error,NULL, MB_ICONEXCLAMATION|MB_OK);
				LIBRT_FinalizarDispositivo(&hClauer);
				LIBRT_Fin();
				CRYPTO_Fin();
				return 0;
			}
CRYPTO_Fin();
			pCurVal = almacen.PGetNextAssoc(pCurVal);

		}

		if ( LIBRT_FinalizarDispositivo(&hClauer) == ERR_LIBRT_SI) {
			LoadString(hLangDLL, ID_ERROR_FILE_FIN, cadena, MAX_LOADSTRING);
			AfxMessageBox(cadena);
			//AfxMessageBox("ERROR: No se pudo finalizar el fichero");
			return 0;
		}

		LIBRT_Fin();

	return 1;
}

HMODULE CGestorCDDlg::ReturnLanguage()
{
	return hLangDLL;
}
HMODULE CGestorCDDlg::LoadLanguageDLL()
{
	TCHAR		ActualPath[MAX_PATH];
	TCHAR		DllLangPath[MAX_PATH];
	HMODULE		hDLL;

	HKEY hKey;
	DWORD tamValue;

	char *idioma = NULL;

	// Get the base directory for the satellite DLL search
	if( GetCurrentDirectory(MAX_PATH, ActualPath)) {
		_tcscat(ActualPath, _T("\\"));
	}

	_tcscpy(DllLangPath, ActualPath);

	if ( idioma != NULL )
		return NULL;

	if ( RegOpenKeyEx(HKEY_LOCAL_MACHINE, 
			     "SOFTWARE\\Universitat Jaume I\\Projecte Clauer", 
				 0,
				 KEY_QUERY_VALUE,
				 &hKey) != ERROR_SUCCESS ) 
	{
		return NULL;
	}

	if ( RegQueryValueEx(hKey,"Idioma", NULL, NULL, NULL, &tamValue) != ERROR_SUCCESS ) {
		RegCloseKey(hKey);
		return NULL;
	}

/*	if ( tamValue > 5 ) {
		RegCloseKey(hKey);
		return NULL;
	}*/

	idioma = new char [tamValue];

	if ( !idioma ) {
		RegCloseKey(hKey);
		return NULL;
	}

	if ( RegQueryValueEx(hKey,"Idioma", NULL, NULL, (BYTE *)idioma, &tamValue) != ERROR_SUCCESS ) {
		delete [] idioma;
		idioma = NULL;
		RegCloseKey(hKey);
		return NULL;
	}

	RegCloseKey(hKey);

	/* Cargamos la tabla de etiquetas
	 */

	if ( strcmp(idioma, "1027") == 0 ) {
		_tcscat(DllLangPath, _T("\Va_language.dll"));
	}else if ( strcmp(idioma, "1033") == 0 ){
		/* Idioma no reconocido entonces Ingles */
		_tcscat(DllLangPath, _T("\Es_language.dll"));
	}else{
		// Idioma no reconocido por defecto espa�ol
		_tcscat(DllLangPath, _T("\Es_language.dll"));
	}

	if( hDLL = LoadLibrary(DllLangPath) ) {
		if ( idioma ) {
			delete [] idioma;
			idioma = NULL;
		}

		return hDLL;
	}else{
		if ( idioma ) {
			delete [] idioma;
			idioma = NULL;
		}
		return NULL;
	}

}
void CAboutDlg::OnBnClickedOk()
{
	// TODO: Agregue aqu� su c�digo de controlador de notificaci�n de control
	OnOK();
}

void CAboutDlg::OnActivate(UINT nState, CWnd* pWndOther, BOOL bMinimized)
{
	CDialog::OnActivate(nState, pWndOther, bMinimized);

	// TODO: Agregue aqu� su c�digo de controlador de mensajes

	HMODULE hLangDLL;
	TCHAR cadena[MAX_LOADSTRING];

	hLangDLL = ((CGestorCDDlg *) this->GetParent())->LoadLanguageDLL();

	LoadString(hLangDLL, ID_BOTON_ACEPTAR, cadena, MAX_LOADSTRING);

	this->m_bottonAbout.SetWindowText(cadena);

	LoadString(hLangDLL, ID_TITULO_ACERCA, cadena, MAX_LOADSTRING);

	this->SetWindowText(cadena);

	LoadString(hLangDLL, ID_ACERCA_VERSION, cadena, MAX_LOADSTRING);

	m_textoVersion = cadena;

	LoadString(hLangDLL, ID_ACERCA_COPYRIGHT, cadena, MAX_LOADSTRING);

	m_textoCopyright = cadena;

	CloseHandle(hLangDLL);

	this->UpdateData(FALSE);
}

void CGestorCDDlg::OnActivate(UINT nState, CWnd* pWndOther, BOOL bMinimized)
{
	CDialog::OnActivate(nState, pWndOther, bMinimized);

	// TODO: Agregue aqu� su c�digo de controlador de mensajes

	char num[2];

	TCHAR cadena2[MAX_LOADSTRING];

	itoa(m_paso, num, 10);

	//hLangDLL = LoadLanguageDLL();

	LoadString(hLangDLL, ID_TITULO_PRINCIPAL, cadena, MAX_LOADSTRING);

	LoadString(hLangDLL, ID_TITULO_PASO, cadena2, MAX_LOADSTRING);

	this->SetWindowText(CString(AfxGetAppName()) + (" :: ") + cadena + CString(" :: ") + cadena2 + CString(num));


}

void CGestorCDDlg::OnClose()
{
	// TODO: Agregue aqu� su c�digo de controlador de mensajes o llame al valor predeterminado

	CloseHandle(hLangDLL);

	CDialog::OnClose();
}