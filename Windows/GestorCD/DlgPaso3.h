#pragma once
#include "afxwin.h"

#define MAX_LOADSTRING	1024

// Cuadro de di�logo de DlgPaso3

class DlgPaso3 : public CDialog
{
	DECLARE_DYNAMIC(DlgPaso3)

public:
	DlgPaso3(CWnd* pParent = NULL);   // Constructor est�ndar

	virtual ~DlgPaso3();

// Datos del cuadro de di�logo
	enum { IDD = IDD_DIALOG_PASO_3 };

	virtual BOOL PreTranslateMessage(MSG* pMsg);

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // Compatibilidad con DDX o DDV

	HMODULE hLangDLL;
	TCHAR cadena[MAX_LOADSTRING];

	// 1 OK 0 ERROR
	int Verificar (CString pwd, CString ruta);

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedButton1();
	afx_msg void OnBnClickedButton2();
	CString m_ruta;
	CListBox m_ListaCert1;
	afx_msg void OnBnClickedButtonEliminar();
	afx_msg void OnBnClickedButtonEliminarTodo();
	CButton m_buttonBorrar;
	CButton m_buttonBorrarTodo;

	CString m_CuadroTexto3;
	CButton m_buttonAdd;
	afx_msg void OnActivate(UINT nState, CWnd* pWndOther, BOOL bMinimized);
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
};
