// DlgPaso3.cpp: archivo de implementaci�n
//

#include "stdafx.h"
#include "GestorCD.h"
#include "DlgPaso3.h"
#include ".\dlgpaso3.h"

#include "GestorCDDlg.h"

#include "DlgPasswdPKCS12.h"

//#include ".\Es_language/resource.h"
#include "Defines.h"

#include <CRYPTOWrapper/CRYPTOWrap.h>


// Cuadro de di�logo de DlgPaso3

IMPLEMENT_DYNAMIC(DlgPaso3, CDialog)
DlgPaso3::DlgPaso3(CWnd* pParent /*=NULL*/)
	: CDialog(DlgPaso3::IDD, pParent)
	, m_ruta(_T(""))
	, m_CuadroTexto3(_T(""))
{
//	hLangDLL = ((CGestorCDDlg *) this->GetParent())->LoadLanguageDLL();
//	this->hLangDLL = ((CGestorCDDlg *) this->GetParent())->ReturnLanguage();

}

DlgPaso3::~DlgPaso3()
{
//	CloseHandle(hLangDLL);
}

void DlgPaso3::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_RUTA, m_ruta);
	DDX_Control(pDX, IDC_LIST1, m_ListaCert1);
	DDX_Control(pDX, IDC_BUTTON_ELIMINAR, m_buttonBorrar);
	DDX_Control(pDX, IDC_BUTTON_ELIMINAR_TODO, m_buttonBorrarTodo);
	DDX_Text(pDX, IDC_CUADRO3, m_CuadroTexto3);
	DDX_Control(pDX, IDC_BUTTON_ANYADIR, m_buttonAdd);
}


BEGIN_MESSAGE_MAP(DlgPaso3, CDialog)
	ON_BN_CLICKED(IDC_BUTTON_ESC, OnBnClickedButton1)
	ON_BN_CLICKED(IDC_BUTTON_ANYADIR, OnBnClickedButton2)
	ON_BN_CLICKED(IDC_BUTTON_ELIMINAR, OnBnClickedButtonEliminar)
	ON_BN_CLICKED(IDC_BUTTON_ELIMINAR_TODO, OnBnClickedButtonEliminarTodo)
	ON_WM_ACTIVATE()
	ON_WM_CREATE()
END_MESSAGE_MAP()


// Controladores de mensajes de DlgPaso3

void DlgPaso3::OnBnClickedButton1()
{
	// TODO: Agregue aqu� su c�digo de controlador de notificaci�n de control

	CFileDialog dialog( TRUE, NULL, NULL, OFN_HIDEREADONLY, "PKCS12 (*.p12)|*.p12| Personal Information Exchange (*.pfx)|*.pfx||", this);
	LoadString(hLangDLL, ID_CHOOSE_CERT, cadena, MAX_LOADSTRING);
	dialog.m_ofn.lpstrTitle = cadena;
	//dialog.m_ofn.lpstrTitle = "Elija el certificado";
	int iRet = dialog.DoModal();
	if ( iRet == IDOK)
	{
		m_ruta = dialog.GetPathName();
		UpdateData(FALSE);
		}else {
		SetFocus();
	}
}

void DlgPaso3::OnBnClickedButton2()
{
	// TODO: Agregue aqu� su c�digo de controlador de notificaci�n de control

	int busca=0;
	int nResponse;
	DlgPasswdPKCS12 m_dlgPasswPKCS12;


	if (m_ruta.IsEmpty()) {
	//	AfxMessageBox("Debe seleccionar un certificado.");
		LoadString(hLangDLL, ID_SELECT_CERT, cadena, MAX_LOADSTRING);
		AfxMessageBox(cadena);
	}else{
		if((busca=m_ListaCert1.FindStringExact(-1,m_ruta)) != -1 ) {
			LoadString(hLangDLL, ID_CERT_IS_ADD, cadena, MAX_LOADSTRING);
			AfxMessageBox(cadena);
			//AfxMessageBox("El certificado ya esta a�adido.");
		}else{
			nResponse = m_dlgPasswPKCS12.DoModal();
			if (nResponse == IDOK)
			{
				// TODO: Place code here to handle when the dialog is
				//  dismissed with OK
				//Contrase�a del certificado correcta a�adimos
				CRYPTO_Ini();
				if(Verificar(m_dlgPasswPKCS12.m_passwdPKCS12,m_ruta)){		
					m_ListaCert1.AddString(m_ruta);	
					((CGestorCDDlg *) this->GetParent())->AddAlmacen(m_ruta,m_dlgPasswPKCS12.m_passwdPKCS12);
				}
				else{
					//ERROR contrase�a no valida para el certificado
					LoadString(hLangDLL, ID_CERT_PASS_ERROR, cadena, MAX_LOADSTRING);
					AfxMessageBox(cadena);
					//AfxMessageBox("La contrase�a del certificado no es correcta.");
				}
				CRYPTO_Fin();
			}
			//else if (nResponse == IDCANCEL)
			//{
				// Nada que hacer
			//}
		}
	}
	
	if(m_ListaCert1.GetCount() != 0){
		m_buttonBorrar.EnableWindow(TRUE);
		m_buttonBorrarTodo.EnableWindow(TRUE);
	}

	m_ruta.Empty();
	UpdateData(FALSE);
}

void DlgPaso3::OnBnClickedButtonEliminar()
{
	// TODO: Agregue aqu� su c�digo de controlador de notificaci�n de control
	int select;
	UpdateData(FALSE);
	select=m_ListaCert1.GetCurSel();
	if(select==LB_ERR ){
		LoadString(hLangDLL, ID_SELECT_CERT, cadena, MAX_LOADSTRING);
		AfxMessageBox(cadena);
		//AfxMessageBox("Debe seleccionar un certificado");
	}else{
		CString m_path;
		m_ListaCert1.GetText(select,m_path);
		((CGestorCDDlg *) this->GetParent())->RemoveAlmacen(m_path);
		m_ListaCert1.DeleteString(select);
	}
	if(m_ListaCert1.GetCount() == 0){
		m_buttonBorrar.EnableWindow(FALSE);
		m_buttonBorrarTodo.EnableWindow(FALSE);
	}
}

void DlgPaso3::OnBnClickedButtonEliminarTodo()
{
	// TODO: Agregue aqu� su c�digo de controlador de notificaci�n de control
	m_ListaCert1.ResetContent();
	((CGestorCDDlg *) this->GetParent())->RemoveAll();
	m_buttonBorrar.EnableWindow(FALSE);
	m_buttonBorrarTodo.EnableWindow(FALSE);
}

BOOL DlgPaso3::PreTranslateMessage(MSG* pMsg) 
{
	// TODO: Add your specialized code here and/or call the base class
	if(pMsg->message==WM_KEYDOWN)
	{
		if ( (pMsg->wParam==VK_ESCAPE) )
			pMsg->wParam=NULL ;
	}
	return CDialog::PreTranslateMessage(pMsg);
}

int DlgPaso3::Verificar(CString pwd, CString ruta)
{

	FILE *fp;
	BYTE *pkcs12;
	DWORD tamPKCS12;
	int ok;
	fp = fopen(ruta.GetBuffer(0), "rb");
	if ( !fp ) {
		return 0;
	}

	fseek(fp, 0, SEEK_END);
	tamPKCS12 = ftell(fp);
	fseek(fp, 0, SEEK_SET);

	pkcs12 = new BYTE [tamPKCS12];
	fread(pkcs12, tamPKCS12, 1, fp);
	fclose(fp);

	ok = CRYPTO_PKCS12_VerificarPassword (pkcs12, tamPKCS12,pwd.GetBuffer(0));

	SecureZeroMemory(pkcs12, tamPKCS12);
	delete [] pkcs12;
	pkcs12 = NULL;

	if ( ok == 0 ) {
	/* Password incorrecta */
	
		fclose(fp);
		return 0;
	} else if ( ok == 2 ) {
	/* Error */
		fclose(fp);
		return 0;
	}
	return 1;
}

void DlgPaso3::OnActivate(UINT nState, CWnd* pWndOther, BOOL bMinimized)
{
	CDialog::OnActivate(nState, pWndOther, bMinimized);

	// TODO: Agregue aqu� su c�digo de controlador de mensajes

//	this->hLangDLL = ((CGestorCDDlg *) this->GetParent())->ReturnLanguage();
	

}

int DlgPaso3::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CDialog::OnCreate(lpCreateStruct) == -1)
		return -1;

	// TODO:  Agregue aqu� su c�digo de creaci�n especializado

	this->hLangDLL = ((CGestorCDDlg *) this->GetParent())->ReturnLanguage();

	return 0;
}
