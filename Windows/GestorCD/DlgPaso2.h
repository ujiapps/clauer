#pragma once
#include "afxwin.h"


// Cuadro de di�logo de DlgPaso2

class DlgPaso2 : public CDialog
{
	DECLARE_DYNAMIC(DlgPaso2)

public:
	DlgPaso2(CWnd* pParent = NULL);   // Constructor est�ndar
	virtual ~DlgPaso2();

	CString GetPIN(void);
	CString GetConfirmacion(void);

// Datos del cuadro de di�logo
	enum { IDD = IDD_DIALOG_PASO_2 };

	virtual BOOL PreTranslateMessage(MSG* pMsg);

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // Compatibilidad con DDX o DDV

	int foco;

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnEnChangeEditPasswd();
	CString m_pin;
	CString m_confirmacion;
	afx_msg void OnEnSetfocusConfirmacion();
	afx_msg void OnEnKillfocusConfirmacion();
	CString m_Texto2;
	CString m_textocuadro;
	CString m_TextoPass;
	CString m_TextoConfirmacion;
};
