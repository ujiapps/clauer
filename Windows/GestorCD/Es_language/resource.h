//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by Es_language.rc
//
#define ID_TEXT_PASO_1                  1
#define ID_TEXT_PASO_2                  2
#define ID_TEXT_PASO_4                  3
#define ID_BOTON_SIGUIENTE              4
#define ID_BOTON_ACERCA                 5
#define ID_BOTON_ANTERIOR               6
#define ID_BOTON_ACEPTAR                7
#define ID_TITULO_PRINCIPAL             8
#define ID_TITULO_ACERCA                9
#define ID_PASO2_CUADRO                 10
#define ID_PASO2_PASS                   11
#define ID_PASO2_CONF                   12
#define ID_PASO3_CUADRO                 13
#define ID_PASO3_ADD                    14
#define ID_PASO3_DELETE                 15
#define ID_PASO3_DELALL                 16
#define ID_PASO4_CUADRO                 17
#define ID_PASO4_BURN                   18
#define ID_PASO4_SAVE                   19
#define ID_NUM_CERT                     20
#define ID_ACERCA_VERSION               21
#define ID_ACERCA_COPYRIGHT             22
#define ID_TITULO_PASO                  23
#define ID_CERT_IS_ADD                  24
#define ID_CERT_PASS_ERROR              25
#define ID_SELECT_CERT                  26
#define ID_SAVE_FILE_ERROR              27
#define ID_FILE_ERROR                   28
#define ID_ERROR_PATH                   29
#define ID_ERROR_STAGGING               30
#define ID_ERROR_CREATE_FILE            31
#define ID_ERROR_FILE_SAVE_BURN         32
#define ID_ERROR_INIT_BURN              33
#define ID_ERROR_BURN                   34
#define ID_ERROR_PASS_CONF              35
#define ID_ERROR_PASS                   36
#define ID_ERROR_PASS_NUMBER            37
#define ID_ERROR_PASS_CARACT            38
#define ID_ERROR_REP_CARACT             39
#define ID_ERROR_REP_NUM                40
#define ID_ERROR_RUNTIME                41
#define ID_ERROR_FILE_INI               42
#define ID_ERROR_IMPORT                 43
#define ID_ERROR_FILE_FIN               44
#define ID_BOTON_FIN                    45
#define ID_FILE_SAVE_CORRECT            46
#define ID_CHOOSE_CERT                  47
#define ID_FILE_FOR_CD                  48
#define ID_ERROR_ASSITENT               49

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        106
#define _APS_NEXT_COMMAND_VALUE         40001
#define _APS_NEXT_CONTROL_VALUE         1001
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
