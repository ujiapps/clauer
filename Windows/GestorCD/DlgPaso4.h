#pragma once
#include "afxwin.h"


#define MAX_LOADSTRING	1024


// Cuadro de di�logo de DlgPaso4

class DlgPaso4 : public CDialog
{
	DECLARE_DYNAMIC(DlgPaso4)

public:
	DlgPaso4(CWnd* pParent = NULL);   // Constructor est�ndar
	virtual ~DlgPaso4();

// Datos del cuadro de di�logo
	enum { IDD = IDD_DIALOG_PASO_4 };

	virtual BOOL PreTranslateMessage(MSG* pMsg);

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // Compatibilidad con DDX o DDV

	HMODULE hLangDLL;
	TCHAR cadena[MAX_LOADSTRING];

	//1 OK 0 ERROR
	int ComprobarNombre(CString nombre);

	DECLARE_MESSAGE_MAP()
public:
	CListBox m_ListaCert2;
	afx_msg void OnBnClickedButtonGuardar();
	afx_msg void OnBnClickedButtonGrabar();
	CString m_numCert;
	CString m_Texto4;
	CString m_CuadroTexto4;
	CButton m_buttonBurn;
	CButton m_buttonSave;
	CString m_TotalCert;
	afx_msg void OnActivate(UINT nState, CWnd* pWndOther, BOOL bMinimized);
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
};
