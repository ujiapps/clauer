// GestorCDDlg.h: archivo de encabezado
//

#pragma once

#include "DlgPaso1.h"
#include "DlgPaso2.h"
#include "DlgPaso3.h"
#include "DlgPaso4.h"
#include "afxwin.h"

#include <afxtempl.h>

#define MAX_LOADSTRING	1024


typedef CMap<CString, LPCSTR, CString, CString> AlmacenPKCS12;

// Cuadro de di�logo de CGestorCDDlg
class CGestorCDDlg : public CDialog
{
// Construcci�n
public:

	CGestorCDDlg(CWnd* pParent = NULL);	// Constructor est�ndar

// Datos del cuadro de di�logo
	enum { IDD = IDD_GESTORCD_DIALOG };

	virtual BOOL PreTranslateMessage(MSG* pMsg);

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// Compatibilidad con DDX/DDV


// Implementaci�n
protected:
	HICON m_hIcon;
	
	DlgPaso1 m_Paso1;
	DlgPaso2 m_Paso2;
	DlgPaso3 m_Paso3;
	DlgPaso4 m_Paso4;

	int m_paso; // Indica en qu� paso estamos actualmente
	void SetPaso (int paso);
	int SeguridadPasswd (CString pwd);

	CString pin;

	HMODULE hLangDLL;
	TCHAR cadena[MAX_LOADSTRING];

	int foco;

	AlmacenPKCS12 almacen;

	// Funciones de asignaci�n de mensajes generadas
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedSiguiente();
	afx_msg void OnBnClickedAbout();
	afx_msg void OnBnClickedCanterior();
	CButton m_buttonAnterior;
	CButton m_buttonSiguiente;
	CButton m_buttonAbout;
	afx_msg void OnBnKillfocusAbout();
	afx_msg void OnBnSetfocusAbout();

	int Importar(CString);

	void AddAlmacen(CString,CString);

	void RemoveAll();

	void RemoveAlmacen(CString);

	CString GetPin();

	HMODULE LoadLanguageDLL();

	HMODULE ReturnLanguage();

	// Devuelve n�mero de bloques, -1 ERROR, 0 no hay certificados
	int GetNumBloque();
	afx_msg void OnActivate(UINT nState, CWnd* pWndOther, BOOL bMinimized);
	afx_msg void OnClose();
};
