// GestorCD.h: archivo de encabezado principal para la aplicación PROJECT_NAME
//

#pragma once

#ifndef __AFXWIN_H__
	#error incluye 'stdafx.h' antes de incluir este archivo para PCH
#endif

#include "resource.h"		// Símbolos principales


// CGestorCDApp:
// Consulte la sección GestorCD.cpp para obtener información sobre la implementación de esta clase
//

class CGestorCDApp : public CWinApp
{
public:
	CGestorCDApp();

// Reemplazos
	public:
	virtual BOOL InitInstance();

// Implementación

	DECLARE_MESSAGE_MAP()
};

extern CGestorCDApp theApp;
