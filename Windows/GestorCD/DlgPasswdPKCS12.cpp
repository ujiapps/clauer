// DlgPasswdPKCS12.cpp: archivo de implementación
//

#include "stdafx.h"
#include "GestorCD.h"
#include "DlgPasswdPKCS12.h"


// Cuadro de diálogo de DlgPasswdPKCS12

IMPLEMENT_DYNAMIC(DlgPasswdPKCS12, CDialog)
DlgPasswdPKCS12::DlgPasswdPKCS12(CWnd* pParent /*=NULL*/)
	: CDialog(DlgPasswdPKCS12::IDD, pParent)
	, m_passwdPKCS12(_T(""))
{
}

DlgPasswdPKCS12::~DlgPasswdPKCS12()
{
}

void DlgPasswdPKCS12::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_EDIT_CONTRASENYA_CERT, m_passwdPKCS12);
}


BEGIN_MESSAGE_MAP(DlgPasswdPKCS12, CDialog)
END_MESSAGE_MAP()


// Controladores de mensajes de DlgPasswdPKCS12
