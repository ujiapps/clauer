// DlgPaso1.cpp: archivo de implementación
//

#include "stdafx.h"
#include "GestorCD.h"
#include "DlgPaso1.h"

#include "GestorCDDlg.h"


// Cuadro de diálogo de DlgPaso1

IMPLEMENT_DYNAMIC(DlgPaso1, CDialog)
DlgPaso1::DlgPaso1(CWnd* pParent /*=NULL*/)
	: CDialog(DlgPaso1::IDD, pParent)
	, m_Texto1(_T(""))
{
}

DlgPaso1::~DlgPaso1()
{
}

void DlgPaso1::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_TEXTO_PASO_1, m_Texto1);
}

BOOL DlgPaso1::PreTranslateMessage(MSG* pMsg) 
{
	// TODO: Add your specialized code here and/or call the base class
	if(pMsg->message==WM_KEYDOWN)
	{
		if ( (pMsg->wParam==VK_ESCAPE) )
			pMsg->wParam=NULL ;
		else if ( (pMsg->wParam==VK_RETURN) ) {
			((CGestorCDDlg *) this->GetParent())->OnBnClickedSiguiente();
			return TRUE;
		}
	}
	return CDialog::PreTranslateMessage(pMsg);
}

BEGIN_MESSAGE_MAP(DlgPaso1, CDialog)
END_MESSAGE_MAP()


// Controladores de mensajes de DlgPaso1
