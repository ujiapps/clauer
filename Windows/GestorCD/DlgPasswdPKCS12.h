#pragma once


// Cuadro de di�logo de DlgPasswdPKCS12

class DlgPasswdPKCS12 : public CDialog
{
	DECLARE_DYNAMIC(DlgPasswdPKCS12)

public:
	DlgPasswdPKCS12(CWnd* pParent = NULL);   // Constructor est�ndar
	virtual ~DlgPasswdPKCS12();

// Datos del cuadro de di�logo
	enum { IDD = IDD_DIALOG_PASSWD_PKCS12 };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // Compatibilidad con DDX o DDV

	DECLARE_MESSAGE_MAP()
public:
	CString m_passwdPKCS12;
};
