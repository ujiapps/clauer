// DlgPaso4.cpp: archivo de implementaci�n
//

#include "stdafx.h"
#include "GestorCD.h"
#include "DlgPaso4.h"

#include <libFormat/usb_format.h>

#include "GestorCDDlg.h"


#include ".\dlgpaso4.h"

//#include ".\Es_language/resource.h"
#include "Defines.h"

// Cuadro de di�logo de DlgPaso4

IMPLEMENT_DYNAMIC(DlgPaso4, CDialog)
DlgPaso4::DlgPaso4(CWnd* pParent /*=NULL*/)
	: CDialog(DlgPaso4::IDD, pParent)
	, m_numCert(_T(""))
	, m_Texto4(_T(""))
	, m_CuadroTexto4(_T(""))
	, m_TotalCert(_T(""))
{
//	hLangDLL = ((CGestorCDDlg *) this->GetParent())->LoadLanguageDLL();
}

DlgPaso4::~DlgPaso4()
{
//	CloseHandle(hLangDLL);
}

void DlgPaso4::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_LIST1, m_ListaCert2);
	DDX_Text(pDX, IDC_NUM_CERT, m_numCert);
	DDX_Text(pDX, IDC_TEXTO_PASO_4, m_Texto4);
	DDX_Text(pDX, IDC_CUADRO4, m_CuadroTexto4);
	DDX_Control(pDX, IDC_BUTTON_GRABAR, m_buttonBurn);
	DDX_Control(pDX, IDC_BUTTON_GUARDAR, m_buttonSave);
	DDX_Text(pDX, IDC_TOTALCERT, m_TotalCert);
}


BEGIN_MESSAGE_MAP(DlgPaso4, CDialog)
	ON_BN_CLICKED(IDC_BUTTON_GUARDAR, OnBnClickedButtonGuardar)
	ON_BN_CLICKED(IDC_BUTTON_GRABAR, OnBnClickedButtonGrabar)
	ON_WM_ACTIVATE()
	ON_WM_CREATE()
END_MESSAGE_MAP()


// Controladores de mensajes de DlgPaso4

BOOL DlgPaso4::PreTranslateMessage(MSG* pMsg) 
{
	// TODO: Add your specialized code here and/or call the base class
	if(pMsg->message==WM_KEYDOWN)
	{
		if ( (pMsg->wParam==VK_ESCAPE) )
			pMsg->wParam=NULL ;
	}
	return CDialog::PreTranslateMessage(pMsg);
}

void DlgPaso4::OnBnClickedButtonGuardar()
{
	// TODO: Agregue aqu� su c�digo de controlador de notificaci�n de control

	int num_cert = m_ListaCert2.GetCount();

	CFileDialog dialog( FALSE, ".cla", "CRYF_000", OFN_HIDEREADONLY | OFN_OVERWRITEPROMPT, "Fichero Clauer (*.cla)|*.cla||", this);
	LoadString(hLangDLL, ID_FILE_FOR_CD, cadena, MAX_LOADSTRING);
	dialog.m_ofn.lpstrTitle = cadena;
	//dialog.m_ofn.lpstrTitle = "Guardar fichero para CD";
	
	int iRet = dialog.DoModal();
	if ( iRet == IDOK)
	{
		CString m_ruta = dialog.GetPathName();
		CString nombre = dialog.GetFileName();
		UpdateData(FALSE);

		if( !ComprobarNombre(nombre) ) {
			LoadString(hLangDLL, ID_SAVE_FILE_ERROR, cadena, MAX_LOADSTRING);
			MessageBox(cadena, NULL, MB_ICONERROR|MB_OK);
			//MessageBox("Nombre del fichero no v�lido.\nDebe nombrase como CRYF_XXX.cla, donde XXX son 3 d�gitos.", NULL, MB_ICONERROR|MB_OK);
		}else{
			if ( format_crear_fichero_cripto(m_ruta.GetBuffer(0),((CGestorCDDlg *) this->GetParent())->GetPin().GetBuffer(0),((CGestorCDDlg *) this->GetParent())->GetNumBloque()) ) {
				LoadString(hLangDLL, ID_ERROR_CREATE_FILE, cadena, MAX_LOADSTRING);
				MessageBox(cadena, NULL, MB_ICONINFORMATION|MB_OK);
				//MessageBox("Error creando el fichero", NULL, MB_ICONINFORMATION|MB_OK);
			}else {
				// A�adimos certificados
				if ( num_cert != 0 ) {

					if ( ((CGestorCDDlg *) this->GetParent())->Importar(m_ruta) ){
						LoadString(hLangDLL, ID_FILE_SAVE, cadena, MAX_LOADSTRING);
						MessageBox(cadena, NULL, MB_ICONINFORMATION|MB_OK);
						//MessageBox("Fichero guardado correctamente", NULL, MB_ICONINFORMATION|MB_OK);
					}else{
						LoadString(hLangDLL, ID_FILE_ERROR, cadena, MAX_LOADSTRING);
						MessageBox(cadena, NULL, MB_ICONINFORMATION|MB_OK);
						//MessageBox("No se pudo guardar el fichero correctamente", NULL, MB_ICONINFORMATION|MB_OK);
					}

				}else{
					LoadString(hLangDLL, ID_FILE_SAVE_CORRECT, cadena, MAX_LOADSTRING);
					MessageBox(cadena, NULL, MB_ICONINFORMATION|MB_OK);
					//MessageBox("Fichero guardado correctamente", NULL, MB_ICONINFORMATION|MB_OK);
				}
			}
		}

	}else {
		SetFocus();
	}
}

int DlgPaso4::ComprobarNombre(CString nombre)
{

	CString subcadena;

	if( (nombre.GetLength()>12) || (nombre.GetLength()<12) ){
		return 0;
	}
	
	subcadena = nombre.Mid(0,5);

	if( (subcadena.CompareNoCase("CRYF_")) != 0) {
		return 0;
	}

	subcadena = nombre.Mid(5,3);

	if (subcadena.SpanIncluding("0123456789")!= subcadena){
		return 0;
	}

	subcadena = nombre.Mid(8,4);

	if( (subcadena.CompareNoCase(".cla")) != 0) {
		return 0;
	}

	return 1;
}

void DlgPaso4::OnBnClickedButtonGrabar()
{
	// TODO: Agregue aqu� su c�digo de controlador de notificaci�n de control
	char path [MAX_PATH+13+1], mask[MAX_PATH+3], aux[MAX_PATH];
	HANDLE hFindFile = 0;
	WIN32_FIND_DATA findData;
	DWORD err;

	int num_cert = m_ListaCert2.GetCount();

	if ( ! SHGetSpecialFolderPath(NULL, path, CSIDL_CDBURN_AREA, FALSE) ) {
		LoadString(hLangDLL, ID_ERROR_PATH, cadena, MAX_LOADSTRING);
		MessageBox(cadena, NULL, MB_ICONINFORMATION|MB_OK);
		//MessageBox("ERROR, no se pudo obtener el path para la grabaci�n", NULL, MB_ICONINFORMATION|MB_OK);
		goto Fin;
	}

	/* Delete the files in the stagged area
	 */

	memset(mask, 0, MAX_PATH+3);
	strncpy(mask, path, MAX_PATH);
	strcat(path, "\\*");

	hFindFile = FindFirstFile(path, &findData);
	if ( hFindFile != INVALID_HANDLE_VALUE ) {
		
		do {
			
			if ( (strcmp(findData.cFileName,".") !=0) && (strcmp(findData.cFileName,"..") !=0) ){
				strcpy(aux, mask);
				strcat(aux, "\\");
				strcat(aux, findData.cFileName);
				if ( ! DeleteFile(aux) ) {
					// ERROR. No se ha podido borrar la stagging area //
					LoadString(hLangDLL, ID_ERROR_STAGGING, cadena, MAX_LOADSTRING);
					MessageBox(cadena, NULL, MB_ICONINFORMATION|MB_OK);
					//MessageBox("ERROR. No se ha podido borrar la stagging area", NULL, MB_ICONINFORMATION|MB_OK);
					goto Fin;
				}
			}

		} while ( FindNextFile(hFindFile, &findData) );

		err = GetLastError();

		FindClose(hFindFile);

		if ( err != ERROR_NO_MORE_FILES ) {
			//ERROR. No se ha podido borrar la stagging area//
			LoadString(hLangDLL, ID_ERROR_STAGGING, cadena, MAX_LOADSTRING);
			MessageBox(cadena, NULL, MB_ICONINFORMATION|MB_OK);
			//MessageBox("ERROR. No se ha podido borrar la stagging area", NULL, MB_ICONINFORMATION|MB_OK);
			goto Fin;
		}
	}

	strcat(mask, "\\CRYF_000.cla");

	if ( format_crear_fichero_cripto(mask,((CGestorCDDlg *) this->GetParent())->GetPin().GetBuffer(0),((CGestorCDDlg *) this->GetParent())->GetNumBloque())	) {
		LoadString(hLangDLL, ID_ERROR_CREATE_FILE, cadena, MAX_LOADSTRING);
		MessageBox(cadena, NULL, MB_ICONINFORMATION|MB_OK);
		//MessageBox("Error creando el fichero", NULL, MB_ICONINFORMATION|MB_OK);
		goto Fin;
	}

	if ( num_cert != 0 ) {
		if ( ! ((CGestorCDDlg *) this->GetParent())->Importar(mask) ) {
			LoadString(hLangDLL, ID_ERROR_FILE_SAVE_BURN, cadena, MAX_LOADSTRING);
			MessageBox(cadena, NULL, MB_ICONINFORMATION|MB_OK);
			//MessageBox("No se pudo guardar el fichero correctamente para la grabaci�n", NULL, MB_ICONINFORMATION|MB_OK);
			goto Fin;
		}
	}

	((CGestorCDDlg *) this->GetParent())->ShowWindow(FALSE);

	HRESULT hr;
	ICDBurn *iCDBurn = NULL;

	if ( ! SUCCEEDED(CoInitialize(NULL)) ) {
		/* ERROR */
		LoadString(hLangDLL, ID_ERROR_INIT_BURN, cadena, MAX_LOADSTRING);
		MessageBox(cadena, NULL, MB_ICONINFORMATION|MB_OK);
		//MessageBox("ERROR. Inicializando grabaci�n", NULL, MB_ICONINFORMATION|MB_OK);
		goto Fin;
	}

	hr = CoCreateInstance(CLSID_CDBurn,
						  NULL, 
						  CLSCTX_INPROC_SERVER|CLSCTX_LOCAL_SERVER, 
						  IID_ICDBurn,
						  (void **) &iCDBurn);

	if ( ! SUCCEEDED(hr) ) {
		/* ERROR */
		LoadString(hLangDLL, ID_ERROR_ASSITENT, cadena, MAX_LOADSTRING);
		MessageBox(cadena, NULL, MB_ICONINFORMATION|MB_OK);
		//MessageBox("ERROR. Instanciando asistente", NULL, MB_ICONINFORMATION|MB_OK);
		goto Fin;
	}
	
	if ( ! SUCCEEDED(iCDBurn->Burn(NULL)) ) {
		/* ERROR */
		LoadString(hLangDLL, ID_ERROR_BURN, cadena, MAX_LOADSTRING);
		MessageBox(cadena, NULL, MB_ICONINFORMATION|MB_OK);
		//MessageBox("ERROR. No se pudo grabar", NULL, MB_ICONINFORMATION|MB_OK);
		goto Fin;
	}

Fin:
	((CGestorCDDlg *) this->GetParent())->ShowWindow(TRUE);
}

void DlgPaso4::OnActivate(UINT nState, CWnd* pWndOther, BOOL bMinimized)
{
	CDialog::OnActivate(nState, pWndOther, bMinimized);

	// TODO: Agregue aqu� su c�digo de controlador de mensajes

}

int DlgPaso4::OnCreate(LPCREATESTRUCT lpCreateStruct)
{
	if (CDialog::OnCreate(lpCreateStruct) == -1)
		return -1;

	// TODO:  Agregue aqu� su c�digo de creaci�n especializado

	this->hLangDLL = ((CGestorCDDlg *) this->GetParent())->ReturnLanguage();

	return 0;
}
