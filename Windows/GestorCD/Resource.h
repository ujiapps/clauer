//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by GestorCD.rc
//
#define IDABOUT                         3
#define IDM_ABOUTBOX                    0x0010
#define IDD_ABOUTBOX                    100
#define IDS_ABOUTBOX                    101
#define IDD_GESTORCD_DIALOG             102
#define IDR_MAINFRAME                   128
#define IDD_DIALOG_PASO_1               130
#define IDD_DIALOG_PASO_2               131
#define IDD_DIALOG_PASO_3               132
#define IDD_DIALOG_PASO_4               133
#define IDB_BITMAP1                     135
#define IDI_LOGO_UJI                    138
#define IDD_DIALOG_PASSWD_PKCS12        139
#define IDC_EDIT_PASSWD                 1001
#define IDC_CONFIRMACION                1002
#define IDC_RUTA                        1002
#define IDC_BUTTON_ESC                  1003
#define IDC_BUTTON_ANYADIR              1005
#define IDC_LIST1                       1006
#define IDCANTERIOR                     1007
#define IDC_BUTTON_ELIMINAR             1007
#define IDSIGUIENTE                     1008
#define IDC_BUTTON_ELIMINAR_TODO        1008
#define IDC_FRAME                       1009
#define IDC_LOGO                        1010
#define IDC_BUTTON_GRABAR               1012
#define IDC_BUTTON_GUARDAR              1013
#define IDC_EDIT_CONTRASENYA_CERT       1013
#define IDC_TEXTO_PASO_1                1014
#define IDC_TEXTO_PASO_2                1015
#define IDC_TEXTO_PASO_4                1016
#define IDC_NUM_CERT                    1017
#define IDC_CUADRO                      1018
#define IDC_PASSWORD                    1019
#define IDC_CONF_PASS                   1020
#define IDC_CUADRO3                     1021
#define IDC_CUADRO4                     1022
#define IDC_TOTALCERT                   1023
#define IDC_TEXTO_VERSION               1024
#define IDC_COPYRIGHT                   1025

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        141
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1026
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
