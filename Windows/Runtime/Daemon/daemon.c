#define _WIN32_WINNT 0x0500
#define WINVER		 0x0500

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>


#include <winsock2.h>
/*#include <winsock.h>*/
#include <windows.h>
#include <winuser.h>
#include <malloc.h>

/* Includes propios */
#include <CAPI_PKCS5/capi_pkcs5.h>
#include <UsbLowLevel/usb_lowlevel.h>
#include <daemon.h>


/* Macros */

#define BLOQUE_ES_VACIO(b)		( (*(b) >= 0) && (*(b) <= 84) )
#define BLOQUE_ES_CLARO(b)		( (*(b) >= 85) && (*(b) <= 169) )
#define BLOQUE_ES_CIFRADO(b)	( (*(b) >= 170) && (*(b) <= 254) )

/* Indica si se permiten conexiones desde fuera(0) o no(1) */
#define SOLO_CONEXIONES_LOCALES		0

enum bool {false,true};

char nulo[1] = "\0", uno[1] = "\1";

/* Tiempo para el timer, en segundos */
int timeout=600;
/* Tiempo en milisegundos */
int timeout_ms;

SOCKET sockfd;

LOG_HANDLE logHandle;

volatile int vivo = true;

struct parametros {
	SOCKET sockcli;
	struct sockaddr_in otro;
};

struct info_dispositivo {

	int modo_pwd;	/* modo_pwd=1 es funcionamiento con password, modo_pwd=0 es funcionamiento sin password */
	HANDLE_DISPOSITIVO handle;
	HCRYPTPROV hProv;
	HCRYPTKEY hKey;
	char path[MAX_PATH_LEN];
	long int sector_ini;
	char id [20];
	char salt [20];
	int tam_zona_reserv;
	int current_block;
	int total_bloques;
	int ref_count;		/* N�mero de referencias abiertas al dispositivo */

};

struct DISPOSITIVO_INSERTADO {

	SOCKET sockcli;
	struct info_dispositivo* info;

};

struct DISPOSITIVO_INSERTADO * disp_insertados=NULL;
unsigned int num_disp_insertados = 0;

/************************/
/* FUNCIONES AUXILIARES */
/************************/

int anyadir_dispositivo( SOCKET sockcli, struct info_dispositivo *info_disp ) {

	num_disp_insertados++;		/* Tenemos un dispositivo m�s */

	/* Ampliamos la zona de memoria */

	disp_insertados = realloc ( disp_insertados, num_disp_insertados * sizeof(struct DISPOSITIVO_INSERTADO) );
	if ( disp_insertados == NULL ) return ERR_DAEMON_SI;	

	/* Rellenamos los datos para ese dispositivo, en el �ltimo espacio de disp_insertados */

		/* Copiamos el socket */

	disp_insertados[num_disp_insertados-1].sockcli = sockcli;

		/* Copiamos la estructura de informaci�n */

	disp_insertados[num_disp_insertados-1].info = malloc ( sizeof( struct info_dispositivo ) );
	if ( disp_insertados[num_disp_insertados-1].info == NULL ) return ERR_DAEMON_SI;

	disp_insertados[num_disp_insertados-1].info->modo_pwd = info_disp->modo_pwd;
	disp_insertados[num_disp_insertados-1].info->handle = info_disp->handle;
	disp_insertados[num_disp_insertados-1].info->hProv = info_disp->hProv;
	disp_insertados[num_disp_insertados-1].info->hKey = info_disp->hKey;
	
	memset(disp_insertados[num_disp_insertados-1].info->path,0,MAX_PATH_LEN);
	strncpy(disp_insertados[num_disp_insertados-1].info->path,info_disp->path,MAX_PATH_LEN);
	disp_insertados[num_disp_insertados-1].info->sector_ini = info_disp->sector_ini;
	disp_insertados[num_disp_insertados-1].info->tam_zona_reserv = info_disp->tam_zona_reserv;
	disp_insertados[num_disp_insertados-1].info->current_block = info_disp->current_block;
	disp_insertados[num_disp_insertados-1].info->total_bloques = info_disp->total_bloques;
	disp_insertados[num_disp_insertados-1].info->ref_count = info_disp->ref_count;

	memcpy( disp_insertados[num_disp_insertados-1].info->id, info_disp->id, 20 );
	memcpy( disp_insertados[num_disp_insertados-1].info->salt, info_disp->salt, 20 );

	return ERR_DAEMON_NO;
}

int eliminar_dispositivo( LOG_HANDLE logHandle ) {

	int nDisp;
	unsigned char encontrado=0;
	unsigned int i,j,indice;
	int result;
	unsigned int posicion;
	char *dispositivos[MAX_DEVICES];

	/* PROCESO DE BUSQUEDA de dispositivo retirado */

	result = lowlevel_enumerar_dispositivos_ex(dispositivos,&nDisp);
	if ( result != ERR_DAEMON_NO ) return ERR_DAEMON_SI;

		/* Debemos averiguar cu�l se ha retirado, por el n�mero de dispositivo */

	for ( i=0; i<num_disp_insertados; i++ ) {

		encontrado = 0;

		for ( j=0; j<(unsigned int)nDisp; j++ ) {
			/* Comparamos por el n�mero identificador del dispositivo */
			if ( strncmp(dispositivos[j], disp_insertados[i].info->path, MAX_PATH_LEN) == 0 ) 
				encontrado = 1;
		}

		if ( ! encontrado ) {	/* Si lo hemos encontrado, no hacemos nada */

			posicion = i;	/* La posici�n del bloque a eliminar de la estructura global */


			/* PROCESO DE REESTRUCTURACION de memoria */

				/* Debemos desplazar los bloques desde el final hacia delante para dejar el hueco al final */

			for ( indice = (posicion+1) ; indice < num_disp_insertados ; indice++ ) {

				/* Copiamos de indice a indice-1 */

					/* Copiamos el socket */

				disp_insertados[indice-1].sockcli = disp_insertados[indice].sockcli;

					/* Copiamos la estructura de informaci�n */

				disp_insertados[indice-1].info->modo_pwd = disp_insertados[indice].info->modo_pwd;
				disp_insertados[indice-1].info->handle = disp_insertados[indice].info->handle;
				disp_insertados[indice-1].info->hProv = disp_insertados[indice].info->hProv;
				disp_insertados[indice-1].info->hKey = disp_insertados[indice].info->hKey;
				memset(disp_insertados[indice-1].info->path, 0, MAX_PATH_LEN);
				strncpy(disp_insertados[indice-1].info->path, disp_insertados[indice].info->path, MAX_PATH_LEN);
				disp_insertados[indice-1].info->sector_ini = disp_insertados[indice].info->sector_ini;
				disp_insertados[indice-1].info->tam_zona_reserv = disp_insertados[indice].info->tam_zona_reserv;
				disp_insertados[indice-1].info->current_block = disp_insertados[indice].info->current_block;
				disp_insertados[indice-1].info->total_bloques = disp_insertados[indice].info->total_bloques;

				memcpy( disp_insertados[indice-1].info->id, disp_insertados[indice].info->id, 20 );
				memcpy( disp_insertados[indice-1].info->salt, disp_insertados[indice].info->salt, 20 );

			}

			/* PROCESO DE LIBERACION de memoria */

			num_disp_insertados = num_disp_insertados - 1;

				/* Liberamos la memoria del �ltimo bloque */
			free(disp_insertados[num_disp_insertados].info);

				/* Aqu� se supone que realloc libera la memoria del final del buffer */
			disp_insertados = realloc ( disp_insertados, num_disp_insertados * sizeof(struct DISPOSITIVO_INSERTADO) );
			if ( ( disp_insertados == NULL ) && ( num_disp_insertados > 0 ) ) return ERR_DAEMON_SI;

			/* PROCESO DE CIERRE de handles */

				/* Cerramos el socket y el dispositivo */

			result = shutdown(disp_insertados[posicion].sockcli, SD_BOTH);
			if ( result == SOCKET_ERROR ) return ERR_DAEMON_CERRANDO_SOCKET;

			result = closesocket(disp_insertados[posicion].sockcli);
			if ( result == SOCKET_ERROR ) return ERR_DAEMON_CERRANDO_SOCKET;

			if (disp_insertados[posicion].info->handle != NULL) {
				result = lowlevel_cerrar_dispositivo( disp_insertados[posicion].info->handle );
				if ( result != ERR_LOWLEVEL_NO ) return ERR_DAEMON_CERRANDO_DISPOSITIVO;
			}
		}

	}

	return ERR_DAEMON_NO;
}

int envia(SOCKET sockcli, char *buffer, int tamanyo) {

	int enviados = 0, n;

	while (enviados < tamanyo) {
		n = send(sockcli, buffer+enviados, tamanyo-enviados, 0);

		if ( n == SOCKET_ERROR )
			return ERR_DAEMON_ENVIANDO;

		enviados += n;
	}

	return ERR_DAEMON_NO;
}

void salir(SOCKET *psockcli, char *msg) {

	char cadena[512];

	sprintf(cadena,"daemon: Terminating on error: %s\n", msg);
	LOG_Escribir(logHandle, cadena);

	if (psockcli != NULL) envia(*psockcli, uno, 1);

}

int recibe(SOCKET sockcli, char *buffer, int tamanyo) {

	int recibidos = 0, n;

	while ( recibidos < tamanyo ) {

		LOG_Escribir(logHandle, "Antes del recv\n");
		n = recv(sockcli, buffer+recibidos, tamanyo-recibidos, 0);
		LOG_Escribir(logHandle, "Despu�s del recv\n");

		if ((n == SOCKET_ERROR) && (WSAGetLastError() == WSAETIMEDOUT)) {
			LOG_Escribir(logHandle, "daemon: Tiempo maximo de espera excedido\n");
			return ERR_DAEMON_TIMED_OUT;
		}

		if ( (n == 0) || (n == SOCKET_ERROR) ) {
			LOG_Escribir(logHandle, "daemon: El cliente ha cerrado la conexion\n");
			return ERR_DAEMON_CONNECTION_CLOSED;
		}

		recibidos += n;

	}

	return ERR_DAEMON_NO;
}


void enviar_resultado(SOCKET sockcli, int result) {

	envia(sockcli, (char *)&result, 1);
}




int listar_dispositivos (int **dispositivos, int *nDispositivos)
{
	int num_dispositivos_total=0;
	int *dispositivos_totales;
	char **unidades;

	int contador=1,i;
	int *ptr;
	int indice;
	int result;
	int es;
	int j;
	HANDLE_DISPOSITIVO handle;


	/* Obtenemos los dispositivos en 2 pasos: */
	
	/*	1.- Listamos todos los dispositivos */
	/*  2.- Filtramos los que no son usbcert y enviamos s�lo los buenos */


	/* N�mero total de dispositivos */
	if ( lowlevel_enumerar_dispositivos( NULL,NULL,&num_dispositivos_total ) != 0 ) {
		return 1;
	}

	/* Reserva de espacio */
	unidades = (char **) malloc (num_dispositivos_total*sizeof(char *));
	if ( !unidades ) 
		return 1;

	for (i=0; i<num_dispositivos_total; i++) {
		unidades[i] = (char *) malloc (5*sizeof(char));
		if ( !unidades[i] ) {
			for ( j=0; j< i; j++ ) {
				free(unidades[i]);
				free(unidades);
				return 1;
			}
		}
	}

	dispositivos_totales = (int *) malloc (num_dispositivos_total * sizeof(int));
	if ( !dispositivos_totales ) {
		for ( i = 0 ; i < num_dispositivos_total ; i++ )
			free(unidades[i]);
		free(unidades);
		return 1;
	}
	*dispositivos = (int *) malloc (num_dispositivos_total * sizeof(int));

	if ( !(*dispositivos) ) {
		for ( i = 0 ; i < num_dispositivos_total ; i++ )
			free(unidades[i]);
		free(unidades);
		return 1;
	}


	/* Obtenemos los identificadores */
	if (lowlevel_enumerar_dispositivos( unidades, dispositivos_totales, &num_dispositivos_total ) != 0 ) {
		LOG_Escribir(logHandle, "ERROR.5\n");
		free(*dispositivos);
		for ( i = 0 ; i < num_dispositivos_total ; i++ )
			free(unidades[i]);
		free(unidades);
		return 1;
	}

	/* Filtramos los que no son usbcerts */

	*nDispositivos = 0;
	ptr = *dispositivos;
	for (indice=0;indice<num_dispositivos_total;indice++) {

		result = lowlevel_abrir_dispositivo( dispositivos_totales[indice], &handle );
		if ( result != ERR_LOWLEVEL_NO ) {
			free(*dispositivos);
			for ( i = 0 ; i < num_dispositivos_total ; i++ )
				free(unidades[i]);
			free(unidades);			
			return ERR_DAEMON_COMPROBANDO_DISPOSITIVO;
		}

		result = lowlevel_es_usbcert( handle, &es );
		if ( result != ERR_LOWLEVEL_NO ) {
			free(*dispositivos);
			for ( i = 0 ; i < num_dispositivos_total ; i++ )
				free(unidades[i]);
			free(unidades);
			return ERR_DAEMON_COMPROBANDO_DISPOSITIVO;
		}

		if (es) {

			/* Copiamos el dispositivo como v�lido */
			
			/* Identificador */
			*ptr = dispositivos_totales[indice];
			ptr++;

			/* N�mero de dispositivos */
			(*nDispositivos)++;

		}

		result = lowlevel_cerrar_dispositivo( handle );
		if ( result != ERR_LOWLEVEL_NO ) {
			free(*dispositivos);
			for ( i = 0 ; i < num_dispositivos_total ; i++ )
				free(unidades[i]);
			free(unidades);

			return ERR_DAEMON_COMPROBANDO_DISPOSITIVO;
		}
	}

	free(dispositivos_totales);

	for (i=0;i<num_dispositivos_total; i++)
		free(unidades[i]);

	free(unidades);

	return ERR_DAEMON_NO;
}



#if 0
/* Devuelve TRUE si el identificador que indica info_disp
 * es el mismo que el del dispositivo al que apunta.
 *
 * Devuelve FALSE si el dispositivo no tiene el n�mero de serie
 * indicado en info_disp o si se produjo un error en la comprobaci�n.
 */

BOOL id_correcto (struct info_dispositivo *info_disp)
{

	BYTE bloqueCifrado[5120], bloqueDescifrado[5120];
	int tamBloqueDescifrado;
	
	if ( lowlevel_leer_buffer(info_disp->handle, bloqueCifrado, 0, (TAM_BLOQUE/512)) != 0 ) 
		return FALSE;
	
	/* Por cuestiones de seguridad. S�lo descifro lo estrictamente necesario: 32+20 = 52 bytes.
	 * De esa forma me quedo s�lo con el id */

	if ( CRYPTO_PBE_Descifrar(info_disp->pass, info_disp->salt, 0, CRYPTO_CIPHER_AES_128_CBC,bloqueCifrado,52,bloqueDescifrado,&tamBloqueDescifrado) != 0 ) {
		CRYPTO_Random(5120, bloqueDescifrado);
		return FALSE;
	}
	
	if ( memcmp(bloqueDescifrado+32,info_disp->id,20) == 0 ) {
		CRYPTO_Random(5120, bloqueDescifrado);
		return TRUE;
	} else {
		CRYPTO_Random(5120,bloqueDescifrado);
		return FALSE;
	}

}
#endif





/*************/
/* FUNCIONES */
/*************/


/*----------------------------------------------*/
/* Funci�n 0									*/
/*----------------------------------------------*/
/* Listar dispositivos disponibles				*/
/*												*/
/* Salida:										*/
/*		- numero de dispos OK (1 byte)			*/
/*		- num del primer dispositivo (1 byte)	*/
/*		- num del segund dispositivo (1 byte)	*/
/*		- ...									*/
/*----------------------------------------------*/

int f0(SOCKET sockcli) {
	
	int num_dispositivos=0;
	char * dispositivos[MAX_DEVICES];
	int contador,i;
	int result;


	/* N�mero total de dispositivos */
	result = lowlevel_enumerar_dispositivos_ex( dispositivos, &num_dispositivos );
	if ( result != ERR_LOWLEVEL_NO ) 
		return ERR_DAEMON_COMPROBANDO_DISPOSITIVO;

	/* Enviamos el n�mero de dispositivos */
	result = envia(sockcli, (char *)&num_dispositivos, 1);
	if (result == ERR_DAEMON_ENVIANDO) {
		for(i= 0; i< MAX_DEVICES && dispositivos[i]; i++ )
			free(dispositivos[i]);
		return ERR_DAEMON_SI;
	}


	/* Enviamos los identificadores */

	contador = 0;
	while (contador < num_dispositivos) {
		int aux;
		aux= strlen(dispositivos[contador]);
		result = envia(sockcli, ( char * ) &aux, 4);
		if (result == ERR_DAEMON_ENVIANDO) {
			for(i= 0; i< MAX_DEVICES && dispositivos[i]; i++ )
				free(dispositivos[i]);
			return ERR_DAEMON_SI;
		}
		result = envia(sockcli, ( char * ) dispositivos[contador], aux);
		if (result == ERR_DAEMON_ENVIANDO) {
			for(i= 0; i< MAX_DEVICES && dispositivos[i]; i++ )
				free(dispositivos[i]);
			return ERR_DAEMON_SI;
		}
		contador++;
	}


	for(i= 0; i< MAX_DEVICES && dispositivos[i]; i++ )
		free(dispositivos[i]);
	

	return ERR_DAEMON_NO;
}

/* Todas las funciones a continuaci�n devuelven 1 (error) o 0 (no error) en 1 byte */

/*----------------------------------------------*/
/* Funci�n 1									*/
/*----------------------------------------------*/
/* Iniciar dispositivo							*/
/*												*/
/* Entrada:										*/
/*		- num del dispos elegido (1 byte)		*/
/*		- num de chars pswd (1 byte)			*/
/*		- pswd (n bytes)			(opcional)	*/
/* Salida:										*/
/*      - id dispositivo (20 bytes)             */
/*----------------------------------------------*/

/* NOTA: Si el n�mero de caracteres de la password recibidos es 0, no espera recepci�n de password */
/*		 Este es el funcionamiento sin password */

int f1(SOCKET sockcli, struct info_dispositivo *info_disp) {

	int result, tamPath;
	char path[MAX_PATH_LEN], *dispositivos[MAX_DEVICES];
	unsigned char num_caract;
	HANDLE_DISPOSITIVO handle;
	HANDLE hFindFile;
	WIN32_FIND_DATA findData;
	PARTICION particiones[4];
	BYTE *buffer = NULL, *salt = NULL, *ptr = NULL;
	BYTE bloque[TAM_BLOQUE];
	char iden[32]="UJI - Clauer PKI storage system";
	char *pass = NULL;
	int nDisp;
	int i;
	unsigned char encontrado;
	int tamBloque;
	unsigned int indice;
	int err = 0;
	int esClauer;

	/* Recibimos el tama�o del path del dispositivo */

	LOG_Escribir(logHandle, "Recibe 1");

	result = recibe(sockcli, (char *)&tamPath, 4);
	if ((result == ERR_DAEMON_TIMED_OUT) || (result == ERR_DAEMON_RECIBIENDO)) 
		return ERR_DAEMON_SI;
	if ( tamPath >= MAX_PATH_LEN ) 
		return ERR_DAEMON_SI;

	/* Recibimos el path en s� */

	LOG_Escribir(logHandle, "Recibe 2");

	result = recibe(sockcli, (char *) path, tamPath);
	if ((result == ERR_DAEMON_TIMED_OUT) || (result == ERR_DAEMON_RECIBIENDO)) 
		return ERR_DAEMON_SI;
	path[tamPath] = 0;

	/* Recibimos el n�mero de caracteres de la password */

	LOG_Escribir(logHandle, "Recibe 3");

	result = recibe(sockcli, &num_caract, 1);
	if ((result == ERR_DAEMON_TIMED_OUT) || (result == ERR_DAEMON_RECIBIENDO)) 
		return ERR_DAEMON_SI;

	if ( num_caract >= MAX_PASS_LEN ) 
		return ERR_DAEMON_SI; 	

	/* Funcionamiento con password */

	if ( num_caract > 0 ) 
		info_disp->modo_pwd = 1;
	else 
		info_disp->modo_pwd = 0;

	if ( info_disp->modo_pwd ) {

		/* Recibimos la password */

		pass = (char *) malloc ((num_caract+1)*sizeof(char));
		if ( pass == NULL ) {
			info_disp->modo_pwd = 0;
			return ERR_DAEMON_SI;
		}

		LOG_Escribir(logHandle, "Recibe 4");

		result = recibe(sockcli, pass, num_caract);
		if ((result == ERR_DAEMON_TIMED_OUT) || (result == ERR_DAEMON_RECIBIENDO)) {
			SecureZeroMemory(pass, num_caract+1);
			free(pass);
			info_disp->modo_pwd = 0;
			return ERR_DAEMON_SI;
		}

		pass[num_caract]='\0';
	}


	/* Comprobamos si el dispositivo ya estaba abierto */

	encontrado = 0;

	if ( num_disp_insertados > 0 ) {

		/* Buscamos el dispositivo por si ya estaba abierto */

		indice = 0;

		while ( ( !encontrado ) && ( indice<num_disp_insertados ) ){

			if ( !strncmp(disp_insertados[indice].info->path,path,MAX_PATH_LEN))
				encontrado=1;
			else 
				indice++;
		}

	}

	LOG_Escribir(logHandle, "Recibe 5");

	/* No estaba abierto previamente */

	if ( ! encontrado) {
		char uds[4];
		
		/* Ya tenemos los datos, inicializamos el dispositivo */

		/* Lo abrimos */

			LOG_Escribir(logHandle, "Recibe 6");

		result = lowlevel_abrir_dispositivo_ex( path , &handle );
		if ( result != ERR_LOWLEVEL_NO ) {
			if ( info_disp->modo_pwd ) {
				SecureZeroMemory(pass, num_caract + 1);
				free(pass);
			}
			info_disp->modo_pwd = 0;
			return ERR_DAEMON_SI;
		}

			LOG_Escribir(logHandle, "Recibe 7");

		/* Comprobamos si el dispositivo es un Clauer o no.
		 * Esta comprobaci�n depender� de si el dispositivo
		 * es un disco o un fichero.
		 */

		hFindFile = FindFirstFile(path, &findData);
		if ( hFindFile != INVALID_HANDLE_VALUE ) {

			/* El clauer es un fichero
			 */


				LOG_Escribir(logHandle, "Es fichero");

			 if ( lowlevel_es_fichero_clauer(path, &esClauer) != ERR_LOWLEVEL_NO ) {
				 if ( info_disp->modo_pwd ) {
					SecureZeroMemory(pass, num_caract+1);
					free(pass);
				 }
				info_disp->modo_pwd = 0;
				return ERR_DAEMON_SI;
			 }

			 if ( esClauer )
				err = 0;
			 else
				err = 1;
			
			 /* Para reaprovechar el c�digo de la Lowlevel de manejo de
			  * dispositivos, lo que hacemos es "enga�ar" al servicio
			  * indicando que el rel_sectors de la cuarta partici�n
			  * es 0
			  */

			 memset(particiones,0,sizeof(PARTICION) * 4);

		} else {

			/* Tenemos un disco
			 */

			if ( lowlevel_es_usbcert(handle, &esClauer) != ERR_LOWLEVEL_NO ) {
				if ( info_disp->modo_pwd ) {
					SecureZeroMemory(pass, num_caract+1);
					free(pass);
				}
				info_disp->modo_pwd = 0;
				return ERR_DAEMON_SI;
			 }

			 if ( esClauer )
				err = 0;
			 else
				err = 1;

			FindClose(hFindFile);

			/* Leemos la tabla de particiones */

			result = lowlevel_leer_particiones( handle, particiones );
			if ( result != ERR_LOWLEVEL_NO ) {
				if ( info_disp->modo_pwd ) {
					SecureZeroMemory(pass, num_caract+1);
					free(pass);
				}
				info_disp->modo_pwd = 0;
				return ERR_DAEMON_SI;
			}

		} /* if es fichero o dispositivo */



		if ( ! err ) {

			/* Si el dispositivo es un Clauer (tanto fichero como dispositivo)
			 */

			if ( info_disp->modo_pwd ) {

				/* Leemos el salt (20 bytes) */
				salt = (BYTE *) malloc (20*sizeof(char));
				if ( salt == NULL ) {
					info_disp->modo_pwd = 0;
					return ERR_DAEMON_SI;
				}

				result = lowlevel_leer_salt( handle, particiones[3], salt );
				if ( result != ERR_LOWLEVEL_NO ) {
					SecureZeroMemory(pass, num_caract+1);
					free(pass);
					free(salt);
					info_disp->modo_pwd = 0;
					return ERR_DAEMON_SI;
				}

			}

			/* Leemos la zona de informaci�n */

			result = lowlevel_leer_buffer( handle, bloque, particiones[3].rel_sectors, TAM_BLOQUE/512 );
			if ( result != ERR_LOWLEVEL_NO ) {
				if ( info_disp->modo_pwd ) {
					SecureZeroMemory(pass, num_caract+1);
					free(pass);
					free(salt);
				}
				info_disp->modo_pwd = 0;
				return ERR_DAEMON_SI;
			}

			if ( info_disp->modo_pwd ) {
				/* Inicializamos el PKCS#5 */
				if (! CAPI_PKCS5_3DES_PBE_Init( pass, salt, 20, 1000, &(info_disp->hProv), &(info_disp->hKey) ) ) {
					info_disp->modo_pwd = 0;
					return ERR_DAEMON_SI;
				}

				/* Desciframos el IDEN String y lo comprobamos */
				buffer = (BYTE *) malloc (40*sizeof(char));
				if ( buffer == NULL ) {
					info_disp->modo_pwd = 0;
					return ERR_DAEMON_SI;
				}

				memcpy(buffer, bloque, 40);

				tamBloque = 40;
				if ( !CryptDecrypt(info_disp->hKey, 0, TRUE, 0, buffer, &tamBloque) ) {
					info_disp->modo_pwd = 0;
					return ERR_DAEMON_SI;
				}

				/* Comprobaci�n del IDEN String */
				if (strncmp(buffer, iden, 32) != 0) {
					SecureZeroMemory(pass, num_caract+1);
					free(pass);
					free(salt);
					free(buffer);
					info_disp->modo_pwd = 0;
					return ERR_DAEMON_SI;
				}

			}  /* si modo autenticado */

			/* Obtenemos la informaci�n y nos la guardamos para uso posterior */

			if ( info_disp->modo_pwd ) {
				/* El salt */
				memcpy(info_disp->salt, salt, 20);
			}

			/* Handle */
			info_disp->handle = handle;

			info_disp->ref_count = 1;

			/* Numero de dispositivo */
			memset(info_disp->path, 0, MAX_PATH_LEN);
			strncpy(info_disp->path,path, MAX_PATH_LEN);

			/* Sector inicial de la partici�n crypto */
			info_disp->sector_ini = particiones[3].rel_sectors;

			ptr=bloque;
			ptr+=40;				/* Saltamos el IDEN string */

			/* Id */
			memcpy(info_disp->id,ptr,20);
			ptr+=20;

			/* Tama�o de la zona reservada */
			memcpy(&(info_disp->tam_zona_reserv), ptr, 4);
			ptr+=4;

			/* Current Block */
			info_disp->current_block=*((int *)ptr);
			ptr+=4;

			/* Total Bloques */
			info_disp->total_bloques=*((int *)ptr);
			ptr+=4;

			if ( anyadir_dispositivo( sockcli, info_disp ) != ERR_DAEMON_NO ) {

				if ( info_disp->modo_pwd ) {
					free(buffer);
					SecureZeroMemory(pass, num_caract+1);
					free(pass);
					free(salt);
				}

				return ERR_DAEMON_SI;
			}

		}

		/* A�adimos el dispositivo a la lista de dispositivos globales */


	} else {	
		
		/* El dispositivo ya estaba abierto, copiamos los datos */

		char uds[4];

		/* Copiamos los datos del dispositivo */
		info_disp->handle = disp_insertados[indice].info->handle;
		memset(info_disp->path,0,MAX_PATH_LEN);
		strncpy(info_disp->path,disp_insertados[indice].info->path,MAX_PATH_LEN);
		info_disp->sector_ini = disp_insertados[indice].info->sector_ini;
		info_disp->tam_zona_reserv = disp_insertados[indice].info->tam_zona_reserv;
		info_disp->current_block = disp_insertados[indice].info->current_block;
		info_disp->total_bloques = disp_insertados[indice].info->total_bloques;

		memcpy( info_disp->id, disp_insertados[indice].info->id, 20 );
	
		/* Comprobamos si estamos abriendo el dispositivo con password o no */

		if ( info_disp->modo_pwd ) {	/* Funcionamiento con password */

			/* Comprobaci�n de password */
			strncpy(uds,path,3);
			uds[3]= '\0';

			hFindFile = FindFirstFile(path, &findData);
			if ( hFindFile != INVALID_HANDLE_VALUE ) 
				memset(particiones,0,sizeof(PARTICION) * 4);
			else{
				FindClose(hFindFile);
				/* Leemos la tabla de particiones */
				result = lowlevel_leer_particiones( info_disp->handle, particiones );
				if ( result != ERR_LOWLEVEL_NO ) {
					if ( info_disp->modo_pwd ) {
						SecureZeroMemory(pass, num_caract+1);
						free(pass);
					}
					info_disp->modo_pwd = 0;
					return ERR_DAEMON_SI;
				}
			}

			/* Leemos el salt (20 bytes) */
			salt = (BYTE *) malloc ( 20 );
			if ( salt == NULL ) {
				info_disp->modo_pwd = 0;
				return ERR_DAEMON_SI;
			}


			result = lowlevel_leer_salt( info_disp->handle, particiones[3], salt );
			if ( result != ERR_LOWLEVEL_NO ) {
				SecureZeroMemory(pass, num_caract+1);
				free(pass);
				free(salt);
				info_disp->modo_pwd = 0;
				return ERR_DAEMON_SI;
			}

			memcpy( info_disp->salt, disp_insertados[indice].info->salt, 20);

			/* Inicializamos el PKCS#5 */
			if (! CAPI_PKCS5_3DES_PBE_Init( pass, salt, 20, 1000, &(info_disp->hProv), &(info_disp->hKey) ) ) {
				info_disp->modo_pwd = 0;
				return ERR_DAEMON_SI;
			}

			/* Leemos la zona de informaci�n */
			result = lowlevel_leer_buffer( info_disp->handle, bloque, particiones[3].rel_sectors, (TAM_BLOQUE/512) );
			if ( result != ERR_LOWLEVEL_NO ) {
				SecureZeroMemory(pass, num_caract+1);
				free(pass);
				info_disp->modo_pwd = 0;
				return ERR_DAEMON_SI;
			}

			/* Desciframos el IDEN String y lo comprobamos */

			buffer = (BYTE *) malloc (40);
			if ( ! buffer ) {
				info_disp->modo_pwd = 0;
				return ERR_DAEMON_SI;
			}

			memcpy(buffer, bloque, 40);

			tamBloque = 40;
			if ( !CryptDecrypt(info_disp->hKey, 0, TRUE, 0, buffer, &tamBloque) ) {
				info_disp->modo_pwd = 0;
				return ERR_DAEMON_SI;
			}

			/* Comprobaci�n del IDEN String */

			if (strncmp(buffer, iden, 32) != 0) {
				SecureZeroMemory(pass, num_caract+1);
				free(pass);
				free(buffer);
				info_disp->modo_pwd = 0;
				return ERR_DAEMON_SI;
			}
		}

		/* Incrementamos las referencias de dispositivo abierto */

		disp_insertados[indice].info->ref_count = disp_insertados[indice].info->ref_count + 1;

		info_disp->ref_count = disp_insertados[indice].info->ref_count;

	}


	if ( info_disp->modo_pwd ) {
		if ( buffer )
			free(buffer);
		if ( pass ) {
			SecureZeroMemory(pass, num_caract+1);
			free(pass);
		}
		if ( salt )
			free(salt);
	}

	/* Enviar resultado */
	enviar_resultado(sockcli, err);

	/*
	 * Devolvemos el id del dispositivo 20 bytes
	 */

	if ( envia(sockcli, info_disp->id, 20) == ERR_DAEMON_ENVIANDO ) {
		info_disp->modo_pwd = 0;
		return ERR_DAEMON_SI;
	}

	return ERR_DAEMON_NO;
}


/*----------------------------------------------*/
/* Funci�n 2									*/
/*----------------------------------------------*/
/* Lee zona reservada							*/
/*												*/
/* Salida:										*/
/*		- num de bytes zona reserv (4 bytes)	*/
/*		- buffer (n bytes)						*/
/*----------------------------------------------*/

int f2(SOCKET sockcli, struct info_dispositivo *info_disp) {

	unsigned int tam;
	char* buffer;
	int result;

	/* N�mero de bytes a devolver de la zona reservada */
	tam = (info_disp->tam_zona_reserv)*TAM_BLOQUE;

	buffer = malloc(tam*sizeof(char));
	if ( buffer == NULL ) return ERR_DAEMON_SI;

	/* Leemos la zona reservada -> Un bloque ocupa 20 sectores, y leemos a partir de la zona reservada */

	result = lowlevel_leer_buffer( info_disp->handle, buffer, info_disp->sector_ini+(TAM_BLOQUE/512), info_disp->tam_zona_reserv*(TAM_BLOQUE/512));
	if ( result != ERR_LOWLEVEL_NO ) {
		free(buffer);
		return ERR_DAEMON_SI;
	}

	/* Enviamos el resultado */
	enviar_resultado(sockcli, 0);

	/* Enviamos el n�mero de bytes de la zona reservada */
	result = envia(sockcli, (char *)&tam, 4);
	if (result == ERR_DAEMON_ENVIANDO) {
		free(buffer);
		return ERR_DAEMON_SI;
	}

	/* Enviamos la zona reservada */
	result = envia(sockcli, (char *)buffer, tam);
	if (result == ERR_DAEMON_ENVIANDO) {
		free(buffer);
		return ERR_DAEMON_SI;
	}

	free(buffer);

	return ERR_DAEMON_NO;

}


/*----------------------------------------------*/
/* Funci�n 3									*/
/*----------------------------------------------*/
/* Escribe zona reservada						*/
/*												*/
/* Entrada:										*/
/*		- num de bytes a escr dsd ppio (4 bytes)*/
/*		- buffer (n bytes)						*/
/*----------------------------------------------*/

int f3(SOCKET sockcli, struct info_dispositivo *info_disp) {

	int result, nbytes;
	char *buffer;
	int num_bloques;
	char zona_info [TAM_BLOQUE];

	/* Recibimos el n�mero de bytes a escribir */
	result = recibe(sockcli, (char *)&nbytes, 4);
	if ((result == ERR_DAEMON_TIMED_OUT) || (result == ERR_DAEMON_RECIBIENDO))
		return ERR_DAEMON_SI;
	
	if ( nbytes <= 0 ) return ERR_DAEMON_SI;

	num_bloques = (nbytes/TAM_BLOQUE) + 1;

	buffer = (char *) malloc (num_bloques*TAM_BLOQUE*sizeof(char));
	if ( buffer == NULL ) return ERR_DAEMON_SI;

	/* Recibimos el buffer de la zona reservada */
	result = recibe(sockcli, buffer, nbytes);

	if ((result == ERR_DAEMON_TIMED_OUT) || (result == ERR_DAEMON_RECIBIENDO)) {
		free(buffer);
		return ERR_DAEMON_SI;
	}


	/* No se puede llamar a esta funci�n sin autenticaci�n previa */

	if ( !(info_disp->modo_pwd) ) {
		free(buffer);
		return ERR_DAEMON_SI;
	}

	/* Hacemos la escritura de todo el bloque */
	result = lowlevel_escribir_buffer( info_disp->handle, buffer, info_disp->sector_ini+(TAM_BLOQUE/512), num_bloques*(TAM_BLOQUE/512) );
	if ( result != ERR_LOWLEVEL_NO ) {
		free(buffer);
		return ERR_DAEMON_SI;
	}


	/* Ahora debemos modificar el tama�o de la zona reservada en la zona de informaci�n */

	/* Leemos */
	result = lowlevel_leer_buffer( info_disp->handle, zona_info, info_disp->sector_ini, (TAM_BLOQUE/512));
	if ( result != ERR_LOWLEVEL_NO ) {
		free(buffer);
		return ERR_DAEMON_SI;
	}

	/* Modificamos */
	memcpy(zona_info+60, &num_bloques, 4);
	info_disp->tam_zona_reserv=num_bloques;

	/* Escribimos */
	result = lowlevel_escribir_buffer( info_disp->handle, zona_info, info_disp->sector_ini, (TAM_BLOQUE/512));
	if ( result != ERR_LOWLEVEL_NO ) {
		free(buffer);
		return ERR_DAEMON_SI;
	}

	free(buffer);

	/* Enviamos resultado */

	enviar_resultado(sockcli, 0);

	return ERR_DAEMON_NO;

}


/*----------------------------------------------*/
/* Funci�n 4									*/
/*----------------------------------------------*/
/* Cambia password								*/
/*												*/
/* Entrada:										*/
/*		- num de bytes nueva pass (1 byte)		*/
/*		- pass (n bytes)						*/
/*----------------------------------------------*/

int f4(SOCKET sockcli, struct info_dispositivo *info_disp) {

	unsigned char nbytes;
	char *nueva_pass;
	int result;
	unsigned long iteraciones,indice,indice2;
	BYTE *buffer, *ptr;
	unsigned long sector_relativo, bytes_restantes;
	HCRYPTPROV hProvTmp;
	HCRYPTKEY hKeyTmp;
	int tamBuffer;
	char iden[32]="UJI - Clauer PKI storage system";


	/* Recibimos el n�mero de bytes de la nueva pass */
	result = recibe(sockcli, &nbytes, 1);
	if ((result == ERR_DAEMON_TIMED_OUT) || (result == ERR_DAEMON_RECIBIENDO)) 
		return ERR_DAEMON_SI;

	/* Para usar esta funci�n debemos haber autenticado mediante f1 previamente */
	if ( !info_disp->modo_pwd ) 
		return ERR_DAEMON_SI;

	nueva_pass = (char *) malloc ((nbytes+1)*sizeof(char));
	if ( nueva_pass == NULL )
		return ERR_DAEMON_SI;

	/* Recibimos la nueva password */
	result = recibe(sockcli, nueva_pass, nbytes);
	if ((result == ERR_DAEMON_TIMED_OUT) || (result == ERR_DAEMON_RECIBIENDO)) {
		free(nueva_pass);
		return ERR_DAEMON_SI;
	}

	nueva_pass[nbytes]='\0';

	/*
	   Debemos conseguir un hProv y hKey temporales para cifrar con la nueva pass y usar el hProv y hKey que ya tenemos
	   para descifrar los antiguos. Al final, asignaremos los hProv y hKey temporales a info_disp
	*/

	if (! CAPI_PKCS5_3DES_PBE_Init( nueva_pass, info_disp->salt, 20, 1000, &hProvTmp, &hKeyTmp ) )
		return ERR_DAEMON_SI;


	/* Ya tenemos los datos, procedemos al cambio de password */


	/* Cambiamos el cifrado del IDEN String */

	buffer = (BYTE *) malloc (512 * sizeof(char));
	if ( buffer == NULL ) {
		free(nueva_pass);
		return ERR_DAEMON_SI;
	}

		/* Leemos la zona de informaci�n */
	
	result = lowlevel_leer_buffer( info_disp->handle, buffer, info_disp->sector_ini, 1 );
	if ( result != ERR_LOWLEVEL_NO ) {
		free(nueva_pass);
		free(buffer);
		return ERR_DAEMON_SI;
	}

		/* Lo desciframos con la password antigua */

	tamBuffer=40;
	if ( !CryptDecrypt( info_disp->hKey, 0, TRUE, 0, buffer, &tamBuffer ) ) {
		free(nueva_pass);
		free(buffer);
		return ERR_DAEMON_SI;
	}
	
		/* Comprobamos que la password sea correcta */

	if (strncmp(buffer, iden, 32) != 0) {
		free(nueva_pass);
		free(buffer);
		return ERR_DAEMON_SI;
	}

		/* Lo ciframos con la nueva password */

	tamBuffer = 32;	/* Sin contar el padding */
	if ( !CryptEncrypt( hKeyTmp, 0, TRUE, 0, buffer, &tamBuffer, 40 ) ) {
		free(nueva_pass);
		free(buffer);
		return ERR_DAEMON_SI;
	}

		/* Escribimos el IDEN de nuevo */

	result = lowlevel_escribir_buffer( info_disp->handle, buffer, info_disp->sector_ini, 1 );
	if ( result != ERR_LOWLEVEL_NO ) {
		free(nueva_pass);
		free(buffer);
		return ERR_DAEMON_SI;
	}

	free(buffer);

	/* El cambio consiste en modificar todos los bloques que se hayan cifrado, leyendo hasta current block */
	/* Para ello, los leemos con la password antigua y los volvemos a cifrar usando la password nueva */


	iteraciones = ( ((info_disp->current_block) + 1) * TAM_BLOQUE ) / TAM_BUFFER;
	bytes_restantes = ( ((info_disp->current_block) + 1) * TAM_BLOQUE ) % TAM_BUFFER;

	/* Saltamos a la zona de bloques */
	sector_relativo = info_disp->sector_ini + (TAM_BLOQUE/512) + ((info_disp->tam_zona_reserv) * (TAM_BLOQUE/512));

	for (indice=0; indice<iteraciones; indice++) {

		buffer = (BYTE *) malloc (TAM_BUFFER * sizeof(BYTE));
		if ( buffer == NULL ) {
			free(nueva_pass);
			return ERR_DAEMON_SI;
		}

		/* Llenamos el buffer */
		result = lowlevel_leer_buffer( info_disp->handle, buffer, sector_relativo, TAM_BUFFER/512 );
		if ( result != ERR_LOWLEVEL_NO ) {
			free(nueva_pass);
			free(buffer);
			return ERR_DAEMON_SI;
		}
	
		ptr = buffer;
		
		/* Tratamos todos los bloques del buffer, uno a uno */

		for ( indice2 = 0; indice2 < (TAM_BUFFER/TAM_BLOQUE); indice2++ ) {

			/* S�lo tratamos los bloques cifrados */

			if ( BLOQUE_ES_CIFRADO(ptr) ) {

				if ( !(info_disp->modo_pwd) ) return ERR_DAEMON_SI;

				/* Desciframos el bloque con la password antigua */

				tamBuffer = TAM_BLOQUE-TAM_CABECERA;
				if ( !CryptDecrypt(info_disp->hKey, 0, TRUE, 0, ptr+TAM_CABECERA, &tamBuffer) ) {
					free(nueva_pass);
					free(buffer);
					return ERR_DAEMON_SI;
				}

				/* Machacamos el padding con random */
				if ( !CryptGenRandom( info_disp->hProv, TAM_PADDING, ptr+(TAM_BLOQUE-TAM_PADDING) ) ) {
					free(nueva_pass);
					free(buffer);
					return ERR_DAEMON_SI;
				}

				/* Ciframos de nuevo con la password nueva */

				tamBuffer = TAM_BLOQUE - TAM_PADDING - TAM_CABECERA;	/* Quitamos el padding y la cabecera */
				if ( !CryptEncrypt( hKeyTmp, 0, TRUE, 0, ptr+TAM_CABECERA, &tamBuffer, TAM_BLOQUE-TAM_CABECERA ) ) {
					free(nueva_pass);
					free(buffer);
					return ERR_DAEMON_SI;
				}

				/* Escribimos el bloque */
				result = lowlevel_escribir_buffer( info_disp->handle, ptr, sector_relativo + (indice2*TAM_BLOQUE)/512, TAM_BLOQUE/512 );
				if ( result != ERR_LOWLEVEL_NO ) {
					free(nueva_pass);
					free(buffer);
					return ERR_DAEMON_SI;
				}
			}

			ptr += TAM_BLOQUE;
		}

		sector_relativo += TAM_BUFFER/512;
		free(buffer);

	}

	/* Si quedan bytes por leer que no cab�an en el buffer */
	if ( bytes_restantes > 0 ) {

		buffer = (BYTE *) malloc (bytes_restantes * sizeof(BYTE));
		if ( buffer == NULL ) {
			free(nueva_pass);
			return ERR_DAEMON_SI;
		}

		/* Llenamos el buffer */
		result = lowlevel_leer_buffer( info_disp->handle, buffer, sector_relativo, bytes_restantes/512 );
		if ( result != ERR_LOWLEVEL_NO ) {
			free(nueva_pass);
			free(buffer);
			return ERR_DAEMON_SI;
		}

		ptr = buffer;

		for ( indice2 = 0; indice2 < (bytes_restantes/TAM_BLOQUE); indice2++ ) {

			/* S�lo tratamos los bloques cifrados */

			if ( BLOQUE_ES_CIFRADO(ptr) ) {

				if ( !(info_disp->modo_pwd) ) return ERR_DAEMON_SI;

				/* Desciframos el bloque con la password antigua */

				tamBuffer = TAM_BLOQUE-TAM_CABECERA;
				if ( !CryptDecrypt(info_disp->hKey, 0, TRUE, 0, ptr+TAM_CABECERA, &tamBuffer) ) {
					free(nueva_pass);
					free(buffer);
					return ERR_DAEMON_SI;
				}

				/* Machacamos el padding con random */
				if ( !CryptGenRandom( info_disp->hProv, TAM_PADDING, ptr+(TAM_BLOQUE-TAM_PADDING) ) ) {
					free(nueva_pass);
					free(buffer);
					return ERR_DAEMON_SI;
				}

				/* Ciframos de nuevo con la password nueva */

				tamBuffer = TAM_BLOQUE - TAM_PADDING - TAM_CABECERA;	/* Quitamos el padding y la cabecera */
				if ( !CryptEncrypt( hKeyTmp, 0, TRUE, 0, ptr+TAM_CABECERA, &tamBuffer, TAM_BLOQUE-TAM_CABECERA ) ) {
					free(nueva_pass);
					free(buffer);
					return ERR_DAEMON_SI;
				}

				/* Escribimos el bloque */
				result = lowlevel_escribir_buffer( info_disp->handle, ptr, sector_relativo + (indice2*TAM_BLOQUE)/512, TAM_BLOQUE/512 );
				if ( result != ERR_LOWLEVEL_NO ) {
					free(nueva_pass);
					free(buffer);
					return ERR_DAEMON_SI;
				}

			}

			ptr += TAM_BLOQUE;

		}

		free(buffer);

	}


	/* Actualizamos la info de la pass */

	info_disp->hProv = hProvTmp;
	info_disp->hKey = hKeyTmp;


	/* Enviamos el resultado */
	enviar_resultado(sockcli, 0);

	return ERR_DAEMON_NO;

}


/*----------------------------------------------*/
/* Funci�n 5									*/
/*----------------------------------------------*/
/* Devuelve bloque identificativo				*/
/*												*/
/* Salida:										*/
/*		- bloque identificativo (1024 bytes)	*/
/*----------------------------------------------*/

int f5(SOCKET sockcli, struct info_dispositivo *info_disp) 
{

	BYTE zona_info[TAM_BLOQUE];
	int result;

	/* Leemos la zona de informaci�n */
	result = lowlevel_leer_buffer( info_disp->handle, zona_info, info_disp->sector_ini, (TAM_BLOQUE/512));
	if ( result != ERR_LOWLEVEL_NO ) return ERR_DAEMON_SI;

	/* Enviamos el resultado antes del bloque identificativo */
	enviar_resultado(sockcli, 0);

	result = envia(sockcli, zona_info, TAM_BLOQUE);
	if (result == ERR_DAEMON_ENVIANDO) return ERR_DAEMON_SI;

	return ERR_DAEMON_NO;

}


/*----------------------------------------------*/
/* Funci�n 6									*/
/*----------------------------------------------*/
/* Lee bloque crypto (bajo nivel)				*/
/*												*/
/* Entrada:										*/
/*		- num de bloque (4 bytes)				*/
/* Salida:										*/
/*		- bloque (TAM_BLOQUE bytes)				*/
/*----------------------------------------------*/

int f6(SOCKET sockcli, struct info_dispositivo *info_disp) {

	int num_bloque;
	int result;
	BYTE buffer_bloque[TAM_BLOQUE];
	int desplazamiento;
	int tamBloque;

	/* Recibimos el n�mero de bloque */
	result = recibe(sockcli, (char *)&num_bloque, 4);
	if ((result == ERR_DAEMON_TIMED_OUT) || (result == ERR_DAEMON_RECIBIENDO)) return ERR_DAEMON_SI;

	if (( num_bloque < 0 ) || ( num_bloque > info_disp->total_bloques)) return ERR_DAEMON_SI;

	/* Saltamos la zona de info y la zona reservada */
	desplazamiento = (TAM_BLOQUE/512) + ((info_disp->tam_zona_reserv)*(TAM_BLOQUE/512));

	result = lowlevel_leer_buffer( info_disp->handle, buffer_bloque, info_disp->sector_ini+desplazamiento+(num_bloque*(TAM_BLOQUE/512)), (TAM_BLOQUE/512));
	if ( result != ERR_LOWLEVEL_NO ) {
		result = GetLastError();
		return ERR_DAEMON_SI;
	}

	/* Lo desciframos si corresponde */

	if ( BLOQUE_ES_CIFRADO(buffer_bloque) ) {

		if (!(info_disp->modo_pwd)) return ERR_DAEMON_SI;

		tamBloque = TAM_BLOQUE-TAM_CABECERA;
		if ( !CryptDecrypt(info_disp->hKey, 0, TRUE, 0, buffer_bloque+TAM_CABECERA, &tamBloque) ) {
			return ERR_DAEMON_SI;
		}

		/* Machacamos el padding con random */
		if (!CryptGenRandom( info_disp->hProv, TAM_PADDING, buffer_bloque+(TAM_BLOQUE-TAM_PADDING) ) ) {
			return ERR_DAEMON_SI;
		}

	}

	/* Enviamos el resultado */
	enviar_resultado(sockcli, 0);

	/* Enviamos el bloque */
	result = envia(sockcli, buffer_bloque, TAM_BLOQUE);
	if (result == ERR_DAEMON_ENVIANDO) return ERR_DAEMON_SI;

	return ERR_DAEMON_NO;


}

/*----------------------------------------------*/
/* Funci�n 7									*/
/*----------------------------------------------*/
/* Lee el primer bloque crypto de un tipo		*/
/* (findfirst)									*/
/*												*/
/* Entrada:										*/
/*		- tipo (1 byte)							*/
/* Salida:										*/
/*		- num del bloque (4 bytes)				*/
/*		- bloque (TAM_BLOQUE bytes)				*/
/*----------------------------------------------*/

/*--------------------------------------------------*/
/* NOTA:											*/
/*--------------------------------------------------*/
/* Esta funci�n devuelve -1 en el num del bloque	*/
/* como salida si no se encuentra nada de ese tipo  */
/* y no devuelve buffer								*/
/*--------------------------------------------------*/

int f7(SOCKET sockcli, struct info_dispositivo *info_disp) {

	unsigned char tipo_bloque;
	int encontrado = 0;
	int result;
	BYTE mini_buffer[512];
	BYTE buffer[TAM_BLOQUE];
	long indice=0;
	unsigned int salto;
	int num_bloque;
	int tamBloque;

	/* Recibimos el tipo de bloque */

	result = recibe(sockcli, &tipo_bloque, 1);

	if ((result == ERR_DAEMON_TIMED_OUT) || (result == ERR_DAEMON_RECIBIENDO)) 
		return ERR_DAEMON_SI;

	/* Buscamos a partir de la zona crypto, saltamos la zona de info y la reservada */
	salto = 1 + (info_disp->tam_zona_reserv);

	/* Vamos recorriendo hasta encontrar el primero de ese tipo */

	while ( !encontrado && (indice <= info_disp->current_block )){
		
		/* Leemos la m�nima cantidad posible de bloque (512 bytes) */
		/* Cuando lo encontremos, leeremos el bloque entero 1 vez */

		result = lowlevel_leer_buffer( info_disp->handle, mini_buffer, (info_disp->sector_ini)+(salto*(TAM_BLOQUE/512))+(indice*(TAM_BLOQUE/512)), 1);
		if (result != ERR_LOWLEVEL_NO ) 
			return ERR_DAEMON_SI;

		if ( ( ! BLOQUE_ES_VACIO(mini_buffer)) && (mini_buffer[1] == tipo_bloque) ) {

			/* Lo hemos encontrado, leemos el bloque entero */

			result = lowlevel_leer_buffer( info_disp->handle, buffer, (info_disp->sector_ini)+(salto*(TAM_BLOQUE/512))+(indice*(TAM_BLOQUE/512)), (TAM_BLOQUE/512));
			if (result != ERR_LOWLEVEL_NO)
				return ERR_DAEMON_SI;

			/* Si el bloque es cifrado y hemos abierto una sesi�n autenticada
			 * lo desciframos e indicamos que ha sido encontrado, en caso contrario
			 * no
			 */

			if ( BLOQUE_ES_CIFRADO(buffer) && info_disp->modo_pwd ) {

				/* Si podemos descifrarlo, lo desciframos */

				tamBloque = TAM_BLOQUE-TAM_CABECERA;	/* Vamos a descifrarlo todo menos la cabecera */

				if ( !CryptDecrypt(info_disp->hKey, 0, TRUE, 0, buffer+TAM_CABECERA, &tamBloque) ) {
					return ERR_DAEMON_SI;
				}

				encontrado = 1;

			} else if ( BLOQUE_ES_CLARO(buffer) )
				encontrado = 1;
			else
				indice++;
			
		}
		else {
			indice++;
		}

	}

	LOG_Escribir(logHandle, "F7:5\n");

	if ( encontrado == 0 ) 
		num_bloque = -1;		/* No se ha encontrado ning�n bloque */
	else 
		num_bloque = indice;

	/* Enviamos el resultado */
	enviar_resultado(sockcli, 0);

	LOG_Escribir(logHandle, "F7:6\n");

	/* Enviamos el n�mero de bloque */
	result = envia(sockcli, (char *)&num_bloque, 4);

	LOG_Escribir(logHandle, "F7:8\n");

	if (result == ERR_DAEMON_ENVIANDO) return ERR_DAEMON_SI;

	LOG_Escribir(logHandle, "F7:9\n");

	if ( num_bloque != -1 ) {	/* Enviamos el buffer */
		result = envia(sockcli, buffer, TAM_BLOQUE);
		if (result == ERR_DAEMON_ENVIANDO) return ERR_DAEMON_SI;
	}

	LOG_Escribir(logHandle, "F7:10\n");

	return ERR_DAEMON_NO;

}

/*----------------------------------------------*/
/* Funci�n 8									*/
/*----------------------------------------------*/
/* Lee el sig bloque crypto del mismo tipo		*/
/* (findnext)									*/
/*												*/
/* Entrada:										*/
/*		- tipo (1 byte)							*/
/*		- num del bloque (4 bytes)				*/
/* Salida:										*/
/*		- num del bloque (4 bytes)				*/
/*		- bloque (TAM_BLOQUE bytes)				*/
/*----------------------------------------------*/

/*--------------------------------------------------*/
/* NOTA:											*/
/*--------------------------------------------------*/
/* Esta funci�n devuelve -1 en el num del bloque	*/
/* como salida si no se encuentra nada de ese tipo  */
/* y no devuelve buffer								*/
/*--------------------------------------------------*/

int f8(SOCKET sockcli, struct info_dispositivo *info_disp) {

	char tipo_bloque;
	unsigned int handle;
	int encontrado = 0;
	int result;
	BYTE mini_buffer[512];
	BYTE buffer[TAM_BLOQUE];
	long indice=0;
	long salto;
	int num_bloque;
	int tamBloque;

	/* Recibimos el tipo de bloque */
	result = recibe(sockcli, &tipo_bloque, 1);
	if ((result == ERR_DAEMON_TIMED_OUT) || (result == ERR_DAEMON_RECIBIENDO)) return ERR_DAEMON_SI;

	/* Recibimos el handle */
	result = recibe(sockcli, (char *)&handle, 4);
	if ((result == ERR_DAEMON_TIMED_OUT) || (result == ERR_DAEMON_RECIBIENDO)) return ERR_DAEMON_SI;

	/* Buscamos a partir de la zona crypto, saltamos la zona de info, la reservada y los bloques ya le�dos */
	salto = 1 + (info_disp->tam_zona_reserv);
	indice = handle + 1;

	/* Vamos recorriendo hasta encontrar el primero de ese tipo */

	while ((!encontrado) && (indice <= info_disp->current_block )){
		
		/* Leemos la m�nima cantidad posible de bloque (512 bytes) */
		/* Cuando lo encontremos, leeremos el bloque entero 1 vez */

		result = lowlevel_leer_buffer( info_disp->handle, mini_buffer, (info_disp->sector_ini)+((salto+indice)*(TAM_BLOQUE/512)), 1);
		if (result != ERR_LOWLEVEL_NO ) return ERR_DAEMON_SI;

		if ( (!BLOQUE_ES_VACIO(mini_buffer)) && (mini_buffer[1] == tipo_bloque) ) {

			/* Lo hemos encontrado, leemos el bloque entero */

			result = lowlevel_leer_buffer( info_disp->handle, buffer, (info_disp->sector_ini)+((salto+indice)*(TAM_BLOQUE/512)), (TAM_BLOQUE/512));
			if (result != ERR_LOWLEVEL_NO) return ERR_DAEMON_SI;

			/* Si es necesario, desciframos */
			if ( BLOQUE_ES_CIFRADO(buffer) && info_disp->modo_pwd ) {
				/* Si podemos descifrarlo, lo desciframos */

				tamBloque = TAM_BLOQUE-TAM_CABECERA;	/* Vamos a descifrarlo todo menos la cabecera */

				if ( !CryptDecrypt(info_disp->hKey, 0, TRUE, 0, buffer+TAM_CABECERA, &tamBloque) ) {
					return ERR_DAEMON_SI;
				}
				encontrado = 1;
			} else if ( BLOQUE_ES_CLARO(buffer) )
				encontrado = 1;
			else 
				indice++;
			
		}
		else {
			indice++;
		}

	}

	if ( encontrado == 0 ) num_bloque = -1;		/* No se ha encontrado ning�n bloque */
	else num_bloque = indice;


	/* Enviamos el resultado */
	enviar_resultado(sockcli, 0);

	/* Enviamos el n�mero de bloque */
	result = envia(sockcli, (char *)&num_bloque, 4);
	if (result == ERR_DAEMON_ENVIANDO) return ERR_DAEMON_SI;

	if ( num_bloque != -1 ) {	/* Enviamos el buffer */
		result = envia(sockcli, buffer, TAM_BLOQUE);
		if (result == ERR_DAEMON_ENVIANDO) return ERR_DAEMON_SI;
	}

	return ERR_DAEMON_NO;

}


/*----------------------------------------------*/
/* Funci�n 9									*/
/*----------------------------------------------*/
/* Lee todos los bloques crypto de un tipo		*/
/*												*/
/* Entrada:										*/
/*		- tipo (1 byte)							*/
/* Salida:										*/
/*		- num de bloques (4 bytes)				*/
/*		- no del 1er bloque (4 bytes)			*/
/*		- no del 2o bloque (4 bytes)			*/
/*		- no del 3er bloque (4 bytes)			*/
/*		- ...									*/
/*		- buffer (n*TAM_BLOQUE bytes)			*/
/*----------------------------------------------*/

int f9(SOCKET sockcli, struct info_dispositivo *info_disp) {

	typedef struct BLOQUE {

			BYTE info[TAM_BLOQUE];
			unsigned long num_bloque;

	} BLOQUE;

	BLOQUE * bloques;
	char tipo_solicitado;
	int result;
	unsigned long iteraciones;
	unsigned long sector_relativo;
	unsigned long contador;
	BYTE* buffer;
	BYTE* ptr;
	BYTE buffer_bloque[TAM_BLOQUE];
	int tamBloque;
	unsigned long indice;
	unsigned long bytes_restantes;
	unsigned long max_bloques_buffer = 100;
	unsigned long contador_bloques_buenos;
	unsigned long contador_bloque_absoluto;

	/* Previsi�n de 100 bloques */
	bloques = (BLOQUE *) malloc ( max_bloques_buffer * sizeof(BLOQUE));
	if ( bloques == NULL ) return ERR_DAEMON_SI;

	/* Recibimos el tipo de bloque */
	result = recibe (sockcli, &tipo_solicitado, 1);
	if (( result == ERR_DAEMON_TIMED_OUT ) || ( result == ERR_DAEMON_RECIBIENDO )) {
		free(bloques);
		return ERR_DAEMON_SI;
	}

	/* Recorremos hasta el current block */

	if ( info_disp->current_block == -1 ) {
		iteraciones = ( (info_disp->total_bloques) * TAM_BLOQUE ) / TAM_BUFFER;
		bytes_restantes = ( (info_disp->total_bloques) * TAM_BLOQUE ) % TAM_BUFFER;
	}
	else {
		iteraciones = ( ((info_disp->current_block) + 1) * TAM_BLOQUE ) / TAM_BUFFER;
		bytes_restantes = ( ((info_disp->current_block) + 1) * TAM_BLOQUE ) % TAM_BUFFER;
	}

	sector_relativo = info_disp->sector_ini + (TAM_BLOQUE/512) + ((info_disp->tam_zona_reserv) * (TAM_BLOQUE/512));
	contador_bloques_buenos = 0;
	contador_bloque_absoluto = 0;

	for ( contador = 0; contador < iteraciones ; contador++ ) {

		buffer = (BYTE *) malloc (TAM_BUFFER * sizeof(char));
		if ( buffer == NULL ) {
			free(bloques);
			return ERR_DAEMON_SI;
		}

		/* Llenamos el buffer */
		result = lowlevel_leer_buffer( info_disp->handle, buffer, sector_relativo, TAM_BUFFER/512 );
		if ( result != ERR_LOWLEVEL_NO ) {
			free(bloques);
			free(buffer);
			return ERR_DAEMON_SI;
		}
	
		ptr = buffer;
		
		/* Tratamos todos los bloques del buffer, uno a uno */

		for ( indice = 0; indice < (TAM_BUFFER/TAM_BLOQUE); indice++ ) {

			/* Bloques no vac�os */
			if ( ! BLOQUE_ES_VACIO(ptr) ) {

				/* Si esta vac�o y podemos, lo desciframos. Si no podemos, pasamos del bloque */
				if ( BLOQUE_ES_CIFRADO(ptr)) {

					if ( !(info_disp->modo_pwd) ) {		/* Pasamos al siguiente bloque */
						contador_bloque_absoluto += 1;
						ptr += TAM_BLOQUE;
						continue;
					}
					else {

						memcpy(buffer_bloque, ptr, TAM_BLOQUE);
						tamBloque = TAM_BLOQUE-TAM_CABECERA;	/* Vamos a descifrarlo todo menos la cabecera */

						if ( !CryptDecrypt(info_disp->hKey, 0, TRUE, 0, buffer_bloque+TAM_CABECERA, &tamBloque) ) {
							return ERR_DAEMON_SI;
						}

						/* Machacamos el padding con random */
						if (!CryptGenRandom( info_disp->hProv, TAM_PADDING, buffer_bloque+(TAM_BLOQUE-TAM_PADDING) ) ) {
							return ERR_DAEMON_SI;
						}

					}
				}
				else if ( BLOQUE_ES_CLARO(ptr) ) {

					/* Lo copiamos en buffer_bloque */

					memcpy( buffer_bloque, ptr, TAM_BLOQUE );

				}

				/* Tratamos el bloque una vez lo tenemos en claro, cogemos la informaci�n si los tipos coinciden */
				
				if ( buffer_bloque[1] == tipo_solicitado ) {

					if ( contador_bloques_buenos >= max_bloques_buffer ) {	/* Lo hemos llenado, reservamos m�s espacio */
						bloques = realloc( bloques, _msize(bloques) + (max_bloques_buffer*sizeof(BLOQUE)) );

						if ( bloques == NULL ) {
							free(bloques);
							free(buffer);
							return ERR_DAEMON_SI;
						}
					}

					/* N�mero de bloque */
					bloques[contador_bloques_buenos].num_bloque = contador_bloque_absoluto;

					/* Informaci�n */
					memcpy(bloques[contador_bloques_buenos].info, buffer_bloque, TAM_BLOQUE);

					contador_bloques_buenos+=1;
				}

			}

			contador_bloque_absoluto += 1;
			ptr += TAM_BLOQUE;

		}

		sector_relativo += TAM_BUFFER/512;
		free(buffer);

	}


	/* Tratamos los bytes restantes que no llenan el buffer */

	if ( bytes_restantes > 0 ) {

		/* Tener en cuenta contador_bloques_buenos y contador_bloque_absoluto */

		buffer = (BYTE *) malloc (bytes_restantes * sizeof(char));
		if ( buffer == NULL ) {
			free(bloques);
			return ERR_DAEMON_SI;
		}

		/* Llenamos el buffer con los bytes restantes */

		result = lowlevel_leer_buffer( info_disp->handle, buffer, sector_relativo, bytes_restantes/512 );
		if ( result != ERR_LOWLEVEL_NO ) {
			free(bloques);
			free(buffer);
			return ERR_DAEMON_SI;
		}
	
		ptr = buffer;
		
		/* Tratamos todos los bloques del buffer, uno a uno */

		for ( indice = 0; indice < (bytes_restantes/TAM_BLOQUE); indice++ ) {

			/* Bloques no vac�os */
			if ( ! BLOQUE_ES_VACIO(ptr) ) {

				/* Si esta vac�o y podemos, lo desciframos. Si no podemos, pasamos del bloque */
				if ( BLOQUE_ES_CIFRADO(ptr)) {

					if ( !(info_disp->modo_pwd) ) {		/* Pasamos al siguiente bloque */
						contador_bloque_absoluto += 1;
						ptr += TAM_BLOQUE;
						continue;
					}
					else {

						memcpy(buffer_bloque, ptr, TAM_BLOQUE);
						tamBloque = TAM_BLOQUE-8;	/* Vamos a descifrarlo todo menos la cabecera */

						if ( !CryptDecrypt(info_disp->hKey, 0, TRUE, 0, buffer_bloque+8, &tamBloque) ) {
							return ERR_DAEMON_SI;
						}

						/* Machacamos el padding con random */
						if (!CryptGenRandom( info_disp->hProv, TAM_PADDING, buffer_bloque+10232 ) ) {
							return ERR_DAEMON_SI;
						}

					}
				}
				else if ( BLOQUE_ES_CLARO(ptr) ) {

					/* Lo copiamos en buffer_bloque */

					memcpy( buffer_bloque, ptr, TAM_BLOQUE );

				}

				/* Tratamos el bloque una vez lo tenemos en claro, cogemos la informaci�n si los tipos coinciden */
				
				if ( buffer_bloque[1] == tipo_solicitado ) {

					if ( contador_bloques_buenos >= max_bloques_buffer ) {	/* Lo hemos llenado, reservamos m�s espacio */
						bloques = realloc( bloques, _msize(bloques) + (max_bloques_buffer*sizeof(BLOQUE)) );

						if ( bloques == NULL ) {
							free(bloques);
							free(buffer);
							return ERR_DAEMON_SI;
						}
					}

					/* N�mero de bloque */
					bloques[contador_bloques_buenos].num_bloque = contador_bloque_absoluto;

					/* Informaci�n */
					memcpy(bloques[contador_bloques_buenos].info, buffer_bloque, TAM_BLOQUE);

					contador_bloques_buenos+=1;
				}

			}

			contador_bloque_absoluto += 1;
			ptr += TAM_BLOQUE;

		}

		free(buffer);
	}


	/* Enviamos la informaci�n */

	/* Primero, el resultado */
	enviar_resultado(sockcli, 0);

	/* Enviamos el n�mero de bloques encontrados */

	result = envia( sockcli, (char *)&contador_bloques_buenos, 4 );
	if ( result == ERR_DAEMON_ENVIANDO) {
		free(bloques);
		return ERR_DAEMON_SI;

	}

	/* Enviamos los n�meros de bloque */

	for ( contador = 0; contador < contador_bloques_buenos ; contador++ ) {

		result = envia(sockcli, (char *)&bloques[contador].num_bloque, 4);
		if ( result == ERR_DAEMON_ENVIANDO ) {
			free(bloques);
			return ERR_DAEMON_SI;
		}
	}

	/* Enviamos los datos */

	for ( contador = 0; contador < contador_bloques_buenos; contador++) {

		result = envia(sockcli, bloques[contador].info, TAM_BLOQUE);
		if ( result == ERR_DAEMON_ENVIANDO ) {
			free(bloques);
			return ERR_DAEMON_SI;
		}
	}

	/* Liberamos la estructura de almacenamiento de los bloques */
	free(bloques);

	return ERR_DAEMON_NO;

}

/*----------------------------------------------*/
/* Funci�n 10									*/
/*----------------------------------------------*/
/* Lee todos los bloques no nulos				*/
/*												*/
/* Salida:										*/
/*		- num de bloques (4 bytes)				*/
/*		- no del 1er bloque (4 bytes)			*/
/*		- no del 2o bloque (4 bytes)			*/
/*		- no del 3er bloque (4 bytes)			*/
/*		- ...									*/
/*		- buffer (n*TAM_BLOQUE bytes)			*/
/*----------------------------------------------*/

int f10(SOCKET sockcli, struct info_dispositivo *info_disp) {

	long indice;
	int result;
	long num_bloques_encontrados=0;
	int* numeros_bloque;
	BYTE* bloques;
	BYTE* ptr;
	long contador=0;
	BYTE minibuffer[512];
	int tamBuffer;
	long num_bloque;

	/* Reservamos el m�ximo posible */
	numeros_bloque = (int *) malloc (((info_disp->current_block)+1)*sizeof(int));
	if ( numeros_bloque == NULL ) return ERR_DAEMON_SI;

	/* Buscamos a partir de la zona crypto, saltamos la zona de info y la reservada */
	indice = 1 + (info_disp->tam_zona_reserv);

	for (contador=0; contador < (info_disp->current_block) + 1; contador++) {

		/* Leemos 512 bytes */
		result = lowlevel_leer_buffer( info_disp->handle, minibuffer, (info_disp->sector_ini) + (indice*(TAM_BLOQUE/512)), 1);
		if (result != ERR_LOWLEVEL_NO ) {
			free(numeros_bloque);
			return ERR_DAEMON_SI;
		}

		if ( !BLOQUE_ES_VACIO(minibuffer) ) {	/* En este caso nos guardamos el n�mero de bloque */

			numeros_bloque[num_bloques_encontrados] = contador;
			num_bloques_encontrados ++;

		}

		indice++;
	}


	/* Una vez conocidos los n�meros de bloque, volvemos a recorrer cogiendo la info */
	bloques = (BYTE *) malloc (TAM_BLOQUE*num_bloques_encontrados*sizeof(BYTE));
	if ( bloques == NULL ) {
		free(numeros_bloque);
		return ERR_DAEMON_SI;
	}
	
	ptr = bloques;

	for (contador=0; contador < num_bloques_encontrados; contador++) {

		num_bloque = numeros_bloque[contador];
		indice = 1 + (info_disp->tam_zona_reserv) + num_bloque;

		/* Leemos ese bloque y lo a�adimos al buffer global */
		result = lowlevel_leer_buffer( info_disp->handle, ptr, (info_disp->sector_ini) + (indice*(TAM_BLOQUE/512)), (TAM_BLOQUE/512));
		if (result != ERR_LOWLEVEL_NO) {
			free(numeros_bloque);
			free(bloques);
			return ERR_DAEMON_SI;
		}

		if ( BLOQUE_ES_CIFRADO(ptr) ) {

			if ( info_disp->modo_pwd ) {
				/* Desciframos el bloque */
				tamBuffer = TAM_BLOQUE-TAM_CABECERA;
				if ( !CryptDecrypt(info_disp->hKey, 0, TRUE, 0, ptr+TAM_CABECERA, &tamBuffer) ) {
					free(numeros_bloque);
					free(bloques);
					return ERR_DAEMON_SI;
				}
			}
			else {
				free(numeros_bloque);
				free(bloques);
				return ERR_DAEMON_SI;
			}
		}

		ptr+=TAM_BLOQUE;

	}


	/* Enviamos la informaci�n */	

	/* El resultado */
	enviar_resultado(sockcli, 0);

	/* Enviamos el n�mero de bloques encontrados */
	result = envia(sockcli, (char *)&num_bloques_encontrados, 4);
	if (result == ERR_DAEMON_ENVIANDO) {
		free(numeros_bloque);
		free(bloques);
		return ERR_DAEMON_SI;
	}

	/* Enviamos los n�meros de bloque */
	for (contador=0; contador < num_bloques_encontrados; contador++) {

		result = envia(sockcli, (char *)&numeros_bloque[contador], 4);
		if (result == ERR_DAEMON_ENVIANDO) {
			free(numeros_bloque);
			free(bloques);
			return ERR_DAEMON_SI;
		}

	}

	/* Enviamos el buffer global */

	result = envia(sockcli, bloques, TAM_BLOQUE*num_bloques_encontrados);
	if (result == ERR_DAEMON_ENVIANDO) {
		free(numeros_bloque);
		free(bloques);
		return ERR_DAEMON_SI;
	}

	free(numeros_bloque);
	free(bloques);

	return ERR_DAEMON_NO;

}

/*----------------------------------------------*/
/* Funci�n 11									*/
/*----------------------------------------------*/
/* Escribe bloque crypto (bajo nivel)			*/
/*												*/
/* Entrada:										*/
/*		- num de bloque (4 bytes)				*/
/*		- bloque (TAM_BLOQUE bytes)				*/
/*----------------------------------------------*/

int f11(SOCKET sockcli, struct info_dispositivo *info_disp) {

	int num_bloque;
	int result;
	unsigned char buffer_bloque[TAM_BLOQUE];
	int desplazamiento;
	int tamBuffer;

	/* Recibimos el n�mero de bloque */
	result = recibe(sockcli, (char *)&num_bloque, 4);
	if ((result == ERR_DAEMON_TIMED_OUT) || (result == ERR_DAEMON_RECIBIENDO)) return ERR_DAEMON_SI;
	if (( num_bloque < 0 ) || ( num_bloque > info_disp->total_bloques)) return ERR_DAEMON_SI;

	/* Recibimos el bloque */
	result = recibe(sockcli, buffer_bloque, TAM_BLOQUE);
	if ((result == ERR_DAEMON_TIMED_OUT) || (result == ERR_DAEMON_RECIBIENDO)) return ERR_DAEMON_SI;


	/* No se puede llamar a esta funci�n sin autenticaci�n previa */
	if ( !(info_disp->modo_pwd) ) {
		return ERR_DAEMON_SI;
	}

	/* Obtenemos info aleatoria en los bytes no usados de la cabecera*/
	if (!CryptGenRandom( info_disp->hProv, TAM_CABECERA-2, buffer_bloque+2 ))
		return ERR_DAEMON_SI;


	if ( BLOQUE_ES_CIFRADO(buffer_bloque) ) {

		if ( !(info_disp->modo_pwd) ) return ERR_DAEMON_SI;

		/* Si podemos cifrarlo, lo ciframos */

		tamBuffer = TAM_BLOQUE - TAM_PADDING - TAM_CABECERA;	/* Quitamos el padding y la cabecera */

		if ( !CryptEncrypt( info_disp->hKey, 0, TRUE, 0, buffer_bloque+TAM_CABECERA, &tamBuffer, TAM_BLOQUE-TAM_CABECERA ) )
			return ERR_DAEMON_SI;

	}

	/* Lo escribimos */

	/* Saltamos la zona de info y la zona reservada */
	desplazamiento = (TAM_BLOQUE/512) + ((info_disp->tam_zona_reserv)*(TAM_BLOQUE/512));

	result = lowlevel_escribir_buffer( info_disp->handle, buffer_bloque, info_disp->sector_ini+desplazamiento+(num_bloque*(TAM_BLOQUE/512)), (TAM_BLOQUE/512));
	if ( result != ERR_LOWLEVEL_NO ) return ERR_DAEMON_SI;



	/* Actualizamos el CURRENT_BLOCK, en el caso de que hayamos escrito m�s all� del actual */


	if ( ( num_bloque >= info_disp->current_block ) && (info_disp->current_block != -1) ){

		/* Leemos el bloque de informaci�n */

		result = lowlevel_leer_buffer( info_disp->handle, buffer_bloque, info_disp->sector_ini, TAM_BLOQUE/512 );
		if ( result != ERR_LOWLEVEL_NO ) return ERR_DAEMON_SI;

		if ( info_disp->current_block == (info_disp->total_bloques - 1) ) {	/* Si apunta al �ltimo */
			*((int *)(buffer_bloque+64)) = -1;
			info_disp->current_block = -1;
		}
		else {
			*((int *)(buffer_bloque+64))= num_bloque + 1;
			info_disp->current_block = num_bloque + 1;
		}

		/* Ahora lo actualizamos */

		result = lowlevel_escribir_buffer( info_disp->handle, buffer_bloque, info_disp->sector_ini, TAM_BLOQUE/512 );
		if ( result != ERR_LOWLEVEL_NO ) return ERR_DAEMON_SI;

	}

	enviar_resultado(sockcli, 0);

	return ERR_DAEMON_NO;
}

/*----------------------------------------------*/
/* Funci�n 12									*/
/*----------------------------------------------*/
/* A�ade bloque crypto							*/
/*												*/
/* Entrada:										*/
/*		- bloque (TAM_BLOQUE bytes)				*/
/* Salida:										*/
/*		- num donde se ha escrito (4 bytes)		*/
/*----------------------------------------------*/

int f12(SOCKET sockcli, struct info_dispositivo *info_disp) {

	BYTE buffer[TAM_BLOQUE];
	BYTE minibuffer[512];
	int result;
	int tamBuffer;
	int desplazamiento;
	int num_bloque_libre=0;
	int num_bloque_escrito;
	int encontrado=0;
	char *ptr;
	int escribir_nuevo_current=1;

	/* Recibimos el bloque */
	result = recibe(sockcli, buffer, TAM_BLOQUE);
	if ((result == ERR_DAEMON_TIMED_OUT) || (result == ERR_DAEMON_RECIBIENDO)) return ERR_DAEMON_SI;

	/* No se puede llamar a esta funci�n sin autenticaci�n previa */
	if ( !(info_disp->modo_pwd) ) {
		return ERR_DAEMON_SI;
	}

	/* Obtenemos info aleatoria en los bytes no usados de la cabecera*/
	if (!CryptGenRandom( info_disp->hProv, TAM_CABECERA-2, buffer+2 ))
		return ERR_DAEMON_SI;

	/* Saltamos la zona de info y la zona reservada */

	desplazamiento = (TAM_BLOQUE/512) + ((info_disp->tam_zona_reserv)*(TAM_BLOQUE/512));

	/* Si el current block apunta al �ltimo bloque y el bloque est� vac�o, lo dejamos ah� */
	/* Esta es una situaci�n especial, que favorece el uso de la funci�n de b�squeda 9 */
	/* De esta forma, la funci�n 9 siempre buscar� hasta el current block */
	/* (todo el dispositivo en este caso) */

	if ( info_disp->current_block == (info_disp->total_bloques - 1) ) {		/* Current block empieza en cero */

		/* Leemos el bloque y vemos si est� vac�o */

		result = lowlevel_leer_buffer( info_disp->handle, minibuffer, info_disp->sector_ini + desplazamiento + (info_disp->total_bloques - 1)*(TAM_BLOQUE/512), 1 );
		if ( result != ERR_LOWLEVEL_NO )
			return ERR_DAEMON_SI;

		/* Si est� vac�o, continuamos normal */
		/* Si no lo est�, buscamos un hueco desde el principio */

		if ( ! BLOQUE_ES_VACIO(minibuffer)) {		/* Buscamos desde el principio */

			escribir_nuevo_current = 0;			/* Vamos a dejar el current block apuntando al final */
			num_bloque_libre = 0;

			while ((!encontrado) && (num_bloque_libre < info_disp->total_bloques)) {

				/* Leemos el bloque */

				result = lowlevel_leer_buffer( info_disp->handle, minibuffer, info_disp->sector_ini + desplazamiento + (num_bloque_libre*(TAM_BLOQUE/512)) , 1);
				if ( result != ERR_LOWLEVEL_NO )
					return ERR_DAEMON_SI;

				if (BLOQUE_ES_VACIO(minibuffer))
					encontrado = 1;
				else
					num_bloque_libre ++;

			}

			if ( ! encontrado)
				num_bloque_libre = -1;		/* No quedan huecos, el dispositivo est� lleno */

		}

	}

	/* Comprobaci�n de dispositivo lleno */

	if ((info_disp->current_block < 0) || (num_bloque_libre < 0))		/* El dispositivo esta lleno! */
		return ERR_DAEMON_SI;


	/* Lo ciframos si corresponde */

	if ( BLOQUE_ES_CIFRADO(buffer) ) {

		if ( !(info_disp->modo_pwd) ) return ERR_DAEMON_SI;

		/* Si podemos cifrarlo, lo ciframos */

		tamBuffer = TAM_BLOQUE - TAM_PADDING - TAM_CABECERA;	/* Quitamos el padding y la cabecera */

		if ( !CryptEncrypt( info_disp->hKey, 0, TRUE, 0, buffer+TAM_CABECERA, &tamBuffer, TAM_BLOQUE-TAM_CABECERA ) )
			return ERR_DAEMON_SI;

	}

	/* Lo escribimos en current block o en el hueco buscado */

	if ( escribir_nuevo_current == 0 ) {

		result = lowlevel_escribir_buffer( info_disp->handle, buffer, info_disp->sector_ini + desplazamiento+(num_bloque_libre*(TAM_BLOQUE/512)), (TAM_BLOQUE/512));
		if ( result != ERR_LOWLEVEL_NO ) 
			return ERR_DAEMON_SI;

		num_bloque_escrito = num_bloque_libre;
	}
	else {

		result = lowlevel_escribir_buffer( info_disp->handle, buffer, info_disp->sector_ini + desplazamiento+(info_disp->current_block*(TAM_BLOQUE/512)), (TAM_BLOQUE/512));
		if ( result != ERR_LOWLEVEL_NO )
			return ERR_DAEMON_SI;

		num_bloque_escrito = info_disp->current_block;
	}

	/* Incremento del current block, supone buscar el siguiente bloque libre */

	num_bloque_libre = info_disp->current_block;

	/* Bucle hasta encontrar el vac�o o llegar al final */

	while ((!encontrado) && (num_bloque_libre < (info_disp->total_bloques - 1))) {

		num_bloque_libre++;

		/* Leemos el bloque */
		result = lowlevel_leer_buffer( info_disp->handle, minibuffer, info_disp->sector_ini + desplazamiento + (num_bloque_libre*(TAM_BLOQUE/512)) , 1);
		if ( result != ERR_LOWLEVEL_NO ) return ERR_DAEMON_SI;

		if (BLOQUE_ES_VACIO(minibuffer)) encontrado = 1;
		
	}


	/* Escribimos el current block nuevo, si corresponde */

	if ( escribir_nuevo_current == 1 ) {

		info_disp->current_block = num_bloque_libre;

		/* Leemos el antiguo */

		result = lowlevel_leer_buffer( info_disp->handle, buffer, info_disp->sector_ini, (TAM_BLOQUE/512) );
		if ( result != ERR_LOWLEVEL_NO ) return ERR_DAEMON_SI;

		/* Modificamos */
		ptr = buffer;
		ptr += 64;

		*((int *)ptr) = info_disp->current_block;

		/* Escribimos el nuevo */
		result = lowlevel_escribir_buffer( info_disp->handle, buffer, info_disp->sector_ini, (TAM_BLOQUE/512) );
		if ( result != ERR_LOWLEVEL_NO ) return ERR_DAEMON_SI;

	}

	/* Enviamos el resultado */
	enviar_resultado(sockcli, 0);

	/* Enviamos donde lo hemos escrito */
	result = envia(sockcli, (char *)&num_bloque_escrito, 4);
	if (result == ERR_DAEMON_ENVIANDO) return ERR_DAEMON_SI;

	if (info_disp->current_block == -1) return ERR_DAEMON_SI;

	return ERR_DAEMON_NO;
}

/*----------------------------------------------*/
/* Funci�n 13									*/
/*----------------------------------------------*/
/* Borra un bloque crypto						*/
/*												*/
/* Entrada:										*/
/*		- num de bloque (4 bytes)				*/
/*----------------------------------------------*/

int f13(SOCKET sockcli, struct info_dispositivo *info_disp) {

	int result;
	int num_bloque;
	BYTE bloque_vacio[TAM_BLOQUE];
	BYTE *ptr;
	int desplazamiento;
	int i;
	int son_vacios=1;

	/* Recibimos el n�mero de bloque */
	result = recibe(sockcli, (char *)&num_bloque, 4);
	if ((result == ERR_DAEMON_TIMED_OUT) || (result == ERR_DAEMON_RECIBIENDO)) return ERR_DAEMON_SI;

	/* No se puede llamar a esta funci�n sin autenticaci�n previa */
	if ( !(info_disp->modo_pwd) ) {
		return ERR_DAEMON_SI;
	}

	/* Comprobamos que se pretende borrar un bloque correcto */
	if (( num_bloque < 0 ) || ( num_bloque > info_disp->total_bloques)) return ERR_DAEMON_SI;

	/* Saltamos la zona de info y la zona reservada */
	desplazamiento = (TAM_BLOQUE/512) + ((info_disp->tam_zona_reserv)*(TAM_BLOQUE/512));

	/* Obtenemos info aleatoria */
	if (!CryptGenRandom( info_disp->hProv, TAM_BLOQUE, bloque_vacio ))
		return ERR_DAEMON_SI;

	/* Marcamos como bloque vac�o */
	srand( (unsigned) time(NULL) );
	bloque_vacio[0] = rand() % 85;	/* Bloques blancos por defecto */

	/* Escribimos */
	result = lowlevel_escribir_buffer( info_disp->handle, bloque_vacio, info_disp->sector_ini + desplazamiento + (num_bloque*(TAM_BLOQUE/512)), (TAM_BLOQUE/512));
	if ( result != ERR_LOWLEVEL_NO ) return ERR_DAEMON_SI;


	/* Actualizamos el CURRENT_BLOCK */
	/* En el caso de que borremos el �ltimo bloque insertado, vamos a ir retrocediendo para eliminar huecos */

	if ( (info_disp->current_block == num_bloque+1) || (info_disp->current_block == num_bloque) ) {

		if ( info_disp->current_block == num_bloque+1 )
			i = num_bloque - 1;
		else
			i = num_bloque;

		while ( (i >= 0) && (son_vacios) ){

			/* Leemos el bloque anterior */
			result = lowlevel_leer_buffer( info_disp->handle, bloque_vacio, info_disp->sector_ini + desplazamiento + (i*(TAM_BLOQUE/512)), TAM_BLOQUE/512 );
			if ( result != ERR_LOWLEVEL_NO ) return ERR_DAEMON_SI;

			/* Vemos si est� vac�o */
			if ( !BLOQUE_ES_VACIO(bloque_vacio) ) son_vacios = 0;
			else i--;

		}

		/* Escribimos el nuevo current block */

		info_disp->current_block = i+1;

		/* Leemos el antiguo */
		result = lowlevel_leer_buffer( info_disp->handle, bloque_vacio, info_disp->sector_ini, (TAM_BLOQUE/512) );
		if ( result != ERR_LOWLEVEL_NO ) return ERR_DAEMON_SI;

		/* Modificamos */
		ptr = bloque_vacio;
		ptr += 64;

		*((int *)ptr) = info_disp->current_block;

		/* Escribimos el nuevo */
		result = lowlevel_escribir_buffer( info_disp->handle, bloque_vacio, info_disp->sector_ini, (TAM_BLOQUE/512) );
		if ( result != ERR_LOWLEVEL_NO ) return ERR_DAEMON_SI;

	}

	/* Enviamos el resultado */
	enviar_resultado(sockcli, 0);

	return ERR_DAEMON_NO;

}




int f14(SOCKET sockcli, struct info_dispositivo *info_disp)
{
	BYTE hwID[HW_ID_LEN];
	int result;

	ZeroMemory(hwID, HW_ID_LEN);
	enviar_resultado(sockcli, 0);

	result = envia(sockcli, (char *) hwID, HW_ID_LEN);
	if (result == ERR_DAEMON_ENVIANDO)
		return ERR_DAEMON_SI;

	result = envia(sockcli, (char *) hwID, HW_ID_LEN);
	if (result == ERR_DAEMON_ENVIANDO)
		return ERR_DAEMON_SI;

	return ERR_DAEMON_NO;
}


#if 0
int finaliza_socket( SOCKET sockcli ) {

	int result;

	/* 1.- Call WSAAsyncSelect to register for FD_CLOSE notification */
	result = WSAAsyncSelect( socket, hWnd, wMsg, FD_CLOSE );

	/* 2.- Call shutdown with how=SD_SEND */
	result = shutdown(sockcli, SD_SEND);

	/* 3.- When FD_CLOSE received, call recv until zero returned, or SOCKET_ERROR */

	/* 4.- Call closesocket */
	result = closesocket(sockcli);
	if ( result == SOCKET_ERROR ) return ERR_DAEMON_CERRANDO_SOCKET;


	return ERR_DAEMON_NO;

}
#endif

int inicializa_socket() {

	char cadaux[512];
	char yes[1]="\1";
	struct sockaddr_in mio;


	if ((sockfd = socket(AF_INET, SOCK_STREAM, 0)) == INVALID_SOCKET) {
		sprintf(cadaux,"Error creando socket");
		salir(NULL, cadaux);
		return ERR_DAEMON_CREANDO_SOCKET;
	}


	/* S�lo permitimos que escuche un runtime en el puerto */

	/*if ((setsockopt(sockfd,SOL_SOCKET,SO_REUSEADDR,yes,sizeof(int))) == SOCKET_ERROR) {

		salir(NULL, "Error estableciendo opciones de socket");
		return ERR_DAEMON_CREANDO_SOCKET;
	}*/


	timeout_ms = timeout * 1000;

    if (setsockopt(sockfd,SOL_SOCKET,SO_RCVTIMEO,(char*)&timeout_ms,sizeof(int)) == SOCKET_ERROR) {

         salir(NULL, "Error estableciendo opciones de socket");
         return ERR_DAEMON_CREANDO_SOCKET;
    }

	mio.sin_family = PF_INET;
	mio.sin_port = htons(puerto);

	mio.sin_addr.s_addr=inet_addr("0.0.0.0");

	memset(&(mio.sin_zero), '\0', 8);

	if (bind(sockfd, (struct sockaddr *)&mio, sizeof(struct sockaddr)) == SOCKET_ERROR) {

		salir(NULL, "Error con la llamada a bind, puede ser que el puerto est� en uso");
		return ERR_DAEMON_CREANDO_SOCKET;
	}


	if ( listen(sockfd, 10) == SOCKET_ERROR) {

		salir(NULL, "Error estableciendo el socket a la espera de peticiones");
		return ERR_DAEMON_CREANDO_SOCKET;
	}

	return ERR_DAEMON_NO;

}

int finalizar( SOCKET sockcli, struct info_dispositivo info_disp ) {

	int result;
	unsigned int indice;
	int encontrado=0;
	unsigned int posicion;
	
	LOG_Escribir(logHandle, "daemon: Finalizando peticion...\n");

	/* PROCESO DE BUSQUEDA del dispositivo a finalizar */

	/* Si el dispositivo no est� abierto, no intentamos buscar nada */

	if ( info_disp.handle == NULL )
		return ERR_DAEMON_SI;

	indice = 0;
	while ( (indice < num_disp_insertados) && (!encontrado) ) {

		if ( memcmp(disp_insertados[indice].info->id, info_disp.id, 20) == 0) 
			encontrado=1;
		if ( !encontrado ) 
			indice++;
	}

		/* Si el dispositivo no se encuentra, salimos sin liberar nada */
	if (!encontrado)
		return ERR_DAEMON_SI;


	if ( disp_insertados[indice].info->ref_count > 1 ) {

			/* No eliminamos la estructura de informaci�n ni cerramos el dispositivo todav�a */

			disp_insertados[indice].info->ref_count = disp_insertados[indice].info->ref_count - 1;

			/* PROCESO DE CIERRE de socket y handle */

			result = shutdown(sockcli, SD_BOTH);
			if ( result == SOCKET_ERROR ) return ERR_DAEMON_CERRANDO_SOCKET;

			result = closesocket(sockcli);
			if ( result == SOCKET_ERROR ) return ERR_DAEMON_CERRANDO_SOCKET;


	}
	else {

			posicion = indice;


			/* PROCESO DE REESTRUCTURACION de memoria */

				/* Debemos desplazar los bloques desde el final hacia delante para dejar el hueco al final */

			for ( indice = (posicion+1) ; indice < num_disp_insertados ; indice++ ) {

				/* Copiamos de indice a indice-1 */

					/* Copiamos el socket */

				disp_insertados[indice-1].sockcli = disp_insertados[indice].sockcli;

					/* Copiamos la estructura de informaci�n */

				disp_insertados[indice-1].info->modo_pwd = disp_insertados[indice].info->modo_pwd;
				disp_insertados[indice-1].info->handle = disp_insertados[indice].info->handle;
				disp_insertados[indice-1].info->hProv = disp_insertados[indice].info->hProv;
				disp_insertados[indice-1].info->hKey = disp_insertados[indice].info->hKey;
				memset(disp_insertados[indice-1].info->path, 0, MAX_PATH_LEN);
				strncpy(disp_insertados[indice-1].info->path, disp_insertados[indice].info->path, MAX_PATH_LEN);
				disp_insertados[indice-1].info->sector_ini = disp_insertados[indice].info->sector_ini;
				disp_insertados[indice-1].info->tam_zona_reserv = disp_insertados[indice].info->tam_zona_reserv;
				disp_insertados[indice-1].info->current_block = disp_insertados[indice].info->current_block;
				disp_insertados[indice-1].info->total_bloques = disp_insertados[indice].info->total_bloques;
				disp_insertados[indice-1].info->ref_count = disp_insertados[indice].info->ref_count;

				memcpy( disp_insertados[indice-1].info->id, disp_insertados[indice].info->id, 20 );
				memcpy( disp_insertados[indice-1].info->salt, disp_insertados[indice].info->salt, 20 );

			}


			/* PROCESO DE LIBERACION de memoria */
			
			num_disp_insertados = num_disp_insertados - 1;

				/* Liberamos la memoria del �ltimo bloque */
			free(disp_insertados[num_disp_insertados].info);

			disp_insertados = realloc ( disp_insertados, num_disp_insertados * sizeof(struct DISPOSITIVO_INSERTADO) );
			if ( (disp_insertados == NULL) && (num_disp_insertados > 0) ) return ERR_DAEMON_SI;


			/* PROCESO DE CIERRE de socket y handle */

			if (info_disp.handle != NULL) {

/*				char unid[12];

				unid[3]=0;
				strcpy(unid, "\\\\.\\");
				strnc(unid, info_disp.path);
				if ( GetDriveType(unid) == DRIVE_CDROM ) {
					HANDLE hCd;
    				hCd = CreateFile(cdromDeviceName,
						 			 GENERIC_READ,
									 FILE_SHARE_READ,
									 NULL,								 
									 OPEN_EXISTING,						
									 FILE_ATTRIBUTE_NORMAL,
									 NULL);
					CloseHandle(hCd);
				}*/

				result = lowlevel_cerrar_dispositivo( info_disp.handle );
				if ( result != ERR_LOWLEVEL_NO ) return ERR_DAEMON_CERRANDO_DISPOSITIVO;
			}

			result = shutdown(sockcli, SD_BOTH);
			if ( result == SOCKET_ERROR ) {
				result = WSAGetLastError();
				/* Si ocurre esto es porque el socket lo hab�a cerrado el cliente */
				if ( result != WSAENOTSOCK ) return ERR_DAEMON_CERRANDO_SOCKET;
			}

			result = closesocket(sockcli);
			if ( result == SOCKET_ERROR ) {
			
				result = WSAGetLastError();
				/* Si ocurre esto es porque el socket lo hab�a cerrado el cliente */
				if ( result != WSAENOTSOCK ) return ERR_DAEMON_CERRANDO_SOCKET;

			}

	}

	return ERR_DAEMON_NO;
}

int atender_peticion(void* argumento) {

	char cadaux[512];
	int result;
	unsigned char com;
	SOCKET sockcli;
	struct sockaddr_in otro;
	struct parametros *pp;
	struct parametros p;
	struct info_dispositivo info_disp;

	pp = (struct parametros *) argumento;
	p = *pp;

	sockcli = p.sockcli;
	otro = p.otro;
	info_disp.handle = NULL;

	if ( SOLO_CONEXIONES_LOCALES ) {

		/* S�lo se permiten conexiones locales */
		sprintf(cadaux, "%s", inet_ntoa(otro.sin_addr));
		if (strncmp(cadaux, "127.0.0.1", 9) != 0) {

			sprintf(cadaux, "daemon: Conexion desde %s rechazada\n", inet_ntoa(otro.sin_addr));
			LOG_Escribir(logHandle, cadaux);
			finalizar(sockcli, info_disp);
			free(pp);
			return ERR_DAEMON_ATENDIENDO_PETICION;

		}
	}


	sprintf(cadaux, "daemon: Conexion desde %s\n", inet_ntoa(otro.sin_addr));
	LOG_Escribir(logHandle, cadaux);

	while (vivo) {

		result = recibe(sockcli, &com, 1);

		if (result != ERR_DAEMON_NO) {
			LOG_Escribir(logHandle, "daemon: No se ha recibido nueva peticion\n");
			finalizar(sockcli, info_disp);
			free(pp);
			return ERR_DAEMON_ATENDIENDO_PETICION;
		}

		switch (com) {

			case 0:
					sprintf(cadaux, "daemon [%ld]: Solicitada funcion 0 (listar dispositivos)\n", GetCurrentThreadId());
					LOG_Escribir(logHandle, cadaux);

					if ( f0(sockcli) != ERR_DAEMON_NO ) {

						//vivo = false;
						//enviar_resultado(sockcli, 1);	/* La funci�n 0 no env�a c�digo de resultado */
						LOG_Escribir(logHandle, "daemon: Ha ocurrido un error en la funcion 0\n");

						/* Cerramos el socket y el dispositivo */
						result = finalizar(sockcli, info_disp);

						if ( result != ERR_DAEMON_NO ) {

							if ( result == ERR_DAEMON_CERRANDO_SOCKET )
								LOG_Escribir(logHandle, "daemon: No se pudo cerrar el socket correctamente\n");
							else if ( result == ERR_DAEMON_CERRANDO_DISPOSITIVO )
								LOG_Escribir(logHandle, "daemon: No se pudo cerrar el dispositivo correctamente\n");
						}
					}
					break;

			case 1:
					sprintf(cadaux, "daemon [%ld]: Solicitada funcion 1 (iniciar dispositivo)\n", GetCurrentThreadId());
					LOG_Escribir(logHandle, cadaux);

					if ( f1(sockcli, &info_disp) != ERR_DAEMON_NO ) {
						//vivo = false;

						enviar_resultado(sockcli, 1);
						LOG_Escribir(logHandle, "daemon: Ha ocurrido un error en la funcion 1\n");

						/* Cerramos el socket y el dispositivo */
						result = finalizar(sockcli, info_disp);

						if ( result != ERR_DAEMON_NO ) {

							if ( result == ERR_DAEMON_CERRANDO_SOCKET )
								LOG_Escribir(logHandle, "daemon: No se pudo cerrar el socket correctamente\n");
							else if ( result == ERR_DAEMON_CERRANDO_DISPOSITIVO )
								LOG_Escribir(logHandle, "daemon: No se pudo cerrar el dispositivo correctamente\n");
						}						
					}
					break;

			case 2:
					sprintf(cadaux, "daemon [%ld]: Solicitada funcion 2 (leer zona reservada)\n", GetCurrentThreadId());
					LOG_Escribir(logHandle, cadaux);

					if ( f2(sockcli, &info_disp) != ERR_DAEMON_NO ) {
						//vivo = false;

						enviar_resultado(sockcli, 1);
						LOG_Escribir(logHandle, "daemon: Ha ocurrido un error en la funcion 2\n");

						/* Cerramos el socket y el dispositivo */
						result = finalizar(sockcli, info_disp);

						if ( result != ERR_DAEMON_NO ) {

							if ( result == ERR_DAEMON_CERRANDO_SOCKET )
								LOG_Escribir(logHandle, "daemon: No se pudo cerrar el socket correctamente\n");
							else if ( result == ERR_DAEMON_CERRANDO_DISPOSITIVO )
								LOG_Escribir(logHandle, "daemon: No se pudo cerrar el dispositivo correctamente\n");
						}
					}
					break;
						
			case 3:
					sprintf(cadaux, "daemon [%ld]: Solicitada funcion 3 (escribir zona reservada)\n", GetCurrentThreadId());
					LOG_Escribir(logHandle, cadaux);

					if ( f3(sockcli, &info_disp) != ERR_DAEMON_NO ) {
						//vivo = false;

						enviar_resultado(sockcli, 1);
						LOG_Escribir(logHandle, "daemon: Ha ocurrido un error en la funcion 3\n");

						/* Cerramos el socket y el dispositivo */
						result = finalizar(sockcli, info_disp);

						if ( result != ERR_DAEMON_NO ) {

							if ( result == ERR_DAEMON_CERRANDO_SOCKET )
								LOG_Escribir(logHandle, "daemon: No se pudo cerrar el socket correctamente\n");
							else if ( result == ERR_DAEMON_CERRANDO_DISPOSITIVO )
								LOG_Escribir(logHandle, "daemon: No se pudo cerrar el dispositivo correctamente\n");
						}

					}
					break;

			case 4:
					sprintf(cadaux, "daemon [%ld]: Solicitada funcion 4 (cambiar password)\n", GetCurrentThreadId());
					LOG_Escribir(logHandle, cadaux);

					if ( f4(sockcli, &info_disp) != ERR_DAEMON_NO ) {
						//vivo = false;

						enviar_resultado(sockcli, 1);
						LOG_Escribir(logHandle, "daemon: Ha ocurrido un error en la funcion 4\n");

						/* Cerramos el socket y el dispositivo */
						result = finalizar(sockcli, info_disp);

						if ( result != ERR_DAEMON_NO ) {

							if ( result == ERR_DAEMON_CERRANDO_SOCKET )
								LOG_Escribir(logHandle, "daemon: No se pudo cerrar el socket correctamente\n");
							else if ( result == ERR_DAEMON_CERRANDO_DISPOSITIVO )
								LOG_Escribir(logHandle, "daemon: No se pudo cerrar el dispositivo correctamente\n");
						}

					}
					break;

			case 5:
					sprintf(cadaux, "daemon [%ld]: Solicitada funcion 5 (bloque identificativo)\n", GetCurrentThreadId());
					LOG_Escribir(logHandle, cadaux);

					if ( f5(sockcli, &info_disp) != ERR_DAEMON_NO ) {
						//vivo = false;

						enviar_resultado(sockcli, 1);
						LOG_Escribir(logHandle, "daemon: Ha ocurrido un error en la funcion 5\n");

						/* Cerramos el socket y el dispositivo */
						result = finalizar(sockcli, info_disp);

						if ( result != ERR_DAEMON_NO ) {

							if ( result == ERR_DAEMON_CERRANDO_SOCKET )
								LOG_Escribir(logHandle, "daemon: No se pudo cerrar el socket correctamente\n");
							else if ( result == ERR_DAEMON_CERRANDO_DISPOSITIVO )
								LOG_Escribir(logHandle, "daemon: No se pudo cerrar el dispositivo correctamente\n");
						}
					}
					break;

			case 6:
					sprintf(cadaux, "daemon [%ld]: Solicitada funcion 6 (leer bloque crypto, bajo nivel)\n", GetCurrentThreadId());
					LOG_Escribir(logHandle, cadaux);

					if ( f6(sockcli, &info_disp) != ERR_DAEMON_NO ) {
						//vivo = false;

						enviar_resultado(sockcli, 1);
						LOG_Escribir(logHandle, "daemon: Ha ocurrido un error en la funcion 6\n");

						/* Cerramos el socket y el dispositivo */
						result = finalizar(sockcli, info_disp);

						if ( result != ERR_DAEMON_NO ) {

							if ( result == ERR_DAEMON_CERRANDO_SOCKET )
								LOG_Escribir(logHandle, "daemon: No se pudo cerrar el socket correctamente\n");
							else if ( result == ERR_DAEMON_CERRANDO_DISPOSITIVO )
								LOG_Escribir(logHandle, "daemon: No se pudo cerrar el dispositivo correctamente\n");
						}
					}
					break;

			case 7:
					sprintf(cadaux, "daemon [%ld]: Solicitada funcion 7 (leer primer bloque crypto de un tipo)\n", GetCurrentThreadId());
					LOG_Escribir(logHandle, cadaux);

					if ( f7(sockcli, &info_disp) != ERR_DAEMON_NO ) {
						//vivo = false;

						enviar_resultado(sockcli, 1);
						LOG_Escribir(logHandle, "daemon: Ha ocurrido un error en la funcion 7\n");

						/* Cerramos el socket y el dispositivo */
						result = finalizar(sockcli, info_disp);

						if ( result != ERR_DAEMON_NO ) {

							if ( result == ERR_DAEMON_CERRANDO_SOCKET )
								LOG_Escribir(logHandle, "daemon: No se pudo cerrar el socket correctamente\n");
							else if ( result == ERR_DAEMON_CERRANDO_DISPOSITIVO )
								LOG_Escribir(logHandle, "daemon: No se pudo cerrar el dispositivo correctamente\n");
						}

					}
					break;

			case 8:
					sprintf(cadaux, "daemon [%ld]: Solicitada funcion 8 (leer siguiente bloque crypto)\n", GetCurrentThreadId());
					LOG_Escribir(logHandle, cadaux);

					if ( f8(sockcli, &info_disp) != ERR_DAEMON_NO ) {
						//vivo = false;

						enviar_resultado(sockcli, 1);
						LOG_Escribir(logHandle, "daemon: Ha ocurrido un error en la funcion 8\n");

						/* Cerramos el socket y el dispositivo */
						result = finalizar(sockcli, info_disp);

						if ( result != ERR_DAEMON_NO ) {

							if ( result == ERR_DAEMON_CERRANDO_SOCKET )
								LOG_Escribir(logHandle, "daemon: No se pudo cerrar el socket correctamente\n");
							else if ( result == ERR_DAEMON_CERRANDO_DISPOSITIVO )
								LOG_Escribir(logHandle, "daemon: No se pudo cerrar el dispositivo correctamente\n");
						}
					}
					break;

			case 9:
					sprintf(cadaux, "daemon [%ld]: Solicitada funcion 9 (leer todos los bloques crypto de un tipo)\n", GetCurrentThreadId());
					LOG_Escribir(logHandle, cadaux);

					if ( f9(sockcli, &info_disp) != ERR_DAEMON_NO ) {
						//vivo = false;

						enviar_resultado(sockcli, 1);
						LOG_Escribir(logHandle, "daemon: Ha ocurrido un error en la funcion 9\n");

						/* Cerramos el socket y el dispositivo */
						result = finalizar(sockcli, info_disp);

						if ( result != ERR_DAEMON_NO ) {

							if ( result == ERR_DAEMON_CERRANDO_SOCKET )
								LOG_Escribir(logHandle, "daemon: No se pudo cerrar el socket correctamente\n");
							else if ( result == ERR_DAEMON_CERRANDO_DISPOSITIVO )
								LOG_Escribir(logHandle, "daemon: No se pudo cerrar el dispositivo correctamente\n");
						}
					}
					break;

			case 10:
					sprintf(cadaux, "daemon [%ld]: Solicitada funcion 10 (leer todos los bloques crypto no nulos)\n", GetCurrentThreadId());
					LOG_Escribir(logHandle, cadaux);

					if ( f10(sockcli, &info_disp) != ERR_DAEMON_NO ) {
						//vivo = false;

						enviar_resultado(sockcli, 1);
						LOG_Escribir(logHandle, "daemon: Ha ocurrido un error en la funcion 10\n");

						/* Cerramos el socket y el dispositivo */
						result = finalizar(sockcli, info_disp);

						if ( result != ERR_DAEMON_NO ) {

							if ( result == ERR_DAEMON_CERRANDO_SOCKET )
								LOG_Escribir(logHandle, "daemon: No se pudo cerrar el socket correctamente\n");
							else if ( result == ERR_DAEMON_CERRANDO_DISPOSITIVO )
								LOG_Escribir(logHandle, "daemon: No se pudo cerrar el dispositivo correctamente\n");
						}
					}
					break;

			case 11:
					sprintf(cadaux, "daemon [%ld]: Solicitada funcion 11 (escribir bloque crypto, bajo nivel)\n", GetCurrentThreadId());
					LOG_Escribir(logHandle, cadaux);

					if ( f11(sockcli, &info_disp) != ERR_DAEMON_NO ) {
						//vivo = false;

						enviar_resultado(sockcli, 1);
						LOG_Escribir(logHandle, "daemon: Ha ocurrido un error en la funcion 11\n");

						/* Cerramos el socket y el dispositivo */
						result = finalizar(sockcli, info_disp);

						if ( result != ERR_DAEMON_NO ) {

							if ( result == ERR_DAEMON_CERRANDO_SOCKET )
								LOG_Escribir(logHandle, "daemon: No se pudo cerrar el socket correctamente\n");
							else if ( result == ERR_DAEMON_CERRANDO_DISPOSITIVO )
								LOG_Escribir(logHandle, "daemon: No se pudo cerrar el dispositivo correctamente\n");
						}
					}
					break;

			case 12:
					sprintf(cadaux, "daemon [%ld]: Solicitada funcion 12 (anyadir bloque crypto)\n", GetCurrentThreadId());
					LOG_Escribir(logHandle, cadaux);

					if ( f12(sockcli, &info_disp) != ERR_DAEMON_NO ) {
						//vivo = false;

						enviar_resultado(sockcli, 1);
						LOG_Escribir(logHandle, "daemon: Ha ocurrido un error en la funcion 12\n");

						/* Cerramos el socket y el dispositivo */
						result = finalizar(sockcli, info_disp);

						if ( result != ERR_DAEMON_NO ) {

							if ( result == ERR_DAEMON_CERRANDO_SOCKET )
								LOG_Escribir(logHandle, "daemon: No se pudo cerrar el socket correctamente\n");
							else if ( result == ERR_DAEMON_CERRANDO_DISPOSITIVO )
								LOG_Escribir(logHandle, "daemon: No se pudo cerrar el dispositivo correctamente\n");
						}
					}
					break;

			case 13:
					sprintf(cadaux, "daemon [%ld]: Solicitada funcion 13 (borrar bloque crypto)\n", GetCurrentThreadId());
					LOG_Escribir(logHandle, cadaux);

					if ( f13(sockcli, &info_disp) != ERR_DAEMON_NO ) {
						//vivo = false;

						enviar_resultado(sockcli, 1);
						LOG_Escribir(logHandle, "daemon: Ha ocurrido un error en la funcion 13\n");

						/* Cerramos el socket y el dispositivo */
						result = finalizar(sockcli, info_disp);

						if ( result != ERR_DAEMON_NO ) {

							if ( result == ERR_DAEMON_CERRANDO_SOCKET )
								LOG_Escribir(logHandle, "daemon: No se pudo cerrar el socket correctamente\n");
							else if ( result == ERR_DAEMON_CERRANDO_DISPOSITIVO )
								LOG_Escribir(logHandle, "daemon: No se pudo cerrar el dispositivo correctamente\n");
						}
					}
					break;

			case 14:
				sprintf(cadaux, "daemon [%ld]: Solicitada funcion 14 (obtener identificadores)\n", GetCurrentThreadId());
				LOG_Escribir(logHandle, cadaux);

				if ( f14(sockcli, &info_disp) != ERR_DAEMON_NO ) {
						enviar_resultado(sockcli, 1);
						LOG_Escribir(logHandle, "daemon: Ha ocurrido un error en la funcion 14\n");
				}

				break;

			case 20:
					sprintf(cadaux, "daemon [%ld]: Solicitada funcion 20 (finalizar comunicacion)\n", GetCurrentThreadId());
					LOG_Escribir(logHandle, cadaux);

					//vivo = false;
					enviar_resultado(sockcli, 0);
					
					/* Cerramos el socket y el dispositivo */

					result = finalizar(sockcli, info_disp);
					if ( result != ERR_DAEMON_NO ) {

						if ( result == ERR_DAEMON_CERRANDO_SOCKET )
								LOG_Escribir(logHandle, "daemon: No se pudo cerrar el socket correctamente\n");
						else if ( result == ERR_DAEMON_CERRANDO_DISPOSITIVO )
								LOG_Escribir(logHandle, "daemon: No se pudo cerrar el dispositivo correctamente\n");
					}

					free(pp);
					return result;

					break;

			default:
					sprintf(cadaux, "daemon: Funcion desconocida (%d)\n",com);
					LOG_Escribir(logHandle, cadaux);
		}

	}

	free(pp);
	return ERR_DAEMON_NO;
}



/**********************/
/* PROGRAMA PRINCIPAL */
/**********************/

int daemon_start(LOG_HANDLE logH) {

	int result,dum,i;
	struct sockaddr_in otro;
	SOCKET sockcli;
	char cadaux[512];
	int *disp;
	int nDisp;
	char * dispositivos[MAX_DEVICES];

	WORD wVersionRequested;
	WSADATA wsaData;
	HANDLE hThread;
	DWORD dwThreadId;

	struct parametros *parametros = NULL;

	logHandle = logH;	/* Establecemos el manejador del log global */

	// Inicializaciones
	
	wVersionRequested = MAKEWORD(1,1);

	result = WSAStartup( wVersionRequested, &wsaData );

	if ( result != 0 ) {
		sprintf(cadaux,"Error enlazando con la librer�a WinSock");
		salir(NULL, cadaux);
		return ERR_DAEMON_WINSOCK_LIBRARY;
	}


	if ( LOBYTE( wsaData.wVersion ) != 1 ||
		    HIBYTE( wsaData.wVersion ) != 1 ) {
		sprintf(cadaux,"Error, no hay una versi�n de la librer�a WinSock v�lida en el sistema");
		salir(NULL, cadaux);
		WSACleanup();
		return ERR_DAEMON_WINSOCK_VERSION;
	}	

	result = inicializa_socket();
	if ( result != ERR_DAEMON_NO ) return ERR_DAEMON_SI;

	vivo = true;

	dum = sizeof(struct sockaddr_in);

	//listar_dispositivos(&disp,&nDisp);
	lowlevel_enumerar_dispositivos_ex(dispositivos,&nDisp);

	for ( i = 0 ; i < MAX_DEVICES && dispositivos[i] ; i++)
		free(dispositivos[i]);

	num_disp_insertados = 0;		/* N�mero de dispositivos inicialmente insertados */

	// Bucle principal

	while (1) {

		if ((sockcli = accept(sockfd, (struct sockaddr *)&otro, &dum)) == INVALID_SOCKET) {

			LOG_Escribir(logHandle, "daemon: Error en socket (accept)\n");
			continue;
		}
		else {		// Si no hay error

				parametros = ( struct parametros *) malloc ( sizeof (struct parametros) );
				if ( ! parametros ) {
					LOG_Escribir(logHandle, "daemon: No memoria\n");
					close(sockcli);
					continue;
				}
				parametros->otro = otro;
				parametros->sockcli = sockcli;

				// Creamos un nuevo thread que atender� la petici�n 

				hThread = CreateThread( NULL,											// default security attributes 
										0,												// use default stack size  
										(LPTHREAD_START_ROUTINE) atender_peticion,		// thread function 
										parametros,									// argument to thread function 
										0,												// use default creation flags 
										&dwThreadId);									// returns the thread identifier 
 
				if (hThread == NULL) {
					sprintf(cadaux, "Error creando hilo para atender petici�n desde %s\n", inet_ntoa(otro.sin_addr));
					LOG_Escribir(logHandle, cadaux);
					free(parametros);
					close(sockcli);
					continue;
				}

		}

	}


	return ERR_DAEMON_NO;
}