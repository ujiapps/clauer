#include "DlgPassword.h"
#include <stdio.h>
#include "resource.h"



static HANDLE mutex = NULL;
static PIN gPin = NULL;
static int *gDisp = NULL;
static int gNDisp = 0;
static int gDispSel = -1;
static int *gTblDispositivos;  /* Asocia �ndice de listbox con dispositivo */



/*
 * Esta se encarga de procesar los mensajes del di�logo
 */

LRESULT CALLBACK DlgProc(HWND hWndDlg, UINT Msg,
		       WPARAM wParam, LPARAM lParam)
{
	int tamPIN, j, selIndex;
	char kk[20];
	HWND hLista, hDesktop;
	RECT dimDesktop, dimDialog;
	
	switch(Msg)
	{
	case WM_INITDIALOG:


		/* Rellenamos la lista de dispositivos */

		gTblDispositivos = (int *) malloc (sizeof(int)*gNDisp);
		if ( !gTblDispositivos ) {
			EndDialog(hWndDlg,-1);
		}
		for ( j = 0 ; j < gNDisp ; j++ ) {
			sprintf(kk, "Dispositivo %d", gDisp[j]);
			gTblDispositivos[SendDlgItemMessage(hWndDlg,IDC_LISTA,LB_ADDSTRING,gDisp[j],(LPARAM)kk)] = gDisp[j];
		}

		/* Seleccionamos el primer elemento de la lista */

		if ( gNDisp > 0 )
			SendDlgItemMessage(hWndDlg,IDC_LISTA,LB_SETCURSEL,0,0);

		/* Vaciamos el campo password */

		SetDlgItemText(hWndDlg,IDC_PIN, "");

		/* Ponemos el icono de la ventana */



		/* Centramos la ventana */

		hDesktop = GetDesktopWindow();
		GetWindowRect(hDesktop,&dimDesktop);
		GetWindowRect(hWndDlg,&dimDialog);
		SetWindowPos(hWndDlg,
					 HWND_TOP,
					 ((dimDesktop.right-dimDesktop.left)-(dimDialog.right-dimDialog.left))/2,
					 ((dimDesktop.bottom-dimDesktop.top)-(dimDialog.bottom-dimDialog.top))/2,
					 0,
					 0,
					 SWP_NOSIZE);
		return TRUE;

	case WM_COMMAND:
		switch(LOWORD(wParam))
		{
		case IDOK:

			tamPIN = GetWindowTextLength(GetDlgItem(hWndDlg,IDC_PIN));
			gPin = (PIN) malloc(tamPIN+1);
			if ( !gPin ) {
				free(gPin);
				free(gTblDispositivos);
				EndDialog(hWndDlg,-1);
			}
			if ( !GetDlgItemText(hWndDlg,IDC_PIN,gPin,tamPIN+1) ) {
				free(gPin);
				free(gTblDispositivos);
				EndDialog(hWndDlg,-1);
			}
			gPin[tamPIN] = '\0';

			/*
			 * Obtengo el dispositivo seleccionado
			 */

			hLista = GetDlgItem(hWndDlg, IDC_LISTA);

			selIndex = SendMessage(hLista,LB_GETCURSEL,0,0);
			gDispSel = gTblDispositivos[selIndex];			
			free(gTblDispositivos);
			
			EndDialog(hWndDlg, 0);
			return TRUE;
		case IDCANCEL:
			free(gTblDispositivos);
			EndDialog(hWndDlg, 1);
			return TRUE;
		case IDC_PIN:
			tamPIN = GetWindowTextLength(GetDlgItem(hWndDlg,IDC_PIN));
			if ( tamPIN == 0 ) {
				
			} else {

			}
			return TRUE;
		}
		break;
	}

	return FALSE;
}




/*
 * 0 --> OK
 * 1 --> CANCEL
 * -1 --> ERROR
 */

int PedirPIN (int *disp, int nDisp, int *dispSel, PIN *pin)
{
	int ret;

	if ( !mutex ) {
		mutex = CreateMutex(NULL,FALSE,NULL);
		if  (!mutex)
			return -1;
	}

	if ( WaitForSingleObject(mutex, 1200000) == WAIT_FAILED)    /* Espero dos minutos si el handle est� adquirido */
		return -1;
	
		
	gDisp = (int *) malloc (nDisp*sizeof(int));
	if ( !gDisp) {
		ReleaseMutex(mutex);
		return -1;
	}
	memcpy(gDisp, disp,nDisp*sizeof(int));
	gNDisp = nDisp;

	ret = DialogBox(GetModuleHandle(NULL),MAKEINTRESOURCE(IDD_PASSWORDLISTA),NULL,DlgProc);

	if ( ret == 0 ) {
		*pin = (PIN) malloc (strlen(gPin)+1);
		strcpy(*pin,gPin);
		SecureZeroMemory(gPin, strlen(gPin)+1);
		free(gPin);
		gPin = NULL;
		*dispSel = gDispSel;
	}


	ReleaseMutex(mutex);

	return ret;
}



