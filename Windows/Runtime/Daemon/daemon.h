#define		ERR_DAEMON_NO						 0
#define		ERR_DAEMON_SI						 1
#define		ERR_DAEMON_ENVIANDO					 2
#define		ERR_DAEMON_RECIBIENDO				 3
#define		ERR_DAEMON_CREANDO_SOCKET			 4
#define		ERR_DAEMON_WINSOCK_LIBRARY			 5
#define		ERR_DAEMON_WINSOCK_VERSION			 6
#define		ERR_DAEMON_TIMED_OUT				 7
#define		ERR_DAEMON_ATENDIENDO_PETICION		 8
#define		ERR_DAEMON_CREANDO_THREAD			 9
#define		ERR_DAEMON_COMPROBANDO_DISPOSITIVO	10
#define		ERR_DAEMON_CERRANDO_SOCKET			11
#define		ERR_DAEMON_CERRANDO_DISPOSITIVO		12
#define		ERR_DAEMON_CONNECTION_CLOSED		13

#define		TAM_BLOQUE							10240
#define		TAM_BUFFER							5242880
#define		TAM_PADDING							8			/* Tama�o del padding, en bytes */
#define		TAM_CABECERA						8			/* Tama�o de la cabecera del bloque */
#define		MAX_PASS_LEN                        128
#define		puerto								969
#define		HW_ID_LEN							16



#include <log/log.h>

int daemon_start(LOG_HANDLE logH);
int eliminar_dispositivo( LOG_HANDLE logHandle );
int listar_dispositivos (int **dispositivos, int *nDispositivos);