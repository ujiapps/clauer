#define _WIN32_WINNT   0x500
#define WINVER		   0x500

#include <windows.h>
#include <winsvc.h>
#include <winuser.h>
#include <winbase.h>
#include <dbt.h>
#include <winioctl.h>
#include <devguid.h>
#include <tchar.h>

/*
 * Includes propios
 */ 

#include <log/log.h>
#include <Daemon/daemon.h>

#define DESCRIPCION_SERVICIO	TEXT("Runtime del proyecto Clauer")
#define NOMBRE_SERVICIO			TEXT("USBPKI Clauer")


SERVICE_STATUS          MyServiceStatus; 
SERVICE_STATUS_HANDLE   MyServiceStatusHandle; 

BOOL parar;

//HDEVNOTIFY hNotify;

LOG_HANDLE logHandle;
BOOL logOk;


/*
 * Prototipos de las funciones del servicio
 */

VOID  MyServiceStart (DWORD dwArgc, LPTSTR *lpszArgv); 
VOID  MyServiceCtrlHandler (DWORD opcode,DWORD dwEventType,LPVOID lpEventData,LPVOID lpContext); 
DWORD MyServiceInitialization (DWORD argc, LPTSTR *argv, DWORD *specificError); 

void RunService       (void);
void InstallService   (void);
void UninstallService (void);
void ImprimirUso      (TCHAR *nombrePrograma);


void InstallService()
{
	SC_HANDLE serviceControlManager = OpenSCManager( 0, 0, SC_MANAGER_CREATE_SERVICE );

	if ( serviceControlManager )
	{
		TCHAR path[ _MAX_PATH + 1 ];
		if ( GetModuleFileName( 0, path, sizeof(path)/sizeof(path[0]) ) > 0 )
		{
			SERVICE_DESCRIPTION descripcionServicio;
			SC_HANDLE service = CreateService( serviceControlManager,
							NOMBRE_SERVICIO, NOMBRE_SERVICIO,
							SERVICE_ALL_ACCESS, SERVICE_WIN32_OWN_PROCESS|SERVICE_INTERACTIVE_PROCESS,
							SERVICE_AUTO_START, SERVICE_ERROR_IGNORE, path,
							NULL, NULL, NULL, NULL, NULL );


			if ( !service ) {
				fprintf(stderr, "ERROR. Imposible instalar servicio: %d\n", GetLastError());
				exit(1);
			}

			/*
			 * Introducimos el texto descriptivo del servicio
			 */

			descripcionServicio.lpDescription = DESCRIPCION_SERVICIO;

			if ( !ChangeServiceConfig2(service,SERVICE_CONFIG_DESCRIPTION, (LPVOID) &descripcionServicio) ) {
				fprintf(stderr, "ERROR. Imposible establecer la descripci�n del servicio\n");
				fprintf(stderr, "       C�digo de error = %ld\n", GetLastError());
				exit(1);
			}

			if ( service )
				CloseServiceHandle( service );
		}

		CloseServiceHandle( serviceControlManager );
	}
}






void UninstallService()
{
	SC_HANDLE serviceControlManager = OpenSCManager( 0, 0, SC_MANAGER_CONNECT );

	if ( serviceControlManager )
	{
		SC_HANDLE service = OpenService( serviceControlManager,
			NOMBRE_SERVICIO, SERVICE_QUERY_STATUS | DELETE );
		if ( service )
		{
			SERVICE_STATUS serviceStatus;
			if ( QueryServiceStatus( service, &serviceStatus ) )
			{
				if ( serviceStatus.dwCurrentState == SERVICE_STOPPED )
					DeleteService( service );
			}

			CloseServiceHandle( service );
		}

		CloseServiceHandle( serviceControlManager );
	}
}


 

int _tmain( int argc, TCHAR* argv[] )
{

	if ( argc > 1 && lstrcmpi( argv[1], TEXT("/i") ) == 0 )
	{
		InstallService();
	}
	else if ( argc > 1 && lstrcmpi( argv[1], TEXT("/u") ) == 0 )
	{
		UninstallService();
	}
	else if ( argc > 1 && lstrcmpi( argv[1], TEXT("/h") ) == 0 )
	{
		ImprimirUso(argv[0]);

	}
	else {
		RunService();
	}

	return 0;
}




void RunService(void) 
{ 
    SERVICE_TABLE_ENTRY   DispatchTable[] = 
    { 
        { NOMBRE_SERVICIO, (LPSERVICE_MAIN_FUNCTION) MyServiceStart      }, 
        { NULL,              NULL          } 
    }; 

	logOk = LOG_Nuevo("C:\\logServicio.log", &logHandle);

 
    if (!StartServiceCtrlDispatcher(DispatchTable)) {

		/*DWORD err;
		err = GetLastError();

		switch (err) {

		case ERROR_FAILED_SERVICE_CONTROLLER_CONNECT:
			sprintf(kk,"ERROR_FAILED_SERVICE_CONTROLLER_CONNECT\n");
			break;
		case ERROR_INVALID_DATA:
			sprintf(kk,"ERROR_INVALID_DATA\n");
			break;
		case ERROR_SERVICE_ALREADY_RUNNING:
			sprintf(kk, "ERROR_SERVICE_ALREADY_RUNNING\n");
			break;
		}*/

		if ( logOk ) LOG_Escribir(logHandle, "service: RunService error, No se pudo establecer el CtrDispatcher\n");

	}
			
	if ( logOk ) LOG_Fin(&logHandle);

} 
 




VOID MyServiceStart (DWORD dwArgc, LPTSTR *lpszArgv)
{
	int result;
	DEV_BROADCAST_DEVICEINTERFACE NotificationFilter;
	DEV_BROADCAST_VOLUME NotFilterVol;
	HDEVNOTIFY *hDevNotify;
	GUID GUID_DEV_INTERFACE_USB_DEVICE;
	GUID GUID_DEVINTERFACE_DISK;

	hDevNotify = malloc (sizeof(HDEVNOTIFY));

	parar = FALSE;
	if ( logOk ) LOG_Escribir(logHandle, "service: Servicio iniciado\n");



	//LOG_Escribir(logHandle, "MyServiceStart. Empiezo\n");

	MyServiceStatus.dwServiceType             = SERVICE_WIN32_OWN_PROCESS;
	MyServiceStatus.dwCurrentState            = SERVICE_START_PENDING;
	MyServiceStatus.dwControlsAccepted        = SERVICE_ACCEPT_SHUTDOWN | SERVICE_ACCEPT_STOP | SERVICE_ACCEPT_PAUSE_CONTINUE;
	MyServiceStatus.dwWin32ExitCode           = 0;
	MyServiceStatus.dwServiceSpecificExitCode = 0;
	MyServiceStatus.dwCheckPoint              = 0;
	MyServiceStatus.dwWaitHint                = 0;

	MyServiceStatusHandle = RegisterServiceCtrlHandlerEx( NOMBRE_SERVICIO, (LPHANDLER_FUNCTION_EX) MyServiceCtrlHandler, NULL); 
 
    if (MyServiceStatusHandle == (SERVICE_STATUS_HANDLE)0) 
    { 
		if ( logOk ) LOG_Escribir(logHandle, "service: MyServiceStart,RegisterServiceCtrlHandler fall�\n");
        return; 
    } 
 
	/* Registramos las notificaciones de eventos de dispositivos */

	ZeroMemory( &NotificationFilter, sizeof(NotificationFilter));

  //GUID_DEVINTERFACE_USB_DEVICE: TGUID = '{A5DCBF10-6530-11D2-901F-00C04FB951ED}';

	GUID_DEV_INTERFACE_USB_DEVICE.Data1 = 0xA5DCBF10;
	GUID_DEV_INTERFACE_USB_DEVICE.Data2 = 0x6530;
	GUID_DEV_INTERFACE_USB_DEVICE.Data3 = 0x11D2;
	GUID_DEV_INTERFACE_USB_DEVICE.Data4[0] = 0x90;
	GUID_DEV_INTERFACE_USB_DEVICE.Data4[1] = 0x1F;
	GUID_DEV_INTERFACE_USB_DEVICE.Data4[2] = 0x00;
	GUID_DEV_INTERFACE_USB_DEVICE.Data4[3] = 0xC0;
	GUID_DEV_INTERFACE_USB_DEVICE.Data4[4] = 0x4F;
	GUID_DEV_INTERFACE_USB_DEVICE.Data4[5] = 0xB9;
	GUID_DEV_INTERFACE_USB_DEVICE.Data4[6] = 0x51;
	GUID_DEV_INTERFACE_USB_DEVICE.Data4[7] = 0xED;


/*
	GUID_DEVINTERFACE_DISK.Data1 = 0x53f56307;
	GUID_DEVINTERFACE_DISK.Data2 = 0xb6bf;
	GUID_DEVINTERFACE_DISK.Data3 = 0x11d0;
	GUID_DEVINTERFACE_DISK.Data4[0] = 0x94;
	GUID_DEVINTERFACE_DISK.Data4[1] = 0xf2;
	GUID_DEVINTERFACE_DISK.Data4[2] = 0x00;
	GUID_DEVINTERFACE_DISK.Data4[3] = 0xa0;
	GUID_DEVINTERFACE_DISK.Data4[4] = 0xc9;
	GUID_DEVINTERFACE_DISK.Data4[5] = 0x1e;
	GUID_DEVINTERFACE_DISK.Data4[6] = 0xfb;
	GUID_DEVINTERFACE_DISK.Data4[7] = 0x8b;
*/

/*	GUID_DEVINTERFACE_DISK.Data1 = 0x53f56308L;
	GUID_DEVINTERFACE_DISK.Data2 = 0xb6bf;
	GUID_DEVINTERFACE_DISK.Data3 = 0x11d0;
	GUID_DEVINTERFACE_DISK.Data4[0] = 0x94;
	GUID_DEVINTERFACE_DISK.Data4[1] = 0xf2;
	GUID_DEVINTERFACE_DISK.Data4[2] = 0x00;
	GUID_DEVINTERFACE_DISK.Data4[3] = 0xa0;
	GUID_DEVINTERFACE_DISK.Data4[4] = 0xc9;
	GUID_DEVINTERFACE_DISK.Data4[5] = 0x1e;
	GUID_DEVINTERFACE_DISK.Data4[6] = 0xfb;
	GUID_DEVINTERFACE_DISK.Data4[7] = 0x8b;
*/
	NotificationFilter.dbcc_size = sizeof(DEV_BROADCAST_DEVICEINTERFACE);
	NotificationFilter.dbcc_devicetype = DBT_DEVTYP_DEVICEINTERFACE;
	NotificationFilter.dbcc_classguid = GUID_DEV_INTERFACE_USB_DEVICE;
	//NotificationFilter.dbcc_classguid = GUID_DEVINTERFACE_DISK;
	//NotificationFilter.dbcc_name = 0;

/*	NotFilterVol.dbcv_size = sizeof(DEV_BROADCAST_VOLUME);
	NotFilterVol.dbcv_devicetype =  DBT_DEVTYP_VOLUME;
	NotFilterVol.dbcv_reserved = 0;
	NotFilterVol.dbcv_unitmask = 0xfffe;
	NotFilterVol.dbcv_flags = DBTF_MEDIA;
*/
	*hDevNotify = RegisterDeviceNotification( MyServiceStatusHandle, &NotificationFilter , DEVICE_NOTIFY_SERVICE_HANDLE);
	/***hDevNotify = RegisterDeviceNotification( MyServiceStatusHandle, &NotFilterVol , DEVICE_NOTIFY_SERVICE_HANDLE );*/

	if (!*hDevNotify) {
		if (logOk) LOG_Escribir(logHandle, "service: RegisterDeviceNotification fall�\n");
	}
    
	// Initialization code goes here. 

    // Initialization complete - report running status. 

    MyServiceStatus.dwCurrentState       = SERVICE_RUNNING; 
    MyServiceStatus.dwCheckPoint         = 0; 
    MyServiceStatus.dwWaitHint           = 0; 
 
    if (!SetServiceStatus (MyServiceStatusHandle, &MyServiceStatus)) 
		LOG_Escribir(logHandle, "service: Error estableciendo estado del servicio\n");
 
    // This is where the service does its work. 



	if (logOk) {
		result = daemon_start(logHandle);
		if ( result != ERR_DAEMON_NO ) {
			LOG_Escribir(logHandle, "service: Error en la inicializaci�n del servicio\n");
			
			MyServiceStatus.dwCurrentState = SERVICE_STOPPED;
			MyServiceStatus.dwCheckPoint         = 0; 
			MyServiceStatus.dwWaitHint           = 0;
			MyServiceStatus.dwWin32ExitCode      = ERROR_SERVICE_SPECIFIC_ERROR;
			MyServiceStatus.dwServiceSpecificExitCode = 1;
			
			if (!SetServiceStatus (MyServiceStatusHandle, &MyServiceStatus)) 
				LOG_Escribir(logHandle, "service: Error parando el servicio\n");

		}
	}
 
	free(hDevNotify);

    return; 
}



VOID MyServiceCtrlHandler (DWORD opcode,DWORD dwEventType,LPVOID lpEventData,LPVOID lpContext)
{

	//if ( logOk ) LOG_Escribir(logHandle, "MyServiceCtrlHandler:Comenzamos\n");

    switch(opcode) {

		case SERVICE_CONTROL_PAUSE:
			MyServiceStatus.dwCurrentState = SERVICE_PAUSED;
			break;

		case SERVICE_CONTROL_CONTINUE:
			MyServiceStatus.dwCurrentState = SERVICE_RUNNING;
			break;

		case SERVICE_CONTROL_STOP:

			parar = TRUE;

			MyServiceStatus.dwWin32ExitCode = 0;
			MyServiceStatus.dwCurrentState = SERVICE_STOPPED;
			MyServiceStatus.dwCheckPoint = 0;
			MyServiceStatus.dwWaitHint = 0;

			if ( !SetServiceStatus(MyServiceStatusHandle, &MyServiceStatus) ) {
				if ( logOk ) LOG_Escribir(logHandle, "service: Error parando servicio\n");
			}
		

			if ( logOk) LOG_Escribir(logHandle, "service: Servicio parado\n");
			return;


		case SERVICE_CONTROL_SHUTDOWN:
			if ( logOk ) LOG_Escribir(logHandle, "service: Servicio apagado\n");
			break;

		case SERVICE_CONTROL_INTERROGATE: 
			break; 
 
		/* Notificaci�n de inserci�n o retirada de dispositivos */

		case SERVICE_CONTROL_DEVICEEVENT:


			switch (dwEventType) {

				/* The system broadcasts the DBT_DEVICEARRIVAL device event when a device or piece of media 
				   has been inserted and becomes available. */

				case DBT_DEVICEARRIVAL:
				//						if (logOk) LOG_Escribir(logHandle, "service: Detectado DBT_DEVICEARRIVAL\n");
										break;
				
				/* The system broadcasts the DBT_DEVICEREMOVECOMPLETE device event when a device or piece of 
				   media has been physically removed. */

				case DBT_DEVICEREMOVECOMPLETE:	//if (logOk) LOG_Escribir(logHandle, "service: Detectado DBT_DEVICEREMOVECOMPLETE\n");
												eliminar_dispositivo(logHandle);
												break;

				/* The system broadcasts the DBT_DEVICEQUERYREMOVE device event to request permission to remove 
				   a device or piece of media. This message is the last chance for applications and drivers to 
				   prepare for this removal. However, any application can deny this request and cancel the operation. */

				//case DBT_DEVICEQUERYREMOVE:		if (logOk) LOG_Escribir(logHandle, "service: Detectado DBT_DEVICEQUERYREMOVE\n");
				//								break;

				/* The system broadcasts the DBT_DEVICEQUERYREMOVEFAILED device event when a request to remove 
				   a device or piece of media has been canceled. */

				//case DBT_DEVICEQUERYREMOVEFAILED:	if (logOk) LOG_Escribir(logHandle, "service: Detectado DBT_DEVICEQUERYREMOVEFAILED\n");
				//									break;

				/* The system broadcasts the DBT_DEVICEREMOVEPENDING device event when a device or piece of media 
				   is being removed and is no longer available for use. */

				//case DBT_DEVICEREMOVEPENDING:	if (logOk) LOG_Escribir(logHandle, "service: Detectado DBT_DEVICEREMOVEPENDING\n");	
				//								break;

				default:	if (logOk) LOG_Escribir(logHandle, "service: Detectado evento de dispositivo desconocido\n");
							break;

			}

			break;

		default: 
			if ( logOk ) LOG_Escribir(logHandle, "service: MyServiceCtrlHandler,Opcode desconocido\n");
    } 
 

    if (!SetServiceStatus (MyServiceStatusHandle,  &MyServiceStatus)) 
        if ( logOk ) LOG_Escribir(logHandle, "service: MyServiceCtrlHandler,SetServiceStatus fall�\n");
   
    return; 

}


void ImprimirUso (TCHAR *nombrePrograma)
{

	fprintf(stderr, "%s [/i] | [/u] | [/r]\n", nombrePrograma);
	fprintf(stderr, "\t. /i Instala el servicio\n");
	fprintf(stderr, "\t. /u Desinstala el servicio\n");
	fprintf(stderr, "\t. /r Ejecuta el servicio\n");

}

