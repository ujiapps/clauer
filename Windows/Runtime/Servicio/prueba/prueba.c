#include <stdio.h>
#include <stdlib.h>
#include <winsock.h>
#include <LIBMSG/libmsg.h>

#include <LIBRT/LIBRT.h>


#define puerto	969




#if 0
void bateria_1 (void) {

	int result;
	SOCKET socket;
	BYTE buffer;
	BYTE* buffer_result, *buffer_result2;
	int yo;
	int i;


	/* Inicio de la comunicación */

	printf("Inicializando...");

	result = LIBMSG_Ini();
	if ( result != 0 ) {
		printf("ERROR!\n");
		exit(1);
	}
	printf("OK!\n");


	printf("Creando socket...");

	socket = LIBMSG_Conectar("localhost", 969);

	if ( socket == INVALID_SOCKET ) {
		printf("ERROR!\n");
		exit(1);
	}
	printf("OK!\n");


	/****** FUNCION 0 ******/

	printf("Envio la funcion 0...");
	buffer = 0;

	result = LIBMSG_Enviar(socket, &buffer, 1);
	if ( result != 0 ) {

		printf("ERROR!\n");
		exit(1);
	}

	printf("OK!\n");

	printf("Recibo los datos de la funcion 0...");

	buffer_result = (BYTE *) malloc (10*sizeof(BYTE));

	result = LIBMSG_Recibir(socket, buffer_result, 1);
	if ( result != 0 ) {

		printf("ERROR!\n");
		exit(1);
	}
	printf("OK!\n");

	printf("NUM DISPOSITIVOS=<%d>\n", *buffer_result);

	if ( *buffer_result > 0 ) {
		printf("Recibo mas datos de la funcion 0...");

		free(buffer_result);
		buffer_result = (BYTE *) malloc (10*sizeof(BYTE));

		result = LIBMSG_Recibir(socket, buffer_result, 1);
		if ( result != 0 ) {

			printf("ERROR!\n");
			exit(1);
		}

		printf("OK!\n");
	}



	/******* FUNCION 1 *******/

	printf("Envio la funcion 1...");
	buffer=1;

	result = LIBMSG_Enviar(socket, &buffer, 1);
	if ( result != 0 ) {
		printf("ERROR!\n");
		exit(1);
	}
	printf("OK!\n");


	printf("Envio los datos de la funcion 1...");

	buffer=1;	/* Numero de dispositivo elegido */
	result = LIBMSG_Enviar(socket, &buffer, 1);
	if ( result != 0 ) {
		printf("ERROR!\n");
		exit(1);
	}

	buffer=8;	/* Numero de caracteres de la password */
	result = LIBMSG_Enviar(socket, &buffer, 1);
	if ( result != 0 ) {
		printf("ERROR!\n");
		exit(1);
	}

	buffer_result=malloc(10*sizeof(char));
	sprintf(buffer_result, "%s", "jajajaja");

	strcpy(buffer_result, "jajajaja");
	
	result = LIBMSG_Enviar(socket, buffer_result, 8);
	if ( result != 0 ) {
		printf("ERROR!\n");
		exit(1);
	}

	printf("OK!\n");

	printf("Recibo el resultado de la funcion 1...");

	buffer_result = (BYTE *) malloc (10*sizeof(BYTE));

	result = LIBMSG_Recibir(socket, buffer_result, 1);
	if ( result != 0 ) {

		printf("ERROR!\n");
		exit(1);
	}
	printf("OK!\n");

	printf("Resultado = <%d>\n", *buffer_result);


	if (*buffer_result == 0) {

			printf("Recibo mas datos de la funcion 1...");

			free(buffer_result);
			buffer_result = (BYTE *) malloc (20*sizeof(BYTE));

			result = LIBMSG_Recibir(socket, buffer_result, 20);
			if ( result != 0 ) {

				printf("ERROR!\n");
				exit(1);
			}

			printf("OK!\n");
	}



	/******* FUNCION 1 *******/

	printf("Envio la funcion 1...");
	buffer=1;

	result = LIBMSG_Enviar(socket, &buffer, 1);
	if ( result != 0 ) {
		printf("ERROR!\n");
		exit(1);
	}
	printf("OK!\n");


	printf("Envio los datos de la funcion 1...");

	buffer=1;	/* Numero de dispositivo elegido */
	result = LIBMSG_Enviar(socket, &buffer, 1);
	if ( result != 0 ) {
		printf("ERROR!\n");
		exit(1);
	}

	buffer=8;	/* Numero de caracteres de la password */
	result = LIBMSG_Enviar(socket, &buffer, 1);
	if ( result != 0 ) {
		printf("ERROR!\n");
		exit(1);
	}

	buffer_result=malloc(10*sizeof(char));
	sprintf(buffer_result, "%s", "jajajaja");

	strcpy(buffer_result, "jajajaja");
	
	result = LIBMSG_Enviar(socket, buffer_result, 8);
	if ( result != 0 ) {
		printf("ERROR!\n");
		exit(1);
	}

	printf("OK!\n");

	printf("Recibo el resultado de la funcion 1...");

	buffer_result = (BYTE *) malloc (10*sizeof(BYTE));

	result = LIBMSG_Recibir(socket, buffer_result, 1);
	if ( result != 0 ) {

		printf("ERROR!\n");
		exit(1);
	}
	printf("OK!\n");

	printf("Resultado = <%d>\n", *buffer_result);


	if (*buffer_result == 0) {

			printf("Recibo mas datos de la funcion 1...");

			free(buffer_result);
			buffer_result = (BYTE *) malloc (20*sizeof(BYTE));

			result = LIBMSG_Recibir(socket, buffer_result, 20);
			if ( result != 0 ) {

				printf("ERROR!\n");
				exit(1);
			}

			printf("OK!\n");
	}




#if 0
	/******* FUNCION 2 *******/

	printf("Envio la funcion 2...");
	buffer=2;

	result = LIBMSG_Enviar(socket, &buffer, 1);
	if ( result != 0 ) {
		printf("ERROR!\n");
		exit(1);
	}
	printf("OK!\n");

	printf("Recibo el resultado de la funcion 2...");

	buffer_result = (BYTE *) malloc (10*sizeof(BYTE));


	yo = 0;
	result = LIBMSG_Recibir(socket, &yo, 1);
	if ( result != 0 ) {

		printf("ERROR!\n");
		exit(1);
	}
	printf("OK!\n");

	printf("Resultado = <%d>\n", yo);


	printf("Recibo mas datos de la funcion 2...");

	free(buffer_result);
	buffer_result = (BYTE *) malloc (4*sizeof(BYTE));

	result = LIBMSG_Recibir(socket, buffer_result, 4);
	if ( result != 0 ) {

		printf("ERROR!\n");
		exit(1);
	}
	printf("OK!\n");

	printf("Num de bytes zona reservada= <%d>\n", *((unsigned int *)buffer_result));

	printf("Recibo mas datos de la funcion 2...");

	buffer_result2 = (BYTE *) malloc ((*buffer_result)*sizeof(BYTE));

	result = LIBMSG_Recibir(socket, buffer_result2, *buffer_result);
	if ( result != 0 ) {

		printf("ERROR!\n");
		exit(1);
	}
	printf("OK!\n");



	free(buffer_result2);
	free(buffer_result);

#endif

#if 0
	/******* FUNCION 3 ********/


	printf("Envio la funcion 3...");
	buffer=3;

	result = LIBMSG_Enviar(socket, &buffer, 1);
	if ( result != 0 ) {
		printf("ERROR!\n");
		exit(1);
	}
	printf("OK!\n");



	printf("Envio el num de bytes a escribir...");
	yo=92160;

	result = LIBMSG_Enviar(socket, (char *)&yo, 4);
	if ( result != 0 ) {
		printf("ERROR!\n");
		exit(1);
	}
	printf("OK!\n");

	printf("Envio el buffer a escribir...");
	buffer_result = (BYTE *) malloc (92160*sizeof(char));


	strcpy(buffer_result+92152,"capullo");

	result = LIBMSG_Enviar(socket, buffer_result, 92160);
	if ( result != 0 ) {
		printf("ERROR!\n");
		exit(1);
	}
	printf("OK!\n");

	free(buffer_result);

	printf("Recibo el resultado de la funcion 3...");

	yo = 0;
	result = LIBMSG_Recibir(socket, (char *)&yo, 1);
	if ( result != 0 ) {

		printf("ERROR!\n");
		exit(1);
	}
	printf("OK!\n");

	printf("Resultado = <%d>\n", yo);

#endif

#if 0

	/******* FUNCION 4 ********/

	printf("Envio la funcion 4...");
	buffer=4;

	result = LIBMSG_Enviar(socket, &buffer, 1);
	if ( result != 0 ) {
		printf("ERROR!\n");
		exit(1);
	}
	printf("OK!\n");


	printf("Envio el num de bytes de la nueva pass...");
	yo=8;

	result = LIBMSG_Enviar(socket, (char *)&yo, 4);
	if ( result != 0 ) {
		printf("ERROR!\n");
		exit(1);
	}
	printf("OK!\n");

	printf("Envio el buffer a escribir...");
	buffer_result = (BYTE *) malloc (yo*sizeof(char));

	strcpy(buffer_result,"jejejeje");

	result = LIBMSG_Enviar(socket, buffer_result, yo);
	if ( result != 0 ) {
		printf("ERROR!\n");
		exit(1);
	}
	printf("OK!\n");

	free(buffer_result);

	printf("Recibo el resultado de la funcion 4...");

	yo = 0;
	result = LIBMSG_Recibir(socket, (char *)&yo, 1);
	if ( result != 0 ) {

		printf("ERROR!\n");
		exit(1);
	}
	printf("OK!\n");

	printf("Resultado = <%d>\n", yo);
#endif

#if 0
	/******* FUNCION 5 ********/

	printf("Envio la funcion 5...");
	buffer=5;

	result = LIBMSG_Enviar(socket, &buffer, 1);
	if ( result != 0 ) {
		printf("ERROR!\n");
		exit(1);
	}
	printf("OK!\n");

	printf("Recibo el resultado de la funcion 5...");

	yo = 0;
	result = LIBMSG_Recibir(socket, (char *)&yo, 1);
	if ( result != 0 ) {

		printf("ERROR!\n");
		exit(1);
	}
	printf("OK!\n");

	printf("Resultado = <%d>\n", yo);



	buffer_result = malloc(1024*sizeof(BYTE));

	printf("Recibo el bloque identificativo...");

	result = LIBMSG_Recibir(socket, buffer_result, 1024);
	if ( result != 0 ) {
		printf("ERROR!\n");
		exit(1);
	}

	printf("OK!\n");


	for (i=0;i<1024;i++) {

		printf("%x", buffer_result[i]);

	}

	free(buffer_result);
#endif

	/******* FUNCION 20 ********/

	printf("Envio la funcion 20...");
	buffer=20;

	result = LIBMSG_Enviar(socket, &buffer, 1);
	if ( result != 0 ) {
		printf("ERROR!\n");
		exit(1);
	}
	printf("OK!\n");

	printf("Recibo el resultado de la funcion 20...");

	yo = 0;
	result = LIBMSG_Recibir(socket, (char *)&yo, 1);
	if ( result != 0 ) {

		printf("ERROR!\n");
		exit(1);
	}
	printf("OK!\n");

	printf("Resultado = <%d>\n", yo);

	/******** Fin de la comunicación *******/

	printf("Cerrando socket...");

	result = LIBMSG_Cerrar(socket);
	if ( result != 0 ) {
		printf("ERROR!\n");
		exit(1);
	}
	printf("OK!\n");

	printf("Finalizando...");
	result = LIBMSG_Fin();
	if ( result != 0 ) {
		printf("ERROR!\n");
		exit(1);
	}
	printf("OK!\n");


	/******* FUNCION 20 ********/

	printf("Envio la funcion 20...");
	buffer=20;

	result = LIBMSG_Enviar(socket, &buffer, 1);
	if ( result != 0 ) {
		printf("ERROR!\n");
		exit(1);
	}
	printf("OK!\n");

	printf("Recibo el resultado de la funcion 20...");

	yo = 0;
	result = LIBMSG_Recibir(socket, (char *)&yo, 1);
	if ( result != 0 ) {

		printf("ERROR!\n");
		exit(1);
	}
	printf("OK!\n");

	printf("Resultado = <%d>\n", yo);

	/******** Fin de la comunicación *******/

	printf("Cerrando socket...");

	result = LIBMSG_Cerrar(socket);
	if ( result != 0 ) {
		printf("ERROR!\n");
		exit(1);
	}
	printf("OK!\n");

	printf("Finalizando...");
	result = LIBMSG_Fin();
	if ( result != 0 ) {
		printf("ERROR!\n");
		exit(1);
	}
	printf("OK!\n");

}

#endif

main ( void )
{
	USBCERTS_HANDLE hClauer;

	LIBRT_Ini();

	if ( LIBRT_IniciarDispositivo ( "C:\\CRYF_001.cla", "jajajaja", &hClauer ) != 0 ) {
		fprintf(stderr, "[ERROR] Iniciando dispositivo\n");
		return 1;
	}



}