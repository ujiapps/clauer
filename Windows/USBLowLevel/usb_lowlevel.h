/* DLL para el acceso a bajo nivel a dispositivos USB */
#ifndef USB_LOWLEVEL_H
#define USB_LOWLEVEL_H

#include <windows.h>
#include <winbase.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <tchar.h>

#include <winnetwk.h>

#ifdef __cplusplus
extern "C" {
#endif

#ifndef IN
#define IN
#endif

#ifndef OUT
#define OUT
#endif

#define MAX_DEVICES		128
#define MAX_PATH_LEN	512
#define MAX_DEVICE_LEN	512

#define WINNT351    3           /* Windows NT 3.51 */
#define WIN95       4           /* Windows 95/98/Me/NT 4.0 */
#define WINXP       5           /* Windows XP/2000/Server 2003 family */

#define ID_PART_NISU    105

#define	TAM_BLOQUE	10240

/* Soporte para Windows 98 deshabilitado */
#define SOPORTE_WIN98	0

#define VXDNAME		"\\\\.\\CLAUER98.VXD"

#define	VXD_LEER		1
#define	VXD_ESCRIBIR	2

/*
 * Tipos de datos
 */

typedef struct _PARTICION {
        long int cyl_ini;
        long int head_ini;
        long int sect_ini;

        long int cyl_fin;
        long int head_fin;
        long int sect_fin;

        long int id;

        long int rel_sectors;
        long int num_sectors;
} PARTICION;

typedef HANDLE HANDLE_DISPOSITIVO;
typedef DWORD HANDLE_OBJETO;

typedef struct _UBICACION {

	unsigned int unidad;		/* n�mero a sumar a la unidad 80h (disco duro) */
	unsigned int cabeza;		/* n�mero de cabeza */
	unsigned int pista;			/* n�mero de pista a leer (cilindro) */
	unsigned int sector;		/* n�mero de sector dentro de esa pista a leer */

} UBICACION;


/*---------------------------- FUNCIONES EXPORTADAS ----------------------------*/

/***** FUNCIONES DE BAJO NIVEL *****/
int lowlevel_obtenerBytesSector(int dispositivo);
int lowlevel_is_readable ( IN char* unidad, IN int dispositivo );

int lowlevel_enumerar_dispositivos    ( OUT char **unidades, OUT int *dispositivos, OUT int *numDispositivos );
int lowlevel_enumerar_dispositivos_ex ( OUT char *dispositivos[MAX_DEVICES], OUT int *numDispositivos);

int lowlevel_abrir_dispositivo_ex( IN char * device, OUT HANDLE_DISPOSITIVO* handle );
int lowlevel_abrir_dispositivo( IN int ndevice, OUT HANDLE_DISPOSITIVO* handle );
int lowlevel_cerrar_dispositivo( IN HANDLE_DISPOSITIVO handle );

int lowlevel_leer_particiones( IN HANDLE_DISPOSITIVO handle, OUT PARTICION particiones[], IN int dispositivo );

int lowlevel_saltar( HANDLE_DISPOSITIVO handle, unsigned long nsectores, BOOL es_relativo, int dispositivo );

int lowlevel_leer_sector( IN HANDLE_DISPOSITIVO handle, IN unsigned long num_sector, OUT BYTE* sector, IN int dispositivo );
int lowlevel_escribir_sector( IN HANDLE_DISPOSITIVO handle, IN unsigned long num_sector, IN BYTE* sector, IN int dispositivo );
int lowlevel_leer_buffer( IN HANDLE_DISPOSITIVO handle, OUT BYTE* buffer, IN unsigned long sector_ini, IN unsigned long nsectores, IN int dispositivo );
int lowlevel_escribir_buffer( IN HANDLE_DISPOSITIVO handle, IN BYTE* buffer, IN unsigned long sector_ini, IN unsigned long nsectores, IN int dispositivo );

int lowlevel_bloquear_unidadlogica( IN char unidad, OUT HANDLE_DISPOSITIVO *handle);
int lowlevel_desbloquear_unidadlogica( IN HANDLE_DISPOSITIVO handle );

int lowlevel_es_usbcert( IN HANDLE_DISPOSITIVO handle, OUT int *es );
int lowlevel_tiene_mbr( IN HANDLE_DISPOSITIVO handle, OUT int *tiene, IN int dispositivo);
int lowlevel_leer_salt( IN HANDLE_DISPOSITIVO handle, IN PARTICION particion, OUT BYTE salt [20], IN int dispositivo );

int lowlevel_get_winversion( void );

int lowlevel_error( IN int codigo_error, OUT char* descripcion );

int lowlevel_soy_admin( IN int ndevice, OUT int* soy );

int lowlevel_calcular_ubicacion( IN unsigned long num_sector, IN long int total_cabezas, IN long int total_cilindros, IN long int total_sectores, 
								 OUT unsigned int *n_cabeza, OUT unsigned int *n_pista, OUT unsigned int *n_sector );


/*
 *  C�digos de error
 */

#define ERR_LOWLEVEL_NO                          0
#define ERR_LOWLEVEL_SI                          1
#define ERR_LOWLEVEL_DISPOSITIVO_INCORRECTO      2
#define ERR_LOWLEVEL_SIST_OPERATIVO_INCORRECTO   3
#define ERR_LOWLEVEL_LECTURA_MBR_INCORRECTA      4
#define ERR_LOWLEVEL_MBR_NO_VALIDO               5
#define ERR_LOWLEVEL_UNIDAD_LOGICA_INCORRECTA	 6
#define ERR_LOWLEVEL_LECTURA_INCORRECTA          7
#define ERR_LOWLEVEL_ESCRITURA_INCORRECTA        8
#define ERR_LOWLEVEL_BLOQUEANDO_UNIDAD_LOGICA    9
#define ERR_LOWLEVEL_DESBLOQUEANDO_UNIDAD_LOGICA 10
#define ERR_LOWLEVEL_FIN_PARTICION               11
#define ERR_LOWLEVEL_PARTICION_INCORRECTA        12
#define ERR_LOWLEVEL_PARTICION_INEXISTENTE       13
#define ERR_LOWLEVEL_NSECTORES_INCORRECTO		 14
#define ERR_LOWLEVEL_TOO_MUCH_DEVICES			 15

#ifdef __cplusplus
}
#endif

#endif

