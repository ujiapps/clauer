#include <iostream>
#include <memory>

#include "../BaseDatos.hpp"
#include "../EBaseDatos.hpp"


void TestTieneStick (void)
{
	CBaseDatos bd;
	string email;

	if ( bd.TieneStick("53224086Q", email) ) 
		cout << "53224086Q: " << email << ": Ya tiene stick" << endl;
	else
		cout << "53224086Q: " << email << ": No tiene stick" << endl;

	if ( bd.TieneStick("52945813C", email) ) 
		cout << "52945813C: " << email << ": Ya tiene stick" << endl;
	else
		cout << "52945813C: " << email << ": No tiene stick" << endl;
}



void main ()
{
	CBaseDatos bd;
	BYTE id[20];

	try {
		bd.RegistrarStick(string("20249511N"), id);
	}
	catch (EBaseDatos &e)
	{
		cout << e << endl;
	}

}
