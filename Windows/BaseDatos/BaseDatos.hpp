/* Gestiona el acceso a la base de datos con información sobre la posesión de clauers
 * El acceso a la base de datos se realiza por HTTP (POST)
 */

#ifndef __BASEDATOS_HPP__
#define __BASEDATOS_HPP__

#include <windows.h>
#include <winhttp.h>

#include <iostream>
#include <string>

#include "EBaseDatos.hpp"

using namespace std;


class CBaseDatos {

protected:
	HINTERNET hSession;
	HINTERNET hConnect;

	struct info_tienestick {
		bool tiene;
		bool error;
		string email;
		string colectiu;
	};

	void ParseTieneStick (unsigned char *buffer, unsigned long tamBuffer, struct info_tienestick &info);
	void ParseRegistrarStick (unsigned char *buffer, unsigned long tamBuffer);

public:
	CBaseDatos();
	~CBaseDatos();

	bool TieneStick (string dni, string &email, string &colectiu);
	void RegistrarStick (string dni, BYTE numeroSerie[20]);

	/* Función que utilizamos para seguir el progreso del windows http services */

	friend void	WINAPI HTTP_Progreso ( HINTERNET hInternet, 
					 				   DWORD_PTR dwContext, 
								       DWORD dwInternetStatus, 
								       LPVOID lpvStatusInformation, 
								       DWORD dwStatusInformationLength );
};


#endif
