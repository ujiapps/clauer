#include "BaseDatos.hpp"
#include "EBaseDatos.hpp"

#include <sstream>
#include <time.h>

#define USER			"class"
#define PWD				"public"
#define MAXRESPUESTA	102400		// 100K


void WINAPI HTTP_Progreso ( HINTERNET hInternet, 
				     DWORD_PTR dwContext, 
					 DWORD dwInternetStatus, 
					 LPVOID lpvStatusInformation, 
					 DWORD dwStatusInformationLength )
{
	switch ( dwInternetStatus ) {

	case WINHTTP_CALLBACK_STATUS_CLOSING_CONNECTION:
	case WINHTTP_CALLBACK_STATUS_CONNECTED_TO_SERVER:
	case WINHTTP_CALLBACK_STATUS_CONNECTING_TO_SERVER:
	case WINHTTP_CALLBACK_STATUS_CONNECTION_CLOSED:
	case WINHTTP_CALLBACK_STATUS_DATA_AVAILABLE:
	case WINHTTP_CALLBACK_STATUS_HANDLE_CREATED:
	case WINHTTP_CALLBACK_STATUS_HANDLE_CLOSING:
	case WINHTTP_CALLBACK_STATUS_HEADERS_AVAILABLE:
	case WINHTTP_CALLBACK_STATUS_INTERMEDIATE_RESPONSE:
	case WINHTTP_CALLBACK_STATUS_NAME_RESOLVED:
	case WINHTTP_CALLBACK_STATUS_READ_COMPLETE:
	case WINHTTP_CALLBACK_STATUS_RECEIVING_RESPONSE:
	case WINHTTP_CALLBACK_STATUS_REDIRECT:
	case WINHTTP_CALLBACK_STATUS_REQUEST_ERROR:
	case WINHTTP_CALLBACK_STATUS_REQUEST_SENT:
	case WINHTTP_CALLBACK_STATUS_RESOLVING_NAME:
	case WINHTTP_CALLBACK_STATUS_RESPONSE_RECEIVED:
	case WINHTTP_CALLBACK_STATUS_SENDING_REQUEST:
	case WINHTTP_CALLBACK_STATUS_SENDREQUEST_COMPLETE:
	case WINHTTP_CALLBACK_STATUS_WRITE_COMPLETE:
		break;

		// Errores en el HTTPS
	case WINHTTP_CALLBACK_STATUS_SECURE_FAILURE:
		
/*		DWORD flagError = *((DWORD *)lpvStatusInformation);


		if ( flagError & WINHTTP_CALLBACK_STATUS_FLAG_CERT_REV_FAILED ) 
			cerr << "Imposible determinar la revocaci�n del certificado" << endl;			

		if ( flagError & WINHTTP_CALLBACK_STATUS_FLAG_INVALID_CERT ) {
			cerr << "El certificado es inv�lido" << endl;
		}
		
		if ( flagError & WINHTTP_CALLBACK_STATUS_FLAG_CERT_REVOKED )
			cerr << "El certificado ha sido revocado" << endl;

		if ( flagError & WINHTTP_CALLBACK_STATUS_FLAG_INVALID_CA ) 
			cerr << "CA inv�lida" << endl;

		if ( flagError & WINHTTP_CALLBACK_STATUS_FLAG_CERT_CN_INVALID )
			cerr << "El CN es inv�lido" << endl;

		if ( flagError & WINHTTP_CALLBACK_STATUS_FLAG_CERT_DATE_INVALID )
			cerr << "El certificado ha expirado" << endl;

		if ( flagError & WINHTTP_CALLBACK_STATUS_FLAG_SECURITY_CHANNEL_ERROR )
			cerr << "Error interno del SSL" << endl;

		cerr << flush;
*/
		break;


	}
}



CBaseDatos::CBaseDatos()
{
	stringstream errMsg;
	int err = 0;

	hConnect = NULL;
	hSession = NULL;

	/* Creamos el objeto sesi�n
	 */

	hSession = WinHttpOpen(L"CLIN v0.1", WINHTTP_ACCESS_TYPE_NO_PROXY, WINHTTP_NO_PROXY_NAME, WINHTTP_NO_PROXY_BYPASS, 0);

	if ( !hSession ) {
		errMsg << "Error en CBaseDatos(). Funci�n WinHttpOpen(). C�digo: " << GetLastError();
		err = 1;
		goto finCBaseDatos;
	}

	/* Establecemos los diversos timeouts:
	 *
	 *      - Timeout para resolver nombres: 5s
	 *      - Timeout conexi�n al servidor: 5s
	 *		- Timeout para el env�o de la petici�n: 5s
	 *      - Timeout para la respuesta: 10s
	 */

	if ( !WinHttpSetTimeouts(hSession, 5000, 5000, 5000, 10000) ) {
		errMsg << "Error en CBaseDatos(). Funci�n WinHttpOpen(). C�digo: " << GetLastError();
		err = 1;
		goto finCBaseDatos;
	}


	/* Creamos el objeto connection: HTTPS
	 */

	hConnect = WinHttpConnect(hSession, L"www.uji.es", INTERNET_DEFAULT_HTTPS_PORT, 0);

	if ( !hConnect ) {
		errMsg << "Error en CBaseDatos(). Funci�n WinHttpConnect(). C�digo: " << GetLastError();
		err = 1;
		goto finCBaseDatos;
	}



finCBaseDatos:

	if ( err ) {

		if ( hSession )
			WinHttpCloseHandle(hSession);
		if ( hConnect )
			WinHttpCloseHandle(hConnect);

		throw EBaseDatos(errMsg.str());
	}

}



CBaseDatos::~CBaseDatos()
{
	if ( hConnect )
		WinHttpCloseHandle(hConnect);

	if ( hSession )
		WinHttpCloseHandle(hSession);

}


/*! \brief Consulta sobre la base de datos si el usuario con DNI dni se le ha asignado ya o no un clauer.
 *
 * Consulta sobre la base de datos si el usuario con DNI dni se le ha asignado ya o no un clauer. La 
 * funci�n devuelve el email oficial del usuario.
 *
 * La consulta se realiza a trav�s de una consulta por POST (HTTP). El formato del mensaje es el siguiente:
 *
 *
 * \param dni
 *		  ENTRADA: El DNI del individuo para el que se va a realizar la consulta.
 *
 * \param email
 *		  SALIDA: El email oficial del individuo.
 *
 * \retval false
 *	 	   El individuo no tiene asignado ning�n clauer.
 *
 * \retval true
 *		   Al individuo ya se le ha asignado un clauer.
 *
 * \remarks La funci�n asume un tama�o m�ximo de respuesta de 100K. Si se modificara todo esto
 *			habr�a que probarlo.
 */

bool CBaseDatos::TieneStick (string dni, string &email, string &colectiu)
{
	stringstream errMsg, response;
	int err = 0;
	DWORD tam, totalTam = 0;
	BYTE *buffer = NULL;
	string peticion;
	unsigned long int secureFlags;
	struct info_tienestick info;
	HINTERNET hRequest;

	/* Vamos a construir la petici�n */

	peticion = string("p_dni=") + dni + string("&p_base=class&p_valor=public");

	hRequest = WinHttpOpenRequest(hConnect, L"POST", L"/pls/www/!gri_www.euji20901", L"HTTP/1.1", WINHTTP_NO_REFERER, WINHTTP_DEFAULT_ACCEPT_TYPES, WINHTTP_FLAG_SECURE);

	if ( !hRequest ) {
		errMsg << "Error en CBaseDatos::TieneStick(). Funci�n WinHttpSetStatusCallback(). C�digo: " << GetLastError();
		err = 1;
		goto finTieneStick;
	}

	secureFlags = 0;
	secureFlags = SECURITY_FLAG_IGNORE_CERT_CN_INVALID |
				  SECURITY_FLAG_IGNORE_UNKNOWN_CA |
				  SECURITY_FLAG_IGNORE_CERT_DATE_INVALID;

	if ( !WinHttpSetOption(hRequest, WINHTTP_OPTION_SECURITY_FLAGS, &secureFlags, sizeof(secureFlags)) ) {
		errMsg << "Error en CBaseDatos::TieneStick(). Funci�n WinHttpSetOption(). C�digo: " << GetLastError();
		err = 1;
		goto finTieneStick;
	}

	if ( WinHttpSetStatusCallback(hRequest, HTTP_Progreso, WINHTTP_CALLBACK_FLAG_ALL_NOTIFICATIONS, NULL) == WINHTTP_INVALID_STATUS_CALLBACK ) {
		errMsg << "Error en CBaseDatos::TieneStick(). Funci�n WinHttpSetStatusCallback(). C�digo: " << GetLastError();
		err = 1;
		goto finTieneStick;
	}

	if ( !WinHttpSendRequest( hRequest, WINHTTP_NO_ADDITIONAL_HEADERS, 0, WINHTTP_NO_REQUEST_DATA, 0, peticion.length(), 0) ) {
		errMsg << "Error en CBaseDatos::TieneStick(). Funci�n WinHttpSendRequest(). C�digo: " << GetLastError();
		err = 1;
		goto finTieneStick;
	}

	if ( !WinHttpWriteData(hRequest, (void *) peticion.c_str(), peticion.length(), &tam) ) {
		errMsg << "Error en CBaseDatos::TieneStick(). Funci�n WinHttpWriteData(). C�digo: " << GetLastError();
		err = 1;
		goto finTieneStick;	
	}
	
	if ( !WinHttpReceiveResponse( hRequest, NULL) ) {
		errMsg << "Error en CBaseDatos::TieneStick(). Funci�n WinHttpReceiveResponse(). C�digo: " << GetLastError();
		err = 1;
		goto finTieneStick;
	}

	buffer = new BYTE[MAXRESPUESTA];

	if ( !buffer ) {
		errMsg << "Error en CBaseDatos::TieneStick(). No se pudo reservar memoria. C�digo: " << GetLastError();
		err = 1;
		goto finTieneStick;
	}

	SecureZeroMemory(buffer, 10240);

	do {

		if ( !WinHttpReadData(hRequest, buffer, 10240, &tam) ) {
			errMsg << "Error en CBaseDatos::TieneStick(). Funci�n WinHttpReadData(). C�digo: " << GetLastError();
			err = 1;
			goto finTieneStick;
		}
		totalTam += tam;

	} while (tam > 0);


	ParseTieneStick(buffer, totalTam, info);
	email = info.email;
	colectiu = info.colectiu;
	

finTieneStick:

	if ( hRequest ) {
		WinHttpCloseHandle(hRequest);
		hRequest = NULL;
	}

	if ( buffer ) {
		SecureZeroMemory(buffer, 10240);
		delete [] buffer;
		buffer = NULL;
	}

	if ( err ) 
		throw EBaseDatos(errMsg.str());	

	return info.tiene;

}

/*! \brief Parsea la respuesta devuelta por la consulta TieneStick().
 *
 * Parsea la respuesta devuelta por la consulta TieneStick(). El formato del mensaje es el siguiente:
 *
 *    <RESPUESTA> -> (S|N)#<EMAIL> | N#<ERROR>
 *	  <ERROR> -> Par�metros o acceso incorrecto | DNI incorrecto, no tiene asociada ninguna cuenta
 *
 * <EMAIL> es la expresi�n regular de una direcci�n de correo electr�nico.
 *
 * \param buffer
 *		  ENTRADA. Buffer que contiene la respuesta de la consulta.
 *
 * \param tamBuffer
 *		  ENTRADA. Tama�o �til de buffer. O lo que es lo mismo, el tama�o de la respuesta.
 *
 * \param info
 *		  SALIDA. Informaci�n parseada del stick.
 *
 * \retval 0
 *		   OK
 *
 * \retval 1
 *		   ERROR. Error en la funci�n, no en la respuesta devuelta.
 *
 * \remarks Se lanza una excepci�n cuando ocurri� un error inesperado. Eso puede ser:
 *
 *				- El DNI que se pasa es incorrecto
 *				- El formato de la respuesta recibida no se reconoce
 *				- No se pudo reservar memoria
 */

void CBaseDatos::ParseTieneStick (unsigned char *buffer, unsigned long tamBuffer, struct info_tienestick &info)
{
	unsigned char * aux = NULL;
	stringstream errMsg;
	int err = 0;
	string cadParser;
	int contParser;

	aux = new unsigned char[tamBuffer+1];
	if ( !aux ) {
		errMsg << "Error en CBaseDatos::ParseTieneStick(). No se pudo reservar memoria";
		err = 1;
		goto finParseTieneStick;
	}
	memcpy(aux, buffer, tamBuffer);
	aux[tamBuffer] = '\0';

	if ( tamBuffer < 3 ) {
		errMsg << "Error en CBaseDatos::ParseTieneStick(). Formato de respuesta incorrecto:" << endl
			   << aux;
		err = 1;
		goto finParseTieneStick;
	}

	if ( *aux == 'S' ) 
		info.tiene = true;
	else if ( *aux == 'N' )
		info.tiene = false;
	else if ( *aux == 'E' ) {

		if ( strncmp((const char *)aux+2, "A este DNI no le corresponde clauer", strlen("A este DNI no le corresponde clauer")) == 0 ) {
			delete aux;
			errMsg << "A este usuario no le corresponde clauer";	
			throw EBaseDatos_SinCuenta(errMsg.str());
		}

		errMsg << "Ocurri� un error en la consulta a la base de datos:" << endl
			   << aux+2;
		err = 1;
		goto finParseTieneStick;
	} else {
		errMsg << "El formato del mensaje de respuesta es inv�lido:" << endl
			   << aux;
		err = 1;
		goto finParseTieneStick;
	}

	cadParser=string((const char *)aux+2);

	contParser=cadParser.find("#",0);

	info.email = cadParser.substr(0,contParser);

	info.colectiu = cadParser.substr(contParser+1);

	//info.email = string((const char *)aux+2);
	//info.colectiu = string((const char *)aux+3);

finParseTieneStick:

	if ( aux == NULL )
		delete [] aux;

	if ( err )
		throw EBaseDatos(errMsg.str());

}




/*! \brief Realiza el registro de un stick para un dni y n�mero de serie en concreto.
 *
 * Realiza el registro de un stick para un dni y n�mero de serie en concreto.
 *
 * La consulta se realiza a trav�s de una consulta por POST (HTTP). El formato del mensaje es el siguiente:
 *
 * <PETICION> -> p_dni=<DNI>&p_base=<USER>&p_valor=<PASSWORD>&p_id=<ID>
 *
 * Donde <DNI> es un DNI, p_base es el usuario, p_valor es la password (ambos a pi�o fijo) y p_id que puede
 * tomar cualquier valor.
 *
 * \param dni
 *		  ENTRADA: El DNI del individuo para el que se va a realizar la consulta.
 *
 * \param numerSerie
 *		  ENTRADA: El n�mero de serie del stick que queremos registrar.
 *
 * \remarks La funci�n asume un tama�o m�ximo de respuesta de 100K. Si se modificara todo esto
 *			habr�a que probarlo.
 */

void CBaseDatos::RegistrarStick (string dni, BYTE numeroSerie[20])
{

	stringstream errMsg, response;
	int err = 0;
	DWORD tam, totalTam = 0;
	BYTE *buffer = NULL;
	string peticion;
	unsigned long int secureFlags;
	struct info_tienestick info;
	HINTERNET hRequest;
	char id[41];

	/* Construyo el id */

	memset(id,0,41);
	for ( int i = 0 ; i < 20 ; i++ ) 
		sprintf(id+i*2, "%02x",numeroSerie[i]);

	peticion = string("p_dni=") + dni + string("&p_base=class&p_valor=public&p_id=") + string(id);

	hRequest = WinHttpOpenRequest(hConnect, L"POST", L"/pls/www/!gri_www.euji20901", L"HTTP/1.1", WINHTTP_NO_REFERER, WINHTTP_DEFAULT_ACCEPT_TYPES, WINHTTP_FLAG_SECURE);

	if ( !hRequest ) {
		errMsg << "Error en CBaseDatos::RegistrarStick(). Funci�n WinHttpSetStatusCallback(). C�digo: " << GetLastError();
		err = 1;
		goto finRegistrarStick;
	}

	secureFlags = 0;
	secureFlags = SECURITY_FLAG_IGNORE_CERT_CN_INVALID |
				  SECURITY_FLAG_IGNORE_UNKNOWN_CA |
				  SECURITY_FLAG_IGNORE_CERT_DATE_INVALID;

	if ( !WinHttpSetOption(hRequest, WINHTTP_OPTION_SECURITY_FLAGS, &secureFlags, sizeof(secureFlags)) ) {
		errMsg << "Error en CBaseDatos::RegistrarStick(). Funci�n WinHttpSetOption(). C�digo: " << GetLastError();
		err = 1;
		goto finRegistrarStick;
	}

	if ( WinHttpSetStatusCallback(hRequest, HTTP_Progreso, WINHTTP_CALLBACK_FLAG_ALL_NOTIFICATIONS, NULL) == WINHTTP_INVALID_STATUS_CALLBACK ) {
		errMsg << "Error en CBaseDatos::RegistrarStick(). Funci�n WinHttpSetStatusCallback(). C�digo: " << GetLastError();
		err = 1;
		goto finRegistrarStick;
	}

	if ( !WinHttpSendRequest( hRequest, WINHTTP_NO_ADDITIONAL_HEADERS, 0, WINHTTP_NO_REQUEST_DATA, 0, peticion.length(), 0) ) {
		errMsg << "Error en CBaseDatos::RegistrarStick(). Funci�n WinHttpSendRequest(). C�digo: " << GetLastError();
		err = 1;
		goto finRegistrarStick;
	}

	if ( !WinHttpWriteData(hRequest, (void *) peticion.c_str(), peticion.length(), &tam) ) {
		errMsg << "Error en CBaseDatos::RegistrarStick(). Funci�n WinHttpWriteData(). C�digo: " << GetLastError();
		err = 1;
		goto finRegistrarStick;	
	}
	
	if ( !WinHttpReceiveResponse( hRequest, NULL) ) {
		errMsg << "Error en CBaseDatos::RegistrarStick(). Funci�n WinHttpReceiveResponse(). C�digo: " << GetLastError();
		err = 1;
		goto finRegistrarStick;
	}

	buffer = new BYTE[MAXRESPUESTA];

	if ( !buffer ) {
		errMsg << "Error en CBaseDatos::RegistrarStick(). No se pudo reservar memoria. C�digo: " << GetLastError();
		err = 1;
		goto finRegistrarStick;
	}

	SecureZeroMemory(buffer, 10240);

	do {

		if ( !WinHttpReadData(hRequest, buffer, 10240, &tam) ) {
			errMsg << "Error en CBaseDatos::RegistrarStick(). Funci�n WinHttpReadData(). C�digo: " << GetLastError();
			err = 1;
			goto finRegistrarStick;
		}
		totalTam += tam;

	} while (tam > 0);

	try {
		ParseRegistrarStick(buffer, totalTam);
	}
	catch (...) {
		if ( hRequest ) {
			WinHttpCloseHandle(hRequest);
			hRequest = NULL;
		}

		if ( buffer ) {
			SecureZeroMemory(buffer, 10240);
			delete [] buffer;
			buffer = NULL;
		}

		throw;
	}

finRegistrarStick:

	if ( hRequest ) {
		WinHttpCloseHandle(hRequest);
		hRequest = NULL;
	}

	if ( buffer ) {
		SecureZeroMemory(buffer, 10240);
		delete [] buffer;
		buffer = NULL;
	}

	if ( err ) 
		throw EBaseDatos(errMsg.str());	

}






/*! \brief Parsea la respuesta devuelta por la consulta TieneStick().
 *
 * Parsea la respuesta devuelta por la llamada RegistarStick(). El formato del mensaje es el siguiente:
 *
 *    <RESPUESTA> -> (S|N)#<EMAIL> | N#<ERROR>
 *	  <ERROR> -> Par�metros o acceso incorrecto | DNI incorrecto, no tiene asociada ninguna cuenta
 *
 * <EMAIL> es la expresi�n regular de una direcci�n de correo electr�nico.
 *
 * \param buffer
 *		  ENTRADA. Buffer que contiene la respuesta de la consulta.
 *
 * \param tamBuffer
 *		  ENTRADA. Tama�o �til de buffer. O lo que es lo mismo, el tama�o de la respuesta.
 *
 * \retval 0
 *		   OK
 *
 * \retval 1
 *		   ERROR. Error en la funci�n, no en la respuesta devuelta.
 *
 * \remarks Se lanza una excepci�n cuando ocurri� un error inesperado. Eso puede ser:
 *
 *				- El DNI que se pasa es incorrecto
 *				- El formato de la respuesta recibida no se reconoce
 *				- No se pudo reservar memoria
 */

void CBaseDatos::ParseRegistrarStick (unsigned char *buffer, unsigned long tamBuffer)
{
	unsigned char *aux = NULL;
	stringstream errMsg;
	int err = 0;

	aux = new unsigned char[tamBuffer+1];
	if ( !aux ) {
		errMsg << "Error en CBaseDatos::ParseRegistrarStick. No se pudo reservar memoria";
		err = 1;
		goto finParseRegistrarStick;
	}
	memcpy(aux, buffer, tamBuffer);
	aux[tamBuffer] = '\0';

	if ( tamBuffer < 3 ) {
		errMsg << "Error en CBaseDatos::ParseRegistrarStick. Formato de respuesta incorrecto:" << endl
			   << aux;
		err = 1;
		goto finParseRegistrarStick;
	}

	if ( *aux == 'N' ) {
		errMsg << "Error en CBaseDatos::ParseRegistrarStick. Ocurri� un error realizando registro:" << endl
			   << aux;
		err = 1;
		goto finParseRegistrarStick;

	} else if ( *aux == 'E' ) {
		errMsg << "Ocurri� un error en la consulta a la base de datos:" << endl
			   << aux+2;
		err = 1;
		goto finParseRegistrarStick;

	} else if ( *aux != 'S' ) {
		errMsg << "El formato del mensaje de respuesta es inv�lido:" << endl
			   << aux;
		err = 1;
		goto finParseRegistrarStick;
	}
	

finParseRegistrarStick:

	if ( aux == NULL )
		delete [] aux;

	if ( err )
		throw EBaseDatos(errMsg.str());

}


