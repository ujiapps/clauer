#ifndef __EBASEDATOS_HPP__
#define __EBASEDATOS_HPP__

#include <iostream>
#include <string>

using namespace std;

class EBaseDatos {
protected:
	string errMsg;

public:
	EBaseDatos() {};
	EBaseDatos(string errMsg) { this->errMsg = errMsg; };

	friend ostream &operator<< (ostream &os, EBaseDatos &bd);

};




/*! \brief Excepci�n que se lanza cuando el DNI proporcionado no tiene cuenta asociada y, por lo tanto, no le
 *		   corresponde DNI.
 *
 * Excepci�n que se lanza cuando el DNI proporcionado no tiene cuenta asociada y, por lo tanto, no le
 * corresponde DNI.
 */

class EBaseDatos_SinCuenta : public EBaseDatos
{
public:
	EBaseDatos_SinCuenta (void) {};
	EBaseDatos_SinCuenta (string errMsg) { this->errMsg = errMsg; };
};



#endif
