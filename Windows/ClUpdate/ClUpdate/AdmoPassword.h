#pragma once
#include "afxwin.h"


// CAdmoPassword dialog

class CAdmoPassword : public CDialog
{
	DECLARE_DYNAMIC(CAdmoPassword)

public:
	CAdmoPassword(CWnd* pParent = NULL);   // standard constructor
	virtual ~CAdmoPassword();

// Dialog Data
	enum { IDD = IDD_DIALOG_PASSWORD };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnEnChangeEdit1();
protected:
	CString m_editUser;
	CString m_editPassword;
public:
	CString GetUser(void);
public:
	CString GetPassword(void);
public:
	afx_msg void OnBnClickedOk();
};
