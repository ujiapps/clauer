#pragma once


// CDlgDescargar dialog

class CDlgDescargar : public CDialog
{
	DECLARE_DYNAMIC(CDlgDescargar)

public:
	CDlgDescargar(CWnd* pParent = NULL);   // standard constructor
	virtual ~CDlgDescargar();

// Dialog Data
	enum { IDD = IDD_DLG_DESCARGAR };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL PreTranslateMessage(MSG* pMsg );
	virtual BOOL OnInitDialog();

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags);
public:
	afx_msg void OnStnClickedStaticDescProgreso();
};
