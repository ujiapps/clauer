#pragma once
#include "afxwin.h"


// CDlgRecordar dialog

class CDlgRecordar : public CDialog
{
	DECLARE_DYNAMIC(CDlgRecordar)

public:
	CDlgRecordar(CWnd* pParent = NULL);   // standard constructor
	virtual ~CDlgRecordar();

// Dialog Data
	enum { IDD = IDD_DLG_RECORDAR };

protected:
	virtual BOOL OnInitDialog();
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL PreTranslateMessage(MSG* pMsg );

	DECLARE_MESSAGE_MAP()
public:
	// Combo para la elección del intervalo de tiempo para la actualización
	CComboBox m_cmbTiempo;
public:
	DWORD GetPeriod(void);
public:
	afx_msg void OnSetFocus(CWnd* pOldWnd);
public:
	afx_msg void OnStnClickedStaticSetPeriod();
};
