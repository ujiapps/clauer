#ifndef __CLUPDATE_APP_MESSAGES_H__
#define __CLUPDATE_APP_MESSAGES_H__

#include "stdafx.h"
#include <windows.h>

/* Mensaje enviado por la task bar
 */

#define WM_CLUPDATE				WM_APP	   

/* Disponible actualización.
 *    wParam:
 *          =0  Actualización ordinaria
 *          =1  Actualización crítica
 */

#define WM_CLUPDATE_UPDATE		WM_APP+1	

/* Descarga finalizada
 *    wParam
 *         =0 Error en descarga
 *         =1 Descarga ok
 */

#define WM_CLUPDATE_DOWNLOAD	WM_APP+2


#endif
