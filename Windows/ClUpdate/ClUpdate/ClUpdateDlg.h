// ClUpdateDlg.h: archivo de encabezado
//

#pragma once

#include "Notify.h"
#include <wincred.h>
#include "afxwin.h"

#include "DlgDescargar.h"
#include "DlgInstalar.h"
#include "DlgRecordar.h"


typedef enum {
	ST_INI,				 // Estado inicial
	ST_UPDATE,		     // Update disponible iniciar descarga
	ST_UPDATEREMIND,     // Update disponible pero usuario elige cu�ndo recordar
	ST_DOWNLOADING,		 // Descargando actualizaci�n
	ST_DOWNLOADED,		 // Descarga realizada
	ST_INSTALLING,		 // Instalando actualizaci�n
	ST_REINSTALL_DOWNLOADING,  // Descargando �ltima versi�n de la web
	ST_REINSTALL_DOWNLOADED,   // Descargada �ltima versi�n de la web
	ST_REINSTALL_INSTALLING,   // Instalando �ltima versi�n de la web
} CLUPDATE_STATE;


#ifdef CLUPDATE_PIPELINE
#define DEFAULT_TIMER_NOTIFIER_ELAP		86400000	// 24h
#else
#define DEFAULT_TIMER_NOTIFIER_ELAP		1800000     // 30 min
#endif

// Cuadro de di�logo de CClUpdateDlg
class CClUpdateDlg : public CDialog
{
// Construcci�n
public:
	CClUpdateDlg(CWnd* pParent = NULL);	// Constructor est�ndar

// Datos del cuadro de di�logo
	enum { IDD = IDD_CLUPDATE_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// Compatibilidad con DDX/DDV


// Implementaci�n
protected:
	HICON m_hIcon;
	// The taskbar notifier
	CNotify *m_notify;
	// El men� contextual del bot�n derecho
	CMenu *m_popMenu;
	// El men� que contiene el popupmenu
	CMenu m_menu;
	CEvent *m_eventThrNotifier;
	CString m_setupFile;

	// Para indicar cu�ndo lanzar el thread notificador
	DWORD m_notifierPeriod;

	// El estado actual
	CLUPDATE_STATE m_st;

	// Cu�ndo volvemos a notificar
	time_t m_tNextNotify;

	// Di�logos
	friend class CDlgDescargar;
	friend class CDlgInstalar;
	friend class CDlgRecordar;

	CDlgDescargar *m_dlgDescargar;
	CDlgInstalar *m_dlgInstalar;
	CDlgRecordar *m_dlgRecordar;

	DWORD m_dwTimerNotifierElap;	   // Tiempo que se planifica el timer del notificador

	BOOL m_showingAbout;		       // (BORRAR) Indica si se muestra el about box
	BOOL m_bFlash;					   // (BORRAR) Para hacer flash de una ventana
	BOOL m_bCriticalUpdate;            // (BORRAR) Indica si hay una actualizaci�n cr�tica 
	BOOL m_bExplicitUpdateAvailable;   // Indica si se ha pedido una comprobaci�n expl�cita de actualizaciones

	BOOL m_bFirstExit;				   // Indica que se pas� el argumento /ini

	// Funciones de asignaci�n de mensajes generadas
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	afx_msg LRESULT OnClupdate         ( WPARAM wParam, LPARAM lParam );
	afx_msg LRESULT OnUpdateAvailable  ( WPARAM wParam, LPARAM lParam );
	afx_msg LRESULT OnDownloadFinished ( WPARAM wParam, LPARAM lParam );
	DECLARE_MESSAGE_MAP()

	TCHAR m_szAdminUser[CREDUI_MAX_USERNAME_LENGTH + 1];
	TCHAR m_szAdminPassword[CREDUI_MAX_PASSWORD_LENGTH + 1];
	HANDLE m_hAdmoToken;

	virtual BOOL PreTranslateMessage(MSG* pMsg );


public:
	afx_msg void OnOpcionesComprobaractualizaciones();
public:
	afx_msg void OnContextMenu(CWnd* /*pWnd*/, CPoint /*point*/);
public:
	afx_msg void OnOpcionesAcercade();
public:
	afx_msg void OnOpcionesTest();
public:
	afx_msg void OnIraProyectoclauer();
public:
	afx_msg void OnIraDesarrolladoresdelproyectoclauer();
public:
	afx_msg void OnIraUniversitatjaumei();
public:
	afx_msg void OnIraNisusecurity();
	BOOL m_bUpdateNotified;
public:
	CString m_editDescription;
public:
	afx_msg void OnTimer(UINT_PTR nIDEvent);
public:
	afx_msg void OnSetFocus(CWnd* pOldWnd);
public:
	afx_msg void OnBnClickedButtonIniciar();
public:
	CButton m_btnDwnIns;
public:
	afx_msg void OnShowWindow(BOOL bShow, UINT nStatus);
public:
	afx_msg void OnClose();
public:
	afx_msg void OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags);
public:
	afx_msg void OnDestroy();
public:
	afx_msg void OnOpcionesCerrar();
public:
	afx_msg void OnOpcionesModosilecionso();
public:
	afx_msg void OnIraAyuda();
public:
	afx_msg void OnIraIra1();
public:
	afx_msg void OnIraIra2();
public:
	afx_msg void OnIraIra3();

public:
	afx_msg void OnModoExperto();
public:
	afx_msg void OnModoNormal();
public:
	afx_msg void OnModoInicio();
public:
	afx_msg void OnOpcionesReinstalarsoftware();
};
