// AdmoPassword.cpp : implementation file
//

#include "stdafx.h"
#include "ClUpdate.h"
#include "AdmoPassword.h"


// CAdmoPassword dialog

IMPLEMENT_DYNAMIC(CAdmoPassword, CDialog)

CAdmoPassword::CAdmoPassword(CWnd* pParent /*=NULL*/)
	: CDialog(CAdmoPassword::IDD, pParent)
	, m_editPassword(_T(""))
{

}

CAdmoPassword::~CAdmoPassword()
{
	int len;
	volatile BYTE *p;

	p = (volatile BYTE *) m_editPassword.GetBuffer();
	len = m_editPassword.GetLength() * sizeof(TCHAR);
	for ( int i = 0 ; i < len ; i++ ) {
		*p = 0x00;
		*p = 0x55;
		*p = 0xaa;
		*p = 0x00;
	}
}

void CAdmoPassword::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_EDIT_USUARIO, m_editUser);
	DDX_Text(pDX, IDC_PASSWORD, m_editPassword);
}


BEGIN_MESSAGE_MAP(CAdmoPassword, CDialog)
	ON_BN_CLICKED(IDOK, &CAdmoPassword::OnBnClickedOk)
END_MESSAGE_MAP()


// CAdmoPassword message handlers

void CAdmoPassword::OnEnChangeEdit1()
{
	// TODO:  If this is a RICHEDIT control, the control will not
	// send this notification unless you override the CDialog::OnInitDialog()
	// function and call CRichEditCtrl().SetEventMask()
	// with the ENM_CHANGE flag ORed into the mask.

	// TODO: Add your control notification handler code here
}

CString CAdmoPassword::GetUser(void)
{
	return m_editUser;
}

CString CAdmoPassword::GetPassword(void)
{
	return m_editPassword;
}

void CAdmoPassword::OnBnClickedOk()
{
	// TODO: Add your control notification handler code here
	OnOK();
}
