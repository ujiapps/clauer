#pragma once


// CErrDlg dialog

class CErrDlg : public CDialog
{
	DECLARE_DYNAMIC(CErrDlg)

public:
	CErrDlg(CWnd* pParent = NULL);   // standard constructor
	virtual ~CErrDlg();

// Dialog Data
	enum { IDD = IDD_DIALOG_ERROR };

protected:
	HICON m_hIcon;

	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL OnInitDialog();

	DECLARE_MESSAGE_MAP()
public:
	// Descripción del error
	BOOL SetErrDescription(UINT msgId);
	BOOL SetDescription(UINT msgId);
	BOOL SetCaption(UINT msgId);
public:
	afx_msg void OnBnClickedCerrar();
public:
	CString m_desc;
	CString m_errDesc;
public:
	afx_msg void OnShowWindow(BOOL bShow, UINT nStatus);
public:
	afx_msg void OnStnClickedStaticErrDesc();
};
