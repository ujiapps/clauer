#pragma once



class CNotify
{
public:
	CNotify(void);
public:
	~CNotify(void);
public:
	// Shows a balloon message in the notification area
	BOOL Notify(CString title, CString msg);
	// Shows the default notify icon in the notification area
	BOOL Show(void);
	// Hides the notify icon
	BOOL Hide(void);
	// Changes the icon shown in the status area
	BOOL ChangeIcon(UINT iconId);
	// Indica si el sistema puede o no puede mostrar un ballon
	BOOL BallonAvailable(void);
	// Establece el tooltip text
	BOOL SetToolTip ( CString msg );
	// Indica si el icono est� o no hidden
	BOOL Hidden ( void );

private:
	// The notifier icon data
	NOTIFYICONDATA m_nid;

private:
	BOOL m_ballonAvailable;
	BOOL m_bHidden;
};
