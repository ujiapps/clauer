#pragma once

#include "stdafx.h"
#include <windows.h>

/* Valores de retorno en el mensaje enviado
 */

#define UPDATE_NEW			0   /* actualización normal disponible */
#define UPDATE_CRITICAL		1   /* actualización crítica disponible */
#define UPDATE_OK			2	/* no hay actualización disponible */
#define UPDATE_ERROR		3	/* se produjo un error comprobando actualización */
#define UPDATE_RELEASE		4	/* nueva release disponible */

/* Tipo del parámetro esperado en pParam
 */

typedef struct {
	CEvent *wait;
	CWnd *wnd;
	DWORD dwParentThreadId;
} THRNOTIFIERPARAM, *PTHRNOTIFIERPARAM;


/* Punto de entrada al thread
 */

UINT CLUPDATE_THR_Notifier ( LPVOID pParam );



