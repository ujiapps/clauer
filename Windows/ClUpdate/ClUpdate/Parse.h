#pragma once
#include "afxwin.h"

class CParse :
	public CCommandLineInfo
{
public:
	CParse(void);
	~CParse(void);
public:
	//virtual void ParseParam( const char* pszParam, BOOL bFlag, BOOL bLast );
	virtual void ParseParam ( const TCHAR * pszParam, BOOL bFlag, BOOL bLast );
public:
	// Indica si se especific� /ini en la l�nea de comandos
	bool m_bIni;
};
