#include "StdAfx.h"
#include "Parse.h"

CParse::CParse(void)
: m_bIni(false)
{
}

CParse::~CParse(void)
{
}

void CParse::ParseParam(const TCHAR * pszParam, BOOL bFlag, BOOL bLast)
{
	if ( _tcscmp(pszParam, _T("ini")) == 0 )
		m_bIni = true;
}
