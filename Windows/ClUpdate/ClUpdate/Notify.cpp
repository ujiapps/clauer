#include "StdAfx.h"
#include "Notify.h"

#include "resource.h"

#include <windows.h>
#include <shellapi.h>
#include <shlwapi.h>
#include <tchar.h>

#include "AppMessages.h"

#include <lang/resource.h>
#include "lang.h"

#define MY_ICON_ID	1

CNotify::CNotify(void)
: m_ballonAvailable(FALSE)
{
	m_bHidden = TRUE;
}



CNotify::~CNotify(void)
{
}



// Shows a balloon message in the notification area
BOOL CNotify::Notify(CString title, CString msg)
{
	if ( ! m_bHidden ) {
		_tcscpy(m_nid.szInfo, msg.GetString());
		_tcscpy(m_nid.szInfoTitle, title.GetString());

		if ( ! Shell_NotifyIcon(NIM_MODIFY, &m_nid) )
			return FALSE;
	}

	return TRUE;
}


// Shows the default notify icon in the notification area

BOOL CNotify::Show(void)
{
	DLLGETVERSIONPROC DllGetVersion = NULL;
	DLLVERSIONINFO dllVersion;

	HMODULE hShell = ::LoadLibrary(_T("Shell32.dll"));
	if ( ! hShell )
		return FALSE;

	DllGetVersion = (DLLGETVERSIONPROC) GetProcAddress(hShell, "DllGetVersion");
	if ( ! DllGetVersion ) {
		FreeLibrary(hShell);
		return FALSE;
	}

	dllVersion.cbSize = sizeof(dllVersion);
	HRESULT h_r = DllGetVersion(&dllVersion);
	::FreeLibrary(hShell);

	if ( h_r == NOERROR ) {
		
		memset(&m_nid,0,sizeof(NOTIFYICONDATA));

		if (dllVersion.dwMajorVersion >= 6) {
			m_nid.cbSize = sizeof(NOTIFYICONDATA);
			m_ballonAvailable = TRUE;
		} else if(dllVersion.dwMajorVersion >= 5) {
			m_nid.cbSize = NOTIFYICONDATA_V2_SIZE;
			m_ballonAvailable = FALSE;
		} else {
			/* Con esta versi�n no tenemos ballon's
			 */
			m_nid.cbSize = NOTIFYICONDATA_V1_SIZE;
			m_ballonAvailable = FALSE;
		}

	} else {

		/* Asumimos cbSize == NOTIFYICONDATA_V1_SIZE
		 */

		m_nid.cbSize = NOTIFYICONDATA_V1_SIZE;
		m_ballonAvailable = FALSE;
	}

	m_nid.cbSize           = sizeof(NOTIFYICONDATA);
	m_nid.uID              = MY_ICON_ID;
	m_nid.uFlags           = NIF_ICON | NIF_MESSAGE | NIF_INFO | NIF_TIP ;
	m_nid.hIcon            = AfxGetApp()->LoadIcon(IDI_UJI);
	m_nid.hWnd             = AfxGetMainWnd()->m_hWnd;
	m_nid.uVersion         = NOTIFYICON_VERSION;
	m_nid.uCallbackMessage = WM_CLUPDATE;
	m_nid.dwInfoFlags      = NIIF_USER;

	_tcscpy(m_nid.szTip, GetClauerString(IDS_UPDATE_AGENT));

	if ( ! Shell_NotifyIcon(NIM_ADD, &m_nid) )
		return FALSE;

	m_bHidden = FALSE;

	return TRUE;
}

// Changes the icon shown in the status area

BOOL CNotify::ChangeIcon(UINT iconId)
{
	return TRUE;
}

// Indica si el sistema puede o no puede mostrar un ballon
BOOL CNotify::BallonAvailable(void)
{
	return m_ballonAvailable;
}


BOOL CNotify::Hide(void)
{
	if ( ! Shell_NotifyIcon(NIM_DELETE, &m_nid) )
		return FALSE;

	m_bHidden = TRUE;

	return TRUE;
}


BOOL CNotify::SetToolTip ( CString msg )
{
	m_nid.szInfo[0] = 0;
	m_nid.szInfoTitle[0] = 0;
	
	_tcscpy(m_nid.szTip, msg.GetString());

	if ( ! Shell_NotifyIcon(NIM_MODIFY, &m_nid) )
		return FALSE;

	return TRUE;
}

BOOL CNotify::Hidden ( void )
{
	return m_bHidden;
}

