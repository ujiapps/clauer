#pragma once

#include <windows.h>
#include <time.h>

#define CLUPDATE_REG_KEY	_T("Software\\Universitat Jaume I\\Projecte Clauer\\Clupdate")
#define DATE_LEN			8

#define VERSION_LEN			10

#define CLUPDATE_TYPE_NO		0
#define CLUPDATE_TYPE_CRITICAL	1
#define CLUPDATE_TYPE_NORMAL	2
#define CLUPDATE_TYPE_RELEASE	3

typedef enum { UPDMODE_NORMAL, UPDMODE_EXPERT, UPDMODE_INI, UPDMODE_AUTO } UPDATE_MODE;


class CClupdateReg
{

protected:

	HKEY m_hKey;

public:
	CClupdateReg(void);
	~CClupdateReg(void);

	BOOL Open  ( void );
	BOOL Close ( void );

	BOOL SetType       ( DWORD dwType );
	BOOL SetDate       ( time_t tDate );
	BOOL SetNextNotify ( time_t tNext );
	BOOL SetPeriod     ( DWORD ms );
	BOOL SetUpdateMode ( UPDATE_MODE um );
	
	BOOL GetType       ( DWORD *dwType );
	BOOL GetDate       ( time_t *tDate );
	BOOL GetNextNotify ( time_t *tNext );
	BOOL GetPeriod     ( DWORD * ms );
	BOOL GetUpdateMode ( UPDATE_MODE *um );
	BOOL GetUpdatePeriod ( DWORD *p );

	static BOOL GetVersion       ( char version[VERSION_LEN+1] );
	static BOOL GetOldVersion    ( char version[VERSION_LEN+1] );
	static BOOL SetOldVersion    ( char version[VERSION_LEN+1] );
	static BOOL DeleteOldVersion ( void );

	static BOOL GetRegUpdateMode ( UPDATE_MODE *um );

};

