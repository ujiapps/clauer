/* Defines específicos para cada release de clupdate
 */

#ifndef __COMMON_DEFINES_H__
#define __COMMON_DEFINES_H__

#ifdef CLUPDATE_PIPELINE

#define URL_HELP		_T("http://www.aavv.com/common/pi/gestion_usuarios/clauer/ayuda.html")

#define CLUPDATE_DOWNLOAD_SERVER	L"www.aavv.com"
#define CLUPDATE_DOWNLOAD_URL		L"common/pi/gestion_usuarios/clauer/setup-clauer-lu.exe"

#define CLUPDATE_NOTIFY_SERVER			L"www.aavv.com"
#define CLUPDATE_NOTIFY_TEMPLATE		"common/pi/gestion_usuarios/clauer/expire.php3?prg=&ver=0000000000"
#define CLUPDATE_NOTIFY_TEMPLATE_PRINTF	"common/pi/gestion_usuarios/clauer/expire.php3?prg=%s&ver=%s"
#define CLUPDATE_NOTIFY_PRG_NAME		"usbpki-base"

#define CLUPDATE_NOTIFY_DOWNLOAD_URL_GETTER L"?prg=usbpki-base&url=si"

#elif CLUPDATE_CYSD

#define URL_HELP	_T("http://clauer.uji.es/es/tutWin.html")

#define CLUPDATE_DOWNLOAD_SERVER	L"dwnl.nisu.org"
#define CLUPDATE_DOWNLOAD_URL		L"dwnl?fic=setup-clauer-CySD.exe&dwnl=si"

#define CLUPDATE_NOTIFY_SERVER			L"expire-cysd.nisu.org"
#define CLUPDATE_NOTIFY_TEMPLATE		"?prg=&ver=0000000000"
#define CLUPDATE_NOTIFY_TEMPLATE_PRINTF	"?prg=%s&ver=%s"
#define CLUPDATE_NOTIFY_PRG_NAME		"usbpki-base"

#define CLUPDATE_NOTIFY_DOWNLOAD_URL_GETTER L"?prg=usbpki-base&url=si"

#elif CLUPDATE_CATCERT

#define URL_HELP	_T("http://clauer.uji.es/es/tutWin.html")

#define CLUPDATE_DOWNLOAD_SERVER	L"dwnl.nisu.org"
#define CLUPDATE_DOWNLOAD_URL		L"dwnl?fic=setup-clauer-idCAT.exe&dwnl=si"

#define CLUPDATE_NOTIFY_SERVER			L"expire-idcat.nisu.org"
#define CLUPDATE_NOTIFY_TEMPLATE		"?prg=&ver=0000000000"
#define CLUPDATE_NOTIFY_TEMPLATE_PRINTF	"?prg=%s&ver=%s"
#define CLUPDATE_NOTIFY_PRG_NAME		"usbpki-base"

#define CLUPDATE_NOTIFY_DOWNLOAD_URL_GETTER L"?prg=usbpki-base&url=si"

#elif CLUPDATE_COITAVC

#define URL_HELP	_T("http://clauer.uji.es/es/tutWin.html")

#define CLUPDATE_DOWNLOAD_SERVER	L"dwnl.nisu.org"
#define CLUPDATE_DOWNLOAD_URL		L"dwnl?fic=setup-clauer-coitavc.exe&dwnl=si"

#define CLUPDATE_NOTIFY_SERVER			L"expire-coitavc.nisu.org"
#define CLUPDATE_NOTIFY_TEMPLATE		"?prg=&ver=0000000000"
#define CLUPDATE_NOTIFY_TEMPLATE_PRINTF	"?prg=%s&ver=%s"
#define CLUPDATE_NOTIFY_PRG_NAME		"usbpki-base"

#define CLUPDATE_NOTIFY_DOWNLOAD_URL_GETTER L"?prg=usbpki-base&url=si"

#elif CLUPDATE_ACCV

#define URL_HELP	_T("http://clauer.uji.es/es/tutWin.html")

#define CLUPDATE_DOWNLOAD_SERVER	L"dwnl.nisu.org"
#define CLUPDATE_DOWNLOAD_URL		L"dwnl?fic=setup-clauer-accv.exe&dwnl=si"

#define CLUPDATE_NOTIFY_SERVER			L"expire-accv.nisu.org"
#define CLUPDATE_NOTIFY_TEMPLATE		"?prg=&ver=0000000000"
#define CLUPDATE_NOTIFY_TEMPLATE_PRINTF	"?prg=%s&ver=%s"
#define CLUPDATE_NOTIFY_PRG_NAME		"usbpki-base"

#define CLUPDATE_NOTIFY_DOWNLOAD_URL_GETTER L"?prg=usbpki-base&url=si"

#else

#define URL_HELP	_T("http://clauer.uji.es/es/tutWin.html")

#define CLUPDATE_DOWNLOAD_SERVER	L"dwnl.nisu.org"
#define CLUPDATE_DOWNLOAD_URL		L"dwnl?fic=setup-clauer.exe&dwnl=si"

#define CLUPDATE_NOTIFY_SERVER			L"expire.nisu.org"
#define CLUPDATE_NOTIFY_TEMPLATE		"?prg=&ver=0000000000"
#define CLUPDATE_NOTIFY_TEMPLATE_PRINTF	"?prg=%s&ver=%s"
#define CLUPDATE_NOTIFY_PRG_NAME		"usbpki-base"

#define CLUPDATE_NOTIFY_DOWNLOAD_URL_GETTER L"?prg=usbpki-base&url=si"

#endif

#endif

