#pragma warning(disable:4996)

#include <stdafx.h>
#include <windows.h>
#include <wincrypt.h>
#include <tchar.h>

#include "regstore.h"


#define CLAUERSTORE_OID	"ClauerStoreProvider"


BOOL CLUPDATE_RegStoreMY (void)
{
	// Declare and initialize variables.

	LPCWSTR pvSystemName = L"ClauerStore"; 
	CERT_PHYSICAL_STORE_INFO PhysicalStoreInfo;
	DWORD dwStoreToRegister;

	// Registramos el store MY

	dwStoreToRegister = 0;

	memset(&PhysicalStoreInfo, 0, sizeof(CERT_PHYSICAL_STORE_INFO));
	
	PhysicalStoreInfo.cbSize                  = sizeof(CERT_PHYSICAL_STORE_INFO);
	PhysicalStoreInfo.pszOpenStoreProvider    = (LPSTR)CLAUERSTORE_OID;
	PhysicalStoreInfo.dwFlags                 = CERT_PHYSICAL_STORE_REMOTE_OPEN_DISABLE_FLAG;
	PhysicalStoreInfo.dwFlags                |= CERT_PHYSICAL_STORE_ADD_ENABLE_FLAG;
	PhysicalStoreInfo.OpenParameters.pbData   = (LPBYTE) &dwStoreToRegister;
	PhysicalStoreInfo.OpenParameters.cbData   = sizeof(DWORD);
	PhysicalStoreInfo.dwPriority              = 1;
	PhysicalStoreInfo.dwOpenEncodingType      = 0;

	if(!CertRegisterPhysicalStore(
		L"MY",
		CERT_SYSTEM_STORE_CURRENT_USER,
		L"ClauerStore",
		&PhysicalStoreInfo,
		NULL
		))
	{
		return FALSE;
	}

	/* Registramos los stores AddressBook, CA y Root
	 */

	LPWSTR stores[] = { L"MY", L"AddressBook", L"CA", L"Root" };

	for ( DWORD dwStoreToRegister = 1 ; dwStoreToRegister < 3 ; dwStoreToRegister++ ) {

		memset(&PhysicalStoreInfo, 0, sizeof(CERT_PHYSICAL_STORE_INFO));
		
		PhysicalStoreInfo.cbSize                  = sizeof(CERT_PHYSICAL_STORE_INFO);
		PhysicalStoreInfo.pszOpenStoreProvider    = (LPSTR)CLAUERSTORE_OID;
		PhysicalStoreInfo.dwFlags                 = CERT_PHYSICAL_STORE_REMOTE_OPEN_DISABLE_FLAG;
		PhysicalStoreInfo.OpenParameters.pbData   = (LPBYTE) &dwStoreToRegister;
		PhysicalStoreInfo.OpenParameters.cbData   = sizeof(DWORD);
		PhysicalStoreInfo.dwPriority              = 1;
		PhysicalStoreInfo.dwOpenEncodingType      = 0;

		if(!CertRegisterPhysicalStore(
			stores[dwStoreToRegister],
			CERT_SYSTEM_STORE_CURRENT_USER,
			L"ClauerStore",
			&PhysicalStoreInfo,
			NULL
			))
		{
			return FALSE;
		}
	}



	return TRUE;










}




BOOL CLUPDATE_UnregStoreMY (void)
{
	BOOL hr = TRUE;

	LPWSTR stores[] = { L"MY", L"AddressBook", L"CA", L"Root" };

	for ( int i = 0 ; i < 3 ; i++ ) {
		if ( ! CertUnregisterPhysicalStore(stores[i],
						  CERT_SYSTEM_STORE_CURRENT_USER, 
						  L"ClauerStore") )
		{
			return FALSE;
		}
	}


	return TRUE;


}

