#include "StdAfx.h"
#include "ClupdateReg.h"

#include <sddl.h>

CClupdateReg::CClupdateReg(void)
{
	m_hKey = 0;
}

CClupdateReg::~CClupdateReg(void)
{
}


BOOL CClupdateReg::Open  ( void )
{
	PSECURITY_ATTRIBUTES sa;
	
	/* Obtengo mi SID para construir una ACL para
	 * la clave del registro
	 */

	HANDLE hProcToken;
	LPTSTR szMySID;
	DWORD dwLength;
	LPVOID p_ti;
	PTOKEN_USER ti;

	if ( ! OpenProcessToken(GetCurrentProcess(), TOKEN_QUERY, &hProcToken) ) {
		return FALSE;
	}
	
	if ( ! GetTokenInformation(hProcToken, TokenUser, NULL, 0, &dwLength) ) {
		if ( GetLastError() != ERROR_INSUFFICIENT_BUFFER ) {
			CloseHandle(hProcToken);
			return FALSE;
		}
	}

	p_ti = new BYTE [dwLength];
	if ( ! p_ti ) {
		CloseHandle(hProcToken);
		return FALSE;
	}

	if ( ! GetTokenInformation(hProcToken, TokenUser, p_ti, dwLength, &dwLength) ) {
		if ( GetLastError() != ERROR_INSUFFICIENT_BUFFER ) {
			delete [] p_ti;
			CloseHandle(hProcToken);
			return FALSE;
		}
	}

	ti = (PTOKEN_USER) p_ti;

	if ( ! ConvertSidToStringSid(ti->User.Sid, &szMySID) ) {
		delete p_ti;
		CloseHandle(hProcToken);
		return FALSE;
	}

	CString strSD;
	strSD = _T("D:P");
	strSD += _T("(A;OICI;GA;;;");		  // Yo mismo, control total
	strSD += szMySID;
	strSD += _T(")");
	strSD += _T("(A;OICI;GA;;;BA)")      // Administradores control total
		     _T("(A;OICI;GA;;;SY)");	  // Local System control total

	LocalFree(szMySID);
		
	DWORD dwDisposition;

	sa = new SECURITY_ATTRIBUTES;
	if ( ! sa ) {
		CloseHandle(hProcToken);
		delete [] p_ti;
		return FALSE;
	}

	ZeroMemory(sa, sizeof(SECURITY_ATTRIBUTES));
	sa->nLength = sizeof(SECURITY_ATTRIBUTES);
	sa->bInheritHandle = FALSE;
	if ( ! ConvertStringSecurityDescriptorToSecurityDescriptor(strSD.GetString(), SDDL_REVISION_1, &(sa->lpSecurityDescriptor), NULL) ) {
		delete sa;
		return FALSE;
	}

	if ( RegCreateKeyEx(HKEY_CURRENT_USER, CLUPDATE_REG_KEY, 0, NULL,REG_OPTION_NON_VOLATILE, KEY_ALL_ACCESS, sa, &m_hKey, &dwDisposition) != ERROR_SUCCESS ) {
		LocalFree(sa->lpSecurityDescriptor);
		delete sa;
		CloseHandle(hProcToken);
		delete [] p_ti;
		return FALSE;
	}

	if ( dwDisposition == REG_CREATED_NEW_KEY ) {
		if ( ! SetType(0) ) {
			LocalFree(sa->lpSecurityDescriptor);
			delete sa;
			CloseHandle(hProcToken);
			delete [] p_ti;
			return FALSE;
		}
		if ( ! SetDate(time(NULL)) ) {
			LocalFree(sa->lpSecurityDescriptor);
			delete sa;
			CloseHandle(hProcToken);
			delete [] p_ti;
			return FALSE;
		}
		if ( ! SetNextNotify(time(NULL)) ) {
			LocalFree(sa->lpSecurityDescriptor);
			delete sa;
			CloseHandle(hProcToken);
			delete [] p_ti;
			return FALSE;
		}
	}

	/* TODO: Tal vez si la clave no se cre� debi�ramos establecer los
	 *       permisos de nuevo
	 */

	LocalFree(sa->lpSecurityDescriptor);
	delete sa;
	CloseHandle(hProcToken);
	delete [] p_ti;

	return TRUE;
}


BOOL CClupdateReg::Close ( void )
{
	if ( m_hKey )
		RegCloseKey(m_hKey);

	return TRUE;
}



BOOL CClupdateReg::SetType ( DWORD dwType )
{

	switch ( dwType ) {
	case CLUPDATE_TYPE_NO:
	case CLUPDATE_TYPE_CRITICAL:
	case CLUPDATE_TYPE_NORMAL:
	case CLUPDATE_TYPE_RELEASE:
		break;

	default:
		return FALSE;
	}

	if ( RegSetValueEx(m_hKey, _T("TYPE"), NULL, REG_DWORD, (BYTE *) &dwType, sizeof(DWORD)) != ERROR_SUCCESS ) 
		return FALSE;

	return TRUE;
}


BOOL CClupdateReg::SetDate ( time_t tDate )
{
	if ( ! m_hKey )
		return FALSE;

	if ( RegSetValueEx(m_hKey, _T("DATE"), 0, REG_BINARY, (BYTE *)&tDate, sizeof(time_t)) != ERROR_SUCCESS ) 
		return FALSE;

	return TRUE;
}


BOOL CClupdateReg::SetNextNotify ( time_t tNext )
{
	if ( ! m_hKey )
		return FALSE;

	if ( RegSetValueEx(m_hKey, _T("NEXT_NOTIFY"), 0, REG_BINARY, (BYTE *)&tNext, sizeof(time_t)) != ERROR_SUCCESS ) 
		return FALSE;

	return TRUE;
}


BOOL CClupdateReg::GetType ( DWORD *dwType )
{
	DWORD dwRegType, size;

	if ( ! m_hKey )
		return FALSE;
	if ( ! dwType )
		return FALSE;

	if ( RegQueryValueEx(m_hKey, _T("TYPE"), 0, &dwRegType, NULL, &size) != ERROR_SUCCESS )
		return FALSE;
	if ( size != sizeof(DWORD) )
		return FALSE;
	if ( dwRegType != REG_DWORD )
		return FALSE;
	if ( RegQueryValueEx(m_hKey, _T("TYPE"), 0, &dwRegType, (BYTE *) dwType, &size) != ERROR_SUCCESS )
		return FALSE;

	return TRUE;
}


BOOL CClupdateReg::GetDate ( time_t *tDate )
{
	DWORD size, dwRegType;

	if ( ! m_hKey )
		return FALSE;
	if ( ! tDate )
		return FALSE;

	if ( RegQueryValueEx(m_hKey, _T("DATE"), 0, &dwRegType, NULL, &size) != ERROR_SUCCESS )
		return FALSE;

	if ( dwRegType != REG_BINARY )
		return FALSE;
	if ( size != sizeof(time_t) )
		return FALSE;

	if ( RegQueryValueEx(m_hKey, _T("DATE"), 0, &dwRegType, (LPBYTE) tDate, &size) != ERROR_SUCCESS )
		return FALSE;

	return TRUE;
}

BOOL CClupdateReg::GetNextNotify ( time_t *tNext )
{
	DWORD size, dwRegType;

	if ( ! m_hKey )
		return FALSE;
	if ( ! tNext )
		return FALSE;

	if ( RegQueryValueEx(m_hKey, _T("NEXT_NOTIFY"), 0, &dwRegType, NULL, &size) != ERROR_SUCCESS )
		return FALSE;

	if ( dwRegType != REG_BINARY )
		return FALSE;
	if ( size > sizeof(time_t) )
		return FALSE;

	*tNext = 0;
	if ( RegQueryValueEx(m_hKey, _T("NEXT_NOTIFY"), 0, &dwRegType, (LPBYTE) tNext, &size) != ERROR_SUCCESS )
		return FALSE;

	return TRUE;
}







BOOL CClupdateReg::GetVersion ( char version[VERSION_LEN+1] )
{
	HKEY hKey;
	DWORD dwSize, dwRegType;

	if ( RegOpenKeyEx(HKEY_LOCAL_MACHINE, _T("Software\\Universitat Jaume I\\Projecte Clauer"), 0, KEY_READ, &hKey) != ERROR_SUCCESS ) 
		return FALSE;
	
	if ( RegQueryValueExA(hKey, "VERSION", 0, &dwRegType, NULL, &dwSize) != ERROR_SUCCESS ) {
		RegCloseKey(hKey);
		return FALSE;
	}

	if ( dwRegType != REG_SZ ) {
		RegCloseKey(hKey);
		return FALSE;
	}
	if ( dwSize != (VERSION_LEN + 1) ) {
		RegCloseKey(hKey);
		return FALSE;
	}

	if ( RegQueryValueExA(hKey, "VERSION", 0, &dwRegType, (LPBYTE) version, &dwSize) != ERROR_SUCCESS ) {
		RegCloseKey(hKey);
		return FALSE;
	}

	RegCloseKey(hKey);

	return TRUE;
}




/*! \brief Devuelve el modo de funcionamiento de Clauer Update: modo silencioso o interactivo.
 *
 * Devuelve el modo de funcionamiento de Clauer Update: modo silencioso o interactivo.
 *
 * \param [SALIDA] um
 *        El modo de funcionamiento: UPDMODE_NORMAL, UPDMODE_EXPERT, UPDMODE_INI
 *
 * \remarks Si la funci�n no tiene �xito se devuelve TRUE (no error) y um se establece
 *          a UPDMODE_NORMAL
 */

BOOL CClupdateReg::GetUpdateMode ( UPDATE_MODE *um )
{
	DWORD dwSize, dwRegType, dwValue;
	UPDATE_MODE default_um;

	if ( ! um )
		return FALSE;

	/* Obtengo el valor establecido en local machine (valor por defecto)
	 */

	CClupdateReg::GetRegUpdateMode(&default_um);
	*um = default_um;

	if ( ! m_hKey )
		return FALSE;
	
	if ( RegQueryValueEx(m_hKey, _T("UPDATE_MODE"), 0, &dwRegType, NULL, &dwSize) != ERROR_SUCCESS ) {
		*um     = default_um;
		dwValue = *um;
		if ( RegSetValueEx(m_hKey, _T("UPDATE_MODE"), 0, REG_DWORD, (BYTE *)&dwValue, sizeof(DWORD)) != ERROR_SUCCESS ) {
			return TRUE;
		}
		return TRUE;
	}

	if ( dwRegType != REG_DWORD ) {

		/* Si el tipo no es DWORD, vuelvo a crear el valor
		 */

		*um     = default_um;
		dwValue = *um;
		

		if ( RegDeleteValue(m_hKey, _T("UPDATE_MODE")) != ERROR_SUCCESS ) 
			return TRUE;

		if ( RegSetValueEx(m_hKey, _T("UPDATE_MODE"), 0, REG_DWORD, (BYTE *)&dwValue, sizeof(DWORD)) != ERROR_SUCCESS ) 
			return TRUE;

		return TRUE;
	}
	if ( dwSize != sizeof(DWORD) ) {
		*um     = default_um;
		dwValue = *um;

		if ( RegDeleteValue(m_hKey, _T("UPDATE_MODE")) != ERROR_SUCCESS ) 
			return TRUE;

		if ( RegSetValueEx(m_hKey, _T("UPDATE_MODE"), 0, REG_DWORD, (BYTE *)&dwValue, sizeof(DWORD)) != ERROR_SUCCESS ) 
			return TRUE;

		return TRUE;
	}

	if ( RegQueryValueEx(m_hKey, _T("UPDATE_MODE"), 0, &dwRegType, (LPBYTE) &dwValue, &dwSize) != ERROR_SUCCESS ) {

		*um     = default_um;
		dwValue = *um;

		if ( RegSetValueEx(m_hKey, _T("UPDATE_MODE"), 0, REG_DWORD, (BYTE *)&dwValue, sizeof(DWORD)) != ERROR_SUCCESS ) 
			return TRUE;

		return TRUE;
	}

	/* Comprobamos que el valor sea correcto
	 */

	if ( dwValue != UPDMODE_NORMAL && dwValue != UPDMODE_INI && dwValue != UPDMODE_EXPERT )
		*um = UPDMODE_NORMAL;
	else
		*um = (UPDATE_MODE) dwValue;


	return TRUE;
}





BOOL CClupdateReg::SetUpdateMode ( UPDATE_MODE um )
{
	DWORD dwValue;

	if ( ! m_hKey ) 
		return FALSE;
	if ( um != UPDMODE_NORMAL && um != UPDMODE_EXPERT && um != UPDMODE_INI )
		return FALSE;

	dwValue = um;
	if ( RegSetValueEx(m_hKey, _T("UPDATE_MODE"), 0, REG_DWORD, (BYTE *)&dwValue, sizeof(DWORD)) != ERROR_SUCCESS ) 
		return TRUE;
	
	return TRUE;
}




BOOL CClupdateReg::GetOldVersion ( char version[VERSION_LEN+1] )
{
	HKEY hKey;
	DWORD dwSize, dwRegType;

	if ( RegOpenKeyEx(HKEY_LOCAL_MACHINE, _T("Software\\Universitat Jaume I\\Projecte Clauer"), 0, KEY_ALL_ACCESS, &hKey) != ERROR_SUCCESS ) 
		return FALSE;
	
	if ( RegQueryValueExA(hKey, "CLUPDATE_OLD_VERSION", 0, &dwRegType, NULL, &dwSize) != ERROR_SUCCESS ) {
		RegCloseKey(hKey);
		return FALSE;
	}

	if ( dwRegType != REG_SZ ) {
		RegCloseKey(hKey);
		return FALSE;
	}
	if ( dwSize != (VERSION_LEN + 1) ) {
		RegCloseKey(hKey);
		return FALSE;
	}

	if ( RegQueryValueExA(hKey, "CLUPDATE_OLD_VERSION", 0, &dwRegType, (LPBYTE) version, &dwSize) != ERROR_SUCCESS ) {
		RegCloseKey(hKey);
		return FALSE;
	}

	RegCloseKey(hKey);

	return TRUE;
}




BOOL CClupdateReg::DeleteOldVersion ( void )
{
	HKEY hKey;
	
	if ( RegOpenKeyEx(HKEY_LOCAL_MACHINE, _T("Software\\Universitat Jaume I\\Projecte Clauer"), 0, KEY_ALL_ACCESS, &hKey) != ERROR_SUCCESS ) 
		return FALSE;
	
	if ( RegDeleteValue(hKey, _T("CLUPDATE_OLD_VERSION")) != ERROR_SUCCESS ) {
		RegCloseKey(hKey);
		return FALSE;
	}

	RegCloseKey(hKey);

	return TRUE;
}




BOOL CClupdateReg::SetOldVersion ( char version[VERSION_LEN+1] )
{
	HKEY hKey;

	if ( RegOpenKeyExA(HKEY_LOCAL_MACHINE, "Software\\Universitat Jaume I\\Projecte Clauer", 0, KEY_WRITE, &hKey) != ERROR_SUCCESS ) 
		return FALSE;

	if ( RegSetValueExA(hKey, "CLUPDATE_OLD_VERSION", 0, REG_SZ, (BYTE *)version, (DWORD) (::strlen(version)+1)) != ERROR_SUCCESS ) {
		RegCloseKey(hKey);
		return TRUE;
	}

	RegCloseKey(hKey);
	
	return TRUE;
}





BOOL CClupdateReg::GetRegUpdateMode ( UPDATE_MODE *um )
{
	HKEY hKey;
	DWORD dwSize, dwRegType, dwValue;

	*um = UPDMODE_NORMAL;
	if ( RegOpenKeyEx(HKEY_LOCAL_MACHINE, _T("Software\\Universitat Jaume I\\Projecte Clauer"), 0, KEY_ALL_ACCESS, &hKey) != ERROR_SUCCESS ) 
		if ( RegOpenKeyEx(HKEY_LOCAL_MACHINE, _T("Software\\Universitat Jaume I\\Projecte Clauer"), 0, KEY_READ, &hKey) != ERROR_SUCCESS ) 
			return FALSE;
	
	/* Si no puedo obtener el valor UPDATE_MODE, devuelvo UPDMODE_NORMAL e intento crear la
	 * entrada en el registro
	 */

	if ( RegQueryValueEx(hKey, _T("UPDATE_MODE"), 0, &dwRegType, NULL, &dwSize) != ERROR_SUCCESS ) {

		dwValue = UPDMODE_NORMAL;
		*um = UPDMODE_NORMAL;

		if ( RegSetValueEx(hKey, _T("UPDATE_MODE"), 0, REG_DWORD, (BYTE *)&dwValue, sizeof(DWORD)) != ERROR_SUCCESS ) {
			RegCloseKey(hKey);
			return TRUE;
		}

		RegCloseKey(hKey);
		return TRUE;
	}

	if ( dwRegType != REG_DWORD ) {

		/* Si el tipo no es DWORD, vuelvo a crear el valor
		 */

		dwValue = UPDMODE_NORMAL;
		*um = UPDMODE_NORMAL;

		if ( RegDeleteValue(hKey, _T("UPDATE_MODE")) != ERROR_SUCCESS ) {
			*um = UPDMODE_NORMAL;
			RegCloseKey(hKey);
			return TRUE;
		}

		if ( RegSetValueEx(hKey, _T("UPDATE_MODE"), 0, REG_DWORD, (BYTE *)&dwValue, sizeof(DWORD)) != ERROR_SUCCESS ) {
			RegCloseKey(hKey);
			return TRUE;
		}

		RegCloseKey(hKey);
		return TRUE;
	}
	if ( dwSize != sizeof(DWORD) ) {
		dwValue = UPDMODE_NORMAL;
		*um = UPDMODE_NORMAL;

		if ( RegDeleteValue(hKey, _T("UPDATE_MODE")) != ERROR_SUCCESS ) {
			*um = UPDMODE_NORMAL;
			RegCloseKey(hKey);
			return TRUE;
		}

		if ( RegSetValueEx(hKey, _T("UPDATE_MODE"), 0, REG_DWORD, (BYTE *)&dwValue, sizeof(DWORD)) != ERROR_SUCCESS ) {
			RegCloseKey(hKey);
			return TRUE;
		}

		RegCloseKey(hKey);
		return TRUE;
	}

	if ( RegQueryValueEx(hKey, _T("UPDATE_MODE"), 0, &dwRegType, (LPBYTE) &dwValue, &dwSize) != ERROR_SUCCESS ) {

		dwValue = UPDMODE_NORMAL;
		*um     = UPDMODE_NORMAL;

		if ( RegSetValueEx(hKey, _T("UPDATE_MODE"), 0, REG_DWORD, (BYTE *)&dwValue, sizeof(DWORD)) != ERROR_SUCCESS ) {
			RegCloseKey(hKey);
			return TRUE;
		}

		RegCloseKey(hKey);
		return TRUE;
	}

	RegCloseKey(hKey);

	/* Comprobamos que el valor sea correcto
	 */

	if ( dwValue != UPDMODE_NORMAL && dwValue != UPDMODE_INI && dwValue != UPDMODE_EXPERT && dwValue != UPDMODE_AUTO )
		*um = UPDMODE_NORMAL;
	else
		*um = (UPDATE_MODE) dwValue;

	return TRUE;
}




BOOL CClupdateReg::GetUpdatePeriod ( DWORD *p )
{
	DWORD dwSize, dwType;

	if ( ! p )
		return FALSE;

	if ( RegQueryValueEx(m_hKey, _T("PERIOD"), NULL, &dwType, NULL, &dwSize) != ERROR_SUCCESS ) 
		return FALSE;
	if ( dwType != REG_DWORD || dwSize != sizeof(DWORD) )
		return FALSE;
	if ( RegQueryValueEx(m_hKey, _T("PERIOD"), NULL, &dwType, (LPBYTE) p, &dwSize) != ERROR_SUCCESS )
		return FALSE;

	return TRUE;
}
