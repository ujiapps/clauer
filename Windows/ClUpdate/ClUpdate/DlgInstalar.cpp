// DlgInstalar.cpp : implementation file
//

#include "stdafx.h"
#include "ClUpdate.h"
#include "DlgInstalar.h"

#include "ClUpdateDlg.h"

#include <lang/resource.h>
#include "lang.h"


// CDlgInstalar dialog

IMPLEMENT_DYNAMIC(CDlgInstalar, CDialog)

CDlgInstalar::CDlgInstalar(CWnd* pParent /*=NULL*/)
	: CDialog(CDlgInstalar::IDD, pParent)
{

}

CDlgInstalar::~CDlgInstalar()
{
}

void CDlgInstalar::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}



BOOL CDlgInstalar::OnInitDialog()
{
	CString aux;

	SetDlgItemText(IDC_STATIC_DESC_INSTALAR, GetClauerString(IDS_UPDATE_DESC_INSTALAR));
	UpdateData();
	return TRUE;
}





BOOL CDlgInstalar::PreTranslateMessage(MSG* pMsg )
{
	CClUpdateDlg *p = (CClUpdateDlg *) this->GetParent();

	if ( pMsg->message == WM_KEYDOWN ) {
		switch ( pMsg->wParam) {
			case VK_ESCAPE:	
				p->OnClose();
				return TRUE;
			case VK_RETURN:
				p->OnBnClickedButtonIniciar();
				return TRUE;
			case VK_TAB:
				p->m_btnDwnIns.SetFocus();
				return TRUE;
		}
	}
	return CDialog::PreTranslateMessage(pMsg);
}

BEGIN_MESSAGE_MAP(CDlgInstalar, CDialog)
	ON_STN_CLICKED(IDC_STATIC_DESC_INSTALAR, &CDlgInstalar::OnStnClickedStaticDescInstalar)
END_MESSAGE_MAP()



void CDlgInstalar::OnStnClickedStaticDescInstalar()
{
	// TODO: Add your control notification handler code here
}
