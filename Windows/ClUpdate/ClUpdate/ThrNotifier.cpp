#include "stdafx.h"
#include "ThrNotifier.h"
#include "AppMessages.h"
#include "ClupdateReg.h"
#include <windows.h>
#include <winhttp.h>

#include "common_defines.h"

#define VERSION_SIZE	10

#define PRG_REG_VALOR		"UP_USBPKIBASE"
#define PRG_NAME			"UJI - USBPKI Base"
#define PRG_NAME_WEB		"usbpki-base"
#define PRG_NAME_FILE		"setup-base.exe"

#define PRG_URL_EXPIRE		"http://expire.nisu.org/"
#define PRG_REG_KEY			"SOFTWARE\\Universitat Jaume I\\Projecte Clauer"
#define PRG_REG_VERSION		"VERSION"

#define SERVER_ERROR	-1
#define SERVER_OK		0
#define SERVER_UP		1
#define SERVER_NEW		2
#define SERVER_RELEASE	3

#define THR_SLEEP_INTERVAL	5000   // Cada 10 minutos hace una consulta al servidor

// Function prototypes

int  CLUPDATE_AskServer         (void);
BOOL CLUPDATE_Get_Local_Version ( char version[VERSION_SIZE+1] );

// -> 0 error
// -> 1 ok

UINT CLUPDATE_THR_Notifier ( LPVOID pParam )
{
	PTHRNOTIFIERPARAM p;
	int serverState;
	CClupdateReg reg;
	BOOL bRegOpened = FALSE, err = FALSE;

	if ( ! pParam )
		return 0;

	p = (PTHRNOTIFIERPARAM) pParam;

	while ( 1 ) {

		if ( ! reg.Open() ) 
			goto thrNotifierSleep;

		bRegOpened = TRUE;

		serverState = CLUPDATE_AskServer ();

		if ( serverState == SERVER_UP || serverState == SERVER_NEW || serverState == SERVER_RELEASE ) {

			DWORD dwType;

			if ( ! reg.GetType(&dwType) ) {
				err = TRUE;
				goto thrNotifierSleep;
			}

			if ( dwType == CLUPDATE_TYPE_NO ) {

				/*if ( ! reg.SetNextNotify(time(NULL)) ) {
					err = TRUE;
					goto thrNotifierSleep;
				}*/
				if ( ! reg.SetDate(time(NULL)) ) {
					err = TRUE;
					goto thrNotifierSleep;
				}
				if ( ! reg.SetType(serverState) ) {
					err = TRUE;
					goto thrNotifierSleep;
				}

			} else {
				if ( ! reg.SetType(serverState) ) {
					err = TRUE;
					goto thrNotifierSleep;
				}
			}
		
			if ( serverState == SERVER_UP )
				::PostMessage(p->wnd->m_hWnd, WM_CLUPDATE_UPDATE, UPDATE_CRITICAL, 0);
			else if ( serverState == SERVER_RELEASE )
				::PostMessage(p->wnd->m_hWnd, WM_CLUPDATE_UPDATE, UPDATE_RELEASE, 0);
			else
				::PostMessage(p->wnd->m_hWnd, WM_CLUPDATE_UPDATE, UPDATE_NEW, 0);

		} else if ( serverState == SERVER_OK ) {

			/* Si todo est� ok, simplemente actualizo el registro para
			 * que todo funcione correctamente y lo indico al otro thread
			 */

			reg.SetType(CLUPDATE_TYPE_NO);			
		} 

thrNotifierSleep:

		if ( bRegOpened ) {
			reg.Close();
			bRegOpened = FALSE;
		}

		if ( err || serverState == SERVER_ERROR ) 
			::PostMessage(p->wnd->m_hWnd, WM_CLUPDATE_UPDATE, UPDATE_ERROR, 0);

		if ( serverState == SERVER_OK )
			::PostMessage(p->wnd->m_hWnd, WM_CLUPDATE_UPDATE, UPDATE_OK, 0);

		p->wait->Lock();
		
		//::Sleep(THR_SLEEP_INTERVAL);

	} // while


}

/* SERVER_ERROR ---> Error
 * SERVER_OK --> Ok
 * SERVER_UP ---> Update
 * SERVER_NEW ---> New
 */

int CLUPDATE_AskServer (void)
{
    DWORD dwSize = 0;
    DWORD dwDownloaded = 0;
	DWORD dwTotal = 0;
    BYTE *pszOutBuffer = NULL, *aux;
    BOOL  bResults = FALSE;
    HINTERNET  hSession = NULL, 
               hConnect = NULL,
               hRequest = NULL;

	char *URLBytes = NULL;
	WCHAR *url = NULL;
	int ret = SERVER_OK;

    // Use WinHttpOpen to obtain a session handle.

    hSession = WinHttpOpen( L"Clauer Update/2.0",  
                            WINHTTP_ACCESS_TYPE_DEFAULT_PROXY,
                            WINHTTP_NO_PROXY_NAME, 
                            WINHTTP_NO_PROXY_BYPASS, 0);

	if ( ! hSession ) {
		ret = SERVER_ERROR;
		goto finPRG_ConectaServidor;
	}	


	/* Establecemos los diversos timeouts:
	 *
	 *      - Timeout para resolver nombres: 10s
	 *      - Timeout conexi�n al servidor: 10s
	 *		- Timeout para el env�o de la petici�n: 10s
	 *      - Timeout para la respuesta: 10s
	 */

	if ( ! WinHttpSetTimeouts(hSession, 10000, 10000, 10000, 10000) ) {
		ret = SERVER_ERROR;
		goto finPRG_ConectaServidor;
	}

    // Specify an HTTP server

    hConnect = WinHttpConnect( hSession, CLUPDATE_NOTIFY_SERVER,
                               INTERNET_DEFAULT_HTTP_PORT, 0);

	if ( !hConnect ) {
		ret = SERVER_ERROR;
		goto finPRG_ConectaServidor;
	}


	URLBytes = (char *) malloc ( strlen(CLUPDATE_NOTIFY_PRG_NAME) + strlen(CLUPDATE_NOTIFY_TEMPLATE) + 1 );

	if ( ! URLBytes ) {
		ret = SERVER_ERROR;
		goto finPRG_ConectaServidor;
	}


	url = (WCHAR *) malloc ( sizeof(WCHAR) * (strlen(CLUPDATE_NOTIFY_PRG_NAME) + strlen(CLUPDATE_NOTIFY_TEMPLATE) + 1));

	if ( !url ) {
		ret = SERVER_ERROR;
		goto finPRG_ConectaServidor;
	}

	/* Obtengo la versi�n del software del clauer. Si no pudiera obtenerla
	 * indico que el software est� caducado poniendo una fecha irrancional :-)
	 */

	char version[VERSION_LEN+1];
	if ( ! CClupdateReg::GetVersion(version) ) 
		strcpy(version, "0000000000");
	
	sprintf(URLBytes, CLUPDATE_NOTIFY_TEMPLATE_PRINTF, CLUPDATE_NOTIFY_PRG_NAME, version);

	if ( ! MultiByteToWideChar(CP_ACP,MB_PRECOMPOSED, URLBytes, -1, url, (int) strlen(CLUPDATE_NOTIFY_PRG_NAME) + strlen(CLUPDATE_NOTIFY_TEMPLATE) + 1) ) {
		ret = SERVER_ERROR;
		goto finPRG_ConectaServidor;
	}


    // Create an HTTP request handle.

    hRequest = WinHttpOpenRequest( hConnect, L"GET", url,
                                   NULL, WINHTTP_NO_REFERER, 
                                   WINHTTP_DEFAULT_ACCEPT_TYPES, 
                                   0);

	if ( !hRequest ) {
		ret = SERVER_ERROR;
		goto finPRG_ConectaServidor;
	}

    // Send a request.

		// Ojo!! Devuelve que no puede conectar con el server...

    bResults = WinHttpSendRequest( hRequest,
                                   WINHTTP_NO_ADDITIONAL_HEADERS, 0,
                                   WINHTTP_NO_REQUEST_DATA, 0, 
                                   0, 0);

	if ( ! bResults ) {
		DWORD err = GetLastError();


		ret = SERVER_ERROR;
		goto finPRG_ConectaServidor;
	}

    // End the request.

    bResults = WinHttpReceiveResponse( hRequest, NULL);

	if ( !bResults ) {
		ret = SERVER_ERROR;
		goto finPRG_ConectaServidor;
	}


	pszOutBuffer = (BYTE *) malloc (1024 * 1024);

	if  (!pszOutBuffer) {
		ret = SERVER_ERROR;
		goto finPRG_ConectaServidor;
	}

	aux = pszOutBuffer;

    // Keep checking for data until there is nothing left.
    do 
    {
        // Check for available data.

        dwSize = 0;
        if (!WinHttpQueryDataAvailable( hRequest, &dwSize)) {
			ret = SERVER_ERROR;
			goto finPRG_ConectaServidor;
		}

        // Read the Data.

		if ( dwTotal + dwSize >= (1024*1024+1) ) 
			break;
		

        ZeroMemory(aux, dwSize+1);

        if (!WinHttpReadData( hRequest, (LPVOID)aux, dwSize, &dwDownloaded)) {
			ret = SERVER_ERROR;
			goto finPRG_ConectaServidor;
		}
    
        // Free the memory allocated to the buffer.
        
		dwTotal += dwDownloaded;
		if ( dwTotal >= (1024*1024 + 1) ) {
			dwSize = 0;
		} else {
			aux += dwDownloaded;
		}

    } while (dwSize>0);

	if ( memcmp(pszOutBuffer, "OK", 2) == 0 ) {
		ret = SERVER_OK;
	} else if ( memcmp(pszOutBuffer, "UP", 2) == 0 ) {
		ret = SERVER_UP;
	} else if ( memcmp(pszOutBuffer, "NW", 2) == 0 ) {
		ret = SERVER_NEW;
	} else if ( memcmp(pszOutBuffer, "RE", 2) == 0 ) {
		ret = SERVER_RELEASE;
	} else {
		ret = SERVER_OK;
	}


finPRG_ConectaServidor:

    if (hRequest) 
		WinHttpCloseHandle(hRequest);

    if (hConnect) 
		WinHttpCloseHandle(hConnect);

    if (hSession) 
		WinHttpCloseHandle(hSession);

	if ( URLBytes )
		free(URLBytes);

	if ( url )
		free(url);

	if ( pszOutBuffer )
		free(pszOutBuffer);

	return ret;

}




BOOL CLUPDATE_Get_Local_Version ( char version[VERSION_SIZE+1] )
{
	HKEY hKey;
	DWORD tamVersion, tipo;

	if ( RegOpenKeyExA(HKEY_LOCAL_MACHINE, PRG_REG_KEY, 0, KEY_READ, &hKey) != ERROR_SUCCESS )
		return FALSE;

	tamVersion = VERSION_SIZE + 1;

	if ( RegQueryValueExA(hKey, PRG_REG_VERSION, NULL, &tipo, (BYTE *) version, &tamVersion) != ERROR_SUCCESS ) {
		RegCloseKey(hKey);	
		return FALSE;
	}

	RegCloseKey(hKey);

	if ( (tamVersion != (VERSION_SIZE + 1)) || tipo != REG_SZ )
		return FALSE;


	return TRUE;
}




