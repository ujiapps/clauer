// DlgRecordar.cpp : implementation file
//

#include "stdafx.h"
#include "ClUpdate.h"
#include "DlgRecordar.h"

#include "ClUpdateDlg.h"
#include <lang/resource.h>
#include "lang.h"


// CDlgRecordar dialog

IMPLEMENT_DYNAMIC(CDlgRecordar, CDialog)

CDlgRecordar::CDlgRecordar(CWnd* pParent /*=NULL*/)
	: CDialog(CDlgRecordar::IDD, pParent)
{

}

CDlgRecordar::~CDlgRecordar()
{
}

void CDlgRecordar::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_COMBO1, m_cmbTiempo);
}

BOOL CDlgRecordar::OnInitDialog()
{
	CDialog::OnInitDialog();

	CString msg;
	//msg.LoadString(IDS_UPDATE_30_MIN);
	msg = GetClauerString(IDS_UPDATE_30_MIN);
	m_cmbTiempo.AddString(msg.GetString());
	//msg.LoadString(IDS_UPDATE_1_HOUR);
	msg = GetClauerString(IDS_UPDATE_1_HOUR);
	m_cmbTiempo.AddString(msg.GetString());
	//msg.LoadStringW(IDS_UPDATE_TOMORROW);
	msg = GetClauerString(IDS_UPDATE_TOMORROW);
	m_cmbTiempo.AddString(msg.GetString());
	m_cmbTiempo.SetCurSel(0);
	UpdateData(FALSE);

	SetDlgItemText(IDC_STATIC_REMEMBER, GetClauerString(IDS_UPDATE_REMEMBER));
	SetDlgItemText(IDC_STATIC_SET_PERIOD, GetClauerString(IDS_UPDATE_SET_PERIOD));

	return TRUE;
}

BEGIN_MESSAGE_MAP(CDlgRecordar, CDialog)
	ON_WM_SETFOCUS()
	ON_STN_CLICKED(IDC_STATIC_SET_PERIOD, &CDlgRecordar::OnStnClickedStaticSetPeriod)
END_MESSAGE_MAP()


// CDlgRecordar message handlers

DWORD CDlgRecordar::GetPeriod(void)
{
	CString strSel, str30Min, str1Hour, strTomorrow;

/*
	str30Min.LoadString(IDS_UPDATE_30_MIN);
	str1Hour.LoadString(IDS_UPDATE_1_HOUR);
	strTomorrow.LoadString(IDS_UPDATE_TOMORROW);
*/
	str30Min    = GetClauerString(IDS_UPDATE_30_MIN);
	str1Hour    = GetClauerString(IDS_UPDATE_1_HOUR);
	strTomorrow = GetClauerString(IDS_UPDATE_TOMORROW);

	m_cmbTiempo.GetLBText(m_cmbTiempo.GetCurSel(), strSel);

	if ( strSel == str30Min )
		return 30*60;
	else if ( strSel == str1Hour )
		return 60*60;
	else if ( strSel == strTomorrow )
		return 24*60*60;
	else
		return 30*60;
}



BOOL CDlgRecordar::PreTranslateMessage(MSG* pMsg )
{
	CClUpdateDlg *p = (CClUpdateDlg *) this->GetParent();

	if ( pMsg->message == WM_KEYDOWN ) {
		switch ( pMsg->wParam) {
			case VK_ESCAPE:	
				p->OnClose();
				return TRUE;
			case VK_RETURN:
				p->OnBnClickedButtonIniciar();
				return TRUE;
			case VK_TAB:
				p->m_btnDwnIns.SetFocus();
				return TRUE;
		}
	}
	return CDialog::PreTranslateMessage(pMsg);
}

void CDlgRecordar::OnSetFocus(CWnd* pOldWnd)
{
	CDialog::OnSetFocus(pOldWnd);

	this->m_cmbTiempo.SetFocus();
}

void CDlgRecordar::OnStnClickedStaticSetPeriod()
{
	// TODO: Add your control notification handler code here
}
