#include <windows.h>
#include <cpl.h>
#include <stdio.h>
#include <string.h>
#include <tchar.h>

#include "resource.h"




BOOL WINAPI DllMain ( HINSTANCE hinstDLL, 
		      DWORD fdwReason,
		      LPVOID lpvReserved )
{


  switch ( fdwReason ) {
       case DLL_PROCESS_ATTACH:
         // Initialize once for each new process.
         // Return FALSE to fail DLL load.
            break;

        case DLL_THREAD_ATTACH:
         // Do thread-specific initialization.
            break;

        case DLL_THREAD_DETACH:
         // Do thread-specific cleanup.
            break;

        case DLL_PROCESS_DETACH:
         // Perform any necessary cleanup.
            break;
  }


  return TRUE;
}

/* La implementación actual se limita a lanzar el ejecutable ClauerControlPanel.exe
 */

LONG CALLBACK CPlApplet( HWND hwndCPl,
		     UINT uMsg,
		     LPARAM lParam1,
		     LPARAM lParam2
		     )
{ 
  CPLINFO *i;
  HKEY hKey;
  TCHAR szControlPanelPath[MAX_PATH];
  DWORD dwType, dwSize;
  
  switch ( uMsg )
    {

    case CPL_INIT:
	  // Sent immediately after the DLL containing the Control Panel item is loaded. The message prompts CPlApplet 
	  // to perform initialization procedures, including memory allocation.
      return TRUE;

    case CPL_GETCOUNT:
	  // Sent after the CPL_INIT message to prompt CPlApplet to return a number that
	  // indicates how many subprograms it supports.
      return 1;

    case CPL_INQUIRE:
      // Sent after the CPL_GETCOUNT message to prompt CPlApplet to provide information
	  // about a specified subprogram. The lParam1 value is an integer that represents
	  // the zero-based index of the subprogram about which information is being requested.
	  // The lParam2 parameter of CPlApplet points to a CPLINFO structure.

      i = (CPLINFO *) lParam2;
      i->lData = 0;
      i->idIcon = IDI_UJI;
      i->idName = IDS_CLCTRL;
      i->idInfo = IDS_CLCTRL_DESC;

      break;

    case CPL_DBLCLK:
   	  // Sent to notify CPlApplet that the user has chosen the icon associated with a given subprogram. 
	  // CPlApplet should display the corresponding dialog box and carry out any user-specified tasks

      if ( RegOpenKeyEx(HKEY_LOCAL_MACHINE, _T("Software\\Universitat Jaume I\\Projecte Clauer"), 0, KEY_READ, &hKey) != ERROR_SUCCESS )
		return 0;
	  if ( RegQueryValueEx(hKey, _T("INSDIR"), NULL, &dwType, NULL, &dwSize) != ERROR_SUCCESS ) {
		RegCloseKey(hKey);
		return 0;
      }
      if ( dwType != REG_SZ ) {
		RegCloseKey(hKey);
		return 0;
      }
      if ( dwSize > (MAX_PATH - 2 - _tcsclen(_T("clauer_control_panel.exe")))*sizeof(TCHAR) ) {
		RegCloseKey(hKey);
		return 0;
      }
      if ( RegQueryValueEx(hKey, _T("insdir"), NULL, &dwType, (LPBYTE) szControlPanelPath, &dwSize) != ERROR_SUCCESS ) {
		RegCloseKey(hKey);
		return 0;
      }
      RegCloseKey(hKey);
      _tcscat(szControlPanelPath, _T("\\clauer_control_panel.exe"));

      ShellExecute(hwndCPl, _T("open"), szControlPanelPath, NULL, NULL, SW_SHOW);
      break;

    case CPL_STOP:
	  // Sent once for each subprogram before the controlling application unloads the Control Panel extension. 
	  // CPlApplet should free any memory associated with the subprogram number provided in lParam1
      break;

    case CPL_EXIT:
	  // Sent after the last CPL_STOP message and immediately before the controlling application uses the FreeLibrary 
	  // function to free the DLL that contains the Control Panel item. CPlApplet should free any remaining memory and 
	  // prepare to close
      break;

    }

  return 0;

}



