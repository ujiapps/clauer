#include "StdAfx.h"
#include "Prop.h"

CProp::CProp(UINT nIDTemplate, CWnd* pParent /*= NULL*/)
	: CDialog(nIDTemplate, pParent)
{
}

CProp::~CProp(void)
{
}


BOOL CProp::PreTranslateMessage(MSG* pMsg )
{
	if ( pMsg->message == WM_KEYDOWN ) {
		switch ( pMsg->wParam) {
			case VK_ESCAPE:	
			case VK_RETURN:
				return TRUE;
		}
	}
	return CDialog::PreTranslateMessage(pMsg);
}

