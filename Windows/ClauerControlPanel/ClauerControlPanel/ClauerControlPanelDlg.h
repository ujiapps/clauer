// ClauerControlPanelDlg.h: archivo de encabezado
//

#pragma once
#include "afxcmn.h"

#include "PropUpdate.h"
#include "PropNoAdmo.h"
#include "PropInfo.h"
#include "PropConnectivity.h"


// Cuadro de di�logo de CClauerControlPanelDlg
class CClauerControlPanelDlg : public CDialog
{
// Construcci�n
public:
	CClauerControlPanelDlg(DWORD dwInitialPage = 0, CWnd* pParent = NULL);	// Constructor est�ndar

// Datos del cuadro de di�logo
	enum { IDD = IDD_CLAUERCONTROLPANEL_DIALOG };

private:
	static const int N_TABS  = 3;   // N�mero m�ximo de pesta�as
	static const int N_PAGES = 4;   // N�mero m�ximo de p�ginas distintas que tenemos
	enum TAB_CONTROL_PAGES { INFO_PAGE, 
		UPDATE_PAGE, 
		PROP_NO_ADMO_PAGE,
		CONNECTIVITY_PAGE
	};
	CProp *m_pages[N_PAGES];
	DWORD m_dwInitialPage;

	BOOL ShowTab ( int tab_num );

protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// Compatibilidad con DDX/DDV


// Implementaci�n
protected:
	HICON m_hIcon;

	BOOL m_bSaveUpdate;

	// Funciones de asignaci�n de mensajes generadas
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()

	// P�gina de configuraci�n del Update
	CPropUpdate *m_propUpdate;
	CPropNoAdmo *m_propNoAdmo;
	CPropInfo *m_propInfo;

	// Indica si el usuario pertenece al grupo
	// administradores o no
	BOOL m_isAdmo;

	// Indica el modo de actualizaci�n actual
	int m_realUpdateMode;

public:
	// The tab with properties
	CTabCtrl m_tab;
public:
	afx_msg void OnTcnSelchangeTabProperties(NMHDR *pNMHDR, LRESULT *pResult);
public:
	afx_msg void OnBnClickedOk();
};
