#ifndef __MISC_H__
#define __MISC_H__

#include <windows.h>

#define CONN_MAX_PROXY_SIZE		255

BOOL GetUpdateMode   ( UPDATE_MODE *updMode );
BOOL SetUpdateMode   ( UPDATE_MODE updMode );
BOOL SetUpdatePeriod ( DWORD updPeriod );
BOOL SetUpdatePeriod ( DWORD updPeriod );

BOOL Reg_GetConnectivityType ( DWORD *dwType );
BOOL Reg_SetConnectivityType ( DWORD dwType );
BOOL Reg_GetConnectivityProxy ( TCHAR szProxy[CONN_MAX_PROXY_SIZE] );
BOOL Reg_SetConnectivityProxy ( TCHAR szProxy[CONN_MAX_PROXY_SIZE] );


#endif

