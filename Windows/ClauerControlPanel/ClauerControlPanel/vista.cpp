#include "stdafx.h"
#include "vista.h"
#include <tchar.h>

#define MAX_NAME 256


// Basada en funci�n:
//
// http://msdn2.microsoft.com/en-US/library/aa379554.aspx

BOOL clauer_vista_is_in_administrator_group ( BOOL & bIsInAdministrorsGroup )
{
	DWORD i, dwSize = 0, dwResult = 0;
	HANDLE hToken;
	PTOKEN_GROUPS pGroupInfo;
	BYTE sidBuffer[100];
	PSID pSID = (PSID)&sidBuffer;
	SID_IDENTIFIER_AUTHORITY SIDAuth = SECURITY_NT_AUTHORITY;

	if (!OpenProcessToken( GetCurrentProcess(), TOKEN_QUERY, &hToken )) {
		return FALSE;
	}

	if(!GetTokenInformation(hToken, TokenGroups, NULL, dwSize, &dwSize)) {
		dwResult = GetLastError();
		if( dwResult != ERROR_INSUFFICIENT_BUFFER ) {
			return FALSE;
		}
	}
	pGroupInfo = (PTOKEN_GROUPS) GlobalAlloc( GPTR, dwSize );
	if ( ! pGroupInfo )
		return FALSE;

	if( ! GetTokenInformation(hToken, TokenGroups, pGroupInfo, 
							dwSize, &dwSize ) ) 
	{
		GlobalFree(pGroupInfo);
		return FALSE;
	}

	// Create a SID for the BUILTIN\Administrators group.

	if(! AllocateAndInitializeSid( &SIDAuth, 2,
					 SECURITY_BUILTIN_DOMAIN_RID,
					 DOMAIN_ALIAS_RID_ADMINS,
					 0, 0, 0, 0, 0, 0,
					 &pSID) ) 
	{
		GlobalFree(pGroupInfo);
		return FALSE;   
	}

	// Loop through the group SIDs looking for the administrator SID.

	bIsInAdministrorsGroup = FALSE;
	for(i=0; i<pGroupInfo->GroupCount; i++) {
		if ( EqualSid(pSID, pGroupInfo->Groups[i].Sid) ) {
			bIsInAdministrorsGroup = TRUE;
			break;
		}
	}

	if (pSID)
		FreeSid(pSID);
	if ( pGroupInfo )
		GlobalFree( pGroupInfo );

	return TRUE;
}



BOOL clauer_vista_is_windows_vista ( BOOL &is )
{
	BOOL ret = TRUE;
	OSVERSIONINFO ver;

	ZeroMemory(&ver, sizeof(OSVERSIONINFO));
	ver.dwOSVersionInfoSize = sizeof(OSVERSIONINFO);

	if ( ! GetVersionEx(&ver) ) {
		DWORD dwerr = GetLastError();
		ret = FALSE;
		goto endclauer_vista_is_windows_vista;
	}

	is = (ver.dwMajorVersion == 6);

endclauer_vista_is_windows_vista:

	return ret;

}



BOOL clauer_vista_is_elevated ( BOOL &bIsAdmo )
{
	BOOL ret = TRUE, bVista;
	HANDLE hToken = 0, hProcess = 0;
	DWORD dwInfoSize = 0;

	SID_IDENTIFIER_AUTHORITY NtAuthority = SECURITY_NT_AUTHORITY;
	PSID AdministratorsGroup = 0; 

	if (  ! clauer_vista_is_windows_vista(bVista) ) {
		ret = FALSE;
		goto endCLUPDATE_UserIsAdmo;
	}

	if ( bVista ) {

		hProcess = GetCurrentProcess();   // no hace falta cerrarlo
		if ( ! hProcess ) {
			ret = FALSE;
			goto endCLUPDATE_UserIsAdmo;
		}
		if ( ! OpenProcessToken(hProcess, TOKEN_QUERY, &hToken) ) {
			ret = FALSE;
			goto endCLUPDATE_UserIsAdmo;
		}
		TOKEN_ELEVATION elevation;
		DWORD dwElevationSize = sizeof elevation;
		if ( ! GetTokenInformation(hToken, TokenElevation, (LPVOID) &elevation, dwElevationSize, &dwElevationSize) ) {
			ret = FALSE;
			goto endCLUPDATE_UserIsAdmo;
		}

		/*
		    TRUE - the current process is elevated. This value indicates that either UAC is enabled,
			       and the process was elevated by the administrator, or that UAC is disabled and the process
				   was started by a user who is a member of the Administrators group.

            FALSE - the current process is not elevated (limited). This value indicates that either UAC is
			        enabled, and the process was started normally, without the elevation, or that UAC is
					disabled and the process was started by a standard user.
		*/

		if ( elevation.TokenIsElevated ==  0 )
			bIsAdmo = FALSE;
		else
			bIsAdmo = TRUE;

	} else {

		if ( ! AllocateAndInitializeSid(&NtAuthority, 
					  2, 
					  SECURITY_BUILTIN_DOMAIN_RID,
					  DOMAIN_ALIAS_RID_ADMINS,
					  0, 0, 0, 0, 0, 0,
					  &AdministratorsGroup) ) 
		{
			ret = FALSE;
			goto endCLUPDATE_UserIsAdmo;
		}

		if ( ! CheckTokenMembership( NULL, AdministratorsGroup, &bIsAdmo) ) {
			ret = FALSE;
			goto endCLUPDATE_UserIsAdmo;
		}
	}

endCLUPDATE_UserIsAdmo:

	if ( hToken )
		CloseHandle(hToken);
	if ( AdministratorsGroup )
		FreeSid(AdministratorsGroup);

	return ret;
}


BOOL clauer_vista_launch_elevated ( BOOL &bCancelled, LPTSTR lpszParams )
{
	BOOL ret = TRUE;
	TCHAR szFileName[MAX_PATH];

	if ( ! GetModuleFileName(NULL, szFileName, MAX_PATH) )
		return FALSE;

    SHELLEXECUTEINFO shex;

    memset( &shex, 0, sizeof( shex) );
    shex.cbSize        = sizeof( SHELLEXECUTEINFO );
    shex.fMask         = 0;
    shex.hwnd          = NULL;
    shex.lpVerb        = _T("runas");
    shex.lpFile        = szFileName;
	shex.lpParameters  = lpszParams;
    //shex.lpParameters  = _T("/INI");
    shex.nShow         = SW_NORMAL;

	// Debo comprobar si el usuario puls� cancelar o no

	ret = ::ShellExecuteEx( &shex );
	if ( ! ret ) {
		if ( GetLastError() == ERROR_CANCELLED ) {
			ret = TRUE;
			bCancelled = TRUE;
		} else {
			ret = FALSE;
			bCancelled = FALSE;
		}
	} else {
		bCancelled = FALSE;
	}

    return ret;
}

