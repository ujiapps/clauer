// PropNoAdmo.cpp : implementation file
//

#include "stdafx.h"
#include "ClauerControlPanel.h"
#include "PropNoAdmo.h"
#include <Commctrl.h>

#include <lang/resource.h>
#include "lang.h"
#include "vista.h"

// CPropNoAdmo dialog

IMPLEMENT_DYNAMIC(CPropNoAdmo, CDialog)

CPropNoAdmo::CPropNoAdmo(CWnd* pParent /*=NULL*/, DWORD msgID /* = = IDS_NO_ADMO  */, DWORD dwPageToShow /* = 0 */)
	: CProp(CPropNoAdmo::IDD, pParent)
{
	m_msgID        = msgID;
	m_dwPageToShow = dwPageToShow;
}

CPropNoAdmo::~CPropNoAdmo()
{
}

void CPropNoAdmo::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}





void CPropNoAdmo::SetPageToShow ( DWORD dwPageToShow )
{
	m_dwPageToShow = dwPageToShow;
}


BOOL CPropNoAdmo::OnInitDialog()
{
	CDialog::OnInitDialog();

	// Traducci�n de cadenas

	SetDlgItemText(IDC_ERROR_MESSAGE,GetClauerString(m_msgID));

	// Si estamos en Windows Vista y no est� el proceso elevado

	BOOL bDrawElevateButton = FALSE;

	BOOL bIsVista;
	if ( ! clauer_vista_is_windows_vista(bIsVista) ) 
		bIsVista = FALSE;

	if ( bIsVista ) {

		BOOL bIsElevated;
		if ( ! clauer_vista_is_elevated(bIsElevated) )
			bIsElevated = TRUE;

		bDrawElevateButton = TRUE;
	}

	CWnd *wnd = GetDlgItem(BTN_ELEVATE);
	if ( bDrawElevateButton ) {
		if ( wnd )
			Button_SetElevationRequiredState(wnd->m_hWnd, TRUE);
		wnd->ShowWindow(SW_SHOW);
		wnd->SetWindowText(GetClauerString(IDS_CTRL_PANEL_NO_ADMO_EXEC_AS_ADMO));
	} else {
		wnd->ShowWindow(SW_HIDE);	
	}

	UpdateData(FALSE);

	return TRUE;
}

BEGIN_MESSAGE_MAP(CPropNoAdmo, CDialog)
	ON_BN_CLICKED(BTN_ELEVATE, &CPropNoAdmo::OnBnClickedElevate)
END_MESSAGE_MAP()


// CPropNoAdmo message handlers

void CPropNoAdmo::OnBnClickedElevate()
{
	BOOL bCanceled;
	CString params;
	
	params.Format(_T("/page %d"), m_dwPageToShow);
	if ( clauer_vista_launch_elevated(bCanceled, (LPTSTR) params.GetString()) )
		if ( ! bCanceled )
			PostQuitMessage(0);
}
