#pragma once

#include <lang/resource.h>
#include "lang.h"
#include "Prop.h"

// CPropNoAdmo dialog

class CPropNoAdmo : public CProp
{
	DECLARE_DYNAMIC(CPropNoAdmo)

public:
	CPropNoAdmo(CWnd* pParent = NULL, DWORD msgID = IDS_CTRL_PANEL_NO_ADMO_REQUIRE_ADMO, DWORD dwPageToShow = 0 );   // standard constructor
	virtual ~CPropNoAdmo();

	BOOL SaveValues(void) { return TRUE; };

// Dialog Data
	enum { IDD = IDD_PROPERTY_NO_ADMO };

protected:

	// El identificador del mensaje a mostrar
	DWORD m_msgID;

	DWORD m_dwPageToShow;

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL OnInitDialog();

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedElevate();

	void SetPageToShow ( DWORD dwPageToShow );
};
