#pragma once
#include "afxwin.h"

class CProp : public CDialog
{
public:
	CProp (void) {};
	CProp(UINT nIDTemplate, CWnd* pParent = NULL);
public:
	~CProp(void);
public:
	virtual BOOL SaveValues ( void ) = 0;

protected:
	virtual BOOL PreTranslateMessage(MSG* pMsg );
};
