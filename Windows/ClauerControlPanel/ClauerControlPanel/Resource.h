//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by ClauerControlPanel.rc
//
#define IDD_CLAUERCONTROLPANEL_DIALOG   102
#define IDR_MAINFRAME                   103
#define IDD_PROPERTY_UPDATE             129
#define IDD_PROPERTY_INFO               130
#define IDD_PROPERTY_NO_ADMO            131
#define IDD_PROPERTY_CONNECTIVITY       132
#define IDI_FORBIDDEN                   133
#define IDI_ICON1                       137
#define IDB_HEADER                      138
#define IDC_TAB_PROPERTIES              1002
#define IDC_COMBO_PERIOD                1004
#define IDC_DESCRIPTION                 1008
#define IDC_PERIOD                      1013
#define IDC_MODE_DESC                   1014
#define IDC_RADIO_AUTO                  1015
#define IDC_RADIO_INI                   1016
#define IDC_RADIO_NORMAL                1017
#define IDC_RADIO_EXPERT                1019
#define IDC_ERROR_MESSAGE               1021
#define IDC_BUTTON1                     1022
#define IDC_BTN_CREDITS                 1022
#define BTN_ELEVATE                     1022
#define IDC_COMBO_VER                   1023
#define IDC_LBL_VER                     1024
#define IDC_STATIC_CONFIG_UPDATE        1029
#define IDC_STATIC_FUNC_MODE            1030
#define IDC_STATIC_DESC                 1031
#define IDC_STATIC_PERIOD               1032
#define IDC_VERSION                     1033
#define IDC_LBL_VERSION_TITLE           1033
#define IDC_INFO                        1034
#define IDC_LBL_IDEN_TITLE              1034
#define IDC_ICON                        1035
#define IDC_LBL_IDIOM_TITLE             1036
#define IDC_LBL_VERSION                 1037
#define IDC_LBL_IDEN                    1038
#define IDC_GROUP_INSTALL_INFO          1040
#define IDC_BTN_REINSTALL               1042
#define IDC_LBL_IDIOM                   1043
#define IDC_BTN_GENERAL_LICENSE         1044
#define IDC_COMBO_CONN_TYPE             1045
#define IDC_EDIT1                       1046
#define IDC_EDIT2                       1047
#define IDC_LBL_TYPE                    1048
#define IDC_LBL_HTTP_SERVER             1049
#define IDC_LBL_PORT                    1050
#define IDC_CONN_DESCRIPTION            1051
#define BTN_ELEVATE_UPDATE              1052
#define BTN_UPDATE_ELEVATE              1052

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        139
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1053
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
