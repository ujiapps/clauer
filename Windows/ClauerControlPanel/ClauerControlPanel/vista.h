#ifndef __CTRL_PANEL_VISTA_H__
#define __CTRL_PANEL_VISTA_H__

#include "stdafx.h"
#include <windows.h>
#include <tchar.h>

BOOL clauer_vista_is_elevated        ( BOOL &bIsElevated );
BOOL clauer_vista_is_windows_vista   ( BOOL &is );
BOOL clauer_vista_launch_elevated    ( BOOL &bCancelled, LPTSTR lpszParams );   // Vista only
BOOL clauer_vista_is_in_administrator_group ( BOOL & bIsInAdministrorsGroup );

#endif