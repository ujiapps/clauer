#pragma once

#include "Prop.h"

#define MAX_VERSION_SIZE	100
#define MAX_IDEN_SIZE		100
#define MAX_IDIOM_SIZE		100

// CPropInfo dialog

class CPropInfo : public CProp
{
	DECLARE_DYNAMIC(CPropInfo)

public:
	CPropInfo(CWnd* pParent = NULL);   // standard constructor
	virtual ~CPropInfo();

	BOOL SaveValues	( void );

// Dialog Data

	enum { IDD = IDD_PROPERTY_INFO };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL OnInitDialog();

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedBtnGeneralLicense();
};
