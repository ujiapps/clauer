// ClauerControlPanel.cpp : define los comportamientos de las clases para la aplicaci�n.
//

#include "stdafx.h"
#include "ClauerControlPanel.h"
#include "ClauerControlPanelDlg.h"
#include "vista.h"
#include "CmdLineParser.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CClauerControlPanelApp

BEGIN_MESSAGE_MAP(CClauerControlPanelApp, CWinApp)
	ON_COMMAND(ID_HELP, &CWinApp::OnHelp)
END_MESSAGE_MAP()


// Construcci�n de CClauerControlPanelApp

CClauerControlPanelApp::CClauerControlPanelApp()
{
	// TODO: agregar aqu� el c�digo de construcci�n,
	// Colocar toda la inicializaci�n importante en InitInstance
}


// El �nico objeto CClauerControlPanelApp

CClauerControlPanelApp theApp;

// Handle a la DLL de idioma

HMODULE g_hLangDll = 0;


// Inicializaci�n de CClauerControlPanelApp

BOOL CClauerControlPanelApp::InitInstance()
{
	// Windows XP requiere InitCommonControlsEx() si un manifiesto de
	// aplicaci�n especifica el uso de ComCtl32.dll versi�n 6 o posterior para habilitar
	// estilos visuales. De lo contrario, se generar� un error al crear ventanas.
	INITCOMMONCONTROLSEX InitCtrls;
	InitCtrls.dwSize = sizeof(InitCtrls);
	// Establecer para incluir todas las clases de control comunes que desee utilizar
	// en la aplicaci�n.
	InitCtrls.dwICC = ICC_WIN95_CLASSES;
	//InitCtrls.dwICC = ICC_USEREX_CLASSES;
	InitCommonControlsEx(&InitCtrls);

	CWinApp::InitInstance();

	AfxEnableControlContainer();

	// Parseamos la l�nea de comandos. Los par�metros que acepta de momento
	// el programa son:
	//
	//       /page {numero}. Indica que el programa debe iniciarse en la p�gina
	//                       especificada. Ejemplo: ClauerControlPanel /page 2

	CCmdLineParser parser;
	CWinApp::ParseCommandLine(parser);
	DWORD dwInitialPage = 0;
	dwInitialPage = parser.GetInitialPage();

	// WINDOWS VISTA: Vamos a ver si elevamos o no
	//
	//    Elevamos si est� en el grupo de administradores

/*	BOOL bIsElevated = FALSE;
	BOOL bIsVista = FALSE;
	BOOL bIsInAdmoGroup = FALSE;
	BOOL bCancelled = FALSE;

	clauer_vista_is_windows_vista(bIsVista);
	if ( bIsVista ) {
		clauer_vista_is_elevated(bIsElevated);
		if ( ! bIsElevated ) {
			clauer_vista_is_in_administrator_group(bIsInAdmoGroup);
			if ( bIsInAdmoGroup ) {
					if ( clauer_vista_launch_elevated(bCancelled) ) {
					// cancele o no cancele, debemos terminar con el programa.
					// no voy a permitir ejecutarlo sin elevaci�n para evitar
					// confusiones
					return FALSE;
				}
			}
		}
	}
*/

	// WINDOWS VISTA

	// Cargamos la DLL de idioma correspondiente

	HKEY hKey;
	DWORD dwType, dwSize;
	TCHAR idioma[10];

	if ( RegOpenKeyEx(HKEY_LOCAL_MACHINE, _T("Software\\Universitat Jaume I\\Projecte Clauer"), 0, KEY_READ, &hKey) != ERROR_SUCCESS ) {
		AfxMessageBox(_T("Cannot get language to use.\r\nPlease, reinstall the Clauer Software"), MB_ICONERROR | MB_OK);
		return FALSE;
	}
	if ( RegQueryValueEx(hKey, _T("Idioma"), NULL, &dwType, NULL, &dwSize) != ERROR_SUCCESS ) {
		RegCloseKey(hKey);
		AfxMessageBox(_T("Cannot get language to use.\r\nPlease, reinstall the Clauer Software"), MB_ICONERROR | MB_OK);
		return FALSE;		
	}
	if ( dwType != REG_SZ ) {
		RegCloseKey(hKey);
		AfxMessageBox(_T("Cannot get language to use.\r\nPlease, reinstall the Clauer Software"), MB_ICONERROR | MB_OK);
		return FALSE;				
	}
	if ( dwSize > 10*sizeof(TCHAR) ) {
		RegCloseKey(hKey);
		AfxMessageBox(_T("Cannot get language to use.\r\nPlease, reinstall the Clauer Software"), MB_ICONERROR | MB_OK);
		return FALSE;		
	}
	if ( RegQueryValueEx(hKey, _T("Idioma"), NULL, &dwType, (LPBYTE) idioma, &dwSize) != ERROR_SUCCESS ) {
		RegCloseKey(hKey);
		AfxMessageBox(_T("Cannot get language to use.\r\nPlease, reinstall the Clauer Software"), MB_ICONERROR | MB_OK);
		return FALSE;		
	}
	RegCloseKey(hKey);
	if ( _tcsncmp(idioma, _T("1027"), 10) == 0 ) {
		/* catal�n */
		g_hLangDll = LoadLibrary(_T("va_language.dll"));
		if ( ! g_hLangDll ) {
			AfxMessageBox(_T("Cannot load va_language.dll.\r\nPlease, reinstall the Clauer Software"), MB_ICONERROR | MB_OK);
			return FALSE;
		}
	} else if ( _tcsncmp(idioma, _T("1034"), 10) == 0 ) {
		/* espa�ol */
		g_hLangDll = LoadLibrary(_T("es_language.dll"));
		if ( ! g_hLangDll ) {
			AfxMessageBox(_T("Cannot load es_language.dll.\r\nPlease, reinstall the Clauer Software"), MB_ICONERROR|MB_OK);
			return FALSE;
		}
	} else {
		AfxMessageBox(_T("Unknown language to use.\r\nPlease, reinstall the Clauer Software"), MB_ICONERROR|MB_OK);
		return FALSE;
	}

	// Inicializaci�n est�ndar
	// Si no utiliza estas caracter�sticas y desea reducir el tama�o
	// del archivo ejecutable final, debe quitar
	// las rutinas de inicializaci�n espec�ficas que no necesite
	// Cambie la clave del Registro en la que se almacena la configuraci�n
	// TODO: debe modificar esta cadena para que contenga informaci�n correcta
	// como el nombre de su compa��a u organizaci�n
	//SetRegistryKey(_T("Aplicaciones generadas con el Asistente para aplicaciones local"));

	CClauerControlPanelDlg dlg(dwInitialPage);
	m_pMainWnd = &dlg;
	INT_PTR nResponse = dlg.DoModal();
	if (nResponse == IDOK)
	{
		// TODO: insertar aqu� el c�digo para controlar
		//  cu�ndo se descarta el cuadro de di�logo con Aceptar
	}
	else if (nResponse == IDCANCEL)
	{
		// TODO: insertar aqu� el c�digo para controlar
		//  cu�ndo se descarta el cuadro de di�logo con Cancelar
	}

	// Dado que el cuadro de di�logo se ha cerrado, devolver FALSE para salir
	//  de la aplicaci�n en vez de iniciar el suministro de mensajes de dicha aplicaci�n.
	return FALSE;
}
