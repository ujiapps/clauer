// PropInfo.cpp : implementation file
//

#include "stdafx.h"
#include "ClauerControlPanel.h"
#include "PropInfo.h"

#include "lang.h"
#include <lang/resource.h>


// CPropInfo dialog

IMPLEMENT_DYNAMIC(CPropInfo, CDialog)

CPropInfo::CPropInfo(CWnd* pParent /*=NULL*/)
	: CProp(CPropInfo::IDD, pParent)
{

}

CPropInfo::~CPropInfo()
{
}

void CPropInfo::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}

// No hay valores por establecer

BOOL CPropInfo::SaveValues	( void )
{
	return TRUE;
}

BOOL CPropInfo::OnInitDialog()
{
	CDialog::OnInitDialog();

	// Traducci�n de cadenas

	SetDlgItemText(IDC_LBL_VERSION_TITLE, GetClauerString(IDS_CTRL_PANEL_INFO_VERSION));
	SetDlgItemText(IDC_LBL_IDEN_TITLE, GetClauerString(IDS_CTRL_PANEL_INFO_IDEN));
	SetDlgItemText(IDC_LBL_IDIOM_TITLE, GetClauerString(IDS_CTRL_PANEL_INFO_IDIOM));
	SetDlgItemText(IDC_GROUP_INSTALL_INFO, GetClauerString(IDS_CTRL_PANEL_INFO_INSTALL_INFO));
	SetDlgItemText(IDC_BTN_GENERAL_LICENSE, GetClauerString(IDS_CTRL_PANEL_INFO_LICENSE));
	SetDlgItemText(IDC_BTN_CREDITS, GetClauerString(IDS_CTRL_PANEL_INFO_CREDITS));
	SetDlgItemText(IDC_BTN_REINSTALL, GetClauerString(IDS_CTRL_PANEL_INFO_REINSTALL));


	// Leemos el asunto

	TCHAR version[MAX_VERSION_SIZE];
	TCHAR iden[MAX_IDEN_SIZE];
	TCHAR idioma[MAX_IDIOM_SIZE];

	ZeroMemory(version, MAX_VERSION_SIZE * sizeof(TCHAR));
	ZeroMemory(iden, MAX_VERSION_SIZE * sizeof(TCHAR));
	ZeroMemory(idioma, MAX_VERSION_SIZE * sizeof(TCHAR));

	_tcscpy(version, _T("Please reinstall clauer software"));
	_tcscpy(iden, _T("Please reinstall clauer software"));
	_tcscpy(idioma, _T("Please reinstall clauer software"));

	HKEY hKey;
	DWORD dwType, dwSize;

	if ( RegOpenKeyEx(HKEY_LOCAL_MACHINE, 
		         _T("Software\\Universitat Jaume I\\Projecte Clauer"), 
				 0,
				 KEY_READ,
				 &hKey) != ERROR_SUCCESS )
	{
		return FALSE;
	}
	if ( RegQueryValueEx(hKey, _T("version"), 0, &dwType, NULL, &dwSize) != ERROR_SUCCESS ) {
		RegCloseKey(hKey);
		return FALSE;
	}
	if ( dwType != REG_SZ ) {
		RegCloseKey(hKey);
		return FALSE;		
	}
	if ( dwSize > (MAX_VERSION_SIZE-1) ) {
		RegCloseKey(hKey);
		return FALSE;		
	}
	if ( RegQueryValueEx(hKey, _T("version"), 0, &dwType, (LPBYTE) version, &dwSize) != ERROR_SUCCESS ) {
		RegCloseKey(hKey);
		return FALSE;
	}

	if ( RegQueryValueEx(hKey, _T("iden"), 0, &dwType, NULL, &dwSize) != ERROR_SUCCESS  ) {
		RegCloseKey(hKey);
		return FALSE;
	}
	if ( dwType != REG_SZ ) {
		RegCloseKey(hKey);
		return FALSE;		
	}
	if ( dwSize > (MAX_IDEN_SIZE-1) ) {
		RegCloseKey(hKey);
		return FALSE;		
	}
	if ( RegQueryValueEx(hKey, _T("iden"), 0, &dwType, (LPBYTE) iden, &dwSize) != ERROR_SUCCESS  ) {
		RegCloseKey(hKey);
		return FALSE;
	}

	if ( RegQueryValueEx(hKey, _T("idioma"), 0, &dwType, NULL, &dwSize) != ERROR_SUCCESS  ) {
		RegCloseKey(hKey);
		return FALSE;
	}
	if ( dwType != REG_SZ ) {
		RegCloseKey(hKey);
		return FALSE;		
	}
	if ( dwSize > (MAX_IDIOM_SIZE-1) ) {
		RegCloseKey(hKey);
		return FALSE;		
	}
	if ( RegQueryValueEx(hKey, _T("idioma"), 0, &dwType, (LPBYTE) idioma, &dwSize) != ERROR_SUCCESS ) {
		RegCloseKey(hKey);
		return FALSE;
	}
	RegCloseKey(hKey);

	if ( _tcsnccmp(idioma, _T("1027"), 4) == 0 ) {
		_tcscpy(idioma, _T("Catal�"));
	} else if ( _tcsnccmp(idioma, _T("1034"), 4) == 0 ) {
		_tcscpy(idioma, _T("Castellano"));
	} else {
		_tcscpy(idioma, _T("Unknown"));
	}

	// Inicializamos el asunto

	SetDlgItemText(IDC_LBL_VERSION, version);
	SetDlgItemText(IDC_LBL_IDEN, iden);
	SetDlgItemText(IDC_LBL_IDIOM, idioma);

	UpdateData();

	return TRUE;
}


BEGIN_MESSAGE_MAP(CPropInfo, CDialog)
	ON_BN_CLICKED(IDC_BTN_GENERAL_LICENSE, &CPropInfo::OnBnClickedBtnGeneralLicense)
END_MESSAGE_MAP()


// CPropInfo message handlers

void CPropInfo::OnBnClickedBtnGeneralLicense()
{
	HKEY hKey;
	DWORD dwSize, dwType;
	TCHAR szLicenseFile[MAX_PATH];

	// Obtenemos el fichero de licencia

	ZeroMemory(szLicenseFile, sizeof(TCHAR) * MAX_PATH);

	if ( RegOpenKeyEx(HKEY_LOCAL_MACHINE, _T("Software\\Universitat Jaume I\\Projecte Clauer"), 0, KEY_READ, &hKey) != ERROR_SUCCESS ) 
		return;
	if ( RegQueryValueEx(hKey, _T("INSDIR"), NULL, &dwType, NULL, &dwSize) != ERROR_SUCCESS ) {
		RegCloseKey(hKey);
		return;
	}
	if ( dwType != REG_SZ ) {
		RegCloseKey(hKey);
		return;
	}
	// tenemos que a�adir \LICENSE.txt al final, as� es que 
	// restamos 12-1 :-)
	if ( dwSize > ((MAX_PATH-13)*sizeof(TCHAR)) ) {
		RegCloseKey(hKey);
		return;
	}
	if ( RegQueryValueEx(hKey, _T("INSDIR"), NULL, &dwType, (LPBYTE) szLicenseFile, &dwSize) != ERROR_SUCCESS ) {
		RegCloseKey(hKey);
		return;
	}
	RegCloseKey(hKey);
	
	_tcscat(szLicenseFile, _T("\\LICENSE.txt"));

	// Lo copiamos a uno temporal ( para que no lo modifiquen
	// f�cilmente que es muy tentador :-))

	TCHAR szTmpLicenseFile[MAX_PATH];
	ZeroMemory(szTmpLicenseFile, MAX_PATH*sizeof(TCHAR));
	if ( ! GetTempPath(MAX_PATH-13, szTmpLicenseFile) ) {
		return;
	}
	_tcscat(szTmpLicenseFile, _T("\\license.txt"));
	if ( ! CopyFile(szLicenseFile, szTmpLicenseFile, FALSE) )
		return;

	// Abrimos la licencia con el notepad
	ShellExecute(m_hWnd, _T("open"), szTmpLicenseFile, NULL, NULL, SW_SHOWNORMAL);
}
