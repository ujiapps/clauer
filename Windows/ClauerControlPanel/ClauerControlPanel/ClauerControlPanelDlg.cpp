// ClauerControlPanelDlg.cpp: archivo de implementaci�n
//

#include "stdafx.h"
#include "ClauerControlPanel.h"
#include "ClauerControlPanelDlg.h"

#include "lang.h"
#include "vista.h"
#include <lang/resource.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#endif






// Cuadro de di�logo de CClauerControlPanelDlg

CClauerControlPanelDlg::CClauerControlPanelDlg(DWORD dwInitialPage /* = 0 */, CWnd* pParent /*=NULL*/)
: CDialog(CClauerControlPanelDlg::IDD, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
	m_propUpdate = NULL;
	m_propNoAdmo = NULL;

	// Indicamos qu� tab mostraremos primero

	if ( (dwInitialPage >= N_TABS) || (dwInitialPage < 0 )  )
		m_dwInitialPage = 0;
	else
		m_dwInitialPage = dwInitialPage;

	// Determinamos la pertenencia del usuario al grupo administradores
	// Si no podemos determinarlo asumimos que no lo es

	BOOL bIsVista, bIsElevated, bIsAdmo;

	if ( ! clauer_vista_is_windows_vista(bIsVista) )
		bIsVista=FALSE;
	if ( bIsVista ) {
		if ( ! clauer_vista_is_elevated(bIsElevated) ) {
			bIsElevated = FALSE;
		}
		m_isAdmo = bIsElevated;
	} else {
		if ( ! clauer_vista_is_in_administrator_group(m_isAdmo) )
			m_isAdmo = FALSE;
	} 


	// El modo de actualizaci�n, de momento sin establecer

	m_realUpdateMode = -1;

}

void CClauerControlPanelDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_TAB_PROPERTIES, m_tab);
}

BEGIN_MESSAGE_MAP(CClauerControlPanelDlg, CDialog)
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	//}}AFX_MSG_MAP
	ON_NOTIFY(TCN_SELCHANGE, IDC_TAB_PROPERTIES, &CClauerControlPanelDlg::OnTcnSelchangeTabProperties)
	ON_BN_CLICKED(IDOK, &CClauerControlPanelDlg::OnBnClickedOk)
END_MESSAGE_MAP()



BOOL CClauerControlPanelDlg::ShowTab ( int tab_num )
{
	TAB_CONTROL_PAGES pageToShow = (TAB_CONTROL_PAGES) 0;

	switch ( tab_num ) {
		case 0:
			// El tab cero --> P�gina de informaci�n
			pageToShow = INFO_PAGE;
			break;

		case 1:

			// P�gina de clupdate

			UPDATE_MODE modeDefault;
			if ( ! ((CPropUpdate *) m_pages[UPDATE_PAGE])->GetUpdateModeDefault(&modeDefault) ) {
				AfxMessageBox(GetClauerString(IDS_CTRL_PANEL_UPDATE_CANT_GET_UPDATING_MODE), MB_ICONERROR);
				PostQuitMessage(0);
				return FALSE;
			}
			if ( m_isAdmo ) 
				pageToShow = UPDATE_PAGE;
			else if ( modeDefault == MODE_AUTO && ! m_isAdmo ) {
				CPropNoAdmo *dlg = (CPropNoAdmo *) m_pages[PROP_NO_ADMO_PAGE];
				dlg->SetPageToShow(1);
				pageToShow = PROP_NO_ADMO_PAGE;
			} else if ( ! m_isAdmo )
				pageToShow = UPDATE_PAGE;

			break;

		case 2:

			// Conectividad

			if ( ! m_isAdmo ) {

				CPropNoAdmo *dlg = (CPropNoAdmo *) m_pages[PROP_NO_ADMO_PAGE];
				dlg->SetPageToShow(2);

				pageToShow = PROP_NO_ADMO_PAGE;
			} else 
				pageToShow = CONNECTIVITY_PAGE;

			break;

	}

	// Mostramos la p�gina

	CWnd *wnd = CWnd::FromHandle(m_tab.m_hWnd);
	CRect rect;
	wnd->GetWindowRect(&rect);
	ScreenToClient(&rect);

	for ( int i = 0 ; i < N_PAGES ; i++ ) {
		if ( i == pageToShow ) {
			m_pages[i]->SetWindowPos(wnd, rect.left+1, rect.top+21, rect.right-rect.left, rect.bottom-rect.top, SWP_NOSIZE|SWP_SHOWWINDOW);
			m_pages[i]->ShowWindow(WS_CHILD|SW_SHOW);			
			m_pages[i]->SetFocus();
		} else {
			m_pages[i]->SetWindowPos(wnd, rect.left, rect.top+25, rect.right-rect.left, rect.bottom-rect.top, SWP_NOSIZE|SWP_HIDEWINDOW);
			m_pages[i]->ShowWindow(WS_CHILD|SW_HIDE);
		}
	}

	return TRUE;
}

// Controladores de mensaje de CClauerControlPanelDlg

BOOL CClauerControlPanelDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	ZeroMemory(m_pages, N_PAGES * sizeof(CProp *));

	// Establecer el icono para este cuadro de di�logo. El marco de trabajo realiza esta operaci�n
	// autom�ticamente cuando la ventana principal de la aplicaci�n no es un cuadro de di�logo

	SetIcon(m_hIcon, TRUE);			// Establecer icono grande
	SetIcon(m_hIcon, FALSE);		// Establecer icono peque�o

	// Inicializamos el tab control con las p�ginas

	m_tab.InsertItem(0, GetClauerString(IDS_CTRL_PANEL_UPDATE_PAGE_GENERAL));
	m_tab.InsertItem(1, GetClauerString(IDS_CTRL_PANEL_UPDATE_PAGE_UPDATE));
	m_tab.InsertItem(2, _T("Conectividad"));

	// Creamos las ventanas de propiedades

	CWnd *wnd = CWnd::FromHandle(m_tab.m_hWnd);
	CRect rect;
	wnd->GetWindowRect(&rect);
	ScreenToClient(&rect);

	m_pages[UPDATE_PAGE] = new CPropUpdate(this,m_isAdmo);
	if ( ! m_pages[UPDATE_PAGE] ) {
		AfxMessageBox(GetClauerString(IDS_CTRL_PANEL_NO_MEMORY), MB_ICONERROR);
		PostQuitMessage(0);
		return FALSE;
	}
	m_pages[PROP_NO_ADMO_PAGE] = new CPropNoAdmo(this);
	if ( ! m_pages[PROP_NO_ADMO_PAGE] ) {
		delete m_pages[UPDATE_PAGE];
		AfxMessageBox(GetClauerString(IDS_CTRL_PANEL_NO_MEMORY), MB_ICONERROR);
		PostQuitMessage(0);
		return FALSE;
	}
	m_pages[INFO_PAGE] = new CPropInfo(this);
	if ( ! m_pages[INFO_PAGE] ) {
		delete m_pages[UPDATE_PAGE];
		delete m_pages[PROP_NO_ADMO_PAGE];
		AfxMessageBox(GetClauerString(IDS_CTRL_PANEL_NO_MEMORY), MB_ICONERROR);
		PostQuitMessage(0);
		return FALSE;
	}
	m_pages[CONNECTIVITY_PAGE] = new CPropConnectivity(this);
	if ( ! m_pages[CONNECTIVITY_PAGE] ) {
		delete m_pages[UPDATE_PAGE];
		delete m_pages[PROP_NO_ADMO_PAGE];
		delete m_pages[CONNECTIVITY_PAGE];
		AfxMessageBox(GetClauerString(IDS_CTRL_PANEL_NO_MEMORY), MB_ICONERROR);
		PostQuitMessage(0);
		return FALSE;
	}

	m_pages[UPDATE_PAGE]->Create(IDD_PROPERTY_UPDATE);
	m_pages[PROP_NO_ADMO_PAGE]->Create(IDD_PROPERTY_NO_ADMO);
	m_pages[INFO_PAGE]->Create(IDD_PROPERTY_INFO);
	m_pages[CONNECTIVITY_PAGE]->Create(IDD_PROPERTY_CONNECTIVITY);

	// Mostramos el tab correspondiente

	m_tab.SetCurSel(m_dwInitialPage);
	ShowTab(m_dwInitialPage);

	// Traducciones

	SetDlgItemText(IDOK, GetClauerString(IDS_CTRL_PANEL_OK));
	SetDlgItemText(IDCANCEL, GetClauerString(IDS_CTRL_PANEL_CANCEL));

	return FALSE;  // Devuelve TRUE  a menos que establezca el foco en un control
}

// Si agrega un bot�n Minimizar al cuadro de di�logo, necesitar� el siguiente c�digo
//  para dibujar el icono. Para aplicaciones MFC que utilicen el modelo de documentos y vistas,
//  esta operaci�n la realiza autom�ticamente el marco de trabajo.

void CClauerControlPanelDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // Contexto de dispositivo para dibujo

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Centrar icono en el rect�ngulo de cliente
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Dibujar el icono
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

// El sistema llama a esta funci�n para obtener el cursor que se muestra mientras el usuario arrastra
//  la ventana minimizada.
HCURSOR CClauerControlPanelDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}


void CClauerControlPanelDlg::OnTcnSelchangeTabProperties(NMHDR *pNMHDR, LRESULT *pResult)
{

	ShowTab(m_tab.GetCurSel());

#if 0
	// Elegimo la p�gina que toca para mostrar

	CWnd *wnd = CWnd::FromHandle(m_tab.m_hWnd);
	CRect rect;
	wnd->GetWindowRect(&rect);
	ScreenToClient(&rect);

	switch ( m_tab.GetCurSel() ) {
		case INFO_PAGE:
			m_propUpdate->SetWindowPos(wnd, rect.left+5, rect.top+25, rect.right-rect.left, rect.bottom-rect.top, SWP_NOSIZE|SWP_HIDEWINDOW);
			m_propNoAdmo->SetWindowPos(wnd, rect.left+5, rect.top+25, rect.right-rect.left, rect.bottom-rect.top, SWP_NOSIZE|SWP_HIDEWINDOW);
			m_propInfo->SetWindowPos(wnd, rect.left+5, rect.top+25, rect.right-rect.left, rect.bottom-rect.top, SWP_NOSIZE|SWP_SHOWWINDOW);
			m_propUpdate->ShowWindow(WS_CHILD|SW_HIDE);
			m_propNoAdmo->ShowWindow(WS_CHILD|SW_HIDE);
			m_propInfo->ShowWindow(WS_CHILD|SW_SHOW);
			break;

		case UPDATE_PAGE:
			m_propUpdate->SetWindowPos(wnd, rect.left+5, rect.top+25, rect.right-rect.left, rect.bottom-rect.top, SWP_NOSIZE|SWP_SHOWWINDOW);
			m_propNoAdmo->SetWindowPos(wnd, rect.left+5, rect.top+25, rect.right-rect.left, rect.bottom-rect.top, SWP_NOSIZE|SWP_HIDEWINDOW);
			m_propInfo->SetWindowPos(wnd, rect.left+5, rect.top+25, rect.right-rect.left, rect.bottom-rect.top, SWP_NOSIZE|SWP_HIDEWINDOW);
			m_propUpdate->ShowWindow(WS_CHILD|SW_SHOW);
			m_propNoAdmo->ShowWindow(WS_CHILD|SW_HIDE);
			m_propInfo->ShowWindow(WS_CHILD|SW_HIDE);

			break;
	}

	*pResult = 0;
#endif

}




void CClauerControlPanelDlg::OnBnClickedOk()
{
	if ( m_bSaveUpdate ) {

		for ( int i = 0 ; i < CClauerControlPanelDlg::N_PAGES ; i++ ) {
			if ( ! m_pages[i]->SaveValues() )
				AfxMessageBox(GetClauerString(IDS_CTRL_PANEL_UPDATE_ERROR_HAPPENED), MB_ICONERROR);
		}
	}

	OnOK();
}


