// PropUpdate.cpp : implementation file
//

#include "stdafx.h"
#include "ClauerControlPanel.h"
#include "PropUpdate.h"

#include "misc.h"


#include <windows.h>
#include <winsvc.h>
#include <Psapi.h>

#include <lang/Resource.h>
#include "lang.h"
#include "vista.h"



DWORD g_periods[] = { 1800000, 3600000, 43200000, 86400000, 0 };


// CPropUpdate dialog

IMPLEMENT_DYNAMIC(CPropUpdate, CDialog)

CPropUpdate::CPropUpdate(CWnd* pParent /*=NULL*/, BOOL isAdmo /*=FALSE*/)
	: CProp(CPropUpdate::IDD, pParent)
	, m_mode(0)
	, m_chkDefault(FALSE)
	, m_isAdmo(isAdmo)
	, m_cboView(0)
{

}

CPropUpdate::~CPropUpdate()
{
}



BOOL CPropUpdate::FillPeriodCombo ( void )
{
	CString aux;
	int sel;

	for ( int i = m_period.GetCount() ; i >= 0 ; i-- )
		m_period.DeleteString(i);
	m_period.Clear();

	switch ( m_mode ) {

	case MODE_NORMAL:
	case MODE_EXPERT:
	case MODE_AUTO:
	
		if ( m_period.AddString(GetClauerString(IDS_CTRL_PANEL_UPDATE_PERIOD_HALF_HOUR)) < 0 )
			return FALSE;
		if  ( m_period.AddString(GetClauerString(IDS_CTRL_PANEL_UPDATE_PERIOD_ONE_HOUR)) < 0 )
			return FALSE;;
		if ( m_period.AddString(GetClauerString(IDS_CTRL_PANEL_UPDATE_PERIOD_TWELVE_HOURS)) < 0 )
			return FALSE;;
		if ( m_period.AddString(GetClauerString(IDS_CTRL_PANEL_UPDATE_PERIOD_ONE_DAY)) < 0 )
			return FALSE;
		if ( m_period.AddString(GetClauerString(IDS_CTRL_PANEL_UPDATE_PERIOD_SESS_INI)) < 0 )
			return FALSE;

		sel = m_selViewData[m_cboView][1];
	
		break;

	case MODE_INI:

		if ( m_period.AddString(GetClauerString(IDS_CTRL_PANEL_UPDATE_PERIOD_SESS_INI)) < 0 )
			return FALSE;

		sel = 0;

		break;
	}

	m_period.SetCurSel(sel);
	UpdateData(FALSE);

	return TRUE;
}


void CPropUpdate::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Radio(pDX, IDC_RADIO_AUTO, m_mode);
	DDX_Control(pDX, IDC_COMBO_PERIOD, m_period);
	DDX_CBIndex(pDX, IDC_COMBO_VER, m_cboView);
}



BOOL CPropUpdate::OnInitDialog()
{
	CWnd *wnd;

	CDialog::OnInitDialog();

	// Obtenemos el modo de actualizaci�n

	if ( ! GetUpdateModeDefault(&m_defaultMode) ) {

		/* Salimos porque el modo de actualizaci�n puede estar
		 * establecido a Autom�tico y no lo sabemos
		 */		  
		AfxMessageBox(GetClauerString(IDS_CTRL_PANEL_UPDATE_CANT_GET_UPDATING_MODE), MB_ICONERROR);
		PostQuitMessage(0);
		return FALSE;
	}
	if ( m_defaultMode == MODE_AUTO )
		m_mode = MODE_AUTO;
	else
		if ( ! GetUpdateModeUser((UPDATE_MODE *) &m_mode) ) 
			m_mode = m_defaultMode;
	
	/* ATENCI�N. Si el modo es autom�tico seguro que soy adminsitrador. Ya me encargo
	 * desde fuera de hacer la comprobaci�n
	 */

	UpdateData(FALSE);

	// Relleno la combo de la vista

	CComboBox *cbo = ( CComboBox * ) GetDlgItem(IDC_COMBO_VER);
	if ( ! cbo ) {
		AfxMessageBox(GetClauerString(IDS_CTRL_PANEL_UPDATE_ERROR_VIEW), MB_ICONERROR);
		PostQuitMessage(0);
		return FALSE;
	}

	cbo->InsertString(0, GetClauerString(IDS_CTRL_PANEL_UPDATE_CURRENT_USER));
	cbo->InsertString(1, GetClauerString(IDS_CTRL_PANEL_UPDATE_DEFAULT));

	if ( m_defaultMode == MODE_AUTO ) {
		cbo->SetCurSel(1);
		cbo->EnableWindow(FALSE);
	} else 
		cbo->SetCurSel(0);
	
	if ( ! m_isAdmo )
		cbo->EnableWindow(FALSE);
	UpdateData();

	// Actualizamos los radio buttons
	OnBnClickedRadioAuto();

	// Inicializamos los modos de funcionamiento y per�odos de
	// comprobaci�n

	wnd = GetDlgItem(IDC_RADIO_AUTO);
	if ( wnd ) 
		if ( m_isAdmo && m_defaultMode == MODE_AUTO )
			wnd->EnableWindow();

	if ( ! FillPeriodCombo() ) {
		AfxMessageBox(GetClauerString(IDS_CTRL_PANEL_UPDATE_CANT_LOAD_PAGE), MB_ICONERROR);
		PostQuitMessage(0);
		return FALSE;
	}


	FillUpdatingPeriod();
	UpdateData(FALSE);

	// Ahora guardamos los valores de las vistas

	UPDATE_MODE updmode, updmodeUser;
	DWORD period, periodUser;
	
	if ( ! GetUpdateModeDefault(&updmode) ) {
		AfxMessageBox(GetClauerString(IDS_CTRL_PANEL_UPDATE_CANT_GET_UPDATING_MODE), MB_ICONERROR);
		PostQuitMessage(0);
		return FALSE;
	}
	if ( ! GetUpdatePeriodDefault(&period) ) 
		period = 1800000;

	m_selViewData[VIEW_DEFAULT][0] = updmode;
	if ( updmode != MODE_INI ) {
		if ( period == 0 )
			m_selViewData[VIEW_DEFAULT][1] = 4;
		else if ( period <= 1800000 )
			m_selViewData[VIEW_DEFAULT][1] = 0;
		else if ( period <= 3600000 )
			m_selViewData[VIEW_DEFAULT][1] = 1;
		else if ( period <= 43200000 )
			m_selViewData[VIEW_DEFAULT][1] = 2;
		else if ( period <= 86400000 )
			m_selViewData[VIEW_DEFAULT][1] = 3;
	} else
			m_selViewData[VIEW_DEFAULT][1] = 0;


	if ( ! GetUpdateModeUser(&updmodeUser) )
		updmodeUser = updmode;

	if ( ! GetUpdatePeriodUser(&periodUser) ) 
		periodUser = period;
	
	m_selViewData[VIEW_USER][0] = updmodeUser;
	if ( updmodeUser != MODE_INI ) {
		if ( periodUser == 0 )
			m_selViewData[VIEW_USER][1] = 4;
		else if ( periodUser <= 1800000 )
			m_selViewData[VIEW_USER][1] = 0;
		else if ( periodUser <= 3600000 )
			m_selViewData[VIEW_USER][1] = 1;
		else if ( periodUser <= 43200000 )
			m_selViewData[VIEW_USER][1] = 2;
		else if ( periodUser <= 86400000 )
			m_selViewData[VIEW_USER][1] = 3;
	} else 
		m_selViewData[VIEW_USER][1] = 0;

	// Si estamos en Windows Vista y no est� el proceso elevado
	// muestro el bot�n de elevaci�n

	BOOL bDrawElevateButton = FALSE;

	BOOL bIsVista;
	if ( ! clauer_vista_is_windows_vista(bIsVista) ) 
		bIsVista = FALSE;

	if ( bIsVista ) {

		BOOL bIsElevated;
		if ( ! clauer_vista_is_elevated(bIsElevated) )
			bIsElevated = TRUE;
		bDrawElevateButton = !bIsElevated;
	}

	wnd = GetDlgItem(BTN_UPDATE_ELEVATE);
	if ( bDrawElevateButton ) {
		if ( wnd )
			Button_SetElevationRequiredState(wnd->m_hWnd, TRUE);
		wnd->ShowWindow(SW_SHOW);
		wnd->SetWindowText(GetClauerString(IDS_CTRL_PANEL_NO_ADMO_EXEC_AS_ADMO));
	} else {
		wnd->ShowWindow(SW_HIDE);	
	}

	UpdateData(FALSE);

	// Traducciones

	SetDlgItemText(IDC_DESCRIPTION, GetClauerString(IDS_CTRL_PANEL_UPDATE_DESCRIPTION));
	SetDlgItemText(IDC_STATIC_CONFIG_UPDATE, GetClauerString(IDS_CTRL_PANEL_UPDATE_CONFIG_UPDATE));
	SetDlgItemText(IDC_LBL_VER, GetClauerString(IDS_CTRL_PANEL_UPDATE_LBL_VER));
	SetDlgItemText(IDC_STATIC_FUNC_MODE, GetClauerString(IDS_CTRL_PANEL_UPDATE_FUNC_MODE));
	SetDlgItemText(IDC_RADIO_AUTO, GetClauerString(IDS_CTRL_PANEL_UPDATE_RADIO_AUTO));
	SetDlgItemText(IDC_RADIO_INI, GetClauerString(IDS_CTRL_PANEL_UPDATE_RADIO_INI));
	SetDlgItemText(IDC_RADIO_NORMAL, GetClauerString(IDS_CTRL_PANEL_UPDATE_RADIO_NORMAL));
	SetDlgItemText(IDC_RADIO_EXPERT, GetClauerString(IDS_CTRL_PANEL_UPDATE_RADIO_EXPERT));
	SetDlgItemText(IDC_STATIC_DESC, GetClauerString(IDS_CTRL_PANEL_UPDATE_STATIC_DESC));
	SetDlgItemText(IDC_STATIC_PERIOD, GetClauerString(IDS_CTRL_PANEL_UPDATE_STATIC_PERIOD));
	SetDlgItemText(IDC_PERIOD, GetClauerString(IDS_CTRL_PANEL_UPDATE_PERIOD));

	return TRUE;
}



BEGIN_MESSAGE_MAP(CPropUpdate, CDialog)
	ON_BN_CLICKED(IDC_RADIO_AUTO, &CPropUpdate::OnBnClickedRadioAuto)
	ON_BN_CLICKED(IDC_RADIO_INI, &CPropUpdate::OnBnClickedRadioAuto)
	ON_BN_CLICKED(IDC_RADIO_NORMAL, &CPropUpdate::OnBnClickedRadioAuto)
	ON_BN_CLICKED(IDC_RADIO_EXPERT, &CPropUpdate::OnBnClickedRadioAuto)
	ON_CBN_SELCHANGE(IDC_COMBO_PERIOD, &CPropUpdate::OnCbnSelchangeComboPeriod)
	ON_CBN_SELCHANGE(IDC_COMBO_VER, &CPropUpdate::OnCbnSelchangeComboVer)
	ON_WM_SETFOCUS()
	ON_BN_CLICKED(BTN_UPDATE_ELEVATE, &CPropUpdate::OnBnClickedUpdateElevate)
END_MESSAGE_MAP()



void CPropUpdate::OnBnClickedRadioAuto()
{
	CWnd *wndDesc;
	CString aux;

	UpdateData();

	wndDesc = this->GetDlgItem(IDC_MODE_DESC);
	if ( wndDesc ) {

		switch ( m_mode ) {
		case MODE_AUTO:
			wndDesc->SetWindowText(GetClauerString(IDS_CTRL_PANEL_UPDATE_DESC_AUTO_MODE));
			if ( ! FillPeriodCombo() ) {
				AfxMessageBox(GetClauerString(IDS_CTRL_PANEL_UPDATE_CANT_LOAD_PAGE), MB_ICONERROR);
				PostQuitMessage(0);
				return;
			}

			break;
		case MODE_INI:
			wndDesc->SetWindowText(GetClauerString(IDS_CTRL_PANEL_UPDATE_DESC_INI_MODE));
			if ( ! FillPeriodCombo() ) {
				AfxMessageBox(GetClauerString(IDS_CTRL_PANEL_UPDATE_CANT_LOAD_PAGE), MB_ICONERROR);
				PostQuitMessage(0);
				return;
			}

			break;
		case MODE_NORMAL:
			wndDesc->SetWindowText(GetClauerString(IDS_CTRL_PANEL_UPDATE_DESC_NORMAL_MODE));
			if ( ! FillPeriodCombo() ) {
				AfxMessageBox(GetClauerString(IDS_CTRL_PANEL_UPDATE_CANT_LOAD_PAGE), MB_ICONERROR);
				PostQuitMessage(0);
				return;
			}

			break;
		case MODE_EXPERT:
			wndDesc->SetWindowText(GetClauerString(IDS_CTRL_PANEL_UPDATE_DESC_EXPERT_MODE));
			if ( ! FillPeriodCombo() ) {
				AfxMessageBox(GetClauerString(IDS_CTRL_PANEL_UPDATE_CANT_LOAD_PAGE), MB_ICONERROR);
				PostQuitMessage(0);
				return;
			}

			break;
		}
	}

	m_selViewData[m_cboView][0] = m_mode;

}




BOOL CPropUpdate::GetUpdateModeUser ( UPDATE_MODE *updMode )
{
	HKEY hKey;
	DWORD dwSize, dwType, dwUpdMode;

	if ( RegOpenKeyEx(HKEY_CURRENT_USER, _T("Software\\Universitat Jaume I\\Projecte Clauer\\Clupdate"), 0, KEY_READ, &hKey) != ERROR_SUCCESS )
		return FALSE;

	if ( RegQueryValueEx(hKey, _T("UPDATE_MODE"), 0, &dwType, NULL, &dwSize) != ERROR_SUCCESS ) {
		DWORD err = GetLastError();
		RegCloseKey(hKey);
		return FALSE;
	}

	if ( dwType != REG_DWORD ) {
		RegCloseKey(hKey);
		return FALSE;
	}
	if ( dwSize != sizeof(DWORD) ) {
		RegCloseKey(hKey);
		return FALSE;
	}

	if ( RegQueryValueEx(hKey, _T("UPDATE_MODE"), 0, &dwType, (LPBYTE) &dwUpdMode, &dwSize) != ERROR_SUCCESS ) {
		RegCloseKey(hKey);
		return FALSE;
	}

	RegCloseKey(hKey);

	switch ( dwUpdMode ) {
	case 0:
		*updMode = MODE_NORMAL;
		break;
	case 1:
		*updMode = MODE_EXPERT;
		break;
	case 2:
		*updMode = MODE_INI;
		break;
	default:
		return FALSE;
	}

	return TRUE;
}




BOOL CPropUpdate::GetUpdateModeDefault  ( UPDATE_MODE *updMode )
{
	HKEY hKey;
	DWORD dwSize, dwType, dwUpdMode;

	if ( RegOpenKeyEx(HKEY_LOCAL_MACHINE, _T("Software\\Universitat Jaume I\\Projecte Clauer"), 0, KEY_READ, &hKey) != ERROR_SUCCESS ) 
			return FALSE;

	if ( RegQueryValueEx(hKey, _T("UPDATE_MODE"), 0, &dwType, NULL, &dwSize) != ERROR_SUCCESS ) {
		RegCloseKey(hKey);
		return FALSE;
	}

	if ( dwType != REG_DWORD ) {
		RegCloseKey(hKey);
		return FALSE;
	}
	if ( dwSize != sizeof(DWORD) ) {
		RegCloseKey(hKey);
		return FALSE;
	}

	if ( RegQueryValueEx(hKey, _T("UPDATE_MODE"), 0, &dwType, (LPBYTE) &dwUpdMode, &dwSize) != ERROR_SUCCESS ) {
		RegCloseKey(hKey);
		return FALSE;
	}

	RegCloseKey(hKey);

	switch ( dwUpdMode ) {
	case 0:
		*updMode = MODE_NORMAL;
		break;
	case 1:
		*updMode = MODE_EXPERT;
		break;
	case 2:
		*updMode = MODE_INI;
		break;
	case 3:
		*updMode = MODE_AUTO;
		break;
	default:
		return FALSE;
	}


	return TRUE;
}


BOOL CPropUpdate::SetUpdateModeUser ( UPDATE_MODE updMode )
{
	HKEY hKey;
	DWORD dwUpdMode;

	switch ( updMode) {
	case MODE_NORMAL:
		dwUpdMode = 0;
		break;
	case MODE_EXPERT:
		dwUpdMode = 1;
		break;
	case MODE_INI:
		dwUpdMode = 2;
		break;
	case MODE_AUTO:
		dwUpdMode = 3;
		break;
	default:
		return FALSE;
	}

	if ( RegOpenKeyEx(HKEY_CURRENT_USER, _T("Software\\Universitat Jaume I\\Projecte Clauer\\Clupdate"), 0, KEY_ALL_ACCESS, &hKey) != ERROR_SUCCESS ) 
		return FALSE;
	if ( RegSetValueEx(hKey, _T("UPDATE_MODE"), 0, REG_DWORD, (LPBYTE) &dwUpdMode, sizeof(DWORD)) != ERROR_SUCCESS ) {
		RegCloseKey(hKey);
		return FALSE;
	}
	RegCloseKey(hKey);

	return TRUE;
}


BOOL CPropUpdate::SetUpdateModeDefault ( UPDATE_MODE updMode )
{
	HKEY hKey;
	DWORD dwUpdMode;

	switch ( updMode) {
	case MODE_NORMAL:
		dwUpdMode = 0;
		break;
	case MODE_EXPERT:
		dwUpdMode = 1;
		break;
	case MODE_INI:
		dwUpdMode = 2;
		break;
	case MODE_AUTO:
		dwUpdMode = 3;
		break;
	default:
		return FALSE;
	}

	if ( ! m_isAdmo )
		return FALSE;

	if ( RegOpenKeyEx(HKEY_LOCAL_MACHINE, _T("Software\\Universitat Jaume I\\Projecte Clauer"), 0, KEY_ALL_ACCESS, &hKey) != ERROR_SUCCESS ) 
		return FALSE;

	if ( RegSetValueEx(hKey, _T("UPDATE_MODE"), 0, REG_DWORD, (LPBYTE) &dwUpdMode, sizeof(DWORD)) != ERROR_SUCCESS ) {
		RegCloseKey(hKey);
		return FALSE;
	}

	RegCloseKey(hKey);

	return TRUE;
}




BOOL GetProcessName ( DWORD id, TCHAR szProcessName[MAX_PATH] ) 
{

    _tcscpy(szProcessName, _T("<unknown>"));

    // Get a handle to the process.

    HANDLE hProcess = OpenProcess( PROCESS_QUERY_INFORMATION |
                                   PROCESS_VM_READ,
                                   FALSE, id );
    // Get the process name.

    if ( hProcess )
    {
        HMODULE hMod;
        DWORD cbNeeded;

        if ( EnumProcessModules( hProcess, &hMod, sizeof(hMod), 
             &cbNeeded) )
        {
            GetModuleBaseName( hProcess, hMod, szProcessName, MAX_PATH );
        } 
	
    } 


    // Print the process name and identifier.

    CloseHandle( hProcess );

	return TRUE;
}



/* Este m�todo se encarga de guardar la configuraci�n para los valores
 * que hemos elegido
 */

BOOL CPropUpdate::SaveValues ( void )
{
	BOOL bStopService = FALSE, bRestartUpdate = FALSE;
	UPDATE_MODE m, mUser;

	/* Actualizamos los valores del registro:
	 *
	 *      . Si es usuario entonces establecemos los valores para �l
	 *      . Si es administrador entonces
	 *              si valores por defecto --> valores por defecto
	 *              sino ---> valores para el usuario
	 */

	UpdateData();
	
	if ( ! GetUpdateModeDefault(&m) ) 
		return FALSE;

	/* Si pasamos de modo autom�tico a un modo no autom�tico, debemos parar el
	 * servicio
	 */

	if ( m == MODE_AUTO && m_selViewData[VIEW_DEFAULT][0] != MODE_AUTO )
		bStopService = TRUE;

	if ( GetUpdateModeUser(&mUser) ) 
		m = mUser;
		
	if ( (m != MODE_AUTO) && (m != m_selViewData[VIEW_USER][0]) )
		bRestartUpdate = TRUE;		
	
	/* Almacenamos los valores en el registro
	 */

	if ( m_isAdmo ) {
		if ( ! SetUpdateModeDefault((UPDATE_MODE) m_selViewData[VIEW_DEFAULT][0]) )
			return FALSE;
		if ( ! SetUpdatePeriodDefault(g_periods[m_selViewData[VIEW_DEFAULT][1]]) )
			return FALSE;
	}
	if ( ! SetUpdateModeUser((UPDATE_MODE) m_selViewData[VIEW_USER][0]) )
		return FALSE;
	if ( ! SetUpdatePeriodUser(g_periods[m_selViewData[VIEW_USER][1]]) )
		return FALSE;

	/* Reiniciamos el servicio si se seleccion� el modo autom�tico
	 */

 	if ( m_selViewData[VIEW_DEFAULT][0] == MODE_AUTO ) {

		if ( ! KillClupdate() ) {
			AfxMessageBox(GetClauerString(IDS_CTRL_PANEL_UPDATE_CANT_KILL_CLUPDATE), MB_ICONERROR);
			return FALSE;
		}

		/* Vamos con el servicio
		 */

		SC_HANDLE hSCM, hService;
		SERVICE_STATUS status;

		hSCM = OpenSCManager(NULL, NULL, SC_MANAGER_ALL_ACCESS);
		if ( ! hSCM ) {
			CloseServiceHandle(hSCM);
			AfxMessageBox(GetClauerString(IDS_CTRL_PANEL_UPDATE_CANT_INIT_SERVICE), MB_ICONERROR);
			return FALSE;
		}
		hService = OpenService(hSCM, _T("ClupdateSrv"), SERVICE_ALL_ACCESS);
		if ( ! hService ) {
			CloseServiceHandle(hSCM);
			AfxMessageBox(GetClauerString(IDS_CTRL_PANEL_UPDATE_CANT_CONNECT_TO_SERVICE), MB_ICONERROR);
			return FALSE;
		}
		ControlService(hService, SERVICE_CONTROL_STOP, &status);
		if ( ! StartService(hService, 0, NULL) ) {
			CloseServiceHandle(hService);
			CloseServiceHandle(hSCM);
			AfxMessageBox(GetClauerString(IDS_CTRL_PANEL_UPDATE_CANT_RESTART_SERVICE), MB_ICONERROR);
			return FALSE;
		}
		CloseServiceHandle(hService);
		CloseServiceHandle(hSCM);

	} else if ( bStopService ) {

		SC_HANDLE hSCM, hService;
		SERVICE_STATUS status;
		BOOL bErr;

		hSCM = OpenSCManager(NULL, NULL, SC_MANAGER_ALL_ACCESS);
		if ( ! hSCM ) {
			CloseServiceHandle(hSCM);
			AfxMessageBox(GetClauerString(IDS_CTRL_PANEL_UPDATE_CANT_INIT_SERVICE), MB_ICONERROR);
			return FALSE;
		}
		hService = OpenService(hSCM, _T("ClupdateSrv"), SERVICE_ALL_ACCESS);
		if ( ! hService ) {
			CloseServiceHandle(hSCM);
			AfxMessageBox(GetClauerString(IDS_CTRL_PANEL_UPDATE_CANT_CONNECT_TO_SERVICE), MB_ICONERROR);
			return FALSE;
		}
		bErr = ControlService(hService, SERVICE_CONTROL_STOP, &status);
		if ( status.dwCurrentState != SERVICE_STOPPED ) {
			CloseServiceHandle(hService);
			CloseServiceHandle(hSCM);
			AfxMessageBox(GetClauerString(IDS_CTRL_PANEL_UPDATE_CANT_RESTART_SERVICE), MB_ICONERROR);
			return FALSE;
		}
		CloseServiceHandle(hService);
		CloseServiceHandle(hSCM);

		HKEY hKey;
		DWORD dwType, dwSize;
		TCHAR szClupdate[MAX_PATH+13];
		if ( RegOpenKeyEx(HKEY_LOCAL_MACHINE, _T("Software\\Universitat Jaume I\\Projecte Clauer"),0,KEY_READ,&hKey) != ERROR_SUCCESS ) {
			AfxMessageBox(GetClauerString(IDS_CTRL_PANEL_UPDATE_CANT_GET_CLUPDATE_PATH), MB_ICONERROR|MB_OK);
			return FALSE;
		}
		if ( RegQueryValueEx(hKey, _T("INSDIR"), NULL, &dwType, NULL, &dwSize) != ERROR_SUCCESS ) {
			RegCloseKey(hKey);
			AfxMessageBox(GetClauerString(IDS_CTRL_PANEL_UPDATE_CANT_GET_CLUPDATE_PATH), MB_ICONERROR|MB_OK);
			return FALSE;
		}
		if ( dwType != REG_SZ || dwSize > MAX_PATH ) {
			RegCloseKey(hKey);
			AfxMessageBox(GetClauerString(IDS_CTRL_PANEL_UPDATE_CANT_GET_CLUPDATE_PATH), MB_ICONERROR|MB_OK);
			return FALSE;
		}
		if ( RegQueryValueEx(hKey, _T("INSDIR"), NULL, &dwType, (LPBYTE) szClupdate, &dwSize) != ERROR_SUCCESS ) {
			RegCloseKey(hKey);
			AfxMessageBox(GetClauerString(IDS_CTRL_PANEL_UPDATE_CANT_GET_CLUPDATE_PATH), MB_ICONERROR|MB_OK);
			return FALSE;
		}
		RegCloseKey(hKey);

		_tcscat(szClupdate, _T("\\ClUpdate.exe"));

		if ( (int)ShellExecute(NULL, _T("open"), szClupdate, _T("/ini"), NULL, SW_SHOW) <= 32 ) {
			AfxMessageBox(GetClauerString(IDS_CTRL_PANEL_UPDATE_CANT_INIT_CLUPDATE), MB_ICONERROR);
			return FALSE;
		}

	} else if ( bRestartUpdate ) {

		if ( ! KillClupdate() ) {
			AfxMessageBox(GetClauerString(IDS_CTRL_PANEL_UPDATE_CANT_KILL_CLUPDATE), MB_ICONERROR);
			return FALSE;
		}

		HKEY hKey;
		DWORD dwType, dwSize;
		TCHAR szClupdate[MAX_PATH+13];

		if ( RegOpenKeyEx(HKEY_LOCAL_MACHINE, _T("Software\\Universitat Jaume I\\Projecte Clauer"),0,KEY_READ,&hKey) != ERROR_SUCCESS ) {
			AfxMessageBox(GetClauerString(IDS_CTRL_PANEL_UPDATE_CANT_GET_CLUPDATE_PATH), MB_ICONERROR|MB_OK);
			return FALSE;
		}
		if ( RegQueryValueEx(hKey, _T("INSDIR"), NULL, &dwType, NULL, &dwSize) != ERROR_SUCCESS ) {
			RegCloseKey(hKey);
			AfxMessageBox(GetClauerString(IDS_CTRL_PANEL_UPDATE_CANT_GET_CLUPDATE_PATH), MB_ICONERROR|MB_OK);
			return FALSE;
		}
		if ( dwType != REG_SZ || dwSize > MAX_PATH ) {
			RegCloseKey(hKey);
			AfxMessageBox(GetClauerString(IDS_CTRL_PANEL_UPDATE_CANT_GET_CLUPDATE_PATH), MB_ICONERROR|MB_OK);
			return FALSE;
		}
		if ( RegQueryValueEx(hKey, _T("INSDIR"), NULL, &dwType, (LPBYTE) szClupdate, &dwSize) != ERROR_SUCCESS ) {
			RegCloseKey(hKey);
			AfxMessageBox(GetClauerString(IDS_CTRL_PANEL_UPDATE_CANT_GET_CLUPDATE_PATH), MB_ICONERROR|MB_OK);
			return FALSE;
		}
		RegCloseKey(hKey);
		_tcscat(szClupdate, _T("\\Clupdate.exe"));
		if ( (int)ShellExecute(NULL, _T("open"), szClupdate, _T("/ini"), NULL, SW_SHOW) <= 32 ) {
			AfxMessageBox(GetClauerString(IDS_CTRL_PANEL_UPDATE_CANT_INIT_CLUPDATE), MB_ICONERROR);
			return FALSE;
		}
	}

	return TRUE;
}





BOOL CPropUpdate::SetUpdatePeriodUser ( DWORD updPeriod )
{
	HKEY hKey;

	if ( RegOpenKeyEx(HKEY_CURRENT_USER, _T("Software\\Universitat Jaume I\\Projecte Clauer\\Clupdate"), 0, KEY_ALL_ACCESS, &hKey) != ERROR_SUCCESS ) 
		return FALSE;
	if ( RegSetValueEx(hKey, _T("PERIOD"), 0, REG_DWORD, (LPBYTE) &updPeriod, sizeof(DWORD)) != ERROR_SUCCESS ) {
		RegCloseKey(hKey);
		return FALSE;
	}
	RegCloseKey(hKey);

	return TRUE;
}


BOOL CPropUpdate::SetUpdatePeriodDefault ( DWORD updPeriod )
{
	HKEY hKey;

	if ( RegOpenKeyEx(HKEY_LOCAL_MACHINE, _T("Software\\Universitat Jaume I\\Projecte Clauer"), 0, KEY_ALL_ACCESS, &hKey) != ERROR_SUCCESS ) 
		return FALSE;
	if ( RegSetValueEx(hKey, _T("PERIOD"), 0, REG_DWORD, (LPBYTE) &updPeriod, sizeof(DWORD) ) != ERROR_SUCCESS ) {
		RegCloseKey(hKey);
		return FALSE;
	}
	RegCloseKey(hKey);

	return TRUE;
}


BOOL CPropUpdate::GetUpdatePeriodUser ( DWORD *updPeriod )
{
	HKEY hKey;
	DWORD dwType, dwSize;

	if ( ! updPeriod )
		return FALSE;

	if ( RegOpenKeyEx(HKEY_CURRENT_USER, _T("Software\\Universitat Jaume I\\Projecte Clauer\\Clupdate"), 0, KEY_READ, &hKey) != ERROR_SUCCESS ) 
		return FALSE;
	if ( RegQueryValueEx(hKey, _T("PERIOD"), NULL, &dwType, NULL, &dwSize) != ERROR_SUCCESS ) {
		RegCloseKey(hKey);
		return FALSE;
	}
	if ( dwType != REG_DWORD || dwSize != sizeof(DWORD) ) {
		RegCloseKey(hKey);
		return FALSE;
	}
	if ( RegQueryValueEx(hKey, _T("PERIOD"), NULL, &dwType, (LPBYTE) updPeriod, &dwSize) != ERROR_SUCCESS ) {
		RegCloseKey(hKey);
		return FALSE;
	}
	RegCloseKey(hKey);

	return TRUE;
}





BOOL CPropUpdate::GetUpdatePeriodDefault ( DWORD *updPeriod )
{
	HKEY hKey;
	DWORD dwType, dwSize;

	if ( ! updPeriod )
		return FALSE;

	if ( RegOpenKeyEx(HKEY_LOCAL_MACHINE, _T("Software\\Universitat Jaume I\\Projecte Clauer"), 0, KEY_READ, &hKey) != ERROR_SUCCESS ) 
		return FALSE;
	if ( RegQueryValueEx(hKey, _T("PERIOD"), NULL, &dwType, NULL, &dwSize) != ERROR_SUCCESS ) {
		RegCloseKey(hKey);
		return FALSE;
	}
	if ( dwType != REG_DWORD || dwSize != sizeof(DWORD) ) {
		RegCloseKey(hKey);
		return FALSE;
	}
	if ( RegQueryValueEx(hKey, _T("PERIOD"), NULL, &dwType, (LPBYTE) updPeriod, &dwSize) != ERROR_SUCCESS ) {
		RegCloseKey(hKey);
		return FALSE;
	}
	RegCloseKey(hKey);

	return TRUE;
}



/* 
 * Rellena el per�odo de actualizaci�n
 */

BOOL CPropUpdate::FillUpdatingPeriod ( void )
{
	/* Miramos el per�odo de actualizaci�n establecido y lo seleccionamos
	 * en la combo
	 */

	DWORD dwPeriod;
	BOOL err = FALSE;

	if ( m_defaultMode == MODE_AUTO ) {

		/* En modo autom�tico, �nicamente extraemos el per�odo de actualizaci�n
		 * por defecto
		 */

		if ( GetUpdatePeriodDefault(&dwPeriod) ) {
				
			if ( dwPeriod == 0 )
				m_period.SetCurSel(4);
			else if ( dwPeriod <= 1800000 )   /* 30 minutes */
				m_period.SetCurSel(0);
			else if ( dwPeriod <= 3600000 )   /* 1 hour */
				m_period.SetCurSel(1);
			else if ( dwPeriod <= 43200000 )  /* 12 hours */
				m_period.SetCurSel(2);
			else if ( dwPeriod <= 86400000 )  /* 24 hours */
				m_period.SetCurSel(3);

		}

	} else {

		/* En otros modos tomamos el per�odo de actualizaci�n del usuario y, en su defecto
		 * el de por defecto
		 */

		if ( m_mode != MODE_INI ) {
			if ( ! GetUpdatePeriodUser(&dwPeriod) ) 
				err = GetUpdatePeriodDefault(&dwPeriod);
			
			if ( ! err ) {
				if ( dwPeriod == 0 )
					m_period.SetCurSel(4);
				else if ( dwPeriod <= 1800000 )   /* 30 minutes */
					m_period.SetCurSel(0);
				else if ( dwPeriod <= 3600000 )   /* 1 hour */
					m_period.SetCurSel(1);
				else if ( dwPeriod <= 43200000 )  /* 12 hours */
					m_period.SetCurSel(2);
				else if ( dwPeriod <= 86400000 )  /* 24 hours */
					m_period.SetCurSel(3);
			}
		}
	}

	UpdateData();

	return TRUE;
}


void CPropUpdate::OnCbnSelchangeComboPeriod()
{
	UpdateData();
	m_selViewData[m_cboView][1] = m_period.GetCurSel();
}

void CPropUpdate::OnCbnSelchangeComboVer()
{
	
	UpdateData();

	// El cambio de implica seleccionar correctamente los valores establecidos
	// en las vistas

	m_mode = m_selViewData[m_cboView][0];
	UpdateData(FALSE);
	OnBnClickedRadioAuto();
	FillPeriodCombo();
	m_period.SetCurSel(m_selViewData[m_cboView][1]);
	UpdateData(FALSE);

	CWnd *wnd = GetDlgItem(IDC_RADIO_AUTO);
	if ( m_cboView == VIEW_DEFAULT ) {
		if ( wnd )
			wnd->EnableWindow(TRUE);
	} else {
		wnd->EnableWindow(FALSE);
	}
}

void CPropUpdate::OnSetFocus(CWnd* pOldWnd)
{
	CDialog::OnSetFocus(pOldWnd);

}

void CPropUpdate::OnBnClickedRadioProxyDirect()
{

}

void CPropUpdate::OnBnClickedUpdateElevate()
{
	BOOL bCanceled;
	CString params;
	
	params.Format(_T("/page %d"), 1);
	if ( clauer_vista_launch_elevated(bCanceled, (LPTSTR) params.GetString()) )
		if ( ! bCanceled )
			PostQuitMessage(0);

}

/* Mata los clupdate que est�n en ejecuci�n
 */

BOOL CPropUpdate::KillClupdate ( void )
{
	DWORD aProcesses[5000];
	DWORD dwSize, dwNum;
	TCHAR name[MAX_PATH];
	HANDLE hProcess;

	if ( ! EnumProcesses(aProcesses, sizeof aProcesses, &dwSize) )
		return FALSE;

	dwNum = dwSize / sizeof(DWORD);
	for ( DWORD i = 0 ; i < dwNum ; i++ ) {

		GetProcessName(aProcesses[i], name);

		for ( int j = 0, size = (int) _tcslen(name) ; j < size ; j++ )
			name[j] = _totlower(name[j]);

		if ( _tcscmp(name, _T("clupdate.exe")) == 0 ) {
			if ( ! (hProcess = OpenProcess(PROCESS_TERMINATE, FALSE, aProcesses[i])) ) {
				CloseHandle(hProcess);
				return FALSE;
			}
			if ( ! TerminateProcess(hProcess, 0) ) {
				CloseHandle(hProcess);
				return FALSE;
			}
			CloseHandle(hProcess);
		}
	}

	return TRUE;

}
