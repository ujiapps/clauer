#pragma once
#include "afxwin.h"

#include "Prop.h"

enum UPDATE_MODE { MODE_AUTO, MODE_INI, MODE_NORMAL, MODE_EXPERT };
enum UPDATE_VIEW { VIEW_USER, VIEW_DEFAULT };

// CPropUpdate dialog

class CPropUpdate : public CProp
{
	DECLARE_DYNAMIC(CPropUpdate)

public:
	BOOL SaveValues			( void );
	BOOL FillUpdatingPeriod ( void );

	BOOL GetSelectedUpdateMode   ( UPDATE_MODE *updMode );
	BOOL GetSelectedUpdatePeriod ( DWORD *dwPeriod );

	BOOL GetUpdateModeUser     ( UPDATE_MODE *updMode );
	BOOL GetUpdateModeDefault  ( UPDATE_MODE *updMode );

	BOOL SetUpdateModeUser     ( UPDATE_MODE updMode );
	BOOL SetUpdateModeDefault  ( UPDATE_MODE updMode );

	BOOL SetUpdatePeriodUser    ( DWORD updPeriod );
	BOOL SetUpdatePeriodDefault ( DWORD updPeriod );
	BOOL GetUpdatePeriodUser    ( DWORD *updPeriod );
	BOOL GetUpdatePeriodDefault ( DWORD *updPeriod );

public:
	CPropUpdate(CWnd* pParent = NULL, BOOL isAdmo = FALSE);   // standard constructor
	virtual ~CPropUpdate();

// Dialog Data
	enum { IDD = IDD_PROPERTY_UPDATE };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL OnInitDialog();

	BOOL FillPeriodCombo ( void );
	BOOL KillClupdate ( void );

	// Indica si el usuario pertenece al grupo administradores o no
	BOOL m_isAdmo;

	// Almacena los valores seleccionados para cada vista:
	//
	//          m_selViewData[0] : Vista de usuario
	//          m_selViewData[1] : Vista por defecto
	//
	// La componente 0 almacena el modo de funcionamiento, la componente 1 almacena
	// el per�odo seleccionado. Por ejemplo:
	//
	//      m_selViewData[0][1] obtiene el per�odo seleccionado en la vista de usuario

	int m_selViewData[2][2];

	DECLARE_MESSAGE_MAP()
public:
	// The mode currently selected
	int m_mode;

	// The default mode
	UPDATE_MODE m_defaultMode;
public:
	afx_msg void OnBnClickedRadioAuto();
public:
	// Combo para indicar el per�odo de actualizaci�n
	CComboBox m_period;
public:
	// Indica si los valores que se toman tienen que ser o no los valores por defecto
	BOOL m_chkDefault;
public:
	// Vista de valores seleccionada
	int m_cboView;
public:
	afx_msg void OnCbnSelchangeComboPeriod();
public:
	afx_msg void OnCbnSelchangeComboVer();
public:
	afx_msg void OnSetFocus(CWnd* pOldWnd);
public:
	afx_msg void OnBnClickedRadioProxyDirect();
public:
	afx_msg void OnBnClickedUpdateElevate();
};
