#pragma hdrstop

#define _WIN32_WINNT 0x0500
#define WINVER		 0x0500

#include <libFormat/usb_format.h>
#include <CAPI_PKCS5/capi_pkcs5.h>

/* Macros */

#define BLOQUE_ES_VACIO(b)		( (*b >= 0) && (*b <= 84) )
#define BLOQUE_ES_CLARO(b)		( (*b >= 85) && (*b <= 169) )
#define BLOQUE_ES_CIFRADO(b)	( (*b >= 170) && (*b <= 254) )

/* Variables globales */

static HCRYPTPROV ghProv = 0;
static HCRYPTKEY ghKey = 0;


/* Devuelve ERR_FORMAT_SI si ha ocurrido alg�n error obteniendo el id del dispositivo */

int get_id (IN HANDLE_DISPOSITIVO handle, OUT BYTE id[20], int dispositivo)
{

	BYTE bloque[TAM_BLOQUE];
	int winver, bytesSector;


	bytesSector= lowlevel_obtenerBytesSector(dispositivo);
	if ( bytesSector < 0 ){
		return ERR_FORMAT_SI;
	}

    /* Obtenemos la versi�n del Windows */
    winver = lowlevel_get_winversion();

    switch (winver) {

        case WINXP:

					if ( lowlevel_leer_buffer(handle, bloque, 0, TAM_BLOQUE/bytesSector, dispositivo) != ERR_LOWLEVEL_NO ) 
						return ERR_FORMAT_SI;

					memcpy( id, bloque+40, 20 );

					break;

        default:        return ERR_FORMAT_SIST_OPERATIVO_INCORRECTO;

    }

	return ERR_FORMAT_NO;

}

//---------------------------------------------------------------------------
/*! \brief Inicializa la librer�a de formateo para realizar operaciones
		   criptogr�ficas

			Esta inicializaci�n resulta necesaria de manera previa a cualquier
			uso de funciones de cifrado, como por ejemplo, la llamada a la funci�n
			format_crear_zona_cripto.
   
   \param handle
			Manejador del dispositivo que se obtiene al abrir el mismo

   \param password
			Es la password que protege la zona criptogr�fica

   \param salt
			Es el salt obtenido del dispositivo

   \retval ERR_FORMAT_NO
             La funci�n se ejecut� correctamente.

   \retval ERR_FORMAT_SI
			 Ha ocurrido un error inicializando la librer�a de formateo

   \retval ERR_FORMAT_PKCS5
			 Ha ocurrido un error al intentar inicializar el PKCS#5

   \retval ERR_FORMAT_SIST_OPERATIVO_INCORRECTO
             Se est� intentando ejecutar la librer�a en un sistema operativo no soportado
*/

int format_ini ( IN HANDLE_DISPOSITIVO handle, IN PASSWORD password, IN BYTE salt [20] ) {

	int winver;

	
    /* Obtenemos la versi�n del Windows */
    winver = lowlevel_get_winversion();

    switch (winver) {

        case WINXP:
					/* Inicializamos el PKCS#5 */
					
					if (! CAPI_PKCS5_3DES_PBE_Init( password, salt, 20, 1000, &ghProv, &ghKey ) )
						return ERR_FORMAT_PKCS5;

					break;

        default:        return ERR_FORMAT_SIST_OPERATIVO_INCORRECTO;

    }

	return ERR_FORMAT_NO;

}

//---------------------------------------------------------------------------
/*! \brief Crea una copia de seguridad de la estructura de directorios contenida
           en la unidad especificada

   Guarda en un directorio temporal del disco duro una copia de seguridad de la
   estructura de directorios y ficheros contenidos en la unidad especificada.

   Los atributos originales de los ficheros se conservan. La copia se almacena
   en el directorio que se devuelve mediante el par�metro ruta_directorio_destino

   \param handle
			Manejador del dispositivo que se obtiene al abrir el mismo

   \param password
			Es la password que protege la zona criptogr�fica. Si se especifica
			(diferente de NULL), se asociar� la copia de seguridad con el
			identificador del dispositivo

   \param unidad
            Unidad de la que se quiere realizar la copia de seguridad (ej: "D:", "E:", ...)

   \param ruta_directorio_destino
            Cadena que indica la ruta del directorio temporal en el que se ha hecho
            la copia de seguridad

   \retval ERR_FORMAT_NO
             La funci�n se ejecut� correctamente.

   \retval ERR_FORMAT_CREANDO_BACKUP
             Ha ocurrido un error durante la ejecuci�n del comando que realiza
             la copia de seguridad.

   \retval ERR_FORMAT_SIST_OPERATIVO_INCORRECTO
             Se est� intentando ejecutar la librer�a en un sistema operativo no soportado
*/

int format_crear_backup_datos (IN HANDLE_DISPOSITIVO handle, IN PASSWORD password, IN char* unidad, OUT char* ruta_directorio_destino, IN int dispositivo) {

	int winver;
    STARTUPINFO si;
    PROCESS_INFORMATION pi;
    char * dir, *buf_temp, *dir_temp, *comando;
    unsigned int num=0;
	BYTE id [20];
	int result;

    /* Obtenemos la versi�n del Windows */
    winver = lowlevel_get_winversion();

    switch (winver) {

                case WINXP:

						if ( password == NULL ) {	/* No asociamos ning�n id a la copia de seguridad */

							/* Obtenemos un directorio temporal para la copia de seguridad, en el disco duro */

							dir = (char *) malloc (255*sizeof(char));

							strcpy(dir, "C:\\TEMPUSB");

							/* Comprobaci�n de que el directorio no exista */

							dir_temp = (char *) malloc(255*sizeof(char));
							strcpy(dir_temp,dir);

							while (_chdir(dir_temp)==0) {  /* Mientras el directorio exista */

                                num+=1;

                                strcpy(dir_temp,dir);

                                buf_temp = (char *) malloc (50*sizeof(char));

                                sprintf(buf_temp,"%d",num);
                                strcat(dir_temp,buf_temp);

                                free(buf_temp);
							}

							strcpy(dir,dir_temp);
							free(dir_temp);

						} else {

							/* El nombre del directorio ser� el id del dispositivo pasado a hexadecimal */

							result = get_id( handle, id, dispositivo );
							if ( result != ERR_FORMAT_NO ) return ERR_FORMAT_CREANDO_BACKUP;

							/* SEGUIR AKIIII */

						}

                        /* Proceso para la copia de archivos mediante xcopy */

                        ZeroMemory( &si, sizeof(si) );
                        si.cb = sizeof(si);
                        ZeroMemory( &pi, sizeof(pi) );

                        comando = (char *) malloc (1024*sizeof(char));

                        strcpy(comando, "xcopy ");
                        strcat(comando, unidad);
                        strcat(comando, "\\* ");
                        strcat(comando, dir);
                        strcat(comando, " /E /C /F /H /Y /I /K");

                        if( !CreateProcess( NULL,                       // No module name (use command line).
                        comando,                                        // Command line.
                        NULL,                                           // Process handle not inheritable.
                        NULL,											// Thread handle not inheritable.
                        FALSE,                                          // Set handle inheritance to FALSE.
                        CREATE_NO_WINDOW,                               // No creation flags.
                        NULL,                                           // Use parent's environment block.
                        NULL,                                           // Use parent's starting directory.
                        &si,                                            // Pointer to STARTUPINFO structure.
                        &pi )                                           // Pointer to PROCESS_INFORMATION structure
                        )

                        return ERR_FORMAT_CREANDO_BACKUP;

                        strcpy(ruta_directorio_destino,dir);

                        free(dir);
                        free(comando);

                        // Esperamos hasta que el proceso finalice
                        WaitForSingleObject( pi.hProcess, INFINITE );

                        // Cerramos los manejadores process y thread
                        CloseHandle( pi.hProcess );
                        CloseHandle( pi.hThread );

                        break;

                default:
                        return ERR_FORMAT_SIST_OPERATIVO_INCORRECTO;

    }

    return ERR_FORMAT_NO;

}

//---------------------------------------------------------------------------
/*! \brief Obtiene informaci�n acerca de la geometr�a del dispositivo

        Obtiene la informaci�n CHS del dispositivo (n�mero de cabezas, sectores y
        cilindros).

   \param handle
            Manejador para acceder al dispositivo

   \param heads
            N�mero de cabezas del dispositivo

   \param sectors
            N�mero total de sectores del dispositivo

   \param cylinders
            N�mero de cilindros del dispositivo

   \retval ERR_FORMAT_NO
             La funci�n se ejecut� correctamente

   \retval ERR_FORMAT_SI
             Ha ocurrido alg�n error al ejecutar las acciones de la funci�n
   
   \retval ERR_FORMAT_SIST_OPERATIVO_INCORRECTO
             Se est� intentando ejecutar la librer�a en un sistema operativo no soportado
*/

int format_obtener_geometria( IN HANDLE_DISPOSITIVO handle, OUT long int *heads, OUT long int *sectors, OUT long int *cylinders, IN int dispositivo ) {

	int error;
	PARTICION particiones[4];
	int i;
	long int valor_max=0;
	int max=0;
	int winver;

    /* Obtenemos la versi�n del Windows */
    winver = lowlevel_get_winversion();

    switch (winver) {

                case WINXP:

							error = lowlevel_leer_particiones( handle, particiones, dispositivo );
							if ( error != ERR_LOWLEVEL_NO ) return ERR_FORMAT_SI;

							/* Partici�n vac�a = tipo 0 */

							for ( i=0; i<4; i++ ) {
								if ( (particiones[i].id != 0) && ( particiones[i].cyl_fin * particiones[i].sect_fin * ((particiones[i].head_fin)+1) > valor_max )) {
									valor_max = particiones[i].cyl_fin * particiones[i].sect_fin * ((particiones[i].head_fin)+1);
									max = i;
								}
							}

							*heads = particiones[max].head_fin + 1;
							*sectors = particiones[max].sect_fin;
							*cylinders = particiones[max].cyl_fin + 1;

							break;

				default:    return ERR_FORMAT_SIST_OPERATIVO_INCORRECTO;

	}

    return ERR_FORMAT_NO;

}

//---------------------------------------------------------------------------
/*! \brief Obtiene el tama�o en bytes del dispositivo indicado

        Utiliza la API de Windows para obtener el tama�o del dispositivo, en bytes

   \param  unidad_logica
            Unidad l�gica del dispositivo

   \param  tamanyo
			Indica el tama�o en bytes del dispositivo

   \retval ERR_FORMAT_NO
			La funci�n se ha ejecutado correctamente

   \retval ERR_FORMAT_SI
            Ha ocurrido un error al obtener el tama�o del dispositivo

   \retval ERR_FORMAT_SIST_OPERATIVO_INCORRECTO
             Se est� intentando ejecutar la librer�a en un sistema operativo no soportado

   \todo Obtener el tama�o de la informaci�n que proporciona el hardware (actualmente se obtiene de la partici�n de datos)

*/

int format_obtener_tamanyo( IN int dispositivo, OUT long long  *tamanyo ) {

	int winver;
	BOOL result;
	//ULARGE_INTEGER FreeBytesAvailable;
	//ULARGE_INTEGER TotalNumberOfBytes;
	HANDLE	hVolume;
	TCHAR	volumePath[MAX_PATH];

	ULONG	bytesWritten;
	UCHAR	DiskExtentsBuffer[0x400];
	PVOLUME_DISK_EXTENTS DiskExtents = (PVOLUME_DISK_EXTENTS)DiskExtentsBuffer;


	/* Obtenemos la versi�n del Windows */
    winver = lowlevel_get_winversion();

    switch (winver) {

                case WINXP:

				
						/*error = DeviceIoControl( handle, IOCTL_DISK_GET_DRIVE_GEOMETRY, NULL, 0, &geometry, sizeof(geometry), &nbytes, NULL);
						if ( error == 0 ) return ERR_FORMAT_SI;*/
						/*(*tamanyo) = (geometry.BytesPerSector) * (geometry.Cylinders.LowPart) * (geometry.SectorsPerTrack) * (geometry.TracksPerCylinder);*/
						
						_stprintf( volumePath, _T("\\\\.\\PHYSICALDRIVE%d"), dispositivo );
						hVolume = CreateFile( volumePath,
											GENERIC_READ, FILE_SHARE_READ|FILE_SHARE_WRITE, 
											NULL, OPEN_EXISTING, 0, NULL );
						
						if( hVolume == INVALID_HANDLE_VALUE ) {
							return ERR_FORMAT_SI;
						}
						else{
							result= DeviceIoControl( hVolume,
									             IOCTL_VOLUME_GET_VOLUME_DISK_EXTENTS,
									             NULL, 0,
									             DiskExtents, sizeof(DiskExtentsBuffer),
									             &bytesWritten, NULL ); 
							
							if ( result == 0 ) {
								return ERR_FORMAT_SI;
							}

							result= CloseHandle(hVolume);
							if (result == 0 ){ 
								return ERR_FORMAT_SI;
							}

						//result = GetDiskFreeSpaceEx( unidad_logica, &FreeBytesAvailable, &TotalNumberOfBytes, NULL );
							
							//*tamanyo = TotalNumberOfBytes.LowPart;
							*tamanyo = DiskExtents->Extents[0].ExtentLength.LowPart;
							memcpy(((char*)tamanyo)+4, &(DiskExtents->Extents[0].ExtentLength.HighPart),4);
						}
						break;

				default:    return ERR_FORMAT_SIST_OPERATIVO_INCORRECTO;

	}

	

	return ERR_FORMAT_NO;
}


//---------------------------------------------------------------------------
/*! \brief Lee de nuevo la informaci�n del dispositivo

        Relee la informaci�n del dispositivo, es �til para el caso en el que
		se modifica la tabla de particiones a mano.

   \param  handle
            Manejador que se obtiene al abrir el dispositivo.

   \retval ERR_FORMAT_NO
			La funci�n se ha ejecutado correctamente

   \retval ERR_FORMAT_SI
            Ha ocurrido un error al obtener el tama�o del dispositivo

   \retval ERR_FORMAT_FUNCION_NO_DISPONIBLE
			Este error indica que el sistema operativo no admite el refresco
			de la cache que contiene la tabla de particiones. En este caso
			debe solicitarse al usuario que extraiga e introduzca el dispositivo.
   
   \retval ERR_FORMAT_SIST_OPERATIVO_INCORRECTO
             Se est� intentando ejecutar la librer�a en un sistema operativo no soportado

*/

int format_releer_mbr( IN HANDLE_DISPOSITIVO handle ) {

	int error;
	long int nbytes;
	int winver;

	/* Obtenemos la versi�n del Windows */
    winver = lowlevel_get_winversion();

    switch (winver) {

                case WINXP:
	
					error = DeviceIoControl( handle, IOCTL_DISK_UPDATE_PROPERTIES, NULL, 0, NULL, 0, &nbytes, NULL );	
					if ( error == 0 ) {
						if ( GetLastError() == ERROR_NOT_SUPPORTED ) return ERR_FORMAT_FUNCION_NO_DISPONIBLE;
					}

					break;

				default:    return ERR_FORMAT_SIST_OPERATIVO_INCORRECTO;

	}				

	return ERR_FORMAT_NO;


	/* Otra forma de hacer lo mismo, utilizando una llamada de m�s bajo nivel: NtDeviceIoControlFile */

#if 0

#include <winternl.h>
#include <ntstatus.h>

	int error;
	PIO_STATUS_BLOCK IoStatusBlock=NULL;
	HINSTANCE handle_dll;
	
	typedef NTSTATUS (__stdcall *MYPROC)(    
		HANDLE FileHandle,
		HANDLE Event,
		PIO_APC_ROUTINE ApcRoutine,
		PVOID ApcContext,
		PIO_STATUS_BLOCK IoStatusBlock,
		ULONG IoControlCode,
		PVOID InputBuffer,
		ULONG InputBufferLength,
		PVOID OutputBuffer,
		ULONG OutputBufferLength );

	MYPROC NtDeviceIoControlFile;

	IoStatusBlock=(PIO_STATUS_BLOCK) malloc (sizeof(IO_STATUS_BLOCK));

	handle_dll = LoadLibrary("ntdll.dll");
	if ( handle_dll == NULL ) return ERR_FORMAT_SI;

	NtDeviceIoControlFile = (MYPROC) GetProcAddress(handle_dll, "NtDeviceIoControlFile");
	if (NtDeviceIoControlFile == NULL) return ERR_FORMAT_SI;

	error = NtDeviceIoControlFile( handle, NULL, NULL, NULL, IoStatusBlock, IOCTL_DISK_UPDATE_PROPERTIES, NULL, 0, NULL, 0);
	if ( error != STATUS_SUCCESS ) return error;

	FreeLibrary(handle_dll);

	free(IoStatusBlock);

#endif

}

//---------------------------------------------------------------------------
/*! \brief Obtiene informaci�n acerca de la geometr�a �ptima del dispositivo

        Obtiene la informaci�n CHS del dispositivo (n�mero de cabezas, sectores y
        cilindros) �ptima para el dispositivo, de manera que el n�mero de
		cilindros no sea mayor que 1024 y que haga que se aproveche la
		mayor cantidad de espacio en el dispositivo posible.

		Esta funci�n es especialmente �til para los dispositivos que no disponen
		de MBR.

   \param handle
            Manejador para acceder al dispositivo

   \param heads
            N�mero �ptimo de cabezas del dispositivo

   \param sectors
            N�mero �ptimo de sectores total del dispositivo

   \param cylinders
            N�mero �ptimo de cilindros del dispositivo

   \retval ERR_FORMAT_NO
             La funci�n se ejecut� correctamente

   \retval ERR_FORMAT_SI
             Ha ocurrido alg�n error al ejecutar las acciones de la funci�n

   \retval ERR_FORMAT_SIST_OPERATIVO_INCORRECTO
             Se est� intentando ejecutar la librer�a en un sistema operativo no soportado

*/
int format_obtener_geometria_optima( IN int unidad_logica, OUT long int *heads, OUT long int *sectors, OUT long int *cylinders ) {
	int error;
	long long tam, tam_best=0;
	long long tam_temp;
	long long tam_opt=0;
	unsigned int cabezas=1;			/* 1-256 cabezas */
	unsigned int cabezas_opt, cabezas_opt_best;
	unsigned int sectores=1, sectores_opt_best;		/* 1-64 sectores */
	unsigned int sectores_opt;
	unsigned int cilindros=1024;	/* 1024 cilindros fijos */
	unsigned int encontrado=0, bytesSector;
	int winver;
	
	/* Obtenemos la versi�n del Windows */
	winver = lowlevel_get_winversion();

	switch (winver) {
	  
	case WINXP:
	  
	  /* Obtenemos el tama�o del dispositivo */
	  error = format_obtener_tamanyo( unidad_logica, &tam );
	  if ( error != ERR_FORMAT_NO) return ERR_FORMAT_SI;
	  
#if 0
	  /* El aumento se va haciendo por bloques de medio Mb */
	  cilindros = 1023;
	  
	  /* Calculamos los valores de heads y sectors hasta llegar a una capacidad aproximada */
	  for ( sectores = 1; sectores <= 63; sectores++ ) {
	    for ( cabezas = 1; cabezas <= 256; cabezas++ ) {
	      tam_temp = 512 * cabezas * sectores * cilindros;
	      
	      if ( ( tam_temp > tam_opt ) && ( tam_temp < tam ) ) {
		
		tam_opt = tam_temp;
		cabezas_opt = cabezas;
		sectores_opt = sectores;
		
	      }
	    }
	  }
	  
	  *cylinders = cilindros;
	  /**heads = cabezas_opt-1;*/
	  *heads = cabezas_opt;
	  *sectors = sectores_opt;
	  
#endif
	  
	  
	  
	  bytesSector= lowlevel_obtenerBytesSector(unidad_logica);
	  sectores = 1; cabezas=1;
	  while ( (!encontrado) && (sectores <=64) ) {
	    cabezas=1;
	    while ( (!encontrado) && (cabezas <= 256) ) {
	      tam_temp = bytesSector * cabezas * sectores * cilindros;
	      
	      if ( ( tam_temp > tam_opt ) && ( tam_temp <= tam ) ){
		
		tam_opt = tam_temp;
		cabezas_opt = cabezas;
		sectores_opt = sectores;
		
		if ( tam_temp == tam ){
		  encontrado = 1;  
		} 
		else if( tam_temp > tam_best && tam_temp < tam ){
		  tam_best= tam_temp;
		  cabezas_opt_best= cabezas;
		  sectores_opt_best=sectores;
		}
	      }
	      cabezas++;
	    }
	    sectores++;
	    
	  }
	  
	  /* *cylinders = cilindros - 1;
	   *heads = cabezas_opt;
	   *sectors = sectores_opt - 1; */
	  
	  if ( encontrado=0 ){
	    cabezas_opt= cabezas_opt_best;
	    sectores_opt= sectores_opt_best;
	  }

	  *cylinders = cilindros;
	  *heads = cabezas_opt;
	  *sectors = sectores_opt;
	  

	  break;
	  
	default:    return ERR_FORMAT_SIST_OPERATIVO_INCORRECTO;
	  
	}

	return ERR_FORMAT_NO;
}

//---------------------------------------------------------------------------
/*! \brief Crea y escribe un MBR para aquellos dispositivos que no dispongan de uno

        Crea y escribe un nuevo MBR en el dispositivo, con una �nica partici�n de datos
		que ocupa todo el dispositivo

   \param  handle
            Manejador que se obtiene al abrir el dispositivo.

   \param  heads
			N�mero de cabezas del dispositivo

   \param  cylinders
			N�mero de cilindros del dispositivo
			
   \param  sectors
			N�mero de sectores por pista del dispositivo

   \retval ERR_FORMAT_NO
			La funci�n se ha ejecutado correctamente

   \retval ERR_FORMAT_ESCRITURA_MBR_INCORRECTA
             No se ha podido realizar la escritura del MBR de forma correcta

   \retval ERR_FORMAT_SIST_OPERATIVO_INCORRECTO
             Se est� intentando ejecutar la librer�a en un sistema operativo no soportado

*/

int format_crear_mbr( IN HANDLE_DISPOSITIVO handle, IN int heads, IN int cylinders, IN int sectors, IN int dispositivo ) {

	unsigned char* mbr;
	unsigned char* ptr;
	int result;
	unsigned long rel_sectors, num_sectors;
	unsigned long rel_sectors_tmp = 0;
	unsigned long num_sectors_tmp = 0;

	unsigned char signature[2] = "\x55\xAA";
	int winver;

	char boot_code[446] =
	"\x0e"			/* push cs */
	"\x1f"			/* pop ds */
	"\xbe\x5b\x7c"	/* mov si, offset message_txt */
					/* write_msg: */
	"\xac"			/* lodsb */
	"\x22\xc0"		/* and al, al */
	"\x74\x0b"		/* jz key_press */
	"\x56"			/* push si */
	"\xb4\x0e"		/* mov ah, 0eh */
	"\xbb\x07\x00"	/* mov bx, 0007h */
	"\xcd\x10"		/* int 10h */
	"\x5e"			/* pop si */
	"\xeb\xf0"		/* jmp write_msg */
					/* key_press: */
	"\x32\xe4"		/* xor ah, ah */
	"\xcd\x16"		/* int 16h */
	"\xcd\x19"		/* int 19h */
	"\xeb\xfe"		/* foo: jmp foo */
					/* message_txt: */

/* Quitar las comillas escapadas para verlo bien :o) */

"                                                            \r\n"
"MMMMMMMMMM  MMMM  MM\r\n"
"MMMMMM \"\"MM \"MMM  MM\r\n"
"MMMMMM\"MMMMo    oMMM\r\n"
"MMMMM   MMMMMMMMMMMM              Universitat Jaume I,2005\r\n"
"MMMM   MMMMM\"ooooMMM\r\n"
"MMMM  \"MMMMMMM\" \"MMM              http://www.clauer.uji.es\r\n"
"MM\"\" oMMMMMMMM   MMM\r\n"
"MM ooMMMMMMMMMo  MMM        Retire el dispositivo USB para continuar\r\n"
"MMMMMMMMMMMMMMM  MMM\x00";


/*

MMMMMMMMMMMMMo    MMMMMM   "M
MMMMMMMMMMMMMMM   "MMMMM    "
MMMMMMMM""MMMMMo   MMMMMM   M
MMMMMMMMM  o"MMM    "MM"   oM
MMMMMMM"MMoMMMMMMo      ooMMM
MMMMMMM "  "MMMMMMMMMoMMMMMMM
MMMMMMM    MMMMMMMMMMMMMM"MMM
MMMMMM    oMMMMMMM"  ""o oMMM
MMMMMM   MMMMMMMMooMMMMMMMMMM
MMMMMM   MMMMMMMMMMMMM    MMM
MMMMM"   MMMMMMMMMMMMM    MMM
MM"     MMMMMMMMMMMMMM    MMM
MMo o oMMMMMMMMMMMMMMMM   MMM
MMMMMMMMMMMMMMMMMMMMMMM   MMM
*/

/*

MMMMMMMMMM  MMMM  MM\r\n
MMMMMM ""MM "MMM  MM\r\n
MMMMMM"MMMMo    oMMM           This USB device can't be used to boot the system\r\n
MMMMM   MMMMMMMMMMMM\r\n
MMMM   MMMMM"ooooMMM              Projecte CLAUER, Universitat Jaume I, 2005\r\n
MMMM  "MMMMMMM" "MMM                      http://www.clauer.uji.es\r\n
MM"" oMMMMMMMM   MMM\r\n
MM ooMMMMMMMMMo  MMM\r\n
MMMMMMMMMMMMMMM  MMM\x00

*/

    //"xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx\r\n"

	/* Obtenemos la versi�n del Windows */
	winver = lowlevel_get_winversion();

	switch (winver) {

                case WINXP:

						mbr = (unsigned char *) malloc (512 * sizeof(char));
						ptr = mbr;

						/* Rellenamos el boot loader code */
						memcpy(ptr, boot_code, 446);
						ptr+=446;

						rel_sectors = 1;
						/*num_sectors = (heads) * (cylinders-1) * sectors;		/* El n�mero de cilindros empieza en 0 */

						num_sectors = (cylinders * heads * sectors) + (heads * sectors) + sectors + 1;


						/* Rellenamos las entradas de particiones */

						/* Particion 1 */

						mbr[446] = (BYTE) '\x80';					/* Partici�n activa					*/
						mbr[447] = (BYTE) '\x01';					/* Cabeza inicial = 1				*/
						mbr[448] = (BYTE) 0x00000001;				/* Sector inicial = 1				*/
						mbr[449] = (BYTE) 0x00000000;				/* Cilindro inicial = 0				*/
						mbr[450] = (BYTE) 0x00000006;				/* Tipo de la partici�n = FAT16		*/
						mbr[451] = (BYTE) heads-1;					/* Cabeza final, por par�metro		*/
						mbr[452] = (BYTE) (0x0000003F & sectors);	/* Sector final, por par�metro		*/
						mbr[452] = ((BYTE) (((cylinders-1) & 0x00000300) >> 2)) | mbr[452];	/* Cilindro final, por par�metro	*/
						mbr[453] = (BYTE) (0x000000FF & cylinders-1);	
						memcpy(mbr+454, &rel_sectors, 4);
						memcpy(mbr+458, &num_sectors, 4);

						/* El resto de particiones, las marcamos como vac�as (tipo 0) */

						/* Particion 2 */

						mbr[462] = (BYTE) '\x00';					/* Partici�n no activa				*/
						mbr[463] = (BYTE) '\x00';					/* Cabeza inicial = 0				*/
						mbr[464] = (BYTE) 0x00000000;				/* Sector inicial = 0				*/
						mbr[465] = (BYTE) 0x00000000;				/* Cilindro inicial = 0				*/
						mbr[466] = 0x00;							/* Partici�n vac�a					*/
						mbr[467] = (BYTE) '\x00';					/* Cabeza final = 0					*/
						mbr[468] = (BYTE) '\x00';					/* Sector final = 0					*/
						mbr[469] = (BYTE) '\x00';					/* Cilindro final = 0				*/	
						memcpy(mbr+470, &rel_sectors_tmp, 4);
						memcpy(mbr+474, &num_sectors_tmp, 4);

						/* Particion 3 */

						mbr[478] = (BYTE) '\x00';					/* Partici�n no activa				*/
						mbr[479] = (BYTE) '\x00';					/* Cabeza inicial = 0				*/
						mbr[480] = (BYTE) 0x00000000;				/* Sector inicial = 0				*/
						mbr[481] = (BYTE) 0x00000000;				/* Cilindro inicial = 0				*/
						mbr[482] = 0x00;							/* Partici�n vac�a					*/
						mbr[483] = (BYTE) '\x00';					/* Cabeza final = 0					*/
						mbr[484] = (BYTE) '\x00';					/* Sector final = 0					*/
						mbr[485] = (BYTE) '\x00';					/* Cilindro final = 0				*/
						memcpy(mbr+486, &rel_sectors_tmp, 4);
						memcpy(mbr+490, &num_sectors_tmp, 4);

						/* Particion 4 */

						mbr[494] = (BYTE) '\x00';					/* Partici�n no activa				*/
						mbr[495] = (BYTE) '\x00';					/* Cabeza inicial = 0				*/
						mbr[496] = (BYTE) 0x00000000;				/* Sector inicial = 0				*/
						mbr[497] = (BYTE) 0x00000000;				/* Cilindro inicial = 0				*/
						mbr[498] = 0x00;							/* Partici�n vac�a					*/
						mbr[499] = (BYTE) '\x00';					/* Cabeza final = 0					*/
						mbr[500] = (BYTE) '\x00';					/* Sector final = 0					*/
						mbr[501] = (BYTE) '\x00';					/* Cilindro final = 0				*/
						memcpy(mbr+502, &rel_sectors_tmp, 4);
						memcpy(mbr+506, &num_sectors_tmp, 4);

						/* Signature */
						memcpy(mbr+510, signature, 2);

						
						/* Escribimos el resultado en el MBR */

						result = lowlevel_escribir_sector( handle, 0, mbr, dispositivo );
						if ( result != ERR_LOWLEVEL_NO ) return ERR_FORMAT_ESCRITURA_MBR_INCORRECTA;

						free(mbr);

						break;

				default:    return ERR_FORMAT_SIST_OPERATIVO_INCORRECTO;

	}

	return ERR_FORMAT_NO;
}



int format_crear_clauer( IN int dispositivo,IN char* unidad_logica,IN char* password, IN int porcentaje )
{
	int winver;
    unsigned char* mbr;
    int result;
	long long tamanyo;
	float aux;
    long int H,S,C,C1;
	HANDLE_DISPOSITIVO handle;
	PARTICION particiones[4];
	BYTE * sector;
	int indice, bytesSector;

	unsigned long rel_sectors, num_sectors;
	unsigned char signature[2] = "\x55\xAA";

	
	bytesSector= lowlevel_obtenerBytesSector(dispositivo);
	if ( bytesSector < 0 ){
		return ERR_FORMAT_SI;
	}
	sector= (BYTE *) malloc(bytesSector);
	if( unidad_logica == NULL ){
		return ERR_FORMAT_SI;
	}

	if(porcentaje>100 || porcentaje<0){
		return ERR_FORMAT_SI;
	}

	/* Obtenemos la versi�n del Windows */
    winver = lowlevel_get_winversion();

	
    switch (winver) {
		case WINXP:	
			aux=(float)porcentaje;
			if ( aux == 0 ){
				result = format_obtener_tamanyo(dispositivo, &tamanyo);
				if ( result != ERR_FORMAT_NO ){
					return ERR_FORMAT_SI;
				}

				tamanyo= tamanyo/1000/1024;
				if ( tamanyo >= 128 ){
					aux= (float)(100.0 - (500.0/tamanyo));	
				}
				else{
					aux= 96;
				}
			}

			result = format_obtener_geometria_optima( dispositivo,&H,&S,&C );
			if ( result != ERR_FORMAT_NO ) return ERR_FORMAT_LEYENDO_GEOMETRIA;
			mbr = (unsigned char *) malloc (bytesSector * sizeof(char));
			memset( mbr, 0, bytesSector*sizeof(char) );
			
			result = lowlevel_abrir_dispositivo( dispositivo, &handle );
			if ( result != ERR_LOWLEVEL_NO ) {
				return ERR_FORMAT_SI;
			}
			
			rel_sectors= S;
			C1=(long int)(aux*((float)C)/100);
			//C1=porcentaje*C/100;
			num_sectors= C1 * H * S - S;
			
			/* Particion 1 */
			mbr[446] = (BYTE) '\x80';	                /* Partici�n activa					*/

			mbr[447] = (BYTE) '\x01';					/* Cabeza inicial = 1				*/
			mbr[448] = (BYTE) 0x00000001;				/* Sector inicial = 1				*/
			mbr[449] = (BYTE) 0x00000000;				/* Cilindro inicial = 0				*/

			mbr[450] = (BYTE) 0x00000006;				/* Tipo de la partici�n = FAT16		*/

			mbr[451] = (BYTE) H-1;					   /* Cabeza final, por par�metro		*/

			mbr[452] = ((BYTE) (((C1-1) & 0x00000300) >> 2)) | (BYTE) S;	/* Cilindro final, por par�metro	*/
			mbr[453] = (BYTE) (0x000000FF & (C1-1));

			memcpy(mbr+454, &rel_sectors, 4);
			memcpy(mbr+458, &num_sectors, 4);
			if(aux!=100){
				rel_sectors= C1*H*S;
				num_sectors= (C-C1) * H * S;

				/* Particion 4 */
				mbr[494] = (BYTE) '\x00';               					    /* Partici�n no activa				*/

				mbr[495] = (BYTE) '\x00';				                     	/* Cabeza inicial = 0				*/
				mbr[496] = (BYTE) ((BYTE) ((C1 & 0x00000300) >> 2))| (BYTE) 1;	/* Sector inicial = 0				*/
				mbr[497] = (BYTE) (0x000000FF & C1);	                        /* Cilindro inicial = 0				*/
				
				mbr[498] = (BYTE) 0x69;						                

				mbr[499] = (BYTE) H-1;					                        /* Cabeza final = 0					*/
				mbr[500] = (BYTE) (((C-1) & 0x00000300) >> 2) | (BYTE) S;		/* Cilindro final = 0				*/
				mbr[501] = (BYTE) (0x000000FF & (C-1));					        /* Sector final = 0					*/

				memcpy(mbr+502, &rel_sectors, 4);
				memcpy(mbr+506, &num_sectors, 4);
			}
				/* Signature */
				memcpy(mbr+510, signature, 2);

			 /* Escribimos el resultado en el MBR */

		

            result = lowlevel_escribir_sector( handle, 0, mbr, dispositivo );
			
            if ( result != ERR_LOWLEVEL_NO ) return ERR_FORMAT_ESCRITURA_MBR_INCORRECTA;
            free(mbr);
			

			/* Esto fuerza la lectura de la unidad l�gica */
			for ( indice=0; indice<=255; indice++ ) sector[indice]=(BYTE) 0;

			result = lowlevel_escribir_sector( handle, 1, sector, dispositivo );
			if ( result != ERR_LOWLEVEL_NO ) return ERR_FORMAT_ESCRITURA_MBR_INCORRECTA;

			break;

		default:
			return ERR_FORMAT_SIST_OPERATIVO_INCORRECTO;	
      }	
	

	result= format_releer_mbr(handle);
		if ( result != ERR_FORMAT_NO ) {
			printf("7\n");
			return ERR_FORMAT_SI;
		}
	
	result = lowlevel_cerrar_dispositivo( handle );
			if ( result != ERR_LOWLEVEL_NO ) {
				return ERR_FORMAT_SI;
			}

	

	result = format_formatear_unidad_logica_win( unidad_logica );
	if ( result != ERR_FORMAT_NO ) {
		return ERR_FORMAT_SI;
	}

	result = lowlevel_abrir_dispositivo( dispositivo, &handle );
			if ( result != ERR_LOWLEVEL_NO ) {
				return ERR_FORMAT_SI;
			}

	if ( aux != 100 ){
		result = lowlevel_leer_particiones( handle, particiones, dispositivo );
		if ( result != ERR_LOWLEVEL_NO ) {
				return ERR_FORMAT_SI;
		}

		result = format_crear_zona_cripto( handle, particiones[3], password, NULL, dispositivo );
		if ( result != ERR_FORMAT_NO ) {
			return ERR_FORMAT_SI;
		}
	}
	/*result= format_releer_mbr(handle);
    if ( result != ERR_FORMAT_NO ) {
		return ERR_FORMAT_SI;
	}
	*/

	result = lowlevel_cerrar_dispositivo( handle );
			if ( result != ERR_LOWLEVEL_NO ) {
				return ERR_FORMAT_SI;
			}

	return ERR_FORMAT_NO;	
}


//---------------------------------------------------------------------------
/*! \brief Reescribe el MBR para tener en cuenta la nueva partici�n criptogr�fica

        Reescribe el MBR para adaptar el dispositivo al nuevo formato, a�adiendo
        la partici�n cuarta como propia.

   \param handle
            Manejador que se obtiene al abrir el dispositivo.

   \param porcentaje
            Indica el porcentaje del dispositivo que se va a destinar a la partici�n
            de datos del usuario (habitualmente un 80-90%)

   \param particiones
            Vector que contiene la estructura de las particiones previa a las
            modificaciones que se van a realizar en esta funci�n

   \param nuevas_particiones
            Vector que contiene la estructura de las particiones una vez modificada

   \retval ERR_FORMAT_NO
             La funci�n se ejecut� correctamente

   \retval ERR_FORMAT_LEYENDO_GEOMETRIA
             Ha ocurrido un error intentando obtener la geometr�a del dispositivo

   \retval ERR_FORMAT_LECTURA_MBR_INCORRECTA
             No se ha podido realizar la lectura del MBR de forma correcta

   \retval ERR_FORMAT_ESCRITURA_MBR_INCORRECTA
             No se ha podido realizar la escritura del MBR de forma correcta

   \retval ERR_FORMAT_SIST_OPERATIVO_INCORRECTO
             Se est� intentando ejecutar la librer�a en un sistema operativo no soportado

*/

int format_cambiar_particiones_mbr( IN HANDLE_DISPOSITIVO handle, IN int porcentaje, IN PARTICION particiones[], OUT PARTICION nuevas_particiones[], IN int dispositivo )
{

      int winver;
      unsigned char* data;
      int result;
      long int H,S,C;
	  BYTE sector[512];
	  int indice;

      /* Obtenemos la versi�n del Windows */
      winver = lowlevel_get_winversion();

      switch (winver) {

                case WINXP:

						result = format_obtener_geometria( handle,&H,&S,&C, dispositivo );
						if ( result != ERR_FORMAT_NO ) return ERR_FORMAT_LEYENDO_GEOMETRIA;

                        /* PARTICION DE DATOS */
                        /* ------------------ */

                        /* El inicio de datos se queda igual */
						/* TODO: Esto es una prueba, debemos cambiar el c�digo como era*/
                        nuevas_particiones[0].cyl_ini  = particiones[0].cyl_ini;
						nuevas_particiones[0].head_ini = particiones[0].head_ini;
                        nuevas_particiones[0].sect_ini = particiones[0].sect_ini;

                        /* Reducci�n de la partici�n de datos, el final es lo que cambia */

                        //nuevas_particiones[0].cyl_fin  = (porcentaje*particiones[0].cyl_fin)/100;
						nuevas_particiones[0].cyl_fin  = ((porcentaje*C)/100)-1;
                        nuevas_particiones[0].head_fin = particiones[0].head_fin;
                        nuevas_particiones[0].sect_fin = particiones[0].sect_fin;


                        /* El id y el inicio de la partici�n no se tocan */
                        nuevas_particiones[0].id = particiones[0].id;
                        nuevas_particiones[0].rel_sectors = particiones[0].rel_sectors;

                        /* El n�mero de sectores total de la partici�n se reduce: */
                        nuevas_particiones[0].num_sectors = (nuevas_particiones[0].cyl_fin - nuevas_particiones[0].cyl_ini) * H * S +
                                                            (nuevas_particiones[0].head_fin - nuevas_particiones[0].head_ini) * S +
                                                            (nuevas_particiones[0].sect_fin - nuevas_particiones[0].sect_ini + 1);

                        /* PARTICION CRYPTO */
                        /* ---------------- */

						/* Si la de datos ocupa el 100%, debemos eliminar la zona criptogr�fica */

						if ( porcentaje == 100 ) {

								nuevas_particiones[3].cyl_ini = 0;
								nuevas_particiones[3].head_ini = 0;
								nuevas_particiones[3].sect_ini = 0;

								nuevas_particiones[3].cyl_fin = 0;
								nuevas_particiones[3].head_fin = 0;
								nuevas_particiones[3].sect_fin = 0;

								nuevas_particiones[3].id = 0;
								nuevas_particiones[3].rel_sectors = 0;
								nuevas_particiones[3].num_sectors = 0;

						}
						else {

								/* El inicio de crypto comienza justo en el cilindro posterior a la partici�n de datos */

								nuevas_particiones[3].cyl_ini = nuevas_particiones[0].cyl_fin+1;
								nuevas_particiones[3].head_ini = 0;
								nuevas_particiones[3].sect_ini = 1;

								/* El final de crypto es el final del dispositivo (lo que diga la geometr�a) */

								/*nuevas_particiones[3].cyl_fin  = particiones[0].cyl_fin;
								nuevas_particiones[3].head_fin = particiones[0].head_fin;
								nuevas_particiones[3].sect_fin = particiones[0].sect_fin;*/

								nuevas_particiones[3].cyl_fin  = C-1;
								nuevas_particiones[3].head_fin = H-1;
								nuevas_particiones[3].sect_fin = S;

								/* Identificador de nuestra partici�n */
								nuevas_particiones[3].id = 0x69;

								/* C�lculo del n�mero de sector de inicio de la partici�n criptogr�fica en base a la nomenclatura CHS */
								nuevas_particiones[3].rel_sectors = nuevas_particiones[3].cyl_ini * H * S +
																	nuevas_particiones[3].head_ini * S +
																	nuevas_particiones[3].sect_ini - 1;

								/* C�lculo del n�mero de sectores de la partici�n criptogr�fica */
								nuevas_particiones[3].num_sectors = (nuevas_particiones[3].cyl_fin - nuevas_particiones[3].cyl_ini) * H * S +
																	(nuevas_particiones[3].head_fin - nuevas_particiones[3].head_ini) * S +
																	(nuevas_particiones[3].sect_fin - nuevas_particiones[3].sect_ini + 1);
						}


                        /* Traslado de informaci�n al MBR, lo leemos primero */

                        data = (unsigned char *) malloc (512 * sizeof(char));

                        result = lowlevel_leer_sector( handle, 0, data, dispositivo );
                        if ( result != ERR_LOWLEVEL_NO ) return ERR_FORMAT_LECTURA_MBR_INCORRECTA;

                        data[447] = (BYTE) nuevas_particiones[0].head_ini;
                        data[448] = (BYTE) (0x0000003F & nuevas_particiones[0].sect_ini);
                        data[448] = ((BYTE) ((nuevas_particiones[0].cyl_ini & 0x00000300) >> 2)) | data[448];
                        data[449] = (BYTE) (0x000000FF & nuevas_particiones[0].cyl_ini);
                        data[451] = (BYTE) nuevas_particiones[0].head_fin;
                        data[452] = (BYTE) (0x0000003F & nuevas_particiones[0].sect_fin);
                        data[452] = ((BYTE) ((nuevas_particiones[0].cyl_fin & 0x00000300) >> 2)) | data[452];
                        data[453] = (BYTE) (0x000000FF & nuevas_particiones[0].cyl_ini);
                        memcpy(data+454, &(nuevas_particiones[0].rel_sectors),4);
                        memcpy(data+458, &(nuevas_particiones[0].num_sectors), 4);

                        data[494] = 0x00;
                        data[495] = (BYTE) nuevas_particiones[3].head_ini;
                        data[496] = (BYTE) (0x0000003F & nuevas_particiones[3].sect_ini);
                        data[496] = ((BYTE) ((nuevas_particiones[3].cyl_ini & 0x00000300) >> 2)) | data[496];
                        data[497] = (BYTE) (0x000000FF & nuevas_particiones[3].cyl_ini);
                        data[498] = (BYTE) nuevas_particiones[3].id;
                        data[499] = (BYTE) nuevas_particiones[3].head_fin;
                        data[500] = (BYTE) (0x0000003F & nuevas_particiones[3].sect_fin);
                        data[500] = ((BYTE) ((nuevas_particiones[3].cyl_fin & 0x00000300) >> 2)) | data[500];
                        data[501] = (BYTE) (0x000000FF & nuevas_particiones[3].cyl_fin);
                        memcpy(data+502, &(nuevas_particiones[3].rel_sectors), 4);
                        memcpy(data+506, &(nuevas_particiones[3].num_sectors), 4);


                        /* Escribimos el resultado en el MBR */

                        result = lowlevel_escribir_sector( handle, 0, data, dispositivo );
                        if ( result != ERR_LOWLEVEL_NO ) return ERR_FORMAT_ESCRITURA_MBR_INCORRECTA;

                        free(data);


						/* Esto fuerza la lectura de la unidad l�gica */
						for ( indice=0; indice<=255; indice++ ) sector[indice]=(BYTE) 0;

						result = lowlevel_escribir_sector( handle, 1, sector, dispositivo );
						if ( result != ERR_LOWLEVEL_NO ) return ERR_FORMAT_ESCRITURA_MBR_INCORRECTA;


                        break;

        default:        return ERR_FORMAT_SIST_OPERATIVO_INCORRECTO;

      }
      return ERR_FORMAT_NO;
}

//---------------------------------------------------------------------------
/* Esta funci�n comprueba si es posible leer una unidad l�gica */

int format_is_readable ( IN char* unidad, IN int dispositivo ) {

	int se_puede_leer = 0;
	char cadena_unidad[20];
	HANDLE_DISPOSITIVO handle;
	int result;
	BYTE sector[512];

	
	strcpy( cadena_unidad, "\\\\.\\" );
	strcat( cadena_unidad, unidad );

	handle = CreateFile(cadena_unidad,			     // leemos del dispositivo ndevice
						GENERIC_READ | GENERIC_WRITE,        // lo abrimos para leer y escribir
						FILE_SHARE_READ | FILE_SHARE_WRITE,  // compartido para leer y escribir
						NULL,								 // el handle no se puede heredar
						OPEN_EXISTING,						 // se supone que existe
						FILE_ATTRIBUTE_NORMAL | FILE_FLAG_WRITE_THROUGH | FILE_FLAG_NO_BUFFERING, // fichero normal
						NULL);								 // sin template file

	if ( handle != INVALID_HANDLE_VALUE ) {

		/* Probamos a leer un sector de la unidad */

		result = lowlevel_leer_sector( handle, 69, sector, dispositivo );
		if ( result == ERR_LOWLEVEL_NO ) se_puede_leer = 1;
		CloseHandle(handle);
	}

	return se_puede_leer;

}

//---------------------------------------------------------------------------
/*! \brief Enumera los dispositivos

        Obtiene los dispositivos que se encuentran en el sistema.

   \param unidades

   \param dispositivos

   \param numDispositivos

   \retval ERR_FORMAT_NO
             La funci�n se ejecut� correctamente

   \retval ERR_FORMAT_SIST_OPERATIVO_INCORRECTO
             Se est� intentando ejecutar la librer�a en un sistema operativo no soportado

*/

int format_enumerar_dispositivos(OUT char **unidades, OUT int *dispositivos, OUT int *numDispositivos)
{

        int winver;
		LPTSTR driveLetters, deviceName;
		char drive[3];
		DWORD tam, i,j;
		PARTICION particiones[4];
		HANDLE_DISPOSITIVO handle;
		unsigned int n_disp = 1;
		int num_dispositivos=0;
		int result;

		HANDLE hDev=NULL;
        char auxDev[8];
        STORAGE_DEVICE_NUMBER sdn;
		DWORD dwBytesReturned;

        /* Obtenemos la versi�n del Windows */

        //winver = lowlevel_get_winversion();
        winver = WINXP;

        switch (winver) {

			case WIN95:

						if ( !SOPORTE_WIN98 ) return ERR_LOWLEVEL_SIST_OPERATIVO_INCORRECTO;

						/* Esta funci�n no devuelve las unidades l�gicas bajo WIN98 */
						unidades = NULL;

						/* Proceso de b�squeda de la partici�n 0x69 */

						for ( n_disp = 1; n_disp < 99 ; n_disp++ ) {

							result = lowlevel_abrir_dispositivo( n_disp, &handle );
							if ( result != ERR_LOWLEVEL_NO ) return ERR_FORMAT_SI;
						
							result = lowlevel_leer_particiones( handle, particiones, -1 );
							if ( result == ERR_LOWLEVEL_NO ) {

								/* Comprobaci�n de partici�n cuarta */

								if ( particiones[3].id == 0x69 ) {

									if ( dispositivos ) dispositivos[num_dispositivos] = n_disp;
									num_dispositivos++;
								}

								result = lowlevel_cerrar_dispositivo( handle );
								if ( result != ERR_LOWLEVEL_NO ) return ERR_FORMAT_SI;
							}
						}

						(*numDispositivos) = num_dispositivos;

				break;

			case WINXP:
				/*NUEVA FUNC */
				//printf("Dentro de enumerar disps\nPasamos por WINXP\n");
					driveLetters = (LPTSTR) malloc (sizeof(TCHAR)*1000);
				deviceName = (LPTSTR) malloc (sizeof(TCHAR)*1000);

				*numDispositivos = 0;

				tam = GetLogicalDriveStrings(999,driveLetters);
				//printf("GetLogical.. devuelve= %d\n",tam);

				i=0;
				while ( i < (tam-1) ) {

					if ( GetDriveType(driveLetters+i) == DRIVE_REMOVABLE ) {

						strncpy(drive,driveLetters+i,2);
						drive[2] = 0;

						QueryDosDevice(drive, deviceName,999);

						/*
						 * Parseo el deviceName para comprobar que
						 * es un disco duro. Busco la cadena Harddisk
						 */

						j = 0;
						while ( j < strlen(deviceName) ) {
							while ( (deviceName[j] != 'H') && (j< strlen(deviceName)) )
								++j;

							if ( deviceName[j] == 'H' )
								if ( strncmp(deviceName+j, "Harddisk",8) == 0 ) {
									//printf("Entrando por dispositivo removable Harddisk deviceName=%s\n", deviceName);	
									char num[3];
									num[0] = deviceName[j+8];
									if ( isdigit(deviceName[j+9]) ) {
										num[1] = deviceName[j+9];
										num[2] = 0;
									} 
									else
										num[1] = 0;
						
									n_disp= atoi(num);

									if ( lowlevel_is_readable(drive, n_disp) ) {		/* Si el dispositivo se puede leer lo a�adimos */

										if ( unidades ) {
											//printf("entrando if unidades\n");
											strcpy(unidades[*numDispositivos], drive);
										}

										if ( dispositivos) {
											//printf("entrando if disps\n");
											 strncpy(auxDev, "\\\\.\\",4);
											 strncpy(auxDev+4,drive,2);
											 auxDev[6]='\0';
	    
										 
											 hDev = CreateFile(auxDev, 0, FILE_SHARE_READ|FILE_SHARE_WRITE, NULL, 
																OPEN_EXISTING, 0, NULL);

											if ( hDev == INVALID_HANDLE_VALUE ) {
												if ( GetLastError() == ERROR_ACCESS_DENIED ) {
													return ERR_LOWLEVEL_SI;
												} 
												else {												
													return ERR_LOWLEVEL_SI;
												}
											}
											//printf("Pasamos CreateFile OK\n");
								  	        if ( DeviceIoControl(hDev,
												IOCTL_STORAGE_GET_DEVICE_NUMBER,
												NULL,
												0,
												&sdn,
												sizeof(sdn),
												&dwBytesReturned,
												NULL) == 0 )
											{
													return ERR_LOWLEVEL_SI;
											}
											//printf("Pasamos Device IOControl OK\n");
											CloseHandle(hDev);
											dispositivos[*numDispositivos] = sdn.DeviceNumber;
										}
										//printf("Incrementando ndisps\n");
										++(*numDispositivos);
									}//else
										//printf("Is readable devuelve false para= %s\n", drive);

									break;
								}
						}

		        }

		        while ( (driveLetters[i] != '\0') && (i < (tam-1)))
			        ++i;
		        ++i;

				}

				free(driveLetters);
				free(deviceName);

                break;

				/*************/

				/*driveLetters = (LPTSTR) malloc (sizeof(TCHAR)*1000);
				deviceName = (LPTSTR) malloc (sizeof(TCHAR)*1000);

				*numDispositivos = 0;

				tam = GetLogicalDriveStrings(999,driveLetters);

				i=0;
				while ( i < (tam-1) ) {

					if ( GetDriveType(driveLetters+i) == DRIVE_REMOVABLE ) {

						strncpy(drive,driveLetters+i,2);
						drive[2] = 0;

						QueryDosDevice(drive, deviceName,999);

						
						 //Parseo el deviceName para comprobar que
						 //es un disco duro. Busco la cadena Harddisk
						 

						j = 0;
						while ( j < strlen(deviceName) ) {
							while ( (deviceName[j] != 'H') && (j< strlen(deviceName)) )
								++j;

							if ( deviceName[j] == 'H' )
								if ( strncmp(deviceName+j, "Harddisk",8) == 0 ) {

									if ( format_is_readable(drive) ) {		// Si el dispositivo se puede leer lo a�adimos 

										if ( unidades ) {
											strcpy(unidades[*numDispositivos], drive);
										}

										if ( dispositivos) {
											char num[3];
											int k = j+8;

											num[0] = deviceName[k];
											if ( isdigit(deviceName[k+1]) ) {
												num[1] = deviceName[k+1];
												num[2] = 0;
											} else
												num[1] = 0;

											dispositivos[*numDispositivos] = atoi(num);
										}


										++(*numDispositivos);
									}

									break;
								}
						}

		        }

		        while ( (driveLetters[i] != '\0') && (i < (tam-1)))
			        ++i;
		        ++i;

				}

				free(driveLetters);
				free(deviceName);

                break;
				*/

                default: return ERR_FORMAT_SIST_OPERATIVO_INCORRECTO;
      }

      return ERR_FORMAT_NO;

}

//---------------------------------------------------------------------------

DWORD WINAPI CodigoThread (LPVOID lpParam)
{

	char *unidad = (char *) lpParam;
	char **argv;

        argv = ( char ** ) malloc (sizeof(char *)*2);
        argv[0] = "mkdosfs";
        argv[1] = (char *) malloc (sizeof(char)*(3));
        strcpy(argv[1], unidad);

	mkdosfs(2, argv);

	return 0;

}

//---------------------------------------------------------------------------
/*! \brief Formatea la unidad l�gica especificada

   Formatea la unidad l�gica especificada mediante el par�metro unidad. Se supone
   de tipo FAT, y a su vez se le aplica un formateo FAT. Para realizar el formateo
   se crea un thread que se encarga de llamar a la librer�a de formateado.

   \param unidad
             Unidad l�gica a formatear, especificada mediante el formato "X:"

   \retval ERR_FORMAT_NO
             La funci�n se ejecut� correctamente

   \retval ERR_FORMAT_FORMATEANDO_DATOS
             Ha ocurrido un error formateando la uniad l�gica

   \retval ERR_FORMAT_SIST_OPERATIVO_INCORRECTO
             Se est� intentando ejecutar la librer�a en un sistema operativo no soportado
*/

int format_formatear_unidad_logica( IN char *unidad ) {

	HANDLE hThread;
	DWORD dwThreadId;
	DWORD ret;
	int winver;
        
	/* Obtenemos la versi�n del Windows */
    winver = lowlevel_get_winversion();

    switch (winver) {

                case WINXP:

							hThread = CreateThread(
									NULL,                        // default security attributes
									0,                           // use default stack size
									CodigoThread,                // thread function
									unidad,                      // argument to thread function
									0,                           // use default creation flags
									&dwThreadId);                // returns the thread identifier

						if ( hThread == NULL )
							return ERR_FORMAT_FORMATEANDO_DATOS;

						if ( WaitForSingleObject(hThread,INFINITE) == WAIT_FAILED ) {
							CloseHandle(hThread);
							return ERR_FORMAT_FORMATEANDO_DATOS;
						}

							/* Obtenemos el c�digo que devuelve el thread */

						if ( GetExitCodeThread(hThread, &ret) == 0 ) {
							CloseHandle(hThread);
							return ERR_FORMAT_FORMATEANDO_DATOS;
						}

						if ( ret == 0 )
							return ERR_FORMAT_NO;
						else
							return ERR_FORMAT_FORMATEANDO_DATOS;

				break;

				default:    return ERR_FORMAT_SIST_OPERATIVO_INCORRECTO;

    }

	return ERR_FORMAT_NO;

}



void PrintWin32Error( DWORD ErrorCode )
{
	LPVOID lpMsgBuf;
	FormatMessage( FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM,
					NULL, ErrorCode, 
					MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
					(LPTSTR) &lpMsgBuf, 0, NULL );
	_tprintf(_T("%s\n"), lpMsgBuf );
	LocalFree( lpMsgBuf );
}



//---------------------------------------------------------------------------
/*! \brief Formatea la unidad l�gica especificada

   Formatea la unidad l�gica especificada mediante el par�metro unidad. Se supone
   de tipo FAT, y a su vez se le aplica un formateo FAT. Para realizar el formateo
   se crea un proceso que llama al comando format del DOS.

   \param unidad
             Unidad l�gica a formatear, especificada mediante el formato "X:"

   \retval ERR_FORMAT_NO
             La funci�n se ejecut� correctamente

   \retval ERR_FORMAT_FORMATEANDO_DATOS
             Ha ocurrido un error formateando la uniad l�gica

*/

int format_formatear_unidad_logica_win( IN char* unidad ) {

        STARTUPINFO si;
        PROCESS_INFORMATION pi;
        char* comando;
		

		if ( unidad == NULL )
			return ERR_FORMAT_SI;
			
        ZeroMemory( &si, sizeof(si) );
        si.cb = sizeof(si);
        ZeroMemory( &pi, sizeof(pi) );

        comando = (char *) malloc (1024*sizeof(char));

//        strcpy(comando, "c:\\WINNT\\system32\\format.com ");
		strcpy(comando, "format.com ");
        strcat(comando, unidad);
        strcat(comando, " /BACKUP /FS:FAT32 /Q /X /V:Clauer");

        if( !CreateProcess( NULL,         // No module name (use command line).
                            comando,                          // Command line.
                            NULL,                             // Process handle not inheritable.
                            NULL,                             // Thread handle not inheritable.
                            FALSE,                            // Set handle inheritance to FALSE.
                            CREATE_NO_WINDOW,                 // No creation flags.
							//CREATE_NEW_CONSOLE,
                            NULL,                             // Use parent's environment block.
                            NULL,                             // Use parent's starting directory.
                            &si,                              // Pointer to STARTUPINFO structure.
                            &pi )                             // Pointer to PROCESS_INFORMATION structure
							){
			//PrintWin32Error(GetLastError());
			return ERR_FORMAT_SI;
		}
        free(comando);
        // Esperamos hasta que el proceso finalice
        WaitForSingleObject( pi.hProcess, INFINITE );

        // Cerramos los manejadores process y thread
        CloseHandle( pi.hProcess );
        CloseHandle( pi.hThread );


        return ERR_FORMAT_NO;
}


//---------------------------------------------------------------------------
/*! \brief Reestablece la copia de seguridad del directorio especificado como
           segundo par�metro en la unidad especificada como primer par�metro

   Reestablece la estructura de directorios almacenada en el directorio temporal
   especificado mediante el par�metro ruta_directorio_origen en la unidad especificada
   mediante el par�metro unidad

   \param handle
			Manejador del dispositivo que se obtiene al abrir el mismo

   \param password
			Es la password que protege la zona criptogr�fica. Si se especifica
			(diferente de NULL), se restaurar� la copia de seguridad correspondiente
			a ese dispositivo y no otra

   \param unidad
            Unidad en la que se quiere reestablecer la copia de seguridad realizada
            previamente. Sigue la sintaxis "D:","E:",etc

   \param ruta_directorio_origen
		    Cadena que indica la ruta al directorio en el que se encuentra
			almacenada la copia de seguridad

   \retval ERR_FORMAT_RESTAURANDO_BACKUP
             Ha ocurrido un error durante la ejecuci�n del comando que restaura
             la copia de seguridad.

   \retval ERR_FORMAT_SIST_OPERATIVO_INCORRECTO
             Se est� intentando ejecutar la librer�a en un sistema operativo no soportado

   \retval ERR_FORMAT_NO
             La funci�n se ejecut� correctamente.
*/

int format_restaurar_backup_datos( IN HANDLE_DISPOSITIVO handle, IN PASSWORD password, IN char* unidad, IN char * ruta_directorio_origen ) {

        int winver;
        STARTUPINFO si;
        PROCESS_INFORMATION pi;
        char* comando;

        /* Obtenemos la versi�n del Windows */
        winver = lowlevel_get_winversion();

        switch (winver) {

                case WINXP:
                                /* Proceso para la restauracion de archivos mediante xcopy */

                                ZeroMemory( &si, sizeof(si) );
                                si.cb = sizeof(si);
                                ZeroMemory( &pi, sizeof(pi) );

                                comando = (char *) malloc (1024*sizeof(char));

                                strcpy(comando, "xcopy ");
                                strcat(comando, ruta_directorio_origen);
                                strcat(comando, " ");
                                strcat(comando, unidad);
                                strcat(comando, " /E /C /F /H /Y /I /K");

                                if( !CreateProcess( NULL,         // No module name (use command line).
                                comando,                          // Command line.
                                NULL,                             // Process handle not inheritable.
                                NULL,                             // Thread handle not inheritable.
                                FALSE,                            // Set handle inheritance to FALSE.
                                CREATE_NO_WINDOW,                 // No creation flags.
                                NULL,                             // Use parent's environment block.
                                NULL,                             // Use parent's starting directory.
                                &si,                              // Pointer to STARTUPINFO structure.
                                &pi )                             // Pointer to PROCESS_INFORMATION structure
                                )
                                return ERR_FORMAT_RESTAURANDO_BACKUP;

                                free(comando);

                                // Esperamos hasta que el proceso finalice
                                WaitForSingleObject( pi.hProcess, INFINITE );

                                // Cerramos los manejadores process y thread
                                CloseHandle( pi.hProcess );
                                CloseHandle( pi.hThread );

                                break;

                default:        return ERR_FORMAT_SIST_OPERATIVO_INCORRECTO;

        }

        return ERR_FORMAT_NO;
}
//---------------------------------------------------------------------------
/*! \brief Elimina la copia de seguridad realizada previamente

   Elimina la copia de seguridad realizada mediante la llamada a
   format_crear_copia_seguridad. Se elimina el directorio indicado mediante
   el par�metro ruta_directorio

   \param handle
			Manejador del dispositivo que se obtiene al abrir el mismo

   \param password
			Es la password que protege la zona criptogr�fica. Si se especifica
			(diferente de NULL), se eliminar� la copia de seguridad correspondiente
			a ese dispositivo y no otra

   \param ruta_directorio
            Cadena que contiene la ruta al directorio en el que se ha almacenado
            la copia de seguridad

   \retval ERR_FORMAT_ELIMINANDO_BACKUP
             Ha ocurrido un error durante la ejecuci�n del comando que elimina
             la copia de seguridad.

   \retval ERR_FORMAT_SIST_OPERATIVO_INCORRECTO
             Se est� intentando ejecutar la librer�a en un sistema operativo no soportado

   \retval ERR_FORMAT_NO
             La funci�n se ejecut� correctamente.
*/

int format_eliminar_backup_datos( IN HANDLE_DISPOSITIVO handle, IN PASSWORD password, IN char * ruta_directorio ) {

        int winver;
		SHFILEOPSTRUCT *estructura;
		int result;
		char *nombre;

        /* Obtenemos la versi�n del Windows */
        winver = lowlevel_get_winversion();

        switch (winver) {

                case WINXP:
								estructura = malloc (sizeof(SHFILEOPSTRUCT));
								nombre = malloc (255*sizeof(char));

								strcpy(nombre, ruta_directorio);
								nombre[strlen(nombre)+1]='\0';

								/* Rellenamos la estructura con la info del directorio a eliminar */

								estructura->hwnd = NULL;
								estructura->wFunc = FO_DELETE;
								estructura->pFrom = nombre;
								estructura->pTo = NULL;
								estructura->fFlags = FOF_NOCONFIRMATION;

								result = SHFileOperation( estructura );
								if (result != 0) return ERR_FORMAT_ELIMINANDO_BACKUP;

								free(estructura);
								free(nombre);

                                break;

                default:        return ERR_LOWLEVEL_SIST_OPERATIVO_INCORRECTO;

        }

        return ERR_FORMAT_NO;
}

//---------------------------------------------------------------------------
/*! \brief Genera el identificador �nico del dispositivo

   Esta funci�n se encarga de generar aleatoriamente el identificador que se
   aplicar� en el formato criptogr�fico posteriormente

   \param id_dispositivo
			Identificador de dispositivo. La reserva de memoria se realiza de manera interna
			a la funci�n.

   \retval ERR_FORMAT_NO
             La funci�n se ejecut� correctamente.

   \retval ERR_FORMAT_SI
			 Ha ocurrido un error generando el identificador

   \retval ERR_FORMAT_SIST_OPERATIVO_INCORRECTO
             Se est� intentando ejecutar la librer�a en un sistema operativo no soportado

*/

int format_generar_id_dispositivo( OUT BYTE* id_dispositivo ) {

	HCRYPTPROV hTmpProv;

	/* Adquirimos un contexto para la generaci�n aleatoria */
	if (!CryptAcquireContext(&hTmpProv, NULL, NULL, PROV_RSA_FULL, CRYPT_VERIFYCONTEXT))
		return ERR_FORMAT_SI;

	/* Buffer para la escritura de datos */
	id_dispositivo = (BYTE *) malloc (NBYTES_ID*sizeof(char));
	if ( id_dispositivo == NULL ) {
		CryptReleaseContext(hTmpProv,0);
		return ERR_FORMAT_SI;
	}

	/* Lo inicializamos con valores aleatorios */
	if (!CryptGenRandom( hTmpProv, NBYTES_ID, id_dispositivo )) {
		CryptReleaseContext(hTmpProv,0);
		return ERR_FORMAT_SI;
	}

	/* Liberamos el contexto */
	CryptReleaseContext(hTmpProv,0);

	return ERR_FORMAT_NO;

}

//---------------------------------------------------------------------------
/*! \brief Almacena temporalmente el identificador del dispositivo en el
		   propio dispositivo

   Esta funci�n almacena el identificador generado previamente en el boot code
   del dispositivo. El objetivo de la funci�n es no perder el identificador
   del dispositivo del cual hemos almacenado en disco una copia de seguridad,
   para poder restaurarla en caso de que ocurra alg�n error en el proceso antes
   de iniciar el formato de la zona criptogr�fica.

   Es requisito indispensable que el dispositivo disponga de MBR.

   Adem�s, se almacena temporalmente un fichero con el contenido anterior del
   boot sector que se ha sobreescrito. Este contenido ser� restaurado por
   format_restaurar_id_dispositivo.

   \param handle
            Manejador que se obtiene al abrir el dispositivo.

   \param id_dispositivo
			El identificador �nico del dispositivo.

   \retval ERR_FORMAT_NO
             La funci�n se ejecut� correctamente.

   \retval ERR_FORMAT_SI
			 Ha ocurrido un error almacenando el identificador en el dispositivo

   \retval ERR_FORMAT_SIST_OPERATIVO_INCORRECTO
             Se est� intentando ejecutar la librer�a en un sistema operativo no soportado

*/

int format_guardar_id_dispositivo( IN HANDLE_DISPOSITIVO handle, IN BYTE* id_dispositivo , IN int dispositivo) {

	int result;
	int tiene;

	/* Comprobamos si el dispositivo dispone de MBR */
	result = lowlevel_tiene_mbr( handle, &tiene, dispositivo);
	if ( result != ERR_LOWLEVEL_NO ) return ERR_FORMAT_SI;

	/* Para poder almacenar el identificador, se requiere que el dispositivo tenga MBR */
	if ( !tiene ) return ERR_FORMAT_SI;

	/* Nos guardamos el trozo del boot code que vamos a sobreescribir, 
	   en un fichero temporal con el nombre de id_dispositivo pasado a hexadecimal */


	/* Bytes 400 - 425: id_dispositivo */

		/* El id_dispositivo queda delimitado por 0x69 0x69 0x69 -id_dispositivo- 0x69 0x69 0x69 */

	return ERR_FORMAT_NO;

}


//---------------------------------------------------------------------------
/*! \brief Obtiene el identificador de dispositivo almacenado temporalmente
		   en el mismo, mediante la funci�n format_guardar_id_dispositivo

   Esta funci�n lee el identificador de dispositivo que se ha almacenado de manera temporal
   en el mismo. Adem�s, restaura el trozo del boot code que se ha sobreescrito al almacenar
   el id_dispositivo en �l.

   \param handle
            Manejador que se obtiene al abrir el dispositivo.

   \param id_dispositivo
			El identificador �nico del dispositivo.

   \retval ERR_FORMAT_NO
             La funci�n se ejecut� correctamente.

   \retval ERR_FORMAT_SI
			 Ha ocurrido un error leyendo el identificador del dispositivo

   \retval ERR_FORMAT_SIST_OPERATIVO_INCORRECTO
             Se est� intentando ejecutar la librer�a en un sistema operativo no soportado

*/

int format_restaurar_id_dispositivo( IN HANDLE_DISPOSITIVO handle, OUT BYTE* id_dispositivo ) {


	/* Leemos el id_dispositivo de la zona 400 del boot code */


	/* Restauramos la zona 400 del boot code */

	return ERR_FORMAT_NO;

}


//---------------------------------------------------------------------------
/*! \brief Obtiene el identificador de dispositivo almacenado temporalmente
		   en el mismo, mediante la funci�n format_guardar_id_dispositivo

   Esta funci�n lee el identificador de dispositivo que se ha almacenado de manera temporal
   en el mismo.

   \param handle
            Manejador que se obtiene al abrir el dispositivo.

   \param id_dispositivo
			El identificador �nico del dispositivo.

   \retval ERR_FORMAT_NO
             La funci�n se ejecut� correctamente.

   \retval ERR_FORMAT_SI
			 Ha ocurrido un error leyendo el identificador del dispositivo

   \retval ERR_FORMAT_SIST_OPERATIVO_INCORRECTO
             Se est� intentando ejecutar la librer�a en un sistema operativo no soportado

*/

int format_leer_id_dispositivo( IN HANDLE_DISPOSITIVO handle, OUT BYTE* id_dispositivo ) {


	/* Leemos el id_dispositivo de la zona 400 del boot code */

	return ERR_FORMAT_NO;

}

//---------------------------------------------------------------------------
/*! \brief Crea la zona criptogr�fica en la partici�n indicada

   Crea la zona criptogr�fica en la partici�n indicada. El formato de esa
   partici�n sigue el formato definido para el USBPKI

   \param handle
            Manejador que se obtiene al abrir el dispositivo.

   \param particion
            Estructura de informaci�n de la partici�n sobre la que se va a
            aplicar el formato usbpki.

   \param password
            La password con la que se cifrar� la partici�n.

   \param id_dispositivo
			El identificador �nico del dispositivo. Si este par�metro es NULL,
			se generar� internamente en la funci�n.

   \retval ERR_FORMAT_NO
             La funci�n se ejecut� correctamente.

   \retval ERR_FORMAT_SI
			 Ha ocurrido un error creando la zona criptogr�fica

   \retval ERR_FORMAT_SIST_OPERATIVO_INCORRECTO
             Se est� intentando ejecutar la librer�a en un sistema operativo no soportado

*/

int format_crear_zona_cripto( IN HANDLE_DISPOSITIVO handle, IN PARTICION particion, IN PASSWORD password, IN BYTE *id_dispositivo, IN int dispositivo )
{
        char *iden = "UJI - Clauer PKI storage system";
        BYTE *buffer;
        BYTE *buffer_cifrado;
        BYTE *ptr;
        DWORD bytesEscritos, tamBufferCifrado;
        BYTE bufftemp[TAM_BLOQUE];
        int result;
        unsigned int indice;
        unsigned long sector_relativo;
        unsigned int total_bloques;
		int winver, bytesSector;

		HCRYPTPROV hTmpProv;

        /* Obtenemos la versi�n del Windows */
        winver = lowlevel_get_winversion();

		switch (winver) {

                case WINXP:
					/* Adquirimos un contexto para la generaci�n aleatoria */
					if (!CryptAcquireContext(&hTmpProv, NULL, NULL, PROV_RSA_FULL, CRYPT_VERIFYCONTEXT))
						return ERR_FORMAT_SI;

					/* Buffer para la escritura de datos */
					buffer = (BYTE *) malloc (TAM_BLOQUE);
					if ( ! buffer ) {
						CryptReleaseContext(hTmpProv, 0);
						printf("1\n");
						return ERR_FORMAT_SI;
					}

					/* Lo inicializamos con valores aleatorios */
					if (!CryptGenRandom( hTmpProv, TAM_BLOQUE, buffer )) {
						free(buffer);
						CryptReleaseContext(hTmpProv,0);
						printf("2\n");
						return ERR_FORMAT_SI;
					}

					/* El puntero apuntar� a la siguiente posici�n en la que escribir */
					ptr = buffer;

					/* Llevamos un control del n�mero de bytes escritos en el buffer */
					bytesEscritos = 0;

					/* Zona de informaci�n */
					/* ------------------- */
					
					CryptReleaseContext(hTmpProv,0);

					/* Inicializamos funci�n de generaci�n de llaves, etc. */

					result = format_ini( handle, password, buffer+40 );
					if ( result != ERR_FORMAT_NO ) {
						free(buffer);
						printf("3\n");
						return ERR_FORMAT_SI;
					}

					/* Campo 1: IDEN String (40 bytes = 32 bytes + 8 bytes padding), se almacena cifrado */

					buffer_cifrado = (BYTE *) malloc ( NBYTES_IDEN );
					if ( ! buffer_cifrado ) {
						free(buffer);
						printf("4\n");
						return ERR_FORMAT_SI;
					}
					memcpy(buffer_cifrado, iden, 32);
					tamBufferCifrado = 32;

					/* Le pasamos 32 bytes y nos devuelve 40 bytes cifrados */

					if ( ! CryptEncrypt( ghKey, 0, TRUE, 0, buffer_cifrado, &tamBufferCifrado, NBYTES_IDEN ) ) {
						free(buffer_cifrado);
						free(buffer);
						printf("5\n");
						return ERR_FORMAT_SI;
					}

					memcpy(ptr, buffer_cifrado, NBYTES_IDEN);
					ptr += NBYTES_IDEN;
					bytesEscritos += NBYTES_IDEN;
					free(buffer_cifrado);

					/* Campo 2: Identificador del dispositivo */
					
					if ( id_dispositivo != NULL ) 
						memcpy( ptr, id_dispositivo, 20 );

					/* Si id_dispositivo es NULL, no escribimos nada, ya ten�amos el bloque con 
					   informaci�n aleatoria de antes */

					ptr += NBYTES_ID;
					bytesEscritos += NBYTES_ID;

					/* Campo 3: Tama�o de la zona reservada (NRSV unidades de 10k) */

					*((unsigned int *)ptr) = NRSV;
					ptr += NBYTES_NRSV;
					bytesEscritos += NBYTES_NRSV;

					/* Campo 4: Current block */

					*((unsigned int *)ptr) = CURRENT_BLOCK;
					ptr += NBYTES_CURRENT;
					bytesEscritos += NBYTES_CURRENT;

					/* Campo 5: Total bloques (descontamos la zona de info y la reservada) */
					if ( dispositivo > -1 ){
						bytesSector= lowlevel_obtenerBytesSector(dispositivo);
						if ( bytesSector < 0 ){
							printf("6\n");
						  return ERR_FORMAT_SI;
						}
					}
					else
						bytesSector= 512;

					total_bloques = ((particion.num_sectors)*bytesSector/TAM_BLOQUE) - 1 - NRSV ;
					*((unsigned int *)ptr)=total_bloques;
					ptr+=NBYTES_TOTAL;
					bytesEscritos+=NBYTES_TOTAL;

					/* Campo 6: Versi�n del formato */

					*((unsigned int *)ptr)=VERSION;
					ptr+=NBYTES_VERSION;
					bytesEscritos+=NBYTES_VERSION;

					/* Escritura del bloque de informaci�n */

					/* Escribimos la zona de informaci�n en el primer bloque */

					sector_relativo = particion.rel_sectors;
					result = lowlevel_escribir_buffer( handle, buffer, sector_relativo, TAM_BLOQUE/bytesSector, dispositivo );
					if (result!=ERR_LOWLEVEL_NO)  {
						free(buffer);
						printf("7\n");
						return ERR_FORMAT_SI;
					}

					sector_relativo+=TAM_BLOQUE/bytesSector;

					free(buffer);


					/* Zona reservada y zona de objetos*/
					/* ------------------------------- */

					buffer = (BYTE *) malloc ( TAM_BUFFER );
					if ( ! buffer ) {
						free(buffer);
						printf("8\n");
						return ERR_FORMAT_SI;
					}

					bytesEscritos=0;
					ptr = buffer;
					
					/* Establecemos la semilla */
					srand( (unsigned) time (NULL) );

					for (indice=0; indice < (total_bloques + NRSV) ; indice++) {

							if (bytesEscritos == TAM_BUFFER) {
									result = lowlevel_escribir_buffer( handle, buffer, sector_relativo, TAM_BUFFER/bytesSector, dispositivo );
									sector_relativo+=TAM_BUFFER/bytesSector;
									if ( result != ERR_LOWLEVEL_NO ) {
										free(buffer);
										printf("9\n");
										return ERR_FORMAT_SI;
									}
									bytesEscritos=0;
									ptr = buffer;
							}

							/*CryptGenRandom( ghProv, TAM_BLOQUE, bufftemp );*/

							bufftemp[0] = rand() % 85;	/* Bloques blancos por defecto */

							memcpy(ptr,bufftemp,TAM_BLOQUE);
							ptr+=TAM_BLOQUE;
							bytesEscritos+=TAM_BLOQUE;
					}

					/* Si no ha llenado el buffer */
					if ((bytesEscritos >0 ) && (bytesEscritos <= TAM_BUFFER)) {
							result = lowlevel_escribir_buffer( handle, buffer, sector_relativo, bytesEscritos/bytesSector, dispositivo);
							if ( result != ERR_LOWLEVEL_NO ) {
								free(buffer);
								printf("10\n");
								return ERR_FORMAT_SI;
							}
					}

					free(buffer);

                    break;

                default:        
					
					return ERR_LOWLEVEL_SIST_OPERATIVO_INCORRECTO;

        }

        return ERR_FORMAT_NO;
}

//---------------------------------------------------------------------------
/*! \brief Crea una copia de seguridad de la zona criptogr�fica del dispositivo

   Lee la zona criptogr�fica del dispositivo, haciendo uso del par�metro
   particion, para leer de esa partici�n. Almacena la copia cifrada en 
   ruta_directorio_origen

   \param handle
            Manejador que se obtiene al abrir el dispositivo

   \param password
			La password que protege la partici�n criptogr�fica

   \param particion
            Los datos de la partici�n criptogr�fica

   \param ruta_backup
            Ruta del fichero en el que se almacenar� la copia de seguridad

   \retval ERR_FORMAT_NO
             La funci�n se ejecut� correctamente.

   \retval ERR_FORMAT_CREANDO_TEMP
			 Ha ocurrido un error creando el fichero temporal para la copia de seguridad

   \retval ERR_FORMAT_CREANDO_BACKUP
             Ha ocurrido un error durante la creaci�n de la copia de seguridad

   \retval ERR_FORMAT_SIST_OPERATIVO_INCORRECTO
             Se est� intentando ejecutar la librer�a en un sistema operativo no soportado

   \todo	Hacer copia de seguridad de la zona reservada
*/

int format_crear_backup_cripto( IN HANDLE_DISPOSITIVO handle, IN PASSWORD password, IN PARTICION particion, OUT char* ruta_backup, IN int dispositivo ) {

	int winver, bytesSector;
	int result;
	BYTE salt[NBYTES_ID];
	BYTE bloque[TAM_BLOQUE];
	DWORD tamBloque;
	unsigned int i;
	FILE *backup;
	BYTE *ptr;
	unsigned long total_bloques;
	BYTE *buffer;
	unsigned int nrsv;
	unsigned long sector_relativo;
	unsigned long iteraciones;
	unsigned long bytes_restantes;

    /* Obtenemos la versi�n del Windows */
    winver = lowlevel_get_winversion();

    switch (winver) {

        case WINXP:

						bytesSector= lowlevel_obtenerBytesSector(dispositivo);
						if ( bytesSector < 0 )
							return ERR_FORMAT_SI;

						/* Leemos y desciframos el bloque de informaci�n */
						result = lowlevel_leer_buffer(handle, bloque, particion.rel_sectors, TAM_BLOQUE/bytesSector, dispositivo);
						if ( result != ERR_LOWLEVEL_NO ) return ERR_FORMAT_CREANDO_BACKUP;

						tamBloque = TAM_BLOQUE;
						if ( !CryptDecrypt(ghKey,0,TRUE,0, bloque, &tamBloque) ) {
							return ERR_FORMAT_CREANDO_BACKUP;
						}

						ptr = bloque;
						ptr = ptr + NBYTES_IDEN;

						memcpy(salt, ptr, NBYTES_ID);

						ptr += NBYTES_ID;
						
						nrsv = *((unsigned int *)ptr);

						ptr = ptr + NBYTES_NRSV + NBYTES_CURRENT;

						/* N�mero total de bloques */

						total_bloques = *((unsigned long *) ptr);

						ptr += NBYTES_TOTAL;


                        /* Obtenemos un nombre de fichero temporal para la copia de seguridad, en el disco duro */

						result = GetTempFileName("C:\\","USB",0,ruta_backup);
						if ( result == 0 ) return ERR_FORMAT_CREANDO_TEMP;

						/* Creamos la copia de seguridad */

						backup=fopen(ruta_backup, "wb");
						if ( backup == NULL ) return ERR_FORMAT_CREANDO_BACKUP;

						/* Almacenamos el salt (primeros 20 bytes del fichero) junto con la copia de seguridad */
						result = fwrite ( salt, NBYTES_ID, 1, backup );
						if ( result != 1 ) return ERR_FORMAT_CREANDO_BACKUP;


						/* Escribimos el primer bloque en la copia de seguridad */

						result = fwrite( bloque, TAM_BLOQUE, 1, backup );
						if ( result != 1 ) return ERR_FORMAT_CREANDO_BACKUP;


						/* Recorremos el resto de bloques y almacenamos los que sean no nulos */


						iteraciones = (total_bloques*TAM_BLOQUE) / TAM_BUFFER;
						bytes_restantes = (total_bloques*TAM_BLOQUE) % TAM_BUFFER;

						sector_relativo = particion.rel_sectors + (TAM_BLOQUE/512)*(nrsv + 1);	/* Saltamos la zona de informaci�n y la reservada */

						/* Con buffering en lectura, hacemos la copia de los bloques no vac�os */

						for ( i=0 ; i < iteraciones ; i++ ) {

							buffer = (BYTE *) malloc (TAM_BUFFER * sizeof(char));

							/* Leemos el buffer */

							result = lowlevel_leer_buffer( handle, buffer, sector_relativo, TAM_BUFFER/bytesSector, dispositivo );
							if ( result != ERR_LOWLEVEL_NO ) return ERR_FORMAT_CREANDO_BACKUP;

							/* Tratamiento para cada uno de los bloques del buffer */
							ptr = buffer;

							for ( i=0 ; i < TAM_BUFFER/TAM_BLOQUE ; i++ ) {

								tamBloque = TAM_BLOQUE;
								if ( !CryptDecrypt( ghKey, 0, TRUE, 0, ptr, &tamBloque ) ) return ERR_FORMAT_CREANDO_BACKUP;

								/* Si el bloque es distinto de blanco, lo escribimos en la copia de seguridad, cifrado */

								if ( !BLOQUE_ES_VACIO(ptr) ) {
									result = fwrite( ptr, TAM_BLOQUE, 1, backup );
									if ( result != 1 ) return ERR_FORMAT_CREANDO_BACKUP;
								}

								ptr+=TAM_BLOQUE;
							}

							sector_relativo += TAM_BUFFER/512;

							free(buffer);
						}

						/* Resto del buffering */

						if ( bytes_restantes > 0 ) {

							buffer = (BYTE *) malloc (bytes_restantes*sizeof(char));

							/* Leemos todos los bloques que nos quedan por procesar */

							result = lowlevel_leer_buffer( handle, buffer, sector_relativo, bytes_restantes/bytesSector, dispositivo );
							if ( result != ERR_LOWLEVEL_NO ) return ERR_FORMAT_CREANDO_BACKUP;


							/* Tratamiento para cada uno de los bloques */

							ptr = buffer;

							for ( i=0 ; i < (bytes_restantes/TAM_BLOQUE) ; i++ ) {

								/* Lo desciframos */

								tamBloque = TAM_BLOQUE;
								if ( !CryptDecrypt( ghKey, 0, TRUE, 0, ptr, &tamBloque ) ) return ERR_FORMAT_CREANDO_BACKUP;

								/* Si es distinto de blanco, lo escribimos en la copia de seguridad, cifrado */

								if ( !BLOQUE_ES_VACIO(ptr) ) {

									result = fwrite( ptr, TAM_BLOQUE, 1, backup );
									if ( result != 1 ) return ERR_FORMAT_CREANDO_BACKUP;
								}

								ptr+=TAM_BLOQUE;
							}

							free(buffer);
						}

						fclose(backup);

						break;

        default:        return ERR_FORMAT_SIST_OPERATIVO_INCORRECTO;

    }


	return ERR_FORMAT_NO;
}

//---------------------------------------------------------------------------
/*! \brief Restaura la copia de seguridad de la zona criptogr�fica en el dispositivo

   Lee la copia de seguridad de la ruta indicada mediante ruta_backup, la descifra
   mediante la password antigua y la restaura en el dispositivo, cifr�ndola con la 
   password proporcionada (password_nueva).

   \param handle
            Manejador que se obtiene al abrir el dispositivo.

   \param ruta_backup
			Ruta de la copia de seguridad que se desea restaurar

   \param particion
            Estructura de informaci�n de la partici�n sobre la que se va a
            restaurar la copia de seguridad

   \param password_antigua
			La password para descifrar la copia de seguridad

   \param password_nueva
            La password con la que se cifrar� la partici�n

   \retval ERR_FORMAT_NO
            La funci�n se ejecut� correctamente

   \retval ERR_FORMAT_RESTAURANDO_BACKUP
			Ha ocurrido un error al restaurar la copia de seguridad en el dispositivo

   \retval ERR_FORMAT_SIST_OPERATIVO_INCORRECTO
             Se est� intentando ejecutar la librer�a en un sistema operativo no soportado

   \todo	Restaurar la copia de seguridad de la zona reservada

*/

int format_restaurar_backup_cripto( IN HANDLE_DISPOSITIVO handle, IN char* ruta_backup, IN PARTICION particion, IN PASSWORD password_antigua, IN PASSWORD password_nueva, IN int dispositivo ) {

	int winver, bytesSector;
	int result;
	FILE * backup;
	/*BYTE salt_nueva[NBYTES_ID];*/
	BYTE salt_antigua[NBYTES_ID];
	BYTE bloque[TAM_BLOQUE];
	BYTE bloque_temp[TAM_BLOQUE];
	BYTE * ptr;
	unsigned long num_bloques;
	unsigned long sector_relativo;
	unsigned long total_bloques_backup;
	unsigned long i;
	unsigned long tamanyo;
	unsigned int nrsv;
	BYTE *buffer;
	DWORD tamBloque;


    /* Obtenemos la versi�n del Windows */
    winver = lowlevel_get_winversion();

    switch (winver) {

        case WINXP:


						bytesSector= lowlevel_obtenerBytesSector(dispositivo);
						if ( bytesSector < 0 )
							return ERR_FORMAT_SI;

						/* Abrimos el fichero de copia de seguridad */

						backup = fopen ( ruta_backup, "rb" );
						if ( backup == NULL ) return ERR_FORMAT_RESTAURANDO_BACKUP;


						fseek(backup, 0, SEEK_END);		/* Nos movemos hasta el final, para ver el tama�o */

						tamanyo = ftell(backup);
						if ( tamanyo < 0 ) return ERR_FORMAT_RESTAURANDO_BACKUP;

						fseek(backup, 0, SEEK_SET);	/* Volvemos al principio, para operar con �l */


						/* Leemos la salt */
						result = fread( salt_antigua, NBYTES_ID, 1, backup );
						if ( result != 1 ) return ERR_FORMAT_RESTAURANDO_BACKUP;

						/* Leemos el bloque de informaci�n */
						result = fread( bloque, TAM_BLOQUE ,1, backup );
						if ( result != 1 ) return ERR_FORMAT_RESTAURANDO_BACKUP;

						/* Hay que descifrar esto usando la password y salt antiguas */
						tamBloque = TAM_BLOQUE;
						if ( !CryptDecrypt( ghKey, 0, TRUE, 0, bloque, &tamBloque ) ) return ERR_FORMAT_CREANDO_BACKUP;

						/* Modificamos el n�mero de bloques de la zona de informaci�n, lo calculamos de nuevo en base
						   a los datos de la nueva partici�n */

						ptr = bloque;
						ptr = ptr + NBYTES_IDEN + NBYTES_ID;
							
						nrsv = *((unsigned int *)ptr);
							
						ptr = ptr + NBYTES_NRSV + NBYTES_CURRENT;
						num_bloques = ((particion.num_sectors) * 512) / TAM_BLOQUE - 1 - NRSV;
						*((unsigned int *)ptr) = num_bloques;

						ptr += NBYTES_TOTAL;

						/* Obtenemos la salt nueva */
						/*result = lowlevel_leer_salt( handle, salt_nueva );
						if (result != ERR_LOWLEVEL_NO) return ERR_FORMAT_RESTAURANDO_BACKUP;*/

						/* Lo ciframos usando la password y salt nueva */
						/*result = CRYPTO_PBE_Cifrar( password_nueva, salt_nueva, 0, CRYPTO_CIPHER_AES_128_CBC, bloque_descifrado, TAM_BLOQUE, bloque_cifrado, &tam_cifrado );
						if ((result !=ERR_CW_NO) || (tam_cifrado != TAM_BLOQUE)) return ERR_FORMAT_RESTAURANDO_BACKUP;*/


						/* En primer lugar, marcamos como vac�os todos los bloques en el dispositivo */

						sector_relativo = particion.rel_sectors + (TAM_BLOQUE/512);		/* Saltamos el bloque de info */

						buffer = (BYTE *) malloc ( (num_bloques + nrsv)*TAM_BLOQUE*sizeof(int) );

						ptr = buffer;

						for ( i=0; i < num_bloques + nrsv; i++ ) {

							CryptGenRandom( ghProv, TAM_BLOQUE, bloque_temp );

							bloque_temp[0] = rand() % 85;	/* Bloques blancos por defecto */

							/* Lo ciframos */
							/*result = CRYPTO_Cifrar( &CKprivatekey, 0, CRYPTO_CIPHER_AES_128_CBC, bloque_temp_descifrado, TAM_BLOQUE, bloque_temp_cifrado, &tam_cifrado );
							if ( (result != ERR_CW_NO ) || (tam_cifrado != TAM_BLOQUE) ) return ERR_FORMAT_CREANDO_BACKUP;*/

							/* Lo a�adimos al buffer */

							memcpy(ptr, bloque_temp, TAM_BLOQUE);

							ptr+=TAM_BLOQUE;
						}

						/* Escribimos todo el buffer */

						result = lowlevel_escribir_buffer( handle, buffer, sector_relativo, ((num_bloques+nrsv)*TAM_BLOQUE)/512, -1 );
						if ( result != ERR_LOWLEVEL_NO ) return ERR_FORMAT_RESTAURANDO_BACKUP;

						free(buffer);

						/* Restauramos la copia en el dispositivo */

						sector_relativo = particion.rel_sectors;	/* Copiamos desde el principio de la partici�n */

						/* Bloque de informaci�n */

						result = lowlevel_escribir_buffer( handle, bloque, sector_relativo, TAM_BLOQUE/bytesSector, dispositivo );
						if ( result != ERR_LOWLEVEL_NO ) return ERR_FORMAT_RESTAURANDO_BACKUP;

						sector_relativo += TAM_BLOQUE/512;

						sector_relativo += nrsv*10;	/* Saltamos los bloques reservados */

						/* Ahora escribimos el resto */

						/* El total de bloques es el tama�o del fichero quitando el salt y el bloque de informaci�n */

						total_bloques_backup = ((tamanyo - 8) / TAM_BLOQUE) - 1;


						buffer = (BYTE *) malloc (total_bloques_backup*TAM_BLOQUE*sizeof(char));

						ptr = buffer;

						for ( i = 0 ; i < total_bloques_backup ; i++ ) {

							/* Leemos el bloque */

							result = fread( bloque, TAM_BLOQUE, 1, backup );
							if ( result != 1 ) return ERR_FORMAT_RESTAURANDO_BACKUP;

							/* Lo copiamos en el buffer */

							memcpy(ptr, bloque, TAM_BLOQUE);

							ptr += TAM_BLOQUE;

						}

						/* Escribimos todo el buffer */
						result = lowlevel_escribir_buffer( handle, buffer, sector_relativo, (total_bloques_backup*TAM_BLOQUE)/bytesSector, dispositivo );
						if ( result != ERR_LOWLEVEL_NO ) return ERR_FORMAT_RESTAURANDO_BACKUP;

						free(buffer);


						fclose(backup);

						break;

        default:        return ERR_FORMAT_SIST_OPERATIVO_INCORRECTO;

    }


	return ERR_FORMAT_NO;
}

//---------------------------------------------------------------------------
/*! \brief Elimina la copia de seguridad de la zona criptogr�fica

	Elimina de la ruta indicada la copia de seguridad realizada previamente

	\param handle
			Manejador que se obtiene al abrir el dispositivo

    \param password
			Password que protege la zona criptogr�fica del dispositivo. Si se
			especifica (diferente de NULL), se eliminar� la copia de seguridad
			asociada a ese dispositivo, y no otra

	\param ruta_backup
            Ruta en la que se encuentra la copia de seguridad que se desea eliminar

	\retval ERR_FORMAT_NO
            La funci�n se ejecut� correctamente.

	\retval ERR_FORMAT_ELIMINANDO_BACKUP
			Ha ocurrido un error eliminando la copia de seguridad

	\retval ERR_FORMAT_SIST_OPERATIVO_INCORRECTO
			El sistema operativo que est� utilizando no est� soportado por esta librer�a
*/

int format_eliminar_backup_cripto( IN HANDLE_DISPOSITIVO handle, IN PASSWORD password, IN char* ruta_backup ) {

	int winver;
	int result;

    /* Obtenemos la versi�n del Windows */
    winver = lowlevel_get_winversion();

    switch (winver) {

        case WINXP:
						result = DeleteFile(ruta_backup);
						if (result == 0) return ERR_FORMAT_ELIMINANDO_BACKUP;

						break;

        default:        return ERR_FORMAT_SIST_OPERATIVO_INCORRECTO;

    }


	return ERR_FORMAT_NO;
}


//---------------------------------------------------------------------------
/*! \brief Crea una copia de seguridad del Master Boot Record del dispositivo

   Crea una copia de seguridad del MBR en el disco duro. La copia se almacenar�
   en el fichero que se indica mediante el par�metro ruta_backup.

   \param handle
            Manejador que se obtiene al abrir el dispositivo

   \param password
			Password que protege la zona criptogr�fica del dispositivo. Si se
			especifica (diferente de NULL), se crear� una copia de seguridad
			asociada a ese dispositivo.

   \param ruta_backup
            Ruta del fichero en el que se almacenar� la copia de seguridad

   \retval ERR_FORMAT_NO
            La funci�n se ejecut� correctamente.

   \retval ERR_FORMAT_CREANDO_TEMP
			Ha ocurrido un error creando el fichero temporal para la copia de seguridad

   \retval ERR_FORMAT_CREANDO_BACKUP
            Ha ocurrido un error durante la creaci�n de la copia de seguridad

   \retval ERR_FORMAT_LECTURA_MBR_INCORRECTA
			No se ha podido leer el sector MBR del dispositivo correctamente 

   \retval ERR_FORMAT_SIST_OPERATIVO_INCORRECTO
            Se est� intentando ejecutar la librer�a en un sistema operativo no soportado
*/

int format_crear_backup_mbr( IN HANDLE_DISPOSITIVO handle, IN PASSWORD password, OUT char* ruta_backup, IN int dispositivo ) {

	int result;
	FILE* fichero;
	unsigned char mbr [512];
	int winver;

    /* Obtenemos la versi�n del Windows */
    winver = lowlevel_get_winversion();

	switch (winver) {

                case WINXP:

							/* Leemos el MBR */
							result = lowlevel_leer_sector( handle, 0, mbr, dispositivo );
							if ( result != ERR_LOWLEVEL_NO ) return ERR_FORMAT_LECTURA_MBR_INCORRECTA;

							/* Creamos el fichero temporal */

							/*result = format_obtener_ruta_temporal( "USB", */

							result = GetTempFileName("C:\\","MBR",0,ruta_backup);
							if ( result == 0 ) return ERR_FORMAT_CREANDO_TEMP;
							fichero = fopen(ruta_backup, "wb");

							/* Escribimos el MBR en el fichero temporal */
							result = fwrite(mbr, 512, 1, fichero);
							if (result != 1) return ERR_FORMAT_CREANDO_BACKUP;

							fclose(fichero);

							break;

        default:		return ERR_FORMAT_SIST_OPERATIVO_INCORRECTO;

    }

	return ERR_FORMAT_NO;
}

//---------------------------------------------------------------------------
/*! \brief Restaura la copia del MBR en el dispositivo

   Restaura la copia de seguridad realizada previamente del MBR en el dispositivo.

   \param handle
            Manejador que se obtiene al abrir el dispositivo.

   \param password
			Password que protege la zona criptogr�fica del dispositivo. Si se
			especifica (diferente de NULL), se restaurar� la copia de seguridad
			asociada a ese dispositivo, y no otra

   \param ruta_backup
			Ruta de la copia de seguridad que se desea restaurar

   \retval ERR_FORMAT_NO
            La funci�n se ejecut� correctamente

   \retval ERR_FORMAT_RESTAURANDO_BACKUP
			Ha ocurrido un error al restaurar la copia de seguridad en el dispositivo

   \retval ERR_FORMAT_ESCRITURA_MBR_INCORRECTA
			No se ha podido escribir en el MBR del dispositivo de forma correcta

   \retval ERR_FORMAT_SIST_OPERATIVO_INCORRECTO
            Se est� intentando ejecutar la librer�a en un sistema operativo no soportado
*/

int format_restaurar_backup_mbr( IN HANDLE_DISPOSITIVO handle, IN PASSWORD password, IN char* ruta_backup, IN int dispositivo ) {

	FILE* fichero;
	unsigned char mbr [512];
	int result;
	int winver;

    /* Obtenemos la versi�n del Windows */
    winver = lowlevel_get_winversion();

	switch (winver) {

                case WINXP:

							/* Leemos el fichero de copia de seguridad */
							fichero = fopen(ruta_backup, "rb");

							result = fread(mbr, 512, 1, fichero);
							if (result != 1) return ERR_FORMAT_RESTAURANDO_BACKUP;

							/* Restauramos el MBR en el dispositivo */
							result = lowlevel_escribir_sector( handle, 0, mbr, dispositivo );
							if ( result != ERR_LOWLEVEL_NO ) return ERR_FORMAT_ESCRITURA_MBR_INCORRECTA;

							fclose(fichero);

							break;

        default:		return ERR_FORMAT_SIST_OPERATIVO_INCORRECTO;

    }

	return ERR_FORMAT_NO;
}


//---------------------------------------------------------------------------
/*! \brief Elimina la copia de seguridad del MBR

	Elimina de la ruta indicada la copia de seguridad del MBR realizada previamente

	\param handle
            Manejador que se obtiene al abrir el dispositivo.

	\param password
			Password que protege la zona criptogr�fica del dispositivo. Si se
			especifica (diferente de NULL), se eliminar� la copia de seguridad
			asociada a ese dispositivo, y no otra

	\param  ruta_backup
            Ruta en la que se encuentra la copia de seguridad que se desea eliminar

	\retval ERR_FORMAT_NO
            La funci�n se ejecut� correctamente.

	\retval ERR_FORMAT_ELIMINANDO_BACKUP
			Ha ocurrido un error eliminando la copia de seguridad

	\retval ERR_FORMAT_SIST_OPERATIVO_INCORRECTO
			El sistema operativo que est� utilizando no est� soportado por esta librer�a
*/

int format_eliminar_backup_mbr( IN HANDLE_DISPOSITIVO handle, IN PASSWORD password, IN char* ruta_backup ) {

	int winver;
	int result;

    /* Obtenemos la versi�n del Windows */
    winver = lowlevel_get_winversion();

    switch (winver) {

        case WINXP:

						result = DeleteFile(ruta_backup);
						if (result == 0) return ERR_FORMAT_ELIMINANDO_BACKUP;

						break;

        default:        return ERR_FORMAT_SIST_OPERATIVO_INCORRECTO;

    }


	return ERR_FORMAT_NO;
}


//---------------------------------------------------------------------------
/*! \brief Obtiene el nombre de una ruta de un fichero temporal

	Obtiene la ruta de un fichero temporal para almacenar copias de seguridad
	temporales. La funci�n toma la cadena pasada como par�metro y la a�ade
	como parte del nombre del fichero. Por otra parte, el nombre del fichero
	tambi�n incluye el identificador del dispositivo.

	\param handle
			Manejador del dispositivo para acceder al mismo.

	\param cadena
			Cadena descriptiva. Se utiliza para indicar el objetivo de la copia
			de seguridad en el nombre del fichero temporal.

	\param ruta
            Ruta completa del nombre del fichero temporal obtenido

	\retval ERR_FORMAT_NO
            La funci�n se ejecut� correctamente.

	\retval ERR_FORMAT_SI
			Ha ocurrido un error obteniendo el nombre de la ruta del fichero temporal

	\retval ERR_FORMAT_SIST_OPERATIVO_INCORRECTO
			El sistema operativo que est� utilizando no est� soportado por esta librer�a
*/

int format_obtener_ruta_temporal( IN HANDLE_DISPOSITIVO handle, IN char* cadena, OUT char* ruta, IN int dispositivo ) {

	int winver;
	int result;
	char directorio [255];
	BYTE id [20];
	char id_legible [20];

    /* Obtenemos la versi�n del Windows */
    winver = lowlevel_get_winversion();

    switch (winver) {

        case WINXP:
						/* Nombre de directorio */
						result = GetTempPath( 255, directorio );
						if ( result == 0 ) return ERR_FORMAT_SI;

						/* Identificador del dispositivo */
						result = get_id( handle, id, dispositivo );
						if ( result != ERR_FORMAT_NO ) return ERR_FORMAT_SI;

						/* Lo transformamos para hacerlo legible */


						/* A�adimos el directorio a la ruta */
						strcat( ruta, directorio );

						/* A�adimos la cadena descriptiva */
						strcat( ruta, cadena );

						/* A�adimos el identificador transformado */
						strcat( ruta, id_legible );

						/*result = GetTempFileName( directorio, cadena, 0, ruta );
						if ( result == 0 ) return ERR_FORMAT_SI;*/

						break;

        default:        return ERR_FORMAT_SIST_OPERATIVO_INCORRECTO;

    }


	return ERR_FORMAT_NO;

}

//---------------------------------------------------------------------------
/*!
   \brief Formatea un dispositivo USB, aplicando el formato Clauer al mismo.

		Esta funci�n

   

	\param handle
			Manejador del dispositivo. Se obtiene abriendo el dispositivo de
			forma previa, con una enumeraci�n de los dispositivos insertados.

	\param descripcion
                         Cadena descriptiva del error, indicado mediante el par�metro
                         codigo_error

	\retval ERR_FORMAT_NO
             La funci�n se ejecut� correctamente

        \retval ERR_FORMAT_SI
             Ha ocurrido un error al obtener la cadena descriptiva del error

        \todo
             Incluir la descripci�n de todos los errores de libformat

 */

int format_formatear( IN HANDLE_DISPOSITIVO handle, int modo ) {


	/* Esta funci�n pretende hacer el formateo global, llamando a las funciones de esta librer�a */

	return ERR_FORMAT_NO;
}


//---------------------------------------------------------------------------
/*!
   \brief Obtiene una descripci�n del error indicado

    Dado un c�digo de error, obtiene una descripci�n textual del mismo.

	\param codigo_error
			 C�digo de error del que se quiere obtener la descripci�n

	\param descripcion
                         Cadena descriptiva del error, indicado mediante el par�metro
                         codigo_error

	\retval ERR_FORMAT_NO
             La funci�n se ejecut� correctamente

        \retval ERR_FORMAT_SI
             Ha ocurrido un error al obtener la cadena descriptiva del error

        \todo
             Incluir la descripci�n de todos los errores de libformat

 */

int format_error( IN int codigo_error, OUT char* descripcion ) {

        switch ( codigo_error ) {

                case ERR_FORMAT_NO:
                                strcpy( descripcion, "No ha ocurrido ningun error");
                                break;
                case ERR_FORMAT_SI:
                                strcpy( descripcion, "Ha ocurrido un error indefinido");
                                break;

                default:        strcpy( descripcion, "Codigo de error desconocido");
                                return ERR_FORMAT_SI;
        }

        return ERR_FORMAT_NO;
}


//---------------------------------------------------------------------------

/*! \brief Verifica la integridad del dispositivo

	Comprueba que los datos almacenados en la zona criptogr�fica sean correctos.
	La verificaci�n se realiza tanto de la zona de informaci�n como del descifrado
	correcto de cada uno de los bloques de la zona reservada y de la zona de objetos.

	\param handle
			Manejador del dispositivo para acceder al mismo.

	\param password
			Es la password que protege al dispositivo.

	\retval ERR_FORMAT_NO
            La funci�n se ejecut� correctamente.

	\retval ERR_FORMAT_SI
			Ha ocurrido un error verificando la integridad del dispositivo

    \retval ERR_FORMAT_ZONA_INFO_INCORRECTA
			Se ha detectado un error en la zona de informaci�n

    \retval ERR_FORMAT_ZONA_RESERVADA_INCORRECTA
			Se ha detectado un error en la zona reservada

    \retval ERR_FORMAT_ZONA_OBJETOS_INCORRECTA
			Se ha detectado un error en la zona de objetos

*/

int format_verificar_integridad( IN HANDLE_DISPOSITIVO handle, IN PASSWORD password, IN int dispositivo ) {


	BYTE bloque [TAM_BLOQUE];
    char *iden = "UJI - Clauer PKI storage system";
	int result, bytesSector;
	PARTICION particiones[4];
	BYTE *buffer;
	DWORD tam_buffer;

	bytesSector= lowlevel_obtenerBytesSector(dispositivo);
	if ( bytesSector < 0 )
		return ERR_FORMAT_SI;

	/* Obtenemos la informaci�n de acceso al dispositivo */

	result = lowlevel_leer_particiones( handle, particiones, dispositivo );
	if ( result != ERR_LOWLEVEL_NO ) return ERR_FORMAT_SI;

	result = lowlevel_leer_buffer( handle, bloque, particiones[3].rel_sectors, TAM_BLOQUE/bytesSector, dispositivo );
	if ( result != ERR_LOWLEVEL_NO ) return ERR_FORMAT_ZONA_INFO_INCORRECTA;
							
	/* Inicializaci�n del PKCS#5 */

	result = format_ini( handle, password, bloque+40 );
	if ( result != ERR_FORMAT_NO ) return ERR_FORMAT_SI;


	/* Verificaci�n de IDEN String */

	buffer = (BYTE *) malloc (NBYTES_IDEN*sizeof(BYTE));
	memcpy(buffer, bloque, NBYTES_IDEN);

	tam_buffer = 40;

	if ( !CryptDecrypt( ghKey, 0, TRUE, 0, buffer, &tam_buffer ) ) {
		free(buffer);
		return ERR_FORMAT_ZONA_INFO_INCORRECTA;
	}

	/* Comparamos los strings */

	if ( strncmp( iden, buffer, NBYTES_IDEN-8 ) != 0 ) {
		free(buffer);
		return ERR_FORMAT_ZONA_INFO_INCORRECTA;

	}

	free(buffer);

	/* Verificaci�n del tama�o de la zona reservada */
		/* Debe ser un n�mero menor que el total de bloques */

	/* Verificaci�n del current block */
		/* Debe ser un n�mero menor o igual que el total de bloques */

	/* Verificaci�n de los bloques de la zona reservada y zona de objetos */

	return ERR_FORMAT_NO;

}



/*! \brief Crea un fichero con el formato de la partici�n criptogr�fica
 *
 * Crea un fichero con el formato de la partici�n criptogr�fica
 *
 * \param fileName
 *        Nombre del fichero destino
 *
 * \param password
 *	      Contrase�a utilizada para cifrar la zona
 *
 * \param sizeBlocks
 *        N� de bloques de la zona de objetos deseado
 *
 * \retval TODOS
 *         Todos los valores de format_crear_zona_cripto
 *
 */

int format_crear_fichero_cripto ( IN char fileName[MAX_PATH], IN PASSWORD password, int sizeBlocks )
{
	int err;
	HANDLE_DISPOSITIVO hFile = 0;
	PARTICION particion;
	FILE *fp = NULL;
	long size;

	/* El tama�o del fichero ser� de 1 bloque de informaci�n + 1 bloque reservado + sizeBlock
	 * bloques para la zona de objetos
	 */

	size = (2 + sizeBlocks) * TAM_BLOQUE;

	fp = fopen(fileName, "wb");
	if ( ! fp ) 
		return ERR_FORMAT_SI;
	
	if ( fseek(fp, size-1, SEEK_SET) != 0 ) {
		err = ERR_FORMAT_SI;
		goto err_format_crear_fichero_cripto;
	}

	fwrite(&err, 1, 1, fp);
	if ( ferror(fp) ) {
		err = ERR_FORMAT_SI;
		goto err_format_crear_fichero_cripto;
	}

	if ( fclose(fp) != 0 ) {
		fp = NULL;
		err = ERR_FORMAT_SI;
		goto err_format_crear_fichero_cripto;
	}

	fp = NULL;
	

	/* Reutilizamos format_crear_zona_cripto(). El truco consiste en pasar
	 * el rel sectors de la partici�n 4 a 0
	 */

	if ( lowlevel_abrir_dispositivo_ex(fileName, &hFile) != ERR_LOWLEVEL_NO ) {
		err = ERR_FORMAT_SI;
		goto err_format_crear_fichero_cripto;
	}

	memset((void *) &particion, 0, sizeof particion);
	particion.num_sectors = size / 512; 

	if ( err = format_crear_zona_cripto_ex ( hFile, particion, password, NULL, 1, -1 ) != ERR_LOWLEVEL_NO ) 
		goto err_format_crear_fichero_cripto;

	if ( lowlevel_cerrar_dispositivo(hFile) != ERR_FORMAT_NO ) {
		hFile = 0;
		err = ERR_FORMAT_SI;
		goto err_format_crear_fichero_cripto;
	}

	return ERR_FORMAT_NO;

err_format_crear_fichero_cripto:

	if ( fp ) 
		fclose(fp);

	if ( hFile )
		lowlevel_cerrar_dispositivo(hFile);
	
	return err;
}



int format_crear_zona_cripto_ex ( IN HANDLE_DISPOSITIVO handle, IN PARTICION particion, IN PASSWORD password, IN BYTE *id_dispositivo, IN DWORD rsv_blocks, IN int dispositivo )
{
        char *iden = "UJI - Clauer PKI storage system";
        BYTE *buffer;
        BYTE *buffer_cifrado;
        BYTE *ptr;
        DWORD bytesEscritos, tamBufferCifrado;
        BYTE bufftemp[TAM_BLOQUE];
        int result;
        unsigned int indice;
        unsigned long sector_relativo;
        unsigned int total_bloques;
		int winver, bytesSector;

		HCRYPTPROV hTmpProv;

        /* Obtenemos la versi�n del Windows */
        winver = lowlevel_get_winversion();

		switch (winver) {

                case WINXP:

					if ( dispositivo > -1){
						bytesSector= lowlevel_obtenerBytesSector(dispositivo);
						if ( bytesSector < 0 ){
							return ERR_FORMAT_SI;
						}
					}
					else{
						bytesSector= 512;
					}

					/* Adquirimos un contexto para la generaci�n aleatoria */
					if (!CryptAcquireContext(&hTmpProv, NULL, NULL, PROV_RSA_FULL, CRYPT_VERIFYCONTEXT))
						return ERR_FORMAT_SI;

					/* Buffer para la escritura de datos */
					buffer = (BYTE *) malloc (TAM_BLOQUE);
					if ( ! buffer ) {
						CryptReleaseContext(hTmpProv, 0);
						return ERR_FORMAT_SI;
					}

					/* Lo inicializamos con valores aleatorios */
					if (!CryptGenRandom( hTmpProv, TAM_BLOQUE, buffer )) {
						free(buffer);
						CryptReleaseContext(hTmpProv,0);
						return ERR_FORMAT_SI;
					}

					/* El puntero apuntar� a la siguiente posici�n en la que escribir */
					ptr = buffer;

					/* Llevamos un control del n�mero de bytes escritos en el buffer */
					bytesEscritos = 0;

					/* Zona de informaci�n */
					/* ------------------- */
					
					CryptReleaseContext(hTmpProv,0);

					/* Inicializamos funci�n de generaci�n de llaves, etc. */

					result = format_ini( handle, password, buffer+40 );
					if ( result != ERR_FORMAT_NO ) {
						free(buffer);
						return ERR_FORMAT_SI;
					}

					/* Campo 1: IDEN String (40 bytes = 32 bytes + 8 bytes padding), se almacena cifrado */

					buffer_cifrado = (BYTE *) malloc ( NBYTES_IDEN );
					if ( ! buffer_cifrado ) {
						free(buffer);
						return ERR_FORMAT_SI;
					}
					memcpy(buffer_cifrado, iden, 32);
					tamBufferCifrado = 32;

					/* Le pasamos 32 bytes y nos devuelve 40 bytes cifrados */

					if ( ! CryptEncrypt( ghKey, 0, TRUE, 0, buffer_cifrado, &tamBufferCifrado, NBYTES_IDEN ) ) {
						free(buffer_cifrado);
						free(buffer);
						return ERR_FORMAT_SI;
					}

					memcpy(ptr, buffer_cifrado, NBYTES_IDEN);
					ptr += NBYTES_IDEN;
					bytesEscritos += NBYTES_IDEN;
					free(buffer_cifrado);

					/* Campo 2: Identificador del dispositivo */
					
					if ( id_dispositivo != NULL ) 
						memcpy( ptr, id_dispositivo, 20 );

					/* Si id_dispositivo es NULL, no escribimos nada, ya ten�amos el bloque con 
					   informaci�n aleatoria de antes */

					ptr += NBYTES_ID;
					bytesEscritos += NBYTES_ID;

					/* Campo 3: Tama�o de la zona reservada (NRSV unidades de 10k) */

					*((unsigned int *)ptr) = rsv_blocks;
					ptr += NBYTES_NRSV;
					bytesEscritos += NBYTES_NRSV;

					/* Campo 4: Current block */

					*((unsigned int *)ptr) = CURRENT_BLOCK;
					ptr += NBYTES_CURRENT;
					bytesEscritos += NBYTES_CURRENT;

					/* Campo 5: Total bloques (descontamos la zona de info y la reservada) */

					total_bloques = ((particion.num_sectors)*bytesSector/TAM_BLOQUE) - 1 - rsv_blocks ;
					*((unsigned int *)ptr)=total_bloques;
					ptr+=NBYTES_TOTAL;
					bytesEscritos+=NBYTES_TOTAL;

					/* Campo 6: Versi�n del formato */

					*((unsigned int *)ptr)=VERSION;
					ptr+=NBYTES_VERSION;
					bytesEscritos+=NBYTES_VERSION;

					/* Escritura del bloque de informaci�n */

					/* Escribimos la zona de informaci�n en el primer bloque */

					sector_relativo = particion.rel_sectors;
					result = lowlevel_escribir_buffer( handle, buffer, sector_relativo, TAM_BLOQUE/bytesSector, dispositivo );
					if (result!=ERR_LOWLEVEL_NO)  {
						free(buffer);
						return ERR_FORMAT_SI;
					}

					sector_relativo+=TAM_BLOQUE/bytesSector;

					free(buffer);


					/* Zona reservada y zona de objetos*/
					/* ------------------------------- */

					buffer = (BYTE *) malloc ( TAM_BUFFER );
					if ( ! buffer ) {
						free(buffer);
						return ERR_FORMAT_SI;
					}

					bytesEscritos=0;
					ptr = buffer;
					
					/* Establecemos la semilla */
					srand( (unsigned) time (NULL) );

					for (indice=0; indice < (total_bloques + rsv_blocks) ; indice++) {

							if (bytesEscritos == TAM_BUFFER) {
									result = lowlevel_escribir_buffer( handle, buffer, sector_relativo, TAM_BUFFER/bytesSector, dispositivo );
									sector_relativo+=TAM_BUFFER/bytesSector;
									if ( result != ERR_LOWLEVEL_NO ) {
										free(buffer);
										return ERR_FORMAT_SI;
									}
									bytesEscritos=0;
									ptr = buffer;
							}

							/*CryptGenRandom( ghProv, TAM_BLOQUE, bufftemp );*/

							bufftemp[0] = rand() % 85;	/* Bloques blancos por defecto */

							memcpy(ptr,bufftemp,TAM_BLOQUE);
							ptr+=TAM_BLOQUE;
							bytesEscritos+=TAM_BLOQUE;
					}

					/* Si no ha llenado el buffer */
					if ((bytesEscritos >0 ) && (bytesEscritos <= TAM_BUFFER)) {
							result = lowlevel_escribir_buffer( handle, buffer, sector_relativo, bytesEscritos/bytesSector, dispositivo);
							if ( result != ERR_LOWLEVEL_NO ) {
								free(buffer);
								return ERR_FORMAT_SI;
							}
					}

					free(buffer);

                    break;

                default:        
					
					return ERR_LOWLEVEL_SIST_OPERATIVO_INCORRECTO;

        }

        return ERR_FORMAT_NO;
}


//---------------------------------------------------------------------------
