/* Formateador para el USB stick */
#ifndef USB_FORMAT_H
#define USB_FORMAT_H

#ifdef __cplusplus
extern "C" {
#endif

#include "estructuras.h"
#include <stdio.h>
#include <UsbLowLevel/usb_lowlevel.h>
#include <direct.h>
#include <time.h>
#include <process.h>
#include <stdlib.h>
//#include <Winioctl.h>

#include <windows.h>
#include <tchar.h>
#include <basetyps.h>
#include <stdlib.h>
#include <wtypes.h>
#include <string.h>
#include <winioctl.h>



#define PROG_INICIALIZANDO			0
#define PROG_CREANDO_BACKUP			1
#define PROG_CAMBIANDO_MBR			2
#define	PROG_FORMATEANDO_FAT	    3
#define	PROG_RESTAURANDO_BACKUP		4
#define PROG_ELIMINANDO_BACKUP      5
#define PROG_PIDIENDO_PASSWORD		6
#define PROG_FORMATEANDO_CRYPTO		7
#define PROG_FINALIZANDO			8
#define PROG_CANCELADO				9

/* TAM_BUFFER debe ser multiplo de 512 */
#define TAM_BUFFER		5242880

/* Tama�o del bloque en bytes */
#define TAM_BLOQUE      10240

/* Par�metros de zona de informaci�n */

        /* Valores por defecto */
#define NRSV            9       /* N�mero de unidades reservadas */
#define CURRENT_BLOCK   0       /* N�mero de bloque actual */
#define VERSION			1		/* N�mero de versi�n de formato */

        /* Tama�os de los campos */
#define NBYTES_IDEN     40
#define NBYTES_ID       20
#define NBYTES_SALT		20
#define NBYTES_NRSV     4
#define NBYTES_CURRENT  4
#define NBYTES_TOTAL    4
#define NBYTES_VERSION	4


/* PENDIENTE DE ELIMINACI�N */
/*#define TAM_LLAVE_PRIV	16
#define NBYTES_TAM_PRIV	1
#define NBYTES_PRIV		TAM_LLAVE_PRIV*/

/* Funci�n callback */
typedef void cb_formato_progreso( int tipoTarea, double porcetajeTarea, double porcentajeGlobal );


/* Funciones de formateo */

int mkdosfs(int,char**);
int format_crear_clauer(IN int dispositivo, IN char * unidad, IN char* password, IN int porcentaje);
int format_cambiar_particiones_mbr( IN HANDLE_DISPOSITIVO handle, IN int porcentaje, IN PARTICION particiones[], OUT PARTICION nuevas_particiones[], IN int dispositivo );
int format_formatear_unidad_logica( IN char *unidad );
int format_formatear_unidad_logica_win( IN char *unidad );

int format_crear_zona_cripto    ( IN HANDLE_DISPOSITIVO handle, IN PARTICION particion, IN PASSWORD password, IN BYTE *id_dispositivo, IN int dispositivo );
int format_crear_zona_cripto_ex ( IN HANDLE_DISPOSITIVO handle, IN PARTICION particion, IN PASSWORD password, IN BYTE *id_dispositivo, IN DWORD rsv_blocks, IN int dispositivo );
int format_crear_fichero_cripto ( IN char fileName[MAX_PATH], IN PASSWORD password, int sizeBlocks );

int format_crear_mbr( IN HANDLE_DISPOSITIVO handle, IN int heads, IN int cylinders, IN int sectors, IN int dispositivo );
int format_releer_mbr( IN HANDLE_DISPOSITIVO handle );

int format_generar_id_dispositivo( OUT BYTE* id_dispositivo );
int format_guardar_id_dispositivo( IN HANDLE_DISPOSITIVO handle, IN BYTE* id_dispositivo, IN int dispositivo );
int format_leer_id_dispositivo( IN HANDLE_DISPOSITIVO handle, OUT BYTE* id_dispositivo );
int format_restaurar_id_dispositivo( IN HANDLE_DISPOSITIVO, OUT BYTE* id_dispositivo );

/* Creaci�n de copias de seguridad */

int format_crear_backup_datos( IN HANDLE_DISPOSITIVO handle, IN PASSWORD password, IN char* unidad, OUT char* ruta_directorio_destino, IN int dispositivo );
int format_crear_backup_cripto( IN HANDLE_DISPOSITIVO handle, IN PASSWORD password, IN PARTICION particion, OUT char* ruta_backup, IN int dispositivo);
int format_crear_backup_mbr( IN HANDLE_DISPOSITIVO handle, IN PASSWORD password, OUT char* ruta_backup, IN int dispositivo );

/* Restauraci�n de copias de seguridad */

int format_restaurar_backup_datos( IN HANDLE_DISPOSITIVO handle, IN PASSWORD password, IN char* unidad, IN char* ruta_directorio_origen );
int format_restaurar_backup_cripto( IN HANDLE_DISPOSITIVO handle, IN char* ruta_backup, IN PARTICION particion, IN PASSWORD password_antigua, IN PASSWORD password_nueva, IN int dispositivo );
int format_restaurar_backup_mbr( IN HANDLE_DISPOSITIVO handle, IN PASSWORD password, IN char* ruta_backup, IN int dispositivo );

/* Eliminaci�n de copias de seguridad */

int format_eliminar_backup_datos( IN HANDLE_DISPOSITIVO handle, IN PASSWORD password, IN char* ruta_directorio );
int format_eliminar_backup_cripto( IN HANDLE_DISPOSITIVO handle, IN PASSWORD password, IN char* ruta_backup );
int format_eliminar_backup_mbr( IN HANDLE_DISPOSITIVO handle, IN PASSWORD password, IN char* ruta_backup );

/* Obtenci�n de informaci�n */

int format_obtener_geometria( IN HANDLE_DISPOSITIVO handle, OUT long int *heads, OUT long int *sectors, OUT long int *cylinders, IN int dispositivo );
int format_obtener_geometria_optima( IN int dispositivo, OUT long int *heads, OUT long int *sectors, OUT long int *cylinders );

int format_obtener_tamanyo( IN int unidad_logica, OUT long long *tamanyo );

int format_enumerar_dispositivos(OUT char **unidades, OUT int *dispositivos, OUT int *numDispositivos);
int format_obtener_ruta_temporal( IN HANDLE_DISPOSITIVO handle, IN char* cadena, OUT char* ruta, IN int dispositivo );

/* Miscel�nea */

int format_ini ( IN HANDLE_DISPOSITIVO handle, IN PASSWORD password, IN BYTE salt [20] );
int format_formatear( IN HANDLE_DISPOSITIVO handle, int modo );
int format_error( IN int codigo_error, OUT char* descripcion );
int format_verificar_integridad( IN HANDLE_DISPOSITIVO handle, IN PASSWORD password, IN int dispositivo );

/* C�digos de error de librer�a de formateo */

#define ERR_FORMAT_NO                           0
#define ERR_FORMAT_SI                           1
#define ERR_FORMAT_CREANDO_BACKUP               2
#define ERR_FORMAT_RESTAURANDO_BACKUP           3
#define ERR_FORMAT_ELIMINANDO_BACKUP            4
#define ERR_FORMAT_LECTURA_MBR_INCORRECTA       5
#define ERR_FORMAT_ESCRITURA_MBR_INCORRECTA     6
#define ERR_FORMAT_IMPOSIBLE_ABRIR_DISPOSITIVO  7
#define ERR_FORMAT_IMPOSIBLE_LEER_PARTICIONES   8
#define ERR_FORMAT_CAMBIANDO_PARTICIONES        9
#define ERR_FORMAT_FORMATEANDO_DATOS            10
#define ERR_FORMAT_CREANDO_ZONA_CRYPTO          11
#define ERR_FORMAT_CERRANDO_DISPOSITIVO         12
#define ERR_FORMAT_DISPOSITIVO_FORMATEADO       13
#define ERR_FORMAT_LEYENDO_GEOMETRIA            14
#define ERR_FORMAT_SIST_OPERATIVO_INCORRECTO    15
#define ERR_FORMAT_BLOQUEANDO_UNIDAD_LOGICA     16
#define ERR_FORMAT_DESBLOQUEANDO_UNIDAD_LOGICA  17
#define ERR_FORMAT_CREANDO_TEMP					18
#define ERR_FORMAT_FUNCION_NO_DISPONIBLE		19
#define ERR_FORMAT_PKCS5						20
#define ERR_FORMAT_ZONA_INFO_INCORRECTA			21
#define ERR_FORMAT_ZONA_RESERVADA_INCORRECTA	22
#define ERR_FORMAT_ZONA_OBJETOS_INCORRECTA		23

#ifdef __cplusplus
}
#endif
#endif
