#include <stdio.h>
#include <stdlib.h>
#include <libFormat/usb_format.h>
#include <UsbLowLevel/usb_lowlevel.h>


void main() {

	int result;
	int numDispositivos;
	char ** unidades;
	int *dispositivos;
	int i;
	// char * password;
	//HANDLE_DISPOSITIVO handle;
	// PARTICION particiones[4];
	// PARTICION nuevas_particiones[4];
	// long int C,H,S;
	//int soy_admin;

	/*printf("Formateador cutre en consola MS-DOS\n");
	printf("-----------------------------------\n\n");

	printf("Por favor, introduce el dispositivo USB y pulsa una tecla para continuar...\n");
	fgetc(stdin);

	result = lowlevel_enumerar_dispositivos( NULL, NULL, &numDispositivos );

	if (( result != ERR_LOWLEVEL_NO ) || ( numDispositivos != 1 )) {
		printf("Error leyendo dispositivo, <%d>\n",numDispositivos);
		return;
	}


	unidades = (char **) malloc (numDispositivos*sizeof(char)*5);

	for ( i=0; i<numDispositivos; i++ ) {
		unidades[i] = (char *) malloc(5*sizeof(char));
	}

	dispositivos = (int *) malloc (numDispositivos*sizeof(int));


	result = lowlevel_enumerar_dispositivos( unidades, dispositivos, &numDispositivos );
	if ( result != ERR_LOWLEVEL_NO ) {
		printf("Error leyendo dispositivo\n");
		return;
	}


	printf("Voy a formatear la unidad %s, con identificador=%d\n\n", unidades[0], dispositivos[0]);


	result = lowlevel_soy_admin( dispositivos[0], &soy_admin );
	if ( result != ERR_LOWLEVEL_NO ) {
		printf("Error comprobando permisos\n");
		return;
	}

	if ( soy_admin ) printf("Soy administrador!\n");
	else {
		printf("Error: Necesitas ser administrador para formatear!\n");
		return;
	}

	printf("Nuevo\n\n");
	printf("Abriendo dispositivo...");
	result = lowlevel_abrir_dispositivo( dispositivos[0], &handle );
	if ( result != ERR_LOWLEVEL_NO ) {
		printf("Error abriendo dispositivo\n");
		return;
	}
	printf("OK!\n");
	
*/


	result = lowlevel_enumerar_dispositivos( NULL, NULL, &numDispositivos );
	
	if (( result != ERR_LOWLEVEL_NO ) || ( numDispositivos != 1 )) {
		printf("Error leyendo dispositivo, <%d>\n",numDispositivos);
		return;
	}


	unidades = (char **) malloc (numDispositivos*sizeof(char)*5);

	for ( i=0; i<numDispositivos; i++ ) {
		unidades[i] = (char *) malloc(5*sizeof(char));
	}

	dispositivos = (int *) malloc (numDispositivos*sizeof(int));


	result = lowlevel_enumerar_dispositivos( unidades, dispositivos, &numDispositivos );
	if ( result != ERR_LOWLEVEL_NO ) {
		printf("Error leyendo dispositivo\n");
		return;
	}


	result= format_crear_clauer( dispositivos[0], unidades[0], "123clauer", 0 );
	//result= format_crear_clauer( 1,"E:","jaja", 95);
	
	switch (result){
	case ERR_FORMAT_NO:
		printf("Ok\n");
		break;
	case ERR_FORMAT_SI:
		printf("Error formateando\n");
		break;
	case	ERR_FORMAT_SIST_OPERATIVO_INCORRECTO:
		printf ("Operativo incorrecto\n");
		break;
	case ERR_FORMAT_ESCRITURA_MBR_INCORRECTA:
		printf("Escritura MBR incorrecta\n");
		break;
	case ERR_FORMAT_LEYENDO_GEOMETRIA:
		printf("Error leyendo geometria optima\n");
		break;
	default:
		printf("Ha pasado algo raro\n");
	}
	printf("Saliendorrrlll \n\n");
	exit(0);
 /*
	long int tam;
	result = format_obtener_tamanyo( dispositivos[0], &tam );
	printf("Tama�o obtenido es: %d\n" ,tam);

	result = format_obtener_geometria_optima( dispositivos[0], &H, &S, &C );
	if ( result != ERR_FORMAT_NO ) {
		printf("Error obteniendo geometria optima\n");
		return;
	}
	
	printf( "Obtenidos: H:%d S:%d C:%d \n",H,S,C );
	
	result = format_crear_mbr( handle, H, C, S );
	if ( result != ERR_FORMAT_NO ) {
		printf("Error obteniendo geometria optima\n");
		return;
	}
	
	printf("Leyendo particiones...");
	result = lowlevel_leer_particiones( handle, particiones );
	if ( result != ERR_LOWLEVEL_NO ) {
		printf("Error leyendo particiones\n");
		return;
	}
	printf("OK!\n");

  
	printf("Cambiando las particiones...");
	result = format_cambiar_particiones_mbr( handle, 95, particiones, nuevas_particiones );
	if ( result != ERR_FORMAT_NO ) {
		printf("Error cambiando las particiones\n");
		return;
	}
	printf("OK!\n");
	
	printf("Releyendo MBR...");
	result = format_releer_mbr( handle );
	if ( ( result != ERR_FORMAT_NO ) && ( result != ERR_FORMAT_FUNCION_NO_DISPONIBLE ) ) {
		printf("Error releyendo el MBR\n");
		return;
	}

	if ( result == ERR_FORMAT_FUNCION_NO_DISPONIBLE ) {

		result = lowlevel_cerrar_dispositivo( handle );
		if ( result != ERR_LOWLEVEL_NO ) {
			printf("Error cerrando dispositivo\n");
			return;
		}
		printf("\n\nPor favor, retire y vuelva a insertar el dispositivo\nPulse una tecla para continuar...\n");
		fgetc(stdin);

		result = lowlevel_abrir_dispositivo( dispositivos[0], &handle );
		if ( result != ERR_LOWLEVEL_NO ) {
			printf("Error abriendo dispositivo\n");
			return;
		}
	}
	else printf("OK!\n");
	
	printf("Formateando particion de datos...\n");

	result = format_formatear_unidad_logica_win( unidades[0] );
	if ( result != ERR_FORMAT_NO ) {
		printf("Error formateando particion de datos : <%d>\n",GetLastError());
		return;
	}
	printf("OK!\n");

	password = (char *) malloc (10*sizeof(char));
	strcpy(password, "jajajaja");

	printf("Formateando particion criptografica...");

	result = format_crear_zona_cripto( handle, nuevas_particiones[3], password, NULL );
	if ( result != ERR_FORMAT_NO ) {
		printf("Error creando zona criptografica\n");
		return;
	}
	printf("OK!\n");

	free(password);

	printf("Cerrando dispositivo...");
	result = lowlevel_cerrar_dispositivo( handle );
	if ( result != ERR_LOWLEVEL_NO ) {
		printf("Error cerrando dispositivo\n");
		return;
	}
*/
	printf("OK!\n");

	printf("\n");

	free(unidades);
	free(dispositivos);
}