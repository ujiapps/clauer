#ifndef ESTRUCTURAS_H
#define ESTRUCTURAS_H

#include <windows.h>

/* Tipos de datos */

typedef int CODIGO_ERROR;
typedef BYTE TIPO_BLOQUE;
typedef BYTE TIPO_CERT;
typedef DWORD HANDLE_OBJETO;
typedef char * PASSWORD;
typedef HANDLE HANDLE_DISPOSITIVO;

/*
 * Para distinguir los par�metros que son de salida, de entrada o de entrada/salida
 */

#ifndef IN
#define IN
#endif

#ifndef OUT
#define OUT
#endif

#define TAM_BLOQUE	10240

/*
 * Los tipos de bloques
 */

#define BLOQUE_BLANCO			0x00
#define BLOQUE_LLAVE_PRIVADA	        0x01
#define BLOQUE_CERT_PROPIO		0x02
#define BLOQUE_CERT_WEB			0x03
#define BLOQUE_CERT_RAIZ		0x04
#define	BLOQUE_KNOWN_HOSTS		0x05
#define BLOQUE_KEY_CONTAINERS	        0x06

/*
 * Los tipos de datos almacenados
 */

#define CERT_PROPIO		BLOQUE_CERT_PROPIO
#define CERT_WEB		BLOQUE_CERT_WEB
#define CERT_RAIZ		BLOQUE_CERT_RAIZ
#define LLAVE_PRIVADA	        BLOQUE_LLAVE_PRIVADA
#define KNOWN_HOSTS		BLOQUE_KNOWN_HOSTS

#define WINNT351    3           /* Windows NT 3.51 */
#define WIN95       4           /* Windows 95/98/Me/NT 4.0 */
#define WINXP       5           /* Windows XP/2000/Server 2003 family */

/* Id de la partici�n NISU: 69h */
#define ID_PART_NISU                      105

/* C�digos de error */
#define ERR_NO                              0 /* No ha ocurrido error */
#define ERR_SI                              1 /* Ha ocurrido un error indefinido */
#define ERR_SIST_OPERATIVO_INCORRECTO      50
#define ERR_DISPOSITIVO_INCORRECTO        100
#define ERR_PARTICION_INEXISTENTE         101
#define ERR_PARTICION_INCORRECTA          102
#define ERR_PASSWORD_INCORRECTA           103
#define ERR_TIPO_DATOS_INEXISTENTE        104
#define ERR_TIPO_DATOS_INCORRECTO         105
#define ERR_NUNIDAD_INCORRECTA            106
#define ERR_LECTURA_MBR_INCORRECTA        107
#define ERR_DISPOSITIVO_NO_UNICO          108
#define ERR_DISPOSITIVO_NO_INICIALIZADO   109
#define ERR_FIN_PARTICION				  110
#define ERR_NO_CERT_DISPONIBLE			  111
#define ERR_FORMATO_INCORRECTO			  112
#define ERR_SIN_BLOQUES_BLANCOS			  113
#define ERR_NO_LLAVE_PRIVADA			  114
#define ERR_SIN_ESPACIO					  115
#define ERR_NO_ENCONTRADO				  116
#define ERR_CONTAINER_EXISTE			  117
#define ERR_FIRMA_INCORRECTA			  118
#define ERR_MBR_NO_VALIDO                 119
#define ERR_MULTIPLES_PARTICIONES         120
#define ERR_CARGANDO_ZIP_LIBRARY          121
#define ERR_ENTRYPOINT_ZIP_LIBRARY        122
#define ERR_ESCRITURA_MBR_INCORRECTA      123
#define ERR_BLOQUEANDO_DISPOSITIVO		  124
#define ERR_DESBLOQUEANDO_DISPOSITIVO	  125
#define ERR_LECTURA_INCORRECTA			  126
#define ERR_ESCRITURA_INCORRECTA		  127
#define ERR_NO_USBCERTS                   128


/* Estructuras de datos */

typedef struct INFO_CERT{
        BYTE *certificado;                /* Certificado en formato PEM */
		unsigned int tamCertificado;
        TIPO_CERT tipoCert;               /* Tipo de certificado */
        HANDLE_OBJETO handle;             /* Handle del certificado. Permite localizar el objeto en la partici�n */
} INFO_CERT;

typedef struct INFO_KEY{
        BYTE *llave;                      /* Llave privada en formato PEM */
		unsigned int tamLLave;
        int tamHandles;                   /* Vector de handles para la llave privada */
        HANDLE_OBJETO *handles;           /* N�mero de elementos del vector */
} INFO_KEY;

#endif
