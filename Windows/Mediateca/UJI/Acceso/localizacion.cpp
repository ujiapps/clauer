#include "localizacion.h"
#include "stdafx.h"

#include <windows.h>
#include <malloc.h>

#define PRG_REG_KEY			"SOFTWARE\\Universitat Jaume I\\Projecte Clauer"
#define PRG_REG_AULA		"CLABLOCK_AULA"

#define PRG_REG_HOST		"CLABLOCK_HOST"
#define PRG_REG_HOSTSSL		"CLABLOCK_HOSTSSL"

#define PRG_REG_PATH		"CLABLOCK_PATH"
#define PRG_REG_PATHSSL		"CLABLOCK_PATHSSL"

char * Localizar()
{

	DWORD tamValue;
	char *regwar=NULL;
	DWORD type;

	HKEY hKey = NULL;

	if ( RegOpenKeyEx(HKEY_LOCAL_MACHINE, PRG_REG_KEY, 0, KEY_ALL_ACCESS, &hKey) != ERROR_SUCCESS ) {
		return NULL;
	}

	RegQueryValueEx(hKey, PRG_REG_AULA, NULL, NULL, NULL, &tamValue);

	regwar=(char *)malloc(tamValue);
	

	if ( RegQueryValueEx(hKey, PRG_REG_AULA, NULL, &type,(BYTE *)regwar, &tamValue) != ERROR_SUCCESS ) {
		return NULL;
	}


	if ( hKey ) 
		RegCloseKey(hKey);

	regwar[6]='\0';
	return regwar;
}

CHAR * Host()
{

	DWORD tamValue;
	CHAR *regwar=NULL;
	DWORD type;

	HKEY hKey = NULL;

	if ( RegOpenKeyEx(HKEY_LOCAL_MACHINE, PRG_REG_KEY, 0, KEY_ALL_ACCESS, &hKey) != ERROR_SUCCESS ) {
		return NULL;
	}

	RegQueryValueEx(hKey, PRG_REG_HOST, NULL, NULL, NULL, &tamValue);

	regwar=(CHAR *)malloc(tamValue);
	

	if ( RegQueryValueEx(hKey, PRG_REG_HOST, NULL, &type,(BYTE *)regwar, &tamValue) != ERROR_SUCCESS ) {
		return NULL;
	}


	if ( hKey ) 
		RegCloseKey(hKey);

//	regwar[6]='\0';
	return regwar; 
}

CHAR * HostSSL()
{

	DWORD tamValue;
	CHAR *regwar=NULL;
	DWORD type;

	HKEY hKey = NULL;

	if ( RegOpenKeyEx(HKEY_LOCAL_MACHINE, PRG_REG_KEY, 0, KEY_ALL_ACCESS, &hKey) != ERROR_SUCCESS ) {
		return NULL;
	}

	RegQueryValueEx(hKey, PRG_REG_HOSTSSL, NULL, NULL, NULL, &tamValue);

	regwar=(CHAR *)malloc(tamValue);
	

	if ( RegQueryValueEx(hKey, PRG_REG_HOSTSSL, NULL, &type,(BYTE *)regwar, &tamValue) != ERROR_SUCCESS ) {
		return NULL;
	}


	if ( hKey ) 
		RegCloseKey(hKey);

//	regwar[6]='\0';
	return regwar;
}

char * Path()
{

	DWORD tamValue;
	char *regwar=NULL;
	DWORD type;

	HKEY hKey = NULL;

	if ( RegOpenKeyEx(HKEY_LOCAL_MACHINE, PRG_REG_KEY, 0, KEY_ALL_ACCESS, &hKey) != ERROR_SUCCESS ) {
		return NULL;
	}

	RegQueryValueEx(hKey, PRG_REG_PATH, NULL, NULL, NULL, &tamValue);

	regwar=(char *)malloc(tamValue);
	

	if ( RegQueryValueEx(hKey, PRG_REG_PATH, NULL, &type,(BYTE *)regwar, &tamValue) != ERROR_SUCCESS ) {
		return NULL;
	}


	if ( hKey ) 
		RegCloseKey(hKey);

//	regwar[6]='\0';
	return regwar;
}

char * PathSSL()
{

	DWORD tamValue;
	char *regwar=NULL;
	DWORD type;

	HKEY hKey = NULL;

	if ( RegOpenKeyEx(HKEY_LOCAL_MACHINE, PRG_REG_KEY, 0, KEY_ALL_ACCESS, &hKey) != ERROR_SUCCESS ) {
		return NULL;
	}

	RegQueryValueEx(hKey, PRG_REG_PATHSSL, NULL, NULL, NULL, &tamValue);

	regwar=(char *)malloc(tamValue);
	

	if ( RegQueryValueEx(hKey, PRG_REG_PATHSSL, NULL, &type,(BYTE *)regwar, &tamValue) != ERROR_SUCCESS ) {
		return NULL;
	}


	if ( hKey ) 
		RegCloseKey(hKey);

//	regwar[6]='\0';
	return regwar;
}