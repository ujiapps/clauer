#pragma once


// Cuadro de di�logo de Dialog

class Dialog : public CDialog
{
	DECLARE_DYNAMIC(Dialog)

public:
	Dialog(CWnd* pParent = NULL);   // Constructor est�ndar
	virtual ~Dialog();

// Datos del cuadro de di�logo
	enum { IDD = IDD_ACCESO_DIALOG };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // Compatibilidad con DDX o DDV

	DECLARE_MESSAGE_MAP()
};
