// Dialog.cpp: archivo de implementación
//

#include "stdafx.h"
#include "Acceso.h"
#include "Dialog.h"


// Cuadro de diálogo de Dialog

IMPLEMENT_DYNAMIC(Dialog, CDialog)
Dialog::Dialog(CWnd* pParent /*=NULL*/)
	: CDialog(Dialog::IDD, pParent)
{
}

Dialog::~Dialog()
{
}

void Dialog::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(Dialog, CDialog)
END_MESSAGE_MAP()


// Controladores de mensajes de Dialog
