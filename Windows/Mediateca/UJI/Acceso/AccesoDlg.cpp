// AccesoDlg.cpp: archivo de implementaci�n
//

#include "stdafx.h"
#include "Acceso.h"
#include "AccesoDlg.h"
#include "Conectar.h"

#include "localizacion.h"
#include "tiempo.h"

#include "Bloqueo.h"
#include <LIBRT/LIBRT.h>

#include <Dbt.h>

#include <stdio.h>

#include "winuser.h"

#include ".\accesodlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

// Forward reference for the procedure that will be hooked to the keyborad
//LRESULT CALLBACK LowLevelKeyboardProc(INT nCode, WPARAM wParam, LPARAM lParam);

// Cuadro de di�logo de CAccesoDlg

CAccesoDlg::CAccesoDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CAccesoDlg::IDD, pParent)
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CAccesoDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_BITMAP, m_BmpImage);
}

BEGIN_MESSAGE_MAP(CAccesoDlg, CDialog)
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_WM_DEVICECHANGE()
	ON_WM_SHOWWINDOW()
	ON_WM_TIMER()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()


// Controladores de mensaje de CAccesoDlg

BOOL CAccesoDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	int i;

	fich_temp = getenv( "UserProfile" );

	fich_temp+="\\clablock_lock.tmp";

	hFile=CreateFile(TEXT(fich_temp.GetBuffer(0)), GENERIC_WRITE,0,NULL, CREATE_NEW,FILE_ATTRIBUTE_NORMAL, NULL);
	if (GetLastError()==80)
	{
		if (DeleteFile(fich_temp.GetBuffer(0))==0)
		{
			MessageBox("Clablock ya se esta ejecutando", NULL, MB_ICONERROR|MB_OK);
			exit(1);
		}
		else
		{
			hFile = CreateFile(TEXT(fich_temp.GetBuffer(0)),GENERIC_WRITE,FILE_SHARE_WRITE,						
                   NULL,CREATE_ALWAYS,			
                   FILE_ATTRIBUTE_NORMAL,	
                   NULL);
			if (hFile == INVALID_HANDLE_VALUE)
			{
				MessageBox("ERROR CREANDO ", NULL, MB_ICONERROR|MB_OK);
			}
		}

	}
//Host();wcstombs
	this->Aula=Localizar();
	this->host=Host();
	this->path=Path();
	this->hostSSL=HostSSL();
	this->pathSSL=PathSSL();

	this->Tiempo=tiempo_sesion();
	if(this->Tiempo==0)
		this->Tiempo=300000;

//	SetPriorityClass(GetCurrentProcess(),HIGH_PRIORITY_CLASS);
	// Establecer el icono para este cuadro de di�logo. El marco de trabajo realiza esta operaci�n
	//  autom�ticamente cuando la ventana principal de la aplicaci�n no es un cuadro de di�logo
	SetIcon(m_hIcon, TRUE);			// Establecer icono grande
	SetIcon(m_hIcon, FALSE);		// Establecer icono peque�o

	ShowWindow(SW_MAXIMIZE);

	// TODO: agregar aqu� inicializaci�n adicional
	// The first thing we must do is maximize the window
    RECT rect;
    // Get the size of the screen.
    GetDesktopWindow()->GetClientRect (&rect);
    MoveWindow(&rect);
    
	if(BloquearWindows())
	{
		PVOID stmpMsg;
        DWORD itmpErrorCode = GetLastError();
        if (FormatMessage(FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_ALLOCATE_BUFFER,
                            NULL, itmpErrorCode, 0, (char*)(&stmpMsg), 0, NULL))
        {
            MessageBox((char*)(stmpMsg),"Error hooking keyboard", MB_ICONSTOP | MB_OK);
            LocalFree(stmpMsg);
			_exit(-1);
		}
	}
	
	for(i=0;i<40;i++){
			this->IDClauer[i]='0';
		}
	
	this->IDAnterior=this->IDClauer;

	this->Inicio=1;

	this->bloq=1;

	// Set the image path
    m_Images.SetPath("insert.bmp");

	// Initialize image
    ChangeBitmap();

	return TRUE;  // Devuelve TRUE  a menos que establezca el foco en un control
}

// Si agrega un bot�n Minimizar al cuadro de di�logo, necesitar� el siguiente c�digo
//  para dibujar el icono. Para aplicaciones MFC que utilicen el modelo de documentos y vistas,
//  esta operaci�n la realiza autom�ticamente el marco de trabajo.

void CAccesoDlg::OnPaint() 
{
	if (IsIconic())
	{
		CPaintDC dc(this); // Contexto de dispositivo para dibujo

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Centrar icono en el rect�ngulo de cliente
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Dibujar el icono
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

// El sistema llama a esta funci�n para obtener el cursor que se muestra mientras el usuario arrastra
//  la ventana minimizada.
HCURSOR CAccesoDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}


void CAccesoDlg::OnStnClickedImage()
{
	// TODO: Agregue aqu� su c�digo de controlador de notificaci�n de control
}


BOOL CAccesoDlg::OnDeviceChange(UINT nEventType,DWORD_PTR dwData)
{
	int ndisp;
	BYTE * disp;
	USBCERTS_HANDLE h;

	int i,a,result;
	
	CString ID;

	a=0;
	if (nEventType==DBT_DEVICEARRIVAL){
		LIBRT_Ini();
		LIBRT_ListarDispositivos(&ndisp,&disp);
		if(ndisp==1){
			KillTimer(69);
			LIBRT_IniciarDispositivo(disp[0],NULL,&h);
			for(i=0;i<20;i++){

				a=sprintf((this->IDClauer)+2*i,"%02x",h.idDispositivo[i]);
			}

			//Comparamos si este clauer ha sido el anterior
			ID=this->IDClauer;
			if((ID.Compare(this->IDAnterior) != 0) && (this->Inicio!=1)) {
				//Salir sesi�n
				while(ndisp!=0){
					if(IDOK==AfxMessageBox("Retire el Clauer e insertelo cuando se le indique", MB_SYSTEMMODAL|MB_OK|MB_ICONINFORMATION));
					LIBRT_ListarDispositivos(&ndisp,&disp);
				}
				LIBRT_FinalizarDispositivo(&h);
				//Ahora cerramos sesion
				ExitWindowsEx(EWX_LOGOFF|EWX_FORCE,SHTDN_REASON_MAJOR_SOFTWARE);
			}
			LIBRT_FinalizarDispositivo(&h);
			strcpy(this->action,"ini");

			result=Conectar(this->IDClauer,this->action,this->Aula,this->host,this->path);
			
			//Puede iniciar sesi�n
			if (result == 1){
				strcpy(this->action,"act");
				//Creamos el Timer 5 minutos
				DesbloquearWindows();
				this->Inicio=0;
				this->bloq=0;
				SetTimer(80,300000,NULL);
			//ID no dado de alta
			}else if(result == 2){
				NOTECLA();
				result=ConectarSSL(this->IDClauer,this->action,this->Aula,this->hostSSL,this->pathSSL);
				TECLA();
				if (result == 1){
					strcpy(this->action,"act");
					//Creamos el Timer 5 minutos
					DesbloquearWindows();
					this->Inicio=0;
					this->bloq=0;
					SetTimer(80,300000,NULL);
				
				//Puede iniciar sesion pero solo le quedan 5 minutos
				}else if (result == 0){
					strcpy(this->action,"act");
					DesbloquearWindows();
					this->bloq=0;
					this->Inicio=0;
					SetTimer(999,300000,NULL);
					AfxMessageBox("�nicamente dispone de 5 minutos", MB_SYSTEMMODAL|MB_OK|MB_ICONINFORMATION);
				//Error Base de Datos
				}else if (result == 2){
					AfxMessageBox("ERROR ACCEDIENDO A LA BASE DE DATOS", MB_SYSTEMMODAL|MB_OK|MB_ICONINFORMATION);
				//Tiempo agotado semana
				}else if (result == 3){
					AfxMessageBox("Tiempo disponible agotado", MB_SYSTEMMODAL|MB_OK|MB_ICONINFORMATION);
				//Usuario no autorizado
				}else if (result == 4){
					AfxMessageBox("Usuario no autorizado", MB_SYSTEMMODAL|MB_OK|MB_ICONINFORMATION);
				}else if (result == 5){
					AfxMessageBox("Ocurrio un error validando su Clauer", MB_SYSTEMMODAL|MB_OK|MB_ICONINFORMATION);
				}
				
			//Puede iniciar sesion pero solo le quedan 5 minutos
			}else if (result == 0){
				strcpy(this->action,"act");
				DesbloquearWindows();
				this->Inicio=0;
				this->bloq=0;
				SetTimer(999,300000,NULL);
				AfxMessageBox("�nicamente dispone de 5 minutos", MB_SYSTEMMODAL|MB_OK|MB_ICONINFORMATION);

			//Tiempo agotado semana
			}else if (result == 3){
				AfxMessageBox("Tiempo disponible agotado", MB_SYSTEMMODAL|MB_OK|MB_ICONINFORMATION);
			//Usuario no autorizado
			}else if (result == 4){
				AfxMessageBox("Usuario no autorizado", MB_SYSTEMMODAL|MB_OK|MB_ICONINFORMATION);
			}else{
				AfxMessageBox("Ocurrio un error validando su Clauer", MB_SYSTEMMODAL|MB_OK|MB_ICONINFORMATION);
			}
		}
	}else if(nEventType==DBT_DEVICEREMOVECOMPLETE){
		LIBRT_Ini();
		LIBRT_ListarDispositivos(&ndisp,&disp);
		if(ndisp==0){
			if(this->bloq!=1) {
				BloquearWindows();
				SetTimer(69,this->Tiempo,NULL);
			}
			this->bloq=1;
			KillTimer(80);
			KillTimer(999);
		}else{
			CString IDRestante;
			int encontrado=0;
			int y;

			for(i=0;i<ndisp;i++){

				char cad[41];

				LIBRT_IniciarDispositivo(disp[i],NULL,&h);
				for(y=0;y<20;y++){
					a=sprintf((cad)+2*y,"%02x",h.idDispositivo[y]);
				}
				LIBRT_FinalizarDispositivo(&h);
				IDRestante=cad;
				if(IDRestante.Compare(this->IDClauer) == 0)
					encontrado=1;
			}
			if(!encontrado){
				if(this->bloq!=1)
				BloquearWindows();
				this->bloq=1;
				KillTimer(80);
				KillTimer(999);
				SetTimer(69,this->Tiempo,NULL);
			}
		}
	}
	LIBRT_Fin();
	return TRUE;
}

void CAccesoDlg::OnShowWindow(BOOL bShow,UINT nStatus)
{
	int i;

	//Ventana visible
	if(bShow == TRUE){
		//Matamos los Timer
		KillTimer(80);
		KillTimer(999);

		//AfxMessageBox("��������Estoy aqui!!!!", MB_YESNO|MB_ICONSTOP);

		//Copiamos el ID
		this->IDAnterior=this->IDClauer;

		//Limpiamos IDClauer
		for(i=0;i<40;i++){
			this->IDClauer[i]='0';
		}
		strcpy(this->action,"fin");

	//Ventana oculta
	}else{
	
	}

}

void CAccesoDlg::OnTimer(UINT nIDEvent)
{
	int result=0;
	
	//80 == Bloqueo / Desbloqueo
	if ( nIDEvent == 80 ) {
		result=Conectar(this->IDClauer,this->action,this->Aula,this->host,this->path);
		//Tiempo agotado
		if(result==0){
			SetTimer(999,300000,NULL);
			AfxMessageBox("�nicamente dispone de 5 minutos", MB_SYSTEMMODAL|MB_OK|MB_ICONINFORMATION);
			//Matamos el Timer
			KillTimer(80);
		}else if (result!=1){
		//	CAccesoDlg::OnTimer(nIDEvent);
			if(this->bloq!=1) {
				BloquearWindows();
				SetTimer(69,this->Tiempo,NULL);
			}
			this->bloq=1;

		}

	// 999 == Tiempo agotado
	}else if (nIDEvent == 999 ) {
		if(this->bloq!=1) {
				BloquearWindows();
			}
		this->bloq=1;
		KillTimer(999);
		ExitWindowsEx(EWX_LOGOFF|EWX_FORCE,SHTDN_REASON_MAJOR_SOFTWARE);
	}else if (nIDEvent == 69 ) {
		KillTimer(69);
		ExitWindowsEx(EWX_LOGOFF|EWX_FORCE,SHTDN_REASON_MAJOR_SOFTWARE);
	}

}

void CAccesoDlg::ChangeBitmap()
{
    m_Images.SetBitmap(&m_BmpImage);
}
