#include "Bloqueo.h"
#include "stdafx.h"
#include "AccesoDlg.h"

 HHOOK TheKeyHook;
 HHOOK TheKeyHook2;
 HWND TheMainWndHnd;
 HWND Tray;

// Forward reference for the procedure that will be hooked to the keyborad
LRESULT CALLBACK LowLevelKeyboardProc(INT nCode, WPARAM wParam, LPARAM lParam);
LRESULT CALLBACK LowLevelKeyboardProc2(INT nCode, WPARAM wParam, LPARAM lParam);


void NOTECLA()
{
	HINSTANCE ppI2 = AfxGetInstanceHandle();
	
	UnhookWindowsHookEx(TheKeyHook);
	TheKeyHook2= SetWindowsHookEx(WH_KEYBOARD_LL, LowLevelKeyboardProc2, ppI2, 0);
}
void TECLA(){

	UnhookWindowsHookEx(TheKeyHook2);
	HINSTANCE ppI = AfxGetInstanceHandle();
	TheKeyHook= SetWindowsHookEx(WH_KEYBOARD_LL, LowLevelKeyboardProc, ppI, 0);
}

int BloquearWindows()
{

	((CAccesoDlg *) AfxGetApp()->GetMainWnd())->ShowWindow(SW_SHOW);

	HINSTANCE ppI = AfxGetInstanceHandle();
    
    // We create the keyboard hook
	//((CAccesoDlg *) AfxGetApp()->GetMainWnd())->TheKeyHook = SetWindowsHookEx(WH_KEYBOARD_LL, LowLevelKeyboardProc, ppI,0);
	TheKeyHook= SetWindowsHookEx(WH_KEYBOARD_LL, LowLevelKeyboardProc, ppI, 0);

    if (TheKeyHook == NULL)
    {
         return (1);
    }
	// quita la barra de tareas
	Tray =  FindWindow("Shell_TrayWnd", 0);
	ShowWindow(Tray, SW_HIDE);

	return (0);
}


void DesbloquearWindows()
{

	((CAccesoDlg *) AfxGetApp()->GetMainWnd())->ShowWindow(SW_HIDE);
	// mostra la barra de tareas

	Tray =  FindWindow("Shell_TrayWnd", 0);
	ShowWindow(Tray, SW_SHOW);

	RegisterHotKey(TheMainWndHnd, 100, MOD_ALT, VK_TAB);	// Alt+Tab Bloqueado

	UnhookWindowsHookEx(TheKeyHook);
}

LRESULT CALLBACK LowLevelKeyboardProc(INT nCode, WPARAM wParam, LPARAM lParam)
{
    try
    {
        // By returning a non-zero value from the hook procedure, the
        // message does not get passed to the target window
        KBDLLHOOKSTRUCT *pkbhs = (KBDLLHOOKSTRUCT *) lParam;
        int error=GetLastError();   
        switch (nCode)
        {
            case HC_ACTION:
            {
                // Disable CTRL + ALT + END. 
                BOOL bControlKeyDown = GetAsyncKeyState (VK_CONTROL) >> ((sizeof(SHORT) * 8) - 1);
                if (pkbhs->vkCode == VK_END && LLKHF_ALTDOWN && bControlKeyDown)
                {
					UnhookWindowsHookEx(TheKeyHook);

				}else{
                    return 1;
				}
                break;
            } 

            default:
                break;
        }
        CallNextHookEx (TheKeyHook, nCode, wParam, lParam);
    }
    catch(...)
    {
    }
    return 0;
}

LRESULT CALLBACK LowLevelKeyboardProc2(INT nCode, WPARAM wParam, LPARAM
lParam)
{
    // By returning a non-zero value from the hook procedure, the
    // message does not get passed to the target window
    KBDLLHOOKSTRUCT *pkbhs = (KBDLLHOOKSTRUCT *) lParam;
    BOOL bControlKeyDown = 0;
	BOOL bEscKeyDown = 0;

    switch (nCode)
    {
        case HC_ACTION:
        {
            // Check to see if the CTRL key is pressed
            bControlKeyDown = GetAsyncKeyState (VK_CONTROL) >>
((sizeof(SHORT) * 8) - 1);

            // Disable CTRL+ESC
            if (pkbhs->vkCode == VK_ESCAPE && bControlKeyDown)
                return 1;

            // Disable ALT+TAB
            if (pkbhs->vkCode == VK_TAB && pkbhs->flags & LLKHF_ALTDOWN)
                return 1;

			// Disable WIN
			if (pkbhs->vkCode == VK_LWIN)
				return 1;

			if (pkbhs->vkCode == VK_RWIN)
				return 1;

			//Select Key
			if (pkbhs->vkCode == VK_SELECT )
				return 1;

			//Applications key 
			if (pkbhs->vkCode == VK_APPS )
				return 1;

			// Disable ALT+SPACE
            if (pkbhs->vkCode == VK_SPACE && pkbhs->flags & LLKHF_ALTDOWN)
                return 1;

			// Disable ALT+F4
			if (pkbhs->vkCode == VK_F4 && pkbhs->flags & LLKHF_ALTDOWN)	
				return 1;

            // Disable ALT+ESC
            if (pkbhs->vkCode == VK_ESCAPE && pkbhs->flags & LLKHF_ALTDOWN)
                return 1;

			// Disable ESC
            if (pkbhs->vkCode == VK_ESCAPE)
                return 1;

            break;
        }

        default:
            break;
    }
    return CallNextHookEx (TheKeyHook2, nCode, wParam, lParam);
} 