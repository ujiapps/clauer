#include "ntray.h"
#include "Resource.h"

class CMainFrame : public CFrameWnd
{
public:
	CMainFrame();
	virtual ~CMainFrame();

protected:
	//{{AFX_VIRTUAL(CMainFrame)
	//}}AFX_VIRTUAL

	DECLARE_DYNCREATE(CMainFrame)

	//{{AFX_MSG(CMainFrame)
	afx_msg int OnCreate(LPCREATESTRUCT lpCreateStruct);
	afx_msg void OnTimer(UINT nIDEvent);
	afx_msg void OnDestroy();
	//}}AFX_MSG
  afx_msg LRESULT OnTrayNotification(WPARAM wParam, LPARAM lParam);
	DECLARE_MESSAGE_MAP()

  CDC m_TrayIconDC;
  CBitmap m_BitmapTrayIcon;
  UINT m_nTimerID;
  int m_nTimerCount;
};

