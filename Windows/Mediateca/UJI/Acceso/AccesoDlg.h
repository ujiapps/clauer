#ifndef ACCESODLG_H
#define ACCESODLG_H
// AccesoDlg.h: archivo de encabezado
//

#pragma once


#include "Resource.h"

#include <windows.h>
#include <winhttp.h>
#include <time.h>
#include <stdio.h>
#include <stdlib.h>

#include "ImageLoader.h"
#include "afxwin.h"


// Cuadro de di�logo de CAccesoDlg
class CAccesoDlg : public CDialog
{
// Construcci�n
public:
	CAccesoDlg(CWnd* pParent = NULL);	// Constructor est�ndar

	 /**
     * Change the static image using the CImageLoader
     */
    void ChangeBitmap();

	//FILE *temporal;
	CString fich_temp;
	HANDLE hFile;

	int res;
	char IDClauer[41];
	char action[3];
	char loc[8];
	CString IDAnterior;
	int Inicio;

	CHAR *host;
	char *path;
	CHAR *hostSSL;
	char *pathSSL;
	char *Aula;
	int Tiempo;

	//0 no bloqueado 1 bloqueado
	int bloq;

// Datos del cuadro de di�logo
	enum { IDD = IDD_ACCESO_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// Compatibilidad con DDX/DDV

// Implementaci�n
protected:
	HICON m_hIcon;

	/// The CImageLoader
    CImageLoader m_Images;

	//Eventos * hilo_evento;
//	char IDClauer[41];
//	char action[3];
	

	// Funciones de asignaci�n de mensajes generadas
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	afx_msg BOOL OnDeviceChange(UINT nEventType,DWORD_PTR dwData);
	afx_msg void OnShowWindow(BOOL bShow,UINT nStatus);
	afx_msg void OnTimer(UINT nIDEvent);

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnStnClickedImage();
	afx_msg void OnStnClickedImage1();
	CStatic m_BmpImage;
};

#endif