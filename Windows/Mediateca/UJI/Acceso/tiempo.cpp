#include "tiempo.h"
#include "stdafx.h"

#include <windows.h>
#include <malloc.h>

#define PRG_REG_KEY			"SOFTWARE\\Universitat Jaume I\\Projecte Clauer"
#define PRG_REG_VALOR		"CLABLOCK_SESION"

int tiempo_sesion()
{

	DWORD tamValue;
	char *regwar=NULL;
	DWORD type;
	int result;

	HKEY hKey = NULL;

	if ( RegOpenKeyEx(HKEY_LOCAL_MACHINE, PRG_REG_KEY, 0, KEY_ALL_ACCESS, &hKey) != ERROR_SUCCESS ) {
		return 300000;
	}

	RegQueryValueEx(hKey, PRG_REG_VALOR, NULL, NULL, NULL, &tamValue);

	regwar=(char *)malloc(tamValue);
	

	if ( RegQueryValueEx(hKey, PRG_REG_VALOR, NULL, &type,(BYTE *)regwar, &tamValue) != ERROR_SUCCESS ) {
		return 300000;
	}

	result=atoi(regwar);

	result=result*60*1000;

	if ( hKey ) 
		RegCloseKey(hKey);

	return result;
}