#include "check.h"
#include "stdafx.h"

#include <process.h>

#include <Winbase.h> 
#include <Windows.h>
#include <Psapi.h>
#include <Tlhelp32.h>

#include <string.h>

int check(void){
	CHAR Buffer[1024];
	CHAR PATH[1024];
	CHAR Clablock[]="Clablock.exe";
	int encontrado=0;
	size_t len1;
	HANDLE handle=NULL; 

	HANDLE hProcessSnap;
	PROCESSENTRY32 pe32;

	handle = OpenProcess(PROCESS_ALL_ACCESS, FALSE, _getpid());
	GetProcessImageFileName(handle, PATH, 1024);
	CloseHandle(handle);

	len1=strlen(PATH);

	strncpy(&PATH[len1-14],Clablock,12);

	PATH[len1-2]='\0';

  hProcessSnap = CreateToolhelp32Snapshot( TH32CS_SNAPPROCESS, 0 );
  if( hProcessSnap == INVALID_HANDLE_VALUE )
  {
    return encontrado;
  }

  pe32.dwSize = sizeof( PROCESSENTRY32 );

  if( !Process32First( hProcessSnap, &pe32 ) )
  {
    return encontrado;
  }

  do
  {
    handle = OpenProcess( PROCESS_ALL_ACCESS, FALSE, pe32.th32ProcessID );
    if( handle != NULL ) {
      GetProcessImageFileName(handle, Buffer, 1024);
	  CloseHandle(handle);
	  if(strncmp(PATH,Buffer,1024)==0){
			printf("El PATH es: %s\n",PATH);
			printf("El Buffer es: %s\n",Buffer);
			CloseHandle(hProcessSnap);
			return 1;
	  }
    }

  } while( Process32Next( hProcessSnap, &pe32 ) );

  CloseHandle( hProcessSnap );

	return encontrado;
}