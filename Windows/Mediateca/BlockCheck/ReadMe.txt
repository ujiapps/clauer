========================================================================
    APLICACI�N DE CONSOLA: Informaci�n general del proyecto BlockCheck
========================================================================

El Asistente para aplicaciones ha creado esta aplicaci�n de BlockCheck.  
Archivo que contiene un resumen del contenido de los archivos que
componen la aplicaci�n de BlockCheck.


BlockCheck.vcproj
    Archivo de proyecto principal para proyectos de VC++ generados con el Asistente para aplicaciones. 
    Contiene informaci�n acerca de la versi�n de Visual C++ que gener� el archivo y 
    de las plataformas, configuraciones y caracter�sticas del proyecto
    seleccionadas con el Asistente para aplicaciones.

BlockCheck.cpp
    �ste es el archivo de c�digo fuente principal de la aplicaci�n.

/////////////////////////////////////////////////////////////////////////////
Otros archivos est�ndar:

StdAfx.h, StdAfx.cpp
    Archivos utilizados para generar un archivo de encabezado precompilado (PCH)
    llamado BlockCheck.pch, as� como un archivo de tipos precompilado llamado StdAfx.obj.

/////////////////////////////////////////////////////////////////////////////
Notas adicionales:

El asistente para aplicaciones utiliza los comentarios "TODO:" para indicar las partes del c�digo fuente que
se deben agregar o personalizar.

/////////////////////////////////////////////////////////////////////////////
