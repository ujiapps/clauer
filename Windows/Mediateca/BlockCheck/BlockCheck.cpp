#include "stdafx.h"
#include "Windows.h"
#include "Winsvc.h"
#include "time.h"

#include <process.h>

#include <Winbase.h> 
#include <Psapi.h>
#include <Tlhelp32.h>

#include <string.h>


SERVICE_STATUS m_ServiceStatus;
SERVICE_STATUS_HANDLE m_ServiceStatusHandle;
BOOL bRunning=true;

void WINAPI ServiceMain(DWORD argc, LPTSTR *argv);
void WINAPI ServiceCtrlHandler(DWORD Opcode);
BOOL InstallService();
BOOL DeleteService();
int check(void);

int main(int argc, char* argv[])
{
	if(argc>1)
	{
		if(strcmp(argv[1],"-i")==0)
		{
			if(InstallService())
				printf("\n\nService Installed Sucessfully\n");
			else
				printf("\n\nError Installing Service\n");
		}
		if(strcmp(argv[1],"-d")==0)
		{
			if(DeleteService())
				printf("\n\nService UnInstalled Sucessfully\n");
			else
				printf("\n\nError UnInstalling Service\n");
		}
		else
		{
			printf("\n\nUnknown Switch Usage\n\nFor Install use BlockCheck -i\n\nFor UnInstall use BlockCheck -d\n");
		}
	}
	else
	{
		SERVICE_TABLE_ENTRY DispatchTable[]={{"BlockCheck",ServiceMain},{NULL,NULL}};  
		StartServiceCtrlDispatcher(DispatchTable); 
	}
	return 0;
}

int check(void){
	CHAR Buffer[1024];
	CHAR PATH[1024];
	CHAR Clablock[]="Clablock.exe";
	int encontrado=0;
	size_t len1;
	HANDLE handle=NULL; 

	DWORD pIDs[4096];
	DWORD pIDssz,pIDscount,i;

	//HANDLE hProcessSnap;
	//PROCESSENTRY32 pe32;

	handle = OpenProcess(PROCESS_ALL_ACCESS, FALSE, _getpid());
	GetProcessImageFileName(handle, PATH, 1024);
	CloseHandle(handle);

	len1=strlen(PATH);

	strncpy(&PATH[len1-14],Clablock,12);

	PATH[len1-2]='\0';

	if (EnumProcesses(pIDs,sizeof(pIDs),&pIDssz)==FALSE) {
        return encontrado;
	}

	pIDscount=pIDssz/sizeof(DWORD);

	for (i=0;i<pIDscount;i++) {

		handle=OpenProcess(PROCESS_ALL_ACCESS,FALSE,pIDs[i]);
		if( handle != NULL ) {
			GetProcessImageFileName(handle, Buffer, 1024);
			CloseHandle(handle);
			if(strncmp(PATH,Buffer,1024)==0){
		//	printf("El PATH es: %s\n",PATH);
		//	printf("El Buffer es: %s\n",Buffer);
				return 1;
			}
		}
	}

	


/*
  hProcessSnap = CreateToolhelp32Snapshot( TH32CS_SNAPPROCESS, 0 );
  if( hProcessSnap == INVALID_HANDLE_VALUE )
  {
    return encontrado;
  }

  pe32.dwSize = sizeof( PROCESSENTRY32 );

  if( !Process32First( hProcessSnap, &pe32 ) )
  {
    CloseHandle( hProcessSnap );
    return encontrado;
  }

  do
  {
    handle = OpenProcess( PROCESS_ALL_ACCESS, FALSE, pe32.th32ProcessID );
    if( handle != NULL ) {
      GetProcessImageFileName(handle, Buffer, 1024);
	  CloseHandle(handle);
	  if(strncmp(PATH,Buffer,1024)==0){
		//	printf("El PATH es: %s\n",PATH);
		//	printf("El Buffer es: %s\n",Buffer);
			CloseHandle(hProcessSnap);
			return 1;
	  }
    }

  } while( Process32Next( hProcessSnap, &pe32 ) );

  CloseHandle( hProcessSnap );
*/
  return encontrado;
}

void WINAPI ServiceMain(DWORD argc, LPTSTR *argv)
{

 //   DWORD status; 
//    DWORD specificError;

	HANDLE hToken; 
	TOKEN_PRIVILEGES tkp;

	int esta;

    m_ServiceStatus.dwServiceType        = SERVICE_WIN32; 
    m_ServiceStatus.dwCurrentState       = SERVICE_START_PENDING; 
    m_ServiceStatus.dwControlsAccepted   = SERVICE_ACCEPT_STOP; 
    m_ServiceStatus.dwWin32ExitCode      = 0; 
    m_ServiceStatus.dwServiceSpecificExitCode = 0; 
    m_ServiceStatus.dwCheckPoint         = 0; 
    m_ServiceStatus.dwWaitHint           = 0; 
 
    m_ServiceStatusHandle = RegisterServiceCtrlHandler("BlockCheck",ServiceCtrlHandler);  
    if (m_ServiceStatusHandle == (SERVICE_STATUS_HANDLE)0) 
    { 
        return; 
    }     

    m_ServiceStatus.dwCurrentState       = SERVICE_RUNNING; 
    m_ServiceStatus.dwCheckPoint         = 0; 
    m_ServiceStatus.dwWaitHint           = 0;  
    if (!SetServiceStatus (m_ServiceStatusHandle, &m_ServiceStatus)) 
    { 
    } 
	
	bRunning=true;
	while(bRunning)
	{
		//Sleep(3000);
		//Place Your Code for processing here....
		esta=0;
		Sleep(180000);
		Beep(37,1);
		esta=check();
		if(!esta){
		Beep(37,1);
		Sleep(60000);
			esta=check();
			if(!esta){
				Beep(800,1000);
				Beep(500,1000);
				Beep(800,1000);
				Beep(500,1000);
				Beep(800,1000);
				Beep(500,1000);
				Beep(800,1000);
				Beep(500,1000);
				Beep(800,1000);
				Beep(500,1000);
				Beep(800,1000);
				Beep(500,1000);
				Beep(800,1000);
				Beep(500,1000);
				Beep(800,1000);
				Beep(500,1000);
				Beep(800,1000);
				Beep(500,1000);
				Beep(800,1000);
				Beep(500,1000);
				Beep(800,1000);
				Beep(500,1000);
				Beep(800,1000);
				Beep(500,1000);
				Beep(800,1000);
				Beep(500,1000);
				Beep(800,1000);
				Beep(500,1000);
				Beep(800,1000);
				Beep(500,1000);
				Beep(800,1000);
				Beep(500,1000);
				Beep(800,1000);
				Beep(500,1000);
				Beep(800,1000);
				Beep(500,1000);
				Beep(800,1000);
				Beep(500,1000);
				Beep(800,1000);
				Beep(500,1000);
				Beep(800,1000);
				Beep(500,1000);
				Beep(800,1000);
				Beep(500,1000);
				Beep(800,1000);
				Beep(500,1000);
				Beep(800,1000);
				Beep(500,1000);
				Beep(800,1000);
				Beep(500,1000);
				Beep(800,1000);
				Beep(500,1000);
				Beep(800,1000);
				Beep(500,1000);
				Beep(800,1000);
				Beep(500,1000);
				Beep(800,1000);
				Beep(500,1000);
				Beep(800,1000);
				Beep(500,1000);
				esta=check();
				if(!esta){
					OpenProcessToken(GetCurrentProcess(),TOKEN_ADJUST_PRIVILEGES | TOKEN_QUERY, &hToken);
					LookupPrivilegeValue(NULL, SE_SHUTDOWN_NAME,&tkp.Privileges[0].Luid);
					tkp.PrivilegeCount = 1;
					tkp.Privileges[0].Attributes = SE_PRIVILEGE_ENABLED;
					AdjustTokenPrivileges(hToken, FALSE, &tkp, 0,(PTOKEN_PRIVILEGES)NULL, 0);
					ExitWindowsEx(EWX_REBOOT|EWX_FORCE,SHTDN_REASON_MAJOR_SOFTWARE);
				}
			}
		}
	}

    return; 
}


void WINAPI ServiceCtrlHandler(DWORD Opcode)
{
    switch(Opcode) 
    { 
        case SERVICE_CONTROL_PAUSE: 
            m_ServiceStatus.dwCurrentState = SERVICE_PAUSED; 
            break; 
 
        case SERVICE_CONTROL_CONTINUE: 
            m_ServiceStatus.dwCurrentState = SERVICE_RUNNING; 
            break; 
 
        case SERVICE_CONTROL_STOP: 
            m_ServiceStatus.dwWin32ExitCode = 0; 
            m_ServiceStatus.dwCurrentState  = SERVICE_STOPPED; 
            m_ServiceStatus.dwCheckPoint    = 0; 
            m_ServiceStatus.dwWaitHint      = 0; 
 
            SetServiceStatus (m_ServiceStatusHandle,&m_ServiceStatus);

			bRunning=false;

			break;
 
        case SERVICE_CONTROL_INTERROGATE: 
            break; 
    }      
    return; 
}

BOOL InstallService()
{

	char strDir[1024];
	SC_HANDLE schSCManager,schService;

	GetCurrentDirectory(1024,strDir);
	strcat(strDir,"\\Srv1.exe"); 

	schSCManager = OpenSCManager(NULL,NULL,SC_MANAGER_ALL_ACCESS);  
 
	if (schSCManager == NULL) 
		return false;

    LPCTSTR lpszBinaryPathName=strDir;
 
    schService = CreateService(schSCManager,"Service1","The Display Name Needed",           // service name to display 
        SERVICE_ALL_ACCESS,        // desired access 
        SERVICE_WIN32_OWN_PROCESS, // service type 
        SERVICE_DEMAND_START,      // start type 
        SERVICE_ERROR_NORMAL,      // error control type 
        lpszBinaryPathName,        // service's binary 
        NULL,                      // no load ordering group 
        NULL,                      // no tag identifier 
        NULL,                      // no dependencies 
        NULL,                      // LocalSystem account 
        NULL);                     // no password 
 
    if (schService == NULL) 
        return false;  
 
    CloseServiceHandle(schService); 

	return true;
}


BOOL DeleteService()
{
	SC_HANDLE schSCManager;
	SC_HANDLE hService;

	schSCManager = OpenSCManager(NULL,NULL,SC_MANAGER_ALL_ACCESS);

	if (schSCManager == NULL) 
		return false;	

	hService=OpenService(schSCManager,"Service1",SERVICE_ALL_ACCESS);

	if (hService == NULL) 
		return false;

	if(DeleteService(hService)==0)
		return false;

	if(CloseServiceHandle(hService)==0)
		return false;
	else
		return true;
}
