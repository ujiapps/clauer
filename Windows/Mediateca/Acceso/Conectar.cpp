#include "Conectar.h"
#include "stdafx.h"

#include <windows.h>
#include <winhttp.h>
#include <time.h>
#include <stdio.h>
#include <stdlib.h>
#include <winsock.h>
#include <wincrypt.h>
#include <Cryptuiapi.h>

/*
*
* 1 = OK (PUEDO SEGUIR CONECTADO)
* 0 = SOLO QUEDAN 5 MINUTOS DE CONECXI�N
* 2 = NO EXISTE ID DEL CLAUER / EN SSL NO SE HA PODIDO ACCEDER A LA BASE DE DATOS
* 3 = TIEMPO AGOTADO
* 4 = EL USUARIO NO ESTA AUTORIZADO
*/

int Conectar (char IDClauer[41], char action[3], char * Aula, CHAR * host, char * path)
{


	DWORD dwSize = 0;
    DWORD dwDownloaded = 0;
	DWORD dwTotal = 0;
    BYTE *pszOutBuffer = NULL, *aux;
    BOOL  bResults = FALSE;
    HINTERNET  hSession = NULL, 
               hConnect = NULL,
               hRequest = NULL;

	char *URLBytes = NULL;
	WCHAR *url = NULL;

	LPCWSTR ServerName;

	int ret = 1;

	WSADATA info;
    if ( WSAStartup(MAKEWORD(1,1), &info) != 0 )
		return 1;

	hSession = WinHttpOpen( L"Clablock",  
                            WINHTTP_ACCESS_TYPE_DEFAULT_PROXY,
                            WINHTTP_NO_PROXY_NAME, 
                            WINHTTP_NO_PROXY_BYPASS, 0);
	
	if ( !hSession ) {
		ret =1;
		goto fin_Conexion;
	}

	/* Establecemos los diversos timeouts:
	 *
	 *      - Timeout para resolver nombres: 5s
	 *      - Timeout conexi�n al servidor: 3s
	 *		- Timeout para el env�o de la petici�n: 3s
	 *      - Timeout para la respuesta: 3s
	 */

/*	if ( !WinHttpSetTimeouts(hSession, 5000, 3000, 3000, 3000) ) {
		ret =1;
		goto fin_Conexion;
	}*/


	// Specify an HTTP server.

	USES_CONVERSION;
	ServerName=A2W(host);
	hConnect = WinHttpConnect( hSession,ServerName,
                              INTERNET_DEFAULT_HTTPS_PORT, 0);

	if ( !hConnect ) {
		ret =1;
		goto fin_Conexion;
	}

	URLBytes = (char *) malloc ( sizeof(char)*41 + strlen(path)+ strlen("?p_id=&p_accion=XXX&p_aula=XXXXXX") + 1 );

	if ( !URLBytes ) {
		ret =1;
		goto fin_Conexion;
	}


	url = (WCHAR *) malloc ( sizeof(WCHAR) * ((sizeof(char)*41) + strlen(path)+ strlen("?p_id=&p_accion=XXX&p_aula=XXXXXX") + 1));

	if ( !url ) {
		ret =1;
		goto fin_Conexion;
	}

	sprintf(URLBytes, "%s?p_id=%s&p_accion=%s&p_aula=%s",path, IDClauer, action,Aula);

	if ( !MultiByteToWideChar(CP_ACP,MB_PRECOMPOSED, URLBytes, -1, url, sizeof(char)*41 + strlen(path)+ strlen("?p_id=&p_accion=XXX&p_aula=XXXXXX") + 1) ) {
		ret =1;
		goto fin_Conexion;
	}

	// Create an HTTP request handle.
    hRequest = WinHttpOpenRequest( hConnect, L"GET", url,
                                   NULL, WINHTTP_NO_REFERER, 
                                   WINHTTP_DEFAULT_ACCEPT_TYPES, 
                                   WINHTTP_FLAG_REFRESH | WINHTTP_FLAG_SECURE);

	if ( !hRequest ) {
		ret =1;
		goto fin_Conexion;
	}

 	DWORD options = SECURITY_FLAG_IGNORE_UNKNOWN_CA; 	

	WinHttpSetOption(hRequest,WINHTTP_OPTION_SECURITY_FLAGS,(LPVOID)&options, sizeof (DWORD));

	// Send a request.
    bResults = WinHttpSendRequest( hRequest,
                                   WINHTTP_NO_ADDITIONAL_HEADERS, 0,
                                   WINHTTP_NO_REQUEST_DATA, 0, 
                                   0, 0);
	 
	DWORD dw = GetLastError();

 
   // MessageBox(NULL, szBuf, "Error", MB_OK); 


	if ( !bResults ) {
		ret =1;
		goto fin_Conexion;
	}

	// End the request.

    bResults = WinHttpReceiveResponse( hRequest, NULL);

	if ( !bResults ) {
		DWORD err;
		err = GetLastError();
		ret =1;
		goto fin_Conexion;
	}

	pszOutBuffer = (BYTE *) malloc (1024 * 1024);

	if  (!pszOutBuffer) {
		ret =1;
		goto fin_Conexion;
	}

	aux = pszOutBuffer;

	// Keep checking for data until there is nothing left.
    do 
    {
        // Check for available data.
        dwSize = 0;
        if (!WinHttpQueryDataAvailable( hRequest, &dwSize)) {
			ret =1;
			goto fin_Conexion;
		}

        // Read the Data.

		if ( dwTotal + dwSize >= (1024*1024+1) ) 
			break;
		

        ZeroMemory(aux, dwSize+1);

        if (!WinHttpReadData( hRequest, (LPVOID)aux, dwSize, &dwDownloaded)) {
			ret =1;
			goto fin_Conexion;
		}
    
        // Free the memory allocated to the buffer.
        
		dwTotal += dwDownloaded;
		if ( dwTotal >= (1024*1024 + 1) ) {
			dwSize = 0;
		} else {
			aux += dwDownloaded;
		}

    } while (dwSize>0);

	if ( memcmp(pszOutBuffer, "0", 1) == 0 ) {
		ret =0;
	} else if ( memcmp(pszOutBuffer, "1", 1) == 0 ) {
		ret =1;
	}else if ( memcmp(pszOutBuffer, "2", 1) == 0 ) {
		ret =2;
	}else if ( memcmp(pszOutBuffer, "3", 1) == 0 ) {
		ret =3;
	} else if ( memcmp(pszOutBuffer, "4", 1) == 0 ) {
		ret =4;
	}else {
		ret =1;
	}
	
fin_Conexion:
	if (hRequest) 
		WinHttpCloseHandle(hRequest);

    if (hConnect) 
		WinHttpCloseHandle(hConnect);

    if (hSession) 
		WinHttpCloseHandle(hSession);

	if ( URLBytes )
		free(URLBytes);

	if ( url )
		free(url);

	if ( pszOutBuffer )
		free(pszOutBuffer);
return ret;

}

int ConectarSSL (char IDClauer[41], char action[3],char * Aula, CHAR * hostSSL, char * pathSSL)
{


	DWORD dwSize = 0;
    DWORD dwDownloaded = 0;
	DWORD dwTotal = 0;
    BYTE *pszOutBuffer = NULL, *aux;
    BOOL  bResults = FALSE;
    HINTERNET  hSession = NULL, 
               hConnect = NULL,
               hRequest = NULL;

	char *URLBytes = NULL;
	WCHAR *url = NULL;

	int ret = 1;

	LPCWSTR ServerName;

	WSADATA info;
    if ( WSAStartup(MAKEWORD(1,1), &info) != 0 )
		return 1;

	hSession = WinHttpOpen( L"Clablock",  
                            WINHTTP_ACCESS_TYPE_DEFAULT_PROXY,
                            WINHTTP_NO_PROXY_NAME, 
                            WINHTTP_NO_PROXY_BYPASS, 0);
	
	if ( !hSession ) {
		ret =5;
		goto fin_Conexion;
	}

	/* Establecemos los diversos timeouts:
	 *
	 *      - Timeout para resolver nombres: 5s
	 *      - Timeout conexi�n al servidor: 3s
	 *		- Timeout para el env�o de la petici�n: 3s
	 *      - Timeout para la respuesta: 3s
	 */ 

/*	if ( !WinHttpSetTimeouts(hSession, 5000, 3000, 3000, 3000) ) {
		ret =1;
		goto fin_Conexion;
	}*/


	// Specify an HTTP server.

	USES_CONVERSION;
	ServerName=A2W(hostSSL);

	hConnect = WinHttpConnect( hSession, ServerName,
                               INTERNET_DEFAULT_HTTPS_PORT, 0);

	if ( !hConnect ) {
		ret =5;
		goto fin_Conexion;
	}

	URLBytes = (char *) malloc ( sizeof(char)*41 + strlen(pathSSL)+strlen("?p_id=&p_accion=XXX&p_aula=XXXXXX") + 1 );

	if ( !URLBytes ) {
		ret =5;
		goto fin_Conexion;
	}


	url = (WCHAR *) malloc ( sizeof(WCHAR) * ((sizeof(char)*41) + strlen(pathSSL)+strlen("?p_id=&p_accion=XXX&p_aula=XXXXXX") + 1));

	if ( !url ) {
		ret =5;
		goto fin_Conexion;
	}

	sprintf(URLBytes, "%s?p_id=%s&p_accion=%s&p_aula=%s",pathSSL, IDClauer, action, Aula);

	if ( !MultiByteToWideChar(CP_ACP,MB_PRECOMPOSED, URLBytes, -1, url, sizeof(char)*41 + strlen(pathSSL)+strlen("?p_id=&p_accion=XXX&p_aula=XXXXXX") + 1) ) {
		ret =5;
		goto fin_Conexion;
	}

	// Create an HTTP request handle.
    hRequest = WinHttpOpenRequest( hConnect, L"GET", url,
                                   NULL, WINHTTP_NO_REFERER, 
                                   WINHTTP_DEFAULT_ACCEPT_TYPES,
                                   WINHTTP_FLAG_REFRESH | WINHTTP_FLAG_SECURE);

	if ( !hRequest ) {
		ret =5;
		goto fin_Conexion;
	}

	DWORD options = SECURITY_FLAG_IGNORE_UNKNOWN_CA; 	

	WinHttpSetOption(hRequest,WINHTTP_OPTION_SECURITY_FLAGS,(LPVOID)&options, sizeof (DWORD));

	// Send a request.
    bResults = WinHttpSendRequest( hRequest,
                                   WINHTTP_NO_ADDITIONAL_HEADERS, 0,
                                   WINHTTP_NO_REQUEST_DATA, 0, 
                                   0, 0);
	 
	DWORD dw = GetLastError();

 
   // MessageBox(NULL, szBuf, "Error", MB_OK); 


	if ( !bResults ) {
		ret =5;
		goto fin_Conexion;
	}

	// End the request.

	//MY is the store the certificate is in.
	/*  HCERTSTORE hMyStore;
	  PCCERT_CONTEXT pCertContext;
	  
      hMyStore = CertOpenSystemStore( 0, TEXT("MY") );
      if( hMyStore )
      {
        pCertContext = CertFindCertificateInStore( hMyStore,
             X509_ASN_ENCODING | PKCS_7_ASN_ENCODING,
             0,
             CERT_FIND_ANY,
			 NULL,
             NULL );
        if( pCertContext )
        {
          WinHttpSetOption( hRequest, WINHTTP_OPTION_CLIENT_CERT_CONTEXT,
                            (LPVOID) pCertContext, sizeof(CERT_CONTEXT) );
          CertFreeCertificateContext( pCertContext );
        }
        CertCloseStore( hMyStore, 0 );

        // NOTE: Application should now resend the request.
      }
*/

	/*bResults = WinHttpReceiveResponse( hRequest, NULL);

	if(bResults == FALSE) {
		if(GetLastError()!=ERROR_WINHTTP_CLIENT_AUTH_CERT_NEEDED){
			ret =5;
			goto fin_Conexion;
		}
	}*/

	HCERTSTORE       hCertStore = NULL;        
	PCCERT_CONTEXT   pCertContext = NULL;    
	TCHAR * pszStoreName = TEXT("MY");
	HWND TheMainWndHnd;

	TheMainWndHnd=FindWindow(NULL,NULL);

	//--------------------------------------------------------------------
	//   Open a certificate store.
	hCertStore = CertOpenSystemStore(NULL,pszStoreName);
	//--------------------------------------------------------------------
	//  Display a list of the certificates in the store and 
	//  allow the user to select a certificate. 
	if(!(pCertContext = CryptUIDlgSelectCertificateFromStore(
	hCertStore,      // Open store containing the certificates to 
			           // display
	TheMainWndHnd,
	NULL,
	NULL,
	CRYPTUI_SELECT_LOCATION_COLUMN,
	0,
	NULL)))
	{
			ret =5;
			goto fin_Conexion;
	}

	// NOTE: Application should now resend the request.

	if(!(WinHttpSetOption( hRequest, WINHTTP_OPTION_CLIENT_CERT_CONTEXT,
		(LPVOID) pCertContext, sizeof(CERT_CONTEXT) ))){
			DWORD err;
			err = GetLastError();
			ret =5;
			goto fin_Conexion;

		}

    bResults = WinHttpReceiveResponse( hRequest, NULL);
	
	if(pCertContext)
		CertFreeCertificateContext(pCertContext);

	if ( !bResults ) {
		DWORD err;
		err = GetLastError();
		ret =5;
		goto fin_Conexion;
	}

	pszOutBuffer = (BYTE *) malloc (1024 * 1024);

	if  (!pszOutBuffer) {
		ret =5;
		goto fin_Conexion;
	}

	aux = pszOutBuffer;

	// Keep checking for data until there is nothing left.
    do 
    {
        // Check for available data.
        dwSize = 0;
        if (!WinHttpQueryDataAvailable( hRequest, &dwSize)) {
			ret =5;
			goto fin_Conexion;
		}

        // Read the Data.

		if ( dwTotal + dwSize >= (1024*1024+1) ) 
			break;
		

        ZeroMemory(aux, dwSize+1);

        if (!WinHttpReadData( hRequest, (LPVOID)aux, dwSize, &dwDownloaded)) {
			ret =5;
			goto fin_Conexion;
		}
    
        // Free the memory allocated to the buffer.
        
		dwTotal += dwDownloaded;
		if ( dwTotal >= (1024*1024 + 1) ) {
			dwSize = 0;
		} else {
			aux += dwDownloaded;
		}

    } while (dwSize>0);

	if ( memcmp(pszOutBuffer, "0", 1) == 0 ) {
		ret =0;
	} else if ( memcmp(pszOutBuffer, "1", 1) == 0 ) {
		ret =1;
	}else if ( memcmp(pszOutBuffer, "2", 1) == 0 ) {
		ret =2;
	}else if ( memcmp(pszOutBuffer, "3", 1) == 0 ) {
		ret =3;
	} else if ( memcmp(pszOutBuffer, "4", 1) == 0 ) {
		ret =4;
	}else {
		ret =1;
	}
	
fin_Conexion:
	if (hRequest) 
		WinHttpCloseHandle(hRequest);

    if (hConnect) 
		WinHttpCloseHandle(hConnect);

    if (hSession) 
		WinHttpCloseHandle(hSession);

	if ( URLBytes )
		free(URLBytes);

	if ( url )
		free(url);

	if ( pszOutBuffer )
		free(pszOutBuffer);
return ret;

}