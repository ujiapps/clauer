#pragma once

#include "Conectar.h"
#include "AccesoDlg.h"

// Eventos

class Eventos : public CWinThread
{
	DECLARE_DYNCREATE(Eventos)

protected:
	//Eventos();           // Constructor protegido utilizado por la creaci�n din�mica
	//virtual ~Eventos();

public:
	int m_result;
	virtual BOOL InitInstance();
	virtual int ExitInstance();
	Eventos();           // Constructor protegido utilizado por la creaci�n din�mica
	virtual ~Eventos();
	int Run();

protected:
	DECLARE_MESSAGE_MAP()
};


