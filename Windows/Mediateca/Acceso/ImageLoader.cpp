// ImageLoader.cpp: implementation of the CImageLoader class.
//
//////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "ImageLoader.h"

#ifdef _DEBUG
#undef THIS_FILE
static char THIS_FILE[]=__FILE__;
#define new DEBUG_NEW
#endif

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CImageLoader::CImageLoader()
{
}

CImageLoader::~CImageLoader()
{

}

bool CImageLoader::SetBitmap(CStatic* a_Static, int a_iIndex /* = -1 */)
{
    bool btmpOk=true;

    try
    {
        CString stmpBMP = GetBmpName(a_iIndex);

        HANDLE tmphBMP1 = LoadImage(NULL, stmpBMP, IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE | LR_DEFAULTSIZE);
        HANDLE tmphBMP2 = a_Static->SetBitmap((HBITMAP)tmphBMP1);
        if (tmphBMP2 != NULL)
            DeleteObject(tmphBMP2), tmphBMP2 = NULL;
        a_Static->CenterWindow();
    }
    catch(...)
    {
        btmpOk = false;
    }

    return btmpOk;
}

CString CImageLoader::GetBmpName(int a_iIndex /* = -1 */)
{
    CString stmpRes = "";
    if (m_BmpNameList.size() > 0)
    {
        int itmpIndex = a_iIndex;
        if ((itmpIndex < 0)||(itmpIndex >= m_BmpNameList.size()))
            itmpIndex = rand()%(m_BmpNameList.size());
        stmpRes = m_BmpNameList[itmpIndex];
    }

    return stmpRes;
}

int CImageLoader::GetBmpListSize()
{
    return m_BmpNameList.size();
}

// ACCESSORS
void CImageLoader::SetPath(CString a_sPath)
{ 
//	WIN32_FIND_DATA tmpFileFindData;

//	HANDLE tmpHdFind = FindFirstFile(a_sPath, &tmpFileFindData);
  //      bool btmpFi = (tmpHdFind == INVALID_HANDLE_VALUE);
    //    if (!btmpFi)
      //  {
			m_BmpNameList.push_back(a_sPath);
        //}
}
