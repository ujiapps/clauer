#include "stdafx.h"
#include "MainFrm.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

#define WM_TRAYNOTIFY WM_USER + 100

CTrayNotifyIcon g_TrayIcon3;



IMPLEMENT_DYNCREATE(CMainFrame, CFrameWnd)

BEGIN_MESSAGE_MAP(CMainFrame, CFrameWnd)
	//{{AFX_MSG_MAP(CMainFrame)
	ON_WM_CREATE()
	ON_WM_TIMER()
	ON_WM_DESTROY()
	//}}AFX_MSG_MAP
  ON_MESSAGE(WM_TRAYNOTIFY, OnTrayNotification)
END_MESSAGE_MAP()

CMainFrame::CMainFrame()
{
 /* m_bHappy = TRUE;
  m_hIcons[0] = CTrayNotifyIcon::LoadIconResource(IDR_HAPPY);
  m_hIcons[1] = CTrayNotifyIcon::LoadIconResource(IDR_SAD);*/
  m_nTimerCount = 59;
}

CMainFrame::~CMainFrame()
{
}

int CMainFrame::OnCreate(LPCREATESTRUCT lpCreateStruct) 
{
  //Let the base class do its thing
	if (CFrameWnd::OnCreate(lpCreateStruct) == -1)
		return -1;

  //Create the dynamic count
  if (!CTrayNotifyIcon::GetDynamicDCAndBitmap(&m_TrayIconDC, &m_BitmapTrayIcon))
  {
    AfxMessageBox(_T("Failed to create tray icon 3"), MB_OK | MB_ICONSTOP);
    return -1;
  }

#ifdef _BUILDING_ON_WIN2K //Defined in the build configurations as setup in the project
  if (!g_TrayIcon3.Create(this, IDR_TRAYPOPUP3, _T("Minutos restantes"), _T("Minutos restantes"), _T("Minutos restantes"), 10, CTrayNotifyIcon::Info, &m_BitmapTrayIcon, WM_TRAYNOTIFY, IDR_TRAYPOPUP3))
#else
  if (!g_TrayIcon3.Create(this, IDR_TRAYPOPUP3, _T("Minutos restantes"), &m_BitmapTrayIcon, WM_TRAYNOTIFY, IDR_TRAYPOPUP3))
#endif
  {
    AfxMessageBox(_T("Failed to create tray icon 3"), MB_OK | MB_ICONSTOP);
    return -1;
  }

  //Also create a timer used for the dynamic tray icons

    m_TrayIconDC.SetBkMode(TRANSPARENT);
    m_TrayIconDC.SetTextColor(RGB(255,255,255));
    int w = m_TrayIconDC.GetDeviceCaps(LOGPIXELSX);
    int h = m_TrayIconDC.GetDeviceCaps(LOGPIXELSY);
    CRect r;
    r.top=0;
    r.left=0;
    r.right=w;
    r.bottom=h;
    CBrush blackBrush;
    blackBrush.CreateStockObject(BLACK_BRUSH);
    m_TrayIconDC.FillRect(&r, &blackBrush);
    m_TrayIconDC.TextOut(0, 0, "59");

  //Update
  g_TrayIcon3.SetIcon(&m_BitmapTrayIcon);

  //Cada minuto
  m_nTimerID = SetTimer(1, 60000, NULL);

	return 0;
}

LRESULT CMainFrame::OnTrayNotification(WPARAM wParam, LPARAM lParam)
{
  //Delegate all the work back to the default implementation in
  //CTrayNotifyIcon.
  g_TrayIcon3.OnTrayNotification(wParam, lParam);
  return 0L;
}


void CMainFrame::OnTimer(UINT nIDEvent) 
{
  if (m_nTimerID == nIDEvent)
  {
	  
	//Increment the count, ready for the next time aroun
    m_nTimerCount--;

    //Draw into the tray icon bitmap
    CString sNum;
    sNum.Format(_T("%d"), m_nTimerCount);
    m_TrayIconDC.SetBkMode(TRANSPARENT);
    m_TrayIconDC.SetTextColor(RGB(255,255,255));
    int w = m_TrayIconDC.GetDeviceCaps(LOGPIXELSX);
    int h = m_TrayIconDC.GetDeviceCaps(LOGPIXELSY);
    CRect r;
    r.top=0;
    r.left=0;
    r.right=w;
    r.bottom=h;
    CBrush blackBrush;
    blackBrush.CreateStockObject(BLACK_BRUSH);
    m_TrayIconDC.FillRect(&r, &blackBrush);
    m_TrayIconDC.TextOut(0, 0, sNum);

    //Update it
    g_TrayIcon3.SetIcon(&m_BitmapTrayIcon);

    //Increment the count, ready for the next time aroun
    m_nTimerCount--;
    if (m_nTimerCount == 0)
      m_nTimerCount = 59;
  }
}

void CMainFrame::OnDestroy() 
{
  //Destroy our timer
  KillTimer(m_nTimerID);

  //Let the parent class do its thing
	CFrameWnd::OnDestroy();
}
