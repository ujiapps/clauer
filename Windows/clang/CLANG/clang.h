#ifndef __CLANG_H__
#define __CLANG_H__

#include <windows.h>
#include <tchar.h>

#include "langids.h"

/* M�ximo tama�o de cadena devuelta
*/

#define MAX_STRING_SIZE 200	

/* dll por defecto que se cargar� en caso de
 * no encontrar la correspondiente al idioma elegido
 */

#define DEFAULT_LANG_DLL_NAME	_T("1027_ca.dll")


#ifdef __cplusplus
extern "C" {
#endif

BOOL    CLANG_Init      ( void );
TCHAR * CLANG_GetString ( DWORD dwStringId );
BOOL    CLANG_End       ( void );

#ifdef __cplusplus
}
#endif


#endif
