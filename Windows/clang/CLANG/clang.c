#include "clang.h"



static HANDLE g_hLangDll = 0;



BOOL WINAPI DllMain( HINSTANCE hinstDLL, DWORD fdwReason, LPVOID lpvReserved )
{
   // Perform actions based on the reason for calling.
    switch( fdwReason ) 
    { 
        case DLL_PROCESS_ATTACH:
         // Initialize once for each new process.
         // Return FALSE to fail DLL load.
			g_hLangDll = hinstDLL;
            break;

        case DLL_THREAD_ATTACH:
         // Do thread-specific initialization.
			DisableThreadLibraryCalls(hinstDLL);
            break;

        case DLL_THREAD_DETACH:
         // Do thread-specific cleanup.
            break;

        case DLL_PROCESS_DETACH:
         // Perform any necessary cleanup.
            break;
    }
    return TRUE;  // Successful DLL_PROCESS_ATTACH.

}





/* Carga la dll de idioma asociada al c�digo indicado de idioma de
 * instalaci�n.
 *
 * Las dlls de idioma residen en el directorio lang de instalaci�n
 * (INSDIR) y tienen el siguiente formato ID_desc.dll. Donde ID es
 * el c�digo num�rico correspondiente al idioma y desc es una cadena
 * descriptiva. Por ejemplo, la dll de idioma para el catal�n es el
 * siguiente: 1027_ca.dll
 *
 * El c�digo de idioma se obtiene del valor almacenado en el registro
 * idioma   
 */

BOOL CLANG_Init (void)
{
	HKEY hKey;
	DWORD dwLangId, dwType, dwSize,dwTotalSize;
	TCHAR szDllName[MAX_PATH];
	HANDLE hFindFile = 0;
	WIN32_FIND_DATA FindFileData;

	/* Constru�mos el camino a la dll
	 */

	if ( RegOpenKeyEx(HKEY_LOCAL_MACHINE, _T("Software\\Universitat Jaume I\\Projecte Clauer"), 0, KEY_READ, &hKey) != ERROR_SUCCESS )
		return FALSE;
	if ( RegQueryValueEx(hKey, _T("idioma"), NULL, &dwType, NULL, &dwSize) != ERROR_SUCCESS ) {
		RegCloseKey(hKey);
		return FALSE;
	}
	if ( (dwType != REG_DWORD) || (dwSize != sizeof dwLangId ) ) {
		RegCloseKey(hKey);
		return FALSE;
	}
	if ( RegQueryValueEx(hKey, _T("idioma"), NULL, &dwType, (LPBYTE) &dwLangId, &dwSize) != ERROR_SUCCESS ) {
		RegCloseKey(hKey);
		return FALSE;
	}
	dwTotalSize = dwSize;
	if ( RegQueryValueEx(hKey, _T("INSDIR"), NULL, &dwType, NULL, &dwSize) != ERROR_SUCCESS ) {
		RegCloseKey(hKey);
		return FALSE;
	}
	if ( (dwType != REG_SZ) || (dwSize > MAX_PATH) ) {
		RegCloseKey(hKey);
		return FALSE;
	}
	if ( RegQueryValueEx(hKey, _T("INSDIR"), NULL, &dwType, (LPBYTE) szDllName, &dwSize) != ERROR_SUCCESS ) {
		RegCloseKey(hKey);
		return FALSE;
	}
	RegCloseKey(hKey);
	dwTotalSize += dwSize;

	if ( dwTotalSize > MAX_PATH )
		return FALSE;

	_tcscat(szDllName, _T("\\"));
	_ltot(dwLangId,szDllName+(dwSize/sizeof(TCHAR)),10);
	_tcscat(szDllName, _T("_*.dll"));

	/* Nos quedamos con el primer fichero que cumpla con la m�scara que hemos
	 * especificado anteriormente
	 */

	hFindFile = FindFirstFile(szDllName, &FindFileData);
	if ( hFindFile == INVALID_HANDLE_VALUE ) {
		/* Si no la encuentra tomamos una dll por defecto
		 */
		*(szDllName+(dwSize/sizeof(TCHAR))) = _T('\0');
		_tcscat(szDllName+(dwSize/sizeof(TCHAR)), DEFAULT_LANG_DLL_NAME);
		_tcscpy(FindFileData.cFileName, szDllName);
	} else
		FindClose(hFindFile);

	/* Ahora ya podemos cargarla
	 */

	g_hLangDll = LoadLibraryEx(FindFileData.cFileName,NULL,0);
	if ( ! g_hLangDll )
		return FALSE;

	/* Todo ok
	 */

	return TRUE;
}


/* Obtiene un string de la librer�a de idiomas. Para evitar tener que liberar
 * desde la rutina que llama la memoria ocupada por el string, lo que realmente
 * se devuelve es un puntero est�tico interno a esta funci�n. Esto implica dos
 * cosas:
 *
 *       1. No hay que liberar la memoria ocupada por el string desde fuera de
 *          la funci�n
 *
 *		 2. La funci�n no es thread safe. Pero esto, de momento, es un mal menor
 */

TCHAR * CLANG_GetString ( DWORD dwStringId )
{
	static TCHAR str[MAX_STRING_SIZE];

	if ( ! g_hLangDll )
		return NULL;

	if ( ! LoadString(g_hLangDll, (int) dwStringId, str, MAX_STRING_SIZE) ) 
		_tcscpy(str, _T("Can't get proper string. Reinstall the software!!"));

	return str;
}



BOOL CLANG_End ( void )
{

	FreeLibrary(g_hLangDll);

	return TRUE;
}
