// clost.h : main header file for the CLOST application
//

#if !defined(AFX_CLOST_H__AA6102C3_9A6A_4912_AF42_93B0AB1DF770__INCLUDED_)
#define AFX_CLOST_H__AA6102C3_9A6A_4912_AF42_93B0AB1DF770__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"		// main symbols

/////////////////////////////////////////////////////////////////////////////
// CClostApp:
// See clost.cpp for the implementation of this class
//

class CClostApp : public CWinApp
{
public:
	CClostApp();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CClostApp)
	public:
	virtual BOOL InitInstance();
	//}}AFX_VIRTUAL

// Implementation

	//{{AFX_MSG(CClostApp)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CLOST_H__AA6102C3_9A6A_4912_AF42_93B0AB1DF770__INCLUDED_)
