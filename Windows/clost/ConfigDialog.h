#if !defined(AFX_CONFIGDIALOG_H__F4D85DD2_E0A8_41FF_93C2_8E028889BEBA__INCLUDED_)
#define AFX_CONFIGDIALOG_H__F4D85DD2_E0A8_41FF_93C2_8E028889BEBA__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// ConfigDialog.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CConfigDialog dialog

class CConfigDialog : public CDialog
{
// Construction
public:
	CConfigDialog(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CConfigDialog)
	enum { IDD = DLG_CONFIGURACION };
	CButton	m_btnCancel;
	CButton	m_btnOk;
	CString	m_editLugar;
	CString	m_editHorario;
	CString	m_editSMTP;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CConfigDialog)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CConfigDialog)
	virtual void OnCancel();
	virtual void OnOK();
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CONFIGDIALOG_H__F4D85DD2_E0A8_41FF_93C2_8E028889BEBA__INCLUDED_)
