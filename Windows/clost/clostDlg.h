// clostDlg.h : header file
//

#if !defined(AFX_CLOSTDLG_H__6AB21C2F_5B45_4A3C_87DA_D655BAC82898__INCLUDED_)
#define AFX_CLOSTDLG_H__6AB21C2F_5B45_4A3C_87DA_D655BAC82898__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

/////////////////////////////////////////////////////////////////////////////
// CClostDlg dialog

class CClostDlg : public CDialog
{
// Construction
public:
	CClostDlg(CWnd* pParent = NULL);	// standard constructor

// Dialog Data
	//{{AFX_DATA(CClostDlg)
	enum { IDD = IDD_CLOST_DIALOG };
	CButton	m_btnConfigurar;
	CButton	m_btnSalir;
	CButton	m_btnActualizar;
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CClostDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	HMODULE hMAPIDll;
	HICON m_hIcon;

	// Generated message map functions
	//{{AFX_MSG(CClostDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	afx_msg void OnActualizar();
	afx_msg void OnSalir();
	afx_msg void OnClose();
	afx_msg void OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags);
	afx_msg void OnConfigurar();
	afx_msg BOOL OnDeviceChange( UINT nEventType, DWORD dwData );
	afx_msg void OnBtnAbout();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CLOSTDLG_H__6AB21C2F_5B45_4A3C_87DA_D655BAC82898__INCLUDED_)
