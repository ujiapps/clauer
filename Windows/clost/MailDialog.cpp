// MailDialog.cpp : implementation file
//

#include "stdafx.h"
#include "clost.h"
#include "MailDialog.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CMailDialog dialog


CMailDialog::CMailDialog(CWnd* pParent /*=NULL*/)
	: CDialog(CMailDialog::IDD, pParent)
{
	//{{AFX_DATA_INIT(CMailDialog)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}


void CMailDialog::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CMailDialog)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CMailDialog, CDialog)
	//{{AFX_MSG_MAP(CMailDialog)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CMailDialog message handlers

BOOL CMailDialog::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	/* Centro el di�logo en el padre */


	this->CenterWindow();



	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}


