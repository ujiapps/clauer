// PropietarioDlg.cpp : implementation file
//

#include "stdafx.h"
#include "clost.h"
#include "PropietarioDlg.h"
#include "smtpsimpson/smtp.h"
#include "MailDialog.h"

#include <windows.h>
#include <wincrypt.h>
#include <dbt.h>
#include <devguid.h>


#include <LIBRT/LIBRT.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif





/////////////////////////////////////////////////////////////////////////////
// CPropietarioDlg dialog


CPropietarioDlg::CPropietarioDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CPropietarioDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CPropietarioDlg)
	//}}AFX_DATA_INIT


}


void CPropietarioDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CPropietarioDlg)
	DDX_Control(pDX, IDC_LIST_PROPIETARIO, m_lstPropietario);
	DDX_Control(pDX, IDC_BUTTON_CERRAR, m_btnCerrar);
	DDX_Control(pDX, BTN_EnviarMail, m_btnMail);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CPropietarioDlg, CDialog)
	//{{AFX_MSG_MAP(CPropietarioDlg)
	ON_BN_CLICKED(IDC_BUTTON_CERRAR, OnButtonCerrar)
	ON_BN_CLICKED(BTN_EnviarMail, OnEnviarMail)
	ON_WM_CLOSE()
	ON_WM_DEVICECHANGE()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

//
/////////////////////////////////////////////////////////////////////////////
// CPropietarioDlg message handlers

BOOL CPropietarioDlg::OnInitDialog() 
{
	CDialog::OnInitDialog();

	this->mailEnviado = FALSE;

	/* Ajustamos la posici�n */

	WINDOWPLACEMENT wpParent, wpThis;
	DWORD widthParent, widthThis, heightThis;

	if ( ! this->GetParent()->GetWindowPlacement(&wpParent) ) {
		MessageBox("Imposible crear ventana", "CLOST :: ERROR :: Datos de usuario", MB_OK | MB_ICONERROR);
		EndDialog(1);
	}

	if ( ! this->GetWindowPlacement(&wpThis) ) {
		MessageBox("Imposible crear ventana", "CLOST :: ERROR :: Datos de usuario", MB_OK | MB_ICONERROR);
		EndDialog(1);
	}

	widthParent = wpParent.rcNormalPosition.right - wpParent.rcNormalPosition.left;
	widthThis   = wpThis.rcNormalPosition.right - wpParent.rcNormalPosition.left;
	heightThis  = wpThis.rcNormalPosition.bottom - wpThis.rcNormalPosition.top;

	wpThis.rcNormalPosition.left   = wpParent.rcNormalPosition.left + widthParent;
	wpThis.rcNormalPosition.right  = wpThis.rcNormalPosition.left + widthThis;
	wpThis.rcNormalPosition.top    = wpParent.rcNormalPosition.top;
	wpThis.rcNormalPosition.bottom = wpThis.rcNormalPosition.top + heightThis;

	if ( ! this->SetWindowPlacement(&wpThis) ) {
		MessageBox("Imposible crear ventana", "CLOST :: ERROR :: Datos de usuario", MB_OK | MB_ICONERROR);
		EndDialog(1);
	}


	/* Creamos las columnas del ListCtrl */

	this->m_lstPropietario.SetExtendedStyle(this->m_lstPropietario.GetExtendedStyle() |
											LVS_EX_FULLROWSELECT |
											LVS_EX_GRIDLINES );

	this->m_lstPropietario.InsertColumn(0, "NIF");
	this->m_lstPropietario.InsertColumn(1, "Nombre");

	/* Rellenamos los datos del usuario */

	TCHAR dato[1024];
	DWORD nChars = 1024, maxNIF=0, maxNombre=0;
	CString nombre, nif, mail, aux;
	int pos, nItem = 0;
	BOOL hayCerts = FALSE;
	LVFINDINFO findInfo;
	CRect subRect;

	findInfo.flags = LVFI_STRING;

	hStore = CertOpenStore("ClauerStoreProvider", 0, NULL, 0, NULL);

	if ( ! hStore ) {
		MessageBox("No se pudo abrir store de certificados", "CLOST :: ERROR :: No se pudo abrir store", MB_OK | MB_ICONERROR);
		EndDialog(1);
		return TRUE;
	}

	pCertContext = NULL;
	pCertContext = CertEnumCertificatesInStore(hStore, pCertContext);

	if ( ! pCertContext ) {
		CertCloseStore(hStore, 0);
		MessageBox("No hay certificados en el stick. No se pudo determinar propietario", "CLOST :: ATENCI�N", MB_OK | MB_ICONINFORMATION);
		EndDialog(0);
		return TRUE;
	}

	do {

		if ( CertGetNameString(pCertContext, CERT_NAME_ATTR_TYPE, 0, szOID_COMMON_NAME, dato, nChars) == 1) {
			CertCloseStore(hStore, 0);
			MessageBox("No se pudo extraer common name", "CLOST :: ERROR :: Error obteniendo datos", MB_OK | MB_ICONERROR);
			EndDialog(1);
			return TRUE;
		}

		nombre = dato;
		pos    = nombre.Find(" - NIF:", 0);

		nif    = nombre.Mid(pos+7);
		nombre = nombre.Left(pos);

		/* Si el usuario no lo hemos insertado a�n, lo insertamos
		 * en la lista
		 */

		findInfo.psz = nif.GetBuffer(0);

		if ( (nItem = this->m_lstPropietario.FindItem(&findInfo)) == -1 ) {

			this->m_lstPropietario.InsertItem(0, nif.GetBuffer(0));
			this->m_lstPropietario.SetItemText(0, 1, nombre.GetBuffer(0));

		} else {

			/* Si el nombre tambi�n es igual, entonces no lo insertamos
			 * pero si no lo es pues entonces a tope con ellos. Esto puede
			 * parecer una chorrada, pero si quisiera folarle el clauer 
			 * a alguien me fabricar�a un certificado con el NIF del otro
			 * y mi nombre. En fin... ser�a una opci�n poco inteligente,
			 * pero no est� de m�s comprobarlo. 
			 */

			aux = this->m_lstPropietario.GetItemText(nItem, 1);

			if ( aux != nombre ) {
				this->m_lstPropietario.InsertItem(0, nif.GetBuffer(0));
				this->m_lstPropietario.SetItemText(0, 1, nombre.GetBuffer(0));
			}
				

		}

		/* Saco la direcci�n de correo y la inserto en mi lista de direcciones de correo
		 * si no existe ya junto con el nombre asociado
		 */

		if ( CertGetNameString(pCertContext, CERT_NAME_EMAIL_TYPE, 0, NULL, dato, nChars) != 1) {		
			mail = dato;
			if ( emails.find(mail) == emails.end() ) 
				emails[mail] = nombre;
		}

		++nItem;

	} while (pCertContext = CertEnumCertificatesInStore(hStore, pCertContext));


	/* Ajustamos el tama�o de las columnas
	 */

	this->m_lstPropietario.SetColumnWidth(0,LVSCW_AUTOSIZE);
	this->m_lstPropietario.SetColumnWidth(1, LVSCW_AUTOSIZE_USEHEADER);



	CertCloseStore(hStore,0);
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}



void CPropietarioDlg::OnButtonCerrar() 
{
	EndDialog(0);	
}







void CPropietarioDlg::OnBtnSiguiente() 
{
	// TODO: Add your control notification handler code here


}



void CPropietarioDlg::OnEnviarMail() 
{

	 CSmtp mail;
	 CSmtpMessage msg;
	 CSmtpAddress cc;
	 CSmtpMessageBody body;
	 CWinApp *app;
	 CString cuerpo, aux;
	 CMailDialog mailDlg;

	 if ( mailEnviado ) {
		 int ret;
		ret = MessageBox(_T("Ya envi� un mail al propietario. �Desea volver a enviarlo?"), "CLOST :: ATENCI�N", MB_OKCANCEL);

		if ( ret == IDCANCEL )
			return;
	 }


	 if ( emails.size() == 0 ) {
		MessageBox("No hay direcciones de correo disponibles para enviar el mail",
				   "CLOST :: ATENCION", MB_OK|MB_ICONWARNING);
		return;
	 }

	 mailDlg.Create(IDD_DIALOG_ENVIANDO_MAIL, this);
	 mailDlg.ShowWindow(SW_SHOWNORMAL);
	 mailDlg.UpdateWindow();

	 map<CString, CString>::iterator i;

	 app = AfxGetApp();

	 UpdateData(TRUE);

	 // Initialize winsock
	 WSADATA wsa;
	 WSAStartup(MAKEWORD(2,0),&wsa);

	 msg.Subject = _T("Clauer perdido. Puede pasar a recogerlo");

	 msg.Sender.Name = _T("Clauer");
	 msg.Sender.Address = _T("clauer@uji.es");

	 i = emails.begin();
	 aux = i->second;
	 msg.Recipient.Name = _T(aux.GetBuffer(0));
	 aux = i->first;
	 msg.Recipient.Address = _T(aux.GetBuffer(0));

	 // Ahora CC al resto de direcciones

	 i++;
	 while ( i != emails.end() ) {
		aux = i->second; 
		cc.Name = _T(aux.GetBuffer(0));
		aux = i->first;
		cc.Address = _T(aux.GetBuffer(0));
		msg.CC.Add(cc);
		i++;
	 }

	 // Assign a value to the message body

	 cuerpo = "Este mail es para informarle que su clauer ha sido encontrado y para indicarle ";
	 cuerpo += "d�nde puede pasar a recogerlo.\r\n\r\n";
	 cuerpo += "   Lugar: " + app->GetProfileString("config", "donde") + "\r\n";
	 cuerpo += "   Horario: " + app->GetProfileString("config", "horario") + "\r\n\r\n";
	 cuerpo += "Un saludo.";

	 body = _T(cuerpo.GetBuffer(0));

	 msg.Message.Add(body);

	 // Attempt to connect to the mailserver
	 if (mail.Connect(_T(app->GetProfileString("config", "smtpserver").GetBuffer(0))))
	 {
	  // Send the message and close the connection afterwards
		mail.SendMessage(msg);
		mail.Close();

		MessageBox(_T("Mensaje enviado correctamente"), "CLOST :: ATENCI�N", MB_OK | MB_ICONINFORMATION);
		this->mailEnviado = TRUE;
	 } else {
		MessageBox(_T("No se pudo enviar mail.\r\nImposible conectar con el servidor."), _T("CLOST :: ERROR"), MB_OK | MB_ICONERROR);
	 }

	 WSACleanup();


	 mailDlg.ShowWindow(SW_HIDE);

}

void CPropietarioDlg::OnClose() 
{
	CDialog::OnClose();
}



BOOL CPropietarioDlg::OnDeviceChange( UINT nEventType, DWORD dwData )
{

	int nDisp;
	BYTE *disp = NULL;

	if ( DBT_DEVICEREMOVECOMPLETE == nEventType ) {
		
		if ( LIBRT_ListarDispositivos(&nDisp, &disp) != 0 ) {
			EndDialog(1);
			return TRUE;			
		}

		if ( nDisp == 0 ) {
			EndDialog(0);
		} else {
			free(disp);
			if ( nDisp> 1 ) {
				EndDialog(1);
				return TRUE;
			}
		} 
	} else if ( DBT_DEVICEARRIVAL == nEventType ) {

		if ( LIBRT_ListarDispositivos(&nDisp, &disp) != 0 ) {
			EndDialog(1);
			return TRUE;
		}

		if ( nDisp == 0 ) {
			EndDialog(0);
			return TRUE;
		} else {
			free(disp);
			if ( nDisp> 1 ) {
				EndDialog(1);
				return TRUE;
			}
		} 

	}

	return TRUE;
}

