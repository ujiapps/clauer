#if !defined(AFX_MAILDIALOG_H__A91DD917_738B_4CB2_BC02_20B9C9E8AAF7__INCLUDED_)
#define AFX_MAILDIALOG_H__A91DD917_738B_4CB2_BC02_20B9C9E8AAF7__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// MailDialog.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CMailDialog dialog

class CMailDialog : public CDialog
{
// Construction
public:
	CMailDialog(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CMailDialog)
	enum { IDD = IDD_DIALOG_ENVIANDO_MAIL };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CMailDialog)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CMailDialog)
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_MAILDIALOG_H__A91DD917_738B_4CB2_BC02_20B9C9E8AAF7__INCLUDED_)
