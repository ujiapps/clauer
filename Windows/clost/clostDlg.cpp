// clostDlg.cpp : implementation file
//

#include "stdafx.h"
#include "clost.h"
#include "clostDlg.h"
#include "PropietarioDlg.h"
#include "ConfigDialog.h"
#include "AboutDlg.h"

#include <windows.h>
#include <wincrypt.h>
#include <mapi.h>
#include <dbt.h>

#include <LIBRT/LIBRT.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CClostDlg dialog

CClostDlg::CClostDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CClostDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CClostDlg)
	//}}AFX_DATA_INIT
	// Note that LoadIcon does not require a subsequent DestroyIcon in Win32
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
	hMAPIDll = NULL;

	LIBRT_Ini();
}

void CClostDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CClostDlg)
	DDX_Control(pDX, BTN_CONFIGURAR, m_btnConfigurar);
	DDX_Control(pDX, BTN_SALIR, m_btnSalir);
	DDX_Control(pDX, BTN_ACTUALIZAR, m_btnActualizar);
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CClostDlg, CDialog)
	//{{AFX_MSG_MAP(CClostDlg)
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(BTN_ACTUALIZAR, OnActualizar)
	ON_BN_CLICKED(BTN_SALIR, OnSalir)
	ON_WM_CLOSE()
	ON_WM_KEYDOWN()
	ON_BN_CLICKED(BTN_CONFIGURAR, OnConfigurar)
	ON_BN_CLICKED(ID_BTN_ABOUT, OnBtnAbout)
	ON_WM_DEVICECHANGE()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CClostDlg message handlers

BOOL CClostDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon
	
	// TODO: Add extra initialization here

	hMAPIDll = LoadLibrary("MAPI32.dll");

	if ( ! hMAPIDll ) {
		MessageBox("No se pudo cargar MAPI32.dll", "CLOST :: ERROR :: Iniciando aplicaci�n", MB_OK | MB_ICONERROR);
		EndDialog(1);
	}


	
	return TRUE;  // return TRUE  unless you set the focus to a control
}

// If you add a minimize button to your dialog, you will need the code below
//  to draw the icon.  For MFC applications using the document/view model,
//  this is automatically done for you by the framework.

void CClostDlg::OnPaint() 
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, (WPARAM) dc.GetSafeHdc(), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

// The system calls this to obtain the cursor to display while the user drags
//  the minimized window.
HCURSOR CClostDlg::OnQueryDragIcon()
{
	return (HCURSOR) m_hIcon;
}

void CClostDlg::OnActualizar() 
{

	int nDisp;
	BYTE *disp;

	if ( LIBRT_ListarDispositivos(&nDisp, &disp) != 0 ) {
		MessageBox("Imposible determinar la existencia de clauers en el sistema", "CLOST :: ERROR", MB_OK | MB_ICONERROR);
		return;
	}

	if ( nDisp == 0 ) {
		MessageBox("No hay clauers insertados en el sistema", "CLOST :: ERROR", MB_OK | MB_ICONWARNING);
		return;
	} else if ( nDisp > 1 ) {
		free(disp);
		MessageBox("Hay m�s de un clauer insertado en el sistema\r\nPor favor, deje s�lo el que desee utilizar", "CLOST :: ERROR", MB_OK | MB_ICONWARNING);
		return;
	}

	CPropietarioDlg dlgProp;

	dlgProp.DoModal();


}






void CClostDlg::OnSalir() 
{
	EndDialog(0);	
}

void CClostDlg::OnClose() 
{
	
	if ( hMAPIDll ) {
		FreeLibrary(hMAPIDll);
		hMAPIDll = NULL;
	}

	CDialog::OnClose();
}

void CClostDlg::OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags) 
{
	// TODO: Add your message handler code here and/or call default
	
	CDialog::OnKeyDown(nChar, nRepCnt, nFlags);
}



void CClostDlg::OnConfigurar() 
{
	CConfigDialog configDlg;

	configDlg.DoModal();
}



BOOL CClostDlg::OnDeviceChange( UINT nEventType, DWORD dwData )
{
	static BOOL on = FALSE;
	int nDisp;
	BYTE *disp = NULL;

	if ( on )
		return TRUE;

	if ( nEventType == DBT_DEVICEARRIVAL ) {

		on = TRUE;
		if ( LIBRT_ListarDispositivos(&nDisp, &disp) != 0 ) {
			on = FALSE;
			return TRUE;
		}

		if ( nDisp == 1 ) {
			CPropietarioDlg dlgProp;
			dlgProp.DoModal();
		}

		if ( nDisp > 0 )
			free(disp);

		on = FALSE;
	}

	return TRUE;
}

void CClostDlg::OnBtnAbout() 
{
	CAboutDlg dlgAbout;
	
	dlgAbout.DoModal();
}
