// ConfigDialog.cpp : implementation file
//

#include "stdafx.h"
#include "clost.h"
#include "ConfigDialog.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


/////////////////////////////////////////////////////////////////////////////
// CConfigDialog dialog

CConfigDialog::CConfigDialog(CWnd* pParent /*=NULL*/)
	: CDialog(CConfigDialog::IDD, pParent)
{
	//{{AFX_DATA_INIT(CConfigDialog)
	m_editLugar = _T("");
	m_editHorario = _T("");
	m_editSMTP = _T("");
	//}}AFX_DATA_INIT
}


void CConfigDialog::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CConfigDialog)
	DDX_Control(pDX, IDCANCEL, m_btnCancel);
	DDX_Control(pDX, IDOK, m_btnOk);
	DDX_Text(pDX, IDC_EDIT_LUGAR, m_editLugar);
	DDX_Text(pDX, IDC_EDIT_HORARIO, m_editHorario);
	DDX_Text(pDX, IDC_EDITSMTPSERVER, m_editSMTP);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CConfigDialog, CDialog)
	//{{AFX_MSG_MAP(CConfigDialog)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CConfigDialog message handlers

void CConfigDialog::OnCancel() 
{
	// TODO: Add extra cleanup here
	
	CDialog::OnCancel();
}

void CConfigDialog::OnOK() 
{
	// TODO: Add extra validation here

	UpdateData(TRUE);
	this->m_editLugar.TrimRight();
	if ( (this->m_editLugar == "Conserjer�a de XXXXXXXXXXXXXXXXXXXX") ||
		 ( this->m_editLugar == "" ) )
	{
		MessageBox("No ha rellenado el campo de lugar de recogida", "CLOST :: ATENCI�N", MB_OK | MB_ICONWARNING);
		return;
	}
	this->m_editHorario.TrimRight();
	if ( ( this->m_editHorario == "De lunes a viernes de XX a XX" ) ||
		 ( this->m_editHorario == "" ) )
	{
		MessageBox("No ha rellenado el campo de horario de recogida", "CLOST :: ATENCI�N", MB_OK | MB_ICONWARNING);
		return;
	}
	this->m_editSMTP.TrimRight();
	if ( this->m_editSMTP == "" )
	{
		MessageBox("No ha rellenado el campo de servidor SMTP", "CLOST :: ATENCI�N", MB_OK | MB_ICONWARNING);
		return;
	}

	AfxGetApp()->WriteProfileString("config", "donde", this->m_editLugar.GetBuffer(0));
	AfxGetApp()->WriteProfileString("config", "horario", this->m_editHorario.GetBuffer(0));
	AfxGetApp()->WriteProfileString("config", "smtpserver", this->m_editSMTP.GetBuffer(0));
	
	CDialog::OnOK();
}

BOOL CConfigDialog::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	/* Ajustamos la posici�n */

	if ( this->GetParent() ) {
		WINDOWPLACEMENT wpParent, wpThis;
		DWORD widthParent, widthThis, heightThis;

		if ( ! this->GetParent()->GetWindowPlacement(&wpParent) ) {
			MessageBox("Imposible crear ventana", "CLOST :: ERROR :: Datos de usuario", MB_OK | MB_ICONERROR);
			EndDialog(1);
		}

		if ( ! this->GetWindowPlacement(&wpThis) ) {
			MessageBox("Imposible crear ventana", "CLOST :: ERROR :: Datos de usuario", MB_OK | MB_ICONERROR);
			EndDialog(1);
		}

		widthParent = wpParent.rcNormalPosition.right - wpParent.rcNormalPosition.left;
		widthThis   = wpThis.rcNormalPosition.right - wpParent.rcNormalPosition.left;
		heightThis  = wpThis.rcNormalPosition.bottom - wpThis.rcNormalPosition.top;

		wpThis.rcNormalPosition.left   = wpParent.rcNormalPosition.left + widthParent;
		wpThis.rcNormalPosition.right  = wpThis.rcNormalPosition.left + widthThis;
		wpThis.rcNormalPosition.top    = wpParent.rcNormalPosition.top;
		wpThis.rcNormalPosition.bottom = wpThis.rcNormalPosition.top + heightThis;

		if ( ! this->SetWindowPlacement(&wpThis) ) {
			MessageBox("Imposible crear ventana", "CLOST :: ERROR :: Datos de usuario", MB_OK | MB_ICONERROR);
			EndDialog(1);
		}
	}

	/* Ponemos valores por defecto */

	CWinApp *app = AfxGetApp();
	CString donde, horario, smtpserver;

	donde   = app->GetProfileString("config", "donde", "-__--_--__----NO ESTABLECIDO----___--___");
	horario = app->GetProfileString("config", "horario", "-__--_--__----NO ESTABLECIDO----___--___");
	smtpserver = app->GetProfileString("config", "smtpserver", "-__--_--__----NO ESTABLECIDO----___--___");

	if (  donde != "-__--_--__----NO ESTABLECIDO----___--___" ) 
		this->m_editLugar = donde;
	else 
		this->m_editLugar = "Conserjer�a de XXXXXXXXXXXXXXXXXXXX";

	if (  horario != "-__--_--__----NO ESTABLECIDO----___--___" ) 
		this->m_editHorario = horario;
	else
		this->m_editHorario = "De lunes a viernes de XX a XX";

	if (  smtpserver != "-__--_--__----NO ESTABLECIDO----___--___" ) 
		this->m_editSMTP = smtpserver;
	else
		this->m_editSMTP = "mail.uji.es";

	UpdateData(FALSE);
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}
