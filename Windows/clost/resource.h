//{{NO_DEPENDENCIES}}
// Microsoft Developer Studio generated include file.
// Used by clost.rc
//
#define IDD_CLOST_DIALOG                102
#define IDR_MAINFRAME                   128
#define IDD_DIALOG_DATOS_USUARIO        131
#define ICON_USUARIO                    132
#define DLG_CONFIGURACION               133
#define ICON_CONFIG                     136
#define IDD_DIALOG_ENVIANDO_MAIL        137
#define ICON_MAIL                       138
#define IDD_ABOUT_DLG                   139
#define BTN_EnviarMail                  1000
#define BTN_ACTUALIZAR                  1001
#define BTN_SALIR                       1002
#define BTN_CONFIGURAR                  1004
#define EDIT_NOMBRE                     1005
#define EDIT_NIF                        1006
#define EDIT_MAIL                       1007
#define IDC_BUTTON_CERRAR               1008
#define IDC_EDIT_LUGAR                  1010
#define IDC_EDIT_HORARIO                1011
#define IDC_VER_MAIL                    1012
#define IDC_EDITSMTPSERVER              1013
#define IDC_LIST_PROPIETARIO            1014
#define ID_BTN_ABOUT                    1015
#define ID_OK                           1016

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        140
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1017
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
