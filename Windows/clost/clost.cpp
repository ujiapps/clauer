// clost.cpp : Defines the class behaviors for the application.
//

#include "stdafx.h"
#include "clost.h"
#include "clostDlg.h"
#include "ConfigDialog.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif



#include <LIBRT/LIBRT.h>



/////////////////////////////////////////////////////////////////////////////
// CClostApp

BEGIN_MESSAGE_MAP(CClostApp, CWinApp)
	//{{AFX_MSG_MAP(CClostApp)
		// NOTE - the ClassWizard will add and remove mapping macros here.
		//    DO NOT EDIT what you see in these blocks of generated code!
	//}}AFX_MSG
	ON_COMMAND(ID_HELP, CWinApp::OnHelp)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CClostApp construction

CClostApp::CClostApp()
{
	// TODO: add construction code here,
	// Place all significant initialization in InitInstance
}

/////////////////////////////////////////////////////////////////////////////
// The one and only CClostApp object

CClostApp theApp;

/////////////////////////////////////////////////////////////////////////////
// CClostApp initialization

BOOL CClostApp::InitInstance()
{
	// Standard initialization
	// If you are not using these features and wish to reduce the size
	//  of your final executable, you should remove from the following
	//  the specific initialization routines you do not need.

#ifdef _AFXDLL
	Enable3dControls();			// Call this when using MFC in a shared DLL
#else
	Enable3dControlsStatic();	// Call this when linking to MFC statically
#endif

	LIBRT_Ini();

	this->SetRegistryKey("Universitat Jaume I");

	if ( this->GetProfileString("config", "donde", "-_--_-NO ESTABLECIDO-_--_-") == "-_--_-NO ESTABLECIDO-_--_-" ) {
		CConfigDialog dlgConfig;
		//m_pMainWnd = &dlgConfig;
		switch ( dlgConfig.DoModal() ) {
		case IDCANCEL:
			LIBRT_Fin();
			return FALSE;
		}
	}

	if ( this->GetProfileString("config", "horario", "-_--_-NO ESTABLECIDO-_--_-") == "-_--_-NO ESTABLECIDO-_--_-" ) {
		CConfigDialog dlgConfig;
		//m_pMainWnd = &dlgConfig;
		switch ( dlgConfig.DoModal() ) {
		case IDCANCEL:
			LIBRT_Fin();
			return FALSE;
		}
	}

	if ( this->GetProfileString("config", "smtpserver", "-_--_-NO ESTABLECIDO-_--_-") == "-_--_-NO ESTABLECIDO-_--_-" ) {
		CConfigDialog dlgConfig;
		//m_pMainWnd = &dlgConfig;
		switch ( dlgConfig.DoModal() ) {
		case IDCANCEL:
			LIBRT_Fin();
			return FALSE;
		}
	}



	CClostDlg dlg;
	m_pMainWnd = &dlg;
	int nResponse = dlg.DoModal();
	if (nResponse == IDOK)
	{
		// TODO: Place code here to handle when the dialog is
		//  dismissed with OK
	}
	else if (nResponse == IDCANCEL)
	{
		// TODO: Place code here to handle when the dialog is
		//  dismissed with Cancel
	}

	LIBRT_Fin();

	// Since the dialog has been closed, return FALSE so that we exit the
	//  application, rather than start the application's message pump.
	return FALSE;
}


