; CLW file contains information for the MFC ClassWizard

[General Info]
Version=1
LastClass=CAboutDlg
LastTemplate=CDialog
NewFileInclude1=#include "stdafx.h"
NewFileInclude2=#include "clost.h"

ClassCount=6
Class1=CClostApp
Class2=CClostDlg

ResourceCount=6
Resource2=IDD_DIALOG_DATOS_USUARIO
Resource1=IDR_MAINFRAME
Class3=CPropietarioDlg
Resource3=IDD_CLOST_DIALOG
Class4=CConfigDialog
Resource4=DLG_CONFIGURACION
Class5=CMailDialog
Resource5=IDD_DIALOG_ENVIANDO_MAIL
Class6=CAboutDlg
Resource6=IDD_ABOUT_DLG

[CLS:CClostApp]
Type=0
HeaderFile=clost.h
ImplementationFile=clost.cpp
Filter=N

[CLS:CClostDlg]
Type=0
HeaderFile=clostDlg.h
ImplementationFile=clostDlg.cpp
Filter=D
BaseClass=CDialog
VirtualFilter=dWC
LastObject=ID_BTN_ABOUT



[DLG:IDD_CLOST_DIALOG]
Type=1
Class=CClostDlg
ControlCount=5
Control1=BTN_ACTUALIZAR,button,1342242817
Control2=BTN_CONFIGURAR,button,1342242816
Control3=BTN_SALIR,button,1342242816
Control4=IDC_STATIC,static,1342177283
Control5=ID_BTN_ABOUT,button,1342242816

[DLG:IDD_DIALOG_DATOS_USUARIO]
Type=1
Class=CPropietarioDlg
ControlCount=7
Control1=BTN_EnviarMail,button,1342242817
Control2=IDC_BUTTON_CERRAR,button,1342242816
Control3=IDC_STATIC,static,1342177283
Control4=IDC_STATIC,static,1342181383
Control5=IDC_STATIC,static,1342308354
Control6=IDC_LIST_PROPIETARIO,SysListView32,1350631461
Control7=IDC_STATIC,static,1342181383

[CLS:CPropietarioDlg]
Type=0
HeaderFile=PropietarioDlg.h
ImplementationFile=PropietarioDlg.cpp
BaseClass=CDialog
Filter=D
VirtualFilter=dWC
LastObject=BTN_EnviarMail

[DLG:DLG_CONFIGURACION]
Type=1
Class=CConfigDialog
ControlCount=15
Control1=IDC_EDIT_LUGAR,edit,1350631552
Control2=IDC_EDIT_HORARIO,edit,1350631552
Control3=IDC_EDITSMTPSERVER,edit,1350631552
Control4=IDOK,button,1342242817
Control5=IDCANCEL,button,1342242816
Control6=IDC_VER_MAIL,button,1476460544
Control7=IDC_STATIC,static,1342181383
Control8=IDC_STATIC,static,1342177283
Control9=IDC_STATIC,static,1342308354
Control10=IDC_STATIC,button,1342177287
Control11=IDC_STATIC,static,1342308352
Control12=IDC_STATIC,static,1342308352
Control13=IDC_STATIC,button,1342177287
Control14=IDC_STATIC,static,1342308352
Control15=IDC_STATIC,static,1342181383

[CLS:CConfigDialog]
Type=0
HeaderFile=ConfigDialog.h
ImplementationFile=ConfigDialog.cpp
BaseClass=CDialog
Filter=D
LastObject=CConfigDialog
VirtualFilter=dWC

[DLG:IDD_DIALOG_ENVIANDO_MAIL]
Type=1
Class=CMailDialog
ControlCount=2
Control1=IDC_STATIC,static,1342308353
Control2=IDC_STATIC,static,1342177283

[CLS:CMailDialog]
Type=0
HeaderFile=MailDialog.h
ImplementationFile=MailDialog.cpp
BaseClass=CDialog
Filter=D
LastObject=CMailDialog
VirtualFilter=dWC

[DLG:IDD_ABOUT_DLG]
Type=1
Class=CAboutDlg
ControlCount=5
Control1=IDC_STATIC,static,1342308352
Control2=IDC_STATIC,static,1342308352
Control3=IDC_STATIC,static,1342177283
Control4=ID_OK,button,1342242817
Control5=IDC_STATIC,static,1342308352

[CLS:CAboutDlg]
Type=0
HeaderFile=AboutDlg.h
ImplementationFile=AboutDlg.cpp
BaseClass=CDialog
Filter=D
LastObject=ID_OK
VirtualFilter=dWC

