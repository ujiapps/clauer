#if !defined(AFX_PROPIETARIODLG_H__FE93A70B_6283_45A0_AC4E_91C0CBB54BFC__INCLUDED_)
#define AFX_PROPIETARIODLG_H__FE93A70B_6283_45A0_AC4E_91C0CBB54BFC__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// PropietarioDlg.h : header file
//
#include <windows.h>
#include <wincrypt.h>

#include <map>

using namespace std;

/////////////////////////////////////////////////////////////////////////////
// CPropietarioDlg dialog

class CPropietarioDlg : public CDialog
{
// Construction
public:
	CPropietarioDlg(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CPropietarioDlg)
	enum { IDD = IDD_DIALOG_DATOS_USUARIO };
	CListCtrl	m_lstPropietario;
	CButton	m_btnCerrar;
	CButton	m_btnMail;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CPropietarioDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	HCERTSTORE hStore;	
	PCCERT_CONTEXT pCertContext;

	DWORD nCerts;
	DWORD certActual;

	HMODULE hMAPIDll;

	map<CString, CString> emails;	// direcciones de correo de los certificados
								    // presentes en el stick

	BOOL mailEnviado;

	// Generated message map functions
	//{{AFX_MSG(CPropietarioDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnButtonCerrar();
	afx_msg void OnBtnSiguiente();
	afx_msg void OnEnviarMail();
	afx_msg void OnClose();
	afx_msg BOOL OnDeviceChange( UINT nEventType, DWORD dwData );
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()

};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_PROPIETARIODLG_H__FE93A70B_6283_45A0_AC4E_91C0CBB54BFC__INCLUDED_)
