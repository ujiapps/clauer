
#pragma warning(disable:4786)
#pragma warning(disable:4996)

#include <windows.h>

#include "iu.hpp"
#include "resource.h"
#include "CSPIDIOMA.h"

#include <stdio.h>

#ifdef _DEBUG
#include <oolog/oolog.h>
#endif






static int gWhat = IU_GLOBAL_PIN; /* Indica si pin global o password de llave */
static PIN gPin = NULL;
extern HINSTANCE g_hModule;

/* Estas variables son utilizadas por IU_PedirPIN_Ex() para
 * elegir las etiquetas que deben mostrar
 */

static DWORD gIdDesc = CSP_IDIOMA_DLGPASSWORD_DESCRIPCION;
static DWORD gIdPIN  = CSP_IDIOMA_DLGPASSWORD_PIN;

/* Variables globales relacionadas con la ventana de nueva password
 */

static PIN gPassword = NULL;
static PIN gConfirmacion = NULL;


static HWND hWndForeground = NULL;



BOOL IU_CrearVentana (HINSTANCE hDll, HWND &hVentana)
{

    WNDCLASS wc;
	WNDCLASSEX info;



	if (!GetClassInfoEx(hDll, "clauerGUI",&info)) {
		wc.style         = CS_HREDRAW | CS_VREDRAW;
		wc.lpfnWndProc   = DefWindowProc;
		wc.hInstance     = hDll;
		wc.lpszClassName = "clauerGUI";
		wc.lpszMenuName  = NULL;
		wc.hbrBackground = (HBRUSH)( COLOR_WINDOW + 1 );
		wc.hIcon         = LoadIcon( hDll, MAKEINTRESOURCE( IDI_UJI ) );
		wc.hCursor       = LoadCursor( NULL, IDC_ARROW );
		wc.cbClsExtra    = wc.cbWndExtra = 0;
	
		if ( !RegisterClassA(&wc) ) {
			return FALSE;
		}
	}

	hVentana = CreateWindowEx(WS_EX_APPWINDOW | WS_EX_TOPMOST,
                              "clauerGUI",
                              "Clauer User Interface",
                              WS_MINIMIZEBOX |
                              WS_SIZEBOX |
                              WS_CAPTION |
                              WS_MAXIMIZEBOX |
                              WS_POPUP |
                              WS_SYSMENU,
                              0, 0,
                              640, 480,
                              NULL, NULL, hDll, NULL );

	if ( !hVentana ) {
		return FALSE;
	}




	return TRUE;

}







BOOL WINAPI PINDlgProc (HWND hDlg, UINT msg, WPARAM wParam, LPARAM lParam)
{

	HWND hDesktop, hWndAux;
	RECT dimDesktop, dimDialog;

	switch (msg) {
        
	case WM_INITDIALOG:

		hWndForeground = GetForegroundWindow();

		hDesktop = GetDesktopWindow();
		GetWindowRect(hDesktop,&dimDesktop);
        GetWindowRect(hDlg,&dimDialog);
        SetWindowPos(hDlg,
			HWND_TOP,
			((dimDesktop.right-dimDesktop.left)-(dimDialog.right-dimDialog.left))/2,
			((dimDesktop.bottom-dimDesktop.top)-(dimDialog.bottom-dimDialog.top))/2,
			0,
			0,
			SWP_NOSIZE);

		/* Traducimos las etiquetas */

		if ( CSP_IDIOMA_Cargado() ) {
			SetWindowText(hDlg, CSP_IDIOMA_Get(CSP_IDIOMA_DLGPASSWORD_CAPTION));

			hWndAux = GetDlgItem(hDlg, IDC_DESCRIPCION);
			SetWindowText(hWndAux, CSP_IDIOMA_Get(CSP_IDIOMA_DLGPASSWORD_DESCRIPCION));
		
			hWndAux = GetDlgItem(hDlg, IDOK);
			SetWindowText(hWndAux, CSP_IDIOMA_Get(CSP_IDIOMA_ACEPTAR));

			hWndAux = GetDlgItem(hDlg, IDCANCEL);
			SetWindowText(hWndAux, CSP_IDIOMA_Get(CSP_IDIOMA_CANCELAR));

			hWndAux = GetDlgItem(hDlg, IDC_PIN);
			SetWindowText(hWndAux, CSP_IDIOMA_Get(CSP_IDIOMA_DLGPASSWORD_PIN));
		}

		return TRUE ;

	case WM_CLOSE:

		if ( hWndForeground )
			SetFocus(hWndForeground);

		return FALSE;

	case WM_COMMAND:

		switch (LOWORD(wParam)) {
		case IDOK:

			/* Sacamos el pin
			 */

			DWORD tamPIN;

			tamPIN = GetWindowTextLength(GetDlgItem(hDlg,IDC_EDITPIN));

			gPin = new char[MAX_PIN+1];
            if ( ! gPin ) 
                EndDialog(hDlg,-1);
			memset(gPin, 0, MAX_PIN+1);

		
            if ( ! GetDlgItemText(hDlg,IDC_EDITPIN,gPin,MAX_PIN) ) {
				SecureZeroMemory(gPin, MAX_PIN+1);
				delete [] gPin;
				gPin = NULL;
				EndDialog(hDlg,-1);
			}

			EndDialog(hDlg, 0);

            return TRUE;

		case IDCANCEL:
			EndDialog(hDlg,1);
			return TRUE;

		case IDACERCADE:
			ShellExecute(GetForegroundWindow(), "open", "http://clauer.uji.es/", NULL, NULL, SW_SHOWDEFAULT);
			return TRUE;
		}

	}

	return FALSE;
}








int IU_PedirPIN (HINSTANCE hDll, HWND padre, BYTE *disp[MAX_DEVICES], int nDIsp, int &dispSel, int what, PIN &pin)
{

	int ret;
	DWORD tamPin = 0;



	pin = NULL;

	if ( !IsWindow(padre) ) {
		ret = -1;
		goto finIU_PedirPIN;
	}

	ret = DialogBox(hDll,MAKEINTRESOURCE(IDD_PASSWORD), padre, PINDlgProc);
	
	switch (ret) {
	case -1:
		goto finIU_PedirPIN;

	case 0:
		/* Pulsa Aceptar */

		tamPin = strlen(gPin);
		pin = new char[tamPin+1];
		if ( !pin ) {
			ret = -1;
			goto finIU_PedirPIN;
		}
	
		strcpy(pin, gPin);

		break;

	}


finIU_PedirPIN:

	if ( gPin ) {
		if ( tamPin > 0 ) 
			SecureZeroMemory(gPin, tamPin+1);

		delete [] gPin;
		gPin = NULL;
	}

	if ( ret == -1 ) {
		if ( pin ) {
			SecureZeroMemory(pin, tamPin+1);
			delete [] pin;
			pin = NULL;
		}
	}




	return ret;

}




BOOL WINAPI PINDlgProc_Ex (HWND hDlg, UINT msg, WPARAM wParam, LPARAM lParam)
{

	HWND hDesktop, hWndAux;
	RECT dimDesktop, dimDialog;

	switch (msg) {
        
	case WM_INITDIALOG:

		hWndForeground = GetForegroundWindow();

		hDesktop = GetDesktopWindow();
		GetWindowRect(hDesktop,&dimDesktop);
        GetWindowRect(hDlg,&dimDialog);
        SetWindowPos(hDlg,
			HWND_TOP,
			((dimDesktop.right-dimDesktop.left)-(dimDialog.right-dimDialog.left))/2,
			((dimDesktop.bottom-dimDesktop.top)-(dimDialog.bottom-dimDialog.top))/2,
			0,
			0,
			SWP_NOSIZE);

		/* Traducimos las etiquetas */

		if ( CSP_IDIOMA_Cargado() ) {
			SetWindowText(hDlg, CSP_IDIOMA_Get(CSP_IDIOMA_DLGPASSWORD_CAPTION));

			hWndAux = GetDlgItem(hDlg, IDC_DESCRIPCION);
			SetWindowText(hWndAux, CSP_IDIOMA_Get(gIdDesc));
		
			hWndAux = GetDlgItem(hDlg, IDOK);
			SetWindowText(hWndAux, CSP_IDIOMA_Get(CSP_IDIOMA_ACEPTAR));

			hWndAux = GetDlgItem(hDlg, IDCANCEL);
			SetWindowText(hWndAux, CSP_IDIOMA_Get(CSP_IDIOMA_CANCELAR));

			hWndAux = GetDlgItem(hDlg, IDC_PIN);
			SetWindowText(hWndAux, CSP_IDIOMA_Get(gIdPIN));
		}

		return TRUE ;

	case WM_CLOSE:

		if ( hWndForeground )
			SetFocus(hWndForeground);

		return FALSE;

	case WM_COMMAND:

		switch (LOWORD(wParam)) {
		case IDOK:

			/* Sacamos el pin
			 */

			DWORD tamPIN;

			tamPIN = GetWindowTextLength(GetDlgItem(hDlg,IDC_EDITPIN));
			gPin = new char[MAX_PIN+1];
            if ( !gPin ) 
                EndDialog(hDlg,-1);
			
			memset(gPin, 0, MAX_PIN+1);

			
            if ( ! GetDlgItemText(hDlg,IDC_EDITPIN,gPin,MAX_PIN) ) {
				SecureZeroMemory(gPin, MAX_PIN+1);
				delete [] gPin;
				gPin = NULL;
				EndDialog(hDlg,-1);
			}

			EndDialog(hDlg, 0);

            return TRUE;

		case IDCANCEL:
			EndDialog(hDlg,1);
			return TRUE;

		case IDACERCADE:
			ShellExecute(GetForegroundWindow(), "open", "http://clauer.uji.es/", NULL, NULL, SW_SHOWDEFAULT);
			return TRUE;
		}

	}

	return FALSE;
}




int IU_PedirPIN_Ex (HINSTANCE hDll, HWND padre, DWORD dwIdDesc, DWORD idPIN, PIN &pin)
{
	int ret;
	DWORD tamPin = 0;



	pin = NULL;

	if ( ! IsWindow(padre) ) {
		ret = -1;
		goto finIU_PedirPIN;
	}

	gIdPIN  = idPIN;
	gIdDesc = dwIdDesc;

	ret = DialogBox(hDll,MAKEINTRESOURCE(IDD_PASSWORD), padre, PINDlgProc_Ex);
	
	switch (ret) {
	case -1:
		goto finIU_PedirPIN;

	case 0:
		/* Pulsa Aceptar */

		tamPin = strlen(gPin);
		pin = new char[tamPin+1];
		if ( !pin ) {
			ret = -1;
			goto finIU_PedirPIN;
		}
	
		strcpy(pin, gPin);

		break;

	}


finIU_PedirPIN:

	if ( gPin ) {
		if ( tamPin > 0 ) 
			SecureZeroMemory(gPin, tamPin+1);

		delete [] gPin;
		gPin = NULL;
	}

	if ( ret == -1 ) {
		if ( pin ) {
			SecureZeroMemory(pin, tamPin+1);
			delete [] pin;
			pin = NULL;
		}
	}



	return ret;
}




BOOL WINAPI NuevaPasswordDlgProc_Ex (HWND hDlg, UINT msg, WPARAM wParam, LPARAM lParam)
{
	HWND hDesktop, hWndAux;
	RECT dimDesktop, dimDialog;

	switch (msg) {
        
	case WM_INITDIALOG:

		hWndForeground = GetForegroundWindow();

		hDesktop = GetDesktopWindow();
		GetWindowRect(hDesktop,&dimDesktop);
        GetWindowRect(hDlg,&dimDialog);
        SetWindowPos(hDlg,
			HWND_TOP,
			((dimDesktop.right-dimDesktop.left)-(dimDialog.right-dimDialog.left))/2,
			((dimDesktop.bottom-dimDesktop.top)-(dimDialog.bottom-dimDialog.top))/2,
			0,
			0,
			SWP_NOSIZE);

		/* Traducimos las etiquetas */

		if ( CSP_IDIOMA_Cargado() ) {
			SetWindowText(hDlg, CSP_IDIOMA_Get(CSP_IDIOMA_NUEVA_PASSWORD_CAPTION));

			hWndAux = GetDlgItem(hDlg, IDC_NUEVA_DESCRIPCION);
			SetWindowText(hWndAux, CSP_IDIOMA_Get(CSP_IDIOMA_NUEVA_PASSWORD_DESCRIPCION));

			hWndAux = GetDlgItem(hDlg, IDOK);
			SetWindowText(hWndAux, CSP_IDIOMA_Get(CSP_IDIOMA_ACEPTAR));

			hWndAux = GetDlgItem(hDlg, IDCANCEL);
			SetWindowText(hWndAux, CSP_IDIOMA_Get(CSP_IDIOMA_CANCELAR));
		
			hWndAux = GetDlgItem(hDlg, IDC_NUEVA_CONTRASENYA);
			SetWindowText(hWndAux, CSP_IDIOMA_Get(CSP_IDIOMA_NUEVA_PASSWORD_CONTRASENYA));

			hWndAux = GetDlgItem(hDlg, IDC_NUEVA_CONFIRMACION);
			SetWindowText(hWndAux, CSP_IDIOMA_Get(CSP_IDIOMA_NUEVA_PASSWORD_CONFIRMACION));
		}

		return TRUE ;

	case WM_CLOSE:

		if ( hWndForeground )
			SetFocus(hWndForeground);

		return FALSE;

	case WM_COMMAND:

		switch (LOWORD(wParam)) {
		case IDOK:

			/* Sacamos el pin
			 */

			DWORD tamContrasenya, tamConfirmacion;

			tamContrasenya = GetWindowTextLength(GetDlgItem(hDlg,IDC_CONTRASENYA));
			tamConfirmacion = GetWindowTextLength(GetDlgItem(hDlg, IDC_CONFIRMACION));
		
			memset(gPassword, 0, MAX_PIN+1);
			memset(gConfirmacion, 0, MAX_PIN+1);
		
            if ( ! GetDlgItemText(hDlg, IDC_CONTRASENYA, gPassword, MAX_PIN) ) {
				SecureZeroMemory(gPassword, MAX_PIN+1);
				EndDialog(hDlg,-1);
				return TRUE;
			}

            if ( ! GetDlgItemText(hDlg, IDC_CONFIRMACION, gConfirmacion, MAX_PIN) ) {
				SecureZeroMemory(gConfirmacion, MAX_PIN+1);
				EndDialog(hDlg,-1);
				return TRUE;
			}


			if ( strncmp(gPassword, gConfirmacion, MAX_PIN) == 0 ) {
				EndDialog(hDlg, 0);
				return TRUE;
			} else {
				if ( CSP_IDIOMA_Cargado() )
					SetDlgItemText(hDlg, IDC_NUEVA_DESCRIPCION, CSP_IDIOMA_Get(CSP_IDIOMA_NUEVA_PASSWORD_CONCORDAR));
				return TRUE;
			}
            
			break;

		case IDCANCEL:
			EndDialog(hDlg,1);
			return TRUE;

		case IDACERCADE:
			ShellExecute(GetForegroundWindow(), "open", "http://clauer.uji.es/", NULL, NULL, SW_SHOWDEFAULT);
			return TRUE;
		}

	}

	return FALSE;
}



/* Muestra la ventana de petici�n de nueva password
 */

int IU_NuevaPassword ( HINSTANCE hDll, HWND padre, DWORD dwIdDesc, DWORD dwIdPassword, DWORD dwIdConfirmacion, char pwd[MAX_PIN+1])
{
	int ret;
	DWORD tamPin = 0;

	if ( ! IsWindow(padre) ) {
		ret = -1;
		goto finIU_NuevaPassword;
	}

	gPassword = new char [MAX_PIN + 1];
	if ( ! gPassword ) {
		ret = -1;
		goto finIU_NuevaPassword;
	}

	gConfirmacion = new char [MAX_PIN + 1];
	if ( ! gConfirmacion ) {
		ret = -1;
		goto finIU_NuevaPassword;
	}

	ret = DialogBox(hDll,MAKEINTRESOURCE(IDD_NUEVA_PASSWORD), padre, NuevaPasswordDlgProc_Ex);
	
	switch (ret) {
	case -1:
		goto finIU_NuevaPassword;

	case 0:
		/* Pulsa Aceptar */
		
		memset(pwd, 0, MAX_PIN+1);
		strncpy(pwd, gPassword, MAX_PIN);

		break;
	}

finIU_NuevaPassword:

	if ( gPassword ) {
		SecureZeroMemory(gPassword, MAX_PIN+1);
		delete [] gPassword;
		gPassword = NULL;
	}

	if ( gConfirmacion ) {
		SecureZeroMemory(gConfirmacion, MAX_PIN+1);
		delete [] gConfirmacion;
		gConfirmacion = NULL;
	}

	return ret;
}
