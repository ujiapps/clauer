#ifndef __IU_HPP__
#define __IU_HPP__

#include <windows.h>

#include <LIBRT/LIBRT.h>


#define MAX_PIN	256

typedef char * PIN;

#define IU_GLOBAL_PIN	0
#define IU_KEY_PWD		1

/*
 * Funciones de ventana
 */

BOOL IU_CrearVentana (HINSTANCE hDll, HWND &hVentana);

/* -1 ERROR
 * 0 Aceptar
 * 1 Cancelar
 */

int IU_PedirPIN      (HINSTANCE hDll, HWND padre, BYTE *disp[MAX_DEVICES], int nDIsp, int &dispSel, int what, PIN &pin);
int IU_PedirPIN_Ex   (HINSTANCE hDll, HWND padre, DWORD dwIdDesc, DWORD idPIN, PIN &pin);
int IU_NuevaPassword ( HINSTANCE hDll, HWND padre, DWORD dwIdDesc, DWORD dwIdPassword, DWORD dwIdConfirmacion, char pwd[MAX_PIN]);

#endif
