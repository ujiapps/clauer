#ifndef __CSP_UTILS_H__
#define __CSP_UTILS_H__

#include <windows.h>

#include <libRT/LIBRT.h>
#include "clui_csp/clui.h"

#pragma warning(disable:4786)

#include <iostream>
#include <map>

using namespace std;

#define CSPUTILS_SAVE_KEYS		TRUE
#define CSPUTILS_NO_SAVE_KEYS	FALSE

#define LOG_FILE		"C:\\ClauerCSP.log"

/* Informaci�n almacenada en la cach� de passwords
 */

struct PASS_CACHE_INFO {
	char *pass;		// the password
	char id[20];	// the device id
	time_t ts;		// time stamp. Indicates the last time the password was requested
};

/* Los tres handles que manejamos
 *     . HUSBCRYPTKEY para las llaves
 *     . HUSBCRYPTPROV es el contexto general a un container determinado
 *     . HUSBCRYPTHASH objetos hash
 */

typedef struct HUSBCRYPTKEY HUSBCRYPTKEY;
typedef struct HUSBCRYPTPROV HUSBCRYPTPROV;
typedef struct HUSBCRYPTHASH HUSBCRYPTHASH;

/* Estructuras relacionadas con el handle a las llaves
 */

enum tipoLlave { LLAVEPUBLICA, PARLLAVES, LLAVESESION };

struct HUSBCRYPTKEY {

	HCRYPTKEY *hKey;		// El handle a la llave en el CSP subyacente

	//long hPrivada;			// no se usa
	//long hPublica;			// no se usa

	DWORD algoritmo;		// El algoritmo al que pertenece esta llave

	enum tipoLlave tipo;	// El tipo de llave

	HUSBCRYPTPROV *hProv;	// El handle al container al que pertenece la llave. S�lo
							// tiene sentido cuando la llave es de tipo PARLLAVES. Este
							// campo es necesario en la exportaci�n de llaves
};


/* Handle a los objetos hash
 */

struct HUSBCRYPTHASH {
	HCRYPTHASH *hHash;    // Handle al objeto hash en el CSP Subyacente
};



/* Estructura utilizada en enumeraciones de containers. 
 * Para la funci�n CryptGetProvParam() y la estructura HUSBCRYPTPROV
 */

typedef struct {
	int pos;							// El n�mero de �tem que tengo que enumerar
	int tamVector;						// El tama�o de los key containers
	DWORD maxItem;						// El mayor tama�o de los nombres
	INFO_KEY_CONTAINER *keyContainers;	// La informaci�n de los key containers
} INFOENUM;


struct HUSBCRYPTPROV {

	HCRYPTPROV *hpProv;		// Handle al container subyacente que contiene
							// la informaci�n persistente (pares de llaves)

	DWORD dwFlags;			// Flags pasados en el CryptAcquireContext

	LPSTR szContainer;		// El nombre del container adquirido

	long hsContainer;		// El handle al container adquirido en el stick
	long hsExchange;		// El handle a la llave de tipo AT_KEYEXCHANGE en el stick
	long hsSignature;		// El handle a la llave de tipo AT_SIGNATURE en el stick


	HCRYPTKEY hKeySessExchange;
	HCRYPTKEY hKeySessSignature;

	char szDevice[MAX_PATH_LEN];    // El dispositivo seleccionado para utilizaci�n
	char *szPass;					// La contrase�a del dispositivo
	char idDispositivo[20];			// El identificador del dispositivo
	BOOL bIdAvailable;				// Indica si el identificador del dispositivo est� disponible

	/* Punteros a funciones del CLUI
	 */

	BOOL (*lpCLUI_CSP_AskGlobalPassphrase)(HWND, BOOL, BOOL, BOOL, char *, char *, char *);
	BOOL (*lpCLUI_CSP_AskNewPassphrase)(HWND, char *, char *);
	BOOL (*lpCLUI_CSP_AskDoublePassphrase)(HWND, char *, char *);

	/****/

	USBCERTS_HANDLE *hpDispositivo;	// Handle al dispositivo que estamos utilizando

	INFOENUM *pInfoEnum;	// Informaci�n referente a una enumeraci�n de
							// key containers

	map<HUSBCRYPTKEY *, HUSBCRYPTKEY *> sesKeys;		// Llaves de sesi�n
	map<HUSBCRYPTHASH *, HUSBCRYPTHASH *> hashObjects;	// Objetos hash

	HWND hWindow;		// Handle a una ventana para interactuar con el usuario
	BOOL auth;			// Indica si el handle ha sido adquirido en modo autenticado
	BOOL conectado;		// Indica si est� o no conectado

	BOOL nodisp;		// Indica que no hay dispositivo adquirido
};


typedef map<char, SOCKET> SOCKCACHE;
typedef map<char, USBCERTS_HANDLE *> CACHE;


/* Inicializaci�n perezosa del CSP. Esta funci�n se llama al principio de cada
 * funci�n del CSP para realizar aquellas tareas de inicializaci�n que no
 * pueden ir en DllMain (aquellas llamadas al API de windows que no est�n
 * en kernel32.lib
 */

BOOL CSPUTILS_Ini ( void );

/* -1 ERROR
 * 0 Aceptar
 * 1 Cancelar
 */

int CSPUTILS_PedirPIN (HUSBCRYPTPROV *hProv, HINSTANCE hDll);


/* Utilidades varias
 */

BOOL CSPUTILS_OS_Ephemereal (void);

/* Utilidades varias
 */

BOOL CSPUTILS_PrivateKeyBlobToPkcs1PrivateKey ( BYTE *blob, DWORD dwBlobSize, BYTE *pk, DWORD *dwPkSize );
BOOL CSPUTILS_Pkcs1PrivateKeyToPEM            ( BYTE *pk, DWORD dwPkSize, BYTE *pem, DWORD *dwPemSize );


/* Utilidades para la gesti�n de la pool de passwords
 */

BOOL   CSPUTILS_PASSPOOL_Init ( void );
BOOL   CSPUTILS_PASSPOOL_Fin  ( void );
char * CSPUTILS_PASSPOOL_New  ( void );
void   CSPUTILS_PASSPOOL_Free ( char *pwd );

/* 
 * Utilidades para la gesti�n de la cach� de dispositivos
 */

BOOL CSPUTILS_PASSCACHE_Get_Password ( HUSBCRYPTPROV *hUsbProv );
BOOL CSPUTILS_PASSCACHE_Set_Password ( HUSBCRYPTPROV *hUsbProv, char *szPassword );

/* 
 * Utilidades para la gesti�n de la conexi�n a CLOS
 */

BOOL CSPUTILS_CLOS_Connect    ( HUSBCRYPTPROV *hUsbProv, BOOL auth );
BOOL CSPUTILS_CLOS_Disconnect ( HUSBCRYPTPROV *hUsbProv );

/*
 * Utilidades para la gesti�n del CSP Subyacente
 */

BOOL CSPUTILS_Subyacente_BorrarKeyContainer   ( LPCTSTR csp, LPCTSTR container );
BOOL CSPUTILS_Subyacente_CargarPersistente    ( HUSBCRYPTPROV *hUsbProv );
BOOL CSPUTILS_Subyacente_DescargarPersistente ( HUSBCRYPTPROV *hProv, DWORD Algid, BOOL guardarLlave, BOOL nuevaPassword );
BOOL CSPUTILS_Subyacente_ImportarCipheredBlob ( HUSBCRYPTPROV *hProv, BYTE bloque[TAM_BLOQUE], DWORD algId, HCRYPTKEY *hKey );

/*
 * Utilidades para la gesti�n de los key containers en el stick
 */

BOOL CSPUTILS_KeyContainer_Existe        (HUSBCRYPTPROV *hProv, LPTSTR nombreKeyContainer);
BOOL CSPUTILS_KeyContainer_Insertar      (HUSBCRYPTPROV *hProv, LPTSTR nombreKeyContainer);
BOOL CSPUTILS_KeyContainer_Listar        (HUSBCRYPTPROV *hProv, INFO_KEY_CONTAINER **infoKeyContainers, int *numContainers);
int  CSPUTILS_KeyContainer_Borrar        (HUSBCRYPTPROV *hProv, LPTSTR nombreKeyContainer);
BOOL CSPUTILS_KeyContainer_BorrarLlave   (HUSBCRYPTPROV *hProv, LPTSTR nombreKeyContainer, DWORD tipo);
long CSPUTILS_KeyContainer_GetHandle     (HUSBCRYPTPROV *hProv, LPTSTR nombreKeyContainer);
BOOL CSPUTILS_KeyContainer_GetKeyHandles (HUSBCRYPTPROV *hProv, long *hExchange, long *hSignature, BOOL bFromConnect = FALSE );
BOOL CSPUTILS_KeyContainer_SetExportable (HUSBCRYPTPROV *hProv, HUSBCRYPTKEY *hKey, BOOL exportable);
BOOL CSPUTILS_KeyContainer_GetExportable (HUSBCRYPTPROV *hProv, HUSBCRYPTKEY *hKey, BOOL &exportable);
BOOL CSPUTILS_KeyContainer_KeyExists     (HUSBCRYPTPROV *hProv, DWORD dwAlgId, BOOL *bExists);

/*
 * Utilidades para la gesti�n de blobs
 */

BOOL CSPUTILS_Blob_Insertar      (HUSBCRYPTPROV *hProv, BYTE *blobPrivada, DWORD tamBlobPrivada, BYTE *blobPublica, DWORD tamBlobPublica, DWORD tipoLLave, long &hLlavePublica, long &hLlavePrivada);
BOOL CSPUTILS_Blob_CalcularId	 (BYTE *blob, DWORD tamBlob, BYTE id[20]);

/*
 * Utilidades para la gesti�n de handles de los distintos objetos
 */

BOOL CSPUTILS_HANDLE_Nuevo   (HUSBCRYPTPROV **hProv);
void CSPUTILS_HANDLE_Liberar (HUSBCRYPTPROV *hProv);

BOOL CSPUTILS_HANDLE_Nuevo   (HUSBCRYPTKEY **hKey);
void CSPUTILS_HANDLE_Liberar (HUSBCRYPTKEY *hKey);

BOOL CSPUTILS_HANDLE_Nuevo   (HUSBCRYPTHASH **hHash);
void CSPUTILS_HANDLE_Liberar (HUSBCRYPTHASH *hHash);

/* 
 * Callback para la LIBRT
 */

int CSPUTILS_LIBRT_IU_callback (USBCERTS_HANDLE *hDispositivo, BYTE *dispositivos[MAX_DEVICES], int nDispositivos, void *appData, char pin[100], int *dispSel);


#endif

