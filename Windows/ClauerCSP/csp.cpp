// csp.cpp : Defines the initialization routines for the DLL.
//

#undef UNICODE

#pragma warning(disable:4786)
#pragma warning(disable:4996)

#include <windows.h>
#include "cspdk.h"
#include <wincrypt.h>

#include <tchar.h>

#include "csp.h"
#include "CSPUtils.h"
#include "iu.hpp"
#include "privadas.h"
#include "CSPIDIOMA.h"



#include <libRT/LIBRT.h>
#include <log/log.h>


#include <iostream>
#include <map>
#include <string>

// Para usar directamente

using namespace std;

// Variables globales

static const TCHAR l_szProviderName[] = TEXT("UJI Clauer CSP");
static const DWORD l_dwCspType        = PROV_RSA_FULL;
static const DWORD CspVersion         = 0x00000001;
TCHAR g_szUnderlayingCSP[]			  = TEXT("Microsoft Enhanced Cryptographic Provider v1.0");

static USBCERTS_HANDLE *cacheUsbHandle = NULL;

// La cach� de passwords

HANDLE g_hThrCacheEndEvent;		// Se activa cuando el thread termina
HANDLE g_hThrCacheEvent;		// Para indicar al thread que termine
HANDLE g_hMutexPassCache;		// Para coordinar el acceso a la cach� de passwords

CRITICAL_SECTION g_csInitialize;   // Utilizada para la initilizaci�n perezosa
BOOL g_bInitialized;				// Indica si la inicializaci�n perezosa se ha llevado a cabo o no

map<string, PASS_CACHE_INFO *> g_mapPassCache;
extern BOOL g_bThrErr;			// Indica si ha ocurrido alg�n error en el thread o no


HANDLE g_hThrCache = 0;



extern "C" {
extern int g_bClauerLOG;
}


HINSTANCE g_hModule = NULL;
map<HUSBCRYPTPROV *, HUSBCRYPTPROV *> gTblProv;

/* Cach� de pins
 */

map<string, char *> g_mapPinCache;

// Prototipo de funci�n necesaria para la instalaci�n del CSP

static HMODULE
GetInstanceHandle(
    void);


// Punto de entrada a la DLL

BOOL WINAPI DllMain(
  HINSTANCE hinstDLL,  // handle to DLL module
  DWORD fdwReason,     // reason for calling function
  LPVOID lpvReserved   // reserved
)
{

	BOOL bRet = TRUE;

	switch (fdwReason) {

         case DLL_PROCESS_ATTACH:

			DisableThreadLibraryCalls(hinstDLL);
			g_hModule = hinstDLL;

			// Inicializamos los par�metros relacionados con inicializaci�n
			// perezosa

			InitializeCriticalSection(&g_csInitialize);
			g_bInitialized = FALSE;

			
			g_hMutexPassCache   = CreateMutex(NULL, FALSE, NULL);
			if ( ! g_hMutexPassCache ) {
				return FALSE;
			}

			g_hThrCacheEvent    = CreateEvent(NULL, FALSE, FALSE, NULL);
			if ( ! g_hThrCacheEvent ) {
				CloseHandle(g_hMutexPassCache);
				CloseHandle(g_hThrCacheEvent);
				return FALSE;
			}

			g_hThrCacheEndEvent = CreateEvent(NULL, FALSE, FALSE, NULL);
			if ( ! g_hThrCacheEndEvent ) {
				CloseHandle(g_hMutexPassCache);
				CloseHandle(g_hThrCacheEvent);
				CloseHandle(g_hThrCacheEndEvent);
				return FALSE;				
			}

			g_bThrErr = FALSE;


			break;
 
         case DLL_PROCESS_DETACH:

			 if ( g_hThrCacheEvent )
				SetEvent(g_hThrCacheEvent);

			 CSP_IDIOMA_Descargar();
			 CSPUTILS_PASSPOOL_Fin();

			 map<HUSBCRYPTPROV *, HUSBCRYPTPROV *>::iterator i;
			 HUSBCRYPTPROV *usbHandle;
		 
			 for ( i = gTblProv.begin() ; i != gTblProv.end() ; i++) { 

				usbHandle = i->first;

				/* Finalizamos el dispositivo
				 */

				LIBRT_FinalizarDispositivo(usbHandle->hpDispositivo);

				/* Si no estamos en CRYPT_VERIFYCONTEXT, adem�s nos cargamos el container subyacente 
				 */

/*				if ( usbHandle->dwFlags != CRYPT_VERIFYCONTEXT )
					CryptAcquireContext(&hAuxProv, usbHandle->szSubTmpContainer, g_szUnderlayingCSP, PROV_RSA_FULL, CRYPT_DELETEKEYSET);
*/
				/* La propia funci�n se encarga de liberar las llaves de sesi�n y los
				 * hash objects adquiridos durante la vida del handle
				 */

				CSPUTILS_HANDLE_Liberar(usbHandle);

			 }


			 // Esta es la �nica forma que tengo de terminar con el thread
			 TerminateThread(g_hThrCache, 1);
			
			 if ( g_hThrCacheEvent )
				 CloseHandle(g_hThrCacheEvent);
			 if ( g_hThrCacheEndEvent )
				 CloseHandle(g_hThrCacheEndEvent);

            break;
     }

     return bRet;
}



/////////////////////////////////////////////////////////////////////////////
// CSP Exported Functions

/****************************************************************************/

BOOL WINAPI CPAcquireContext(OUT HCRYPTPROV *phpProv, IN LPCSTR szContainer, IN DWORD dwFlags, IN PVTableProvStruc pVTable)
{
	HUSBCRYPTPROV *hUsbProv = NULL;
	BOOL ok = TRUE;
	BYTE *dispositivos[MAX_DEVICES];
	HWND hWindow;
	BOOL dispositivoIniciado = FALSE;
	DWORD lastErr = 0;

	memset(dispositivos, 0, MAX_DEVICES*sizeof(char *));

	if ( ! CSPUTILS_Ini () ) {
		lastErr = NTE_PROVIDER_DLL_FAIL;
		ok = FALSE;
		goto finCPAcquireContext;
	}

	LOG_BeginFunc(1);

	/* Si no se pudo crear el mutex para la cach� de dispositivos devolvemos
	 * error
	 */

	if ( ! g_hMutexPassCache ) {
		lastErr = NTE_PROVIDER_DLL_FAIL;
		ok = FALSE;
		goto finCPAcquireContext;
	}

	/* Si no se pudo crear evento para sincronizar con el thread --> error
	 */

	if ( ! g_hThrCacheEvent ) {
		lastErr = NTE_PROVIDER_DLL_FAIL;
		ok = FALSE;
		goto finCPAcquireContext;
	}

	/* Si el thread marca errr --> error
	 */

	if ( g_bThrErr ) {
		lastErr = NTE_PROVIDER_DLL_FAIL;
		ok = FALSE;
		goto finCPAcquireContext;
	}

	/* Controlamos las combinaciones de flags posibles.
	 *
	 * El CSP implementa CRYPT_MACHINEKEYSET ignorando el flag
	 */

	if ( dwFlags & CRYPT_NEWKEYSET ) {
		
		/* S�lo con CRYPT_SILENT y/o CRYPT_MACHINE_KEYSET	*/
		if ( dwFlags & ~CRYPT_NEWKEYSET & ~CRYPT_MACHINE_KEYSET & ~CRYPT_SILENT ) {
			lastErr = NTE_BAD_FLAGS;
			ok = FALSE;
			goto finCPAcquireContext;
		}
	} else if ( dwFlags & CRYPT_DELETEKEYSET ) {
		/* S�lo con CRYPT_SILENT y/o CRYPT_MACHINE_KEYSET */
		if ( dwFlags & ~CRYPT_DELETEKEYSET & ~CRYPT_MACHINE_KEYSET & ~CRYPT_SILENT ) {
			lastErr = NTE_BAD_FLAGS;
			ok = FALSE;
			goto finCPAcquireContext;
		}
	} else if ( dwFlags & CRYPT_VERIFYCONTEXT ) {
		/* S�lo con CRYPT_SILENT */
		if ( dwFlags & ~CRYPT_SILENT & ~CRYPT_VERIFYCONTEXT ) {
			lastErr = NTE_BAD_FLAGS;
			ok = FALSE;
			goto finCPAcquireContext;
		}
	} else if ( dwFlags & CRYPT_SILENT ) {
		/* S�lo con CRYPT_DELETEKEYSET o CRYPT_NEWEYSET  o CRYPT_MACHINE_KEYSET */
		if ( dwFlags & ~CRYPT_SILENT & ~CRYPT_MACHINE_KEYSET & ~CRYPT_DELETEKEYSET & ~CRYPT_NEWKEYSET) {
			lastErr = NTE_BAD_FLAGS;
			ok = FALSE;
			goto finCPAcquireContext;
		}
	}

	// Creamos un nuevo handle

	if ( !CSPUTILS_HANDLE_Nuevo(&hUsbProv) ) {
		lastErr = NTE_NO_MEMORY;
		ok = FALSE;
		goto finCPAcquireContext;
	}

	hUsbProv->dwFlags = dwFlags;


	/* CRYPT_VERIFYCONTEXT no requiere el uso de ventanas ya que no existe acceso
	 * al dispositivo. Lo mismo pasar�a con CRYPT_SILENT.
	 */

	if ( ! (dwFlags & CRYPT_VERIFYCONTEXT) || !(dwFlags & CRYPT_SILENT) ) {

		/* Cualquier otro flag requiere de ventanuca
		 */
		
		if ( pVTable->FuncReturnhWnd ) {

			pVTable->FuncReturnhWnd(&hWindow);

		   /* Comprobamos que el handle devuelto es de una ventana previamente 
			* creada
			*/

			if ( IsWindow(hWindow) ) 
				hUsbProv->hWindow = hWindow;
			else {
				hUsbProv->hWindow = GetForegroundWindow();
			}
		}
	}

	// Acciones espec�ficas a cada flag
	//
	//     CRYPT_VERIFYCONTEXT se trata de manera especial

	if ( dwFlags & CRYPT_VERIFYCONTEXT ) {

		/* CRYPT_VERIFYCONTEXT.
		 *
		 * Con este flag todos los objetos que tratemos ser�n temporales.
		 * No hay acceso al clauer
		 */

		if ( szContainer ) {
			lastErr = NTE_BAD_FLAGS;
			ok = FALSE;
			goto finCPAcquireContext;
		}
	
	} else {
		
   	   /* El resto de flags presentan caracter�sticas comunes */

	   /* Si szContainer es NULL y dwFlags != CRYPT_VERIFYCONTEXT, entonces utilizamos
		* el container por defecto. Con CRYPT_VERIFYCONTEXT no es necesario puesto que todos
		* los objetos que se generen o utilicen son temporales.
		*/

		if  ( (szContainer == NULL) || (szContainer[0] == '\0') ) 
			szContainer = "default";

	    // Si el nombre del container es mayor que MAX_PATH entonces error
		
		if ( strlen(szContainer) > MAX_PATH ) {	
			lastErr = NTE_BAD_KEYSET_PARAM;
			ok = FALSE;
			goto finCPAcquireContext;
		}
	
		/* M�s inicializac�ones 
		 */

		hUsbProv->szContainer = new char [::strlen(szContainer)+1];
		if ( !hUsbProv->szContainer ) {
			lastErr = NTE_NO_MEMORY;
			ok = FALSE;
			goto finCPAcquireContext;
		}
		::strcpy(hUsbProv->szContainer, szContainer);

		/* Ignoramos el flag CRYPT_MACHINE_KEYSET. Lo desactivamos para
		 * evitar posibles problemas con el CSP subyacente
		 */

		if ( dwFlags & CRYPT_MACHINE_KEYSET )
			dwFlags = dwFlags & ~CRYPT_MACHINE_KEYSET;

		if ( dwFlags & CRYPT_NEWKEYSET ) {

			if ( ! CSPUTILS_CLOS_Connect(hUsbProv, TRUE) ) {
				ok = FALSE;
				goto finCPAcquireContext;
			}

			/* Comprobamos si el container existe ya o no
			*/

			hUsbProv->hsContainer = CSPUTILS_KeyContainer_GetHandle(hUsbProv, hUsbProv->szContainer);

			if ( hUsbProv->hsContainer != -1 ) {			
				lastErr = NTE_EXISTS;
				ok = FALSE;
				goto finCPAcquireContext;
			} 

			/* Nosotros creamos un container vac�o, es decir, no generamos un nuevo par
			 * de llaves
			 */

			if ( CSPUTILS_KeyContainer_Insertar (hUsbProv, (char *)szContainer) != 0 ) {			
				ok = FALSE;
				goto finCPAcquireContext;
			}

			/* Indicamos el n�mero de bloque en el que se encuentra el container
			 */

			hUsbProv->hsContainer = CSPUTILS_KeyContainer_GetHandle(hUsbProv, hUsbProv->szContainer);

			CSPUTILS_CLOS_Disconnect(hUsbProv);

		} else if ( dwFlags & CRYPT_DELETEKEYSET ) {

			int err;


			if ( ! CSPUTILS_CLOS_Connect(hUsbProv, TRUE) ) {
				ok = FALSE;
				goto finCPAcquireContext;
			}
		
			/* Comprobamos la existencia del key container 
			 */

			err = CSPUTILS_KeyContainer_GetHandle(hUsbProv, (char *)szContainer);
		
			if (  err == -1 ) {
				lastErr = NTE_KEYSET_NOT_DEF;
				ok = FALSE;
				goto finCPAcquireContext;	
			} else if ( err == -2 ) {
				ok = FALSE;
				goto finCPAcquireContext;
			}

			/* Borramos el key container 
			 */

			if ( CSPUTILS_KeyContainer_Borrar (hUsbProv,(char *)szContainer) != 0 ) {
				ok = FALSE;
				goto finCPAcquireContext;
			}


			CSPUTILS_CLOS_Disconnect(hUsbProv);

			ok = TRUE;
			goto finCPAcquireContext;
	
		} else {

			if ( ! CSPUTILS_CLOS_Connect(hUsbProv, FALSE) ) {
				
				ok = FALSE;
				goto finCPAcquireContext;
			}


			/* Comprobamos la existencia del container
			 */


			hUsbProv->hsContainer = CSPUTILS_KeyContainer_GetHandle(hUsbProv, (char *)szContainer);

			LOG_Debug(10, "Container Name: %s", szContainer);
			LOG_Debug(10, "CONTAINER: %ld", hUsbProv->hsContainer);

			CSPUTILS_CLOS_Disconnect(hUsbProv);

			if ( hUsbProv->hsContainer == -1 ) {
				
				lastErr = NTE_BAD_KEYSET;
				ok = FALSE;
				goto finCPAcquireContext;
			} else if ( hUsbProv->hsContainer == -2 ) {
				
				ok = FALSE;
				goto finCPAcquireContext;
			}
		
		}

	} 

	if ( ! CryptAcquireContext(hUsbProv->hpProv, NULL, g_szUnderlayingCSP, PROV_RSA_FULL, CRYPT_VERIFYCONTEXT) ) {
		ok = FALSE;
		goto finCPAcquireContext;
	}

	/* Devolvemos el handle convenientemente inicializado
	 */

    *phpProv = (HCRYPTPROV) hUsbProv; // Replace NULL with your own structure.
	gTblProv[hUsbProv] = hUsbProv;

finCPAcquireContext:
		
	if ( ! ok  ) {
		if ( hUsbProv ) {			
			if ( hUsbProv->conectado ) {
				CSPUTILS_CLOS_Disconnect(hUsbProv);
			}

			CSPUTILS_HANDLE_Liberar(hUsbProv);
			hUsbProv = NULL;
		}

		for ( int i = 0 ; i < MAX_DEVICES ; i++ )
			if ( dispositivos[i] )
				free(dispositivos[i]);
			else
				break;

	}

	LOG_EndFunc(1, ok);
	SetLastError(lastErr);

    return ok;

}

/****************************************************************************/
/* TODO: Revisar esta funci�n
 */

BOOL WINAPI CPReleaseContext(IN HCRYPTPROV hProv, IN DWORD dwFlags)
{
	HUSBCRYPTPROV *usbHandle;
	BOOL ok = TRUE;

	if ( ! CSPUTILS_Ini () )
		return FALSE;

	LOG_BeginFunc(1);
	
	usbHandle = (HUSBCRYPTPROV *) hProv;

	if  (dwFlags != 0 ) {
		ok = FALSE;
		SetLastError(NTE_BAD_FLAGS);
		goto finCPReleaseContext;
	}


	if ( usbHandle->hKeySessExchange )
		CryptDestroyKey(usbHandle->hKeySessExchange);
	
	if ( usbHandle->hKeySessSignature )
		CryptDestroyKey(usbHandle->hKeySessSignature);

	CryptReleaseContext(*(usbHandle->hpProv), 0);
	gTblProv.erase(usbHandle);

	/* La propia funci�n se encarga de liberar las llaves de sesi�n y los
	 * hash objects adquiridos durante la vida del handle
	 */

	CSPUTILS_HANDLE_Liberar(usbHandle);

finCPReleaseContext:

	DWORD lastErr = GetLastError();
	LOG_EndFunc(1, ok);
	SetLastError(lastErr);

    return ok;

}

/****************************************************************************/
/* Si se pretende generar un par de llaves p�blica/privada entonces habr� que
 * interactuar con el stick. La generaci�n de una llave de sesi�n no implica ninguna
 * interacci�n
 */

BOOL WINAPI CPGenKey(IN HCRYPTPROV hpProv, IN ALG_ID Algid, IN DWORD dwFlags, OUT HCRYPTKEY *phKey)
{
	HUSBCRYPTPROV *usbHandle;
	HUSBCRYPTKEY *usbKey = NULL;

	BOOL ok = TRUE;

	if ( ! CSPUTILS_Ini () )
		return FALSE;


	LOG_BeginFunc(1);

	if ( ! hpProv ) {
		SetLastError(NTE_BAD_UID);
		ok = FALSE;
		goto finCPGenKey;
	}

	usbHandle = (HUSBCRYPTPROV *) hpProv;

	// Si el contexto fue adquirido en modo silencioso y me piden generar un par de llaves persistentes
	// entonces error 
	if ( (usbHandle->dwFlags & CRYPT_SILENT) && !(usbHandle->dwFlags & CRYPT_VERIFYCONTEXT) &&
		 ((Algid == AT_KEYEXCHANGE) || (Algid == AT_SIGNATURE) || (Algid == CALG_RSA_SIGN) || (Algid == CALG_RSA_KEYX)) ) {
		ok = FALSE;
		SetLastError(NTE_SILENT_CONTEXT);
		goto finCPGenKey;
	}

	/* Comprobaci�n de errores
	 */

	if ( dwFlags & CRYPT_PREGEN ) {

		/* Este flag es s�lo para CSPs Diffie-Hellman/DSS
		 */

		SetLastError(NTE_BAD_FLAGS);
		ok = FALSE;
		goto finCPGenKey;

	} else if ( ((dwFlags & CRYPT_CREATE_SALT) || (dwFlags & CRYPT_NO_SALT) ) && 
		((Algid == AT_KEYEXCHANGE) || ( Algid == AT_SIGNATURE)) ) {
		/* SALT con pares de llaves no tiene sentido
		 */
	
		SetLastError(NTE_BAD_FLAGS);
		ok = FALSE;
		goto finCPGenKey;

	}

	/* No admito tama�os de llave mayores a 16384 bits
	 */

	switch ( Algid ) {
		case AT_KEYEXCHANGE:
		case AT_SIGNATURE:
		case CALG_RSA_SIGN:
		case CALG_RSA_KEYX:

			if ( (dwFlags >> 16) > 16384 ) {
				SetLastError(NTE_BAD_FLAGS);
				ok = FALSE;
				goto finCPGenKey;
			}

			break;

/*		default:
			SetLastError(NTE_BAD_ALGID);
			ok = FALSE;
			goto finCPGenKey;*/
	}

	/* Instanciamos el handle de llave
	 */

	if ( ! CSPUTILS_HANDLE_Nuevo(&usbKey) ) {
		SetLastError(NTE_FAIL);
		ok = FALSE;
		goto finCPGenKey;
	}

	if ( ( AT_KEYEXCHANGE == Algid ) || ( AT_SIGNATURE == Algid ) )
		usbKey->tipo = PARLLAVES;
	else
		usbKey->tipo = LLAVESESION;

	usbKey->algoritmo = Algid;


	if ( (( AT_KEYEXCHANGE == Algid ) || ( AT_SIGNATURE == Algid )) &&
		 ( usbHandle->dwFlags != CRYPT_VERIFYCONTEXT ) ) {

		/* La generaci�n de un par de llaves p�blica/privada persistentes implica
		 * la utilizaci�n del container persistente. Si el contexto fue adquirido
		 * como CRYPT_VERIFYCONTEXT, entonces todos los objetos generados son 
		 * temporales, siendo innecesaria la interacci�n con el stick. Para este
		 * �ltimo caso empleamos el container que almacena los objetos temporales.
		 */

		DWORD tamKey;
		BOOL bUserProtected;


/*		if ( ! CSPUTILS_Subyacente_CargarPersistente(usbHandle) ) {
			SetLastError(NTE_FAIL);
			ok = FALSE;
			goto finCPGenKey;
		}
*/
		tamKey         = dwFlags & 0xFFFF0000;
		bUserProtected = dwFlags & CRYPT_USER_PROTECTED;
	
		/* Tenemos que generar la llave como exportable. Hay que volcarla despu�s en el stick
		 */

		if ( ! CryptGenKey(*(usbHandle->hpProv), Algid, tamKey | CRYPT_EXPORTABLE, usbKey->hKey) ) {
			CSPUTILS_Subyacente_DescargarPersistente(usbHandle, AT_KEYEXCHANGE, CSPUTILS_NO_SAVE_KEYS, FALSE);
			CSPUTILS_Subyacente_DescargarPersistente(usbHandle, AT_SIGNATURE, CSPUTILS_NO_SAVE_KEYS, FALSE);
			ok = FALSE;
			goto finCPGenKey;
		}

		/* �nicamente guardamos la llave que se nos pide generar en el stick
		 *
		 * TODO: No entiendo muy bien por qu� se ha quedado dos descargar persistentes... analizar
		 */

		if ( ! CSPUTILS_Subyacente_DescargarPersistente(usbHandle, Algid, CSPUTILS_SAVE_KEYS, bUserProtected) ) {
			CryptDestroyKey(*(usbKey->hKey));
			SetLastError(NTE_FAIL);
			ok = FALSE;
			goto finCPGenKey;
		}

		if ( Algid == AT_SIGNATURE ) 
			CSPUTILS_Subyacente_DescargarPersistente(usbHandle, AT_KEYEXCHANGE, CSPUTILS_NO_SAVE_KEYS, FALSE);
		else
			CSPUTILS_Subyacente_DescargarPersistente(usbHandle, AT_SIGNATURE, CSPUTILS_NO_SAVE_KEYS, FALSE);
		
		usbKey->hProv = usbHandle;
		
	    *phKey = (HCRYPTKEY) usbKey;

	} else {

		/* Generamos una llave de sesi�n o bien un par de llaves p�blica/privada
		 * cuando el contexto fue adquirido como CRYPT_VERIFYCONTEXT
		 */

		if ( ! CryptGenKey(*(usbHandle->hpProv), Algid, dwFlags, usbKey->hKey) ) {
			ok = FALSE;
			goto finCPGenKey;
		}

		usbKey->hProv = usbHandle;
		usbHandle->sesKeys[usbKey] = usbKey;
		
		*phKey = (HCRYPTKEY) usbKey;    
	}


finCPGenKey:

	DWORD lastErr = GetLastError();

	if ( FALSE == ok ) {
		if ( usbKey ) 
			CSPUTILS_HANDLE_Liberar(usbKey);
	}


	LOG_EndFunc(1, ok);
	SetLastError(lastErr);

    return ok;

}

/****************************************************************************/
/*
 * Esta funci�n s�lo tiene que ver con llaves de sesi�n, de modo que solamente
 * utilizaremos el container temporal.
 */

BOOL WINAPI CPDeriveKey(IN HCRYPTPROV hpProv, IN ALG_ID Algid, IN HCRYPTHASH hHash, IN DWORD dwFlags, OUT HCRYPTKEY *phKey)
{
	HUSBCRYPTPROV *usbHandle;
	HUSBCRYPTKEY *usbKey = NULL;
	HUSBCRYPTHASH *usbHash;

	DWORD dwLastErr = 0;
	BOOL ok = TRUE;

	if ( ! CSPUTILS_Ini () ) {
		return FALSE;
	}

	LOG_BeginFunc(1);

	if ( ! hpProv ) {
		dwLastErr = NTE_BAD_UID;
		ok = FALSE;
		goto finCPDeriveKey;
	}

	if ( ! hHash ) {
		dwLastErr = NTE_BAD_HASH;
		ok = FALSE;
		goto finCPDeriveKey;
	}

	usbHandle = (HUSBCRYPTPROV *) hpProv;
	usbHash   = (HUSBCRYPTHASH *) hHash;

	if ( usbHash == NULL ) {
		dwLastErr = NTE_BAD_HASH;
		ok = FALSE;
		goto finCPDeriveKey;
	}

	if ( usbHandle == NULL ) {
		dwLastErr = ERROR_INVALID_HANDLE;
		ok = FALSE;
		goto finCPDeriveKey;
	}

	if ( ! CSPUTILS_HANDLE_Nuevo(&usbKey) ) {
		dwLastErr = NTE_FAIL;
		ok = FALSE;
		goto finCPDeriveKey;
	}
	
	if ( ! CryptDeriveKey(*(usbHandle->hpProv), Algid, *(usbHash->hHash), dwFlags, usbKey->hKey) ) {
		dwLastErr = GetLastError();
		ok = FALSE;
		goto finCPDeriveKey;
	}

	usbKey->tipo  = LLAVESESION;
	usbKey->hProv = usbHandle;
	
	usbHandle->sesKeys[usbKey] = usbKey;

    *phKey = (HCRYPTKEY) usbKey;

finCPDeriveKey:

	if ( ok == FALSE ) {
		SetLastError(dwLastErr);
		if ( usbKey )
			CSPUTILS_HANDLE_Liberar(usbKey);
	}

	LOG_EndFunc(1, ok);
    return ok;
}

/****************************************************************************/

BOOL WINAPI CPDestroyKey(IN HCRYPTPROV hpProv, IN HCRYPTKEY hKey)
{
	HUSBCRYPTPROV *usbHandle;
	HUSBCRYPTKEY *usbKey;

	BOOL ok = TRUE;

	if ( ! CSPUTILS_Ini () )
		return FALSE;

	LOG_BeginFunc(1);

	if ( ! hpProv ) {
		SetLastError(NTE_BAD_UID);
		ok = FALSE;
		goto finCPDestroyKey;
	}

	if ( ! hKey ) {
		SetLastError(NTE_BAD_KEY);
		ok = FALSE;
		goto finCPDestroyKey;
	}


	usbHandle = (HUSBCRYPTPROV *) hpProv;
	usbKey    = (HUSBCRYPTKEY *) hKey;

	/* Liberamos simpre llaves de sesi�n y llaves p�blicas porque est�n cargadas en el container
	 * temporal independientemente de c�mo fue adquirido el contexto.
	 *
	 * Los pares de llaves �nicamente estar�n cargados en el caso de que el contexto fuera adquirido
	 * como CRYPT_VERIFYCONTEXT (Ephemereal)
	 */

	if ( (usbKey->tipo == LLAVEPUBLICA) || 
		 (usbKey->tipo == LLAVESESION)  ||
		 ((usbKey->tipo == PARLLAVES) && (usbHandle->dwFlags == CRYPT_VERIFYCONTEXT)) ) {

		if ( ! CryptDestroyKey(*(usbKey->hKey)) ) {
			ok = FALSE;
			goto finCPDestroyKey;
		}

		usbHandle->sesKeys.erase(usbKey);
	}

	CSPUTILS_HANDLE_Liberar(usbKey);
	usbKey = NULL;

finCPDestroyKey:

	DWORD lastErr = GetLastError();

	LOG_EndFunc(1, ok);
	SetLastError(lastErr);

    return ok;

}


/****************************************************************************/

BOOL WINAPI CPSetKeyParam(IN HCRYPTPROV hpProv, IN HCRYPTKEY hKey, IN DWORD dwParam, IN CONST BYTE *pbData, IN DWORD dwFlags)
{
	HUSBCRYPTPROV *usbHandle;
	HUSBCRYPTKEY *usbKey;

	BOOL ok = TRUE;

	if ( ! CSPUTILS_Ini () )
		return FALSE;

	LOG_BeginFunc(1);

	switch ( dwParam ) {
	case KP_SALT:
		LOG_Msg(10, "dwParam: KPSALT");
		break;
	case KP_SALT_EX:
		LOG_Msg(10, "dwParam: KPSALT_EX");
		break;
	case KP_PERMISSIONS:
		LOG_Msg(10, "dwParam: KP_PERMISSIONS");
		break;
	case KP_IV:
		LOG_Msg(10, "dwParam: KP_IV");
		break;
	case KP_PADDING:
		LOG_Msg(10, "dwParam: KP_PADDING");
		break;
	case KP_MODE:
		LOG_Msg(10, "dwParam: KP_MODE");
		break;
	case KP_MODE_BITS:
		LOG_Msg(10, "dwParam: KP_MODE_BITS");
		break;
	case KP_EFFECTIVE_KEYLEN:
		LOG_Msg(10, "dwParam: KP_EFFECTIVE_KEYLEN");
		break;
	case KP_ALGID:
		LOG_Msg(10, "dwParam: KP_ALGID");
		break;
	case KP_P:
		LOG_Msg(10, "dwParam: KP_P");
		break;
	case KP_Q:
		LOG_Msg(10, "dwParam: KP_Q");
		break;
	case KP_G:
		LOG_Msg(10, "dwParam: KP_G");
		break;
	case KP_X:
		LOG_Msg(10, "dwParam: KP_X");
		break;
	case KP_PUB_PARAMS:
		LOG_Msg(10, "dwParam: KP_PUB_PARAMS");
		break;

	default:
		LOG_Debug(10, "dwParam: %x", dwParam);
	}

	if ( ! hpProv ) {
		SetLastError(NTE_BAD_UID);
		ok = FALSE;
		goto finCPSetKeyParam;
	}

	if ( ! hKey ) {
		SetLastError(NTE_BAD_UID);
		ok = FALSE;
		goto finCPSetKeyParam;
	}

	usbHandle = (HUSBCRYPTPROV *) hpProv;
	usbKey    = (HUSBCRYPTKEY *) hKey;

	if ( (usbKey->tipo == LLAVESESION)  || 
		 (usbKey->tipo == LLAVEPUBLICA) ||
		 ( (usbKey->tipo == PARLLAVES) && (usbHandle->dwFlags == CRYPT_VERIFYCONTEXT) ) ) {


		LOG_Msg(10, "Llave p�blica o llave de sesi�n");

		if ( !CryptSetKeyParam(*(usbKey->hKey), dwParam,(BYTE *) pbData,dwFlags) ) {
			LOG_Error(1, "Fall� CryptSetKeyParam: %ld", GetLastError());
			ok = FALSE;
			goto finCPSetKeyParam;
		}

	} else {

		LOG_Msg(10, "Par de llaves");

		/* Tenemos un par de llaves persistentes. El �nico par�metro con sentido en este
		 * contexto es KP_PERMISSIONS. Nosotros �nicamente tendremos en cuenta la exportabilidad
		 * de las llaves.
		 */

		DWORD value;

		if ( dwFlags != 0 ) {
			LOG_MsgError(1, "BAD FLAGS");
			SetLastError(NTE_BAD_FLAGS);
			ok = FALSE;
			goto finCPSetKeyParam;
		}

		// ATENCI�N. No tenemos muy claro el funcionamiento correcto para KP_CERTIFICATE. La 
		// documentaci�n brilla por su ausencia. Nosotros, de momento, ignoramos el flag y
		// esperamos que la inserci�n del certificado se haga via el store provider.

		if ( dwParam == KP_CERTIFICATE ) {
			LOG_Msg(1, "Ignorado KP_CERTIFICATE");
			ok = TRUE;
			goto finCPSetKeyParam;
		}

/*		if ( dwParam == KP_MODE ) {
			LOG_Msg(1, "Ignorando KP_MODE para par de llaves");
			ok = TRUE;
			goto finCPSetKeyParam;
		}
*/
		if ( dwParam != KP_PERMISSIONS ) {
			LOG_MsgError(1, "NTE_BAD_TYPE");
			SetLastError(NTE_BAD_TYPE);
			ok = FALSE;
			goto finCPSetKeyParam;
		}

		if ( pbData == NULL ) {
			LOG_MsgError(1, "NTE_BAD_FLAGS");
			SetLastError(NTE_BAD_FLAGS);
			ok = FALSE;
			goto finCPSetKeyParam;
		}

		value = *((DWORD *) pbData);

		BOOL exp;
		exp = (value & CRYPT_EXPORT) ? TRUE : FALSE;

		if ( ! CSPUTILS_KeyContainer_SetExportable(usbHandle,usbKey,exp) ) {
			LOG_MsgError(1, "No se pudo establecer a exportable");
			SetLastError(NTE_FAIL);
			ok = FALSE;
			goto finCPSetKeyParam;
		}
	}

finCPSetKeyParam:

	DWORD lastErr = GetLastError();

	LOG_EndFunc(1, ok);
	SetLastError(lastErr);

    return ok;
}

/****************************************************************************/

BOOL WINAPI CPGetKeyParam(IN HCRYPTPROV hpProv, IN HCRYPTKEY hKey, IN DWORD dwParam, OUT LPBYTE pbData, IN OUT LPDWORD pcbDataLen, IN DWORD dwFlags)
{
	HUSBCRYPTPROV *usbHandle;
	HUSBCRYPTKEY *usbKey;

	BOOL ok = TRUE;

	if ( ! CSPUTILS_Ini () )
		return FALSE;

	LOG_BeginFunc(1);


	switch ( dwParam ) {
		case KP_ALGID:
			LOG_Msg(10, "KP_ALGID");
			break;
		case KP_BLOCKLEN:
			LOG_Msg(10, "KP_BLOCKLEN");
			break;

		case KP_KEYLEN:
			LOG_Msg(10, "KP_KEYLEN");
			break;

		case KP_SALT:
			LOG_Msg(10, "KP_SALT");
			break;

		case KP_PERMISSIONS:
			LOG_Msg(10, "KP_PERMISSIONS");
			break;

		case KP_IV:
			LOG_Msg(10, "KP_IV");
			break;

		case KP_PADDING:
			LOG_Msg(10, "KP_PADDING");
			break;

		case KP_MODE:
			LOG_Msg(10, "KP_ALGID");
			break;

		case KP_MODE_BITS:
			LOG_Msg(10, "KP_ALGID");
			break;

		case KP_EFFECTIVE_KEYLEN:
			LOG_Msg(10, "KP_ALGID");
			break;

	default:
		LOG_Debug(10, "dwParam: %x", dwParam);
	}

	if ( ! hpProv ) {
		SetLastError(NTE_BAD_UID);
		ok = FALSE;
		goto finCPGetKeyParam;
	}

	if ( ! hKey ) {
		SetLastError(NTE_BAD_UID);
		ok = FALSE;
		goto finCPGetKeyParam;
	}


	usbHandle = (HUSBCRYPTPROV *) hpProv;
	usbKey    = (HUSBCRYPTKEY *) hKey;

	if ((usbKey->tipo == LLAVESESION) || 
		(usbKey->tipo == LLAVEPUBLICA) || 
		((usbKey->tipo == PARLLAVES) && ( usbHandle->dwFlags == CRYPT_VERIFYCONTEXT) ) ) {

		if ( ! CryptGetKeyParam(*(usbKey->hKey),dwParam,pbData,pcbDataLen,dwFlags) ) {
			ok = FALSE;
			goto finCPGetKeyParam;
		}

	} else {

		if ( dwFlags != 0 ) {
			ok = FALSE;
			goto finCPGetKeyParam;
		}

		/* Al igual que en la funci�n CPSetKeyParam, lo que hacemos es �nicamente
		 * considerar el par�metro KP_PERMISSIONS y �nicamente referido a la exportabilidad de las llaves
		 */

		if ( dwParam == KP_ALGID ) {
			if ( pbData == NULL ) {
				*pcbDataLen = sizeof(ALG_ID);
				ok = TRUE;
				goto finCPGetKeyParam;
			}
			if ( (*pcbDataLen) < sizeof(ALG_ID) ) {
				SetLastError(ERROR_MORE_DATA);
				ok = FALSE;
				goto finCPGetKeyParam;
			}

			if ( usbKey->algoritmo == AT_SIGNATURE ) {
				LOG_Msg(10, "AT_SIGNATURE");
				*pbData = CALG_RSA_SIGN;
			} else if ( usbKey->algoritmo == AT_KEYEXCHANGE ) {
				LOG_Msg(10, "AT_KEYEXCHANGE");
				*pbData = CALG_RSA_KEYX;
			} else {
				*pbData = usbKey->algoritmo;

				if ( (*pbData == CALG_RSA_KEYX) || (*pbData == AT_KEYEXCHANGE) )
					LOG_Msg(10, "AT_KEYEXCHANGE");
				else
					LOG_Msg(10, "AT_SIGNATURE");
			}



		} else if ( dwParam == KP_PERMISSIONS ) {
			if ( pbData == NULL ) {
				*pcbDataLen = sizeof(DWORD);
				ok = TRUE;
				goto finCPGetKeyParam;
			}

			if ( (*pcbDataLen) < sizeof(DWORD) ) {
				SetLastError(ERROR_MORE_DATA);
				ok = FALSE;
				goto finCPGetKeyParam;
			}
			BOOL exp;
			DWORD *data = (DWORD *) pbData;
			if ( ! CSPUTILS_KeyContainer_GetExportable(usbHandle, usbKey, exp) ) {
				SetLastError(NTE_FAIL);
				ok = FALSE;
				goto finCPGetKeyParam;
			}
			if ( exp )
				*data = 0 | CRYPT_EXPORT;
			else
				*data = 0;
		} else {
			SetLastError(NTE_BAD_TYPE);
			ok = FALSE;
			goto finCPGetKeyParam;
		}


	}

finCPGetKeyParam:

	DWORD lastErr = GetLastError();
	LOG_EndFunc(1, ok);
	SetLastError(lastErr);

    return ok;

}

/****************************************************************************/

BOOL WINAPI CPSetProvParam(IN HCRYPTPROV hpProv, IN DWORD dwParam, IN CONST BYTE *pbData, IN DWORD dwFlags)
{
	DWORD ret = TRUE;

	if ( ! CSPUTILS_Ini () )
		return FALSE;

	LOG_BeginFunc(1);

	/* El �nico valor documentado y admitido para dwParam es PP_CLIENT_HWND. 
	 * Nosotros no hacemos nada con �l. Simplemente testearemos que se pasan
	 * correctamente los flags y ya est�
	 */

	if ( ! hpProv ) {
		ret = FALSE;
		SetLastError(NTE_BAD_UID);
		goto finCPSetProvParam;
	}

	if ( dwParam != PP_KEYSET_SEC_DESCR ) {
		ret = FALSE;
		SetLastError(NTE_BAD_TYPE);
		goto finCPSetProvParam;
	}

	if ( dwFlags & ~OWNER_SECURITY_INFORMATION & ~GROUP_SECURITY_INFORMATION & ~DACL_SECURITY_INFORMATION & ~SACL_SECURITY_INFORMATION ) {
		SetLastError(NTE_BAD_FLAGS);
		ret = FALSE;
		goto finCPSetProvParam;
	}

	/* Cuando se pasa NULL a pbData se espera que se devuelva ERROR_INVALID_PARAMETER
	 * Eso es lo que pide el CSP Test Suite
	 */

	if ( ! pbData ) {
		ret = FALSE;
		SetLastError(ERROR_INVALID_PARAMETER);
		goto finCPSetProvParam;
	}


finCPSetProvParam:

	LOG_EndFunc(1, ret);

    return ret;
}

/****************************************************************************/

BOOL WINAPI CPGetProvParam(IN HCRYPTPROV hpProv, IN DWORD dwParam, OUT LPBYTE pbData, IN OUT LPDWORD pcbDataLen, IN DWORD dwFlags)
{
	HUSBCRYPTPROV *usbHandle;
	BOOL ok = TRUE;
	DWORD dwLastError = 0;

	if ( ! CSPUTILS_Ini () )
		return FALSE;

	LOG_BeginFunc(1);

	if ( ! hpProv ) {
		dwLastError = NTE_BAD_UID;
		ok = FALSE;
		goto finCPGetProvParam;
	}

	usbHandle = (HUSBCRYPTPROV *) hpProv;

	/*
	 * Testeo la correci�n del par�metro dwFlags
	 */

	if ( dwParam == PP_KEYSET_SEC_DESCR ) {
		/* S�lo los siguientes flags pueden estar activados en este caso
		 */
		if ( dwFlags & ~OWNER_SECURITY_INFORMATION & ~GROUP_SECURITY_INFORMATION & ~DACL_SECURITY_INFORMATION & ~SACL_SECURITY_INFORMATION ) {
			dwLastError = NTE_BAD_FLAGS;
			ok = FALSE;
			goto finCPGetProvParam;
		}
	} else if ( ( dwFlags & CRYPT_FIRST ) && ( dwParam != PP_ENUMALGS ) && ( dwParam != PP_ENUMALGS_EX ) && ( dwParam != PP_ENUMCONTAINERS ) )
	{
		/* Si dwFlags == CRYPT_FIRST y no me piden una enumeraci�n, entonces error
		 */

		dwLastError = NTE_BAD_FLAGS;
		ok = FALSE;
		goto finCPGetProvParam;

	}


	/*************************************************************************/

	if ( (dwParam == PP_CONTAINER) || (dwParam == PP_UNIQUE_CONTAINER) ) {

		DWORD tamContainer;

		if ( ! pcbDataLen ) {
			dwLastError = ERROR_INVALID_PARAMETER;
			ok = FALSE;
			goto finCPGetProvParam;
		}

		if ( ! usbHandle->szContainer ) {
			dwLastError = ERROR_INVALID_PARAMETER;
			ok = FALSE;
			goto finCPGetProvParam;
		}

		tamContainer = strlen(usbHandle->szContainer) + 1;

		if ( ! pbData ) {
			*pcbDataLen = tamContainer;
			ok = TRUE;
			goto finCPGetProvParam;
		}

		if ( *pcbDataLen < tamContainer ) {
			dwLastError = ERROR_MORE_DATA;
			ok = FALSE;
			goto finCPGetProvParam;
		}

		*pcbDataLen = tamContainer;
		memcpy(pbData,usbHandle->szContainer, tamContainer);

		goto finCPGetProvParam;

	/*************************************************************************/

	} else if ( dwParam == PP_IMPTYPE ) {
	
		if ( pcbDataLen == NULL ) {
			dwLastError = ERROR_INVALID_PARAMETER;
			ok = FALSE;
			goto finCPGetProvParam;
		}
		
		if ( pbData == NULL ) {
			*pcbDataLen = sizeof(DWORD);
			goto finCPGetProvParam;
		}

		if ( *pcbDataLen < sizeof(DWORD) ) {

			dwLastError = ERROR_MORE_DATA;
			ok = FALSE;
			goto finCPGetProvParam;

		}

		*pcbDataLen = sizeof(DWORD);

		*pbData = CRYPT_IMPL_SOFTWARE;

		goto finCPGetProvParam;

	/*************************************************************************/

	} else if ( dwParam == PP_NAME ) {

		DWORD tamName;

		if ( pcbDataLen == NULL ) {
			dwLastError = ERROR_INVALID_PARAMETER;
			ok = FALSE;
			goto finCPGetProvParam;
		}


		tamName = strlen(l_szProviderName) + 1;
		
		if ( pbData == NULL ) {
			*pcbDataLen = tamName;
			goto finCPGetProvParam;
		}

		if ( *pcbDataLen < tamName ) {
			dwLastError = ERROR_MORE_DATA;
			ok = FALSE;
			goto finCPGetProvParam;
		}

		*pcbDataLen = tamName;
		memcpy(pbData, l_szProviderName, tamName);

		goto finCPGetProvParam;

	/*************************************************************************/

	} else if ( dwParam == PP_PROVTYPE ) {

		if ( pcbDataLen == NULL ) {
			dwLastError = ERROR_INVALID_PARAMETER;
			ok = FALSE;
			goto finCPGetProvParam;
		}
		
		if ( pbData == NULL ) {
			*pcbDataLen = sizeof(DWORD);
			goto finCPGetProvParam;
		}

		if ( *pcbDataLen < sizeof(DWORD) ) {
			dwLastError = ERROR_MORE_DATA;
			ok = FALSE;
			goto finCPGetProvParam;
		}

		*pcbDataLen = sizeof(DWORD);
		*pbData     = PROV_RSA_FULL;

		goto finCPGetProvParam;

	/*************************************************************************/

	} else if ( dwParam == PP_ENUMALGS ) {

		/* El CSP Test Suite da un warning aqu�. Le he pasado el mismo test al
		 * Enhanced y saca el mismo warning. No pasa nada (aunque no deja de
		 * ser curioso que un CSP de Microsoft de un warning en una bater�a
		 * de pruebas de Microsoft)
		 */

		if ( !CryptGetProvParam(*(usbHandle->hpProv), dwParam, pbData, pcbDataLen, dwFlags) ) {
			dwLastError = GetLastError();
			ok = FALSE;
			goto finCPGetProvParam;
		}
	
		goto finCPGetProvParam;

	/*************************************************************************/

	} else if ( dwParam == PP_ENUMALGS_EX ) {


		if ( ! CryptGetProvParam(*(usbHandle->hpProv), dwParam, pbData, pcbDataLen, dwFlags) ) {
			dwLastError = GetLastError();
			ok = FALSE;
			goto finCPGetProvParam;
		}

		goto finCPGetProvParam;

	/*************************************************************************/

	} else if ( dwParam == PP_SIG_KEYSIZE_INC  ) {

		if ( !CryptGetProvParam(*(usbHandle->hpProv), dwParam, pbData, pcbDataLen, dwFlags) ) {
			dwLastError = GetLastError();
			ok = FALSE;
			goto finCPGetProvParam;
		}

		goto finCPGetProvParam;

	/*************************************************************************/

	} else if ( dwParam == PP_KEYX_KEYSIZE_INC ) {


		if ( !CryptGetProvParam(*(usbHandle->hpProv), dwParam, pbData, pcbDataLen, dwFlags) ) {
			dwLastError = GetLastError();
			ok = FALSE;
			goto finCPGetProvParam;
		}
	
		goto finCPGetProvParam;

	/*************************************************************************/

	} else if ( dwParam == PP_VERSION ) {
		
		if ( pcbDataLen == NULL ) {
			dwLastError = ERROR_INVALID_PARAMETER;
			ok = FALSE;
			goto finCPGetProvParam;
		}

		if ( pbData == NULL ) {

			*pcbDataLen = sizeof(DWORD);

			goto finCPGetProvParam;
		}

		if ( *pcbDataLen < sizeof(DWORD) ) {
			dwLastError = ERROR_MORE_DATA;
			ok = FALSE;
			goto finCPGetProvParam;
		}

		*pcbDataLen = sizeof(DWORD);
		*pbData     = CspVersion;
		
		goto finCPGetProvParam;

	/*************************************************************************/

	} else if ( dwParam == PP_ENUMCONTAINERS ) {

		if ( usbHandle->nodisp ) 
		{
			ok = FALSE;
			goto finCPGetProvParam;
		}

		if ( pcbDataLen == NULL ) {
			dwLastError = ERROR_INVALID_PARAMETER;
			ok = FALSE;
			goto finCPGetProvParam;
		}

		
		if ( dwFlags == CRYPT_FIRST ) {
			INFO_KEY_CONTAINER *infoKC = NULL;
			int tamKC;
			DWORD max = 0, aux;
	
			// Ante un CRYPT_FIRST reelemos la lista de containers

			if ( usbHandle->pInfoEnum->keyContainers ) {
				delete usbHandle->pInfoEnum->keyContainers;	
				usbHandle->pInfoEnum->pos = -1;
				usbHandle->pInfoEnum->tamVector = 0;
			}

			if ( !CSPUTILS_KeyContainer_Listar(usbHandle, &infoKC, &tamKC) ) {
				ok = FALSE;
				goto finCPGetProvParam;
			}

			// Determino el tama�o de max

			for ( int i = 0 ; i < tamKC ; i++ ) {
				aux = ::strlen(infoKC[i].nombreKeyContainer); 
				if ( aux > max )
					max = aux;
			}


			usbHandle->pInfoEnum->keyContainers = infoKC;
			usbHandle->pInfoEnum->pos = 0;
			usbHandle->pInfoEnum->tamVector = tamKC;
			usbHandle->pInfoEnum->maxItem = (max > 0) ? max + 1 : 0;

		}

		// Devuelvo el elemento a mostrar
		
		if ( (usbHandle->pInfoEnum->pos) >= (usbHandle->pInfoEnum->tamVector) ) {
			/* No hay m�s elementos a mostrar
			 */
			dwLastError = ERROR_NO_MORE_ITEMS;
			ok = FALSE;
			goto finCPGetProvParam;
		}

				
		if ( pbData != NULL ) {
			int pos;
			pos = usbHandle->pInfoEnum->pos;

			if ( *pcbDataLen < (usbHandle->pInfoEnum->maxItem) ) {
				*pcbDataLen = usbHandle->pInfoEnum->maxItem;
				dwLastError = ERROR_MORE_DATA;
				ok = FALSE;
				goto finCPGetProvParam;

			}

			::memcpy(pbData,
				     usbHandle->pInfoEnum->keyContainers[pos].nombreKeyContainer,
					 ::strlen(usbHandle->pInfoEnum->keyContainers[pos].nombreKeyContainer)+1);

			++(usbHandle->pInfoEnum->pos);

		} else 
			*pcbDataLen = usbHandle->pInfoEnum->maxItem;

		goto finCPGetProvParam;


	/*************************************************************************/

	} else if ( dwParam == PP_KEYSET_SEC_DESCR ) {
		
		*pcbDataLen = 0;
		pbData = NULL;

	/*************************************************************************/			

	} else if ( dwParam == PP_SGC_INFO ) {

		
		if ( !CryptGetProvParam(*(usbHandle->hpProv), dwParam, pbData, pcbDataLen, dwFlags) ) {
			dwLastError = GetLastError();
			ok = FALSE;
			goto finCPGetProvParam;
		}
	
		goto finCPGetProvParam;

	/*************************************************************************/
		
	} else if ( dwParam == PP_KEYSPEC ) {
		
	
		if ( !CryptGetProvParam(*(usbHandle->hpProv), dwParam, pbData, pcbDataLen, dwFlags) ) {
			dwLastError = GetLastError();
			ok = FALSE;
			goto finCPGetProvParam;
		}
	
		goto finCPGetProvParam;		
		
	} else {

		dwLastError = NTE_BAD_TYPE;

		ok = FALSE;
		goto finCPGetProvParam;
	}


finCPGetProvParam:

	LOG_EndFunc(1, ok);
	SetLastError(dwLastError);

	return ok;
}


/****************************************************************************/

BOOL WINAPI CPSetHashParam(IN HCRYPTPROV hpProv, IN HCRYPTHASH hHash, IN DWORD dwParam, IN CONST BYTE *pbData, IN DWORD dwFlags)
{
	HUSBCRYPTHASH *usbHash;
	BOOL ok = TRUE;

	if ( ! CSPUTILS_Ini () )
		return FALSE;

	LOG_BeginFunc(1);

	if ( ! hpProv ) {
		SetLastError(NTE_BAD_UID);
		ok = FALSE;
		goto finCPSetHashParam;
	}

	if ( ! hHash ) {
		SetLastError(NTE_BAD_HASH);
		ok = FALSE;
		goto finCPSetHashParam;
	}

	usbHash = (HUSBCRYPTHASH *) hHash;

	ok = CryptSetHashParam(*(usbHash->hHash), dwParam, (BYTE *) pbData, dwFlags);

finCPSetHashParam:

	DWORD lastErr = GetLastError();



	LOG_EndFunc(1, ok);
	SetLastError(lastErr);

    return ok;
}


/****************************************************************************/

BOOL WINAPI CPGetHashParam(IN HCRYPTPROV hpProv, IN HCRYPTHASH hHash, IN DWORD dwParam, OUT LPBYTE pbData, IN OUT LPDWORD pcbDataLen, IN DWORD dwFlags)
{
	HUSBCRYPTHASH *usbHash;
	BOOL ok = TRUE;

	if ( ! CSPUTILS_Ini () )
		return FALSE;

	LOG_BeginFunc(1);

	if ( ! hHash ) {
		SetLastError(NTE_BAD_HASH);
		ok = FALSE;
		goto finCPGetHashParam;
	}

	usbHash = (HUSBCRYPTHASH *) hHash;

	ok = CryptGetHashParam(*(usbHash->hHash), dwParam, pbData, pcbDataLen, dwFlags);

finCPGetHashParam:

	DWORD lastErr = GetLastError();


	LOG_EndFunc(1, ok);
	SetLastError(lastErr);

    return ok;
}


/****************************************************************************/

BOOL WINAPI CPExportKey(IN HCRYPTPROV hpProv, IN HCRYPTKEY hKey, IN HCRYPTKEY hPubKey, IN DWORD dwBlobType, IN DWORD dwFlags, OUT LPBYTE pbData, IN OUT LPDWORD pcbDataLen)
{
	HUSBCRYPTKEY *usbKey, *usbPubKey;
	HUSBCRYPTPROV *usbProv, *usbProvTemp = NULL;
	BOOL ok = TRUE, exportable = FALSE;

	if ( ! CSPUTILS_Ini () )
		return FALSE;

	LOG_BeginFunc(1);

	if ( ! hpProv ) {
		SetLastError(NTE_BAD_UID);
		ok = FALSE;
		goto finCPExportKey;
	}

	if ( ! hKey ) {
		SetLastError(NTE_BAD_KEY);
		ok = FALSE;
		goto finCPExportKey;
	}

	usbProv   = (HUSBCRYPTPROV *) hpProv;
	usbKey    = (HUSBCRYPTKEY *) hKey;

	if ( (hPubKey == 0) || (hPubKey == NULL) ) 
		usbPubKey = 0;
	else 
		usbPubKey = (HUSBCRYPTKEY *) hPubKey;

	/* Este fragmento de c�digo hace que CAPICOM no funcione del todo
	 * correcto. Seguramente debido a alg�n fallo del CSP. Estudiar.
	 */

/*	if ( ! CSPUTILS_KeyContainer_GetExportable (usbProv, usbKey, exportable) ) {
				ok = FALSE;
		goto finCPExportKey;
	}

	if ( ! exportable ) {
				ok = FALSE;
		SetLastError(NTE_BAD_KEY_STATE);
		goto finCPExportKey;
	}
*/

	/* Cargamos la llave p�blica de exportaci�n en caso de que sea de tipo PARLLAVES
	 */

	if ( usbPubKey ) {

		if ( (usbPubKey->tipo == PARLLAVES) && (usbPubKey->hProv->dwFlags != CRYPT_VERIFYCONTEXT) ) {

			/* De momento me veo obligado a cargar todo el persistente. usbPubKey es realmente
			 * una llave p�blica importada, por aqu� no vamos a pasar. Sin embargo, si se trata
			 * por ejemplo, de la llave AT_KEYEXCHANGE m�a, no la tengo en el container todav�a
			 */

			if ( ! CSPUTILS_Subyacente_CargarPersistente(usbPubKey->hProv) ) {
					ok = FALSE;
					goto finCPExportKey;
			}
	
			if ( ! CryptGetUserKey(*(usbPubKey->hProv->hpProv), usbPubKey->algoritmo, usbPubKey->hKey) ) {
				ok = FALSE;
				goto finCPExportKey;
			}
		}
	}

	/* Cargamos el par de llaves a exportar si es de tipo PARLLAVES
	 */

	if ( (usbKey->tipo == PARLLAVES) && (usbKey->hProv->dwFlags != CRYPT_VERIFYCONTEXT)) {

		if ( ! CSPUTILS_Subyacente_CargarPersistente(usbProv) ) {
			
			if ( usbPubKey )
				if ( (usbPubKey->tipo == PARLLAVES) && (usbPubKey->hProv->dwFlags != CRYPT_VERIFYCONTEXT) ) 
				{
					CSPUTILS_Subyacente_DescargarPersistente(usbPubKey->hProv, AT_KEYEXCHANGE, CSPUTILS_NO_SAVE_KEYS, FALSE);
					CSPUTILS_Subyacente_DescargarPersistente(usbPubKey->hProv, AT_SIGNATURE, CSPUTILS_NO_SAVE_KEYS, FALSE);
				}

			ok = FALSE;
			goto finCPExportKey;
		}


		if ( ! CryptGetUserKey(*(usbProv->hpProv), usbKey->algoritmo, usbKey->hKey) ) {
			if ( usbPubKey )
				if ( (usbPubKey->tipo == PARLLAVES) &&
					(usbPubKey->hProv->dwFlags != CRYPT_VERIFYCONTEXT) ) {

					CSPUTILS_Subyacente_DescargarPersistente(usbPubKey->hProv, AT_KEYEXCHANGE, CSPUTILS_NO_SAVE_KEYS,FALSE);
					CSPUTILS_Subyacente_DescargarPersistente(usbPubKey->hProv, AT_SIGNATURE, CSPUTILS_NO_SAVE_KEYS,FALSE);
					}

			CSPUTILS_Subyacente_DescargarPersistente(usbProv, AT_KEYEXCHANGE, CSPUTILS_NO_SAVE_KEYS,FALSE);
			CSPUTILS_Subyacente_DescargarPersistente(usbProv, AT_SIGNATURE, CSPUTILS_NO_SAVE_KEYS,FALSE);

			ok = FALSE;
			goto finCPExportKey;
		}
	}

	/* Muy bien... ahora a exportar
	 */

	if ( usbPubKey ) {

		if ( !CryptExportKey(*(usbKey->hKey), *(usbPubKey->hKey), dwBlobType, dwFlags, pbData, pcbDataLen ) ) {

			if ( (usbPubKey->tipo == PARLLAVES) &&
				(usbPubKey->hProv->dwFlags != CRYPT_VERIFYCONTEXT) ) {
				CSPUTILS_Subyacente_DescargarPersistente(usbPubKey->hProv, AT_KEYEXCHANGE, CSPUTILS_NO_SAVE_KEYS,FALSE);
				CSPUTILS_Subyacente_DescargarPersistente(usbPubKey->hProv, AT_SIGNATURE, CSPUTILS_NO_SAVE_KEYS,FALSE);
			}

			if ( (usbKey->tipo == PARLLAVES) &&
				(usbKey->hProv->dwFlags != CRYPT_VERIFYCONTEXT)) {
				CSPUTILS_Subyacente_DescargarPersistente(usbProv, AT_KEYEXCHANGE, CSPUTILS_NO_SAVE_KEYS,FALSE);
				CSPUTILS_Subyacente_DescargarPersistente(usbProv, AT_SIGNATURE, CSPUTILS_NO_SAVE_KEYS,FALSE);
			}
		
			ok = FALSE;
			goto finCPExportKey;

		}

	} else {

		if ( !CryptExportKey(*(usbKey->hKey), 0, dwBlobType, dwFlags, pbData, pcbDataLen ) ) {

			if ( (usbKey->tipo == PARLLAVES) &&
				(usbKey->hProv->dwFlags != CRYPT_VERIFYCONTEXT)) {
				CSPUTILS_Subyacente_DescargarPersistente(usbProv, AT_KEYEXCHANGE, CSPUTILS_NO_SAVE_KEYS,FALSE);
				CSPUTILS_Subyacente_DescargarPersistente(usbProv, AT_SIGNATURE, CSPUTILS_NO_SAVE_KEYS,FALSE);
			}
		
			ok = FALSE;
			goto finCPExportKey;
		}

	}

	/* Ahora limpiamos lo que tengamos que limpiar
	 */

	if ( usbPubKey )
		if ( (usbPubKey->tipo == PARLLAVES) && (usbPubKey->hProv->dwFlags != CRYPT_VERIFYCONTEXT) ) {
			CSPUTILS_Subyacente_DescargarPersistente(usbPubKey->hProv, AT_KEYEXCHANGE, CSPUTILS_NO_SAVE_KEYS,FALSE);
			CSPUTILS_Subyacente_DescargarPersistente(usbPubKey->hProv, AT_SIGNATURE, CSPUTILS_NO_SAVE_KEYS,FALSE);
		}

	if ( (usbKey->tipo == PARLLAVES) && (usbKey->hProv->dwFlags != CRYPT_VERIFYCONTEXT)) {
		CSPUTILS_Subyacente_DescargarPersistente(usbProv, AT_KEYEXCHANGE, CSPUTILS_NO_SAVE_KEYS,FALSE);
		CSPUTILS_Subyacente_DescargarPersistente(usbProv, AT_SIGNATURE, CSPUTILS_NO_SAVE_KEYS,FALSE);
	}


finCPExportKey:

	DWORD lastErr = GetLastError();


	LOG_EndFunc(1, ok);
	SetLastError(lastErr);

    return ok;
}

/****************************************************************************/

/* Esta funci�n est� para revisar
 */

BOOL WINAPI CPImportKey(IN HCRYPTPROV hpProv, IN CONST BYTE *pbData, IN DWORD cbDataLen, IN HCRYPTKEY hPubKey, IN DWORD dwFlags, OUT HCRYPTKEY *phKey)
{
	HUSBCRYPTPROV *usbProv;
	HUSBCRYPTKEY *usbPubKey, *usbKeyOut = NULL;
	BOOL ok = TRUE, nuevaPassword;
	ALG_ID Algid;
	DWORD len;

	if ( ! CSPUTILS_Ini () )
		return FALSE;

	LOG_BeginFunc(1);


	LOG_Msg(10, "1");

	if ( ! hpProv ) {
		LOG_MsgError(1, "ERROR");
		SetLastError(NTE_BAD_UID);
		ok = FALSE;
		goto finCPImportKey;
	}

	LOG_Msg(10, "1");

	usbProv = (HUSBCRYPTPROV *) hpProv;
	if( hPubKey )
		usbPubKey = (HUSBCRYPTKEY *) hPubKey;
	else
		usbPubKey = NULL;

LOG_Msg(10, "1");

	if ( usbPubKey ) {

		if ( (usbPubKey->tipo == PARLLAVES) && (usbPubKey->hProv->dwFlags != CRYPT_VERIFYCONTEXT) ) {

			if ( ! CSPUTILS_Subyacente_CargarPersistente(usbPubKey->hProv) ) {
				LOG_MsgError(1, "ERROR");
				ok = FALSE;
				goto finCPImportKey;
			}

			if ( !CryptGetUserKey(*(usbPubKey->hProv->hpProv), usbPubKey->algoritmo, usbPubKey->hKey) ) {
				LOG_MsgError(1, "ERROR");
				ok = FALSE;
				goto finCPImportKey;
			}
		}
	}


	LOG_Msg(10, "1");
	if ( ! CSPUTILS_HANDLE_Nuevo(&usbKeyOut) ) {
		LOG_MsgError(1, "ERROR");
		ok = FALSE;
		goto finCPImportKey;
	}
	LOG_Msg(10, "1");

	if ( *pbData == PUBLICKEYBLOB ) {

		LOG_Msg(10, "1");
		
		if ( usbPubKey ) {
			if ( ! CryptImportKey(*(usbProv->hpProv), pbData, cbDataLen, *(usbPubKey->hKey), dwFlags,usbKeyOut->hKey) ) {
				LOG_MsgError(1, "ERROR");
				ok = FALSE;
				goto finCPImportKey;
			}
		} else {
			if ( ! CryptImportKey(*(usbProv->hpProv), pbData, cbDataLen, 0, dwFlags,usbKeyOut->hKey) ) {
				LOG_MsgError(1, "ERROR");
				ok = FALSE;
				goto finCPImportKey;
			}
		}

		LOG_Msg(10, "1");

		usbKeyOut->algoritmo = *((ALG_ID *) pbData+sizeof(BLOBHEADER));
		usbKeyOut->tipo = LLAVEPUBLICA;
		
	} else if ( *pbData == PRIVATEKEYBLOB ) {

		RSAPUBKEY *pk;

		LOG_Msg(10, "1");

		/* Compruebo que el tama�o de la llave sea menor a 16384 bits
		 */

		pk = (RSAPUBKEY *) (pbData+sizeof(BLOBHEADER));
		if ( pk->bitlen > 16384 ) {
			LOG_MsgError(1, "Key too long!!");
			SetLastError(NTE_FAIL);
			ok = FALSE;
			goto finCPImportKey;
		}

		LOG_Msg(10, "1");

		if ( (usbProv->dwFlags & CRYPT_SILENT) && (dwFlags & CRYPT_USER_PROTECTED) ) {
			LOG_MsgError(1, "Bad flags");
			SetLastError(NTE_SILENT_CONTEXT);
			ok = FALSE;
			goto finCPImportKey;
		}

		LOG_Msg(10, "1");

		if ( usbPubKey ) {
			if ( ! CryptImportKey(*(usbProv->hpProv), pbData, cbDataLen, *(usbPubKey->hKey), CRYPT_EXPORTABLE, usbKeyOut->hKey) ) {
				ok = FALSE;
				goto finCPImportKey;
			}
		} else {
		
			if ( ! CryptImportKey(*(usbProv->hpProv), pbData, cbDataLen, 0, CRYPT_EXPORTABLE, usbKeyOut->hKey) ) {
				LOG_MsgError(1, "HEre");
				ok = FALSE;
				goto finCPImportKey;
			}
		}

		LOG_Msg(10, "1");

		/* Determinamos qu� llave nos han importado
		 */

		len = sizeof(Algid);
		if ( ! CryptGetKeyParam(*(usbKeyOut->hKey), KP_ALGID, (BYTE *) &Algid, &len, 0) ) {
			LOG_MsgError(1, "ERROROROR");
			CryptDestroyKey(*(usbKeyOut->hKey));
			ok = FALSE;
			goto finCPImportKey;
		}

		LOG_Msg(10, "1");

		if ( Algid == AT_KEYEXCHANGE || Algid == CALG_RSA_KEYX )
			usbKeyOut->algoritmo = AT_KEYEXCHANGE;
		else if ( Algid == AT_SIGNATURE || Algid == CALG_RSA_SIGN )
			usbKeyOut->algoritmo = AT_SIGNATURE;

		LOG_Msg(10, "1");

		usbKeyOut->tipo = PARLLAVES;
		nuevaPassword   = dwFlags & CRYPT_USER_PROTECTED;

		if ( ! CSPUTILS_Subyacente_DescargarPersistente(usbProv, usbKeyOut->algoritmo, CSPUTILS_SAVE_KEYS, nuevaPassword) ) {
			LOG_MsgError(1, "ERROROROR");
			CryptDestroyKey(*(usbKeyOut->hKey));
			ok = FALSE;
			goto finCPImportKey;
		}

		LOG_Msg(10, "1");

/*		if ( usbKeyOut->algoritmo == AT_SIGNATURE ) 
			CSPUTILS_Subyacente_DescargarPersistente(usbProv, AT_KEYEXCHANGE, CSPUTILS_NO_SAVE_KEYS, FALSE);
		else
			CSPUTILS_Subyacente_DescargarPersistente(usbProv, AT_SIGNATURE, CSPUTILS_NO_SAVE_KEYS, FALSE);
*/
	} else if ( *pbData == SIMPLEBLOB ) {

		LOG_Msg(10, "1");

		/*
		 * Si el blob lleva una llave de sesi�n... pues a por ellos
		 */

		if ( usbPubKey ) {

			/* El blob est� cifrado con una llave p�blica
			 */

			if ( !CryptImportKey(*(usbProv->hpProv), pbData, cbDataLen, *(usbPubKey->hKey), dwFlags, usbKeyOut->hKey) ) {
				ok = FALSE;
				goto finCPImportKey;
			}

		} else {

			if ( !CryptImportKey(*(usbProv->hpProv), pbData, cbDataLen, NULL, dwFlags, usbKeyOut->hKey) ) {
				ok = FALSE;
				goto finCPImportKey;
			}
		}

		LOG_Msg(10, "1");

		usbKeyOut->algoritmo = *((ALG_ID *) pbData+sizeof(BLOBHEADER));
		usbKeyOut->tipo = LLAVESESION;			

	} else {
		LOG_Msg(10, "1");

		SetLastError(NTE_BAD_TYPE);
		ok = FALSE;
		goto finCPImportKey;
	}

	LOG_Msg(10, "1");

	*phKey = (HCRYPTKEY) usbKeyOut;

finCPImportKey:

	DWORD lastErr = GetLastError();

	if ( ! ok ) 
		if ( usbKeyOut ) 
			CSPUTILS_HANDLE_Liberar(usbKeyOut);

	LOG_EndFunc(1, ok);
	SetLastError(lastErr);

    return ok;
}

/****************************************************************************/

BOOL WINAPI CPEncrypt(IN HCRYPTPROV hpProv, IN HCRYPTKEY hKey, IN HCRYPTHASH hHash, IN BOOL fFinal, IN DWORD dwFlags, IN OUT LPBYTE pbData, IN OUT LPDWORD pcbDataLen, IN DWORD cbBufLen)
{
	HUSBCRYPTKEY *usbKey;
	HUSBCRYPTHASH *usbHash;
	HUSBCRYPTPROV *usbProv;

	BOOL descargar = FALSE, ok = TRUE;

	if ( ! CSPUTILS_Ini () )
		return FALSE;

	LOG_BeginFunc(1);

	if ( ! hpProv ) {
		SetLastError(NTE_BAD_UID);
		ok = FALSE;
		goto finCPEncrypt;
	}

	if ( ! hKey ) {
		SetLastError(NTE_BAD_KEY);
		ok = FALSE;
		goto finCPEncrypt;
	}

	usbKey    = (HUSBCRYPTKEY *) hKey;
	usbHash   = (HUSBCRYPTHASH *) hHash;
	usbProv   = (HUSBCRYPTPROV *) hpProv;

	if ((usbKey->tipo == PARLLAVES) && (usbProv->dwFlags != CRYPT_VERIFYCONTEXT)) {
		
		descargar = TRUE;

		if  ( ! CSPUTILS_Subyacente_CargarPersistente(usbProv)) {
			ok = FALSE;
			goto finCPEncrypt;
		}

		if ( ! CryptGetUserKey(*(usbProv->hpProv), usbKey->algoritmo, usbKey->hKey) ) {
			CSPUTILS_Subyacente_DescargarPersistente(usbProv, AT_SIGNATURE, CSPUTILS_NO_SAVE_KEYS, FALSE);
			CSPUTILS_Subyacente_DescargarPersistente(usbProv, AT_KEYEXCHANGE, CSPUTILS_NO_SAVE_KEYS, FALSE);
			ok = FALSE;
			goto finCPEncrypt;
		}
	}


	if ( usbHash ) {
		ok = CryptEncrypt(*(usbKey->hKey), *(usbHash->hHash), fFinal, dwFlags, pbData, pcbDataLen, cbBufLen);
	} else {
		ok = CryptEncrypt(*(usbKey->hKey), 0, fFinal, dwFlags, pbData, pcbDataLen, cbBufLen);
	}

	if ( descargar ) {
		DWORD err;
		if (!ok) {
			err = GetLastError();
		}
	
		CSPUTILS_Subyacente_DescargarPersistente(usbProv, AT_SIGNATURE, CSPUTILS_NO_SAVE_KEYS, FALSE);
		CSPUTILS_Subyacente_DescargarPersistente(usbProv, AT_KEYEXCHANGE, CSPUTILS_NO_SAVE_KEYS, FALSE);
		CryptDestroyKey(*(usbKey->hKey));

		if ( !ok )
			SetLastError(err);
	}

finCPEncrypt:

	DWORD lastErr = GetLastError();


	LOG_EndFunc(1, ok);
	SetLastError(lastErr);
	
	return ok;

}

/****************************************************************************/

BOOL WINAPI CPDecrypt(IN HCRYPTPROV hpProv, IN HCRYPTKEY hKey, IN HCRYPTHASH hHash, IN BOOL fFinal, IN DWORD dwFlags, IN OUT LPBYTE pbData, IN OUT LPDWORD pcbDataLen)
{
	HUSBCRYPTKEY *usbKey;
	HUSBCRYPTHASH *usbHash;
	HUSBCRYPTPROV *usbProv;

	BOOL descargar = FALSE,ok = TRUE;

	if ( ! CSPUTILS_Ini () )
		return FALSE;

	LOG_BeginFunc(1);

	if ( ! hpProv ) {
		SetLastError(NTE_BAD_UID);
		ok = FALSE;
		goto finCPDecrypt;
	}


	if ( ! hKey ) {
		SetLastError(NTE_BAD_KEY);
		ok = FALSE;
		goto finCPDecrypt;
	}

	usbKey    = (HUSBCRYPTKEY *) hKey;
	usbHash   = (HUSBCRYPTHASH *) hHash;
	usbProv   = (HUSBCRYPTPROV *) hpProv;

	if ((usbKey->tipo == PARLLAVES) &&
		(usbProv->dwFlags != CRYPT_VERIFYCONTEXT)) {
		
		descargar = TRUE;

		if  ( ! CSPUTILS_Subyacente_CargarPersistente(usbProv)) {
			ok = FALSE;
			goto finCPDecrypt;
		}

		if ( !CryptGetUserKey(*(usbProv->hpProv), usbKey->algoritmo, usbKey->hKey) ) {
			CSPUTILS_Subyacente_DescargarPersistente(usbProv, AT_SIGNATURE, CSPUTILS_NO_SAVE_KEYS, FALSE);
			CSPUTILS_Subyacente_DescargarPersistente(usbProv, AT_KEYEXCHANGE, CSPUTILS_NO_SAVE_KEYS, FALSE);
			ok = FALSE;
			goto finCPDecrypt;
		}
	}


	if ( usbHash ) {
		ok = CryptDecrypt(*(usbKey->hKey), *(usbHash->hHash), fFinal, dwFlags, pbData, pcbDataLen);
	} else {
		ok = CryptDecrypt(*(usbKey->hKey), 0, fFinal, dwFlags, pbData, pcbDataLen);
	}


	if ( descargar ) {
		DWORD err;
		if (!ok)
			err = GetLastError();
	
		CSPUTILS_Subyacente_DescargarPersistente(usbProv, AT_SIGNATURE, CSPUTILS_NO_SAVE_KEYS, FALSE);
		CSPUTILS_Subyacente_DescargarPersistente(usbProv, AT_KEYEXCHANGE, CSPUTILS_NO_SAVE_KEYS, FALSE);
		CryptDestroyKey(*(usbKey->hKey));

		if ( !ok )
			SetLastError(err);
	}

finCPDecrypt:

	DWORD lastErr = GetLastError();


	LOG_EndFunc(1, ok);
	SetLastError(lastErr);
	
	return ok;
}

/****************************************************************************/

BOOL WINAPI CPCreateHash(IN HCRYPTPROV hpProv, IN ALG_ID Algid, IN HCRYPTKEY hKey, IN DWORD dwFlags, OUT HCRYPTHASH *phHash)
{
	HUSBCRYPTPROV *usbProv;
	HUSBCRYPTKEY *usbKey;
	HUSBCRYPTHASH *usbHash = NULL;

	BOOL ok = TRUE, descargar = FALSE;

	if ( ! CSPUTILS_Ini () )
		return FALSE;

	LOG_BeginFunc(1);

	if ( ! hpProv ) {
		SetLastError(NTE_BAD_UID);
		ok = FALSE;
		goto finCPCreateHash;
	}

	usbProv = (HUSBCRYPTPROV *) hpProv;
	usbKey  = (HUSBCRYPTKEY *) hKey;

	if ( dwFlags != 0 ) {
		SetLastError(NTE_BAD_FLAGS);
		ok = FALSE;
		goto finCPCreateHash;
	}

	if ( ((Algid != CALG_HMAC) ||(Algid != CALG_MAC)) && (hKey != 0) ) {
		SetLastError(NTE_BAD_KEY);
		ok = FALSE;
		goto finCPCreateHash;
	}


	if ( hKey && (usbKey->tipo == PARLLAVES) &&
		(usbProv->dwFlags != CRYPT_VERIFYCONTEXT)) {
		
		descargar = TRUE;

		if  ( ! CSPUTILS_Subyacente_CargarPersistente(usbProv)) {
			ok = FALSE;
			goto finCPCreateHash;
		}

		if ( !CryptGetUserKey(*(usbProv->hpProv), usbKey->algoritmo, usbKey->hKey) ) {
			CSPUTILS_Subyacente_DescargarPersistente(usbProv, AT_SIGNATURE, CSPUTILS_NO_SAVE_KEYS, FALSE);
			CSPUTILS_Subyacente_DescargarPersistente(usbProv, AT_KEYEXCHANGE, CSPUTILS_NO_SAVE_KEYS, FALSE);
			return FALSE;
		}
	}
	
	if ( ! CSPUTILS_HANDLE_Nuevo(&usbHash) ) {
		ok = FALSE;
		goto finCPCreateHash;
	}

	if ( hKey ) {
		ok = CryptCreateHash(*(usbProv->hpProv), Algid, *(usbKey->hKey), dwFlags, usbHash->hHash);
	} else {
		ok = CryptCreateHash(*(usbProv->hpProv), Algid, 0, dwFlags, usbHash->hHash);
	}

	if ( hKey && descargar ) {

		DWORD err;
		if (!ok) {
			err = GetLastError();
		}
		CSPUTILS_Subyacente_DescargarPersistente(usbProv, AT_SIGNATURE, CSPUTILS_NO_SAVE_KEYS, FALSE);
		CSPUTILS_Subyacente_DescargarPersistente(usbProv, AT_KEYEXCHANGE, CSPUTILS_NO_SAVE_KEYS, FALSE);
		CryptDestroyKey(*(usbKey->hKey));

		if ( !ok )
			SetLastError(err);
	}

	if ( ok ) {

		
		*phHash = (HCRYPTHASH) usbHash;

#ifndef _DEBUG
		usbProv->hashObjects[usbHash] = usbHash;
#endif

		
	}


finCPCreateHash:

	DWORD lastErr = GetLastError();

	if ( ! ok ) 
		if ( usbHash )
			CSPUTILS_HANDLE_Liberar(usbHash);

	

	LOG_EndFunc(1, ok);
	SetLastError(lastErr);

	return ok;

}

/****************************************************************************/

BOOL WINAPI CPHashData(IN HCRYPTPROV hpProv, IN HCRYPTHASH hHash, IN CONST BYTE *pbData, IN DWORD cbDataLen, IN DWORD dwFlags)
{
	HUSBCRYPTPROV *usbHandle;
	HUSBCRYPTHASH *usbHash;
	BOOL ok = FALSE;

	if ( ! CSPUTILS_Ini () )
		return FALSE;

	LOG_BeginFunc(1);

	if ( ! hpProv ) {
		ok = FALSE;
		SetLastError(NTE_BAD_UID);
		goto finCPHashData;
	}

	if ( ! hHash ) {
		ok = FALSE;
		SetLastError(NTE_BAD_HASH);
		goto finCPHashData;
	}

	/* El CSP Test Suite indica que:
	 *      "CryptHashData CRYPT_USERDATA should fail when dwDataLen is not zero (optional)"
	 * Nosotros lo tenemos en cuenta pq el Enhanced no lo hace
	 */

	if ( dwFlags == CRYPT_USERDATA && cbDataLen != 0 ) {
		ok = FALSE;
		SetLastError(NTE_BAD_LEN);
		goto finCPHashData;
	}
	
	usbHandle = (HUSBCRYPTPROV *) hpProv;
	usbHash   = (HUSBCRYPTHASH *) hHash;

    ok = CryptHashData(*(usbHash->hHash), pbData, cbDataLen, dwFlags);

finCPHashData:

	DWORD lastErr = GetLastError();


	LOG_EndFunc(1, ok);
	SetLastError(lastErr);

	return ok;
}

/****************************************************************************/

BOOL WINAPI CPHashSessionKey(IN HCRYPTPROV hpProv, IN HCRYPTHASH hHash, IN HCRYPTKEY hKey, IN DWORD dwFlags)
{
	HUSBCRYPTPROV *usbHandle;
	HUSBCRYPTHASH *usbHash;
	HUSBCRYPTKEY *usbKey;

	BOOL ok = FALSE;;

	if ( ! CSPUTILS_Ini () )
		return FALSE;

	LOG_BeginFunc(1);

	if ( ! hpProv ) {
		ok = FALSE;
		SetLastError(NTE_BAD_UID);
		goto finCPHashSessionKey;
	}

	if ( ! hHash ) {
		SetLastError(NTE_BAD_HASH);
		ok = FALSE;
		goto finCPHashSessionKey;
	}

	if ( ! hKey ) {
		SetLastError(NTE_BAD_KEY);
		ok = FALSE;
		goto finCPHashSessionKey;
	}

	usbHandle = (HUSBCRYPTPROV *) hpProv;
	usbHash   = (HUSBCRYPTHASH *) hHash;
	usbKey    = (HUSBCRYPTKEY *) hKey;

	ok = CryptHashSessionKey(*(usbHash->hHash), *(usbKey->hKey), dwFlags);

finCPHashSessionKey:

	DWORD lastErr = GetLastError();

	LOG_EndFunc(1, ok);

	SetLastError(lastErr);

    return ok;
}

/****************************************************************************/

BOOL WINAPI CPSignHash(IN HCRYPTPROV hpProv, IN HCRYPTHASH hHash, IN DWORD dwKeySpec, IN LPCWSTR szDescription, IN DWORD dwFlags, OUT LPBYTE pbSignature, IN OUT LPDWORD pcbSigLen)
{
	HUSBCRYPTHASH *usbHash;
	HUSBCRYPTPROV *usbProv;
	HCRYPTKEY aux;
	BOOL descargar = FALSE;
	BOOL ok = TRUE;

	if ( ! CSPUTILS_Ini () )
		return FALSE;

	LOG_BeginFunc(1);

	if ( ! hHash ) {
		SetLastError(NTE_BAD_HASH);
		ok = FALSE;
		goto finCPSignHash;
	}

	if ( ! hpProv ) {
		SetLastError(NTE_BAD_UID);
		ok = FALSE;
		goto finCPSignHash;
	}

	/*
	 * PROBLEMA. Resulta que ahora cuando llamo a CryptSignHash no se le pasa
	 *			 como argumento un HKEY sino la especificaci�n de un tipo de
	 *			 llave que, supongo, tomar� del mismo key container que el
	 *			 propio hash. Esto es un problema ya que los objetos hash est�n
	 *			 en el container temporal. La soluci�n pasar�a por importar
	 *			 la llave privada en el temporal, pero luego no tengo forma
	 *			 de borrarla si no es machacando todo el container. Pero esto
	 *			 provocar�a la p�rdida de todos los objetos temporales. En
	 *			 especial de los objetos hash (las llaves de sesi�n podr�an
	 *			 exportarse a otro key container). No tengo ninguna forma de
	 *			 copiar un objeto hash a otro key container.
	 *
	 * SOLUCI�N. Bien, esto es lo que se me ocurre. Cargo el par de llaves en
	 *			 cuesti�n. Una vez he acabado de realizar la operaci�n, lo que
	 *		     hago es machacar el par de llaves poniendo otro par a pi�o fijo.
	 */
	
	usbHash   = (HUSBCRYPTHASH *) hHash;
	usbProv   = (HUSBCRYPTPROV *) hpProv;

	LOG_Msg(10, "AQUI X");
	if ( ! CSPUTILS_Subyacente_CargarPersistente(usbProv) ) {
		ok = FALSE;
		goto finCPSignHash;
	}
	LOG_Msg(10, "AQUI X2");

	if ( dwKeySpec == AT_SIGNATURE ) {
		LOG_Msg(10, "KeySpec: AT_SIGNATURE");
	} else {
		LOG_Msg(10, "KeySpec: AT_KEYEXCHANGE");
	}

	LOG_Msg(10, "AQUI1");
	if ( ! CryptSignHash(*(usbHash->hHash), dwKeySpec, (const char *)szDescription, dwFlags, pbSignature, pcbSigLen) ) {
		if ( dwKeySpec == AT_SIGNATURE){
			dwKeySpec = AT_KEYEXCHANGE;
		}
		/* Paul: 02/08/2011 If the type is AT_SIGNATURE, try also with AT_KEYEXCHANGE as far as it is also valid to sign 
		 *       purposes.
		 *       This solves the problem with the Java SunMSCapi provider. 
		 */
		if ( ! CryptSignHash(*(usbHash->hHash), dwKeySpec, (const char *)szDescription, dwFlags, pbSignature, pcbSigLen) ){
		  DWORD err = GetLastError();
		  switch (err) {
		  case ERROR_INVALID_HANDLE:
	  		  LOG_Msg(10, "ERROR_INVALID_HANDLE");
			  break;
		  case ERROR_INVALID_PARAMETER:
			  LOG_Msg(10, "ERROR_INVALID_PARAMETER");
			  break;
		  case ERROR_MORE_DATA:
			  LOG_Msg(10, "ERROR_MORE_DATA");
			  break;
		  case NTE_BAD_ALGID:
			  LOG_Msg(10, "NTE_BAD_ALGID");
			  break;
		  case NTE_BAD_FLAGS:
			  LOG_Msg(10, "NTE_BAD_FLAGS");
			  break;
		  case NTE_BAD_HASH:
			  LOG_Msg(10, "NTE_BAD_HASH");
			  break;
		  case NTE_BAD_UID:
			  LOG_Msg(10, "NTE_BAD_UID");
			  break;
		  case NTE_NO_KEY:
			  LOG_Msg(10, "NTE_NO_KEY");
			  break;
		  case NTE_NO_MEMORY:
			  LOG_Msg(10, "NTE_NO_MEMORY");
			  break;
		  default:
			  LOG_Debug(10, "El error: 0x%x", err);

		  }

		  ok = FALSE;
		  goto finCPSignHash;
		}
	}
	LOG_Msg(10, "Aquo 2");

finCPSignHash:

	DWORD lastErr = GetLastError();

	if ( dwKeySpec == AT_SIGNATURE ) {
		if ( CryptImportKey(*(usbProv->hpProv), privSig, 1172, NULL, 0, &aux) )
			CryptDestroyKey(aux);
	} else
		if ( CryptImportKey(*(usbProv->hpProv), privEx, 1172, NULL, 0, &aux) )
			CryptDestroyKey(aux);


	LOG_EndFunc(1, ok);
	SetLastError(lastErr);

    return ok;
}

/****************************************************************************/

BOOL WINAPI CPDestroyHash(IN HCRYPTPROV hpProv, IN HCRYPTHASH hHash)
{
	HUSBCRYPTHASH *usbHash;
	HUSBCRYPTPROV *usbProv;

	BOOL ok = TRUE;

	if ( ! CSPUTILS_Ini () )
		return FALSE;

	LOG_BeginFunc(1);

	if ( ! hpProv ) {
		SetLastError(NTE_BAD_UID);
		ok = FALSE;
		goto finCPDestroyHash;
	}

	if ( ! hHash ) {
		SetLastError(NTE_BAD_HASH);
		ok = FALSE;
		goto finCPDestroyHash;
	}

	usbProv   = (HUSBCRYPTPROV *) hpProv;
	usbHash   = (HUSBCRYPTHASH *) hHash;

    if ( ! CryptDestroyHash(*(usbHash->hHash)) ) {
		ok = FALSE;
		goto finCPDestroyHash;
	}

	usbProv->hashObjects.erase(usbHash);

	CSPUTILS_HANDLE_Liberar(usbHash);

finCPDestroyHash:

	DWORD lastErr = GetLastError();


	LOG_EndFunc(1, ok);
	SetLastError(lastErr);

	return ok;
}

/****************************************************************************/

BOOL WINAPI CPVerifySignature(IN HCRYPTPROV hpProv, IN HCRYPTHASH hHash, IN CONST BYTE *pbSignature, IN DWORD cbSigLen, IN HCRYPTKEY hPubKey, IN LPCWSTR szDescription, IN DWORD dwFlags)
{
	HUSBCRYPTPROV *usbHandle;
	HUSBCRYPTHASH *usbHash;
	HUSBCRYPTKEY *usbKey;

	BOOL ok = TRUE;

	if ( ! CSPUTILS_Ini () )
		return FALSE;

	LOG_BeginFunc(1);

	if ( ! hpProv ) {
		SetLastError(NTE_BAD_UID);
		ok = FALSE;
		goto finCPVerifySignature;
	}

	if ( ! hHash ) {
		SetLastError(NTE_BAD_HASH);
		ok = FALSE;
		goto finCPVerifySignature;
	}

	if ( ! hPubKey ) {
		SetLastError(NTE_BAD_KEY);
		ok = FALSE;
		goto finCPVerifySignature;
	}

	usbHandle = (HUSBCRYPTPROV *) hpProv;
	usbHash   = (HUSBCRYPTHASH *) hHash;
	usbKey    = (HUSBCRYPTKEY *) hPubKey;

	if ( (usbKey->tipo == PARLLAVES) && 
		 (usbKey->hProv->dwFlags != CRYPT_VERIFYCONTEXT)) {
		if ( !CSPUTILS_Subyacente_CargarPersistente(usbHandle) ) {
			ok = FALSE;
			goto finCPVerifySignature;
		}
	}
	

	if (!CryptVerifySignature(*(usbHash->hHash), pbSignature, cbSigLen, *(usbKey->hKey), (char *) szDescription, dwFlags) ) {
		DWORD err;
		err = GetLastError();

		if ( (usbKey->tipo == PARLLAVES) && 
			 (usbKey->hProv->dwFlags != CRYPT_VERIFYCONTEXT)) {
			CSPUTILS_Subyacente_DescargarPersistente(usbHandle, AT_SIGNATURE, CSPUTILS_NO_SAVE_KEYS, FALSE);
			CSPUTILS_Subyacente_DescargarPersistente(usbHandle, AT_KEYEXCHANGE, CSPUTILS_NO_SAVE_KEYS, FALSE);

		}	
		
		SetLastError(err);
		ok = FALSE;
		goto finCPVerifySignature;
	}

	if ( (usbKey->tipo == PARLLAVES) && 
		(usbKey->hProv->dwFlags != CRYPT_VERIFYCONTEXT)) {
		CSPUTILS_Subyacente_DescargarPersistente(usbHandle, AT_SIGNATURE, CSPUTILS_NO_SAVE_KEYS, FALSE);
		CSPUTILS_Subyacente_DescargarPersistente(usbHandle, AT_KEYEXCHANGE, CSPUTILS_NO_SAVE_KEYS, FALSE);
	}


	
finCPVerifySignature:
    
	DWORD lastErr = GetLastError();


	LOG_EndFunc(1, ok);
	SetLastError(lastErr);

	return ok;
}

/****************************************************************************/

BOOL WINAPI CPGenRandom(IN HCRYPTPROV hpProv, IN DWORD cbLen, OUT LPBYTE pbBuffer)
{
	HUSBCRYPTPROV *usbHandle;
	BOOL ok = TRUE;

	if ( ! CSPUTILS_Ini () )
		return FALSE;

	LOG_BeginFunc(1);
	
	if ( ! hpProv ) {
		SetLastError(NTE_BAD_UID);
		ok = FALSE;
		goto finCPGenRandom;
	}
		
	usbHandle = (HUSBCRYPTPROV *) hpProv;

	ok = CryptGenRandom(*(usbHandle->hpProv), cbLen, pbBuffer);

finCPGenRandom:

	DWORD lastErr = GetLastError();


	LOG_EndFunc(1, ok);
	SetLastError(lastErr);
		
    return ok;
}

/****************************************************************************/

BOOL WINAPI CPGetUserKey(IN HCRYPTPROV hpProv, IN DWORD dwKeySpec, OUT HCRYPTKEY *phUserKey)
{
	HUSBCRYPTKEY *usbKey;
	HUSBCRYPTPROV *usbProv;
	BOOL bExists;

	BOOL ok = TRUE;

	if ( ! CSPUTILS_Ini () )
		return FALSE;

	LOG_BeginFunc(1);

	if ( ! hpProv ) {
		SetLastError(NTE_BAD_UID);
		ok = FALSE;
		goto finCPGetUserKey;
	}

	if ( (dwKeySpec != AT_KEYEXCHANGE) && (dwKeySpec != AT_SIGNATURE) ) {
		SetLastError(NTE_BAD_KEY);
		ok = FALSE;
		goto finCPGetUserKey;
	}

	usbProv = (HUSBCRYPTPROV *) hpProv;

	if ( ! CSPUTILS_KeyContainer_KeyExists(usbProv, dwKeySpec, &bExists ) ) {
		ok = FALSE;
		goto finCPGetUserKey;
	}

	if ( ! bExists ) {
		SetLastError(NTE_NO_KEY);
		ok = FALSE;
		goto finCPGetUserKey;
	}

	if ( ! CSPUTILS_HANDLE_Nuevo(&usbKey) ) {
		ok = FALSE;
		goto finCPGetUserKey;
	}

	usbKey->algoritmo = dwKeySpec;
	usbKey->tipo      = PARLLAVES;
	usbKey->hProv     = usbProv;

	*phUserKey = (HCRYPTKEY) usbKey;

finCPGetUserKey:

	DWORD lastErr = GetLastError();

	LOG_EndFunc(1, ok);
	SetLastError(lastErr);

    return ok;
}





/*++

DllUnregisterServer:

    This service removes the registry entries associated with this CSP.

Arguments:

    None

Return Value:

    Status code as an HRESULT.

Author:

    Doug Barlow (dbarlow) 3/11/1998

--*/

STDAPI
DllUnregisterServer(
    void)
{
    LONG nStatus;
    DWORD dwDisp;
    HRESULT hReturnStatus = NO_ERROR;
    HKEY hpProviders = NULL;

#ifdef _AFXDLL
    AFX_MANAGE_STATE(AfxGetStaticModuleState());
#endif

	if ( g_hThrCacheEvent )
		SetEvent(g_hThrCacheEvent);
	if ( g_hThrCacheEndEvent )
		SetEvent(g_hThrCacheEndEvent);


    //
    // Delete the Registry key for this CSP.
    //

    nStatus = RegCreateKeyEx(
                    HKEY_LOCAL_MACHINE,
                    TEXT("SOFTWARE\\Microsoft\\Cryptography\\Defaults\\Provider"),
                    0,
                    TEXT(""),
                    REG_OPTION_NON_VOLATILE,
                    KEY_ALL_ACCESS,
                    NULL,
                    &hpProviders,
                    &dwDisp);

    if (ERROR_SUCCESS == nStatus)
    {
        RegDeleteKey(hpProviders, l_szProviderName);
        RegCloseKey(hpProviders);
        hpProviders = NULL;
    }

    //
    // ?vendor?
    // Delete vendor specific registry entries.
    //



    //
    // All done!
    //

    return hReturnStatus;
}


/*++

DllRegisterServer:

    This function installs the proper registry entries to enable this CSP.

Arguments:

    None

Return Value:

    Status code as an HRESULT.

Author:

    Doug Barlow (dbarlow) 3/11/1998

--*/

STDAPI
DllRegisterServer(
    void)
{
    TCHAR szModulePath[MAX_PATH];
    BYTE pbSignature[136];  // Room for a 1024 bit signature, with padding.
    OSVERSIONINFO osVer;
    LPTSTR szFileName, szFileExt;
    HINSTANCE hThisDll;
    HRSRC hSigResource;
    DWORD dwStatus;
    LONG nStatus;
    BOOL fStatus;
    DWORD dwDisp;
    DWORD dwIndex;
    DWORD dwSigLength;
    HRESULT hReturnStatus = NO_ERROR;
    HKEY hpProviders = NULL;
    HKEY hMyCsp = NULL;
    HKEY hCalais = NULL;
    HKEY hVendor = NULL;
    BOOL fSignatureFound = FALSE;
    HANDLE hSigFile = INVALID_HANDLE_VALUE;

#ifdef _AFXDLL
    AFX_MANAGE_STATE(AfxGetStaticModuleState());
#endif

	if ( g_hThrCacheEvent )
		SetEvent(g_hThrCacheEvent);
	if ( g_hThrCacheEndEvent )
		SetEvent(g_hThrCacheEndEvent);



    //
    // Figure out the file name and path.
    //

    hThisDll = GetInstanceHandle();
    if (NULL == hThisDll)
    {
        hReturnStatus = HRESULT_FROM_WIN32(ERROR_INVALID_HANDLE);
        goto ErrorExit;
    }

    dwStatus = GetModuleFileName(
                    hThisDll,
                    szModulePath,
                    sizeof(szModulePath) / sizeof(TCHAR));
    if (0 == dwStatus)
    {
        hReturnStatus = HRESULT_FROM_WIN32(GetLastError());
        goto ErrorExit;
    }

    szFileName = _tcsrchr(szModulePath, TEXT('\\'));
    if (NULL == szFileName)
        szFileName = szModulePath;
    else
        szFileName += 1;
    szFileExt = _tcsrchr(szFileName, TEXT('.'));
    if (NULL == szFileExt)
    {
        hReturnStatus = HRESULT_FROM_WIN32(ERROR_INVALID_NAME);
        goto ErrorExit;
    }
    else
        szFileExt += 1;


    //
    // Create the Registry key for this CSP.
    //

    nStatus = RegCreateKeyEx(
                    HKEY_LOCAL_MACHINE,
                    TEXT("SOFTWARE\\Microsoft\\Cryptography\\Defaults\\Provider"),
                    0,
                    TEXT(""),
                    REG_OPTION_NON_VOLATILE,
                    KEY_ALL_ACCESS,
                    NULL,
                    &hpProviders,
                    &dwDisp);
    if (ERROR_SUCCESS != nStatus)
    {
        hReturnStatus = HRESULT_FROM_WIN32(nStatus);
        goto ErrorExit;
    }
    nStatus = RegCreateKeyEx(
                    hpProviders,
                    l_szProviderName,
                    0,
                    TEXT(""),
                    REG_OPTION_NON_VOLATILE,
                    KEY_ALL_ACCESS,
                    NULL,
                    &hMyCsp,
                    &dwDisp);
    if (ERROR_SUCCESS != nStatus)
    {
        hReturnStatus = HRESULT_FROM_WIN32(nStatus);
        goto ErrorExit;
    }
    nStatus = RegCloseKey(hpProviders);
    hpProviders = NULL;
    if (ERROR_SUCCESS != nStatus)
    {
        hReturnStatus = HRESULT_FROM_WIN32(nStatus);
        goto ErrorExit;
    }


    //
    // Install the trivial registry values.
    //

    nStatus = RegSetValueEx(
                    hMyCsp,
                    TEXT("Image Path"),
                    0,
                    REG_SZ,
                    (LPBYTE)szModulePath,
                    (_tcslen(szModulePath) + 1) * sizeof(TCHAR));
    if (ERROR_SUCCESS != nStatus)
    {
        hReturnStatus = HRESULT_FROM_WIN32(nStatus);
        goto ErrorExit;
    }

    nStatus = RegSetValueEx(
                    hMyCsp,
                    TEXT("Type"),
                    0,
                    REG_DWORD,
                    (LPBYTE)&l_dwCspType,
                    sizeof(DWORD));
    if (ERROR_SUCCESS != nStatus)
    {
        hReturnStatus = HRESULT_FROM_WIN32(nStatus);
        goto ErrorExit;
    }


    //
    // See if we're self-signed.  On NT5, CSP images can carry their own
    // signatures.
    //

    hSigResource = FindResource(
                        hThisDll,
                        MAKEINTRESOURCE(CRYPT_SIG_RESOURCE_NUMBER),
                        RT_RCDATA);


    //
    // Install the file signature.
    //

    ZeroMemory(&osVer, sizeof(OSVERSIONINFO));
    osVer.dwOSVersionInfoSize = sizeof(OSVERSIONINFO);
    fStatus = GetVersionEx(&osVer);
    // ?BUGBUG? -- This works on Windows Millenium, too.
    if (fStatus
        && (VER_PLATFORM_WIN32_NT == osVer.dwPlatformId)
        && (5 <= osVer.dwMajorVersion)
        && (NULL != hSigResource))
    {

        //
        // Signature in file flag is sufficient.
        //

        dwStatus = 0;
        nStatus = RegSetValueEx(
                        hMyCsp,
                        TEXT("SigInFile"),
                        0,
                        REG_DWORD,
                        (LPBYTE)&dwStatus,
                        sizeof(DWORD));
        if (ERROR_SUCCESS != nStatus)
        {
            hReturnStatus = HRESULT_FROM_WIN32(nStatus);
            goto ErrorExit;
        }
    }
    else
    {

        //
        // We have to install a signature entry.
        // Try various techniques until one works.
        //

        for (dwIndex = 0; !fSignatureFound; dwIndex += 1)
        {
            switch (dwIndex)
            {

            //
            // Look for an external *.sig file and load that into the registry.
            //

            case 0:
                _tcscpy(szFileExt, TEXT("sig"));
                hSigFile = CreateFile(
                                szModulePath,
                                GENERIC_READ,
                                FILE_SHARE_READ,
                                NULL,
                                OPEN_EXISTING,
                                FILE_ATTRIBUTE_NORMAL,
                                NULL);
                if (INVALID_HANDLE_VALUE == hSigFile)
                    continue;
                dwSigLength = GetFileSize(hSigFile, NULL);
                if ((dwSigLength > sizeof(pbSignature))
                    || (dwSigLength < 72))      // Accept a 512-bit signature
                {
                    hReturnStatus = NTE_BAD_SIGNATURE;
                    goto ErrorExit;
                }

                fStatus = ReadFile(
                                hSigFile,
                                pbSignature,
                                sizeof(pbSignature),
                                &dwSigLength,
                                NULL);
                if (!fStatus)
                {
                    hReturnStatus = HRESULT_FROM_WIN32(GetLastError());
                    goto ErrorExit;
                }
                fStatus = CloseHandle(hSigFile);
                hSigFile = NULL;
                if (!fStatus)
                {
                    hReturnStatus = HRESULT_FROM_WIN32(GetLastError());
                    goto ErrorExit;
                }
                fSignatureFound = TRUE;
                break;


            //
            // Other cases may be added in the future.
            //

            default:
                hReturnStatus = NTE_BAD_SIGNATURE;
                goto ErrorExit;
            }

            if (fSignatureFound)
            {
                for (dwIndex = 0; dwIndex < dwSigLength; dwIndex += 1)
                {
                    if (0 != pbSignature[dwIndex])
                        break;
                }
                if (dwIndex >= dwSigLength)
                    fSignatureFound = FALSE;
            }
        }


        //
        // We've found a signature somewhere!  Install it.
        //

        nStatus = RegSetValueEx(
                        hMyCsp,
                        TEXT("Signature"),
                        0,
                        REG_BINARY,
                        pbSignature,
                        dwSigLength);
        if (ERROR_SUCCESS != nStatus)
        {
            hReturnStatus = HRESULT_FROM_WIN32(nStatus);
            goto ErrorExit;
        }
    }

    nStatus = RegCloseKey(hMyCsp);
    hMyCsp = NULL;
    if (ERROR_SUCCESS != nStatus)
    {
        hReturnStatus = HRESULT_FROM_WIN32(nStatus);
        goto ErrorExit;
    }


    //
    // ?vendor?
    // Add any additional initialization required here.
    //

    //
    // All done!
    //

    return hReturnStatus;


    //
    // An error was detected.  Clean up any outstanding resources and
    // return the error.
    //

    ErrorExit:

    if (NULL != hVendor)
        RegCloseKey(hVendor);
    if (INVALID_HANDLE_VALUE != hSigFile)
        CloseHandle(hSigFile);
    if (NULL != hMyCsp)
        RegCloseKey(hMyCsp);
    if (NULL != hpProviders)
        RegCloseKey(hpProviders);
    DllUnregisterServer();
    return hReturnStatus;
}


/*++

GetInstanceHandle:

    This routine is CSP dependant.  It returns the DLL instance handle.  This
    is typically provided by the DllMain routine and stored in a global
    location.

Arguments:

    None

Return Value:

    The DLL Instance handle provided to the DLL when DllMain was called.

Author:

    Doug Barlow (dbarlow) 3/11/1998

--*/

//extern "C" HINSTANCE g_hModule;

static HINSTANCE
GetInstanceHandle(
    void)
{
//#ifdef _AFXDLL*/
//    return AfxGetInstanceHandle();
//#else
	
    //  ?vendor?
    // Make sure this returns your DLL instance handle.
    return g_hModule;

//#endif
}

