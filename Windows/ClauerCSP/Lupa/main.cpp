#include <windows.h>

#include <iostream>
#include <fstream>
#include <sstream>

#include <LIBRT/LIBRT.h>


using namespace std;




void main ()
{
	ofstream of;
	USBCERTS_HANDLE h;
	int nDevices;
	unsigned char *dispositivos;
	char *passwd;
	string pin;
	unsigned long *hBloques,i;
	BYTE *bloques;
	unsigned long nBloques;


	LIBRT_Ini();

	if ( LIBRT_ListarDispositivos(&nDevices, &dispositivos) != 0 ) {
		cerr << "ERROR listando dispositivos" << endl;
		return;
	}

	if ( nDevices == 0 ) {
		cout << "NO HAY LLAVEROS EN EL SISTEMA" << endl;
		return;
	}	

	if ( LIBRT_IniciarDispositivo(dispositivos[0], "00359570", &h) != 0 ) {
		cerr << "ERROR iniciando dispositivo" << endl;
		return;
	}

	if ( LIBRT_LeerTodosBloquesOcupados(&h, NULL, NULL, &nBloques) != 0 ) {
		cerr << "ERROR leyendo bloques ocupados 1" << endl;
		return;
	}

	bloques = new BYTE[TAM_BLOQUE*nBloques];
	hBloques = new unsigned long[nBloques];

	if ( LIBRT_LeerTodosBloquesOcupados(&h, (long *)hBloques, bloques, &nBloques) != 0 ) {
		cerr << "ERROR leyendo bloques ocupados 1" << endl;
		return;
	}

	of.open("c:\\lupa.log", ofstream::trunc);


	for ( i = 0 ; i < nBloques ; i++ ) {

		of << "BLOQUE: " << hBloques[i] << endl;
		of << "-----------------------------------------------" << endl;
		of << "\tEstado: " << BLOQUE_Estado_Str(bloques+i*TAM_BLOQUE) << endl;
		of << "\tTipo: " << BLOQUE_Tipo_Str(bloques+i*TAM_BLOQUE) << endl;
		BLOQUE_Print(bloques+i*TAM_BLOQUE, stdout);

	}
	
	of.close();

}

