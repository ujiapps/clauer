//#include "stdafx.h"
#include "CSPUtils.h"

#include <time.h>
#include <map>
#include <string>


#include "iu.hpp"
#include "CSPIDIOMA.h"
#include <LIBRT/LIBRT.h>
#include <CAPI_PKCS5/CAPI_PKCS5.h>
#include "clui_csp/clui.h"
#include <log/log.h>
#include <b64/b64.h>
#include "ThrCacheCleaner.h"

#include <WinCrypt.h>


extern HINSTANCE g_hModule;
extern BYTE *privEx;
extern BYTE *privSig;
extern TCHAR g_szUnderlayingCSP[];

extern "C" {
extern int g_bClauerLOG;
}


/* Cach� de passwords 
 */

using namespace std;


/* Gesti�n de CLUI.dll
 */

extern HANDLE g_hMutexPassCache;
extern CRITICAL_SECTION g_csInitialize;
extern BOOL g_bInitialized;
extern HANDLE g_hThrCache;

extern map<string, PASS_CACHE_INFO *> g_mapPassCache;

BOOL CSPUTILS_Ini ( void )
{
	BOOL ret = TRUE;

	EnterCriticalSection(&g_csInitialize);

	if ( g_bInitialized ) {
		LeaveCriticalSection(&g_csInitialize);
		return TRUE;
	}
	g_bClauerLOG = 1;
	LOG_Ini(LOG_WHERE_FILE, 10, LOG_FILE);

	if ( LIBRT_Ini() != 0 ) {
		LeaveCriticalSection(&g_csInitialize);
		return FALSE;
	}

	if ( ! LIBRT_HayRuntime() ) {
		LeaveCriticalSection(&g_csInitialize);
		return FALSE;
	}

	if ( ! CSP_IDIOMA_Cargar() ) {
		LeaveCriticalSection(&g_csInitialize);
		return FALSE;
	}

	if ( ! CSPUTILS_PASSPOOL_Init() ) {
		CSP_IDIOMA_Descargar();
		LeaveCriticalSection(&g_csInitialize);
		return FALSE;
	}
	if ( ! (g_hThrCache = CreateThread(NULL, 0, ThrCacheCleaner, NULL, 0, NULL)) ) {
		return FALSE;
	}
	g_bInitialized = TRUE;
		
	LeaveCriticalSection(&g_csInitialize);
	return TRUE;
}





/*! \brief Callback que se le pasa a la LIBRT para realizar las reconexiones en caso de vencimiento
 *         de timeout y se necesite reconectar.
 *
 * \param dispositivos
 *		  ENTRADA. Los dispositivos insertados en el sistema.
 *
 * \param nDispositivos
 *		  ENTRADA. El n�mero de dispositivos presentes en el sistema
 *
 * \param appData
 *		  ENTRADA. Informaci�n de aplicaci�n pasada mediante la funci�n LIBRT_Set_AppData().
 *
 * \param dispSel
 *		  SALIDA. Dispositivo seleccionado.
 *
 */

int CSPUTILS_LIBRT_IU_callback (USBCERTS_HANDLE *hDispositivo, BYTE *dispositivos[MAX_DEVICES], int nDispositivos, void *appData, char pin[100], int *dispSel)
{

	HUSBCRYPTPROV *hProv = (HUSBCRYPTPROV *) appData;
	int ret;
	PIN miPIN;
	int miDispSel;

	if ( ! dispositivos )
		return -1;

	if ( ! appData )
		return -1;

	if ( ! dispSel )
		return -1;

	if ( nDispositivos == 0 ) {

		if ( CSP_IDIOMA_Cargado() ) {
			if ( MessageBox(hProv->hWindow, CSP_IDIOMA_Get(CSP_IDIOMA_NOCLAUER), CSP_IDIOMA_Get(CSP_IDIOMA_PROJECTE_CLAUER), MB_OKCANCEL) == IDCANCEL ) {
				return -1;
			} 
		} else {
			if ( MessageBox(hProv->hWindow, CSP_IDIOMA_Get(CSP_IDIOMA_NOCLAUER), CSP_IDIOMA_Get(CSP_IDIOMA_PROJECTE_CLAUER), MB_OKCANCEL) == IDCANCEL ) {
				return -1;
			} 
		}

	}

	if ( hDispositivo->auth ) {

		ret = IU_PedirPIN (g_hModule, hProv->hWindow, dispositivos, nDispositivos, miDispSel, IU_GLOBAL_PIN, miPIN);

		if ( ret != 0 )
			return ret;

		if ( strlen(miPIN) > 99 ) {
			SecureZeroMemory(miPIN, strlen(miPIN)+1);
			delete [] miPIN;
			miPIN = NULL;
			return -1;
		}
	
		memcpy(pin, miPIN, strlen(miPIN+1));
		SecureZeroMemory(miPIN, strlen(miPIN)+1);
		delete [] miPIN;
		miPIN = NULL;
	}

	*dispSel = 0;

	return 0;
}


/*******************************************************************************************/
/*! \brief Pide el pin en caso de que el handle haya sido adquirido en modo no autenticado.
 *
 * Pide el pin en caso de que el handle haya sido adquirido en modo no autenticado.
 *
 * \param hProv
 *		  Handle creado en CPAcquireContext
 *
 * \param hDll
 *		  Handle de la DLL
 *
 * \retval -1
 *		   ERROR o se ha pulsado el bot�n Cancelar
 *
 * \retval 0
 *		   OK
 *
 */

int CSPUTILS_PedirPIN (HUSBCRYPTPROV *hProv, HINSTANCE hDll)
{
	int nDispositivos, ret, i;
	unsigned char *dispositivos[MAX_DEVICES];
	USBCERTS_HANDLE *hUsb = NULL, *aux = NULL;
	char pin[CLUI_CSP_MAX_PASS_LEN];
	BOOL soloDigitos = TRUE;

	memset(dispositivos, 0, sizeof(BYTE *) * MAX_DEVICES);


	LOG_BeginFunc(1);

	/* Si el handle ya era autenticado, no hacemos nada
	 */

	if ( hProv->auth ) {
		ret = 0;
		goto finCSPUTILS_PedirPIN;
	}

	/* Siempre utilizamos el primer dispositivo insertado. Si hay m�s de uno insertado pues no puedes
	 * interactuar con �l. Damos tres intentos al usuario
	 */

	if ( LIBRT_ListarDispositivos(&nDispositivos, dispositivos) != 0 ) {
		ret = -1;
		goto finCSPUTILS_PedirPIN;
	}

	if ( dispositivos == 0 ) {
		ret = -1;
		goto finCSPUTILS_PedirPIN;
	}

	DWORD intentos = 0;
	char *desc;

	desc = CSP_IDIOMA_Get(CSP_IDIOMA_DLGPASSWORD_DESCRIPCION);

	do {

		ret = hProv->lpCLUI_CSP_AskGlobalPassphrase(hProv->hWindow, 
									   FALSE, 
									   TRUE, 
									   FALSE, 
									   desc,
									   (char *) dispositivos[0],
									   pin);


		if ( ret == CLUI_CSP_ERR ) 
		{
			ret = -1;
			goto finCSPUTILS_PedirPIN;
		} else if ( ret == CLUI_CSP_CANCEL ) {
			/* Cancelar */
			ret = -1;
			goto finCSPUTILS_PedirPIN;
		}

		hUsb = new USBCERTS_HANDLE;

		if (!hUsb) {
			ret = -1;
			goto finCSPUTILS_PedirPIN;
		}

		if ( LIBRT_IniciarDispositivo(dispositivos[0], pin, hUsb) != 0 ) {

			/* Permitimos el fallo 3 veces. Asumimos que el mismo se debe
			 * a una contrase�a incorrecta.
			 */

			SecureZeroMemory(pin, sizeof pin);

			delete hUsb;
			hUsb = NULL;

			intentos++;
			if ( intentos == 3 ) {
				LIBRT_FinalizarDispositivo(hUsb);
				ret = -1;
				goto finCSPUTILS_PedirPIN;
			}
			desc = CSP_IDIOMA_Get(CSP_IDIOMA_DLGPASSWORD_DESC_INCORRECTA);

		} else {
			/* Inicializaci�n correcta --> PIN correcto. Continuamos
			 */
			break;
		}

	} while ( 1 );

	BOOL auxConectado;

	aux = hProv->hpDispositivo;
	hProv->hpDispositivo = hUsb;
	hProv->auth = TRUE;
	hProv->nodisp = FALSE;
	auxConectado = hProv->conectado;
	hProv->conectado = TRUE;

	/* En una llamada con CRYPT_NEWKEYSET no tenemos todav�a creado
	 * el container, as� es que no tiene sentido buscar los bloques
	 * de las llaves
	 */

	if ( hProv->hsContainer != -1 ) {
		if ( ! CSPUTILS_KeyContainer_GetKeyHandles (hProv,
												  &(hProv->hsExchange),
												  &(hProv->hsSignature))) 
		{
			LIBRT_FinalizarDispositivo(hUsb);
			hProv->hpDispositivo = aux;
			hProv->auth = FALSE;
			hProv->conectado = auxConectado;
			ret = -1;
			goto finCSPUTILS_PedirPIN;
		}
	}

	
	/* Finalizamos el handle (no autenticado) abierto previamente
	 */

	if ( auxConectado ) {
		if ( LIBRT_FinalizarDispositivo(aux) != 0 ) {
			ret = -1;
			goto finCSPUTILS_PedirPIN;
		}
	}

finCSPUTILS_PedirPIN:

	SecureZeroMemory(pin, sizeof pin);

	for ( i = 0 ; i < MAX_DEVICES && dispositivos[i] ; i++ ) {
		free(dispositivos[i]);
		dispositivos[i] = NULL;
	}

	if ( ret == -1 ) {
		if ( hUsb ) {
			delete hUsb;
			hUsb = NULL;
		}
	}

	return ret;

}


/*******************************************************************************************/
/*! \brief Indica si existe o no un key container.
 *
 * Indica si existe o no un key container.
 *
 * \param hProv
 *		  Handle creado en CPAcquireContext
 *
 * \param nombreKeyContainer
 *		  El nombre del key container que estamos buscando.
 *
 * \retval TRUE
 *		   El container existe
 *
 * \retval FALSE
 *		   El container NO existe o ERROR
 *
 */

BOOL CSPUTILS_KeyContainer_Existe (HUSBCRYPTPROV *hProv, 
								   LPTSTR nombreKeyContainer)
{
	BOOL encontrado = FALSE;
	BYTE bloque[TAM_BLOQUE];
	long nBloque;

	if ( ! CSPUTILS_CLOS_Connect(hProv, FALSE) ) {
		encontrado = FALSE;
		goto finCSPUTILS_KeyContainer_Existe;
	}

	if ( LIBRT_LeerTipoBloqueCrypto(hProv->hpDispositivo, BLOQUE_KEY_CONTAINERS, TRUE, bloque, &nBloque) != 0 ) {
		encontrado = FALSE;
		goto finCSPUTILS_KeyContainer_Existe;
	}

	while ( (nBloque != -1) && !encontrado) {

		if ( BLOQUE_KeyContainer_Buscar(bloque, nombreKeyContainer) )
			encontrado = TRUE;
		else {

			if ( LIBRT_LeerTipoBloqueCrypto(hProv->hpDispositivo, BLOQUE_KEY_CONTAINERS, FALSE, bloque, &nBloque) != 0 ) {
				encontrado = FALSE;
				goto finCSPUTILS_KeyContainer_Existe;
			}

		}
	}

finCSPUTILS_KeyContainer_Existe:


	if ( hProv->conectado ) 
		CSPUTILS_CLOS_Disconnect(hProv);	


	SecureZeroMemory(bloque, TAM_BLOQUE);

	return encontrado;

}

/*******************************************************************************************/
/*! \brief Inserta un nuevo key container en el dispositiv
 *
 * Inserta un nuevo key container en el dispositivo. No comprueba
 * la existencia del nuevo insertado.
 *
 * \param hDispositivo
 *		  El handle al dispositivo.
 *
 * \nombreKeyContainer
 *		  El nombre del key container en formato ASCII y null-terminated.
 *
 * \retval 1
 *		   ERROR
 *
 * \retval 0
 *		   OK
 *
 */

int CSPUTILS_KeyContainer_Insertar (HUSBCRYPTPROV *hProv,
							        LPTSTR nombreKeyContainer)
{
	long *hBloques, nBloques, n, numBloque;
	BYTE *bloques, bloqueContainer[TAM_BLOQUE];
	BOOL encontrado = FALSE;
	int ok;
	USBCERTS_HANDLE *hDispositivo = hProv->hpDispositivo;


	if ( ! CSPUTILS_CLOS_Connect(hProv, TRUE) )
		return 1;

	/* Leemos todos los bloques de tipo key container y buscamos
	 * el container en cuesti�n.
	 */

	ok = LIBRT_LeerTodosBloquesTipo (hDispositivo, 
					 				 BLOQUE_KEY_CONTAINERS, 
									 NULL, 
									 NULL, 
									 &nBloques);

	if ( ok != 0 ) {
		CSPUTILS_CLOS_Disconnect(hProv);
		return 1;
	}

	bloques  = new BYTE [nBloques*TAM_BLOQUE];

	if ( !bloques ) {
		CSPUTILS_CLOS_Disconnect(hProv);
		return 1;
	}

	hBloques = new long [nBloques];

	if ( !hBloques ) {
		CSPUTILS_CLOS_Disconnect(hProv);
		delete [] bloques;
		return 1;
	}

	ok = LIBRT_LeerTodosBloquesTipo (hDispositivo, 
										BLOQUE_KEY_CONTAINERS, 
										hBloques, 
										bloques, 
										&nBloques);
	if ( ok != 0 ) {
		CSPUTILS_CLOS_Disconnect(hProv);
		delete [] bloques;
		delete [] hBloques;
		return 1;
	}

	/*
	 * Buscamos un bloque donde insertar el key container
	 */

	n = 0;

	while ( !encontrado && (n< nBloques) ) {
	
		ok = BLOQUE_KeyContainer_Insertar (bloques+n*TAM_BLOQUE, nombreKeyContainer);

		if ( ok == ERR_BLOQUE_SIN_ESPACIO ) {
			++n;
		} else if ( ok == ERR_BLOQUE_NO ) {
			/*
			 * Se ha insertado correctamente
			 */
			encontrado = TRUE;
		} else  {

			/*
			 * Cualquier otro error provoca el abandono con error :-P
			 */
			CSPUTILS_CLOS_Disconnect(hProv);
			delete [] bloques;
			delete [] hBloques;
			return 1;
		}
	}

	if ( !encontrado ) {
	
		BLOQUE_Set_Claro(bloqueContainer);
		BLOQUE_KeyContainer_Nuevo(bloqueContainer);
		BLOQUE_KeyContainer_Insertar(bloqueContainer,(char *)nombreKeyContainer);

		ok = LIBRT_InsertarBloqueCrypto (hDispositivo, bloqueContainer, &numBloque);

		if ( ok != 0 ) {
			CSPUTILS_CLOS_Disconnect(hProv);
			delete [] bloques;
			delete [] hBloques;
			return 1;
		}

	} else {

		/*
		 * Hemos encontrado un bloque con espacio suficiente
		 */

		ok = LIBRT_EscribirBloqueCrypto (hDispositivo, hBloques[n], bloques+n*TAM_BLOQUE);

		if ( ok != 0 ) {
			CSPUTILS_CLOS_Disconnect(hProv);
			delete [] bloques;
			delete [] hBloques;
			return 1;
		}
	}

	delete [] bloques;
	delete [] hBloques;

	CSPUTILS_CLOS_Disconnect(hProv);

	return 0;
}





int CSPUTILS_KeyContainer_Borrar (HUSBCRYPTPROV *hUsbProv,
							      LPTSTR nombreKeyContainer)
{
	long *hBloques;
	BYTE *bloques;
	long nBloques, n;
	BOOL encontrado = FALSE;
	int ok;
	USBCERTS_HANDLE *hDispositivo = hUsbProv->hpDispositivo;


	if ( ! CSPUTILS_CLOS_Connect(hUsbProv, TRUE) )
		return FALSE;


	/*
	 * Leemos todos los bloques de tipo key container y buscamos
	 * el container en cuesti�n.
	 */

	ok = LIBRT_LeerTodosBloquesTipo (hDispositivo, 
			 						BLOQUE_KEY_CONTAINERS, 
										NULL, 
										NULL, 
										&nBloques);

	if ( ok != 0 ) {
		CSPUTILS_CLOS_Disconnect(hUsbProv);
		return 1;
	}


	bloques  = new BYTE [nBloques*TAM_BLOQUE];

	if ( !bloques ) {
		CSPUTILS_CLOS_Disconnect(hUsbProv);
		return 1;
	}


	hBloques = new long [nBloques];

	if ( !hBloques ) {
		CSPUTILS_CLOS_Disconnect(hUsbProv);
		delete [] bloques;
		return 1;
	}

	ok = LIBRT_LeerTodosBloquesTipo (hDispositivo, 
										BLOQUE_KEY_CONTAINERS, 
										hBloques, 
										bloques, 
										&nBloques);
	if ( ok != 0 ) {
		CSPUTILS_CLOS_Disconnect(hUsbProv);
		delete [] bloques;
		delete [] hBloques;
		return 1;
	}

	/*
	 * Buscamos el bloque donde se encuentra el key container y lo borramos
	 */

	n = 0;

	while ( !encontrado && (n< nBloques) ) {
		BYTE *kc;
		unsigned int numContainers;
		kc = BLOQUE_KeyContainer_Buscar (bloques+n*TAM_BLOQUE, nombreKeyContainer);
		if ( kc ) {
			long hExchange, hSignature;

			hExchange = BLOQUE_KeyContainer_GetUnidadExchange(kc);
			hSignature = BLOQUE_KeyContainer_GetUnidadSignature(kc);

			encontrado = TRUE;

			if ( hExchange != -1 ) {
				if ( LIBRT_BorrarBloqueCrypto(hDispositivo, hExchange) != 0 ) {
					CSPUTILS_CLOS_Disconnect(hUsbProv);
					delete [] bloques;
					delete [] hBloques;
					return 1;
				}
			}

			if ( hSignature != -1 ) {
				if ( LIBRT_BorrarBloqueCrypto(hDispositivo, hSignature) != 0) {
					CSPUTILS_CLOS_Disconnect(hUsbProv);
					delete [] bloques;
					delete [] hBloques;
					return 1;
				}
			}

			BLOQUE_KeyContainer_Borrar (bloques+n*TAM_BLOQUE, nombreKeyContainer);
			BLOQUE_KeyContainer_Enumerar(bloques+n*TAM_BLOQUE,NULL,&numContainers);

			if ( numContainers == 0)  {
				/* Si ya no hay containers, borro el bloque
				 */

				if ( LIBRT_BorrarBloqueCrypto(hDispositivo, hBloques[n]) != 0 ) {
					CSPUTILS_CLOS_Disconnect(hUsbProv);
					delete [] bloques;
					delete [] hBloques;
					return 1;
				}
			} else {
								
				if ( LIBRT_EscribirBloqueCrypto (hDispositivo, hBloques[n], bloques+n*TAM_BLOQUE) != 0 ) {
					CSPUTILS_CLOS_Disconnect(hUsbProv);
					delete [] bloques;
					delete [] hBloques;
					return 1;
				}
			}

		} else 
			++n;

	}

	delete [] bloques;
	delete [] hBloques;

	CSPUTILS_CLOS_Disconnect(hUsbProv);

	if ( encontrado )
		return 0;
	else
		return 1;

}




/*! \brief Devuelve un handle al bloque del key container.
 *
 * Devuelve un handle al bloque del key container indicado.
 *
 * \param hDispositivo
 *		  Handle al dispositivo usb
 *
 * \param nombreKeyContainer
 *		  El nombre del key container que buscamos.
 *
 * \retval El n�mero de bloque (handle) del bloque donde se encuentra el
 *		   key container.
 *
 * \retval -1
 *		   Si no se encontr� el key container.
 *
 * \retval -2
 *		   Se produjo un error
 *
 */

long CSPUTILS_KeyContainer_GetHandle (HUSBCRYPTPROV *hProv,
									  LPTSTR nombreKeyContainer)
{
	long *hBloques;
	BYTE *bloques, *aux;
	long nBloques, n;
	BOOL encontrado = FALSE;
	int ok;
	USBCERTS_HANDLE *hDispositivo = hProv->hpDispositivo;


	//

	if ( ! CSPUTILS_CLOS_Connect(hProv, FALSE) ) {
		//LOG_Mark();
		return -2;
	}

	/*
	 * Leemos todos los bloques de tipo key container y buscamos
	 * el container en cuesti�n.
	 */

	//LOG_Mark();

	ok = LIBRT_LeerTodosBloquesTipo (hDispositivo, 
						 			 BLOQUE_KEY_CONTAINERS, 
									 NULL, 
									 NULL, 
									 &nBloques);

	if ( ok != 0 ) { 
		CSPUTILS_CLOS_Disconnect(hProv);
		return -2;
	}

	/*
	 * Si no hay bloques de tipo key container
	 * entonces seguro que no existe el container :-)
	 */

	if ( nBloques == 0 ) {
		CSPUTILS_CLOS_Disconnect(hProv);
		return -1;
	}
	bloques  = new BYTE [nBloques*TAM_BLOQUE];
	if ( ! bloques ) {
		CSPUTILS_CLOS_Disconnect(hProv);
		return -2;
	}
	hBloques = new long [nBloques];

	if ( !hBloques ) {
		CSPUTILS_CLOS_Disconnect(hProv);
		delete [] bloques;
		return -2;
	}
	ok = LIBRT_LeerTodosBloquesTipo (hDispositivo, 
									 BLOQUE_KEY_CONTAINERS, 
									 hBloques, 
									 bloques, 
									 &nBloques);

	if ( ok != 0 ) {
		CSPUTILS_CLOS_Disconnect(hProv);
		delete [] bloques;
		delete [] hBloques;
		return -2;
	}

	/*
	 * Comienza la b�squeda
	 */

	n = 0;

	while ( ! encontrado && (n< nBloques) ) {

		aux = BLOQUE_KeyContainer_Buscar(bloques + n*TAM_BLOQUE, 
										 nombreKeyContainer);
		if ( aux ) 
			encontrado = TRUE;
		else
			++n;

	}


	if ( encontrado ) 
		n = hBloques[n];
	else {
		n = -1;
	}

	delete [] bloques;
	delete [] hBloques;

	CSPUTILS_CLOS_Disconnect(hProv);
	return n;
}



/*! \brief Busca los n�meros de bloque de las llaves de tipo key exchange y signature.
 *
 * Busca los n�meros de bloque de las llaves de tipo AT_KEYEXCHANGE y AT_SIGNATURE.
 * La b�squeda se realizar� mediante los identificadores de llave.
 *
 * \param hProv
 *        ENTRADA. Handle del csp.
 *
 * \param hExchange
 *	      SALIDA. N�mero de bloque en el que se encuentra el private key blob
 *		  correspondiente a la llave de tipo AT_KEYEXCHANGE.
 *
 * \param hSignature
 *		  SALIDA. N�mero de bloque en el que se encuentra el private key blob
 *		  correspondiente a la llave de tipo AT_SIGNATURE.
 *
 * \retval TRUE
 *		   Ok
 *
 * \retval FALSE
 *		   Error
 */

BOOL CSPUTILS_KeyContainer_GetKeyHandles (HUSBCRYPTPROV *hProv,
										  long *hExchange,
										  long *hSignature,
										  BOOL bFromConnect /* = FALSE */ )
{
	BYTE bloque[TAM_BLOQUE], idExchange[20], idSignature[20], *idBlob, noId[20], *aux;
	USBCERTS_HANDLE *hDispositivo = hProv->hpDispositivo;
	long nBloque;
	int i;
	BOOL encExchange = FALSE, encSignature = FALSE;

	//unsigned char mdauxBuff[61];                                                                                                                    
                                               
	LOG_BeginFunc(5);

	// Para evitar una llamada recursiva

	if ( ! bFromConnect ) {
		if ( ! CSPUTILS_CLOS_Connect(hProv, TRUE) ) {
			LOG_EndFunc(5, FALSE);
			return FALSE;
		}
	}

	*hExchange = -1;
	*hSignature = -1;

	memset(noId, 0, 20);

	/* Obtengo los IDs de las llaves del key container
	 */

	LOG_Debug(0, "hProv->hsContainer: %l", hProv->hsContainer); 
	if ( LIBRT_LeerBloqueCrypto(hDispositivo, hProv->hsContainer, bloque) != 0 ) {
		CSPUTILS_CLOS_Disconnect(hProv);
		LOG_EndFunc(5, FALSE);
		return FALSE;
	}
	aux = BLOQUE_KeyContainer_Get_ID_Exchange(bloque, hProv->szContainer);
	if  ( ! aux ) {
		CSPUTILS_CLOS_Disconnect(hProv);
		LOG_EndFunc(5, FALSE);
		return FALSE;
	}
	/*
	for(char i=0; i<20; i++){
	  snprintf( (char * )mdauxBuff+(2*i), 2, "%02x", aux[i] );
	}
	mdauxBuff[40]='\0';
	LOG_Debug(0, "Aux en Get_ID_Exchange: en hexadecimal es: %s", mdauxBuff);  
	*/
	memcpy(idExchange, aux, 20);

	aux = BLOQUE_KeyContainer_Get_ID_Signature(bloque, hProv->szContainer);
	if ( ! aux ) {
		CSPUTILS_CLOS_Disconnect(hProv);
		LOG_EndFunc(5, FALSE);
		return FALSE;
	}
	
	memcpy(idSignature, aux, 20);

	/* Si los identificadores son 0, entonces no tenemos llaves
	 * en el key container. Volvemos con *hExchange == -1 y
	 * *hSignature == -1   ---> No hemos encontrado nada.
	 */

	if ( memcmp(idExchange, noId, 20) == 0 ) {
		encExchange = TRUE;
	}

	if ( memcmp(idSignature, noId, 20) == 0 ) {
		encSignature = TRUE;
		LOG_Msg(1, "P.1");
	}

	if ( encExchange && encSignature ) {
		CSPUTILS_CLOS_Disconnect(hProv);
		LOG_EndFunc(5, FALSE);
		return TRUE;
	}

	/* Ahora buscamos los private key blobs con los IDs correspondientes a los
	 * containers. El bucle for es para buscar en los dos tipos de bloques
	 * que pueden contener blobs: PRIVKEY_BLOB y CIPHER_PRIVKEY_BLOB
	 */

	int tipoBloque = BLOQUE_PRIVKEY_BLOB;	

	for ( int i = 0 ; (i < 2) && !(encExchange && encSignature); i++ ) {

		if ( LIBRT_LeerTipoBloqueCrypto(hDispositivo, tipoBloque, TRUE, bloque, &nBloque) != 0) {
			CSPUTILS_CLOS_Disconnect(hProv);
			SecureZeroMemory(bloque, TAM_BLOQUE);
			LOG_EndFunc(5, FALSE);
			return FALSE;
		}

		LOG_Msg(1, "P.1.1");
		while ( nBloque != -1 ) {
			/*
			for(char i=0; i<27; i++){
					 snprintf( (char * )mdauxBuff+(2*i), 2, "%02x", bloque[i] );
			}
			mdauxBuff[54]='\0';
			LOG_Debug(0, "Bloque: en hexadecimal es: %s", mdauxBuff); 

			LOG_Msg(1, "P.1.2");
			*/
			if ( tipoBloque == BLOQUE_PRIVKEY_BLOB ){
				idBlob = BLOQUE_PRIVKEYBLOB_Get_Id(bloque);
				 /*
				 for(char i=0; i<20; i++){
					 snprintf( (char * )mdauxBuff+(2*i), 2, "%02x", idBlob[i] );
				 }
				 mdauxBuff[40]='\0';
				 LOG_Debug(0, "idBlob: en hexadecimal es: %s", mdauxBuff);    
				 */
			}
			else{
				idBlob = BLOQUE_CIPHPRIVKEYBLOB_Get_Id(bloque);
			}

			LOG_Msg(1, "P.1.3");
			if ( !idBlob ) {
				CSPUTILS_CLOS_Disconnect(hProv);
				SecureZeroMemory(bloque, TAM_BLOQUE);
				LOG_EndFunc(5, FALSE);
				return FALSE;
			}

			LOG_Msg(1, "P.1.1.4");
			if ( ! encExchange ) {
				LOG_Msg(1, "P.1.5");
				if ( memcmp(idExchange, idBlob, 20) == 0 ) {
					*hExchange = nBloque;
					encExchange = TRUE;
					LOG_Msg(1, "P.1.6");
				}
			}

			if ( ! encSignature ) {
				LOG_Msg(1, "P.1.1.7");
				if ( memcmp(idSignature, idBlob, 20 ) == 0 ) {
					*hSignature = nBloque;
					encSignature = TRUE;
					LOG_Msg(1, "P.1.1.8");
				} 
			}

			SecureZeroMemory(bloque, TAM_BLOQUE);

			if ( encExchange && encSignature )
				break;

			if ( LIBRT_LeerTipoBloqueCrypto(hProv->hpDispositivo, tipoBloque, FALSE, bloque, &nBloque) != 0 ) {
				CSPUTILS_CLOS_Disconnect(hProv);
				SecureZeroMemory(bloque, TAM_BLOQUE);
				LOG_EndFunc(5, FALSE);
				return FALSE;
			}
		}

		tipoBloque = BLOQUE_CIPHER_PRIVKEY_BLOB;
	}

	/* Si el handle estaba adquirido como no autenticado y no hemos encontrado todav�a
	 * las llaves, abrimos una sesi�n en modo autenticado para la b�squeda de las mismas.
	 * CSPUTILS_PedirPIN() llama tambi�n a GetKeyHandle. No hay problema de que exista
	 * recursividad infinita porque CSPUTILS_PedirPIN() s�lo llamar� a GetKeyHAndle en
	 * el supuesto de que el handle no sea autenticado.
	 */

	LOG_Msg(1, "J.1");
	LOG_Debug(1, "encSignature: %ld", encSignature);
	LOG_Debug(1, "encExchange: %ld", encExchange);
	LOG_Debug(1, "hProv->auth: %ld", hProv->auth);

	if ( !(encSignature && encExchange) && ! hProv->auth ) {

		LOG_Msg(1, "J.2");

		if ( CSPUTILS_PedirPIN(hProv, g_hModule) == -1 ) {
			LOG_Msg(1, "J.3");
			CSPUTILS_CLOS_Disconnect(hProv);
			SecureZeroMemory(bloque, TAM_BLOQUE);
			LOG_EndFunc(5, FALSE);
			return FALSE;
		}

		LOG_Msg(1, "J.4");

		*hSignature = hProv->hsSignature;
		*hExchange = hProv->hsExchange;
	}

	LOG_Msg(1, "J.5");
	
	SecureZeroMemory(bloque, TAM_BLOQUE);
	CSPUTILS_CLOS_Disconnect(hProv);

	LOG_EndFunc(5, TRUE);

	return TRUE;
}




/*! \brief Inserta los blobs en el stick.
 *
 * Inserta los blobs en el stick.
 */

BOOL CSPUTILS_Blob_Insertar ( HUSBCRYPTPROV *hProv, DWORD Algid, BOOL cifrar )
{
	BYTE bloquePrivada[TAM_BLOQUE];
	BYTE bloqueContainer[TAM_BLOQUE], *keyContainer;
	long numBloque, posLlave;
	BYTE idLlaves[20];
	USBCERTS_HANDLE *hDispositivo = hProv->hpDispositivo;
	HCRYPTKEY hKey;
	BYTE salt[20], *blob;
	DWORD iterCount = 2000, tamBlob;
	LPSTR pwd = NULL;
	HCRYPTKEY hSessKey;
	DWORD auxTamBlob;

	BYTE *pkcs1 = NULL, *pem = NULL;


	/* Si la llave no existe en el subyacente no puede existir en el stick
	 */

	if ( ! CryptGetUserKey(*(hProv->hpProv), Algid, &hKey ) ) {
		if ( GetLastError() == NTE_NO_KEY )
			return TRUE;
		else
			return FALSE;
	}

	/* Si hay que cifrar con una nueva password
	 */

	if ( cifrar ) {

		int ret;
		pwd = new char [ CLUI_CSP_MAX_PASS_LEN ];
		if ( ! pwd ) {
			CryptDestroyKey(hKey);
			return FALSE;
		}

		ret = hProv->lpCLUI_CSP_AskNewPassphrase( hProv->hWindow, CSP_IDIOMA_Get(CSP_IDIOMA_NUEVA_PASSWORD_DESCRIPCION), pwd );
		if ( ret != CLUI_CSP_OK ) {
			CryptDestroyKey(hKey);
			delete [] pwd;
			return FALSE;
		}

		/* Generamos la nueva llave de sesi�n
		 */

		if ( ! CAPI_PKCS5_3DES_PBE_Init_Ex (pwd, salt, 20, iterCount, hProv->hpProv, &hSessKey) ) {
			CryptDestroyKey(hKey);
			SecureZeroMemory(pwd, CLUI_CSP_MAX_PASS_LEN);
			delete [] pwd;
			return FALSE;
		}

		SecureZeroMemory(pwd, CLUI_CSP_MAX_PASS_LEN);
		delete [] pwd;

		if ( Algid == AT_SIGNATURE ) {
			CryptDestroyKey(hProv->hKeySessSignature);
			hProv->hKeySessSignature = hSessKey;
		} else {
			CryptDestroyKey(hProv->hKeySessExchange);
			hProv->hKeySessExchange = hSessKey;
		}

	} else {

		/* Si no piden que cifremos de nuevo, utilizaremos la llave de sesi�n
		 * que estaba estabecida previamente para el tipo
		 */
		
		if ( Algid == AT_SIGNATURE )
			hSessKey = hProv->hKeySessSignature;
		else
			hSessKey = hProv->hKeySessExchange;
		
	}

	/* Pimero exporto el blob sin cifrar para poder calcular su identificador.
	 * Despu�s, si hace falta, lo cifraremos y lo insertaremos en un bloque
	 * CIPHPRIVKEYBLOB
	 */

	if ( ! CryptExportKey(hKey, 0, PRIVATEKEYBLOB, 0, NULL, &auxTamBlob) ) {
		CryptDestroyKey(hKey);
		return FALSE;
	}

	/* Reservamos 8 bytes extra por si debemos cifrar el blob. 8 es el
	 * tama�o de bloque del triple des
	 */

	blob = new BYTE [auxTamBlob + 8];

	if ( ! blob ) {
		CryptDestroyKey(hKey);
		return FALSE;
	}

	if ( ! CryptExportKey(hKey, 0, PRIVATEKEYBLOB, 0, blob, &auxTamBlob) ) {
		SecureZeroMemory(blob,auxTamBlob+8);
		delete [] blob;
		blob = NULL;
		tamBlob = -1;
		CryptDestroyKey(hKey);
		return FALSE;
	}
	CryptDestroyKey(hKey);


	/* Transformo el blob a formato pkcs1 PEM 
	 */
/*
	if ( ! CSPUTILS_PrivateKeyBlobToPkcs1PrivateKey(blob, auxTamBlob, NULL, &dwPkcs1Size) ) {
		SecureZeroMemory(blob, auxTamBlob+8);
		delete [] blob;
		blob = NULL;
		tamBlob = -1;
		return FALSE;
	}
	pkcs1 = new BYTE[dwPkcs1Size];
	if ( ! pkcs1 ) {
		SecureZeroMemory(blob, auxTamBlob+8);
		delete [] blob;
		blob = NULL;
		tamBlob = -1;
		return FALSE;
	}
	if ( ! CSPUTILS_PrivateKeyBlobToPkcs1PrivateKey(blob, auxTamBlob, pkcs1, &dwPkcs1Size) ) {
		delete [] pkcs1;
		SecureZeroMemory(blob, auxTamBlob+8);
		delete [] blob;
		blob = NULL;
		tamBlob = -1;
		return FALSE;
	}
	if ( ! CSPUTILS_Pkcs1PrivateKeyToPEM(pkcs1, dwPkcs1Size, NULL, &dwPemSize) ) {
		SecureZeroMemory(pkcs1, dwPkcs1Size);
		SecureZeroMemory(blob, auxTamBlob+8);
		delete [] pkcs1;
		delete [] blob;
		blob = NULL;
		tamBlob = -1;
		return FALSE;
	}
	pem = new BYTE[dwPemSize];
	if ( ! pem ) {
		SecureZeroMemory(pkcs1, dwPkcs1Size);
		SecureZeroMemory(blob, auxTamBlob+8);
		delete [] pkcs1;
		delete [] blob;
		blob = NULL;
		tamBlob = -1;
		return FALSE;
	}
	if ( ! CSPUTILS_Pkcs1PrivateKeyToPEM(pkcs1, dwPkcs1Size, pem, &dwPemSize) ) {
		SecureZeroMemory(pkcs1, dwPkcs1Size);
		SecureZeroMemory(blob, auxTamBlob+8);
		delete [] pem;
		delete [] pkcs1;
		delete [] blob;
		blob = NULL;
		tamBlob = -1;
		return FALSE;
	}
	SecureZeroMemory(pkcs1, dwPkcs1Size);
	delete [] pkcs1;
*/
	/* Calculo el identificador de la llave
	 */

	if ( ! CSPUTILS_Blob_CalcularId(blob,auxTamBlob,idLlaves) ) {
		//SecureZeroMemory(pem, dwPemSize);
		SecureZeroMemory(blob,auxTamBlob+8);
		//delete [] pem;
		delete [] blob;
		blob = NULL;
		return FALSE;
	}

	/* Si la llave debe ser almacenada cifrada, la ciframos. Para el Enhanced un PRIVATEKEYBLOB
	 * cifrado es un PRIVATEKEYBLOB en el que se cifra todo excepto el BLOBHEADER
	 */

	if ( hSessKey ) {

		tamBlob = auxTamBlob - sizeof(BLOBHEADER);
		if ( ! CryptEncrypt(hSessKey, 0, TRUE, 0, blob + sizeof(BLOBHEADER), &tamBlob, auxTamBlob+8) ) {
			//SecureZeroMemory(pem, dwPemSize);
			SecureZeroMemory(blob,auxTamBlob+8);
			//delete [] pem;
			delete [] blob;
			blob = NULL;
			return FALSE;
		}
		tamBlob = tamBlob + sizeof(BLOBHEADER);
	} else
		tamBlob = auxTamBlob;


	/* Abrimos una sesi�n autenticada contra el clauer si hace falta
	 */

	if ( ! CSPUTILS_CLOS_Connect(hProv, TRUE) ) {
		//SecureZeroMemory(pem, dwPemSize);
		SecureZeroMemory(blob,auxTamBlob+8);
		//delete [] pem;
		delete [] blob;
		blob = NULL;
		return FALSE;
	}

	// Buscamos el key container y comprobamos si existe ya una llave
	// de ese tipo 

	if ( LIBRT_LeerBloqueCrypto(hDispositivo, hProv->hsContainer, bloqueContainer) != 0 ) {
		//SecureZeroMemory(pem, dwPemSize);
		CSPUTILS_CLOS_Disconnect(hProv);
		SecureZeroMemory(blob, auxTamBlob+8);
		delete [] blob;
		//delete [] pem;
		blob = NULL;
		return FALSE;
	}

	keyContainer = BLOQUE_KeyContainer_Buscar(bloqueContainer, hProv->szContainer);

	if ( ! keyContainer ) {
		//SecureZeroMemory(pem, dwPemSize);
		CSPUTILS_CLOS_Disconnect(hProv);
		SecureZeroMemory(blob,auxTamBlob+8);
		//delete [] pem;
		delete [] blob;
		blob = NULL;
		return FALSE;
	}

	switch ( Algid ) {
	case AT_KEYEXCHANGE:
		posLlave = BLOQUE_KeyContainer_GetUnidadExchange(keyContainer);
		break;

	case AT_SIGNATURE:
		posLlave = BLOQUE_KeyContainer_GetUnidadSignature(keyContainer);
		break;
	}

	/* Creamos los bloques a insertar
	 */

	BLOQUE_Set_Cifrado(bloquePrivada);
	
	if ( hSessKey ) {
		unsigned char iv[20];
		char desc[31] = "";

		memset(iv, 0, 20);

		BLOQUE_CIPHPRIVKEYBLOB_Nuevo(bloquePrivada);
		BLOQUE_CIPHPRIVKEYBLOB_Set_Objeto(bloquePrivada, blob, tamBlob);
		BLOQUE_CIPHPRIVKEYBLOB_Set_Salt(bloquePrivada, salt);
		BLOQUE_CIPHPRIVKEYBLOB_Set_Id(bloquePrivada, idLlaves);
		BLOQUE_CIPHPRIVKEYBLOB_Set_IterCount(bloquePrivada, iterCount);
		BLOQUE_CIPHPRIVKEYBLOB_Set_Iv(bloquePrivada, iv);
		BLOQUE_CIPHPRIVKEYBLOB_Set_Desc(bloquePrivada, desc);

	} else {	
		BLOQUE_PRIVKEYBLOB_Nuevo(bloquePrivada);
		BLOQUE_PRIVKEYBLOB_Set_Objeto(bloquePrivada, blob, tamBlob);
		BLOQUE_PRIVKEYBLOB_Set_Id(bloquePrivada, idLlaves);
	}

	SecureZeroMemory(blob, auxTamBlob+8);
	delete [] blob;	
	blob = NULL;

/*	SecureZeroMemory(bloquePEM, TAM_BLOQUE);
	BLOQUE_Set_Cifrado(bloquePEM);
	BLOQUE_LLAVEPRIVADA_Nuevo(bloquePEM);
	BLOQUE_LLAVEPRIVADA_Set_Objeto(bloquePEM, pem, dwPemSize);
	BLOQUE_LLAVEPRIVADA_Set_Tam(bloquePEM, dwPemSize);
	BLOQUE_LLAVEPRIVADA_Set_Id(bloquePEM, idLlaves);

	SecureZeroMemory(pem, dwPemSize);
	delete [] pem;
	pem = NULL;
*/
	if ( posLlave == -1 ) {

		/* Si no ten�amos almacenada la llave, entonces la insertamos
		 */

/*		if ( LIBRT_InsertarBloqueCrypto(hDispositivo, bloquePEM, &numBloque) != 0 ) {
			SecureZeroMemory(bloquePrivada, TAM_BLOQUE);
			SecureZeroMemory(bloquePEM, TAM_BLOQUE);
			CSPUTILS_CLOS_Disconnect(hProv);
			return FALSE;
		}

		SecureZeroMemory(bloquePEM, TAM_BLOQUE);
*/
		if ( LIBRT_InsertarBloqueCrypto (hDispositivo, bloquePrivada, &numBloque) != 0 ) {
			SecureZeroMemory(bloquePrivada, TAM_BLOQUE);
			CSPUTILS_CLOS_Disconnect(hProv);
			return FALSE;
		}
	
		// Actualizamos la informaci�n del keyContainer

		if ( Algid == AT_KEYEXCHANGE ) {
		
			 if ( BLOQUE_KeyContainer_EstablecerEXCHANGE (bloqueContainer,
   							    hProv->szContainer,
								numBloque,
								FALSE) != 0 ) 
			 {
				CSPUTILS_CLOS_Disconnect(hProv);
				SecureZeroMemory(bloquePrivada, TAM_BLOQUE);
				return FALSE;
			 }

			 if ( BLOQUE_KeyContainer_Establecer_ID_Exchange(bloqueContainer, hProv->szContainer, idLlaves) != 0 ) {
				 CSPUTILS_CLOS_Disconnect(hProv);
				SecureZeroMemory(bloquePrivada, TAM_BLOQUE);
				//SecureZeroMemory(bloquePEM, TAM_BLOQUE);
				return FALSE;
			 }

		} else {

			if ( BLOQUE_KeyContainer_EstablecerSIGNATURE (bloqueContainer,
							     hProv->szContainer,
								 numBloque,
								 FALSE) != 0 ) 
			{
				CSPUTILS_CLOS_Disconnect(hProv);
				SecureZeroMemory(bloquePrivada, TAM_BLOQUE);
				//SecureZeroMemory(bloquePEM, TAM_BLOQUE);
				return FALSE;
			}

			if ( BLOQUE_KeyContainer_Establecer_ID_Signature(bloqueContainer, hProv->szContainer, idLlaves) != 0 ) {
				CSPUTILS_CLOS_Disconnect(hProv);
				SecureZeroMemory(bloquePrivada, TAM_BLOQUE);
				//SecureZeroMemory(bloquePEM, TAM_BLOQUE);
				return FALSE;
			}
		}

		if ( LIBRT_EscribirBloqueCrypto (hDispositivo, hProv->hsContainer, bloqueContainer) != 0 ) {
			CSPUTILS_CLOS_Disconnect(hProv);
			SecureZeroMemory(bloquePrivada, TAM_BLOQUE);
			//SecureZeroMemory(bloquePEM, TAM_BLOQUE);
			return FALSE;
		}

	} else {

		// El container ya contiene una llave. Machacamos el contenido de la misma

		BYTE *prevId;

		if ( Algid == AT_KEYEXCHANGE ) 
			prevId = BLOQUE_KeyContainer_Get_ID_Exchange(bloqueContainer, hProv->szContainer);
		else
			prevId = BLOQUE_KeyContainer_Get_ID_Signature(bloqueContainer, hProv->szContainer);


/*		if ( LIBRT_LeerTipoBloqueCrypto(hDispositivo, BLOQUE_LLAVE_PRIVADA, 1, bloque, &nb) != 0 ) {
			SecureZeroMemory(bloquePrivada, TAM_BLOQUE);
			//SecureZeroMemory(bloquePEM, TAM_BLOQUE);
			CSPUTILS_CLOS_Disconnect(hProv);
			return FALSE;
		}
		while ( nb != -1 ) {

			if ( memcmp(prevId, BLOQUE_LLAVEPRIVADA_Get_Id(bloque), 20) == 0 )
				break;

			SecureZeroMemory(bloque, TAM_BLOQUE);
			if ( LIBRT_LeerTipoBloqueCrypto(hDispositivo, BLOQUE_LLAVE_PRIVADA, 0, bloque, &nb) != 0 ) {
				SecureZeroMemory(bloquePrivada, TAM_BLOQUE);
				//SecureZeroMemory(bloquePEM, TAM_BLOQUE);
				CSPUTILS_CLOS_Disconnect(hProv);
				return FALSE;
			}
		}
		SecureZeroMemory(bloque, TAM_BLOQUE);

		if ( nb == -1 ) {
			if ( LIBRT_InsertarBloqueCrypto(hDispositivo, bloquePEM, &nb) != 0 ) {
				SecureZeroMemory(bloquePrivada, TAM_BLOQUE);
				SecureZeroMemory(bloquePEM, TAM_BLOQUE);
				CSPUTILS_CLOS_Disconnect(hProv);
				return FALSE;
			}
		} else {
			if ( LIBRT_EscribirBloqueCrypto(hDispositivo, nb, bloquePEM) != 0 ) {
				SecureZeroMemory(bloquePrivada, TAM_BLOQUE);
				SecureZeroMemory(bloquePEM, TAM_BLOQUE);
				CSPUTILS_CLOS_Disconnect(hProv);
				return FALSE;
			}
		}

		SecureZeroMemory(bloquePEM, TAM_BLOQUE);
*/
		if ( Algid == AT_KEYEXCHANGE ) {
			if ( BLOQUE_KeyContainer_Establecer_ID_Exchange(bloqueContainer, hProv->szContainer, idLlaves) != 0 ) {
				CSPUTILS_CLOS_Disconnect(hProv);
				SecureZeroMemory(bloquePrivada, TAM_BLOQUE);
				return FALSE;			
			}

		} else {
			if ( BLOQUE_KeyContainer_Establecer_ID_Signature(bloqueContainer, hProv->szContainer, idLlaves) != 0 ) {
				CSPUTILS_CLOS_Disconnect(hProv);
				SecureZeroMemory(bloquePrivada, TAM_BLOQUE);
				return FALSE;
			}
		}

		if ( LIBRT_EscribirBloqueCrypto (hDispositivo, hProv->hsContainer, bloqueContainer) != 0 ) {
			CSPUTILS_CLOS_Disconnect(hProv);
			SecureZeroMemory(bloquePrivada, TAM_BLOQUE);
			return FALSE;
		}

		if ( LIBRT_EscribirBloqueCrypto (hDispositivo, posLlave, bloquePrivada) != 0 ) {
			CSPUTILS_CLOS_Disconnect(hProv);
			SecureZeroMemory(bloquePrivada, TAM_BLOQUE);
			return FALSE;			
		}


	}
	SecureZeroMemory(bloquePrivada, TAM_BLOQUE);
	//SecureZeroMemory(bloquePEM, TAM_BLOQUE);
	CSPUTILS_CLOS_Disconnect(hProv);

	return TRUE;
}






BOOL CSPUTILS_KeyContainer_Listar (HUSBCRYPTPROV *hProv,
								   INFO_KEY_CONTAINER **infoKeyContainers,
								   int *numContainers)
{

	long *handleBloques=NULL, numBloques;
	BYTE *bloques=NULL;
	BOOL encontrado = FALSE, ok=TRUE;
	unsigned int auxNumContainers;
	int j,i;
	INFO_KEY_CONTAINER *auxInfo=NULL;
	USBCERTS_HANDLE *hDispositivo = hProv->hpDispositivo;


	if ( ! CSPUTILS_CLOS_Connect(hProv, FALSE) ) 
	{
		ok = FALSE;
		goto finCSPUTILS_KeyContainer_Listar;
	}

	*numContainers = 0;

	if ( LIBRT_LeerTodosBloquesTipo (hDispositivo, 
									 BLOQUE_KEY_CONTAINERS, 
									 NULL,
									 NULL,
									 &numBloques) != 0 )
	{
		ok = FALSE;
		goto finCSPUTILS_KeyContainer_Listar;
	}
	
	if ( numBloques == 0 ) {
		ok = TRUE;
		goto finCSPUTILS_KeyContainer_Listar;
	}

	handleBloques = new long [numBloques];

	if ( !handleBloques ) {
		ok = FALSE;
		goto finCSPUTILS_KeyContainer_Listar;
	}

	bloques = new BYTE [TAM_BLOQUE*numBloques];

	if ( !bloques ) {
		ok = FALSE;
		goto finCSPUTILS_KeyContainer_Listar;
	}

	if ( LIBRT_LeerTodosBloquesTipo (hDispositivo, 
										BLOQUE_PRIVKEY_BLOB, 
										handleBloques,
										bloques,
										&numBloques) != 0 ) 
	{
		ok = FALSE;
		goto finCSPUTILS_KeyContainer_Listar;
	}

	for ( i = 0 ; i < numBloques ; i++ ) 
	{
		BLOQUE_KeyContainer_Enumerar (bloques+i*TAM_BLOQUE, NULL, &auxNumContainers);
		*numContainers = *numContainers + auxNumContainers;
	}

	
	(*infoKeyContainers) = new INFO_KEY_CONTAINER [*numContainers];
	j = 0;
	for ( i = 0 ; i < numBloques ; i++ ) {
		BLOQUE_KeyContainer_Enumerar (bloques+i*TAM_BLOQUE, NULL, &auxNumContainers);
		auxInfo = new INFO_KEY_CONTAINER [auxNumContainers];
		BLOQUE_KeyContainer_Enumerar (bloques+i*TAM_BLOQUE, auxInfo, &auxNumContainers);

		for ( unsigned int k = 0 ; k < auxNumContainers ; k++ ) {
			memcpy((*infoKeyContainers)+j, auxInfo+k, sizeof(INFO_KEY_CONTAINER));
			++j;
		}

		delete [] auxInfo;
	}


finCSPUTILS_KeyContainer_Listar:

	if ( handleBloques ) {
		SecureZeroMemory(handleBloques, sizeof(long)*numBloques);
		delete [] handleBloques;
		handleBloques = NULL;
	}

	if ( bloques ) {
		SecureZeroMemory(bloques, numBloques*TAM_BLOQUE);
		delete [] bloques;
		bloques = NULL;
	}

	if ( hProv->conectado ) 
		CSPUTILS_CLOS_Disconnect(hProv);

	return ok;

}




char * CSPUTILS_Subyacente_NuevoTemporal ( LPCTSTR szUnderlayingCSP )
{
	HCRYPTPROV hProv;
	char *cName;

	if ( !CryptAcquireContext(&hProv, NULL, "Microsoft Enhanced Cryptographic Provider v1.0",PROV_RSA_FULL,CRYPT_VERIFYCONTEXT) ) 
		return NULL;

	cName = new char [11];
	
	if ( ! cName )
		return NULL;

	if ( !CryptGenRandom(hProv, 10, (unsigned char *)cName ) ) {
		delete cName;
		cName = NULL;
		return NULL;
	}

	if ( !CryptReleaseContext(hProv,0) ) {
		SecureZeroMemory(cName, 11);
		delete cName;
		cName = NULL;
		return NULL;
	}

	// Mapeamos en nombre del nuevo container

	for ( int i = 0 ; i < 10; i++ ) 
		cName[i] = (((unsigned char)cName[i]) % 20) + 'a';
	
	cName[10] = '\0';

	// Creamos el container en el subyacente


	if  (!CryptAcquireContext(&hProv,cName,szUnderlayingCSP,PROV_RSA_FULL,CRYPT_NEWKEYSET) ) {
		SecureZeroMemory(cName, 11);
		delete cName;
		cName = NULL;
		return NULL;
	}
		
	CryptReleaseContext(hProv,0);

	return cName;

}




BOOL CSPUTILS_Subyacente_BorrarKeyContainer (LPCTSTR csp,
											 LPCTSTR container)
{
	HCRYPTPROV hProv;

	if (!CryptAcquireContext(&hProv, container, csp, PROV_RSA_FULL,CRYPT_DELETEKEYSET))
		return FALSE;

	return TRUE;
}





BOOL CSPUTILS_Subyacente_CargarPersistente (HUSBCRYPTPROV *hUsbProv)
{
	char *kc = NULL;
	BYTE bloque[TAM_BLOQUE];
	long hExchange, hSignature;
	HCRYPTKEY hKey;
	BOOL ret = TRUE;

	if ( ! CSPUTILS_KeyContainer_GetKeyHandles(hUsbProv, &hExchange, &hSignature) ) {
		ret = FALSE;
		goto finCSPUTILS_Subyacente_CargarPersistente;
	}

	LOG_Debug(1, "HANDLE. Exchange: %ld", hExchange);
	LOG_Debug(1, "HANDLE. Signature: %ld", hSignature);

	if ( ! CSPUTILS_CLOS_Connect(hUsbProv, TRUE) ) {
		
		ret = FALSE;
		goto finCSPUTILS_Subyacente_CargarPersistente;
	}

	/* Cargo el par de llaves de tipo AT_KEYEXCHANGE
	 */
	
	if ( hExchange != -1 ) {

		if ( LIBRT_LeerBloqueCrypto(hUsbProv->hpDispositivo, hExchange, bloque) != 0 ) {
			CSPUTILS_CLOS_Disconnect(hUsbProv);
			CryptReleaseContext(*(hUsbProv->hpProv),0);
			ret = FALSE;
			goto finCSPUTILS_Subyacente_CargarPersistente;
		}

		/* Si el tipo de bloque del blob es blob cifrado lo importamos de
		 * manera especial
		 */

		if ( *(bloque + 1) == BLOQUE_CIPHER_PRIVKEY_BLOB ) {


			LOG_Msg(10, "CARGANDO CIPHER KEY BLOB");

			if ( ! CSPUTILS_Subyacente_ImportarCipheredBlob ( hUsbProv, 
															  bloque, 
															  AT_KEYEXCHANGE, 
															  &hKey) ) 
			{
				CSPUTILS_CLOS_Disconnect(hUsbProv);
				CryptReleaseContext(*(hUsbProv->hpProv),0);
				ret = FALSE;
				goto finCSPUTILS_Subyacente_CargarPersistente;				
			}

		} else {

			if ( !CryptImportKey(*(hUsbProv->hpProv), 
								 BLOQUE_PRIVKEYBLOB_Get_Objeto(bloque), 
								 BLOQUE_PRIVKEYBLOB_Get_Tam(bloque),
								 NULL,
								 CRYPT_EXPORTABLE,
								 &hKey))
			{
				CSPUTILS_CLOS_Disconnect(hUsbProv);
				CryptReleaseContext(*(hUsbProv->hpProv),0);
				ret = FALSE;
				goto finCSPUTILS_Subyacente_CargarPersistente;
			}
		}
	
		if ( ! CryptDestroyKey(hKey) ) {
			CSPUTILS_CLOS_Disconnect(hUsbProv);
			CryptReleaseContext(*(hUsbProv->hpProv),0);
			ret = FALSE;
			goto finCSPUTILS_Subyacente_CargarPersistente;
		}

	}

	if ( hSignature != -1 ) {

		if ( LIBRT_LeerBloqueCrypto(hUsbProv->hpDispositivo, hSignature, bloque) != 0 ) {
			CSPUTILS_CLOS_Disconnect(hUsbProv);
			CryptReleaseContext(*(hUsbProv->hpProv),0);
			ret = FALSE;
			goto finCSPUTILS_Subyacente_CargarPersistente;
		}

		if ( *(bloque + 1) == BLOQUE_CIPHER_PRIVKEY_BLOB ) {

			if ( ! CSPUTILS_Subyacente_ImportarCipheredBlob ( hUsbProv, 
															  bloque, 
															  AT_SIGNATURE, 
															  &hKey) ) 
			{
				CSPUTILS_CLOS_Disconnect(hUsbProv);
				CryptReleaseContext(*(hUsbProv->hpProv),0);
				ret = FALSE;
				goto finCSPUTILS_Subyacente_CargarPersistente;				
			}

		} else {

			if ( ! CryptImportKey(*(hUsbProv->hpProv), 
								 BLOQUE_PRIVKEYBLOB_Get_Objeto(bloque), 
								 BLOQUE_PRIVKEYBLOB_Get_Tam(bloque),
								 0,
								 CRYPT_EXPORTABLE,
								 &hKey))
			{
				CSPUTILS_CLOS_Disconnect(hUsbProv);
				CryptReleaseContext(*(hUsbProv->hpProv),0);
				ret = FALSE;
				goto finCSPUTILS_Subyacente_CargarPersistente;
			}

		}

		if ( ! CryptDestroyKey(hKey) ) {
			CSPUTILS_CLOS_Disconnect(hUsbProv);
			CryptReleaseContext(*(hUsbProv->hpProv),0);
			ret = FALSE;
			goto finCSPUTILS_Subyacente_CargarPersistente;
		}
	}


finCSPUTILS_Subyacente_CargarPersistente:

	SecureZeroMemory(bloque, TAM_BLOQUE);

	if ( ret == FALSE ) {
		if ( kc ) {
			CSPUTILS_Subyacente_BorrarKeyContainer(g_szUnderlayingCSP, kc);
			SecureZeroMemory(kc, strlen(kc)+1);
			delete [] kc;
			kc = NULL;
		}
	}

	if ( hUsbProv->conectado )
		CSPUTILS_CLOS_Disconnect(hUsbProv);

	return ret;
}

/*
 * Algid. Indica el tipo de llave a descargar
 * guardarLlaves. Indica si hay que almacenar las llaves en el stick
 * nuevaPassword. Indica que se debe pedir la password al usuario para cifrar la llave. S�lo
 *                tiene sentido si se indica que se deben guardar las llaves
 */

BOOL CSPUTILS_Subyacente_DescargarPersistente (HUSBCRYPTPROV *hProv, DWORD Algid, BOOL guardarLlave, BOOL nuevaPassword)
{
	/* Si debemos guardar las llaves, primero las volcamos al stick y despu�s
	 * las machacamos en el subyacente
	 */

	if ( guardarLlave ) {

		/* Pedimos el pin de acceso al clauer si hace falta
		 */

		if ( ! CSPUTILS_CLOS_Connect(hProv, TRUE) )
			return FALSE;

		if ( ! CSPUTILS_Blob_Insertar(hProv, Algid, nuevaPassword ) ) {
			CSPUTILS_CLOS_Disconnect(hProv);
			return FALSE;
		}
		CSPUTILS_CLOS_Disconnect(hProv);
	}

	/* Importamos un par de llaves de cada tipo para machacar el contenido del subyacente
	 */

	HCRYPTKEY aux;

	if ( Algid == AT_SIGNATURE ) {
		if ( CryptImportKey(*(hProv->hpProv), privSig, 1172, NULL, 0, &aux) )
			CryptDestroyKey(aux);
	} else {
		if ( CryptImportKey(*(hProv->hpProv), privEx, 1172, NULL, 0, &aux) )
			CryptDestroyKey(aux);
	}

	return TRUE;
}


/* \brief Borrar la llave de tipo tipo del kecontainer nombreKeyContainer.
 * 
 * Borrar la llave de tipo tipo del kecontainer nombreKeyContainer.
 *
 * \param hProv
 *		  ENTRADA. Handle al CSP.
 *
 * \param nombreKeyContainer
 *		  ENTRADA. El nombre del key container que contiene la llave a borrar.
 *
 * \param tipo
 *		  El tipo de la llave a borrar.
 *
 * \retval TRUE
 *		   Ok
 *
 * \retval FALSE
 *		   Error.
 */

BOOL CSPUTILS_KeyContainer_BorrarLlave (HUSBCRYPTPROV *hProv,
										LPTSTR nombreKeyContainer,
										DWORD tipo)
{
	long hsContainer, hLlave;
	BYTE bloque[TAM_BLOQUE];
	BYTE *keyContainer;
	USBCERTS_HANDLE *hDispositivo = hProv->hpDispositivo;
	BYTE id[20];

	memset(id, 0, 20);


	if ( ! CSPUTILS_CLOS_Connect(hProv, TRUE) ) 
		return FALSE;


	hsContainer = CSPUTILS_KeyContainer_GetHandle (hProv, nombreKeyContainer);

	if ( hsContainer == -1 ) {
		CSPUTILS_CLOS_Disconnect(hProv);
		return FALSE;
	}

	if ( LIBRT_LeerBloqueCrypto(hDispositivo, hsContainer, bloque) != 0 ) {
		CSPUTILS_CLOS_Disconnect(hProv);
		return FALSE;
	}

	keyContainer = BLOQUE_KeyContainer_Buscar (bloque, nombreKeyContainer);

	/*
	 * Actualizo el key container
	 */

	if ( tipo == AT_SIGNATURE ) {
		if ( BLOQUE_KeyContainer_EstablecerSIGNATURE (bloque, nombreKeyContainer, -1, FALSE) != 0 ) {
			CSPUTILS_CLOS_Disconnect(hProv);
			return FALSE;
		}
		
		if ( BLOQUE_KeyContainer_Establecer_ID_Signature(bloque, nombreKeyContainer, id) != 0 ) {
			CSPUTILS_CLOS_Disconnect(hProv);
			return FALSE;
		}
	} else {
		if ( BLOQUE_KeyContainer_EstablecerEXCHANGE(bloque, nombreKeyContainer, -1, FALSE) != 0 ) {
			CSPUTILS_CLOS_Disconnect(hProv);
			return FALSE;
		}

		if ( BLOQUE_KeyContainer_Establecer_ID_Exchange(bloque, nombreKeyContainer, id) != 0 ) {
			CSPUTILS_CLOS_Disconnect(hProv);
			return FALSE;
		}
	}

	if ( LIBRT_EscribirBloqueCrypto(hDispositivo, hsContainer, bloque) != 0 ) {
		CSPUTILS_CLOS_Disconnect(hProv);
		return FALSE;
	}

	/*
	 * Machaco el contenido de la llave
	 */

	if ( tipo == AT_SIGNATURE )
		hLlave = BLOQUE_KeyContainer_GetUnidadSignature(keyContainer);
	else
		hLlave = BLOQUE_KeyContainer_GetUnidadExchange(keyContainer);

	if ( hLlave == -1 ) {
		CSPUTILS_CLOS_Disconnect(hProv);
		return TRUE;
	}

	if ( LIBRT_BorrarBloqueCrypto(hDispositivo, hLlave) != 0 ) {
		CSPUTILS_CLOS_Disconnect(hProv);
		return FALSE;
	}

	CSPUTILS_CLOS_Disconnect(hProv);

	return TRUE;
}



/*
 * Crea un nuevo handle con los campos convenientemente inicializados
 *
 *    . FALSE si no hay suficiente memoria para reservar un nuevo handle.
 *	  . TRUE si no hay error.
 */

BOOL CSPUTILS_HANDLE_Nuevo (HUSBCRYPTPROV **hProv)
{
	*hProv = new HUSBCRYPTPROV;

	if ( ! (*hProv) )
		return FALSE;

	// Por razones hist�ricas (jejeje... esto me ha quedao bien) conservo el dise�o
	// original con puntero a funci�n

	(*hProv)->lpCLUI_CSP_AskGlobalPassphrase = CLUI_CSP_AskGlobalPassphrase;
	if ( ! (*hProv)->lpCLUI_CSP_AskGlobalPassphrase ) {
		LOG_MsgError(1, "ERROR AQUI 1");
		delete *hProv;
		*hProv = NULL;
		return FALSE;
	}
	(*hProv)->lpCLUI_CSP_AskNewPassphrase = CLUI_CSP_AskNewPassphrase;
	if ( ! (*hProv)->lpCLUI_CSP_AskNewPassphrase ) {
		LOG_MsgError(1, "ERROR AQUI 2");
		delete *hProv;
		*hProv = NULL;
		return FALSE;
	}
	(*hProv)->lpCLUI_CSP_AskDoublePassphrase = CLUI_CSP_AskDoublePassphrase;
	if ( ! (*hProv)->lpCLUI_CSP_AskNewPassphrase ) {
		LOG_MsgError(1, "ERROR AQUI 3");
		delete *hProv;
		*hProv = NULL;
		return FALSE;
	}

	(*hProv)->dwFlags = 0;

	(*hProv)->hpDispositivo = new USBCERTS_HANDLE;
	if ( ! ( (*hProv)->hpDispositivo ) ) {
		CSPUTILS_HANDLE_Liberar(*hProv);
		return FALSE;
	}

	(*hProv)->hpProv        = new HCRYPTPROV;
	if ( ! ((*hProv)->hpProv) ) {
		CSPUTILS_HANDLE_Liberar(*hProv);
		return FALSE;
	}

	(*hProv)->hsContainer = -1;
	(*hProv)->hsExchange  = -1;
	(*hProv)->hsSignature = -1;

	(*hProv)->pInfoEnum = new INFOENUM;

	if ( ! ( (*hProv)->pInfoEnum ) ) {
		CSPUTILS_HANDLE_Liberar(*hProv);
		return FALSE;
	}

	(*hProv)->pInfoEnum->keyContainers = NULL;
	(*hProv)->pInfoEnum->pos = -1;
	(*hProv)->pInfoEnum->tamVector = 0;
	(*hProv)->pInfoEnum->maxItem = 0;

	(*hProv)->szContainer       = NULL;

	(*hProv)->hWindow = 0;

	(*hProv)->auth = FALSE;
	(*hProv)->conectado = FALSE;

	(*hProv)->nodisp = TRUE;

	(*hProv)->hKeySessExchange = 0;
	(*hProv)->hKeySessSignature = 0;

	*((*hProv)->szDevice) = 0;
	(*hProv)->szPass = NULL;
	(*hProv)->bIdAvailable = FALSE;

	

	return TRUE;

}



void CSPUTILS_HANDLE_Liberar (HUSBCRYPTKEY *hKey)
{

	if ( hKey ) {

		if ( hKey->hKey ) {
			delete hKey->hKey;
			hKey->hKey = 0;
		}

		SecureZeroMemory(hKey, sizeof(HUSBCRYPTKEY));
		delete hKey;
		hKey = 0;

	}

}




/*
 * Libera los campos del handle y el propio handle
 */

void CSPUTILS_HANDLE_Liberar (HUSBCRYPTPROV *hProv)
{
	map<HUSBCRYPTKEY *, HUSBCRYPTKEY *>::iterator iKey;
	map<HUSBCRYPTHASH *, HUSBCRYPTHASH *>::iterator iHash;

	for ( iKey = hProv->sesKeys.begin() ; iKey != hProv->sesKeys.end() ; iKey++ ) {
		HUSBCRYPTKEY *key;
		key = (*iKey).first;
		CSPUTILS_HANDLE_Liberar(key);
	}
	hProv->sesKeys.clear();

	for ( iHash = hProv->hashObjects.begin() ; iHash != hProv->hashObjects.end() ; iHash++ ) {
		HUSBCRYPTHASH *hash;
		hash = (*iHash).first;
		CSPUTILS_HANDLE_Liberar(hash);
	}
	hProv->hashObjects.clear();

	/*
	if ( hProv->hpDispositivo )
		delete hProv->hpDispositivo;
	*/

	if ( hProv->hpProv )
		delete hProv->hpProv;

	if ( hProv->pInfoEnum ) {

		if ( hProv->pInfoEnum->keyContainers ) {
			for ( int i = 0 ; i < hProv->pInfoEnum->tamVector ; i++ )
				delete [] hProv->pInfoEnum->keyContainers[i].nombreKeyContainer;
			delete [] hProv->pInfoEnum->keyContainers;
		}

		delete [] hProv->pInfoEnum;

	}

	if ( hProv->szContainer )
		delete [] hProv->szContainer;

	hProv->szPass = NULL;

	hProv->lpCLUI_CSP_AskGlobalPassphrase = NULL;
	hProv->lpCLUI_CSP_AskNewPassphrase    = NULL;
	hProv->lpCLUI_CSP_AskDoublePassphrase = NULL;
	
	delete hProv;

}







BOOL CSPUTILS_KeyContainer_SetExportable (HUSBCRYPTPROV *hProv,
										  HUSBCRYPTKEY *hKey,
										  BOOL exportable)
{

	BYTE bloqueContainer[TAM_BLOQUE];
	BYTE *kc;
	BOOL ok = TRUE;

	/* S�lo establecemos la exportabilidad de los pares de llaves almacenados en el stick
	 */

	if ( (hKey->tipo != PARLLAVES) || (hProv->dwFlags == CRYPT_VERIFYCONTEXT) ) {
		ok = FALSE;
		goto finCSPUTILS_KeyContainer_SetExportable;
	}

	if ( ! CSPUTILS_CLOS_Connect(hProv, TRUE) ) {
		ok = FALSE;
		goto finCSPUTILS_KeyContainer_SetExportable;
	}

	if ( LIBRT_LeerBloqueCrypto(hProv->hpDispositivo, hProv->hsContainer, bloqueContainer) != 0 ) {
		CSPUTILS_CLOS_Disconnect(hProv);
		ok = FALSE;
		goto finCSPUTILS_KeyContainer_SetExportable;
	}
		
	kc = BLOQUE_KeyContainer_Buscar(bloqueContainer, hProv->szContainer);

	if (!kc) {
		CSPUTILS_CLOS_Disconnect(hProv);
		ok = FALSE;
		goto finCSPUTILS_KeyContainer_SetExportable;
	}

	
	if ( hKey->algoritmo == AT_KEYEXCHANGE ) {
		if ( exportable )
			BLOQUE_KeyContainer_SetExchange(kc);
		else
			BLOQUE_KeyContainer_UnsetExchange(kc);
	} else if ( hKey->algoritmo == AT_SIGNATURE ) {
		if ( exportable )
			BLOQUE_KeyContainer_SetSignature(kc);
		else
			BLOQUE_KeyContainer_UnsetSignature(kc);
	}


	if ( LIBRT_EscribirBloqueCrypto(hProv->hpDispositivo, hProv->hsContainer, bloqueContainer) != 0 ) {
		CSPUTILS_CLOS_Disconnect(hProv);
		ok = FALSE;
		goto finCSPUTILS_KeyContainer_SetExportable;
	}

finCSPUTILS_KeyContainer_SetExportable:

	SecureZeroMemory(bloqueContainer, TAM_BLOQUE);

	if ( hProv->conectado )
		CSPUTILS_CLOS_Disconnect(hProv);

	return ok;
}


BOOL CSPUTILS_KeyContainer_GetExportable (HUSBCRYPTPROV *hProv,
										  HUSBCRYPTKEY *hKey,
										  BOOL &exportable)
{
	BYTE bloqueContainer[TAM_BLOQUE];
	BYTE *kc;

	if ( (hKey->tipo != PARLLAVES) || (hProv->dwFlags == CRYPT_VERIFYCONTEXT) )
		return FALSE;

	if ( ! CSPUTILS_CLOS_Connect(hProv, FALSE) )
		return FALSE;

	if ( LIBRT_LeerBloqueCrypto(hProv->hpDispositivo, hProv->hsContainer, bloqueContainer) != 0 ) {
		CSPUTILS_CLOS_Disconnect(hProv);
		return FALSE;
	}

	CSPUTILS_CLOS_Disconnect(hProv);
		
	kc = BLOQUE_KeyContainer_Buscar(bloqueContainer, hProv->szContainer);

	if (!kc) {
		return FALSE;
	}

	if ( hKey->algoritmo == AT_KEYEXCHANGE ) 
		exportable = (BLOQUE_KeyContainer_GetExchange(kc)) ? TRUE : FALSE;
	else if ( hKey->algoritmo == AT_SIGNATURE ) 
		exportable = (BLOQUE_KeyContainer_GetSignature(kc)) ? TRUE : FALSE;

	return TRUE;
}


/*! \brief Calcula un id para la llave. El id lo calculo yo haciendo un hash
    del vector de bytes formados por la concatenaci�n del m�dulo y el
	exponente p�blico.
   
	Calcula un id para la llave. El id lo calculo yo haciendo un hash
    del vector de bytes formados por la concatenaci�n del m�dulo y el
	exponente p�blico.
 */

BOOL CSPUTILS_Blob_CalcularId (BYTE *blob, DWORD tamBlob, BYTE id[20])
{
	BLOBHEADER *hdr;
	HCRYPTPROV hProv;
	HCRYPTHASH hHash;
	RSAPUBKEY *pk;
	BYTE *auxId;
	DWORD tamAux;

	hdr = (BLOBHEADER *) blob;
	if ( (hdr->bType != PRIVATEKEYBLOB) && (hdr->bType != PUBLICKEYBLOB) ) 
		return FALSE;
	
	pk = (RSAPUBKEY *) (blob+sizeof(BLOBHEADER));
	tamAux = (pk->bitlen/8) + sizeof(DWORD);

	auxId = new BYTE [tamAux];
	if ( !auxId ) 
		return FALSE;
	
	memcpy(auxId, blob+sizeof(BLOBHEADER)+sizeof(RSAPUBKEY), pk->bitlen/8);
	memcpy(auxId+(pk->bitlen/8),&(pk->pubexp),sizeof(DWORD));

	if ( !CryptAcquireContext(&hProv, NULL, "Microsoft Enhanced Cryptographic Provider v1.0",PROV_RSA_FULL, CRYPT_VERIFYCONTEXT) ) {
		delete [] auxId;
		auxId = NULL;
		return FALSE;
	}

	if ( !CryptCreateHash(hProv, CALG_SHA1, 0, 0, &hHash) ) {
		delete [] auxId;
		auxId = NULL;
		CryptReleaseContext(hProv,0);
		return FALSE;
	}

	if ( !CryptHashData(hHash, auxId, tamAux, 0) ) {
		delete [] auxId;
		auxId = NULL;
		CryptDestroyHash(hHash);
		CryptReleaseContext(hProv,0);
		return FALSE;
	}

	delete [] auxId;
	auxId = NULL;

	tamAux = 20;
	if ( !CryptGetHashParam(hHash,HP_HASHVAL,id,&tamAux,0) ) {
		CryptDestroyHash(hHash);
		CryptReleaseContext(hProv,0);
		return FALSE;
	}

	CryptDestroyHash(hHash);
	CryptReleaseContext(hProv,0);

	return TRUE;

}

/*! \brief Indica si la versi�n del sistema operativo que est� corriendo permite generar pares de llaves
 *         en memoria.
 *
 * Indica si la versi�n del sistema operativo que est� corriendo permite generar pares de llaves
 * en memoria. Esto se consigue utilizando CRYPT_VERIFYCONTEXT en la adquisici�n del handle al container.
 * La creaci�n de llaves ephemereal NO ES POSIBLE en windows 95/98/Me y NT.
 *
 * \retval TRUE
 *		   Se puede.
 *
 * \retval FALSE
 *		   No se puede.
 */

BOOL CSPUTILS_OS_Ephemereal (void)
{
	OSVERSIONINFO osv;

	osv.dwOSVersionInfoSize = sizeof(OSVERSIONINFO);
	
	if ( !GetVersionEx(&osv) )
		return -1;

	if ( ( osv.dwMajorVersion == 3 ) || ( osv.dwMajorVersion == 4 ) ) {
		/* Windows 95/98/Me y NT */
		return FALSE;
	} else {
		/* Windows 2000/XP */
		return TRUE;
	}

}




/*! \brief Reserva un handle HUSBCRYPKEY.
 *
 * Reserva un handle HUSBCRYPKEY.
 *
 * \remarks Para liberar usar CSPUTILS_HANDLE_Liberar
 *
 * \param hKey
 *		  [SALIDA] El handle que se devuelve.
 *
 * \retval TRUE
 *		   Ok
 *
 * \retval FALSE
 *		   Error
 */

BOOL CSPUTILS_HANDLE_Nuevo (HUSBCRYPTKEY **hKey)
{
	
	BOOL ret = TRUE;
	HUSBCRYPTKEY *aux = NULL;



	aux = new HUSBCRYPTKEY;
	if ( ! aux ) {
		ret = FALSE;
		goto finCSPUTILS_HANDLE_Nuevo_KEY;
	}

	memset(aux, 0, sizeof(HUSBCRYPTKEY));

	aux->hKey = new HCRYPTKEY;

	if ( ! aux->hKey ) {
		ret = FALSE;
		goto finCSPUTILS_HANDLE_Nuevo_KEY;
	}

	*hKey = aux;

finCSPUTILS_HANDLE_Nuevo_KEY:

	if ( FALSE == ret ) {

		if ( aux->hKey ) {
			delete aux->hKey;
			aux->hKey = 0;
		}

		if ( aux ) {
			delete aux;
			aux = 0;
		}

	}




	return ret;
}




BOOL CSPUTILS_HANDLE_Nuevo   (HUSBCRYPTHASH **hHash)
{
	HUSBCRYPTHASH *usbHash = NULL;
	BOOL ok = TRUE;

	usbHash = new HUSBCRYPTHASH;

	if ( ! usbHash ) {
		ok = FALSE;
		goto finCSPUTILS_HANDLE_Nuevo;
	}

	usbHash->hHash = new HCRYPTHASH;

	if ( ! usbHash->hHash ) {
		ok = FALSE;
		goto finCSPUTILS_HANDLE_Nuevo;
	}

	*hHash = usbHash;

finCSPUTILS_HANDLE_Nuevo:

	if ( ! ok ) { 
	
		if ( usbHash ) {
			if ( usbHash->hHash ) {
				delete usbHash->hHash;
				usbHash->hHash = 0;
			}

			delete usbHash;
			usbHash = NULL;
		}
	}

	return ok;
}


void CSPUTILS_HANDLE_Liberar (HUSBCRYPTHASH *hHash)
{

	if ( hHash ) {
		delete hHash->hHash;
		hHash->hHash = 0;
		delete hHash;
	}

}




/* Importa un blob cifrado en el csp subyacente
 */

BOOL CSPUTILS_Subyacente_ImportarCipheredBlob ( HUSBCRYPTPROV *hProv, BYTE bloque[TAM_BLOQUE], DWORD algId, HCRYPTKEY *hKey )
{
	char pwd[CLUI_CSP_MAX_PASS_LEN];
	int ret;
	HCRYPTKEY hSessKey;
	int intento;
	BOOL retry;
	BYTE salt[20];
	DWORD saltSize, iterCount;

	if ( *(bloque + 1) != BLOQUE_CIPHER_PRIVKEY_BLOB ) 
		return FALSE;

	if ( algId == AT_KEYEXCHANGE ) {
		memcpy(salt, BLOQUE_CIPHPRIVKEYBLOB_Get_Salt(bloque), 20);
		saltSize = 20;
		iterCount = BLOQUE_CIPHPRIVKEYBLOB_Get_IterCount(bloque);
		hSessKey = hProv->hKeySessExchange;
	} else {
		memcpy(salt, BLOQUE_CIPHPRIVKEYBLOB_Get_Salt(bloque), 20);
		saltSize = 20;
		iterCount = BLOQUE_CIPHPRIVKEYBLOB_Get_IterCount(bloque);
		hSessKey = hProv->hKeySessSignature;
	}

	/* Si no se pidi� previamente la password la pedimos. Tendremos tres
	 * intentos
	 */

	DWORD idDesc, idPIN;

	idDesc  = CSP_IDIOMA_DLGPASSWORD_KEY_DESCRIPCION;
	idPIN   = CSP_IDIOMA_DLGPASSWORD_KEY;
	intento = 0;

	do {

		if ( ! hSessKey ) {

			idDesc = intento == 0 ? CSP_IDIOMA_DLGPASSWORD_KEY_DESCRIPCION : CSP_IDIOMA_DLGPASSWORD_KEY_DESC_INCORRECTA;

			ret = hProv->lpCLUI_CSP_AskDoublePassphrase(hProv->hWindow, CSP_IDIOMA_Get(idDesc), pwd);
			if ( ret == CLUI_CSP_ERR || ret == CLUI_CSP_CANCEL ) {
				return FALSE;
			}
			

			if ( ! CAPI_PKCS5_3DES_PBE_Init_Ex (pwd, 
												salt,
												saltSize,
												iterCount,
												hProv->hpProv,
												&hSessKey) ) 
			{
				SecureZeroMemory(pwd, CLUI_CSP_MAX_PASS_LEN);
				return FALSE;
			}

		}

		/* Ahora importamos el blob. Si la importaci�n provoca alg�n error
		 * pedimos de nuevo la contrase�a
		 */

		if ( ! CryptImportKey(*(hProv->hpProv), 
							  BLOQUE_CIPHPRIVKEYBLOB_Get_Objeto(bloque),
							  BLOQUE_CIPHPRIVKEYBLOB_Get_Tam(bloque), 
							  hSessKey,
							  CRYPT_EXPORTABLE,
							  hKey) )
		{
			DWORD err = GetLastError();
			SecureZeroMemory(pwd, CLUI_CSP_MAX_PASS_LEN);
			CryptDestroyKey(hSessKey);
			hSessKey = 0;

			if ( err == NTE_BAD_DATA ) {
				retry = TRUE;
			} else {
				return FALSE;
			}
		} else
			retry = FALSE;

		++intento;

	} while ( retry && intento < 3 );

	SecureZeroMemory(pwd, CLUI_CSP_MAX_PASS_LEN);

	if ( retry ) 
		return FALSE;

	if ( algId == AT_SIGNATURE )
		hProv->hKeySessSignature = hSessKey;
	else
		hProv->hKeySessExchange = hSessKey;

	return TRUE;

}




BOOL CSPUTILS_KeyContainer_KeyExists (HUSBCRYPTPROV *hProv, DWORD dwAlgId, BOOL *bExists)
{
	unsigned char block[TAM_BLOQUE], *id, zeroId[20];

	if ( ! CSPUTILS_CLOS_Connect(hProv, FALSE) ) 
		return FALSE;

	if ( LIBRT_LeerBloqueCrypto(hProv->hpDispositivo, hProv->hsContainer, block) != 0 ) {
		CSPUTILS_CLOS_Disconnect(hProv);
		return FALSE;
	}

	CSPUTILS_CLOS_Disconnect(hProv);

	switch ( dwAlgId ) {
		case AT_KEYEXCHANGE:
			id = BLOQUE_KeyContainer_Get_ID_Exchange(block, hProv->szContainer);
			break;
		case AT_SIGNATURE:
			id = BLOQUE_KeyContainer_Get_ID_Signature(block, hProv->szContainer);
			break;
	}

	memset(zeroId, 0, 20);
	if ( memcmp(id, zeroId, 20) == 0 ) 
		*bExists = FALSE;
	else
		*bExists = TRUE;

	return TRUE;
}






BOOL CSPUTILS_CLOS_Connect ( HUSBCRYPTPROV *hUsbProv, BOOL auth )
{
	int nDispositivos, i;
	unsigned char *dispositivos[MAX_DEVICES];
	BOOL ret = TRUE;
	int ret_clui;
	char *szAuxPass = NULL;

	LOG_BeginFunc(5);

	LOG_Debug(1, "hUsbProv->auth: %d", hUsbProv->auth);

	if ( ! hUsbProv ) {
		LOG_EndFunc(5, FALSE);
		return FALSE;
	}

	if ( hUsbProv->conectado ) {
		if ( auth ) {
			if ( hUsbProv->auth ) {
				LOG_EndFunc(5, TRUE);
				return TRUE;
			}
		} else {
			if ( ! hUsbProv->auth ) {
				LOG_EndFunc(5, TRUE);
				return TRUE;
			}
		}
	}

	/* Siempre utilizamos el primer dispositivo insertado. Si hay m�s de uno insertado pues no puedes
	 * interactuar con �l. Damos tres intentos al usuario
	 */

	if ( ! *(hUsbProv->szDevice) ) {

		if ( LIBRT_ListarDispositivos(&nDispositivos, dispositivos) != 0 ) {
			LOG_EndFunc(5, FALSE);
			ret = FALSE;
			goto endCSPUTILS_CLOS_Connect;
		}

		if ( dispositivos == 0 ) {
			LOG_EndFunc(5, FALSE);
			ret = FALSE;
			goto endCSPUTILS_CLOS_Connect;
		}

		strcpy(hUsbProv->szDevice, (char *) dispositivos[0]);

		for ( i = 0 ; i < nDispositivos ; i++ )
			free(dispositivos[i]);

	}

	// Pedimos el PIN si no tenemos la password y nos piden autenticaci�n

	if ( auth ) {

		if ( hUsbProv->conectado ) {
			LIBRT_FinalizarDispositivo(hUsbProv->hpDispositivo);
			hUsbProv->conectado = FALSE;
			hUsbProv->auth = FALSE;
		}

		// Si tenemos cacheada la passsword la cogemos de cach�
		// Si no seguimos a nuestro rollo

		if ( CSPUTILS_PASSCACHE_Get_Password(hUsbProv) ) {

			LOG_Msg(1, "PASSWORD CACHEDADA");

			if ( LIBRT_IniciarDispositivo((unsigned char *) hUsbProv->szDevice, hUsbProv->szPass, hUsbProv->hpDispositivo) != 0 ) {
				ret = FALSE;
				goto endCSPUTILS_CLOS_Connect;
			}
		} else {

			LOG_Msg(1, "PASSWORD NO CACHEADA");

			szAuxPass = CSPUTILS_PASSPOOL_New();
			if ( ! szAuxPass ) {
				LOG_EndFunc(5, FALSE);
				ret = FALSE;
				goto endCSPUTILS_CLOS_Connect;
			}


			DWORD intentos = 0;
			char *desc;

			desc = CSP_IDIOMA_Get(CSP_IDIOMA_DLGPASSWORD_DESCRIPCION);

			do {

				LOG_Msg(1, "ANTES de MOSTRAR VENTANA");

				ret_clui = hUsbProv->lpCLUI_CSP_AskGlobalPassphrase(
											   //hUsbProv->hWindow, 
											   NULL,
											   FALSE, 
											   TRUE, 
											   FALSE, 
											   desc,
											   hUsbProv->szDevice,
											   szAuxPass);

				if ( ret_clui == CLUI_CSP_ERR ) {
					ret = FALSE;
					LOG_EndFunc(5, ret);
					goto endCSPUTILS_CLOS_Connect;
				} else if ( ret_clui == CLUI_CSP_CANCEL ) {
					ret = FALSE;
					LOG_EndFunc(5, ret);
					goto endCSPUTILS_CLOS_Connect;
				}

				LOG_Msg(1,"DESPUES DE MOSTRAR VENTANA");

				if ( LIBRT_IniciarDispositivo((unsigned char *) hUsbProv->szDevice, szAuxPass, hUsbProv->hpDispositivo) != 0 ) {

					/* Permitimos el fallo 3 veces. Asumimos que el mismo se debe
					 * a una contrase�a incorrecta.
					 */

					SecureZeroMemory(szAuxPass, CLUI_CSP_MAX_PASS_LEN);

					intentos++;
					if ( intentos == 3 ) {
						ret = FALSE;
						LOG_EndFunc(5, ret);
						goto endCSPUTILS_CLOS_Connect;
					}
					desc = CSP_IDIOMA_Get(CSP_IDIOMA_DLGPASSWORD_DESC_INCORRECTA);

				} else {

					/* Inicializaci�n correcta --> PIN correcto. Continuamos
					 */

					break;
				}

			} while ( 1 );

			memcpy(hUsbProv->idDispositivo, hUsbProv->hpDispositivo->idDispositivo, 20);
			hUsbProv->bIdAvailable = TRUE;
			CSPUTILS_PASSCACHE_Set_Password(hUsbProv, szAuxPass);
			hUsbProv->szPass = szAuxPass;

		} // else 

		/* Aqu� hemos conseguido iniciar una sesi�n autenticada
		 */

		hUsbProv->conectado = TRUE;
		hUsbProv->auth      = TRUE;
		hUsbProv->nodisp    = FALSE;

	} else {

		
		// Nos piden iniciar una sesi�n no autenticada

		if ( hUsbProv->conectado )  {
			LIBRT_FinalizarDispositivo(hUsbProv->hpDispositivo);
			hUsbProv->conectado = FALSE;
			hUsbProv->auth = FALSE;
		}

		if ( LIBRT_IniciarDispositivo((unsigned char *) hUsbProv->szDevice, NULL, hUsbProv->hpDispositivo) != 0 ) {
			ret = FALSE;
			LOG_EndFunc(5, ret);
			goto endCSPUTILS_CLOS_Connect;
		}

		hUsbProv->conectado   = TRUE;
		hUsbProv->auth        = FALSE;
	}


endCSPUTILS_CLOS_Connect:

	if ( ! ret ) {
		if ( szAuxPass ) {
			CSPUTILS_PASSPOOL_Free(szAuxPass);
			szAuxPass = NULL;
		}
	}

	LOG_EndFunc(5, ret);

	return ret;

}


BOOL CSPUTILS_CLOS_Disconnect ( HUSBCRYPTPROV *hUsbProv )
{
	if ( ! hUsbProv )
		return FALSE;

	LIBRT_FinalizarDispositivo(hUsbProv->hpDispositivo);

	hUsbProv->conectado   = FALSE;
	hUsbProv->auth        = FALSE;
/*
    hUsbProv->hsExchange  = -1;
	hUsbProv->hsSignature = -1;
	hUsbProv->hsContainer = -1;
*/
	return TRUE;
}




BOOL CSPUTILS_PASSCACHE_Get_Password ( HUSBCRYPTPROV *hUsbProv )
{
	map<string, PASS_CACHE_INFO *>::iterator i;
	PASS_CACHE_INFO *info;

	WaitForSingleObject(g_hMutexPassCache, 10000);

	if ( ! hUsbProv ) {
		ReleaseMutex(g_hMutexPassCache);
		return FALSE;
	}

	i = g_mapPassCache.find(hUsbProv->szDevice);
	if ( i == g_mapPassCache.end() ) {
		ReleaseMutex(g_hMutexPassCache);
		return FALSE;
	}
	
	info     = i->second;
	info->ts = time(NULL);

	// Si el thread limpia cach�s marco el elemento para ser eliminado (password == NULL)
	// entonces lo borramos el elemento y devolvemos FALSE

	if ( ! info->pass ) {
		g_mapPassCache.erase(hUsbProv->szDevice);
		if ( hUsbProv->szPass ) 
			hUsbProv->szPass = NULL;
		ReleaseMutex(g_hMutexPassCache);
		return FALSE;
	}

	// Comparo los identificadores de los dispositivos para ver si puedo o no acceder

	if ( hUsbProv->bIdAvailable ) {
		USBCERTS_HANDLE hClauer;

		if ( LIBRT_IniciarDispositivo((unsigned char *) hUsbProv->szDevice, NULL, &hClauer) != 0 ) {
			CSPUTILS_PASSPOOL_Free(info->pass);
			info->pass = NULL;
			g_mapPassCache.erase(hUsbProv->szDevice);
			hUsbProv->szPass = NULL;
			ReleaseMutex(g_hMutexPassCache);
			delete info;
			return FALSE;
		}
		LIBRT_FinalizarDispositivo(&hClauer);

		if ( memcmp(hClauer.idDispositivo, hUsbProv->idDispositivo, 20) != 0 ) {
			CSPUTILS_PASSPOOL_Free(info->pass);
			info->pass = NULL;
			g_mapPassCache.erase(hUsbProv->szDevice);
			hUsbProv->szPass = NULL;
			ReleaseMutex(g_hMutexPassCache);
			delete info;
			return FALSE;
		}
		
	}

	hUsbProv->szPass = info->pass;
	ReleaseMutex(g_hMutexPassCache);

	return TRUE;
}



BOOL CSPUTILS_PASSCACHE_Set_Password ( HUSBCRYPTPROV *hUsbProv, char *szPassword )
{
	PASS_CACHE_INFO *info;
	
	info = new PASS_CACHE_INFO;
	if ( ! info )
		return FALSE;

	WaitForSingleObject(g_hMutexPassCache, 10000);

	info->pass = szPassword;
	memcpy(info->id, hUsbProv->hpDispositivo->idDispositivo, 20);
	g_mapPassCache[hUsbProv->szDevice] = info;

	ReleaseMutex(g_hMutexPassCache);

	return TRUE;
}


/*****************************************************************************************

   Utilidades para la gesti�n de la "piscina" (pool) de passwords.

   La idea es reservar y bloquear en memoria RAM una p�gina. En esa zona almacenaremos
   todas las passwords cacheadas. El tama�o de las password es de 128 bytes (contando '\0'),
   y el primer byte indica si ocupado o no, por lo tanto el n�mero de passwords simultaneas 
   que podemos manejar es de pageSize/128

*/


static char * g_passPool = NULL;
static DWORD g_dwPassPoolSize = 0;


BOOL CSPUTILS_PASSPOOL_Init ( void )
{
	SYSTEM_INFO si;

	LOG_BeginFunc(5);

	GetSystemInfo(&si);
	g_dwPassPoolSize = si.dwPageSize;

	g_passPool = (char *) VirtualAlloc(NULL, g_dwPassPoolSize, MEM_COMMIT | MEM_RESERVE, PAGE_READWRITE);
	if ( ! g_passPool ) {
		LOG_EndFunc(5, FALSE);
		return FALSE;
	}
	if ( ! VirtualLock(g_passPool, g_dwPassPoolSize) ) {
		LOG_EndFunc(5, FALSE);
		VirtualFree(g_passPool, 0, MEM_RELEASE);
		return FALSE;
	}
	ZeroMemory(g_passPool, g_dwPassPoolSize);

	LOG_EndFunc(5, TRUE);

	return TRUE;
}



BOOL CSPUTILS_PASSPOOL_Fin  ( void )
{
	LOG_BeginFunc(5);

	if ( ! g_passPool ) {
		LOG_EndFunc(5, FALSE);
		return FALSE;
	}
	SecureZeroMemory(g_passPool, g_dwPassPoolSize);
	if ( ! VirtualUnlock(g_passPool, g_dwPassPoolSize) ) {
		LOG_EndFunc(5, FALSE);
		return FALSE;
	}
	if ( ! VirtualFree(g_passPool, 0, MEM_RELEASE) ) {
		LOG_EndFunc(5, FALSE);
		return FALSE;
	}

	LOG_EndFunc(5, TRUE);

	return TRUE;
}



char * CSPUTILS_PASSPOOL_New  ( void )
{
	char *p = g_passPool;
	DWORD max;

	LOG_BeginFunc(5);

	if ( ! p ) {
		LOG_EndFunc(5, FALSE);
		return NULL;
	}

	max = g_dwPassPoolSize - CLUI_CSP_MAX_PASS_LEN + 1;
	while ( ((p - g_passPool) <= max) && *p ) 
		p += CLUI_CSP_MAX_PASS_LEN + 1;
	if ( (p-g_passPool) >= max ){
		LOG_EndFunc(5, FALSE);
		return NULL;
	}

	LOG_EndFunc(5, TRUE);

	return p+1;
}



void CSPUTILS_PASSPOOL_Free ( char *pwd )
{
	if ( ! pwd )
		return;
	SecureZeroMemory(pwd, CLUI_CSP_MAX_PASS_LEN);
	*(pwd-1) = 0;
}




/******************************************************************************************
 * Utilidades varias
 *
 */



/* Convierte un PRIVATE KEY BLOB en claro en una llave privada PKCS#1
 */

#define P1_PVK_ELEMS	9
BOOL CSPUTILS_PrivateKeyBlobToPkcs1PrivateKey ( BYTE *blob, DWORD dwBlobSize, BYTE *pk, DWORD *dwPkSize )
{
	RSAPUBKEY *pubKey;
	CRYPT_SEQUENCE_OF_ANY seq;
	CRYPT_UINT_BLOB n[P1_PVK_ELEMS];		// Necesito codificar 9 enteros
	LPVOID ders[P1_PVK_ELEMS];
	DWORD dwDerSizes[P1_PVK_ELEMS], dwTotalSize;
	DWORD dwVersion = 0;
	int i;

	BOOL ret = TRUE;

	if ( ! blob )
		return FALSE;
	if ( ! dwBlobSize )
		return FALSE;

	ZeroMemory(dwDerSizes, P1_PVK_ELEMS*sizeof(DWORD));
	ZeroMemory(ders, P1_PVK_ELEMS*sizeof(void *));
	ZeroMemory(n, P1_PVK_ELEMS*sizeof(CRYPT_UINT_BLOB));
	ZeroMemory(&seq, sizeof(CRYPT_SEQUENCE_OF_ANY));
	
	/* Empezamos creando los blobs codificados para los enteros
	 * version, e, n, p, q, exponen1, exponent2, coeficient, privateExponent
	 */

	pubKey = (RSAPUBKEY *) (blob+sizeof(BLOBHEADER));

	/* Version */
	n[0].cbData = sizeof(DWORD);
	n[0].pbData = (LPBYTE) &dwVersion;

	/* n */
	n[1].cbData = pubKey->bitlen / 8;
	n[1].pbData = (LPBYTE) (blob + sizeof(BLOBHEADER) + sizeof(RSAPUBKEY));

	/* e */
	n[2].cbData = sizeof(DWORD);
	n[2].pbData = (LPBYTE) &(pubKey->pubexp);

	/* private exponent */
	n[3].cbData = pubKey->bitlen / 8;
	n[3].pbData = (LPBYTE) (blob + sizeof(BLOBHEADER) + sizeof(RSAPUBKEY)
		+ pubKey->bitlen / 8 + 5 * pubKey->bitlen / 16);

	/* prime1 */
	n[4].cbData = pubKey->bitlen / 16;
	n[4].pbData = (LPBYTE) (blob + sizeof(BLOBHEADER) + sizeof(RSAPUBKEY) 
		+ pubKey->bitlen / 8);

	/* prime 2 */
	n[5].cbData = pubKey->bitlen / 16;
	n[5].pbData = (LPBYTE) (blob + sizeof(BLOBHEADER) + sizeof(RSAPUBKEY) 
		+ pubKey->bitlen / 8 + pubKey->bitlen / 16);

	/* exponent 1 */
	n[6].cbData = pubKey->bitlen / 16;
	n[6].pbData = (LPBYTE) (blob + sizeof(BLOBHEADER) + sizeof(RSAPUBKEY) 
		+ pubKey->bitlen / 8 + 2 * pubKey->bitlen / 16);

	/* exponent 2 */
	n[7].cbData = pubKey->bitlen / 16;
	n[7].pbData = (LPBYTE) (blob + sizeof(BLOBHEADER) + sizeof(RSAPUBKEY) 
		+ pubKey->bitlen / 8 + 3 * pubKey->bitlen / 16);

	/* coefficient */
	n[8].cbData = pubKey->bitlen / 16;
	n[8].pbData = (LPBYTE) (blob + sizeof(BLOBHEADER) + sizeof(RSAPUBKEY)
		+ pubKey->bitlen / 8 + 4 * pubKey->bitlen / 16);

	/* Codificamos cada una de las estructuras
	 */

	dwTotalSize = 0;
	for ( i = 0 ; i < P1_PVK_ELEMS ; i++ ) {

		if ( ! CryptEncodeObjectEx(X509_ASN_ENCODING, 
								   X509_MULTI_BYTE_UINT, 
								   n+i, 
								   0,
								   NULL, 
								   NULL, 
								   &(dwDerSizes[i]) ) )
		{	
			goto errCSPUTILS_PrivateKeyBlobToPkcs1PrivateKey;
		}
		ders[i] = new BYTE[dwDerSizes[i]];
		if ( ! ders[i] ) 
			goto errCSPUTILS_PrivateKeyBlobToPkcs1PrivateKey;
		
		if ( ! CryptEncodeObjectEx(X509_ASN_ENCODING, 
								   X509_MULTI_BYTE_UINT, 
								   n+i, 
								   0,
								   NULL, 
								   ders[i], 
								   &(dwDerSizes[i])) )
		{	
			goto errCSPUTILS_PrivateKeyBlobToPkcs1PrivateKey;
		}
		dwTotalSize += dwDerSizes[i];
	}

	/* Constru�mos la secuencia 
	 */

	seq.cValue  = P1_PVK_ELEMS;
	seq.rgValue = new CRYPT_DER_BLOB[P1_PVK_ELEMS];
	if ( ! seq.rgValue )
		goto errCSPUTILS_PrivateKeyBlobToPkcs1PrivateKey;
	for ( i = 0 ; i < P1_PVK_ELEMS ; i++ ) {
		seq.rgValue[i].cbData = dwDerSizes[i];
		seq.rgValue[i].pbData = (BYTE *) ders[i];
	}
	
	/* Codificamos la secuencia
	 */

	*dwPkSize = 0;
	if ( ! CryptEncodeObject(X509_ASN_ENCODING, X509_SEQUENCE_OF_ANY, &seq, NULL, dwPkSize) )
		goto errCSPUTILS_PrivateKeyBlobToPkcs1PrivateKey;
	if ( pk ) 
		if ( ! CryptEncodeObject(X509_ASN_ENCODING, X509_SEQUENCE_OF_ANY, &seq, pk, dwPkSize) ) 
			goto errCSPUTILS_PrivateKeyBlobToPkcs1PrivateKey;

	/* cleanup
	 */

	for ( i = 0 ; i < P1_PVK_ELEMS ; i++ ) {
		SecureZeroMemory(ders[i], dwDerSizes[i]);
		delete [] ders[i];
		ders[i] = NULL;
	}
	SecureZeroMemory(seq.rgValue, P1_PVK_ELEMS*sizeof(CRYPT_DER_BLOB));
	delete [] seq.rgValue;
	seq.rgValue = NULL;
	
	return TRUE;

errCSPUTILS_PrivateKeyBlobToPkcs1PrivateKey:

	for ( i = 0 ; i < P1_PVK_ELEMS && ders[i] ; i++ ) {
		SecureZeroMemory(ders[i], dwDerSizes[i]);
		delete [] ders[i];
		ders[i] = NULL;
	}
	if ( seq.rgValue ) {
		SecureZeroMemory(seq.rgValue, P1_PVK_ELEMS*sizeof(CRYPT_DER_BLOB));
		delete [] seq.rgValue;
		seq.rgValue = NULL;
	}

	return FALSE;
}




/* Convierte a formato PEM una llave privada
 */

BOOL CSPUTILS_Pkcs1PrivateKeyToPEM ( BYTE *pk, DWORD dwPkSize, BYTE *pem, DWORD *dwPemSize )
{
	unsigned long iPemSize;

	if ( ! pk )
		return FALSE;
	if ( ! dwPemSize )
		return FALSE;

	B64_Codificar(pk, dwPkSize, NULL, &iPemSize);
	iPemSize += 62;   // Header and footer
	if ( pem ) {

		if ( *dwPemSize < iPemSize )
			return FALSE;

		memcpy(pem, "-----BEGIN RSA PRIVATE KEY-----\n", 32);
		B64_Codificar(pk, dwPkSize, pem+32, dwPemSize);
		memcpy(pem+32+*dwPemSize, "-----END RSA PRIVATE KEY-----\n", 31);

	} 

	*dwPemSize = iPemSize;

	return TRUE;

}


