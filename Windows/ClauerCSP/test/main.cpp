#include <windows.h>
#include <wincrypt.h>

//#include <LIBRT/LIBRT.h>
//#include <CRYPTOWrapper/CRYPTOWrap.h>
//#include <CAPI_PKCS5/CAPI_PKCS5.h>

#include <iostream>
#include <iomanip>

#include "../privadas.h"


using namespace std;


#if 0


void EnumerarContainersEnhanced (BOOL borrar)
{
	HCRYPTPROV hProv, hProv2;
	DWORD len=999;
	BYTE data[1000];
	char c;

	if ( !CryptAcquireContext(&hProv, NULL, "Microsoft Enhanced Cryptographic Provider v1.0", PROV_RSA_FULL, CRYPT_VERIFYCONTEXT) ) {
		cerr << "ERROR adquiriendo contexto" << endl;
		return;
	}


	if ( !CryptGetProvParam(hProv, PP_ENUMCONTAINERS, data, &len, CRYPT_FIRST) ) {
		if ( GetLastError() == ERROR_NO_MORE_ITEMS ) {
			cout << "No hay containers" << endl;
		} else
			cerr << "ERROR en enum 1" << endl;
		return;
	}

	do {

		cout << "CONTAINER: " << data << endl << flush;
		if ( borrar ) {
			c = 0;
			cout << "�Borrar? (s/n) " << flush;

			cin >> c;

			if ( c == 's' ) {
				if ( !CryptAcquireContext(&hProv2, (const char *)data, "Microsoft Enhanced Cryptographic Provider v1.0", PROV_RSA_FULL, CRYPT_DELETEKEYSET) ) {
					cerr << "ERROR. No se pudo borrar container" << endl;
				}
			}
		}

		if ( !CryptGetProvParam(hProv, PP_ENUMCONTAINERS, data, &len, 0) ) {
			if ( GetLastError() == ERROR_NO_MORE_ITEMS ) {
				break;
			} else
				cerr << "ERROR en enum 1" << endl;
			return;
		}

	} while (1);

	CryptReleaseContext(hProv, 0);
}



void GenerarBlob (DWORD numBits, char *outFileName)
{
	HCRYPTPROV hProv;
	HCRYPTKEY hKey;
	DWORD blobLenSig, blobLenEx;
	BYTE *blobSig, *blobEx;

	if  (!CryptAcquireContext(&hProv, NULL, "Microsoft Enhanced Cryptographic Provider v1.0", PROV_RSA_FULL, CRYPT_VERIFYCONTEXT)) {
		cerr << "ERROR adquiriendo contexto: " << hex << GetLastError() << endl;
		return;
	}

	numBits = numBits << 16;
	
	cout << "Generando llaves" << endl;

	if ( !CryptGenKey(hProv, AT_SIGNATURE, numBits|CRYPT_EXPORTABLE, &hKey) ) {
		cerr << "ERROR generando llaves: " << hex << GetLastError() << endl;
		CryptReleaseContext(hProv,0);
		return;
	}

	cout << "Exportando" << endl;

	if (!CryptExportKey(hKey, NULL, PRIVATEKEYBLOB,0,NULL, &blobLenSig)) {
		cerr << "ERROR exportando llaves" << endl;
		CryptReleaseContext(hProv, 0);
		return;
	}

	blobSig = new BYTE[blobLenSig];

	if (!CryptExportKey(hKey, NULL, PRIVATEKEYBLOB, 0, blobSig, &blobLenSig)) {
		cerr << "ERROR exportando llaves: " << hex << GetLastError() << endl;
		CryptDestroyKey(hKey);
		CryptReleaseContext(hProv, 0);
		return;
	}

	CryptDestroyKey(hKey);

	if ( !CryptGenKey(hProv, AT_KEYEXCHANGE, numBits|CRYPT_EXPORTABLE, &hKey) ) {
		cerr << "ERROR generando llaves: " << hex << GetLastError() << endl;
		CryptReleaseContext(hProv,0);
		return;
	}

	cout << "Exportando" << endl;

	if (!CryptExportKey(hKey, NULL, PRIVATEKEYBLOB,0,NULL, &blobLenEx)) {
		cerr << "ERROR exportando llaves" << endl;
		CryptReleaseContext(hProv, 0);
		return;
	}

	blobEx = new BYTE[blobLenEx];

	if (!CryptExportKey(hKey, NULL, PRIVATEKEYBLOB, 0, blobEx, &blobLenEx)) {
		cerr << "ERROR exportando llaves: " << hex << GetLastError() << endl;
		CryptDestroyKey(hKey);
		CryptReleaseContext(hProv, 0);
		return;
	}

	CryptDestroyKey(hKey);

	CryptReleaseContext(hProv, 0);

	delete [] blobEx;
	delete [] blobSig;

}





void Probar_VERIFYCONTEXT(void)
{
	HCRYPTPROV hProv;
	HCRYPTKEY hKey;
	DWORD len = 0;
	BYTE blob[2000];

	if ( !CryptAcquireContext(&hProv, NULL, MS_ENHANCED_PROV, PROV_RSA_FULL, CRYPT_VERIFYCONTEXT) ) {
		cerr << "ERROR en Acquire: " << hex << GetLastError() << endl;
		return;
	}

	cout << "Generando llave..." << flush;

	if  (!CryptGenKey(hProv, AT_SIGNATURE, 0x08000000|CRYPT_EXPORTABLE, &hKey) ) {
		cerr << "ERROR en CryptGenKey: " << hex << GetLastError() << endl;
		return;
	}

	cout << " [OK]" << endl;

	len = 2000;
	if ( !CryptExportKey(hKey, NULL, PRIVATEKEYBLOB, 0, blob, &len) ) {
		cerr << "ERROR exportando llave: " << hex << GetLastError() << endl;
		return;
	}

	CryptReleaseContext(hProv,0);

	/* Ahora vamos a importar */

	if ( !CryptAcquireContext(&hProv, NULL, MS_ENHANCED_PROV, PROV_RSA_FULL, CRYPT_VERIFYCONTEXT) ) {
		cerr << "ERROR en AcquireContext 2: " << hex << GetLastError() << endl;
		return;
	}

	cout << "Importando..." << flush;

	if  ( !CryptImportKey(hProv, blob, len, NULL, CRYPT_EXPORTABLE, &hKey) ) {
		cerr << "ERROR en CryptImportKey: " << hex << GetLastError() << endl;
		return;
	}

	cout << " [OK]" << endl;

	CryptReleaseContext(hProv,0);

}





BOOL CSPUTILS_Blob_CalcularId (BYTE *blob, DWORD tamBlob, BYTE id[20])
{
	BLOBHEADER *hdr;
	HCRYPTPROV hProv;
	HCRYPTHASH hHash;
	RSAPUBKEY *pk;
	BYTE *auxId;
	DWORD tamAux;

	hdr = (BLOBHEADER *) blob;
	if ( (hdr->bType != PRIVATEKEYBLOB) && (hdr->bType != PUBLICKEYBLOB) ) 
		return FALSE;
	
	pk = (RSAPUBKEY *) (blob+sizeof(BLOBHEADER));
	tamAux = (pk->bitlen/8) + sizeof(DWORD);

	auxId = new BYTE [tamAux];
	if ( !auxId ) 
		return FALSE;
	
	memcpy(auxId, blob+sizeof(BLOBHEADER)+sizeof(RSAPUBKEY), pk->bitlen/8);
	memcpy(auxId+(pk->bitlen/8),&(pk->pubexp),sizeof(DWORD));





	for ( int i = 0 ; i < pk->bitlen/8 ; i++ )
		cout << setfill('0') << setw(2) << hex << (int) auxId[i] << " " << flush;
	cout << endl;







	if ( !CryptAcquireContext(&hProv, NULL, "Microsoft Enhanced Cryptographic Provider v1.0",PROV_RSA_FULL, CRYPT_VERIFYCONTEXT) ) {
		delete [] auxId;
		auxId = NULL;
		return FALSE;
	}

	if ( !CryptCreateHash(hProv, CALG_SHA1, 0, 0, &hHash) ) {
		delete [] auxId;
		auxId = NULL;
		CryptReleaseContext(hProv,0);
		return FALSE;
	}

	if ( !CryptHashData(hHash, auxId, tamAux, 0) ) {
		delete [] auxId;
		auxId = NULL;
		CryptDestroyHash(hHash);
		CryptReleaseContext(hProv,0);
		return FALSE;
	}

	delete [] auxId;
	auxId = NULL;

	tamAux = 20;
	if ( !CryptGetHashParam(hHash,HP_HASHVAL,id,&tamAux,0) ) {
		CryptDestroyHash(hHash);
		CryptReleaseContext(hProv,0);
		return FALSE;
	}

	CryptDestroyHash(hHash);
	CryptReleaseContext(hProv,0);

	return TRUE;

}









int PruebaIdentificadores ()
{
	BYTE *blob, *llave;
	DWORD tamBlob, tamLlave;
	FILE *fp;
	BYTE id[20], idLlave[20], bloque[TAM_BLOQUE];
	USBCERTS_HANDLE h;

/*	LIBRT_Ini();


	if ( LIBRT_IniciarDispositivo(1, "10240031", &h) != 0 ) {
		cerr << "ERROR Iniciando dispositivo" << endl;
		return 1;
	}

	if ( LIBRT_LeerBloqueCrypto(&h, 4, bloque) != 0 ) {
		cerr << "ERROR leyendo bloque crypto" << endl;
		return 1;
	}

	fp = fopen("C:\\llave", "wb");
	fwrite(BLOQUE_LLAVEPRIVADA_Get_Objeto(bloque), BLOQUE_LLAVEPRIVADA_Get_Tam(bloque), 1, fp);
	fclose(fp);

	if ( LIBRT_LeerBloqueCrypto(&h, 5, bloque) != 0 ) {
		cerr << "ERROR leyendo bloque crypto 2" << endl;
		return 1;
	}

	fp = fopen("C:\\blob", "wb");
	fwrite(BLOQUE_PRIVKEYBLOB_Get_Objeto(bloque), BLOQUE_PRIVKEYBLOB_Get_Tam(bloque), 1, fp);
	fclose(fp);

	LIBRT_FinalizarDispositivo(&h);

	return 0;
*/

	CRYPTO_Ini();

	fp = fopen("C:\\blob", "rb");
	fseek(fp, 0, SEEK_END);
	tamBlob = ftell(fp);
	fseek(fp, 0, SEEK_SET);
	blob = new BYTE [tamBlob];
	fread(blob, 1, tamBlob, fp);
	fclose(fp);

	fp = fopen("C:\\llave", "rb");
	fseek(fp, 0, SEEK_END);
	tamLlave = ftell(fp);
	fseek(fp, 0, SEEK_SET);
	llave = new BYTE [tamLlave];
	fread(llave, 1, tamLlave, fp);
	fclose(fp);

	if ( ! CSPUTILS_Blob_CalcularId (blob, tamBlob, id) ) {
		cerr << "ERROR calculando id del blob" << endl;
		return 1;
	}

	if ( CRYPTO_LLAVE_PEM_Id (llave, tamLlave, 1, NULL, idLlave) != 0 ) {
		cerr << "ERROR calculando id de la llave" << endl;
		return 1;
	}

	printf("\n");

	cout << "ID del blob: " << flush;
	for ( int i = 0 ; i < 20 ; i ++ ) {
		cout << setw(2) << setfill('0') << hex << (int) id[i] << " " << flush;
	}
	cout << endl;


	cout << "ID de la llave: " << flush;
	for ( int i = 0 ; i < 20 ; i ++ ) {
		cout << setw(2) << setfill('0') << hex << (int) idLlave[i] << " " << flush;
	}
	cout << endl;

}






void importar ( void )
{
	HCRYPTPROV hProv;
	HCRYPTKEY hKey;

	if ( ! CryptAcquireContext(&hProv, "kakakaka", "UJI Clauer CSP", PROV_RSA_FULL, CRYPT_NEWKEYSET) ) {
		printf("e1\n");
		return;
	}


/*	if ( ! CryptGenKey(hProv, AT_SIGNATURE, 16384 << 16, &hKey) ) {
		printf("e2\n");
		return;
	}
*/
	if ( ! CryptImportKey(hProv, privSig, sizeof privSig, 0, CRYPT_USER_PROTECTED, &hKey) ) {
		printf("e2\n");
		return;
	}
	
	/*if ( ! CryptGetUserKey(hProv, AT_SIGNATURE, &hKey) ) {
		printf("e3\n");
		return;
	}*/

	CryptReleaseContext(hProv,0);

}
#endif

#include <tchar.h>
#include <strsafe.h>


void ErrorExit(LPTSTR lpszFunction, DWORD dw) 
{ 
    LPVOID lpMsgBuf;
    LPVOID lpDisplayBuf;

    FormatMessage(
        FORMAT_MESSAGE_ALLOCATE_BUFFER | 
        FORMAT_MESSAGE_FROM_SYSTEM,
        NULL,
        dw,
        MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT),
        (LPTSTR) &lpMsgBuf,
        0, NULL );

    lpDisplayBuf = (LPVOID)LocalAlloc(LMEM_ZEROINIT, 
        (lstrlen((LPCTSTR)lpMsgBuf)+lstrlen((LPCTSTR)lpszFunction)+40)*sizeof(TCHAR)); 
    StringCchPrintf((LPTSTR)lpDisplayBuf, 
        LocalSize(lpDisplayBuf),
        TEXT("%s failed with error %d: %s"), 
        lpszFunction, dw, lpMsgBuf); 
    MessageBox(NULL, (LPCTSTR)lpDisplayBuf, TEXT("Error"), MB_OK); 

    LocalFree(lpMsgBuf);
    LocalFree(lpDisplayBuf);
    ExitProcess(dw); 
}

int main ()
{

	HCRYPTPROV hProv;
	HCRYPTHASH hHash;
	unsigned char hashToAdd[] = { 1,7,3,5,76,7,4,3,56,45,24,4,54,46,7,3,2,34,6 };
	unsigned char signature[1000];
	DWORD sigSize;
	WSADATA info;

	if ( ! CryptAcquireContext(&hProv, NULL, L"UJI Clauer CSP", PROV_RSA_FULL, CRYPT_NEWKEYSET) ) {
		ErrorExit(_T("JAJAJA"), GetLastError());
		fprintf(stderr, "[ERROR] Error en CryptAcquireContext: %x\n", GetLastError());
		return 1;
	}

	HCRYPTKEY hKey;
	if ( ! CryptGenKey(hProv, AT_SIGNATURE, 0, &hKey) ) {
		ErrorExit(_T("JAJAJA"), GetLastError());
		return 1;
	}

	if ( ! CryptCreateHash(hProv, CALG_SHA, 0, 0, &hHash) ) {
		fprintf(stderr, "[ERROR]CryptCreateHash: %x\n", GetLastError());
		return 1;
	}

	if ( ! CryptSetHashParam(hHash, HP_HASHVAL, hashToAdd, 0) ) {
		fprintf(stderr, "[ERROR] CryptSetHashParam: %x\n", GetLastError());
		return 1;
	}
	sigSize = 1000;
	if ( ! CryptSignHash(hHash, AT_SIGNATURE, NULL, 0, signature, &sigSize) ) {
		ErrorExit(_T("JAJAJA"), GetLastError());
		return 1;
	}

	printf("YA");
	fflush(stdout);
	


/*
	HCRYPTPROV hProv1, hProv2;
	HCRYPTKEY hKey, hSessKey;
	BYTE blob[10240];
	DWORD blobSize;
	char dev[] = "C:\\cryf_001.cla";
	char pwd[] = "";
	BYTE bloque[TAM_BLOQUE], *id;

	BYTE cont[MAX_PATH+1];
	DWORD contLen;

	USBCERTS_HANDLE hClauer;


	if ( ! CryptAcquireContext(&hProv1, NULL, "UJI Clauer CSP", PROV_RSA_FULL, CRYPT_VERIFYCONTEXT) ) {
		fprintf(stderr, "[ERROR] 1\n");
		return 1;
	}
	CryptReleaseContext(hProv1, 0);
*/

/*
	importar();
*/
/*	if ( ! CryptAcquireContext(&hProv1, "TestSuiteContainer", "UJI Clauer CSP", PROV_RSA_FULL, CRYPT_DELETEKEYSET) ) {
		printf("e1\n");
		return 1;
	}	
*/

/*	memset(bloque, 1,sizeof bloque);
	BLOQUE_Set_Cifrado(bloque);
	BLOQUE_CIPHPRIVKEYBLOB_Set_IterCount(bloque, 1111);

	printf("%ld\n", BLOQUE_CIPHPRIVKEYBLOB_Get_IterCount(bloque));
*/

/*	if ( ! CryptAcquireContext(&hProv1, "prueba cifrada", "UJI Clauer CSP", PROV_RSA_FULL, 0) ) {
		printf("E1\n");
		return 1;
	}
*/
/*	if ( ! CryptGenKey(hProv1, AT_KEYEXCHANGE, CRYPT_USER_PROTECTED, &hKey) ) {
		printf("e2\n");
		return 1;
	}
*/
/*	if ( ! CryptGetUserKey(hProv1, AT_KEYEXCHANGE, &hKey ) ) {
		printf("e3\n");
		return 1;
	}
*/
/*	LIBRT_Ini();
	if ( LIBRT_IniciarDispositivo((unsigned char *) dev, NULL, &hClauer) != 0 ) {
		printf("ERROR 1\n");
		return 1;
	}	

	if ( LIBRT_LeerBloqueCrypto(&hClauer, 1, bloque) ) {
		printf("ERROR 2\n");
		return 1;
	}

	id = BLOQUE_PRIVKEYBLOB_Get_Id(bloque);
	for ( int i = 0 ; i < 20 ; i++ ) {
		printf("%02x ", id[i]);
	}

	BYTE salt[20], ciphBlob[10240], iv[20];
	HCRYPTPROV hProv, hKey1;
	DWORD ciphBlobSize;
	char desc[31]="";

	memset(salt,0,20);
	memset(iv,0,20);
*/	
/*
	if ( ! CryptAcquireContext(&hProv, NULL, NULL, PROV_RSA_FULL, CRYPT_VERIFYCONTEXT) ) {
		printf("Error 33\n");
		return 1;
	}

	if ( ! CAPI_PKCS5_3DES_PBE_Init_Ex ("jajajaja", salt, 20, 2000, &hProv, &hKey1) ) {
		printf("Error 34\n");
		return 1;
	}

	memcpy(ciphBlob, BLOQUE_PRIVKEYBLOB_Get_Objeto(bloque), BLOQUE_PRIVKEYBLOB_Get_Tam(bloque));

	ciphBlobSize = BLOQUE_PRIVKEYBLOB_Get_Tam(bloque) - sizeof(BLOBHEADER);
	if ( ! CryptEncrypt(hKey1, 0, TRUE, 0, ciphBlob+sizeof(BLOBHEADER), &ciphBlobSize, 10240) ) {
		printf("Error 35\n");
		return 1;
	}

	putchar(ciphBlob[0]);
	putchar(ciphBlob[1]);
	putchar(ciphBlob[2]);

	BYTE bloqueCiph[TAM_BLOQUE];

	BLOQUE_Set_Cifrado(bloqueCiph);
	BLOQUE_CIPHPRIVKEYBLOB_Nuevo(bloqueCiph);
	BLOQUE_CIPHPRIVKEYBLOB_Set_Tam(bloqueCiph, ciphBlobSize+sizeof(BLOBHEADER));
	BLOQUE_CIPHPRIVKEYBLOB_Set_Id(bloqueCiph, BLOQUE_PRIVKEYBLOB_Get_Id(bloque));
	BLOQUE_CIPHPRIVKEYBLOB_Set_Salt(bloqueCiph, salt);
	BLOQUE_CIPHPRIVKEYBLOB_Set_Iv(bloqueCiph, iv);
	BLOQUE_CIPHPRIVKEYBLOB_Set_Desc(bloqueCiph, desc);
	BLOQUE_CIPHPRIVKEYBLOB_Set_IterCount(bloqueCiph, 2000);
	BLOQUE_CIPHPRIVKEYBLOB_Set_Objeto(bloqueCiph, ciphBlob, ciphBlobSize+sizeof(BLOBHEADER));

	printf("\n");
	printf("IterCount: %ld\n", BLOQUE_CIPHPRIVKEYBLOB_Get_IterCount(bloqueCiph));
	printf("Salt: ");
	for ( int i = 0 ; i < 20 ; i++ ) {
		printf("%02x", BLOQUE_CIPHPRIVKEYBLOB_Get_Salt(bloqueCiph)[i]);
	}
	printf("\n");
*/
/*printf("\n");
	BLOQUE_Set_Claro(bloque);

	printf("%d\n", *bloque);

	if ( LIBRT_EscribirBloqueCrypto(&hClauer, 1, bloque) != 0 ){
		printf("Erorr 36\n");
		return 1;
	}
*/


}



