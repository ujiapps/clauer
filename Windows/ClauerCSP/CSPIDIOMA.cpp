#include "CSPIDIOMA.h"

#include <stdio.h>

static char *idioma = NULL;
static char *etiquetas[NUM_ETIQUETAS];


/*! TRUE Ok
 *  FALSE error
 */

BOOL CSP_IDIOMA_Cargar (void)
{

	HKEY hKey;
	DWORD tamValue, dwType;
	char *iden;

	if ( idioma != NULL )
		return FALSE;

	if ( RegOpenKeyEx(HKEY_LOCAL_MACHINE, 
			     "SOFTWARE\\Universitat Jaume I\\Projecte Clauer", 
				 0,
				 KEY_QUERY_VALUE,
				 &hKey) != ERROR_SUCCESS ) 
	{
		return FALSE;
	}

	if ( RegQueryValueEx(hKey,"Idioma", NULL, NULL, NULL, &tamValue) != ERROR_SUCCESS ) {
		RegCloseKey(hKey);
		return FALSE;
	}

	if ( tamValue > 5 ) {
		RegCloseKey(hKey);
		return FALSE;
	}

	idioma = new char [tamValue];

	if ( !idioma ) {
		RegCloseKey(hKey);
		return FALSE;
	}

	if ( RegQueryValueEx(hKey,"Idioma", NULL, NULL, (BYTE *)idioma, &tamValue) != ERROR_SUCCESS ) {
		delete [] idioma;
		idioma = NULL;
		RegCloseKey(hKey);
		return FALSE;
	}


	if ( RegQueryValueEx(hKey, "iden", NULL, &dwType, NULL, &tamValue) != ERROR_SUCCESS ) {
		delete [] idioma;
		idioma = NULL;
		RegCloseKey(hKey);
		return FALSE;
	}
	if ( dwType != REG_SZ ) {
		delete [] idioma;
		idioma = NULL;
		RegCloseKey(hKey);
		return FALSE;
	}
	if ( tamValue > 50 ) {
		delete [] idioma;
		idioma = NULL;
		RegCloseKey(hKey);
		return FALSE;
	}
	iden = new char [tamValue];
	if ( ! iden ) {
		delete [] idioma;
		idioma = NULL;
		RegCloseKey(hKey);
		return FALSE;
	}
	if ( RegQueryValueEx(hKey, "iden", NULL, &dwType, (LPBYTE) iden, &tamValue) != ERROR_SUCCESS ) {
		delete [] idioma;
		delete [] iden;
		idioma = NULL;
		RegCloseKey(hKey);
		return FALSE;
	}
	RegCloseKey(hKey);

	/* Cargamos la tabla de etiquetas
	 */

	if ( strcmp(idioma, "1034") == 0 ) 
	{
		/* Espa�ol 
		 */

		if ( strcmp(iden, "catcert") == 0 )
				etiquetas[CSP_IDIOMA_DLGPASSWORD_DESCRIPCION]  = "Introduzca la contrase�a que protege su Clauer idCAT";
		else
				etiquetas[CSP_IDIOMA_DLGPASSWORD_DESCRIPCION]  = "Introduzca la contrase�a que protege su Clauer";

		if ( strcmp(iden, "catcert") == 0 )
			etiquetas[CSP_IDIOMA_DLGPASSWORD_CAPTION]      = "Contrase�a del Clauer idCAT";
		else
			etiquetas[CSP_IDIOMA_DLGPASSWORD_CAPTION]      = "Contrase�a del Clauer";

		etiquetas[CSP_IDIOMA_DLGPASSWORD_KEY_DESCRIPCION] = "Introduzca la contrase�a que protege el certificado";
		etiquetas[CSP_IDIOMA_DLGPASSWORD_KEY_CAPTION]     = "Contrase�a del certificado";
		etiquetas[CSP_IDIOMA_DLGPASSWORD_KEY]             = "Password:";

		etiquetas[CSP_IDIOMA_ACEPTAR]  = "Aceptar";
		etiquetas[CSP_IDIOMA_CANCELAR] = "Cancelar";
		etiquetas[CSP_IDIOMA_DLGPASSWORD_PIN]          = "Contrase�a:";

		if ( strcmp(iden, "catcert") == 0 )
			etiquetas[CSP_IDIOMA_NOCLAUER]             = "No hay Clauer idCAT. Introduzca el Clauer idCAT a utilizar";
		else
			etiquetas[CSP_IDIOMA_NOCLAUER]             = "No hay Clauer. Introduzca el Clauer a utilizar";

		etiquetas[CSP_IDIOMA_PROJECTE_CLAUER]		   = "UJI :: Proyecto Clauer";

		if ( strcmp(iden, "catcert") == 0 )
			etiquetas[CSP_IDIOMA_MASDEUNCLAUER]            = "M�s de un dispositivo Clauer idCAT insertado. Por favor, deje insertado �nicamente el que desee utilizar";
		else
			etiquetas[CSP_IDIOMA_MASDEUNCLAUER]            = "M�s de un dispositivo Clauer insertado. Por favor, deje insertado �nicamente el que desee utilizar";

		etiquetas[CSP_IDIOMA_CONTINUA_GVA]             = "Parece que utiliza una contrase�a d�bil.\r\nLe recomendamos que lo cambie lo antes posible";

		if ( strcmp(iden, "catcert") == 0 ) 
			etiquetas[CSP_IDIOMA_DLGPASSWORD_DESC_INCORRECTA] = "Contrase�a incorrecta. Tiene tres intentos. Introduzca la contrase�a que protege su Clauer idCAT";
		else
			etiquetas[CSP_IDIOMA_DLGPASSWORD_DESC_INCORRECTA] = "Contrase�a incorrecta. Tiene tres intentos. Introduzca la contrase�a que protege su Clauer";

		etiquetas[CSP_IDIOMA_DLGPASSWORD_KEY_DESC_INCORRECTA] = "Contrase�a incorrecta. Tiene tres intentos. Introduzca la contrase�a que protege el certificado";

		etiquetas[CSP_IDIOMA_NUEVA_PASSWORD_DESCRIPCION]  = "Introduzca la contrase�a que proteger� su llave privada";
		etiquetas[CSP_IDIOMA_NUEVA_PASSWORD_CONTRASENYA]  = "Contrase�a";
		etiquetas[CSP_IDIOMA_NUEVA_PASSWORD_CONFIRMACION] = "Confirmaci�n";

		etiquetas[CSP_IDIOMA_NUEVA_PASSWORD_CONCORDAR] = "Contrase�a y confirmaci�n deben coincidir. Int�ntelo de nuevo";
		etiquetas[CSP_IDIOMA_NUEVA_PASSWORD_CAPTION] = "Contrase�a para la protecci�n de llaves";

	} else if ( strcmp(idioma, "1027") == 0 )
	{
		/* Valenci�
		 */

		if ( strcmp(iden, "catcert") == 0 ) {
			etiquetas[CSP_IDIOMA_DLGPASSWORD_DESCRIPCION]  = "Introdueix la paraula de pas que protegeix el Clauer idCAT";
		} else
			etiquetas[CSP_IDIOMA_DLGPASSWORD_DESCRIPCION]  = "Introdueix la paraula de pas que protegeix el Clauer";

		if ( strcmp(iden, "catcert") == 0 )
			etiquetas[CSP_IDIOMA_DLGPASSWORD_CAPTION]      = "Paraula de pas del Clauer idCAT";
		else
			etiquetas[CSP_IDIOMA_DLGPASSWORD_CAPTION]      = "Paraula de pas del Clauer";

		etiquetas[CSP_IDIOMA_DLGPASSWORD_KEY_DESCRIPCION] = "Introdueix la paraula de pas que protegeix el certificat";
		etiquetas[CSP_IDIOMA_DLGPASSWORD_KEY_CAPTION] = "Paraula de pas del certificat";
		etiquetas[CSP_IDIOMA_DLGPASSWORD_KEY] = "Paraula de pas:";

		etiquetas[CSP_IDIOMA_ACEPTAR]  = "Acceptar";
		etiquetas[CSP_IDIOMA_CANCELAR] = "Cancel-lar";
		etiquetas[CSP_IDIOMA_DLGPASSWORD_PIN]          = "Paraula de pas:";		

		if ( strcmp(iden, "catcert") == 0 )
			etiquetas[CSP_IDIOMA_NOCLAUER]                 = "No hi ha Clauers al sistema. Introdueix el Clauer idCAT a utilitzar";
		else
			etiquetas[CSP_IDIOMA_NOCLAUER]                 = "No hi ha Clauers al sistema. Introdueix el Clauer a utilitzar";

		etiquetas[CSP_IDIOMA_PROJECTE_CLAUER]		   = "UJI :: Projecte Clauer";

		if ( strcmp(iden, "catcert") == 0 )
			etiquetas[CSP_IDIOMA_MASDEUNCLAUER] = "M�s d'un Clauer idCAT insertat al sistema. Per favor, deixe nom�s el que vulga utilitzar";
		else
			etiquetas[CSP_IDIOMA_MASDEUNCLAUER] = "M�s d'un Clauer insertat al sistema. Per favor, deixe nom�s el que vulga utilitzar";
		etiquetas[CSP_IDIOMA_CONTINUA_GVA]             = "Pareix que has introdu�t una paraula de pas feble.\r\nEt recomanem que el canvies el m�s aviat possible.";

		if ( strcmp(iden, "catcert") == 0 )
			etiquetas[CSP_IDIOMA_DLGPASSWORD_DESC_INCORRECTA] = "Paraula de pas incorrecta. Tens tres intents. Introdueix la paraula de pas que protegeix el seu Clauer idCAT";
		else
			etiquetas[CSP_IDIOMA_DLGPASSWORD_DESC_INCORRECTA] = "Paraula de pas incorrecta. Tens tres intents. Introdueix la paraula de pas que protegeix el seu Clauer";

		etiquetas[CSP_IDIOMA_DLGPASSWORD_KEY_DESC_INCORRECTA] = "Paraula de pas incorrecta. Tens tres intents. Introdueix la paraula de pas que protegeix el certificat";

		etiquetas[CSP_IDIOMA_NUEVA_PASSWORD_DESCRIPCION]  = "Introdueix la paraula de pas de la clau privada";
		etiquetas[CSP_IDIOMA_NUEVA_PASSWORD_CONTRASENYA]  = "Paraula de pas";
		etiquetas[CSP_IDIOMA_NUEVA_PASSWORD_CONFIRMACION] = "Confirmaci�";
		etiquetas[CSP_IDIOMA_NUEVA_PASSWORD_CONCORDAR]    = "La paraula de pas i la confirmaci� no concorden. Intenta-ho de nou";
		etiquetas[CSP_IDIOMA_NUEVA_PASSWORD_CAPTION] = "Paraula de pas per a la protecci� de claus";

	} else if ( strcmp(idioma, "1036") == 0 )
	{
		/* Franc�s
		 */

		if ( strcmp(iden, "catcert") == 0 )
			etiquetas[CSP_IDIOMA_DLGPASSWORD_DESCRIPCION]  = "Introduisez le PIN du Clauer idCAT";
		else
			etiquetas[CSP_IDIOMA_DLGPASSWORD_DESCRIPCION]  = "Introduisez le PIN du Clauer";

		if ( strcmp(iden, "catcert") == 0 )
			etiquetas[CSP_IDIOMA_DLGPASSWORD_CAPTION]      = "PIN du Clauer idCAT";
		else
			etiquetas[CSP_IDIOMA_DLGPASSWORD_CAPTION]      = "PIN du Clauer";

		etiquetas[CSP_IDIOMA_DLGPASSWORD_KEY_DESCRIPCION] = "Introduzca la contrase�a que protege el certificado";
		etiquetas[CSP_IDIOMA_DLGPASSWORD_KEY_CAPTION]     = "Contrase�a del certificado";
		etiquetas[CSP_IDIOMA_DLGPASSWORD_KEY]             = "Password:";

		etiquetas[CSP_IDIOMA_ACEPTAR]  = "Accepter";
		etiquetas[CSP_IDIOMA_CANCELAR] = "Annuler";
		etiquetas[CSP_IDIOMA_DLGPASSWORD_PIN]          = "PIN:";		

		if ( strcmp(iden, "catcert") == 0 )
			etiquetas[CSP_IDIOMA_NOCLAUER]                 = "Il n'y a pas de Clauers dans le syst�me. Introduisez le Clauer idCAT � utiliser";
		else
			etiquetas[CSP_IDIOMA_NOCLAUER]                 = "Il n'y a pas de Clauers dans le syst�me. Introduisez le Clauer � utiliser";

		etiquetas[CSP_IDIOMA_PROJECTE_CLAUER]		   = "UJI :: Projecte Clauer"; 

		if ( strcmp(iden, "catcert") == 0 )
			etiquetas[CSP_IDIOMA_MASDEUNCLAUER]            = "Plus d'une Clauer idCAT ins�r� au syst�me. Laissez seulement ce que vous voulez utiliser.";
		else
			etiquetas[CSP_IDIOMA_MASDEUNCLAUER]            = "Plus d'une Clauer ins�r� au syst�me. Laissez seulement ce que vous voulez utiliser.";

		etiquetas[CSP_IDIOMA_CONTINUA_GVA]             = "Il semble que vous utilisez un PIN faible.\r\nNous vous recommandons de le changer le plus rapidement possible.";

		if ( strcmp(iden, "catcert") == 0 )
			etiquetas[CSP_IDIOMA_DLGPASSWORD_DESC_INCORRECTA]     = "Invalid PIN. You have three attempts. Type in the Clauer idCAT's PIN";
		else
			etiquetas[CSP_IDIOMA_DLGPASSWORD_DESC_INCORRECTA]     = "Invalid PIN. You have three attempts. Type in the Clauer's PIN";


		etiquetas[CSP_IDIOMA_DLGPASSWORD_KEY_DESC_INCORRECTA] = "Invalid passphrase. You have three attempts. Type in the certificate's passphrase";

		etiquetas[CSP_IDIOMA_NUEVA_PASSWORD_DESCRIPCION]  = "Type in the key's passphrase";
		etiquetas[CSP_IDIOMA_NUEVA_PASSWORD_CONTRASENYA]  = "Password";
		etiquetas[CSP_IDIOMA_NUEVA_PASSWORD_CONFIRMACION] = "Confirmation";

		etiquetas[CSP_IDIOMA_NUEVA_PASSWORD_CONCORDAR] = "Password and confirmation don't match. Try it again";
		etiquetas[CSP_IDIOMA_NUEVA_PASSWORD_CAPTION] = "Key protection passphrase";


	} else {
		/* Ingl�s = 1033
		 */

		if ( strcmp(iden, "catcert") == 0 )
			etiquetas[CSP_IDIOMA_DLGPASSWORD_DESCRIPCION]  = "Type in the Clauer idCAT's passphrase";
		else
			etiquetas[CSP_IDIOMA_DLGPASSWORD_DESCRIPCION]  = "Type in the Clauer's passphrase";	

		if ( strcmp(iden, "catcert") == 0 )
			etiquetas[CSP_IDIOMA_DLGPASSWORD_CAPTION]      = "Clauer idCAT's passphrase";
		else
			etiquetas[CSP_IDIOMA_DLGPASSWORD_CAPTION]      = "Clauer's passphrase";

		etiquetas[CSP_IDIOMA_DLGPASSWORD_KEY_DESCRIPCION] = "Type in the certificate's passphrase";
		etiquetas[CSP_IDIOMA_DLGPASSWORD_KEY_CAPTION]     = "Certificate's Password";
		etiquetas[CSP_IDIOMA_DLGPASSWORD_KEY]             = "Password:";

		etiquetas[CSP_IDIOMA_ACEPTAR]  = "Ok";
		etiquetas[CSP_IDIOMA_CANCELAR] = "Cancel";
		etiquetas[CSP_IDIOMA_DLGPASSWORD_PIN]          = "Passphrase:";

		if ( strcmp(iden, "catcert") == 0 )
			etiquetas[CSP_IDIOMA_NOCLAUER] = "There's no Clauer idCAT. Insert the Clauer idCAT to use";
		else
			etiquetas[CSP_IDIOMA_NOCLAUER] = "There's no Clauer. Insert the Clauer to use";

		etiquetas[CSP_IDIOMA_PROJECTE_CLAUER] = "UJI :: Clauer Project";

		if ( strcmp(iden, "catcert") == 0 )
			etiquetas[CSP_IDIOMA_MASDEUNCLAUER] = "There is more than one Clauer idCAT device plugged. Please, keep the one you want to use";
		else
			etiquetas[CSP_IDIOMA_MASDEUNCLAUER] = "There is more than one Clauer device plugged. Please, keep the one you want to use";

		etiquetas[CSP_IDIOMA_CONTINUA_GVA]             = "It seems that you typed a weak passphrase.\r\nWe recommend you to change it as soon as possible.";

		if ( strcmp(iden, "catcert") == 0 ) 
			etiquetas[CSP_IDIOMA_DLGPASSWORD_DESC_INCORRECTA] = "Invalid passphrase. You have three attempts. Type in the Clauer idCAT's passphrase";
		else
			etiquetas[CSP_IDIOMA_DLGPASSWORD_DESC_INCORRECTA] = "Invalid passphrase. You have three attempts. Type in the Clauer's passphrase";

		etiquetas[CSP_IDIOMA_DLGPASSWORD_KEY_DESC_INCORRECTA] = "Invalid passphrase. You have three attempts. Type in the certificate's passphrase";

		etiquetas[CSP_IDIOMA_NUEVA_PASSWORD_DESCRIPCION]  = "Type in the key's passphrase";
		etiquetas[CSP_IDIOMA_NUEVA_PASSWORD_CONTRASENYA]  = "Password";
		etiquetas[CSP_IDIOMA_NUEVA_PASSWORD_CONFIRMACION] = "Confirmation";
		etiquetas[CSP_IDIOMA_NUEVA_PASSWORD_CONCORDAR] = "Password and confirmation don't match. Try it again";
		etiquetas[CSP_IDIOMA_NUEVA_PASSWORD_CAPTION] = "Key protection passphrase";
	}



	return TRUE;

}


BOOL CSP_IDIOMA_Descargar (void)
{
	if ( idioma ) {
		delete [] idioma;
		idioma = NULL;
	}

	return TRUE;
}




LPSTR CSP_IDIOMA_Get (DWORD etiqueta)
{
	if ( !idioma )
		return NULL;

	if ( etiqueta > (NUM_ETIQUETAS - 1) )
		return NULL;

	return etiquetas[etiqueta];
}



BOOL CSP_IDIOMA_Cargado (void)
{

	return idioma != NULL;


}

