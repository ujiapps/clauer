#include <windows.h>

#include <string>
#include <map>
#include <log/log.h>
#include <time.h>


using namespace std;

#include "CSPUtils.h"
#include "ThrCacheCleaner.h"

extern HANDLE g_hMutexPassCache;    // M�tex para el acceso a las cach� de passwords
extern map<string, PASS_CACHE_INFO *> g_mapPassCache;   // cach� de passwords

extern HANDLE g_hThrCacheEvent;	    // Para esperas
extern HANDLE g_hThrCacheEndEvent;  // Indica que el thread ha terminado su ejecuci�n

BOOL g_bThrErr;

#define SLEEP_INTERVAL_MS	300000    /* 5 minutos */

extern "C" {
extern int g_bClauerLOG;
}




DWORD WINAPI ThrCacheCleaner( LPVOID lpParam )
{
	map<string, PASS_CACHE_INFO *>::iterator i;
	time_t now;
	PASS_CACHE_INFO *info;
	DWORD ret;

	g_bThrErr = FALSE;

	if ( ! g_hThrCacheEvent ) {
		g_bThrErr = TRUE;
		if ( g_hThrCacheEndEvent )
			SetEvent(g_hThrCacheEndEvent);
		return 1;
	}
	if ( ! g_hMutexPassCache ) {
		g_bThrErr = TRUE;
		if ( g_hThrCacheEndEvent )
			SetEvent(g_hThrCacheEndEvent);
		return 1;
	}
	if ( ! g_hThrCacheEndEvent ) {
		g_bThrErr = TRUE;
		if ( g_hThrCacheEndEvent )
			SetEvent(g_hThrCacheEndEvent);
		return 1;
	}

	do {

		ret = WaitForSingleObject(g_hThrCacheEvent, SLEEP_INTERVAL_MS);

		if ( ret == WAIT_FAILED ) {
			g_bThrErr = TRUE;
			break;
		} else if ( ret == WAIT_OBJECT_0 ) {
			break;
		}
		
		/* Recorro la cach� para detectar qu� passwords fueron utilizadas en el intervalo y
		 * cuales no
		 */

		WaitForSingleObject(g_hMutexPassCache, 10000);

		now = time(NULL);

		for ( i = g_mapPassCache.begin() ; i != g_mapPassCache.end() ; i++ ) {
			
			info = i->second;

			if ( ((now - info->ts)*1000) >= SLEEP_INTERVAL_MS ) {

				/* Marco la entrada de la cach� para ser borrada.
				 * Esto es lo mismo que poner info->pass a NULL
				 */

				if ( info->pass ) {
					CSPUTILS_PASSPOOL_Free(info->pass);
					info->pass = NULL;
				}
			}
		}

		ReleaseMutex(g_hMutexPassCache);

	} while (1);

	/*CloseHandle(g_hThrCacheEvent);
	g_hThrCacheEvent = NULL;*/
	SetEvent(g_hThrCacheEndEvent);

	return 0;
}



