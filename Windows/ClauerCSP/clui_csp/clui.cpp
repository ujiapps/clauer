#include "clui.h"

#include <windows.h>
#include <Dbt.h>
#include <commctrl.h>
#include "resource_clui.h"

#include <LIBRT/libRT.h>
#include <b64/b64.h>

#include "CluiIDIOMA.h"

#include <atlbase.h>
#include <atlstr.h>
#include <atlrx.h>

extern HINSTANCE g_hModule;

/* Variables globales para IDD_DIALOG_GLOBAL_PASS
 */

static char g_szPwd[CLUI_CSP_MAX_PASS_LEN];
static char g_szDevice[CLUI_CSP_MAX_DEVICE_LEN];
static char g_szDescription[CLUI_CSP_MAX_DESC];
static BOOL g_bAskDevice;
static BOOL g_bDeviceWithOwner;
static BOOL g_bVerifyPassword;

/* Variables globales para IDD_DIALOG_ASK_PASSWORD
 */

static char g_szNewDescription[CLUI_CSP_MAX_DESC];
static char g_szNewPwd[CLUI_CSP_MAX_PASS_LEN];

/* Variables globales para IDD_DIALOG_DOUBLE_PASS
 */

static char g_szDoubleDescription[CLUI_CSP_MAX_DESC];
static char g_szDoublePwd[CLUI_CSP_MAX_PASS_LEN];

/* Prototipos de funciones 
 */

BOOL GetHeaderBitmapFromFile ( HBITMAP *hBmp );

#define MAX_IDEN   10
BOOL CLUI_CSP_REG_Get_Iden ( TCHAR iden[MAX_IDEN] );
BOOL CLUI_CSP_REG_Get_headerBitmap ( TCHAR headerBitamp[MAX_PATH] );


/* Convierte un certificado de PEM a DER
 */

BOOL CLUI_CSP_CERT_PEM2DER (BYTE *pem, unsigned long tamPEM, BYTE *der, unsigned long *tamDER)
{
  BYTE *cuerpo_i, *cuerpo_f,*aux;
  BOOL encontrado = FALSE, ret = TRUE;
  unsigned long pos;
  
  /* Buscamos la cabecera
   */

  pos = 0;
  aux = pem;
  while (!encontrado && (pos<tamPEM)) {
    while ( ((*aux) != '-') && (pos< tamPEM) ) {
      ++aux;
      ++pos;
    }
    if ( strncmp((char *)aux,"-----BEGIN CERTIFICATE-----", 27) == 0 )
      encontrado = TRUE;
    else {
      ++aux;
      ++pos;
    }
  }
  
  if ( !encontrado ) {
    ret = FALSE;
    goto finPEM2DER;
  }
  
  
  cuerpo_i = aux + 28; /* del begin certificate y el cambio de l�nea */
  
  /*
   * Ahora eliminamos el pie
   */
  
  aux = cuerpo_i;
  encontrado = FALSE;
  while ( !encontrado && (pos < tamPEM)) {
    while ( (*aux != '-') && (pos < tamPEM)) {
      ++aux;
      ++pos;
    }
    if ( strncmp((char *)aux,"-----END CERTIFICATE-----", 25) == 0 ) {
      encontrado = TRUE;
    } else {
      ++pos;
      ++aux;
    }
  }
  
  
  if (!encontrado) {
    ret = FALSE;
    goto finPEM2DER;
  }
  
  cuerpo_f = aux-1;
  
  B64_Decodificar(cuerpo_i, cuerpo_f-cuerpo_i+1,NULL,tamDER);
  
  if ( !der ) {
    ret = TRUE;
    goto finPEM2DER;
  }
  
  B64_Decodificar(cuerpo_i, cuerpo_f-cuerpo_i+1,der,tamDER);
  
 finPEM2DER:
  
  return ret;
  
}




/* Obtiene el propietario del clauer
 */



BOOL CLUI_CSP_GetClauerOwner ( char *device, char owner[MAX_CLAUER_OWNER] )
{
	USBCERTS_HANDLE hClauer;
	unsigned char block[TAM_BLOQUE];
	long nBlock;

	unsigned long tamDer;
	BYTE *certDer;

	PCCERT_CONTEXT certCtx;

	*owner = 0;

	if ( LIBRT_IniciarDispositivo((unsigned char *) device,NULL, &hClauer) != 0 ) 
		return FALSE;
	
	if ( LIBRT_LeerTipoBloqueCrypto(&hClauer, BLOQUE_CERT_PROPIO, 1, block, &nBlock) != 0 )
		return FALSE;

	if ( nBlock == -1 )
		return FALSE;
	
	/* Intento evitar en la medida de lo posible utilizar OpenSSL en Windows...
     * menos DLLs a instalar
     */

    if ( ! CLUI_CSP_CERT_PEM2DER ( BLOQUE_CERTPROPIO_Get_Objeto(block),
				  BLOQUE_CERTPROPIO_Get_Tam(block),
				  NULL,
				  &tamDer ) ) 
      {
	LIBRT_FinalizarDispositivo(&hClauer);
	return 1;
      }


    certDer = ( BYTE * ) malloc ( tamDer );
    if ( ! certDer ) {
      LIBRT_FinalizarDispositivo(&hClauer);
      return 1;
    }

    if ( ! CLUI_CSP_CERT_PEM2DER ( BLOQUE_CERTPROPIO_Get_Objeto(block),
				  BLOQUE_CERTPROPIO_Get_Tam(block),
				  certDer,
				  &tamDer ) ) 
      {
	LIBRT_FinalizarDispositivo(&hClauer);
	free(certDer);
	return 1;
      }
        
    certCtx = CertCreateCertificateContext(X509_ASN_ENCODING, certDer, tamDer);
    if ( ! certCtx ) {
      LIBRT_FinalizarDispositivo(&hClauer);
      free(certDer);
      return 1;
    }


    CertGetNameString(certCtx, CERT_NAME_SIMPLE_DISPLAY_TYPE, 0, NULL, owner, MAX_CLAUER_OWNER+1);

    CertFreeCertificateContext(certCtx);
    free(certDer);
	return TRUE;
}



BOOL VerifyPassphraseStrength ( char *szPass )
{

	CAtlRegExp<> re;
	CAtlREMatchContext<> mcUrl;
	HKEY hKey;

	DWORD dwType, dwSize;
	TCHAR szData[200];

	if ( RegOpenKeyEx(HKEY_LOCAL_MACHINE, _T("Software\\Universitat Jaume I\\Projecte Clauer"), 0, KEY_READ, &hKey) != ERROR_SUCCESS )
		return TRUE;
	if ( RegQueryValueEx(hKey, _T("WEAKPASS"), NULL, &dwType, NULL, &dwSize) != ERROR_SUCCESS ) {
		RegCloseKey(hKey);
		return TRUE;
	}
	if ( dwType != REG_SZ ) {
		RegCloseKey(hKey);
		return TRUE;
	}
	if ( dwSize > 200 ) {
		RegCloseKey(hKey);
		return TRUE;
	}
	if ( RegQueryValueEx(hKey, _T("WEAKPASS"), NULL, &dwType, (LPBYTE) szData, &dwSize) != ERROR_SUCCESS ) {
		RegCloseKey(hKey);
		return TRUE;
	}
	RegCloseKey(hKey);

	if ( re.Parse(szData) != REPARSE_ERROR_OK )
		return TRUE;
	if ( ! re.Match(szPass, &mcUrl) )
		return TRUE;
	else
		return FALSE;
}







// Dialog proc

INT_PTR CALLBACK CLUI_CSP_GlobalPassDlgProc ( HWND hwndDlg,
										  UINT uMsg,
										  WPARAM wParam,
										  LPARAM lParam )
{
	HWND hWndAux;
	INT_PTR ret = TRUE;
	HBITMAP hHeader;

	char *devs[MAX_DEVICES], deviceItem[MAX_PATH_LEN + MAX_CLAUER_OWNER+3];
	char owner[MAX_CLAUER_OWNER+3];
	char szFileName[MAX_PATH], szCaption[MAX_PATH+11], *p;
	int nDevs = 0, i;
	RECT dimDialog, dimDesktop;
	HWND hDesktop;
	DWORD tam;

	USBCERTS_HANDLE hClauer;

	switch ( uMsg ) {

	case WM_INITDIALOG:

		hDesktop = GetDesktopWindow();
		GetWindowRect(hDesktop,&dimDesktop);
		GetWindowRect(hwndDlg,&dimDialog);
		SetWindowPos(hwndDlg,
			HWND_TOP,
			((dimDesktop.right-dimDesktop.left)-(dimDialog.right-dimDialog.left))/2,
			((dimDesktop.bottom-dimDesktop.top)-(dimDialog.bottom-dimDialog.top))/2,
			0,
			0,
			SWP_NOSIZE);

		CLUI_CSP_IDIOMA_Cargar();

		/* Traducciones
		 */

		SetDlgItemText(hwndDlg, IDC_STATIC_CREDENCIALES, CLUI_CSP_IDIOMA_Get(CLUI_CSP_IDIOMA_CREDENTIALS));
		SetDlgItemText(hwndDlg, IDC_LBL_PASS, CLUI_CSP_IDIOMA_Get(CLUI_CSP_IDIOMA_PASSPHRASE));
		SetDlgItemText(hwndDlg, IDC_LBL_DEVICE, CLUI_CSP_IDIOMA_Get(CLUI_CSP_IDIOMA_DEVICE));
		SetDlgItemText(hwndDlg, IDOK, CLUI_CSP_IDIOMA_Get(CLUI_CSP_IDIOMA_OK));
		SetDlgItemText(hwndDlg, IDCANCEL, CLUI_CSP_IDIOMA_Get(CLUI_CSP_IDIOMA_CANCEL));

		/* Cambiamos el bitmap
		 */

		/*GetHeaderBitmap(&bitmapId);*/
		if ( ! GetHeaderBitmapFromFile(&hHeader) )
			hHeader = NULL;
	
		hWndAux = GetDlgItem(hwndDlg, IDC_HEADER);
		if ( hWndAux ) {
			if ( hHeader ) {
				SendMessage(hWndAux, STM_SETIMAGE, IMAGE_BITMAP, (LPARAM) hHeader);
			} 
		}

		/* Muestro el nombre de la aplicaci�n que solicita el acceso al clauer
		 */
		
		tam = GetModuleFileName(NULL, szFileName, MAX_PATH);

		if ( ! tam ) {
			*szFileName = 0;
			p = szFileName;
		} else {
			szFileName[MAX_PATH-1] = 0;
			i = tam-2;
			while ( i > 0 && szFileName[i] != '\\' )
				--i;
			if ( i > 0 )
				++i;
			p = szFileName+i;
		}

		strcpy(szCaption, p);
		
		SetWindowText(hwndDlg, szCaption);
 
		/* Si g_bAskDevice es TRUE enumeramos dispositivos y rellenamos la combo
		 * de dispositivos. Si no es as� lo que hacemos es utilizar el dispositivo
		 * que se pas� por argumentos
		 */

		LIBRT_Ini();

		if ( g_bAskDevice ) {

			if ( LIBRT_ListarDispositivos(&nDevs, (unsigned char **) devs) != 0 ) {
				EndDialog(hwndDlg, CLUI_CSP_ERR);
				return FALSE;
			}
			
		} else {
			nDevs = 1;
			devs[0] = ( char * ) malloc (strlen(g_szDevice)+1);
			if ( ! devs[0] ) {
				EndDialog(hwndDlg, CLUI_CSP_ERR);
				return FALSE;
			}
			strcpy(devs[0], g_szDevice);
		}

		/* Inicializamos el texto descriptivo */

		if ( *g_szDescription ) {

			hWndAux = GetDlgItem(hwndDlg, IDC_STATIC_DESCRIPTION);
			if ( ! hWndAux ) {
				for ( i = 0 ; i < nDevs ; i++ )
					free(devs[i]);
				EndDialog(hwndDlg, CLUI_CSP_ERR);
				return FALSE;
			}
			SetWindowText(hWndAux, g_szDescription);

		} else {
			SetDlgItemText(hwndDlg, IDC_STATIC_DESCRIPTION, CLUI_CSP_IDIOMA_Get(CLUI_CSP_IDIOMA_DEFAULT_DESC_GLOBAL));
		}

		/* Rellenamos la combo de los dispositivos
		 */

		hWndAux = GetDlgItem(hwndDlg, IDC_COMBO_DEVICE);
		if ( ! hWndAux ) {
			for ( i = 0 ; i < nDevs ; i++ )
				free(devs[i]);
			EndDialog(hwndDlg, CLUI_CSP_ERR);
			return FALSE;
		}

		for ( i = 0 ; i < nDevs ; i++ ) {

			if ( g_bDeviceWithOwner ) {
				*owner = 0;
				CLUI_CSP_GetClauerOwner ( devs[i], owner );
				_snprintf(deviceItem, MAX_CLAUER_OWNER + MAX_PATH_LEN + 3, "%s (%s)", devs[i], owner);
			} else
				strcpy(deviceItem, devs[i]);
		
			SendMessage(hWndAux, 
						CB_ADDSTRING,
						0,
						(LPARAM) deviceItem);
		}

		SendMessage(hWndAux,
				CB_SETCURSEL,
				0,
				0);

		if ( !g_bAskDevice || nDevs == 1 || nDevs == 0 )
			EnableWindow(hWndAux, FALSE);
		
		return TRUE;

	case WM_CLOSE:
		EndDialog(hwndDlg, CLUI_CSP_CANCEL);
		return TRUE;
		break;

	case WM_COMMAND:

		switch (LOWORD(wParam)) {
		case IDOK:

			/* Obtengo la password
			 */

			if ( ! GetDlgItemText(hwndDlg, IDC_EDIT_PASS, g_szPwd, CLUI_CSP_MAX_PASS_LEN) ) {
				if ( GetLastError() != 0 ) {
					*g_szPwd = 0;
					EndDialog(hwndDlg, CLUI_CSP_ERR);
					return TRUE;
				}
				*g_szPwd = 0;
			}
			if ( ! GetDlgItemText(hwndDlg, IDC_COMBO_DEVICE, g_szDevice, CLUI_CSP_MAX_DEVICE_LEN) ) {
				if ( GetLastError() != 0 ) {
					*g_szDevice = 0;
					EndDialog(hwndDlg, CLUI_CSP_ERR);
					return TRUE;
				}
				*g_szDevice = 0;
			}
			if ( g_bDeviceWithOwner ) {
				for (i = 0 ; *g_szDevice && g_szDevice[i] != '(' ; i++ );
				if ( *g_szDevice )
					g_szDevice[i-1] = 0;
			}

			if ( ! VerifyPassphraseStrength(g_szPwd) ) {
				MessageBox(hwndDlg, CLUI_CSP_IDIOMA_Get(CLUI_CSP_IDIOMA_CHANGE_PASSPHRASE), CLUI_CSP_IDIOMA_Get(CLUI_CSP_IDIOMA_WARNING), MB_OK|MB_ICONWARNING);
				EndDialog(hwndDlg, CLUI_CSP_CANCEL);
				return TRUE;
			}

			if ( g_bVerifyPassword ) {

				/* Inicializamos el dispostivo para comprobar la password
				 */

				if ( !*g_szPwd || LIBRT_IniciarDispositivo((unsigned char *) g_szDevice, g_szPwd, &hClauer) != 0 ) {
					MessageBox(hwndDlg, CLUI_CSP_IDIOMA_Get(CLUI_CSP_IDIOMA_WRONG_PASS), CLUI_CSP_IDIOMA_Get(CLUI_CSP_IDIOMA_ERROR), MB_ICONERROR);

					SetDlgItemText(hwndDlg, IDC_EDIT_PASS, "");
					SetFocus(GetDlgItem(hwndDlg, IDC_EDIT_PASS));

				}else {
					EndDialog(hwndDlg, CLUI_CSP_OK);
				}

			} else {
				EndDialog(hwndDlg, CLUI_CSP_OK);
			}

            return TRUE;

		case IDCANCEL:
			EndDialog(hwndDlg,CLUI_CSP_CANCEL);
			return TRUE;
		}

		break;

	case WM_DEVICECHANGE:

		/* Si se inserta un dispositivo o se extrae, recargo la lista de
		 * dispositivos
		 */

		if ( g_bAskDevice ) {
			if ( wParam == DBT_DEVICEARRIVAL || wParam == DBT_DEVICEREMOVECOMPLETE ) {
				
				hWndAux = GetDlgItem(hwndDlg, IDC_COMBO_DEVICE);
				if ( ! hWndAux ) {
					EndDialog(hwndDlg, CLUI_CSP_ERR);
					return FALSE;
				}
				SendMessage(hWndAux,
							CB_RESETCONTENT,
							0,
							0);

				if ( LIBRT_ListarDispositivos(&nDevs, (unsigned char **) devs) != 0 ) {
					EndDialog(hwndDlg, CLUI_CSP_ERR);
					return FALSE;
				}
				
				hWndAux = GetDlgItem(hwndDlg, IDC_COMBO_DEVICE);
				if ( ! hWndAux ) {
					for ( i = 0 ; i < nDevs ; i++ )
						free(devs[i]);
					EndDialog(hwndDlg, CLUI_CSP_ERR);
					return FALSE;
				}

				for ( i = 0 ; i < nDevs ; i++ ) {

					if ( g_bDeviceWithOwner ) {
						*owner = 0;
						CLUI_CSP_GetClauerOwner ( devs[i], owner );
						_snprintf(deviceItem, MAX_CLAUER_OWNER + MAX_PATH_LEN + 3, "%s (%s)", devs[i], owner);
					}
				
					SendMessage(hWndAux, 
								CB_ADDSTRING,
								0,
								(LPARAM) deviceItem);
				}

				SendMessage(hWndAux,
					CB_SETCURSEL,
					0,
					0);

				if ( nDevs == 1 || nDevs == 0 ) 
					EnableWindow(hWndAux, FALSE);
				else
					EnableWindow(hWndAux, TRUE);

			}
		}
		
		return TRUE;
	}


	return FALSE;
}

// Pide la contrase�a global del clauer

int CLUI_CSP_AskGlobalPassphrase ( HWND hWndParent,
							   BOOL bAskDevice,
							   BOOL bDeviceWithOwner,
							   BOOL bVerifyPassword,
							   char szDescription[CLUI_CSP_MAX_DESC],
							   char szDevice[CLUI_CSP_MAX_DEVICE_LEN],
							   char szPass[CLUI_CSP_MAX_PASS_LEN] )
{
	HWND hWnd = 0;
	INT_PTR ret;

	g_bDeviceWithOwner = bDeviceWithOwner;
	g_bAskDevice       = bAskDevice;
	g_bVerifyPassword  = bVerifyPassword;

	if ( szDescription ) {
		strncpy(g_szDescription, szDescription, CLUI_CSP_MAX_DESC-1);
		g_szDescription[CLUI_CSP_MAX_DESC-1] = 0;
	} else {
		g_szDescription[0] = 0;
	}

	strncpy(g_szDevice, szDevice, CLUI_CSP_MAX_DEVICE_LEN-1);
	g_szDevice[CLUI_CSP_MAX_DEVICE_LEN-1] = 0;
	
	ret = DialogBox(g_hModule, MAKEINTRESOURCE(IDD_DIALOG_GLOBAL_PASS), hWndParent, CLUI_CSP_GlobalPassDlgProc);	

	if ( ret == CLUI_CSP_OK ) {
		if ( bAskDevice ) {
			strcpy(szDevice, g_szDevice);
			SecureZeroMemory(g_szDevice, sizeof g_szDevice);
		}
		strcpy(szPass, g_szPwd);
		SecureZeroMemory(g_szPwd, sizeof g_szPwd);
	}
	
	return (int) ret;
}








INT_PTR CALLBACK CLUI_CSP_NewPassDlgProc ( HWND hwndDlg,
									   UINT uMsg,
									   WPARAM wParam,
									   LPARAM lParam )
{
	HWND hWndAux;
	INT_PTR ret = TRUE;

	char szFileName[MAX_PATH], *p;
	char szConfirmation[CLUI_CSP_MAX_PASS_LEN];
	int nDevs = 0, i;
	RECT dimDialog, dimDesktop;
	HWND hDesktop;
	DWORD tam;

	HBITMAP hHeader;

	switch ( uMsg ) {

	case WM_INITDIALOG:

		CLUI_CSP_IDIOMA_Cargar();

		hDesktop = GetDesktopWindow();
		GetWindowRect(hDesktop,&dimDesktop);
		GetWindowRect(hwndDlg,&dimDialog);
		SetWindowPos(hwndDlg,
			HWND_TOP,
			((dimDesktop.right-dimDesktop.left)-(dimDialog.right-dimDialog.left))/2,
			((dimDesktop.bottom-dimDesktop.top)-(dimDialog.bottom-dimDialog.top))/2,
			0,
			0,
			SWP_NOSIZE);

		SetDlgItemText(hwndDlg, IDC_STATIC_NEW_DESCRIPTION, CLUI_CSP_IDIOMA_Get(CLUI_CSP_IDIOMA_DEFAULT_DESC_NEW));
		SetDlgItemText(hwndDlg, IDC_PASSWORDS, CLUI_CSP_IDIOMA_Get(CLUI_CSP_IDIOMA_PASSWORDS));
		SetDlgItemText(hwndDlg, IDC_LBL_NEW_PASS, CLUI_CSP_IDIOMA_Get(CLUI_CSP_IDIOMA_PASSPHRASE));
		SetDlgItemText(hwndDlg, IDC_LBL_NEW_CONFIRMATION, CLUI_CSP_IDIOMA_Get(CLUI_CSP_IDIOMA_NEW_CONFIRMATION));
		SetDlgItemText(hwndDlg, IDOK, CLUI_CSP_IDIOMA_Get(CLUI_CSP_IDIOMA_OK));
		SetDlgItemText(hwndDlg, IDCANCEL, CLUI_CSP_IDIOMA_Get(CLUI_CSP_IDIOMA_CANCEL));

		/*GetHeaderBitmap(&bitmapId);*/
		if ( ! GetHeaderBitmapFromFile(&hHeader) )
			hHeader = NULL;
	
		hWndAux = GetDlgItem(hwndDlg, IDC_HEADER);
		if ( hWndAux ) {
			if ( hHeader ) {
				SendMessage(hWndAux, STM_SETIMAGE, IMAGE_BITMAP, (LPARAM) hHeader);
			}
		}

		/* Muestro el nombre de la aplicaci�n que solicita el acceso al clauer
		 */
		
		tam = GetModuleFileName(NULL, szFileName, MAX_PATH);
		
		if ( ! tam ) {
			*szFileName = 0;
			p = szFileName;
		} else {
			szFileName[MAX_PATH-1] = 0;
			i = tam-2;
			while ( i > 0 && szFileName[i] != '\\' )
				--i;
			if ( i > 0 )
				++i;
			p = szFileName+i;
		}

		SetWindowText(hwndDlg, p);
 
		/* Inicializamos el texto descriptivo 
		 */

		if ( *g_szNewDescription ) {

			hWndAux = GetDlgItem(hwndDlg, IDC_STATIC_DESCRIPTION);
			if ( ! hWndAux ) {
				EndDialog(hwndDlg, CLUI_CSP_ERR);
				return FALSE;
			}
			SetWindowText(hWndAux, g_szNewDescription);

		} else 
			SetDlgItemText(hwndDlg, IDC_STATIC_DESCRIPTION, CLUI_CSP_IDIOMA_Get(CLUI_CSP_IDIOMA_DEFAULT_DESC_NEW));

		return TRUE;

	case WM_CLOSE:

		EndDialog(hwndDlg, CLUI_CSP_CANCEL);
		return TRUE;
		break;

	case WM_COMMAND:

		switch (LOWORD(wParam)) {
		case IDOK:

			/* Obtengo la password
			 */

			if ( ! GetDlgItemText(hwndDlg, IDC_EDIT_NEW_PASS, g_szNewPwd, CLUI_CSP_MAX_PASS_LEN) ) {
				if ( GetLastError() != 0 ) {
					*g_szNewPwd = 0;
					EndDialog(hwndDlg, CLUI_CSP_ERR);
					return TRUE;
				}
				*g_szNewPwd = 0;
			}
			if ( ! GetDlgItemText(hwndDlg, IDC_EDIT_NEW_CONFIRMATION, szConfirmation, CLUI_CSP_MAX_PASS_LEN) ) {
				if ( GetLastError() != 0 ) {
					SecureZeroMemory(g_szNewPwd, sizeof g_szNewPwd);
					*g_szNewPwd = 0;
					EndDialog(hwndDlg, CLUI_CSP_ERR);
					return TRUE;
				}
				*szConfirmation = 0;
			}

			if ( strcmp(g_szNewPwd, szConfirmation) != 0 ){
				SecureZeroMemory(g_szNewPwd, sizeof g_szNewPwd);
				SecureZeroMemory(szConfirmation, sizeof szConfirmation);
				MessageBox(hwndDlg, CLUI_CSP_IDIOMA_Get(CLUI_CSP_IDIOMA_DONT_MATCH), CLUI_CSP_IDIOMA_Get(CLUI_CSP_IDIOMA_WARNING), MB_ICONWARNING);
			} else {
				SecureZeroMemory(szConfirmation, sizeof szConfirmation);
				EndDialog(hwndDlg, CLUI_CSP_OK);
			}

            return TRUE;

		case IDCANCEL:
			EndDialog(hwndDlg,CLUI_CSP_CANCEL);
			return TRUE;
		}

		break;
	}



	return FALSE;
}





int CLUI_CSP_AskNewPassphrase ( HWND hWndParent,
						    char szDescription[CLUI_CSP_MAX_DESC],
							char szPass[CLUI_CSP_MAX_PASS_LEN])
{
	INT_PTR ret;

	ret = DialogBox(g_hModule, MAKEINTRESOURCE(IDD_DIALOG_ASK_PASSWORD), hWndParent, CLUI_CSP_NewPassDlgProc);	

	if ( ret == CLUI_CSP_OK ) {
		strcpy(szPass, g_szNewPwd);
		SecureZeroMemory(g_szNewPwd, sizeof g_szNewPwd);		
	}

	return (int) ret;
}









INT_PTR CALLBACK CLUI_CSP_DoublePassDlgProc ( HWND hwndDlg,
									   UINT uMsg,
									   WPARAM wParam,
									   LPARAM lParam )
{
	HWND hWndAux;
	INT_PTR ret = TRUE;

	char szFileName[MAX_PATH], *p;
	int nDevs = 0, i;
	RECT dimDialog, dimDesktop;
	HWND hDesktop;
	DWORD tam;

	HBITMAP hHeader;

	switch ( uMsg ) {

	case WM_INITDIALOG:

		CLUI_CSP_IDIOMA_Cargar();

		hDesktop = GetDesktopWindow();
		GetWindowRect(hDesktop,&dimDesktop);
		GetWindowRect(hwndDlg,&dimDialog);
		SetWindowPos(hwndDlg,
			HWND_TOP,
			((dimDesktop.right-dimDesktop.left)-(dimDialog.right-dimDialog.left))/2,
			((dimDesktop.bottom-dimDesktop.top)-(dimDialog.bottom-dimDialog.top))/2,
			0,
			0,
			SWP_NOSIZE);

		SetDlgItemText(hwndDlg, IDC_STATIC_DESCRIPTION, CLUI_CSP_IDIOMA_Get(CLUI_CSP_IDIOMA_CERT_PASS));
		SetDlgItemText(hwndDlg, IDC_STATIC_CREDENCIALES, CLUI_CSP_IDIOMA_Get(CLUI_CSP_IDIOMA_CREDENTIALS));
		SetDlgItemText(hwndDlg, IDC_LBL_PASS, CLUI_CSP_IDIOMA_Get(CLUI_CSP_IDIOMA_PASSPHRASE));
		SetDlgItemText(hwndDlg, IDOK, CLUI_CSP_IDIOMA_Get(CLUI_CSP_IDIOMA_OK));
		SetDlgItemText(hwndDlg, IDCANCEL, CLUI_CSP_IDIOMA_Get(CLUI_CSP_IDIOMA_CANCEL));

		if ( ! GetHeaderBitmapFromFile(&hHeader) )
			hHeader = NULL;
	
		hWndAux = GetDlgItem(hwndDlg, IDC_HEADER);
		if ( hWndAux ) {
			if ( hHeader ) {
				SendMessage(hWndAux, STM_SETIMAGE, IMAGE_BITMAP, (LPARAM) hHeader);
			}
		}

		/* Muestro el nombre de la aplicaci�n que solicita el acceso al clauer
		 */
		
		tam = GetModuleFileName(NULL, szFileName, MAX_PATH);
		
		if ( ! tam ) {
			*szFileName = 0;
			p = szFileName;
		} else {
			szFileName[MAX_PATH-1] = 0;
			i = tam-2;
			while ( i > 0 && szFileName[i] != '\\' )
				--i;
			if ( i > 0 )
				++i;
			p = szFileName+i;
		}

		SetWindowText(hwndDlg, p);
 
		/* Inicializamos el texto descriptivo 
		 */

		if ( *g_szDoubleDescription ) {

			hWndAux = GetDlgItem(hwndDlg, IDC_STATIC_DESCRIPTION);
			if ( ! hWndAux ) {
				EndDialog(hwndDlg, CLUI_CSP_ERR);
				return FALSE;
			}
			SetWindowText(hWndAux, g_szDoubleDescription);

		} 

		return TRUE;

	case WM_CLOSE:
		EndDialog(hwndDlg, CLUI_CSP_CANCEL);
		return TRUE;
		break;

	case WM_COMMAND:

		switch (LOWORD(wParam)) {
		case IDOK:

			/* Obtengo la password
			 */

			if ( ! GetDlgItemText(hwndDlg, IDC_EDIT_PASS, g_szDoublePwd, CLUI_CSP_MAX_PASS_LEN) ) {
				if ( GetLastError() != 0 ) {
					*g_szDoublePwd = 0;
					EndDialog(hwndDlg, CLUI_CSP_ERR);
					return TRUE;
				}
				*g_szDoublePwd = 0;
			}

			EndDialog(hwndDlg, CLUI_CSP_OK);

            return TRUE;

		case IDCANCEL:

			EndDialog(hwndDlg,CLUI_CSP_CANCEL);
			return TRUE;
		}

		break;
	}

	return FALSE;
}



int CLUI_CSP_AskDoublePassphrase ( HWND hWndParent,
							   char szDescription[CLUI_CSP_MAX_DESC],
							   char szPass[CLUI_CSP_MAX_PASS_LEN] )
{
	INT_PTR ret;

	if ( szDescription ) {
		strncpy(g_szDoubleDescription, szDescription, CLUI_CSP_MAX_DESC-1);
		g_szDoubleDescription[CLUI_CSP_MAX_DESC-1] = 0;
	} else {
		*g_szDoubleDescription = 0;
	}

	ret = DialogBox(g_hModule, MAKEINTRESOURCE(IDD_DIALOG_DOUBLE_PASS), hWndParent, CLUI_CSP_DoublePassDlgProc);

	if ( ret == CLUI_CSP_OK ) {
		strcpy(szPass, g_szDoublePwd);
		SecureZeroMemory(g_szDoublePwd, sizeof g_szDoublePwd);		
	}

	return (int) ret;
}



/*! \brief Obtiene del registro el iden de la organizaci�n
 *
 * Obtiene del registro el iden de la organizaci�n
 *
 * \param iden
 *        SALIDA. El iden :-)
 *
 * \retval FALSE
 *         Error
 *
 * \retval TRUE
 *         Ok
 */

BOOL CLUI_CSP_REG_Get_Iden ( TCHAR iden[MAX_IDEN] )
{
	HKEY hKey;
	DWORD dwType, dwSize;

	if ( ! iden )
		return FALSE;

	if ( RegOpenKeyEx(HKEY_LOCAL_MACHINE, "Software\\Universitat Jaume I\\Projecte Clauer", 0, KEY_READ, &hKey) != ERROR_SUCCESS ) 
		return FALSE;
	if ( RegQueryValueEx(hKey, "IDEN", NULL, &dwType, NULL, &dwSize) != ERROR_SUCCESS ) {
		RegCloseKey(hKey);
		return FALSE;
	}
	if ( dwType != REG_SZ ) {
		RegCloseKey(hKey);
		return FALSE;
	}
	if ( dwSize > MAX_IDEN ) {
		RegCloseKey(hKey);
		return FALSE;
	}
	if ( RegQueryValueEx(hKey, "IDEN", NULL, &dwType, (LPBYTE) iden, &dwSize) != ERROR_SUCCESS ) {
		RegCloseKey(hKey);
		return FALSE;
	}
	RegCloseKey(hKey);

	return TRUE;
}

/*! \brief Obtiene el headerBitmap.
 *
 * Obtiene del registro headerBitmap, que es la ruta donde se almacenan las im�genes del CLUI
 *
 * \param iden
 *        SALIDA. El iden :-)
 *
 * \retval FALSE
 *         Error
 *
 * \retval TRUE
 *         Ok
 */

BOOL CLUI_CSP_REG_Get_headerBitmap ( TCHAR headerBitmap[MAX_PATH] )
{
	HKEY hKey;
	DWORD dwType, dwSize;

	if ( ! headerBitmap )
		return FALSE;

	if ( RegOpenKeyEx(HKEY_LOCAL_MACHINE, "Software\\Universitat Jaume I\\Projecte Clauer\\clui", 0, KEY_READ, &hKey) != ERROR_SUCCESS ) 
		return FALSE;
	if ( RegQueryValueEx(hKey, "headerBitmap", NULL, &dwType, NULL, &dwSize) != ERROR_SUCCESS ) {
		RegCloseKey(hKey);
		return FALSE;
	}
	if ( dwType != REG_SZ ) {
		RegCloseKey(hKey);
		return FALSE;
	}
	if ( dwSize > MAX_PATH-1 ) {
		RegCloseKey(hKey);
		return FALSE;
	}
	if ( RegQueryValueEx(hKey, "headerBitmap", NULL, &dwType, (LPBYTE) headerBitmap, &dwSize) != ERROR_SUCCESS ) {
		RegCloseKey(hKey);
		return FALSE;
	}
	RegCloseKey(hKey);

	return TRUE;
}


#define MAX_BITMAP_SIZE  60000   /* en bytes */

BOOL GetHeaderBitmapFromFile ( HBITMAP *hBmp )
{
	TCHAR szIden[MAX_IDEN], szHeaderBitmap[MAX_PATH];
	DWORD dwFileNameSize;

	if ( ! hBmp )
		return FALSE;

	if ( ! CLUI_CSP_REG_Get_Iden(szIden) )
		return FALSE;
	if ( ! CLUI_CSP_REG_Get_headerBitmap(szHeaderBitmap) )
		return FALSE;

	/* headerBitmap\\IDEN_header.bmp */
	dwFileNameSize = _tcsclen(szHeaderBitmap) + 1 + _tcsclen(szIden) + 11 + 1;
	if ( dwFileNameSize > MAX_PATH )
		return FALSE;

	_tcscat(szHeaderBitmap, "\\");
	_tcscat(szHeaderBitmap, szIden);
	_tcscat(szHeaderBitmap, "_header.bmp");

	*hBmp = (HBITMAP) LoadImage(NULL, szHeaderBitmap, IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
	if ( ! *hBmp )
		return FALSE;

	return TRUE;
}
