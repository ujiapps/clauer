﻿#include "CluiIDIOMA.h"

#include <stdio.h>

static char *idioma = NULL;
static char *etiquetas[NUM_ETIQUETAS];


/*! TRUE Ok
 *  FALSE error
 */

BOOL CLUI_CSP_IDIOMA_Cargar (void)
{

	HKEY hKey;
	DWORD tamValue;

	if ( idioma != NULL )
		return FALSE;

	if ( RegOpenKeyEx(HKEY_LOCAL_MACHINE, 
			     "SOFTWARE\\Universitat Jaume I\\Projecte Clauer", 
				 0,
				 KEY_QUERY_VALUE,
				 &hKey) != ERROR_SUCCESS ) 
	{
		return FALSE;
	}

	if ( RegQueryValueEx(hKey,"Idioma", NULL, NULL, NULL, &tamValue) != ERROR_SUCCESS ) {
		RegCloseKey(hKey);
		return FALSE;
	}

	if ( tamValue > 5 ) {
		RegCloseKey(hKey);
		return FALSE;
	}

	idioma = new char [tamValue];

	if ( !idioma ) {
		RegCloseKey(hKey);
		return FALSE;
	}

	if ( RegQueryValueEx(hKey,"Idioma", NULL, NULL, (BYTE *)idioma, &tamValue) != ERROR_SUCCESS ) {
		delete [] idioma;
		idioma = NULL;
		RegCloseKey(hKey);
		return FALSE;
	}

	RegCloseKey(hKey);

	/* Cargamos la tabla de etiquetas
	 */


	if ( strcmp(idioma, "1034") == 0 ) 
	{
		/* Castellano */

		etiquetas[CLUI_CSP_IDIOMA_OK]          = "Aceptar";
		etiquetas[CLUI_CSP_IDIOMA_CANCEL]      = "Cancelar";
		etiquetas[CLUI_CSP_IDIOMA_CREDENTIALS] = "Credenciales";
		etiquetas[CLUI_CSP_IDIOMA_PASSPHRASE]  = "Contraseña:";
		etiquetas[CLUI_CSP_IDIOMA_DEVICE]			   = "Dispositivo";
		etiquetas[CLUI_CSP_IDIOMA_DEFAULT_DESC_GLOBAL] = "Introduzca la contraseña que protege el dispositivo";
		etiquetas[CLUI_CSP_IDIOMA_DEFAULT_DESC_NEW]    = "Escriba la contraseña y su confirmación";
		etiquetas[CLUI_CSP_IDIOMA_NEW_PASSPHRASE] = "Contraseña:";
		etiquetas[CLUI_CSP_IDIOMA_NEW_CONFIRMATION] = "Confirmación:";
		etiquetas[CLUI_CSP_IDIOMA_PASSWORDS] = " Contraseñas ";
		etiquetas[CLUI_CSP_IDIOMA_CERT_PASS] = "Introduzca la contraseña";
		etiquetas[CLUI_CSP_IDIOMA_WRONG_PASS] = "Contraseña incorrecta";
		etiquetas[CLUI_CSP_IDIOMA_ERROR] = "Error";
		etiquetas[CLUI_CSP_IDIOMA_DONT_MATCH] = "La contraseña y su confirmación no coinciden";
		etiquetas[CLUI_CSP_IDIOMA_WARNING] = "Atención";
		etiquetas[CLUI_CSP_IDIOMA_CHANGE_PASSPHRASE] = "Ha introducido una contraseña débil para acceder a su dispositivo.\r\n"
			                                       "Debe cambiar la contraseña antes de poder utilizarlo.\r\n"
												   "Para ello puede utilizar el Gestor del Clauer";

	} else if ( strcmp(idioma, "1027") == 0 )
	{
		/* Catalán */

		etiquetas[CLUI_CSP_IDIOMA_OK]          = "Acceptar";
		etiquetas[CLUI_CSP_IDIOMA_CANCEL]      = "Cancel-lar";
		etiquetas[CLUI_CSP_IDIOMA_CREDENTIALS] = "Credencials";
		etiquetas[CLUI_CSP_IDIOMA_PASSPHRASE]  = "Paraula de pas:";
		etiquetas[CLUI_CSP_IDIOMA_DEVICE]      = "Dispositiu";
		etiquetas[CLUI_CSP_IDIOMA_DEFAULT_DESC_GLOBAL] = "Introdueix la paraula de pas que protegeix el dispositiu";
		etiquetas[CLUI_CSP_IDIOMA_DEFAULT_DESC_NEW]    = "Introdueix la paraula de pas i la seua confirmació";

		etiquetas[CLUI_CSP_IDIOMA_NEW_PASSPHRASE] = "Paraula de pas:";
		etiquetas[CLUI_CSP_IDIOMA_NEW_CONFIRMATION] = "Confirmació:";
		etiquetas[CLUI_CSP_IDIOMA_PASSWORDS] = " Paraules de pas ";
		etiquetas[CLUI_CSP_IDIOMA_CERT_PASS] = "Introdueix la paraula de pas";
		etiquetas[CLUI_CSP_IDIOMA_WRONG_PASS] = "Paraula de pas incorrecta";
		etiquetas[CLUI_CSP_IDIOMA_ERROR] = "Error";

		etiquetas[CLUI_CSP_IDIOMA_DONT_MATCH] = "La paraula de pas i la seua confirmació no coincideixen";
		etiquetas[CLUI_CSP_IDIOMA_WARNING] = "Atenció";
		etiquetas[CLUI_CSP_IDIOMA_CHANGE_PASSPHRASE] = "Has introduït una paraula de pas feble per a accedir al dispositiu.\r\n"
			                                       "Has de canviar-la abans de poder utilitzar-lo.\r\n"
												   "Per a això pot utilitzar el Gestor del Clauer";

	} else if ( strcmp(idioma, "1033") == 0 ){

		/* Inglés ( = 1033 ) */

		etiquetas[CLUI_CSP_IDIOMA_OK]          = "Ok";
		etiquetas[CLUI_CSP_IDIOMA_CANCEL]      = "Cancel";
		etiquetas[CLUI_CSP_IDIOMA_CREDENTIALS] = "Credentials";
		etiquetas[CLUI_CSP_IDIOMA_PASSPHRASE]  = "Passphrase:";
		etiquetas[CLUI_CSP_IDIOMA_DEVICE]      = "Device:";
		etiquetas[CLUI_CSP_IDIOMA_DEFAULT_DESC_GLOBAL] = "Type in the device's passphrase";
		etiquetas[CLUI_CSP_IDIOMA_DEFAULT_DESC_NEW]  = "Type in the passphrase and its confirmation";

		etiquetas[CLUI_CSP_IDIOMA_NEW_PASSPHRASE] = "Passphrase:";
		etiquetas[CLUI_CSP_IDIOMA_NEW_CONFIRMATION] = "Confirmation:";
		etiquetas[CLUI_CSP_IDIOMA_PASSWORDS] = " Passphrases ";
		etiquetas[CLUI_CSP_IDIOMA_CERT_PASS] = "Type in the passphrase";
		etiquetas[CLUI_CSP_IDIOMA_WRONG_PASS] = "Wrong passphrase";
		etiquetas[CLUI_CSP_IDIOMA_ERROR] = "Error";

		etiquetas[CLUI_CSP_IDIOMA_DONT_MATCH] = "Passphrase and confirmation doesn't match";
		etiquetas[CLUI_CSP_IDIOMA_WARNING] = "Warning";
		etiquetas[CLUI_CSP_IDIOMA_CHANGE_PASSPHRASE] = "You must change the device's passphrase before using it\r\n"
			                                       "You can use the Clauer Manager program";

	} else if ( strcmp(idioma, "1026") == 0 ){

		/* bulgaro *

		etiquetas[CLUI_CSP_IDIOMA_OK]          = "OK";
		etiquetas[CLUI_CSP_IDIOMA_CANCEL]      = "Отказ";
		etiquetas[CLUI_CSP_IDIOMA_CREDENTIALS] = "Удостоверяване на самоличността";
		etiquetas[CLUI_CSP_IDIOMA_PASSPHRASE]  = "Пин код:";
		etiquetas[CLUI_CSP_IDIOMA_DEVICE]      = "Device:";
		etiquetas[CLUI_CSP_IDIOMA_DEFAULT_DESC_GLOBAL] = "Въведете пин кода за Clauer устройството";
		etiquetas[CLUI_CSP_IDIOMA_DEFAULT_DESC_NEW]  = "Type in the passphrase and its confirmation";

		etiquetas[CLUI_CSP_IDIOMA_NEW_PASSPHRASE] = "Пин код:";
		etiquetas[CLUI_CSP_IDIOMA_NEW_CONFIRMATION] = "Confirmation:";
		etiquetas[CLUI_CSP_IDIOMA_PASSWORDS] = " Passphrases ";
		etiquetas[CLUI_CSP_IDIOMA_CERT_PASS] = "Type in the passphrase";
		etiquetas[CLUI_CSP_IDIOMA_WRONG_PASS] = "Wrong passphrase";
		etiquetas[CLUI_CSP_IDIOMA_ERROR] = "Error";

		etiquetas[CLUI_CSP_IDIOMA_DONT_MATCH] = "Passphrase and confirmation doesn't match";
		etiquetas[CLUI_CSP_IDIOMA_WARNING] = "Warning";
		etiquetas[CLUI_CSP_IDIOMA_CHANGE_PASSPHRASE] = "You must change the device's passphrase before using it\r\n"
			                                       "You can use the Clauer Manager program";*/
	}


	return TRUE;

}


BOOL CLUI_CSP_IDIOMA_Descargar (void)
{
	if ( idioma ) {
		delete [] idioma;
		idioma = NULL;
	}

	return TRUE;
}




LPSTR CLUI_CSP_IDIOMA_Get (DWORD etiqueta)
{
	if ( !idioma )
		return NULL;

	if ( etiqueta > (NUM_ETIQUETAS - 1) )
		return NULL;

	return etiquetas[etiqueta];
}



BOOL CLUI_CSP_CSP_IDIOMA_Cargado (void)
{

	return idioma != NULL;


}

