//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by csp.rc
//
#define IDP_SOCKETS_INIT_FAILED         101
#define IDC_LISTDISPOSITIVOS            1002
#define IDC_EDITPASSWORD                1003
#define IDC_EDITPIN                     1003
#define IDC_BUTTON1                     1004
#define IDACERCADE                      1004
#define IDR_DEFAULT1                    1005
#define IDD_DIALOG1                     1006
#define IDD_PASSWORD                    1006
#define IDC_DESCRIPCION                 1006
#define IDI_UJI                         1007
#define IDC_PIN                         1007
#define IDD_NUEVA_PASSWORD              1009
#define IDC_CONTRASENYA                 1010
#define IDC_CONFIRMACION                1011
#define IDC_NUEVA_CONTRASENYA           1012
#define IDC_NUEVA_CONFIRMACION          1013
#define IDC_NUEVA_DESCRIPCION           1014

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        1011
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1015
#define _APS_NEXT_SYMED_VALUE           1000
#endif
#endif
