/* Este programa se encarga de registrar el store f�sico Clauer
 * para cada usuario en el store del sistema MY
 *
 * Historial de modificaciones:
 *
 * 15/03/2006 :
 *               JUAN. Cambiada funci�n main() por WinMain() para evitar la
 *               aparici�n de la ventana de consola.
 */

#include <windows.h>
#include <wincrypt.h>

#define CLAUERSTORE_OID	"ClauerStoreProvider"



int WINAPI WinMain (HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int CmdShow)
{
	// Declare and initialize variables.

	LPCWSTR pvSystemName = L"ClauerStore"; 
	CERT_PHYSICAL_STORE_INFO PhysicalStoreInfo;

	// Initialize PhysicalStoreInfo.

	memset(&PhysicalStoreInfo, 0, sizeof(CERT_PHYSICAL_STORE_INFO));
	
	PhysicalStoreInfo.cbSize                = sizeof(CERT_PHYSICAL_STORE_INFO);
	PhysicalStoreInfo.pszOpenStoreProvider  = (LPSTR)CLAUERSTORE_OID;
	PhysicalStoreInfo.dwFlags               = CERT_PHYSICAL_STORE_REMOTE_OPEN_DISABLE_FLAG;
	PhysicalStoreInfo.OpenParameters.pbData = NULL;
	PhysicalStoreInfo.OpenParameters.cbData = 0;
	PhysicalStoreInfo.dwPriority            = 10;

	// Register the physical store.

	if( ! CertRegisterPhysicalStore(
		L"MY",
		CERT_SYSTEM_STORE_CURRENT_USER,
		L"ClauerStore",
		&PhysicalStoreInfo,
		NULL
		))
	{
		return 1;
	}

	return 0;
}



