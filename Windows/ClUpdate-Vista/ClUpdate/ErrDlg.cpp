// ErrDlg.cpp : implementation file
//

#include "stdafx.h"
#include "ClUpdate.h"
#include "ErrDlg.h"

#include "lang.h"



// CErrDlg dialog

IMPLEMENT_DYNAMIC(CErrDlg, CDialog)

CErrDlg::CErrDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CErrDlg::IDD, pParent)
	, m_desc(_T(""))
	, m_errDesc(_T(""))
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

CErrDlg::~CErrDlg()
{
}

void CErrDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_DESCRIPCION, m_desc);
	DDX_Text(pDX, IDC_STATIC_ERR_DESC, m_errDesc);
}


BEGIN_MESSAGE_MAP(CErrDlg, CDialog)
	ON_BN_CLICKED(IDC_CERRAR, &CErrDlg::OnBnClickedCerrar)
	ON_WM_SHOWWINDOW()
	ON_STN_CLICKED(IDC_STATIC_ERR_DESC, &CErrDlg::OnStnClickedStaticErrDesc)
END_MESSAGE_MAP()


// Descripción del error

BOOL CErrDlg::SetErrDescription(UINT msgId)
{
	//m_errDesc.LoadString(msgId);
	m_errDesc = GetClauerString(msgId);
	return TRUE;
}

BOOL CErrDlg::SetDescription(UINT msgId)
{
	//m_desc.LoadString(msgId);
	m_desc = GetClauerString(msgId);
	return TRUE;
}

BOOL CErrDlg::SetCaption(UINT msgId)
{
	CString msg;
	//msg.LoadString(msgId);
	msg = GetClauerString(msgId);

	return TRUE;
}

void CErrDlg::OnBnClickedCerrar()
{
	EndDialog(0);
}

void CErrDlg::OnShowWindow(BOOL bShow, UINT nStatus)
{
	CDialog::OnShowWindow(bShow, nStatus);
	UpdateData(FALSE);
}

void CErrDlg::OnStnClickedStaticErrDesc()
{
	// TODO: Add your control notification handler code here
}


BOOL CErrDlg::OnInitDialog()
{
	SetIcon(m_hIcon, TRUE);
	SetIcon(m_hIcon, FALSE);
	return FALSE;
}
