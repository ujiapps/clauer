#ifndef __CLUPDATE_REG_STORE_H__
#define __CLUPDATE_REG_STORE_H__

#include "stdafx.h"
#include <windows.h>

#ifdef __cplusplus
extern "C" {
#endif

BOOL CLUPDATE_RegStoreMY   (void);
BOOL CLUPDATE_UnregStoreMY (void);

#ifdef __cplusplus
}
#endif

#endif
