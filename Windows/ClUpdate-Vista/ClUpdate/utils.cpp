#include "stdafx.h"
#include "utils.h"

#include <tchar.h>



#define MAX_NAME 256


// Basada en funci�n:
//
// http://msdn2.microsoft.com/en-US/library/aa379554.aspx

BOOL CLUPDATE_IsInAdministratorGroup ( BOOL & bIsInAdministrorsGroup )
{
	DWORD i, dwSize = 0, dwResult = 0;
	HANDLE hToken;
	PTOKEN_GROUPS pGroupInfo;
	BYTE sidBuffer[100];
	PSID pSID = (PSID)&sidBuffer;
	SID_IDENTIFIER_AUTHORITY SIDAuth = SECURITY_NT_AUTHORITY;

	   
	if (!OpenProcessToken( GetCurrentProcess(), TOKEN_QUERY, &hToken )) {
		return FALSE;
	}

	if(!GetTokenInformation(hToken, TokenGroups, NULL, dwSize, &dwSize)) {
		dwResult = GetLastError();
		if( dwResult != ERROR_INSUFFICIENT_BUFFER ) {
			return FALSE;
		}
	}
	pGroupInfo = (PTOKEN_GROUPS) GlobalAlloc( GPTR, dwSize );
	if ( ! pGroupInfo )
		return FALSE;

	if( ! GetTokenInformation(hToken, TokenGroups, pGroupInfo, 
							dwSize, &dwSize ) ) 
	{
		GlobalFree(pGroupInfo);
		return FALSE;
	}

	// Create a SID for the BUILTIN\Administrators group.

	if(! AllocateAndInitializeSid( &SIDAuth, 2,
					 SECURITY_BUILTIN_DOMAIN_RID,
					 DOMAIN_ALIAS_RID_ADMINS,
					 0, 0, 0, 0, 0, 0,
					 &pSID) ) 
	{
		GlobalFree(pGroupInfo);
		return FALSE;   
	}

	// Loop through the group SIDs looking for the administrator SID.

	bIsInAdministrorsGroup = FALSE;
	for(i=0; i<pGroupInfo->GroupCount; i++) {
		if ( EqualSid(pSID, pGroupInfo->Groups[i].Sid) ) {
			bIsInAdministrorsGroup = TRUE;
			break;
		}
	}

	if (pSID)
		FreeSid(pSID);
	if ( pGroupInfo )
		GlobalFree( pGroupInfo );

	return TRUE;
}



BOOL CLUPDATE_IsWindowsVista ( BOOL &is )
{
	BOOL ret = TRUE;
	OSVERSIONINFO ver;

	ZeroMemory(&ver, sizeof ver);
	ver.dwOSVersionInfoSize = sizeof ver;

	if ( ! GetVersionEx(&ver) ) {
		DWORD dwerr = GetLastError();
		ret = FALSE;
		goto endCLUPDATE_IsWindowsVista;
	}

	is = (ver.dwMajorVersion == 6);

endCLUPDATE_IsWindowsVista:

	return ret;

}



BOOL CLUPDATE_IsElevated ( BOOL &bIsAdmo )
{
	BOOL ret = TRUE, bVista;
	HANDLE hToken = 0, hProcess = 0;
	DWORD dwInfoSize = 0;

	SID_IDENTIFIER_AUTHORITY NtAuthority = SECURITY_NT_AUTHORITY;
	PSID AdministratorsGroup = 0; 

	if (  ! CLUPDATE_IsWindowsVista(bVista) ) {
		ret = FALSE;
		goto endCLUPDATE_UserIsAdmo;
	}

	if ( bVista ) {

		hProcess = GetCurrentProcess();   // no hace falta cerrarlo
		if ( ! hProcess ) {
			ret = FALSE;
			goto endCLUPDATE_UserIsAdmo;
		}
		if ( ! OpenProcessToken(hProcess, TOKEN_QUERY, &hToken) ) {
			ret = FALSE;
			goto endCLUPDATE_UserIsAdmo;
		}
		TOKEN_ELEVATION elevation;
		DWORD dwElevationSize = sizeof elevation;
		if ( ! GetTokenInformation(hToken, TokenElevation, (LPVOID) &elevation, dwElevationSize, &dwElevationSize) ) {
			ret = FALSE;
			goto endCLUPDATE_UserIsAdmo;
		}

		/*
		    TRUE - the current process is elevated. This value indicates that either UAC is enabled,
			       and the process was elevated by the administrator, or that UAC is disabled and the process
				   was started by a user who is a member of the Administrators group.

            FALSE - the current process is not elevated (limited). This value indicates that either UAC is
			        enabled, and the process was started normally, without the elevation, or that UAC is
					disabled and the process was started by a standard user.
		*/

		if ( elevation.TokenIsElevated ==  0 )
			bIsAdmo = FALSE;
		else
			bIsAdmo = TRUE;

	} else {

		if ( ! AllocateAndInitializeSid(&NtAuthority, 
					  2, 
					  SECURITY_BUILTIN_DOMAIN_RID,
					  DOMAIN_ALIAS_RID_ADMINS,
					  0, 0, 0, 0, 0, 0,
					  &AdministratorsGroup) ) 
		{
			ret = FALSE;
			goto endCLUPDATE_UserIsAdmo;
		}

		if ( ! CheckTokenMembership( NULL, AdministratorsGroup, &bIsAdmo) ) {
			ret = FALSE;
			goto endCLUPDATE_UserIsAdmo;
		}
	}

endCLUPDATE_UserIsAdmo:

	if ( hToken )
		CloseHandle(hToken);
	if ( AdministratorsGroup )
		FreeSid(AdministratorsGroup);

	return ret;
}

extern HANDLE g_hMutexOneInstance;

BOOL CLUPDATE_LaunchElevated ( BOOL &bCancelled )
{
	BOOL ret = TRUE;
	TCHAR szFileName[MAX_PATH];

	if ( ! GetModuleFileName(NULL, szFileName, MAX_PATH) )
		return FALSE;

	// Lo que hacemos ahora es eliminar el mutex global para la detecci�n
	// de una misma instancia de clupdate corriendo
	
	if ( g_hMutexOneInstance ) {
		CloseHandle(g_hMutexOneInstance);
		g_hMutexOneInstance = 0;
	}

	// Ahora lo lanzamos elevated
	
    SHELLEXECUTEINFO shex;

    memset( &shex, 0, sizeof( shex) );
    shex.cbSize        = sizeof( SHELLEXECUTEINFO );
    shex.fMask         = 0;
    shex.hwnd          = NULL;
    shex.lpVerb        = _T("runas");
    shex.lpFile        = szFileName;
    shex.lpParameters  = _T("/INI");
   // shex.lpDirectory   = NULL;
    shex.nShow         = SW_NORMAL;

	// Debo comprobar si el usuario puls� cancelar o no
	ret = ::ShellExecuteEx( &shex );
	if ( ! ret ) {
		if ( GetLastError() == ERROR_CANCELLED ) {
			ret = TRUE;
			bCancelled = TRUE;
		} else {
			ret = FALSE;
			bCancelled = FALSE;
		}
		g_hMutexOneInstance = CreateMutex(NULL, TRUE, _T("Local\\CLUPDATE_ONE_INSTANCE"));
	} else {
		bCancelled = FALSE;
	}

    return ret;
}

