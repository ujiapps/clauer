// ClUpdate.cpp : define los comportamientos de las clases para la aplicaci�n.
//

#include "stdafx.h"
#include "ClUpdate.h"
#include "ClUpdateDlg.h"
#include "regstore.h"
#include "ErrDlg.h"

#include "ThrDownload.h"
#include "ThrNotifier.h"
#include "ThrDelete.h"

#include "ClupdateReg.h"

#include <lang/resource.h>
#include "lang.h"

#include "utils.h"


#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// Handle a la DLL de idioma
HMODULE g_hLangDll = 0;


// CClUpdateApp

BEGIN_MESSAGE_MAP(CClUpdateApp, CWinApp)
	ON_COMMAND(ID_HELP, &CWinApp::OnHelp)
END_MESSAGE_MAP()


// Construcci�n de CClUpdateApp

CClUpdateApp::CClUpdateApp()
{
	// TODO: agregar aqu� el c�digo de construcci�n,
	// Colocar toda la inicializaci�n importante en InitInstance
}


// El �nico objeto CClUpdateApp

CClUpdateApp theApp;


HANDLE g_hMutexOneInstance = 0;


// Inicializaci�n de CClUpdateApp

BOOL CClUpdateApp::InitInstance()
{
	// Windows XP requiere InitCommonControlsEx() si un manifiesto de
	// aplicaci�n especifica el uso de ComCtl32.dll versi�n 6 o posterior para habilitar
	// estilos visuales. De lo contrario, se generar� un error al crear ventanas.

	INITCOMMONCONTROLSEX InitCtrls;
	InitCtrls.dwSize = sizeof(InitCtrls);
	InitCtrls.dwICC = ICC_WIN95_CLASSES;
	InitCommonControlsEx(&InitCtrls);

	CWinApp::InitInstance();
	AfxEnableControlContainer();
	//SetRegistryKey(_T("Aplicaciones generadas con el Asistente para aplicaciones local"));

    UPDATE_MODE um; 
    if ( ! CClupdateReg::GetRegUpdateMode(&um) ) { 
            return FALSE; 
    } 

	// Cargamos la DLL de idioma correspondiente

	HKEY hKey;
	DWORD dwType, dwSize;
	TCHAR idioma[10];

	if ( RegOpenKeyEx(HKEY_LOCAL_MACHINE, _T("Software\\Universitat Jaume I\\Projecte Clauer"), 0, KEY_READ, &hKey) != ERROR_SUCCESS ) {
		if ( um != UPDMODE_AUTO )
			AfxMessageBox(_T("Cannot get language to use.\r\nPlease, reinstall the Clauer Software"), MB_ICONERROR | MB_OK);
		return FALSE;
	}
	if ( RegQueryValueEx(hKey, _T("Idioma"), NULL, &dwType, NULL, &dwSize) != ERROR_SUCCESS ) {
		RegCloseKey(hKey);
		if ( um != UPDMODE_AUTO )
			AfxMessageBox(_T("Cannot get language to use.\r\nPlease, reinstall the Clauer Software"), MB_ICONERROR | MB_OK);
		return FALSE;		
	}
	if ( dwType != REG_SZ ) {
		RegCloseKey(hKey);
		if ( um != UPDMODE_AUTO )
			AfxMessageBox(_T("Cannot get language to use.\r\nPlease, reinstall the Clauer Software"), MB_ICONERROR | MB_OK);
		return FALSE;				
	}
	if ( dwSize > 10*sizeof(TCHAR) ) {
		RegCloseKey(hKey);
		if ( um != UPDMODE_AUTO )
			AfxMessageBox(_T("Cannot get language to use.\r\nPlease, reinstall the Clauer Software"), MB_ICONERROR | MB_OK);
		return FALSE;		
	}
	if ( RegQueryValueEx(hKey, _T("Idioma"), NULL, &dwType, (LPBYTE) idioma, &dwSize) != ERROR_SUCCESS ) {
		RegCloseKey(hKey);
		if ( um != UPDMODE_AUTO )
			AfxMessageBox(_T("Cannot get language to use.\r\nPlease, reinstall the Clauer Software"), MB_ICONERROR | MB_OK);
		return FALSE;		
	}
	RegCloseKey(hKey);
	if ( _tcsncmp(idioma, _T("1027"), 10) == 0 ) {
		/* catal�n */
		g_hLangDll = LoadLibrary(_T("va_language.dll"));
		if ( ! g_hLangDll ) {
			AfxMessageBox(_T("Cannot load va_language.dll.\r\nPlease, reinstall the Clauer Software"), MB_ICONERROR | MB_OK);
			return FALSE;
		}
	} else if ( _tcsncmp(idioma, _T("1027"), 10) == 0 ) { 
		g_hLangDll = LoadLibrary(_T("ar_language.dll"));
		if ( ! g_hLangDll ) {
			if ( um != UPDMODE_AUTO )
				AfxMessageBox(_T("Cannot load va_language.dll.\r\nPlease, reinstall the Clauer Software"), MB_ICONERROR | MB_OK);
			return FALSE;
		}
	
	} else {
		/* espa�ol */

		g_hLangDll = LoadLibrary(_T("es_language.dll"));
		if ( ! g_hLangDll ) {
			if ( um != UPDMODE_AUTO )
				AfxMessageBox(_T("Cannot load es_language.dll.\r\nPlease, reinstall the Clauer Software"), MB_ICONERROR|MB_OK);
			return FALSE;
		}
	}

	// Controlamos que exista una sola instancia de Clauer Update

	g_hMutexOneInstance = CreateMutex(NULL, TRUE, _T("Local\\CLUPDATE_ONE_INSTANCE"));
	if ( g_hMutexOneInstance ) {
		if ( GetLastError() == ERROR_ALREADY_EXISTS ) {
			ReleaseMutex(g_hMutexOneInstance);
			CErrDlg errDlg;
			errDlg.SetDescription(IDS_UPDATE_ERR);
			errDlg.SetErrDescription(IDS_UPDATE_CLUPDATE_ALREADY_EXECUTING);
			errDlg.DoModal();
			return FALSE;
		}
		ReleaseMutex(g_hMutexOneInstance);
	}

	// Registro el store

	CClupdateReg reg;
	time_t diasQuedan, fechaUpdate;

	if ( ! reg.Open() ) {
		AfxMessageBox(GetClauerString(IDS_UPDATE_NO_UPD_PENDING), MB_ICONERROR);
		return FALSE;
	}
	if ( ! reg.GetType(&dwType) ) {
		AfxMessageBox(GetClauerString(IDS_UPDATE_NO_UPD_PENDING), MB_ICONERROR);
		return FALSE;
	}

	/* Si hay una actualizaci�n cr�tica pendiente y ha pasado el per�odo
	 * de actualizaci�n no registramos el store
	 */

	if ( dwType == 1 ) {
		if ( ! reg.GetDate(&fechaUpdate) ){
			AfxMessageBox(GetClauerString(IDS_UPDATE_EXPIRE_CRITICAL), MB_ICONERROR);
			return FALSE;
		}
		diasQuedan = 15 - (time(NULL)-fechaUpdate)/(24*3600);
		if ( diasQuedan <= 0 ) {
			CLUPDATE_UnregStoreMY();
		} else {
			CLUPDATE_RegStoreMY();
		}
	} else {
		CLUPDATE_RegStoreMY();
	}

	// Si el modo de actualizaci�n es autom�tico, entonces nos salimos

	if ( um == UPDMODE_AUTO )
		return FALSE;

	// Si hab�a alguna actualizaci�n previa en el escritorio la borro mediante el thread de borrado

	AfxBeginThread(CLUPDATE_THR_Delete, NULL);

	// Creamos la ventana principal no modal. �sta ser� la que reciba
	// los mensajes de notificaci�n

	CClUpdateDlg *dlg = new CClUpdateDlg();
	if ( ! dlg )
		return FALSE;

	m_pMainWnd = dlg;
	if ( ! dlg->Create(IDD_CLUPDATE_DIALOG) ) {
		delete dlg;
		return FALSE;
	}

	dlg->ShowWindow(SW_HIDE);

	// Empezamos con el bucle de mensajes

	theApp.Run();

	delete dlg;

	return FALSE;
}

