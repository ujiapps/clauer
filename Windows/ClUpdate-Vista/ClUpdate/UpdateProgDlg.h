#pragma once


// CUpdateProgDlg dialog

class CUpdateProgDlg : public CDialog
{
	DECLARE_DYNAMIC(CUpdateProgDlg)

public:
	CUpdateProgDlg(CWnd* pParent = NULL, BOOL bCritical = FALSE);   // standard constructor
	virtual ~CUpdateProgDlg();

// Dialog Data
	enum { IDD = IDD_DIALOG_UPDATE_PROGRESS };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	BOOL m_bCritical;

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnBnClickedOk();
public:
	afx_msg void OnBnClickedBtnIniciar();
};
