//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by ClUpdate.rc
//
#define IDM_ABOUTBOX                    0x0010
#define IDD_ABOUTBOX                    100
#define IDD_CLUPDATE_DIALOG             102
#define IDI_UJI                         130
#define IDR_MENU_CTX                    131
#define IDD_DIALOG1                     132
#define IDD_DIALOG_ERROR                132
#define IDD_DLG_DESCARGAR               141
#define IDD_DLG_INSTALAR                142
#define IDD_DLG_RECORDAR                143
#define IDD_DLG_CERRAR                  144
#define IDB_UPDATE_TOP                  146
#define IDB_ERR_UPDATE_TOP              147
#define IDB_ABOUT                       149
#define IDR_MAINFRAME                   151
#define IDC_CERRAR                      1000
#define IDC_DESCRIPCION                 1001
#define IDC_EDIT_ERR_DESC               1003
#define IDC_BUTTON_INICIAR              1012
#define IDC_STATIC_ERR_DESC             1014
#define IDC_STATIC_DESC_PROGRESO        1016
#define IDC_FRAME                       1017
#define IDC_COMBO1                      1020
#define IDC_STATIC_PUSH_DOWNLOAD        1021
#define IDC_STATIC_FINISHED_UPDATE_NOTIFY 1022
#define IDC_STATIC_DESC_INSTALAR        1023
#define IDC_STATIC_SET_PERIOD           1024
#define IDC_STATIC_REMEMBER             1025
#define ID_OPCIONES_IRA                 32771
#define ID_IRA_PROYECTOCLAUER           32772
#define ID_IRA_DESARROLLADORESDELPROYECTOCLAUER 32773
#define ID_IRA_UNIVERSITATJAUMEI        32774
#define ID_IRA_NISUSECURITY             32775
#define ID_OPCIONES_COMPROBARACTUALIZACIONES 32776
#define ID_OPCIONES_ACERCADE            32777
#define ID_OPCIONES_AYUDA               32778
#define ID_OPCIONES_CERRAR              32779
#define ID_OPCIONES_TEST                32780
#define ID_IRA_AYUDA                    32781
#define ID_OPCIONES_MODOSILECIONSO      32782
#define ID_OPCIONES_MODOSILENCIOSO      32783
#define ID_OPCIONES_IRA32784            32784
#define ID_OPCIONES_IRA32785            32785
#define ID_OPCIONES_IRA32786            32786
#define ID_IRA_IRA1                     32787
#define ID_IRA_IRA2                     32788
#define ID_IRA_IRA3                     32789
#define ID_OPCIONES_MODO                32790
#define ID_MODO_NORMAL                  32791
#define ID_MODO_EXPERTO                 32792
#define ID_MODO_S32793                  32793
#define ID_MODO_INICIO                  32794

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        152
#define _APS_NEXT_COMMAND_VALUE         32797
#define _APS_NEXT_CONTROL_VALUE         1026
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
