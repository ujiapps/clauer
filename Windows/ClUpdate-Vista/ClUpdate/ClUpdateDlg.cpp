// ClUpdateDlg.cpp: archivo de implementaci�n
//

#include "stdafx.h"

#include <windows.h>
#include <wincred.h>
#include <shellapi.h>

#include <string.h>

#include "ClUpdate.h"
#include "ClUpdateDlg.h"
#include "ErrDlg.h"
#include "AppMessages.h"
#include "Notify.h"
#include "regstore.h"
#include "ThrNotifier.h"
#include "ThrDownload.h"
#include "ClupdateReg.h"
#include "DlgDescargar.h"
#include "DlgInstalar.h"
#include "Parse.h"
#include "ClupdateReg.h"

#include "lang.h"
#include <lang/resource.h>

#include "common_defines.h"
#include "utils.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

#define TIMER_FLASH             1
#define TIMER_NOTIFY_DOWNLOAD	2	// Utilizado cuando se descarga una actualizaci�n 
									// y se cierra la ventana

#define TIMER_THR_NOTIFIER		3   // Utilizado para lanzar el thread notificador


#define TIMER_NOTIFY_DOWNLOAD_ELAP	1800000    // media hora


// Cuadro de di�logo CAboutDlg utilizado para el comando Acerca de

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// Datos del cuadro de di�logo
	enum { IDD = IDD_ABOUTBOX };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // Compatibilidad con DDX/DDV

// Implementaci�n
protected:
	DECLARE_MESSAGE_MAP()

};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
END_MESSAGE_MAP()


// Cuadro de di�logo de CClUpdateDlg


CClUpdateDlg::CClUpdateDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CClUpdateDlg::IDD, pParent)
	, m_popMenu(NULL)
	, m_bUpdateNotified(FALSE)
	, m_editDescription(_T(""))
{
	m_hIcon = AfxGetApp()->LoadIcon(IDI_UJI);
	m_showingAbout = FALSE;
	m_dwTimerNotifierElap = DEFAULT_TIMER_NOTIFIER_ELAP;
	m_bExplicitUpdateAvailable = FALSE;
	m_notify = NULL;
	m_tNextNotify = 0;
}

void CClUpdateDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_BUTTON_INICIAR, m_btnDwnIns);

}

BEGIN_MESSAGE_MAP(CClUpdateDlg, CDialog)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	//}}AFX_MSG_MAP
	ON_MESSAGE(WM_CLUPDATE, OnClupdate)
	ON_MESSAGE(WM_CLUPDATE_UPDATE, OnUpdateAvailable)
	ON_MESSAGE(WM_CLUPDATE_DOWNLOAD, OnDownloadFinished)
	ON_COMMAND(ID_OPCIONES_COMPROBARACTUALIZACIONES, &CClUpdateDlg::OnOpcionesComprobaractualizaciones)
	ON_WM_CONTEXTMENU()
	ON_COMMAND(ID_OPCIONES_ACERCADE, &CClUpdateDlg::OnOpcionesAcercade)
	ON_COMMAND(ID_OPCIONES_TEST, &CClUpdateDlg::OnOpcionesTest)
	ON_COMMAND(ID_IRA_PROYECTOCLAUER, &CClUpdateDlg::OnIraProyectoclauer)
	ON_COMMAND(ID_IRA_DESARROLLADORESDELPROYECTOCLAUER, &CClUpdateDlg::OnIraDesarrolladoresdelproyectoclauer)
	ON_COMMAND(ID_IRA_UNIVERSITATJAUMEI, &CClUpdateDlg::OnIraUniversitatjaumei)
	ON_COMMAND(ID_IRA_NISUSECURITY, &CClUpdateDlg::OnIraNisusecurity)
	ON_WM_TIMER()
	ON_WM_SETFOCUS()
	ON_BN_CLICKED(IDC_BUTTON_INICIAR, &CClUpdateDlg::OnBnClickedButtonIniciar)
	ON_WM_SHOWWINDOW()
	ON_WM_CLOSE()
	ON_WM_KEYDOWN()
	ON_WM_DESTROY()
	ON_COMMAND(ID_OPCIONES_CERRAR, &CClUpdateDlg::OnOpcionesCerrar)
	ON_COMMAND(ID_IRA_AYUDA, &CClUpdateDlg::OnIraAyuda)
	ON_COMMAND(ID_IRA_IRA1, &CClUpdateDlg::OnIraIra1)
	ON_COMMAND(ID_IRA_IRA2, &CClUpdateDlg::OnIraIra2)
	ON_COMMAND(ID_IRA_IRA3, &CClUpdateDlg::OnIraIra3)
	ON_COMMAND(ID_MODO_EXPERTO, &CClUpdateDlg::OnModoExperto)
	ON_COMMAND(ID_MODO_NORMAL, &CClUpdateDlg::OnModoNormal)
	ON_COMMAND(ID_MODO_INICIO, &CClUpdateDlg::OnModoInicio)
END_MESSAGE_MAP()


// Controladores de mensaje de CClUpdateDlg

BOOL CClUpdateDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		CString strAboutMenu;
		//strAboutMenu.LoadString(IDS_UPDATE_ABOUTBOX);
		strAboutMenu = GetClauerString(IDS_UPDATE_ABOUTBOX);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}
	
	// Establecer el icono para este cuadro de di�logo. El marco de trabajo realiza esta operaci�n
	// autom�ticamente cuando la ventana principal de la aplicaci�n no es un cuadro de di�logo

	SetIcon(m_hIcon, TRUE);			// Establecer icono grande
	SetIcon(m_hIcon, FALSE);		// Establecer icono peque�o

	// Creamos el notificador

	try {

		/* Parseamos la l�nea de comandos en busca del par�metro /ini. Si est�
		 * m_bFirstExit == TRUE. Si se indic� /ini retrasamos el mostrar el icono
		 * de notificaci�n para m�s tarde
		 */

		UPDATE_MODE um;
		CParse parse;
		AfxGetApp()->ParseCommandLine(parse);
		m_bFirstExit = parse.m_bIni;

		m_notify = new CNotify();
		if ( ! m_notify ) 
			throw 1;

		CClupdateReg reg;
		reg.Open();
		reg.GetUpdateMode(&um);

		if ( ! reg.GetUpdatePeriod(&m_dwTimerNotifierElap) )
			m_dwTimerNotifierElap = DEFAULT_TIMER_NOTIFIER_ELAP;

		reg.Close();
		if ( ! (um == UPDMODE_INI && m_bFirstExit) )
			if ( ! m_notify->Show() )
				throw 2;

		m_bFlash = FALSE;

		/* Si es la primera vez que se ejecuta el update, lo que hacemos es indicar
		 * que se actualiz� con �xito o sin �xito
		 */

		char version[VERSION_LEN+1], old_version[VERSION_LEN+1];

		if ( CClupdateReg::GetOldVersion(old_version) ) {
			if ( CClupdateReg::GetVersion(version) ) {
				
				/* Si version > old_version ---> termin� una actualizaci�n con �xito
				 */

				CString msg, title;
				int i;
				BOOL bHide;

				if ( m_notify->Hidden() ) {
					bHide = TRUE;
					m_notify->Show();
				} else
					bHide = FALSE;

				for ( i = 0 ; version[i] && (version[i] == old_version[i]); i++ );

				if ( version[i] > old_version[i] ) {
					//msg.LoadString(IDS_UPDATE_UPDATE_OK_TEXT);
					//title.LoadString(IDS_UPDATE_UPDATE_OK_TITLE);
					msg   = GetClauerString(IDS_UPDATE_UPDATE_OK_TEXT);
					title = GetClauerString(IDS_UPDATE_UPDATE_OK_TITLE);
					m_notify->Notify(title, msg);
				} else {
					//msg.LoadString(IDS_UPDATE_UPDATE_ERROR_TEXT);
					//title.LoadString(IDS_UPDATE_UPDATE_ERROR_TITLE);
					msg = GetClauerString(IDS_UPDATE_UPDATE_ERROR_TEXT);
					title = GetClauerString(IDS_UPDATE_UPDATE_OK_TITLE);
					m_notify->Notify(title, msg);
				}

				CClupdateReg::DeleteOldVersion();

				if ( bHide )
					m_notify->Hide();

			}
		}

		/* Lanzamos el thread notificador
		 */

		THRNOTIFIERPARAM *param;
		CWinThread *currentThread;

		param = new THRNOTIFIERPARAM;
		if ( ! param )
			throw 3;

		param->wait = new CEvent();
		if ( ! param->wait )
			throw 4;
		m_eventThrNotifier = param->wait;

		currentThread = AfxGetThread();
		if ( ! currentThread )
			throw 5;

		param->dwParentThreadId = currentThread->m_nThreadID;
		param->wnd              = this;

		AfxBeginThread(CLUPDATE_THR_Notifier, param);

		/* Creamos los di�logos del frame del di�logo principal
		 */

		CWnd *pWndFrame;
		CRect rect;

		pWndFrame = GetDlgItem(IDC_FRAME);
		pWndFrame->GetWindowRect(&rect);
		ScreenToClient(&rect);

		m_dlgDescargar = new CDlgDescargar;
		if ( ! m_dlgDescargar )
			throw 6;
		if ( ! m_dlgDescargar->Create(IDD_DLG_DESCARGAR, this) )
			throw 7;
		if ( ! m_dlgDescargar->SetWindowPos(pWndFrame, rect.left, rect.top, rect.right - rect.left, rect.bottom - rect.top,SWP_SHOWWINDOW) )
			throw 8;
		m_dlgDescargar->ShowWindow(WS_CHILD|WS_VISIBLE);

		m_dlgInstalar = new CDlgInstalar;
		if ( ! m_dlgInstalar )
			throw 9;
		if ( ! m_dlgInstalar->Create(IDD_DLG_INSTALAR, this) )
			throw 10;
		if ( ! m_dlgInstalar->SetWindowPos(pWndFrame, rect.left, rect.top, rect.right - rect.left, rect.bottom - rect.top,SWP_HIDEWINDOW) )
			throw 11;
		m_dlgInstalar->ShowWindow(WS_CHILD|WS_VISIBLE);

		m_dlgRecordar = new CDlgRecordar;
		if ( ! m_dlgRecordar )
			throw 12;
		if ( ! m_dlgRecordar->Create(IDD_DLG_RECORDAR, this) )
			throw 13;
		if ( ! m_dlgRecordar->SetWindowPos(pWndFrame, rect.left, rect.top, rect.right - rect.left, rect.bottom - rect.top, SWP_HIDEWINDOW) )
			throw 14;
		m_dlgInstalar->ShowWindow(WS_CHILD|WS_VISIBLE);

		/* Asignamos el estado inicial
		 */

		m_st = ST_INI;
	}
	catch ( int err ) {

		/* Un error en la inicializaci�n y salimos del update
		 */

		BOOL hidden;
		if ( m_notify ) {
			hidden = m_notify->Hidden();
			m_notify->Hide();
		} else
			hidden = TRUE;

		CErrDlg dlg(this);
		dlg.SetDescription(IDS_UPDATE_ERR_INI_DESC);
		dlg.SetErrDescription(IDS_UPDATE_ERR_OUT_OF_MEMORY);
		dlg.DoModal();
		if ( ! hidden )
			m_notify->Show();
		PostQuitMessage(1);

		// chapuza: esto lo hago para evitarme el warning de que no uso err
		err = 0;

		return TRUE;
	}

	return TRUE;  // Devuelve TRUE  a menos que establezca el foco en un control
}

void CClUpdateDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialog::OnSysCommand(nID, lParam);
	}
}

//  Si agrega un bot�n Minimizar al cuadro de di�logo, necesitar� el siguiente c�digo
//  para dibujar el icono. Para aplicaciones MFC que utilicen el modelo de documentos y vistas,
//  esta operaci�n la realiza autom�ticamente el marco de trabajo.

void CClUpdateDlg::OnPaint()
{
	if (IsIconic())
	{
		CPaintDC dc(this); // Contexto de dispositivo para dibujo

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Centrar icono en el rect�ngulo de cliente
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Dibujar el icono
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

// El sistema llama a esta funci�n para obtener el cursor que se muestra mientras el usuario arrastra
// la ventana minimizada.

HCURSOR CClUpdateDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}



/* Runtina de tratamiento de mensajes para el icono de notificaci�n
 */

LRESULT CClUpdateDlg::OnClupdate( WPARAM wParam, LPARAM lParam)
{
	CPoint point;
	CMenu menu, *pop;
	CMenu *mnuIrA = NULL, *mnuModo = NULL;
	UPDATE_MODE um = UPDMODE_NORMAL;
	CClupdateReg reg;

	try {
		switch ( lParam ) {

			case WM_RBUTTONDOWN:

				/* Bot�n derecho del rat�n pulsado
				 */

				if ( ! menu.LoadMenu(IDR_MENU_CTX) )
					throw 1;

				pop = menu.GetSubMenu(0);
				if ( ! pop )
					throw 1;

				pop->SetDefaultItem(ID_OPCIONES_COMPROBARACTUALIZACIONES);
				
				menu.ModifyMenu(ID_MODO_NORMAL, MF_BYCOMMAND, ID_MODO_NORMAL, GetClauerString(IDS_UPDATE_NORMAL));
				menu.ModifyMenu(ID_MODO_EXPERTO, MF_BYCOMMAND, ID_MODO_EXPERTO, GetClauerString(IDS_UPDATE_EXPERT));
				menu.ModifyMenu(ID_MODO_INICIO, MF_BYCOMMAND, ID_MODO_INICIO, GetClauerString(IDS_UPDATE_INI));

				if ( ! ::GetCursorPos(&point) )
					throw 1;
				::SetForegroundWindow(this->m_hWnd);

				/* Dependiendo de la empresa, se insertan unas opciones para
				 * el men� Ir a... distintas
				 */

				mnuIrA = pop->GetSubMenu(1);
				if ( ! mnuIrA )
					throw 1;

				pop->ModifyMenu(0, MF_BYPOSITION, ID_OPCIONES_COMPROBARACTUALIZACIONES, GetClauerString(IDS_UPDATE_TEST_UPDATES));
				pop->ModifyMenu(1, MF_BYPOSITION, 0, GetClauerString(IDS_UPDATE_GO_TO));
				pop->ModifyMenu(3, MF_BYPOSITION, 0, GetClauerString(IDS_UPDATE_MODE));
				pop->ModifyMenu(5, MF_BYPOSITION, ID_OPCIONES_CERRAR, GetClauerString(IDS_UPDATE_CLOSE));

#ifdef CLUPDATE_PIPELINE

				if ( ! mnuIrA->ModifyMenu(0, MF_BYPOSITION, ID_IRA_IRA1, GetClauerString(IDS_UPDATE_HELP)) )
					throw 1;
				if ( ! mnuIrA->ModifyMenu(1, MF_BYPOSITION, ID_IRA_IRA2, GetClauerString(IDS_UPDATE_LU)) )
					throw 1;
				if ( ! mnuIrA->ModifyMenu(2, MF_BYPOSITION, ID_IRA_IRA3, GetClauerString(IDS_UPDATE_CLAUER_PROJECT)) )
					throw 1;

#elif CLUPDATE_CATCERT

				if ( ! mnuIrA->ModifyMenu(0, MF_BYPOSITION, ID_IRA_IRA1, GetClauerString(IDS_UPDATE_WEB_IDCAT)) )
					throw 1;
				if ( ! mnuIrA->ModifyMenu(1, MF_BYPOSITION, ID_IRA_IRA2, GetClauerString(IDS_UPDATE_WEB_CATCERT)) )
					throw 1;
				mnuIrA->DeleteMenu(2,MF_BYPOSITION);

#else

				if ( ! mnuIrA->ModifyMenu(0, MF_BYPOSITION, ID_IRA_IRA1, GetClauerString(IDS_UPDATE_HELP)) )
					throw 1;
				if ( ! mnuIrA->ModifyMenu(1, MF_BYPOSITION, ID_IRA_IRA2, GetClauerString(IDS_UPDATE_CLAUER_PROJECT)) )
					throw 1;
				if ( ! mnuIrA->ModifyMenu(2, MF_BYPOSITION, ID_IRA_IRA3, GetClauerString(IDS_UPDATE_CLAUER_DEVELOP)) )
					throw 1;
#endif

				reg.Open();
				reg.GetUpdateMode(&um);
				reg.Close();
				if ( um == UPDMODE_NORMAL )
					pop->CheckMenuItem(ID_MODO_NORMAL, MF_BYCOMMAND|MF_CHECKED);
				else if ( um == UPDMODE_INI )
					menu.CheckMenuItem(ID_MODO_INICIO, MF_BYCOMMAND|MF_CHECKED);
				else if ( um == UPDMODE_EXPERT )
					menu.CheckMenuItem(ID_MODO_EXPERTO, MF_BYCOMMAND|MF_CHECKED);

				if ( ! pop->TrackPopupMenu(TPM_LEFTALIGN | TPM_RIGHTBUTTON, point.x, point.y, AfxGetMainWnd()) )
					throw 1;

				break;

			case WM_LBUTTONDOWN:

				/* La pulsaci�n del bot�n izquierdo implica la comprobaci�n
				 * de una actualizaci�n
				 */

				reg.Open();
				reg.GetUpdateMode(&um);
				reg.Close();

				if ( m_st == ST_INI || ( (um == UPDMODE_NORMAL || um == UPDMODE_INI) && (m_st == ST_UPDATE || m_st == ST_UPDATEREMIND) ) ) {
					m_bExplicitUpdateAvailable = TRUE;
					m_eventThrNotifier->SetEvent();
				}

				/* En modo silencioso o modo ini, simplemente indicamos al thread notificador
				 * que lance la comprobaci�n
				 */

				if ( (um == UPDMODE_NORMAL) || ( um == UPDMODE_INI ) ) {
					if ( m_st == ST_UPDATEREMIND || m_st == ST_UPDATE || m_st == ST_INI ) {
						m_st = ST_INI;
						break;
					}
				}

			case NIN_BALLOONUSERCLICK:

				/* Mostramos la ventana si hay actualizaciones
				 * disponibles. Es decir, si estamos en estado
				 * ST_UPDATE (este estado se alcanza cuando
				 * el thread notificador indica existencia de
				 * actualizaci�n y se es administrador)
				 */

				reg.Open();
				reg.GetUpdateMode(&um);
				reg.Close();

				if ( um != UPDMODE_NORMAL && um != UPDMODE_INI ) {

					if ( (m_st == ST_UPDATE) || (m_st == ST_DOWNLOADED) || (m_st == ST_UPDATEREMIND) ) {

						CString msg;
						CWnd *btn = GetDlgItem(IDC_BUTTON_INICIAR);

						if ( m_st == ST_UPDATE ) {

							m_dlgInstalar->ShowWindow(SW_HIDE);
							m_dlgDescargar->ShowWindow(SW_SHOW);				
							m_dlgRecordar->ShowWindow(SW_HIDE);
							msg = GetClauerString(IDS_UPDATE_DOWNLOAD);
							btn->SetWindowText(msg.GetString());
							
						} else if ( m_st == ST_DOWNLOADED) {
							
							m_dlgInstalar->ShowWindow(SW_SHOW);
							m_dlgDescargar->ShowWindow(SW_HIDE);
							m_dlgRecordar->ShowWindow(SW_HIDE);
							msg = GetClauerString(IDS_UPDATE_INSTALL);
							btn->SetWindowText(msg.GetString());

						} else if ( m_st == ST_UPDATEREMIND ) {

							m_dlgRecordar->ShowWindow(SW_SHOW);
							m_dlgInstalar->ShowWindow(SW_HIDE);
							m_dlgDescargar->ShowWindow(SW_HIDE);
							msg = GetClauerString(IDS_UPDATE_ACEPTAR);
							btn->SetWindowText(msg.GetString());
						}

						if ( ! UpdateData(FALSE) )
							throw 1;

						ShowWindow(SW_SHOWNORMAL);
					}
				} else {

					/* Si estamos en modo silencioso (NORMAL o INI) llamo directamente a iniciar 
					 * descarga
					 */

					if ( m_st == ST_UPDATE || m_st == ST_DOWNLOADED )
						OnBnClickedButtonIniciar();

				}

				break;
		}
	}
	catch ( int err ) {
		BOOL bHidden;
		bHidden = m_notify->Hidden();

		CErrDlg dlg(this);
		dlg.SetDescription(IDS_UPDATE_ERR);
		dlg.SetErrDescription(IDS_UPDATE_ERR_OP_UNFINISHED);
		dlg.DoModal();
		if ( m_notify )
			m_notify->Hide();
		PostQuitMessage(1);
		ShowWindow(SW_HIDE);

		if ( ! bHidden )
			m_notify->Show();

		// chapuza: esto lo hago para evitarme el warning de que no uso err
		err = 0;

		return 0;
	}

	return 0;
}


/* Se dispara cuando el thread notificador indica que existe una actualizaci�n
 * disponible.
 *
 * Si wParam == 0 entonces actualizaci�n normal
 * si wParam == 1 entonces actualizaci�n cr�tica
 */

LRESULT CClUpdateDlg::OnUpdateAvailable ( WPARAM wParam, LPARAM lParam)
{
	BOOL bIsAdmo;
    SID_IDENTIFIER_AUTHORITY NtAuthority = SECURITY_NT_AUTHORITY;

	BOOL bCancelled;
	
	TCHAR szAux2[600];

	time_t aux;

	try { 

		/* Calculo la siguiente vez que el thread notificador
		 * tiene que comprobar actualizaciones
		 */

		CClupdateReg reg;

		time_t elap = m_tNextNotify;
		elap = elap - time(NULL);
		if ( (elap > (24*3600)) || (elap < 0) )
			elap = m_dwTimerNotifierElap;
		else
			elap *= 1000;


		SetTimer(TIMER_THR_NOTIFIER, (UINT) elap, NULL);		

		// Comprobamos si estamos en Vista

		BOOL bIsVista = FALSE;
		BOOL bIsElevated = FALSE;
		if ( ! CLUPDATE_IsWindowsVista(bIsVista) )
			throw 1;
		if ( ! CLUPDATE_IsElevated(bIsElevated) )
			throw 1;
		if ( ! CLUPDATE_IsInAdministratorGroup(bIsAdmo) )
			throw 1;

		/* �nicamente notificar� al usuario de actualizaci�n disponible, en los
		 * estados ST_UPDATE y ST_INI, es decir, cuando exista una actualizaci�n
		 * disponible pero no se haya iniciado la descarga o cuando no se haya
		 * iniciado nada
		 */

		if ( m_st == ST_UPDATE || m_st == ST_INI || m_st == ST_UPDATEREMIND ) {
			
			CString msg1, msg2;
			time_t fechaUpdate, diasQuedan;
			CClupdateReg reg;

			if ( ! reg.Open() ) 
				throw 1;
			if ( ! reg.GetDate(&fechaUpdate) )
				throw 1;
			reg.Close();

			diasQuedan = 15 - (time(NULL)-fechaUpdate)/(24*3600);

			switch ( wParam ) {

				case UPDATE_RELEASE:

					/* Actualizaci�n NW
					 */

					if ( m_bExplicitUpdateAvailable ) {

						// si Windows Vista, soy administrador y el proceso
						// no est� elevado, me relanzo a mi mismo como elevado

						if ( bIsVista && bIsAdmo && !bIsElevated ) {

							if ( ! CLUPDATE_LaunchElevated(bCancelled) ) 
								throw 1;

							if ( bCancelled ) {
								// Si el usuario cancel� la elevaci�n, indicamos que no
								// soy administrador y que siga el flujo normal
								bIsAdmo = FALSE;
							} else {
								if ( m_notify )
									m_notify->Hide();
								PostQuitMessage(0);	
								return 0;
							}

						}

						m_bExplicitUpdateAvailable = FALSE;
												
						if ( m_notify->BallonAvailable() ) {

							msg1 = GetClauerString(IDS_UPDATE_BALLON_NORMAL_TITLE);
							
							if ( bIsAdmo ) {
								msg2 = GetClauerString(IDS_UPDATE_CLUPDATE_NORMAL_ADMO);
							} else {
								msg2 = GetClauerString(IDS_UPDATE_CLUPDATE_NORMAL_USER);
							}

							if ( ! m_notify->Notify(msg1, msg2) )
								throw 1;
						} 
						if ( bIsAdmo ) {
							if ( m_st == ST_INI )
								m_st = ST_UPDATE;
						} else {
							if ( m_st == ST_INI )
								m_st = ST_UPDATEREMIND;
						}

					}

					break;

				case UPDATE_NEW:

					/* Actualizaci�n normal
					 */

					if ( m_st == ST_INI && m_bFirstExit ) 
						m_notify->Show();
					

					aux = m_tNextNotify - time(NULL);
					if ( (aux <= 0) || (aux >24*60*60) || m_bExplicitUpdateAvailable) {

						/* �nicamente notificamos en caso de que
						 * se nos indique que hay que notificar
						 * o bien si han modificado la llave del
						 * registro (nextnotify) y nos han puesto
						 * un valor estratosf�rico
						 */

						if ( bIsVista && bIsAdmo && !bIsElevated ) {

							if ( ! CLUPDATE_LaunchElevated(bCancelled) ) {
								throw 1;
							}
							if ( bCancelled ) {
								// Si el usuario cancel� la elevaci�n, indicamos que no
								// soy administrador y que siga el flujo normal
								bIsAdmo = FALSE;
							} else {
								if ( m_notify )
									m_notify->Hide();
								PostQuitMessage(0);	
								return 0;
							}
						}

						if ( m_bExplicitUpdateAvailable )
							m_bExplicitUpdateAvailable = FALSE;
												
						if ( m_notify->BallonAvailable() ) {
							msg1 = GetClauerString(IDS_UPDATE_BALLON_NORMAL_TITLE);
							
							if ( bIsAdmo ) {
								msg2 = GetClauerString(IDS_UPDATE_CLUPDATE_NORMAL_ADMO);
							} else {
								msg2 = GetClauerString(IDS_UPDATE_CLUPDATE_NORMAL_USER);
							}

							if ( ! m_notify->Notify(msg1, msg2) )
								throw 1;
						} else {
							
						}
					}


					if ( bIsAdmo ) {
						if ( m_st == ST_INI )
							m_st = ST_UPDATE;
					} else {
						if ( m_st == ST_INI )
							m_st = ST_UPDATEREMIND;
					}

					break;

				case UPDATE_CRITICAL:

					/* Actualizaci�n cr�tica 
					 */

					if ( bIsVista && bIsAdmo && !bIsElevated ) {
						if ( ! CLUPDATE_LaunchElevated(	bCancelled ) ) {
							throw 1;
						}
						if ( bCancelled ) {
							// Si el usuario cancel� la elevaci�n, indicamos que no
							// soy administrador y que siga el flujo normal
							bIsAdmo = FALSE;
						} else {
							if ( m_notify )
								m_notify->Hide();
							PostQuitMessage(0);	
							return 0;
						}
					}

					if ( m_st == ST_INI && m_bFirstExit ) 
						m_notify->Show();

					aux = m_tNextNotify - time(NULL);
					if ( (aux <= 0) || (aux >24*60*60) || m_bExplicitUpdateAvailable ) {

						if ( m_bExplicitUpdateAvailable )
							m_bExplicitUpdateAvailable = FALSE;

						msg1 = GetClauerString(IDS_UPDATE_BALLON_CRITICAL_TITLE);

						if ( bIsAdmo ) {

							if ( diasQuedan > 0 ) {							
								_stprintf(szAux2, GetClauerString(IDS_UPDATE_CLUPDATE_CRITICAL_ADMO), diasQuedan);
								msg2 = szAux2;
							} else {
								/* Versi�n caducada
								 */
								CLUPDATE_UnregStoreMY();
								msg2 = GetClauerString(IDS_UPDATE_CLUPDATE_EXPIRE_ADMO);
							}
							if ( ! m_notify->Notify(msg1, msg2) )
								throw 1;
			
						} else {

							if ( diasQuedan > 0 ) {
								_stprintf(szAux2, GetClauerString(IDS_UPDATE_CLUPDATE_CRITICAL_USER), diasQuedan);
								msg2 = szAux2;
							} else {
								/* Versi�n caducada
								 */
								CLUPDATE_UnregStoreMY();
								msg2 = GetClauerString(IDS_UPDATE_CLUPDATE_EXPIRE_USER);
							}
							
							if ( ! m_notify->Notify(msg1, msg2) )
								throw 1;
						}
					}

					if ( bIsAdmo ) {
						if ( m_st == ST_INI )
							m_st = ST_UPDATE;
					} else {
						if ( m_st == ST_INI )
							m_st = ST_UPDATEREMIND;
					}

					break;

				case UPDATE_OK:

					/* Si estamos en modo inicio y se pas� el argumento /ini
					 * aqu� termina la ejecuci�n del programa
					 */

					UPDATE_MODE um;
					reg.Open();
					reg.GetUpdateMode(&um);
					reg.Close();

					if ( m_bFirstExit && um == UPDMODE_INI ) {
						if ( m_notify )
							m_notify->Hide();
						PostQuitMessage(0);
						return 0;
					}

					/* Si se pidi� una comprobaci�n expl�cita, entonces
					 * indicamos que no hay actualizaciones disponibles
					 * y la versi�n del programa
					 */
					m_st = ST_INI;
					if ( m_bExplicitUpdateAvailable ) {

						m_bExplicitUpdateAvailable = FALSE;
						msg1 = GetClauerString(IDS_UPDATE_NO_UPDATES_TITLE);
						msg2 = GetClauerString(IDS_UPDATE_NO_UPDATES_TEXT);

						char v[VERSION_LEN+1];
						ZeroMemory(v, VERSION_LEN+1);
						CClupdateReg::GetVersion(v);

						CString msg_aux;
#ifdef UNICODE
						WCHAR wszVersion[VERSION_LEN+1];
						if ( ! MultiByteToWideChar(CP_ACP, MB_PRECOMPOSED, v, -1, wszVersion, VERSION_LEN+1) ) {
							msg_aux = msg2;
						} else {
							msg_aux.Format(msg2.GetString(), wszVersion);
						}
#else
						msg_aux.Format(msg2.GetString(), v);
#endif

						if ( ! m_notify->Notify(msg1, msg_aux.GetString()) )
							throw 1;
					}

					break;

				case UPDATE_ERROR:

					reg.Open();
					reg.GetUpdateMode(&um);
					reg.Close();

					if ( m_bFirstExit && um == UPDMODE_INI ) {
						if ( m_notify )
							m_notify->Hide();
						PostQuitMessage(0);
						return 0;
					}

					/* Se produjo un error y la comprobaci�n es expl�cita
					 * muestro mensaje de error
					 */

					if ( m_bExplicitUpdateAvailable ) {
						m_bExplicitUpdateAvailable = FALSE;
						msg1 = GetClauerString(IDS_UPDATE_COMPROBACION_IMPOSIBLE_TITLE);
						msg2 = GetClauerString(IDS_UPDATE_COMPROBACION_IMPOSIBLE);
						if ( ! m_notify->Notify(msg1, msg2) )
							throw 1;
					}

					break;
			}
		}
	}
	catch ( int err ) {
		m_notify->Hide();
		CErrDlg dlgErr(this);
		dlgErr.SetDescription(IDS_UPDATE_ERR);
		dlgErr.SetErrDescription(IDS_UPDATE_ERR_UPDATE_AVAILABLE);
		dlgErr.DoModal();
		PostQuitMessage(1);
		ShowWindow(SW_HIDE);

		// chapuza: esto lo hago para evitarme el warning de que no uso err
		err = 0;

	}

	return 0;
}




void CClUpdateDlg::OnOpcionesComprobaractualizaciones()
{
	m_bExplicitUpdateAvailable = TRUE;
	m_eventThrNotifier->SetEvent();
}



void CClUpdateDlg::OnContextMenu(CWnd* /*pWnd*/, CPoint /*point*/)
{

}



void CClUpdateDlg::OnOpcionesAcercade()
{
	CAboutDlg dlgAbout;

	/* Un error aqu� no es relevante. Simplemente no muestra
	 * el About Box
	 */

	if ( ! m_showingAbout ) {
		m_showingAbout = TRUE;
		dlgAbout.DoModal();
		m_showingAbout = FALSE;
	}
	
}


void CClUpdateDlg::OnOpcionesTest()
{
	
}

//void CClUpdateDlg::OnDestroy()
//{
//	CDialog::OnDestroy();
//	
//	if ( m_notify )
//		delete m_notify;
//}

void CClUpdateDlg::OnIraProyectoclauer()
{
	::ShellExecute(m_hWnd, _T("open"), _T("http://clauer.uji.es/"), NULL, NULL, SW_SHOWNORMAL);
}


void CClUpdateDlg::OnIraDesarrolladoresdelproyectoclauer()
{
	::ShellExecute(m_hWnd, _T("open"), _T("http://clauer.nisu.org/"), NULL, NULL, SW_SHOWNORMAL);
}

void CClUpdateDlg::OnIraUniversitatjaumei()
{
	::ShellExecute(m_hWnd, _T("open"), _T("http://www.uji.es/"), NULL, NULL, SW_SHOWNORMAL);
}

void CClUpdateDlg::OnIraNisusecurity()
{
	::ShellExecute(m_hWnd, _T("open"), _T("http://www.nisu.org/"), NULL, NULL, SW_SHOWNORMAL);
}

void CClUpdateDlg::OnTimer(UINT_PTR nIDEvent)
{
	CString title, msg;

	if ( nIDEvent == TIMER_FLASH ) 
		FlashWindow(TRUE);
	else if ( nIDEvent == TIMER_NOTIFY_DOWNLOAD ) {
		//title.LoadString(IDS_UPDATE_UPDATE_AVAILABLE_TITLE);
		//msg.LoadString(IDS_UPDATE_UPDATE_AVAILABLE_TEXT);
		title = GetClauerString(IDS_UPDATE_UPDATE_AVAILABLE_TITLE);
		msg = GetClauerString(IDS_UPDATE_UPDATE_AVAILABLE_TEXT);
		m_notify->Notify(title, msg);
	} else if ( nIDEvent == TIMER_THR_NOTIFIER ) {
		KillTimer(nIDEvent);
		if ( ! m_eventThrNotifier->SetEvent() )
			SetTimer(TIMER_THR_NOTIFIER, m_dwTimerNotifierElap, NULL);
	}

	CDialog::OnTimer(nIDEvent);
}

void CClUpdateDlg::OnSetFocus(CWnd* pOldWnd)
{
	CDialog::OnSetFocus(pOldWnd);

	/* Desactivo el timer de flash
	*/

	KillTimer(TIMER_FLASH);
}


void CClUpdateDlg::OnBnClickedButtonIniciar()
{
	/* Compruebo si soy o no soy administrador
	 */

	BOOL b;
    SID_IDENTIFIER_AUTHORITY NtAuthority = SECURITY_NT_AUTHORITY;
	PSID AdministratorsGroup; 
	HMODULE hCredUI = 0;

	try {
		if ( ! AllocateAndInitializeSid(&NtAuthority, 
					  2, 
					  SECURITY_BUILTIN_DOMAIN_RID,
					  DOMAIN_ALIAS_RID_ADMINS,
					  0, 0, 0, 0, 0, 0,
					  &AdministratorsGroup) ) 
		{
			CErrDlg errDlg(this);
			errDlg.SetDescription(IDS_UPDATE_ERR_ADMO_DESC);
			errDlg.SetErrDescription(IDS_UPDATE_ERR_ADMO_TEXT);
			errDlg.DoModal();
			ShowWindow(SW_HIDE);
			throw 1;
		}

		if ( ! CheckTokenMembership( NULL, AdministratorsGroup, &b) ) {
			FreeSid(AdministratorsGroup); 
			ShowWindow(SW_HIDE);
			throw 1;
		}

		FreeSid(AdministratorsGroup);
	}
	catch ( int err ) {
		BOOL hidden = m_notify->Hidden();
		m_notify->Hide();

		CErrDlg errDlg(this);
		errDlg.SetDescription(IDS_UPDATE_ERR_ADMO_DESC);
		errDlg.SetErrDescription(IDS_UPDATE_ERR_ADMO_TEXT);
		errDlg.DoModal();
		ShowWindow(SW_HIDE);

		m_notify->Show();

		// chapuza: esto lo hago para evitarme el warning de que no uso err

		err = 0;

		return;		
	}

	if ( m_st == ST_UPDATEREMIND ) {

		m_tNextNotify = time(NULL) + m_dlgRecordar->GetPeriod();

		CString msg;
		CWnd *btn = GetDlgItem(IDC_BUTTON_INICIAR);

		if ( b ) {
			m_st = ST_UPDATE;
			msg = GetClauerString(IDS_UPDATE_DOWNLOAD);
			btn->SetWindowText(msg.GetString());
			m_dlgRecordar->ShowWindow(SW_HIDE);
			m_dlgDescargar->ShowWindow(SW_SHOW);
			UpdateData(FALSE);
		} else {
			m_st = ST_INI;
		}

		m_notify->Show();
		ShowWindow(SW_HIDE);

	} else if ( m_st == ST_UPDATE ) {

		try {

			/* Si se nos ha notificado de la existencia de una actualizaci�n
			 * entonces empezamos con la descarga
			 */

			PTHRDOWNLOADPARAM thrDownloadParam;

			thrDownloadParam = new THRDOWNLOADPARAM;
			if ( ! thrDownloadParam )
				throw 1;
			ZeroMemory(thrDownloadParam, sizeof(THRDOWNLOADPARAM));
			thrDownloadParam->wnd = this;

			AfxBeginThread(CLUPDATE_THR_Download, thrDownloadParam);

			m_st = ST_DOWNLOADING;
			ShowWindow(SW_HIDE);

			CString msg;
			/*if ( ! msg.LoadString(IDS_UPDATE_TOOLTIP_DOWNLOADING) )
				throw 1;
				*/
			msg = GetClauerString(IDS_UPDATE_TOOLTIP_DOWNLOADING);
			m_notify->SetToolTip(msg);

		}
		catch ( int err ) {
			/* Si ocurre un error aqu�, lo que hacemos es notificarlo al usuario y transitar
			 * al estado INI para que se inicie de nuevo la notificaci�n
			 */
			BOOL hidden = m_notify->Hidden();
			m_notify->Hide();
			CErrDlg dlgErr(this);
			dlgErr.SetDescription(IDS_UPDATE_ERR);
			dlgErr.SetErrDescription(IDS_UPDATE_ERR_DOWNLOAD);
			dlgErr.DoModal();
			ShowWindow(SW_HIDE);
			if ( ! hidden )
				m_notify->Show();

			// chapuza: esto lo hago para evitarme el warning de que no uso err

			err = 0;
			return;
		}

	} else if ( m_st == ST_DOWNLOADED ) {

		/* Guardo la versi�n en CLUPDATE_OLD_VERSION
		 * para comprobar a posteriori si la actualizaci�n
		 * se realiz� de manera adecuada
		 */

		char version[VERSION_LEN+1];
		if ( ! CClupdateReg::GetVersion(version) ) {
			BOOL hidden = m_notify->Hidden();
			m_notify->Hide();
			CErrDlg dlgErr(this);
			dlgErr.SetDescription(IDS_UPDATE_ERR);
			dlgErr.SetErrDescription(IDS_UPDATE_ERR_INSTALL);
			dlgErr.DoModal();
			m_st = ST_INI;
			ShowWindow(SW_HIDE);
			if ( ! hidden )
				m_notify->Show();
			return;
		}

		if ( ! CClupdateReg::SetOldVersion(version) ) {
			BOOL hidden = m_notify->Hidden();
			m_notify->Hide();
			CErrDlg dlgErr(this);
			dlgErr.SetDescription(IDS_UPDATE_ERR);
			dlgErr.SetErrDescription(IDS_UPDATE_ERR_INSTALL);
			dlgErr.DoModal();
			m_st = ST_INI;
			ShowWindow(SW_HIDE);
			if ( ! hidden )
				m_notify->Show();
			return;
		}

		UPDATE_MODE um;

		STARTUPINFO si;
		PROCESS_INFORMATION pi;

		m_st = ST_INSTALLING;
		KillTimer(TIMER_NOTIFY_DOWNLOAD);

		ZeroMemory(&si, sizeof si);
		si.cb = sizeof si;
		ZeroMemory( &pi, sizeof pi );

		CClupdateReg reg;
		reg.Open();
		reg.GetUpdateMode(&um);
		reg.Close();

		if ( um == UPDMODE_NORMAL || um == UPDMODE_INI ) {
			CString commandLine = m_setupFile + ((LPTSTR) _T(" /S"));
			if ( ! CreateProcess(m_setupFile.GetString(), (LPTSTR) commandLine.GetString(), NULL, NULL, FALSE, 0, NULL, NULL, &si, &pi) ) {

				CClupdateReg::DeleteOldVersion();

				BOOL hidden = m_notify->Hidden();
				m_notify->Hide();
				CErrDlg dlgErr(this);
				dlgErr.SetDescription(IDS_UPDATE_ERR);
				dlgErr.SetErrDescription(IDS_UPDATE_ERR_INSTALL);
				dlgErr.DoModal();
				m_st = ST_INI;
				ShowWindow(SW_HIDE);
				if ( ! hidden )
					m_notify->Show();
				return;
			}
		} else {
			if ( ! CreateProcess(m_setupFile.GetString(), NULL, NULL, NULL, FALSE, 0, NULL, NULL, &si, &pi) ) {

				CClupdateReg::DeleteOldVersion();

				BOOL hidden = m_notify->Hidden();
				m_notify->Hide();
				CErrDlg dlgErr(this);
				dlgErr.SetDescription(IDS_UPDATE_ERR);
				dlgErr.SetErrDescription(IDS_UPDATE_ERR_INSTALL);
				dlgErr.DoModal();
				m_st = ST_INI;
				ShowWindow(SW_HIDE);
				if ( ! hidden )
					m_notify->Show();
				return;
			}
		}
		
		ShowWindow(SW_HIDE);

		if ( WaitForSingleObject(pi.hProcess, INFINITE) == WAIT_FAILED ) {
			/* Si falla la espera, vuelvo a un estado iniciar 
			 * y ya me matar� el instalador
			 */
			CClupdateReg::DeleteOldVersion();
			BOOL hidden = m_notify->Hidden();
			m_notify->Hide();
			CErrDlg dlgErr(this);
			dlgErr.SetDescription(IDS_UPDATE_ERR);
			dlgErr.SetErrDescription(IDS_UPDATE_ERR_INSTALL);
			dlgErr.DoModal();
			if ( ! hidden )
				m_notify->Show();
			m_st = ST_UPDATE;
			return;
		}
		
		/* Si llega a ejecutar este fragmento de c�digo
		 * es porque el update ha ido mal o el usuario cancel�
		 * la operaci�n
		 *
		 * TODO: ERROR
		 */

		CClupdateReg::DeleteOldVersion();

		m_st = ST_UPDATE;

		BOOL hidden = m_notify->Hidden();
		m_notify->Hide();
		CErrDlg dlgErr(this);
		dlgErr.SetDescription(IDS_UPDATE_ERR);
		dlgErr.SetErrDescription(IDS_UPDATE_ERR_INSTALL_CANCEL_ERROR);
		dlgErr.DoModal();
		if ( ! hidden )
			m_notify->Show();
	}

}



/* Invocado cuando el thread de descarga termina de realizar la descarga :-P
 *
 * wParam == 1 
 *        Ok y lParam es (TCHAR *) que indica la ruta al ejecutable
 *
 * wParam == 0
 *		  Error
 */

LRESULT CClUpdateDlg::OnDownloadFinished( WPARAM wParam, LPARAM lParam)
{
	CClupdateReg reg;
	CString title, msg;

	switch ( wParam ) {

		case 1:

			/* La descarga no produjo ning�n error
			 */

			m_st = ST_DOWNLOADED;
			m_setupFile = (TCHAR *) lParam;

			UPDATE_MODE um;

			reg.Open();
			reg.GetUpdateMode(&um);
			reg.Close();

			m_notify->SetToolTip(CString(""));
			if ( um == UPDMODE_NORMAL || um == UPDMODE_INI ) {
				/* En modo silencioso fuerzo el evento de pulsaci�n del
				 * globo
				 */

				OnClupdate(0, WM_LBUTTONDOWN);
			} else {

				//title.LoadString(IDS_UPDATE_BALLON_DOWNLOADED_TITLE);
				//msg.LoadString(IDS_UPDATE_BALLON_DOWNLOADED_TEXT);
				title = GetClauerString(IDS_UPDATE_BALLON_DOWNLOADED_TITLE);
				msg = GetClauerString(IDS_UPDATE_BALLON_DOWNLOADED_TEXT);
				m_notify->Notify(title, msg);
				SetTimer(TIMER_NOTIFY_DOWNLOAD, TIMER_NOTIFY_DOWNLOAD_ELAP, NULL );
			}
			
			break;

		case 0:

			/* Error en la descarga
			 */

			BOOL hidden = m_notify->Hidden();
			m_notify->Hide();
			CErrDlg dlgErr(this);
			dlgErr.SetDescription(IDS_UPDATE_ERR);
			dlgErr.SetErrDescription(IDS_UPDATE_ERR_DOWNLOADING);
			dlgErr.DoModal();
			if ( ! hidden )
				m_notify->Show();

			m_st = ST_INI;

			break;
	}

	return 0;
}

void CClUpdateDlg::OnShowWindow(BOOL bShow, UINT nStatus)
{
	CDialog::OnShowWindow(bShow, nStatus);

	if ( bShow ) {
		m_notify->Hide();
		//if ( m_st == ST_DOWNLOADED )
		//KillTimer(TIMER_NOTIFY_DOWNLOAD);
	} else {
		m_notify->Show();
	}
}

void CClUpdateDlg::OnClose()
{
	CString msg;

	if ( m_st == ST_UPDATE ) {

		/* Muestro s�lo la pantalla de recuerdo si ya ha expirado el
		 * tiempo de notificaci�n
		 */

		CClupdateReg reg;
		BOOL bShow = TRUE;


		if ( !reg.Open() )
			bShow = TRUE;
		else {
			if ( time(NULL) >= m_tNextNotify )
				bShow = TRUE;
			else if ( (m_tNextNotify - time(NULL)) > (24*60*60) )
				bShow = TRUE;
			else
				bShow = FALSE;

		}

		if ( bShow ) {
			m_st = ST_UPDATEREMIND;
			m_dlgDescargar->ShowWindow(SW_HIDE);
			m_dlgRecordar->ShowWindow(SW_SHOW);
			CWnd *btn = GetDlgItem(IDC_BUTTON_INICIAR);
			//msg.LoadString(IDS_UPDATE_ACEPTAR);
			msg = GetClauerString(IDS_UPDATE_ACEPTAR);
			btn->SetWindowText(msg);
			UpdateData(FALSE);
		} else {
			ShowWindow(SW_HIDE);
			m_notify->Show();
		}
	} else if ( m_st == ST_UPDATEREMIND ) {

		BOOL bIsElevated = FALSE;
		BOOL bIsAdmo;
		if ( ! CLUPDATE_IsInAdministratorGroup(bIsAdmo) )
			bIsAdmo = FALSE;
		if ( ! CLUPDATE_IsElevated(bIsElevated) )
			bIsElevated = FALSE;

		if ( bIsAdmo && bIsElevated ) 
			m_st = ST_UPDATE;

		m_dlgRecordar->ShowWindow(SW_HIDE);
		m_dlgDescargar->ShowWindow(SW_SHOW);
		ShowWindow(SW_HIDE);
		m_notify->Show();
	} else {
		ShowWindow(SW_HIDE);
		m_notify->Show();
	}

	//CDialog::OnClose();
}

void CClUpdateDlg::OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags)
{
	CDialog::OnKeyDown(nChar, nRepCnt, nFlags);
}




BOOL CClUpdateDlg::PreTranslateMessage(MSG* pMsg )
{
	if ( pMsg->message == WM_KEYDOWN ) {
		
		switch ( pMsg->wParam ) {
			case VK_ESCAPE:
				OnClose();
				return TRUE;

			case VK_TAB:
				if ( m_st == ST_UPDATEREMIND ) 
					m_dlgRecordar->SetFocus();
				return TRUE;

		}
	}

	return CDialog::PreTranslateMessage(pMsg);
}

void CClUpdateDlg::OnDestroy()
{
	CDialog::OnDestroy();
}


void CClUpdateDlg::OnOpcionesCerrar()
{
	CString aux;
	LPTSTR s;

	s = GetClauerString(IDS_UPDATE_CLOSE_CLUPDATE);

	if ( ! s ) {
		switch ( AfxMessageBox(GetClauerString(IDS_UPDATE_CLOSE_CLUPDATE), MB_YESNO,0) ) {
		case IDYES:
			m_notify->Hide();
			PostQuitMessage(0);
			break;
		}
	} else {
		aux = s;
		switch ( AfxMessageBox(aux.GetString(), MB_YESNO,0) ) {
		case IDYES:
			m_notify->Hide();
			PostQuitMessage(0);
			break;
		}
	}
}





void CClUpdateDlg::OnIraAyuda()
{
	::ShellExecute(m_hWnd, _T("open"), URL_HELP, NULL, NULL, SW_SHOWNORMAL);
}




void CClUpdateDlg::OnIraIra1()
{
#ifdef CLUPDATE_PIPELINE
	::ShellExecute(m_hWnd, _T("open"), _T("http://www.aavv.com/common/pi/gestion_usuarios/clauer/ayuda.html"), NULL, NULL, SW_SHOWNORMAL);
#elif CLUPDATE_CATCERT
	::ShellExecute(m_hWnd, _T("open"), _T("http://www.idcat.net/"), NULL, NULL, SW_SHOWNORMAL);
#else
	::ShellExecute(m_hWnd, _T("open"), _T("http://clauer.uji.es/es/tutWin.html"), NULL, NULL, SW_SHOWNORMAL);
#endif
}




void CClUpdateDlg::OnIraIra2()
{
#ifdef CLUPDATE_PIPELINE
	::ShellExecute(m_hWnd, _T("open"), _T("http://www.pipeline.es"), NULL, NULL, SW_SHOWNORMAL);
#elif CLUPDATE_CATCERT
	::ShellExecute(m_hWnd, _T("open"), _T("http://www.catcert.net"), NULL, NULL, SW_SHOWNORMAL);
#else
	::ShellExecute(m_hWnd, _T("open"), _T("http://clauer.uji.es"), NULL, NULL, SW_SHOWNORMAL);
#endif
}




void CClUpdateDlg::OnIraIra3()
{
	::ShellExecute(m_hWnd, _T("open"), _T("http://clauer.nisu.org"), NULL, NULL, SW_SHOWNORMAL);
}



void CClUpdateDlg::OnModoExperto()
{
	CClupdateReg reg;
	reg.Open();
	reg.SetUpdateMode(UPDMODE_EXPERT);
	reg.Close();
}

void CClUpdateDlg::OnModoNormal()
{
	CClupdateReg reg;
	reg.Open();
	reg.SetUpdateMode(UPDMODE_NORMAL);
	reg.Close();
}

void CClUpdateDlg::OnModoInicio()
{
	CClupdateReg reg;
	reg.Open();
	reg.SetUpdateMode(UPDMODE_INI);
	reg.Close();
}




