#ifndef __CLUPDATE_UTILS_H__
#define __CLUPDATE_UTILS_H__

#include "stdafx.h"
#include <windows.h>
#include <tchar.h>

BOOL CLUPDATE_IsElevated ( BOOL &bIsElevated );
BOOL CLUPDATE_IsWindowsVista ( BOOL &is );
BOOL CLUPDATE_LaunchElevated ( BOOL &bCancelled );   // Vista only
BOOL CLUPDATE_IsInAdministratorGroup ( BOOL & bIsInAdministrorsGroup );

#endif
