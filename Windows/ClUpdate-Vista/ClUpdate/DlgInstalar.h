#pragma once


// CDlgInstalar dialog

class CDlgInstalar : public CDialog
{
	DECLARE_DYNAMIC(CDlgInstalar)

public:
	CDlgInstalar(CWnd* pParent = NULL);   // standard constructor
	virtual ~CDlgInstalar();

// Dialog Data
	enum { IDD = IDD_DLG_INSTALAR };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	virtual BOOL PreTranslateMessage(MSG* pMsg );

	virtual BOOL OnInitDialog();

	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnStnClickedStaticDescInstalar();
};
