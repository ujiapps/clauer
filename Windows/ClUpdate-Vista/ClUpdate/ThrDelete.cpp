#include "stdafx.h"
#include "ThrDelete.h"

#include <shlwapi.h>



// Cuando se hace una actualizaci�n y se lanza el proceso de instalaci�n
// el fichero setup-clauer no se borra. El motivo es que el instalador se 
// est� ejecutando

UINT CLUPDATE_THR_Delete ( LPVOID pParam )
{
	// Si hab�a alguna actualizaci�n previa en el escritorio la borro

	TCHAR szFileDir[MAX_PATH];
	BOOL bExit = TRUE;

	if ( SHGetFolderPath(NULL, CSIDL_DESKTOPDIRECTORY, 0, SHGFP_TYPE_DEFAULT, szFileDir) != S_FALSE) 
		if (  PathAppend(szFileDir, _T("update-clauer.exe")) ) 
			bExit = FALSE;

	while ( ! bExit ) {
		if ( DeleteFile(szFileDir) ) 
			bExit=TRUE;
		else {
			DWORD err = GetLastError();
			switch ( err ) {
				case ERROR_FILE_NOT_FOUND:
				case ERROR_PATH_NOT_FOUND:
					bExit = TRUE;
					break;
			}
		}

		Sleep(10000);
	}

	return 0;
}
