#pragma once

#include "stdafx.h"
#include <windows.h>


typedef struct {
	CEvent *wait;
	CWnd *wnd;
	HANDLE hToken;	/* An admo access token */
} THRDOWNLOADPARAM, *PTHRDOWNLOADPARAM;


UINT CLUPDATE_THR_Download ( LPVOID pParam );

