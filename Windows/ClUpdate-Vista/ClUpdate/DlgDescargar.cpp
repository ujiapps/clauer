// DlgDescargar.cpp : implementation file
//

#include "stdafx.h"
#include "ClUpdate.h"
#include "DlgDescargar.h"

#include <lang/resource.h>
#include "lang.h"


// CDlgDescargar dialog

IMPLEMENT_DYNAMIC(CDlgDescargar, CDialog)

CDlgDescargar::CDlgDescargar(CWnd* pParent /*=NULL*/)
	: CDialog(CDlgDescargar::IDD, pParent)
{

}

CDlgDescargar::~CDlgDescargar()
{
}


BOOL CDlgDescargar::OnInitDialog()
{
	CDialog::OnInitDialog();

	CString aux;

	CWnd *wnd = GetDlgItem(IDC_STATIC_DESC_PROGRESO);
	if ( wnd ) {
		//aux.LoadString(IDS_UPDATE_DESC_PROGRESO);
		aux = GetClauerString(IDS_UPDATE_DESC_PROGRESO);
		wnd->SetWindowText(aux.GetString());
	}
	wnd = GetDlgItem(IDC_STATIC_PUSH_DOWNLOAD);
	if ( wnd ) {
		//aux.LoadString(IDS_UPDATE_PUSH_DOWNLOAD);
		aux = GetClauerString(IDS_UPDATE_PUSH_DOWNLOAD);
		wnd->SetWindowText(aux.GetString());
	}
	wnd = GetDlgItem(IDC_STATIC_FINISHED_UPDATE_NOTIFY);
	if ( wnd ) {
		//aux.LoadString(IDS_UPDATE_FINISHED_UPDATE_NOTIFY);
		aux = GetClauerString(IDS_UPDATE_FINISHED_UPDATE_NOTIFY);
		wnd->SetWindowText(aux.GetString());
	}
	UpdateData();



	return TRUE;
}


void CDlgDescargar::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(CDlgDescargar, CDialog)
	ON_WM_KEYDOWN()
	ON_STN_CLICKED(IDC_STATIC_DESC_PROGRESO, &CDlgDescargar::OnStnClickedStaticDescProgreso)
END_MESSAGE_MAP()


// CDlgDescargar message handlers

void CDlgDescargar::OnKeyDown(UINT nChar, UINT nRepCnt, UINT nFlags)
{
	// TODO: Add your message handler code here and/or call default

	CDialog::OnKeyDown(nChar, nRepCnt, nFlags);
}


BOOL CDlgDescargar::PreTranslateMessage(MSG* pMsg )
{
	if ( pMsg->message == WM_KEYDOWN ) {
		switch ( pMsg->wParam) {
			case VK_ESCAPE:	
			case VK_RETURN:
				return TRUE;
		}
	}
	return CDialog::PreTranslateMessage(pMsg);
}

void CDlgDescargar::OnStnClickedStaticDescProgreso()
{
	// TODO: Add your control notification handler code here
}
