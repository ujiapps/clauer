#include "stdafx.h"
#include "ThrDownload.h"
#include <windows.h>
#include <winhttp.h>
#include <Sddl.h>

#include "AppMessages.h"
#include "shlwapi.h"

#include "common_defines.h"
#include "ClupdateReg.h"

#define CLUPDATE_DOWNLOAD_BUFFER_SIZE	10240
#define CLUPDATE_MAX_DOWNLOAD_URL       1024   // en WCHAR

BOOL CLUPDATE_DOWNLOAD_Setup ( TCHAR szOutFileName[MAX_PATH+1] );


UINT CLUPDATE_THR_Download ( LPVOID pParam )
{	
	PTHRDOWNLOADPARAM p = ( PTHRDOWNLOADPARAM ) pParam;
	TCHAR *szFileDir, szDesktopDir[MAX_PATH];
	DWORD ret;

	// ATENCI�N
	//
	// szFileDir TIENE que estar reservado en el HEAP ya se env�a un mensaje con
	// un puntero al mismo (muuu cansado pa explicarlo :-) simplemente conserva
	// la reserva de szFileDir as� y no lo definas como un vector est�tico)

	szFileDir = new TCHAR[MAX_PATH];
	if ( ! szFileDir ) {
		PostMessage(p->wnd->m_hWnd, WM_CLUPDATE_DOWNLOAD, 0, NULL);
		return 0;		
	}

	ret = GetTempPath(MAX_PATH, szFileDir);
	if ( ret == 0 || ret > MAX_PATH ) {
		delete [] szFileDir;
		PostMessage(p->wnd->m_hWnd, WM_CLUPDATE_DOWNLOAD, 0, NULL);
		return 0;
	}

	if ( ! PathAppend(szFileDir, _T("update-clauer.exe")) ) {
		delete [] szFileDir;
		PostMessage(p->wnd->m_hWnd, WM_CLUPDATE_DOWNLOAD, 0, NULL);
		return 0;
	}

	/* Realizamos la descarga
	 */

	if ( ! CLUPDATE_DOWNLOAD_Setup(szFileDir) ) {
		delete [] szFileDir;
		PostMessage(p->wnd->m_hWnd, WM_CLUPDATE_DOWNLOAD, 0, NULL);
		return 0;
	}

	/* Una vez acabada copiamos el fichero en el escritorio
	 */

	if ( SHGetFolderPath(NULL, CSIDL_DESKTOPDIRECTORY, 0, SHGFP_TYPE_DEFAULT, szDesktopDir) == S_FALSE) {
		delete [] szFileDir;
		PostMessage(p->wnd->m_hWnd, WM_CLUPDATE_DOWNLOAD, 0, NULL);
		return 0;
	}
	
	if ( ! PathAppend(szDesktopDir, _T("update-clauer.exe")) ) {
		delete [] szFileDir;
		PostMessage(p->wnd->m_hWnd, WM_CLUPDATE_DOWNLOAD, 0, NULL);
		return 0;
	}

	if ( CopyFile(szFileDir, szDesktopDir, FALSE) ) {
		DeleteFile(szFileDir);
		_tcscpy(szFileDir, szDesktopDir);
	}
	
	/* Notificamos que la actualizaci�n se termin�
	 */

	PostMessage(p->wnd->m_hWnd, WM_CLUPDATE_DOWNLOAD, 1, (LPARAM) szFileDir);
	
	return 0;
}


// Liberar downloadUrl a la salida con delete []

BOOL CLUPDATE_Get_Download_URL ( WCHAR wszServer[CLUPDATE_MAX_DOWNLOAD_URL+1],
								 WCHAR wszDownloadUrl[CLUPDATE_MAX_DOWNLOAD_URL+1] )
{
	BOOL ret = TRUE;

	DWORD dwSize = 0, dwDownloaded = 0, dwTotal = 0;
    BYTE *pszOutBuffer = NULL;
    HINTERNET  hSession = NULL, hConnect = NULL, hRequest = NULL;

	DWORD dwAccessType;
	CString proxy, proxy_aux;
	DWORD dwPort;
	LPWSTR lpwszProxy;

	HANDLE hFile = 0;
	DWORD dwBytesWritten, dwBytesToRead;

	if ( ! wszDownloadUrl ) 
		return FALSE;

	// Configuraci�n de conectividad
	
	BOOL bIEProxyConfig = FALSE;
	BOOL bAutoDetect = FALSE;
	WINHTTP_CURRENT_USER_IE_PROXY_CONFIG ieConfig;

	if ( ! CClupdateReg::GetConnectivityType(&dwAccessType) ) {
		dwAccessType = WINHTTP_ACCESS_TYPE_DEFAULT_PROXY;
		lpwszProxy = WINHTTP_NO_PROXY_NAME;
	} else {
		if ( ! CClupdateReg::GetConnectivityProxy(&proxy, &dwPort) ) {
			dwAccessType = WINHTTP_ACCESS_TYPE_DEFAULT_PROXY;
			lpwszProxy = WINHTTP_NO_PROXY_NAME;
		} else {
			if ( dwAccessType == WINHTTP_ACCESS_TYPE_NAMED_PROXY ) {
				CString strPort;
				strPort.Format(_T("%ld"), dwPort);
				proxy_aux= CString(_T("http=http://")) + proxy + _T(":") + strPort;
				lpwszProxy = (LPWSTR) proxy_aux.GetString();
			} else if ( dwAccessType == -1 ) {

				// Tomamos la configuraci�n de Internet Explorer

				bIEProxyConfig = TRUE;

				if ( ! WinHttpGetIEProxyConfigForCurrentUser(&ieConfig) ) {
					ret = FALSE;
					goto finCLUPDATE_Get_Download_URL;
				}
				dwAccessType   = WINHTTP_ACCESS_TYPE_NO_PROXY;
				lpwszProxy     = WINHTTP_NO_PROXY_NAME;
			} else {
				lpwszProxy = NULL;
			}
		}
	}

    hSession = WinHttpOpen( L"Clauer Update/3.0",  
                            dwAccessType,
                            lpwszProxy, 
                            WINHTTP_NO_PROXY_BYPASS, 0);

	if ( ! hSession ) {
		ret = FALSE;
		goto finCLUPDATE_Get_Download_URL;
	}	

	/* Establecemos los diversos timeouts:
	 *
	 *      - Timeout para resolver nombres: 5s
	 *      - Timeout conexi�n al servidor: 2s
	 *		- Timeout para el env�o de la petici�n: 2s
	 *      - Timeout para la respuesta: 2s
	 */

	if ( ! WinHttpSetTimeouts(hSession, 10000, 10000, 10000, 10000) ) {
		ret = FALSE;
		goto finCLUPDATE_Get_Download_URL;
	}

	// Specify an HTTP server.

    hConnect = WinHttpConnect( hSession, CLUPDATE_NOTIFY_SERVER,
                               INTERNET_DEFAULT_HTTP_PORT, 0);

	if ( !hConnect ) {
		ret = FALSE;
		goto finCLUPDATE_Get_Download_URL;
	}

    // Create an HTTP request handle.

    hRequest = WinHttpOpenRequest( hConnect, L"GET", CLUPDATE_NOTIFY_DOWNLOAD_URL_GETTER,
                                   NULL, WINHTTP_NO_REFERER, 
                                   WINHTTP_DEFAULT_ACCEPT_TYPES, 
                                   0);

	if ( !hRequest ) {
		ret = FALSE;
		goto finCLUPDATE_Get_Download_URL;
	}


	// Manejamos los seteos del proxy seg�n la configuraci�n de
	// Internet Explorer

	WINHTTP_AUTOPROXY_OPTIONS  AutoProxyOptions;
    WINHTTP_PROXY_INFO         ProxyInfo;
    DWORD                      cbProxyInfoSize = sizeof(ProxyInfo);
  
    ZeroMemory( &AutoProxyOptions, sizeof(AutoProxyOptions) );
    ZeroMemory( &ProxyInfo, sizeof(ProxyInfo) );

	if ( bIEProxyConfig ) {

		if ( ieConfig.fAutoDetect || ieConfig.lpszAutoConfigUrl ) {
			if ( ieConfig.lpszAutoConfigUrl ) {
			    AutoProxyOptions.dwFlags           = WINHTTP_AUTOPROXY_CONFIG_URL;
			    AutoProxyOptions.lpszAutoConfigUrl = ieConfig.lpszAutoConfigUrl;
			} else { 
				AutoProxyOptions.dwFlags           = WINHTTP_AUTOPROXY_AUTO_DETECT;
				AutoProxyOptions.dwAutoDetectFlags = WINHTTP_AUTO_DETECT_TYPE_DHCP |
													 WINHTTP_AUTO_DETECT_TYPE_DNS_A;
			}

			AutoProxyOptions.fAutoLogonIfChallenged = FALSE;

			// Si devuelve error se ignora: http://msdn2.microsoft.com/en-us/library/aa384122(VS.85).aspx

			CString auxUrl;
			auxUrl = CString(_T("http://")) + CString(CLUPDATE_NOTIFY_SERVER) + CString(_T("/")) + CLUPDATE_NOTIFY_DOWNLOAD_URL_GETTER;
			if( WinHttpGetProxyForUrl( hSession,
									 auxUrl.GetString(),
									 &AutoProxyOptions,
									 &ProxyInfo ) ) 
			{			    
				if( ! WinHttpSetOption( hRequest, 
									   WINHTTP_OPTION_PROXY,
									   &ProxyInfo,
									   cbProxyInfoSize ) )
				{
					ret = FALSE;
					goto finCLUPDATE_Get_Download_URL;
				}					
			}
		} else if ( ieConfig.lpszProxy ) {
			ProxyInfo.dwAccessType    = WINHTTP_ACCESS_TYPE_NAMED_PROXY;
			ProxyInfo.lpszProxy       = ieConfig.lpszProxy;
			ProxyInfo.lpszProxyBypass = ieConfig.lpszProxyBypass;

			if( ! WinHttpSetOption( hRequest, 
								   WINHTTP_OPTION_PROXY,
								   &ProxyInfo,
								   cbProxyInfoSize ) )
			{
				ret = FALSE;
				goto finCLUPDATE_Get_Download_URL;
			}					
		}
	}

    // Send a request.

    if ( ! WinHttpSendRequest( hRequest,
                               WINHTTP_NO_ADDITIONAL_HEADERS, 0,
                               WINHTTP_NO_REQUEST_DATA, 0, 
							   0, 0) )
	{
		ret = FALSE;
		goto finCLUPDATE_Get_Download_URL;
	}
 
    // Obtenemos la respuesta

	if ( ! WinHttpReceiveResponse( hRequest, NULL) ) {
		ret = FALSE;
		goto finCLUPDATE_Get_Download_URL;
	}

	pszOutBuffer = new BYTE [ CLUPDATE_MAX_DOWNLOAD_URL + 1 ];
	if ( ! pszOutBuffer ) {
		ret = FALSE;
		goto finCLUPDATE_Get_Download_URL;
	}
	ZeroMemory(pszOutBuffer,CLUPDATE_MAX_DOWNLOAD_URL+1);

    // Keep checking for data until there is nothing left.

    dwSize = 0;
    if ( ! WinHttpQueryDataAvailable( hRequest, &dwSize) ) {
		ret = FALSE;
		goto finCLUPDATE_Get_Download_URL;
	}

    while ( dwSize > 0 )
    {
        // Read the Data.
		
		dwBytesToRead = (dwSize > CLUPDATE_MAX_DOWNLOAD_URL) ? (CLUPDATE_MAX_DOWNLOAD_URL) : dwSize;
		while ( dwSize > 0 ) {

			if ( ! WinHttpReadData( hRequest, pszOutBuffer, dwBytesToRead, &dwDownloaded)) {
				ret = FALSE;
				goto finCLUPDATE_Get_Download_URL;
			}

			dwSize  -= dwDownloaded;
			dwTotal += dwDownloaded;
		}

		if ( dwTotal >= CLUPDATE_MAX_DOWNLOAD_URL )
			break;
		
		if ( ! WinHttpQueryDataAvailable( hRequest, &dwSize) ) {
			ret = FALSE;
			goto finCLUPDATE_Get_Download_URL;
		}    
    }

	if ( dwTotal > 0 ) {

		// quito el server de la url:
		//    http://server/(mio y solo mio)

		// cojo servidor

		int i = 0;
		BYTE * aux = pszOutBuffer;
		while ( ( i < 2 ) && *aux ) {
			if ( *aux == '/' )
				i++;
			++aux;
		}
		if ( *aux ) {

			BYTE *aux_ini = aux;
			while ( *aux && *aux!='/' ) {
				++aux;
			}
			if ( *aux == '/' ) {
				*aux = 0;
				if ( ! MultiByteToWideChar(CP_ACP, MB_PRECOMPOSED, (LPCSTR) aux_ini, -1, wszServer, CLUPDATE_MAX_DOWNLOAD_URL+1) ) { 
 					ret = FALSE;
					goto finCLUPDATE_Get_Download_URL;
				}
				// Ahora cojemos lo que queda de url
				++aux;
				if ( ! MultiByteToWideChar(CP_ACP, MB_PRECOMPOSED, (LPCSTR) aux, -1, wszDownloadUrl, CLUPDATE_MAX_DOWNLOAD_URL+1) ) { 
 					ret = FALSE;
					goto finCLUPDATE_Get_Download_URL;
				}
			} else {
				ret = FALSE;
				goto finCLUPDATE_Get_Download_URL;
			}
		}

	} else {
		ret = FALSE;
	}

finCLUPDATE_Get_Download_URL:

    if (hRequest) 
		WinHttpCloseHandle(hRequest);

    if (hConnect) 
		WinHttpCloseHandle(hConnect);

    if (hSession) 
		WinHttpCloseHandle(hSession);

	if ( pszOutBuffer )
		delete [] pszOutBuffer;

	return ret;

}




BOOL CLUPDATE_DOWNLOAD_Setup ( TCHAR szOutFileName[MAX_PATH+1] )
{
	BOOL ret = TRUE;

	DWORD dwSize = 0, dwDownloaded = 0, dwTotal = 0;
    BYTE *pszOutBuffer = NULL;
    HINTERNET  hSession = NULL, hConnect = NULL, hRequest = NULL;

	WCHAR url[CLUPDATE_MAX_DOWNLOAD_URL+1];
	WCHAR server[CLUPDATE_MAX_DOWNLOAD_URL+1];
	HANDLE hFile = 0;
	DWORD dwBytesWritten, dwBytesToRead;

	DWORD dwAccessType;
	CString proxy, proxy_aux;
	DWORD dwPort;
	LPWSTR lpwszProxy;


	TCHAR szSecurityDescriptor[] = _T("D:")
		_T("(A;OICI;GA;;;BA)")	  // Administradores control total
		_T("(D;OICI;GA;;;WD)");	  // Resto del mundo nada
	PSECURITY_ATTRIBUTES sa = NULL;

	// Obtenemos la url de descarga

	if ( ! CLUPDATE_Get_Download_URL(server, url) ) {
		ret = FALSE;
		goto finCLUPDATE_DOWNLOAD_Setup;
	}

	// y ahora a descargar
	
	sa = new SECURITY_ATTRIBUTES;
	if ( ! sa ) {
		ret = FALSE;
		goto finCLUPDATE_DOWNLOAD_Setup;
	}

	ZeroMemory(sa, sizeof(SECURITY_ATTRIBUTES));
	sa->nLength = sizeof(SECURITY_ATTRIBUTES);
	sa->bInheritHandle = FALSE;

	if ( ! ConvertStringSecurityDescriptorToSecurityDescriptor(szSecurityDescriptor,
															  SDDL_REVISION_1,
															  &(sa->lpSecurityDescriptor),
															  NULL) )
	{
		ret = FALSE;
		goto finCLUPDATE_DOWNLOAD_Setup;
	}

	hFile = CreateFile(szOutFileName, GENERIC_WRITE, 0, sa, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, 0);
	if ( hFile == INVALID_HANDLE_VALUE ) {
		hFile = 0;
		ret = FALSE;
		goto finCLUPDATE_DOWNLOAD_Setup;
	}

    // Use WinHttpOpen to obtain a session handle.

	// Configuraci�n de conectividad
	
	BOOL bIEProxyConfig = FALSE;
	BOOL bAutoDetect = FALSE;
	WINHTTP_CURRENT_USER_IE_PROXY_CONFIG ieConfig;

	if ( ! CClupdateReg::GetConnectivityType(&dwAccessType) ) {
		dwAccessType = WINHTTP_ACCESS_TYPE_DEFAULT_PROXY;
		lpwszProxy = WINHTTP_NO_PROXY_NAME;
	} else {
		if ( ! CClupdateReg::GetConnectivityProxy(&proxy, &dwPort) ) {
			dwAccessType = WINHTTP_ACCESS_TYPE_DEFAULT_PROXY;
			lpwszProxy = WINHTTP_NO_PROXY_NAME;
		} else {
			if ( dwAccessType == WINHTTP_ACCESS_TYPE_NAMED_PROXY ) {
				CString strPort;
				strPort.Format(_T("%ld"), dwPort);
				proxy_aux= CString(_T("http=http://")) + proxy + _T(":") + strPort;
				lpwszProxy = (LPWSTR) proxy_aux.GetString();
			} else if ( dwAccessType == -1 ) {

				// Tomamos la configuraci�n de Internet Explorer

				bIEProxyConfig = TRUE;

				if ( ! WinHttpGetIEProxyConfigForCurrentUser(&ieConfig) ) {
					ret = FALSE;
					goto finCLUPDATE_DOWNLOAD_Setup;
				}
				dwAccessType   = WINHTTP_ACCESS_TYPE_NO_PROXY;
				lpwszProxy     = WINHTTP_NO_PROXY_NAME;
			} else {
				lpwszProxy = NULL;
			}
		}
	}

    hSession = WinHttpOpen( L"Clauer Update/3.0",  
                            dwAccessType,
                            lpwszProxy, 
                            WINHTTP_NO_PROXY_BYPASS, 0);


/*    hSession = WinHttpOpen( L"Clauer Update/3.0",  
                            WINHTTP_ACCESS_TYPE_DEFAULT_PROXY,
                            WINHTTP_NO_PROXY_NAME, 
                            WINHTTP_NO_PROXY_BYPASS, 0);
*/
	if ( ! hSession ) {
		ret = FALSE;
		goto finCLUPDATE_DOWNLOAD_Setup;
	}	

	/* Establecemos los diversos timeouts:
	 *
	 *      - Timeout para resolver nombres: 5s
	 *      - Timeout conexi�n al servidor: 2s
	 *		- Timeout para el env�o de la petici�n: 2s
	 *      - Timeout para la respuesta: 2s
	 */

	if ( ! WinHttpSetTimeouts(hSession, 10000, 10000, 10000, 10000) ) {
		ret = FALSE;
		goto finCLUPDATE_DOWNLOAD_Setup;
	}

	// Specify an HTTP server.

    hConnect = WinHttpConnect( hSession, server,
                               INTERNET_DEFAULT_HTTP_PORT, 0);

	if ( !hConnect ) {
		ret = FALSE;
		goto finCLUPDATE_DOWNLOAD_Setup;
	}

    // Create an HTTP request handle.

    hRequest = WinHttpOpenRequest( hConnect, L"GET", url,
                                   NULL, WINHTTP_NO_REFERER, 
                                   WINHTTP_DEFAULT_ACCEPT_TYPES, 
                                   0);

	if ( !hRequest ) {
		ret = FALSE;
		goto finCLUPDATE_DOWNLOAD_Setup;
	}

	// Manejamos los seteos del proxy seg�n la configuraci�n de
	// Internet Explorer

	WINHTTP_AUTOPROXY_OPTIONS  AutoProxyOptions;
    WINHTTP_PROXY_INFO         ProxyInfo;
    DWORD                      cbProxyInfoSize = sizeof(ProxyInfo);
  
    ZeroMemory( &AutoProxyOptions, sizeof(AutoProxyOptions) );
    ZeroMemory( &ProxyInfo, sizeof(ProxyInfo) );

	if ( bIEProxyConfig ) {

		if ( ieConfig.fAutoDetect || ieConfig.lpszAutoConfigUrl ) {
			if ( ieConfig.lpszAutoConfigUrl ) {
			    AutoProxyOptions.dwFlags           = WINHTTP_AUTOPROXY_CONFIG_URL;
			    AutoProxyOptions.lpszAutoConfigUrl = ieConfig.lpszAutoConfigUrl;
			} else { 
				AutoProxyOptions.dwFlags           = WINHTTP_AUTOPROXY_AUTO_DETECT;
				AutoProxyOptions.dwAutoDetectFlags = WINHTTP_AUTO_DETECT_TYPE_DHCP |
													 WINHTTP_AUTO_DETECT_TYPE_DNS_A;
			}

			AutoProxyOptions.fAutoLogonIfChallenged = FALSE;

			// Si devuelve error se ignora: http://msdn2.microsoft.com/en-us/library/aa384122(VS.85).aspx

			CString auxUrl;
			auxUrl = CString(_T("http://")) + CString(CLUPDATE_NOTIFY_SERVER) + CString(_T("/")) + url;
			if( WinHttpGetProxyForUrl( hSession,
									 auxUrl.GetString(),
									 &AutoProxyOptions,
									 &ProxyInfo ) ) 
			{			    
				if( ! WinHttpSetOption( hRequest, 
									   WINHTTP_OPTION_PROXY,
									   &ProxyInfo,
									   cbProxyInfoSize ) )
				{
					ret = FALSE;
					goto finCLUPDATE_DOWNLOAD_Setup;
				}					
			}
		} else if ( ieConfig.lpszProxy ) {
			ProxyInfo.dwAccessType    = WINHTTP_ACCESS_TYPE_NAMED_PROXY;
			ProxyInfo.lpszProxy       = ieConfig.lpszProxy;
			ProxyInfo.lpszProxyBypass = ieConfig.lpszProxyBypass;

			if( ! WinHttpSetOption( hRequest, 
								   WINHTTP_OPTION_PROXY,
								   &ProxyInfo,
								   cbProxyInfoSize ) )
			{
				ret = FALSE;
				goto finCLUPDATE_DOWNLOAD_Setup;
			}					
		}
	}


    // Send a request.

    if ( ! WinHttpSendRequest( hRequest,
                               WINHTTP_NO_ADDITIONAL_HEADERS, 0,
                               WINHTTP_NO_REQUEST_DATA, 0, 
							   0, 0) )
	{
		ret = FALSE;
		goto finCLUPDATE_DOWNLOAD_Setup;
	}
 
    // Obtenemos la respuesta

	if ( ! WinHttpReceiveResponse( hRequest, NULL) ) {
		ret = FALSE;
		goto finCLUPDATE_DOWNLOAD_Setup;
	}

	pszOutBuffer = new BYTE [ CLUPDATE_DOWNLOAD_BUFFER_SIZE ];
	if ( ! pszOutBuffer ) {
		ret = FALSE;
		goto finCLUPDATE_DOWNLOAD_Setup;
	}

    // Keep checking for data until there is nothing left.

    dwSize = 0;
    if ( ! WinHttpQueryDataAvailable( hRequest, &dwSize) ) {
		ret = FALSE;
		goto finCLUPDATE_DOWNLOAD_Setup;
	}

    while ( dwSize > 0 )
    {
        // Read the Data.
		
		dwBytesToRead = (dwSize > CLUPDATE_DOWNLOAD_BUFFER_SIZE) ? CLUPDATE_DOWNLOAD_BUFFER_SIZE : dwSize;
		while ( dwSize > 0 ) {

			if ( ! WinHttpReadData( hRequest, pszOutBuffer, dwBytesToRead, &dwDownloaded)) {
				ret = FALSE;
				goto finCLUPDATE_DOWNLOAD_Setup;
			}

			if ( ! WriteFile(hFile, pszOutBuffer, dwDownloaded, &dwBytesWritten, NULL) ) {
				ret = FALSE;
				goto finCLUPDATE_DOWNLOAD_Setup;
			}

			dwSize  -= dwDownloaded;
			dwTotal += dwDownloaded;
		}

		if ( ! WinHttpQueryDataAvailable( hRequest, &dwSize) ) {
			ret = FALSE;
			goto finCLUPDATE_DOWNLOAD_Setup;
		}    
    }


finCLUPDATE_DOWNLOAD_Setup:

	if ( hFile ) {
		CloseHandle(hFile);
		if ( ret == FALSE ) 
			DeleteFile(szOutFileName);
	}

	if ( sa ) {
		if ( sa->lpSecurityDescriptor ) 
			LocalFree(sa->lpSecurityDescriptor);
		delete [] sa;
	}


    if (hRequest) 
		WinHttpCloseHandle(hRequest);

    if (hConnect) 
		WinHttpCloseHandle(hConnect);

    if (hSession) 
		WinHttpCloseHandle(hSession);

	if ( pszOutBuffer )
		delete [] pszOutBuffer;

	return ret;

}








