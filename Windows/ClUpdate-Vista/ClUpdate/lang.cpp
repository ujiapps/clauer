#include "stdafx.h"
#include "lang.h"

#include <windows.h>
#include <tchar.h>


extern HMODULE g_hLangDll;


LPTSTR GetClauerString ( UINT uId )
{
	static TCHAR str[MAX_STRING_SIZE];

	if ( ! LoadString(g_hLangDll, uId, str, MAX_STRING_SIZE) ) 
		_tcscpy(str, _T("Can't get proper string. Reinstall the software!!"));

	return str;
}
