// UpdateProgDlg.cpp : implementation file
//

#include "stdafx.h"
#include "ClUpdate.h"
#include "UpdateProgDlg.h"


// CUpdateProgDlg dialog

IMPLEMENT_DYNAMIC(CUpdateProgDlg, CDialog)

CUpdateProgDlg::CUpdateProgDlg(CWnd* pParent /*=NULL*/, BOOL bCritical )
	: CDialog(CUpdateProgDlg::IDD, pParent)
{
	m_bCritical = bCritical;
}

CUpdateProgDlg::~CUpdateProgDlg()
{
}

void CUpdateProgDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
}


BEGIN_MESSAGE_MAP(CUpdateProgDlg, CDialog)
	ON_BN_CLICKED(IDOK, &CUpdateProgDlg::OnBnClickedOk)
	ON_BN_CLICKED(IDC_BTN_INICIAR, &CUpdateProgDlg::OnBnClickedBtnIniciar)
END_MESSAGE_MAP()


// CUpdateProgDlg message handlers

void CUpdateProgDlg::OnBnClickedOk()
{

	OnOK();
}

void CUpdateProgDlg::OnBnClickedBtnIniciar()
{

	



}
