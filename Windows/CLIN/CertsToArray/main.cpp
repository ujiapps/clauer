#include <windows.h>

#include <iostream>
#include <string>
#include <vector>

#include <stdio.h>


using namespace std;



void main (void)
{
	FILE *fIn, *fOut;
	HANDLE hFind;
	WIN32_FIND_DATA fData;

	BYTE *cert;
	DWORD tam, numCerts = 0;
	string aux;

	vector<int> tamCAs;


	fOut = fopen(".\\CAs.h", "w");

	if ( !fOut ) {
		fprintf(stderr, "ERROR abriendo fichero de salida\n");
		return;
	}

	fprintf(fOut, "#ifndef __CAS_H__\n");
	fprintf(fOut, "#define __CAS_H__\n");
	fprintf(fOut, "#include <windows.h>\n\n");

	hFind = FindFirstFile(".\\certs\\*.cer", &fData);
	if ( hFind == INVALID_HANDLE_VALUE ) {
		fprintf(stderr, "FALLO\n");
		return;
	}
	
	while (1) {

		++numCerts;

		fIn = fopen((string(".\\certs\\")+string(fData.cFileName)).c_str(), "rb");
		if ( !fIn ) {
			fprintf(stderr, "ERROR");
			return;
		}

		fseek(fIn, 0, SEEK_END);
		tam = ftell(fIn);
		fseek(fIn, 0, SEEK_SET);

		cert = new BYTE[tam];

		fread(cert, 1, tam, fIn);
		fclose(fIn);

		tamCAs.push_back(tam);

		/* Escribo el certificado */

		fprintf(fOut, "BYTE CA%d[%ld] = {\n", numCerts, tam);
		fprintf(fOut, "  0x%02x", cert[0]);
		for ( int i = 1 ; i < tam ; i++ ) {
			
			if ( (i % 10) != 0 )
				fprintf(fOut, ", 0x%02x", cert[i]);
			else
				fprintf(fOut, "\n, 0x%02x", cert[i]);
		}
		fprintf(fOut, "\n};\n");

		delete [] cert;

		if  (!FindNextFile(hFind, &fData)) {
			break;
		}
	} 
	
	FindClose(hFind);

	if ( numCerts > 0 ) {
		fprintf(fOut, "BYTE *gCAs[%ld] = {", numCerts);
		fprintf(fOut, "CA1");

		for ( int i = 1 ; i < numCerts ; i++ ) {	
			fprintf(fOut, ", CA%d", i+1);
		}
		fprintf(fOut, "};\n");

		fprintf(fOut, "int gTamCAs[%ld] = {", numCerts);
		fprintf(fOut, "%d", tamCAs[0]);
		for ( i = 1 ; i < numCerts ; i++ ) {
			fprintf(fOut, ", %d", tamCAs[i]);
		}
		fprintf(fOut, "};\n");
	}


	fprintf(fOut, "int gNumCAs = %ld;\n", numCerts);

	fprintf(fOut, "#endif");
	fclose(fOut);

}
