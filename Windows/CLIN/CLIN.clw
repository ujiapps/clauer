; CLW file contains information for the MFC ClassWizard

[General Info]
Version=1
LastClass=CDlgPaso2
LastTemplate=CDialog
NewFileInclude1=#include "stdafx.h"
NewFileInclude2=#include "CLIN.h"

ClassCount=11
Class1=CCLINApp
Class2=CCLINDlg

ResourceCount=9
Resource2=IDD_DIALOG_PASO_4
Resource3=IDD_DIALOG_PASO_2_5
Resource4=IDD_DIALOG_PASO_3
Resource5=IDD_CLIN_DIALOG
Resource6=IDD_DIALOG_PASO_2
Resource1=IDR_MAINFRAME
Class3=CDlgPaso1
Class4=CDlgPaso2
Class5=CDlgPaso3
Class6=CDlgPaso4
Class7=CDlgPaso5
Class8=CThrPaso4
Resource7=IDD_DIALOG_PASO_5
Class9=CDlgPaso2_5
Resource8=IDD_DIALOG_PASO_1
Class10=CDlgPaso1_5
Class11=CDlgExplorer
Resource9=IDD_DIALOG_PASO_1_5

[CLS:CCLINApp]
Type=0
HeaderFile=CLIN.h
ImplementationFile=CLIN.cpp
Filter=N

[CLS:CCLINDlg]
Type=0
HeaderFile=CLINDlg.h
ImplementationFile=CLINDlg.cpp
Filter=D
LastObject=IDC_FRAME
BaseClass=CDialog
VirtualFilter=dWC



[DLG:IDD_CLIN_DIALOG]
Type=1
Class=CCLINDlg
ControlCount=5
Control1=IDC_FRAME,static,1342242824
Control2=IDC_BTNSIGUIENTE,button,1342242817
Control3=IDC_BTN_ANTERIOR,button,1476460544
Control4=IDC_BTN_SALIR,button,1342242816
Control5=IDC_IMAGENES,static,1342177294

[DLG:IDD_DIALOG_PASO_1]
Type=1
Class=CDlgPaso1
ControlCount=4
Control1=IDC_EDIT1,edit,1350631560
Control2=IDC_STATIC,static,1342308352
Control3=IDC_STATIC,static,1342308352
Control4=IDC_CHECK_OMITIR,button,1342242819

[DLG:IDD_DIALOG_PASO_2]
Type=1
Class=CDlgPaso2
ControlCount=5
Control1=IDC_EDIT_PIN,edit,1342242976
Control2=IDC_LBL_DESCRIPCION,static,1342308352
Control3=IDC_LBL_PIN,static,1342308352
Control4=IDC_STATIC,button,1342177287
Control5=IDC_CHECK_OMITIR_DNIS,button,1342242819

[DLG:IDD_DIALOG_PASO_3]
Type=1
Class=CDlgPaso3
ControlCount=6
Control1=IDC_EDIT_PIN,edit,1342242976
Control2=IDC_EDIT_CONFIRMACION,edit,1342242976
Control3=IDC_LBL_DESCRIPCION,static,1342308352
Control4=IDC_STATIC,static,1342308352
Control5=IDC_STATIC,static,1342308352
Control6=IDC_STATIC,button,1342177287

[DLG:IDD_DIALOG_PASO_4]
Type=1
Class=CDlgPaso4
ControlCount=4
Control1=IDC_LBL_DESCRIPCION_1,static,1342308352
Control2=IDC_STATIC,static,1342308352
Control3=IDC_LBL_DESCRIPCION,static,1342308352
Control4=IDC_PROGRESO,msctls_progress32,1342177281

[DLG:IDD_DIALOG_PASO_5]
Type=1
Class=CDlgPaso5
ControlCount=4
Control1=IDC_STATIC,static,1342308352
Control2=IDC_STATIC,static,1342308352
Control3=IDC_STATIC,static,1342308352
Control4=IDC_STATIC,static,1342308352

[CLS:CDlgPaso1]
Type=0
HeaderFile=DlgPaso1.h
ImplementationFile=DlgPaso1.cpp
BaseClass=CDialog
Filter=D
LastObject=CDlgPaso1
VirtualFilter=dWC

[CLS:CDlgPaso2]
Type=0
HeaderFile=DlgPaso2.h
ImplementationFile=DlgPaso2.cpp
BaseClass=CDialog
Filter=D
LastObject=CDlgPaso2
VirtualFilter=dWC

[CLS:CDlgPaso3]
Type=0
HeaderFile=DlgPaso3.h
ImplementationFile=DlgPaso3.cpp
BaseClass=CDialog
Filter=D
LastObject=CDlgPaso3
VirtualFilter=dWC

[CLS:CDlgPaso4]
Type=0
HeaderFile=DlgPaso4.h
ImplementationFile=DlgPaso4.cpp
BaseClass=CDialog
Filter=D
LastObject=CDlgPaso4
VirtualFilter=dWC

[CLS:CDlgPaso5]
Type=0
HeaderFile=DlgPaso5.h
ImplementationFile=DlgPaso5.cpp
BaseClass=CDialog
Filter=D
LastObject=CDlgPaso5
VirtualFilter=dWC

[CLS:CThrPaso4]
Type=0
HeaderFile=ThrPaso4.h
ImplementationFile=ThrPaso4.cpp
BaseClass=CWinThread
Filter=N

[DLG:IDD_DIALOG_PASO_2_5]
Type=1
Class=CDlgPaso2_5
ControlCount=1
Control1=IDC_LBL_DESCRIPCION,static,1342308352

[CLS:CDlgPaso2_5]
Type=0
HeaderFile=DlgPaso2_5.h
ImplementationFile=DlgPaso2_5.cpp
BaseClass=CDialog
Filter=D
VirtualFilter=dWC
LastObject=CDlgPaso2_5

[DLG:IDD_DIALOG_PASO_1_5]
Type=1
Class=CDlgPaso1_5
ControlCount=6
Control1=IDC_STATIC,static,1342308352
Control2=IDC_STATIC,button,1342177287
Control3=IDC_STATIC,static,1342308352
Control4=IDC_STATIC,static,1342308352
Control5=IDC_EDIT_EMAIL,edit,1342179456
Control6=IDC_EDIT_DNI,edit,1342179456

[CLS:CDlgPaso1_5]
Type=0
HeaderFile=DlgPaso1_5.h
ImplementationFile=DlgPaso1_5.cpp
BaseClass=CDialog
Filter=D
LastObject=IDC_STATIC_DNI
VirtualFilter=dWC

[CLS:CDlgExplorer]
Type=0
HeaderFile=DlgExplorer.h
ImplementationFile=DlgExplorer.cpp
BaseClass=CDialog
Filter=D
VirtualFilter=dWC
LastObject=IDC_EXPLORER1

