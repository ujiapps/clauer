// DlgPaso4.cpp : implementation file
//

#include "stdafx.h"
#include "CLIN.h"
#include "DlgPaso4.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CDlgPaso4 dialog


CDlgPaso4::CDlgPaso4(CWnd* pParent /*=NULL*/)
	: CDialog(CDlgPaso4::IDD, pParent)
{
	//{{AFX_DATA_INIT(CDlgPaso4)
	//}}AFX_DATA_INIT
}


void CDlgPaso4::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CDlgPaso4)
	DDX_Control(pDX, IDC_LBL_DESCRIPCION_1, m_lblDescripcion_1);
	DDX_Control(pDX, IDC_LBL_DESCRIPCION, m_lblDescripcion);
	DDX_Control(pDX, IDC_PROGRESO, m_prgProgreso);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CDlgPaso4, CDialog)
	//{{AFX_MSG_MAP(CDlgPaso4)
	ON_WM_SHOWWINDOW()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CDlgPaso4 message handlers

BOOL CDlgPaso4::PreTranslateMessage(MSG* pMsg) 
{
	// TODO: Add your specialized code here and/or call the base class

	if(pMsg->message==WM_KEYDOWN)
	{
		if ( (pMsg->wParam==VK_ESCAPE) || (pMsg->wParam==VK_RETURN))
			pMsg->wParam=NULL ;
	}

	return CDialog::PreTranslateMessage(pMsg);
}

void CDlgPaso4::SetDescripcion(CString descripcion)
{
	//UpdateData(TRUE);
	m_lblDescripcion.SetWindowText(descripcion);
}


/*! \brief Establece el paso de la barra de progreso
 *
 * Establece el paso de la barra de progreso.
 */

void CDlgPaso4::SetPaso(int paso)
{
	//UpdateData(TRUE);
	m_prgProgreso.SetStep(paso);
}



void CDlgPaso4::Paso()
{
//	UpdateData(TRUE);
	m_prgProgreso.StepIt();
}



void CDlgPaso4::Reset()
{
	UpdateData(TRUE);
	m_prgProgreso.SetPos(0);
}

void CDlgPaso4::OnShowWindow(BOOL bShow, UINT nStatus) 
{
	CDialog::OnShowWindow(bShow, nStatus);
	
	// TODO: Add your message handler code here

	this->Reset();
	this->SetDescripcion("Inicializando...");
	
}

BOOL CDlgPaso4::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here

	CFont *font = new CFont;
	font->CreatePointFont(12, "System");
	m_lblDescripcion_1.SetFont(font);
	delete font;

	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}
