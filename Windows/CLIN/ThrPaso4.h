#if !defined(AFX_THRPASO4_H__49473790_0A05_41A3_9D62_69E95B14FF0D__INCLUDED_)
#define AFX_THRPASO4_H__49473790_0A05_41A3_9D62_69E95B14FF0D__INCLUDED_

#include "DlgPaso4.h"	// Added by ClassView
#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// ThrPaso4.h : header file
//



/////////////////////////////////////////////////////////////////////////////
// CThrPaso4 thread

class CThrPaso4 : public CWinThread
{
	DECLARE_DYNCREATE(CThrPaso4)
protected:


// Attributes
public:

// Operations
public:
	void SetComprobacion(BOOL omitir);
	void SetDNI(CString dni);
	void SetPINClauer(CString pin);
	void SetPINDisquete(CString pin);
	CString GetMsgError();
	BOOL GetError();
	BOOL Terminado();
	CThrPaso4();           // protected constructor used by dynamic creation
	virtual ~CThrPaso4();
	int Run();
	void SetDlg(CDlgPaso4 *dlgPaso4);

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CThrPaso4)
	public:
	virtual BOOL InitInstance();
	virtual int ExitInstance();
	//}}AFX_VIRTUAL

// Implementation
protected:
	CString m_strDNI;
	CString m_strPINClauer;
	CString m_strPINDisquete;

	CString m_strErrMsg;
	BOOL m_bError;
	BOOL m_bFinished;
	BOOL m_bOmitirBaseDatos;
	CDlgPaso4 *m_dlgPaso4;

	// Generated message map functions
	//{{AFX_MSG(CThrPaso4)
		// NOTE - the ClassWizard will add and remove member functions here.
	//}}AFX_MSG

	DECLARE_MESSAGE_MAP()
};

/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_THRPASO4_H__49473790_0A05_41A3_9D62_69E95B14FF0D__INCLUDED_)
