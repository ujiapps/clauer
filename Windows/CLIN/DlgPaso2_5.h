#if !defined(AFX_DLGPASO2_5_H__A6328C8F_6B61_4FC3_92F0_87E643B667A3__INCLUDED_)
#define AFX_DLGPASO2_5_H__A6328C8F_6B61_4FC3_92F0_87E643B667A3__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// DlgPaso2_5.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CDlgPaso2_5 dialog

class CDlgPaso2_5 : public CDialog
{
// Construction
public:
	CDlgPaso2_5(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CDlgPaso2_5)
	enum { IDD = IDD_DIALOG_PASO_2_5 };
	CStatic	m_lblDescripcion;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CDlgPaso2_5)
	public:
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CDlgPaso2_5)
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DLGPASO2_5_H__A6328C8F_6B61_4FC3_92F0_87E643B667A3__INCLUDED_)
