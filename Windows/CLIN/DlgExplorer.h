//{{AFX_INCLUDES()
#include "webbrowser2.h"
//}}AFX_INCLUDES
#if !defined(AFX_DLGEXPLORER_H__9360F732_9CFF_450A_B143_6673E301BC14__INCLUDED_)
#define AFX_DLGEXPLORER_H__9360F732_9CFF_450A_B143_6673E301BC14__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// DlgExplorer.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CDlgExplorer dialog

class CDlgExplorer : public CDialog
{
// Construction
public:
	CDlgExplorer(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CDlgExplorer)
	enum { IDD = IDD_DIALOG_IE };
	CWebBrowser2	m_IE;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CDlgExplorer)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CDlgExplorer)
		// NOTE: the ClassWizard will add member functions here
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DLGEXPLORER_H__9360F732_9CFF_450A_B143_6673E301BC14__INCLUDED_)
