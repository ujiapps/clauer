// DlgPaso3.cpp : implementation file
//

#include "stdafx.h"
#include "CLIN.h"
#include "DlgPaso3.h"
#include "CLINDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CDlgPaso3 dialog


CDlgPaso3::CDlgPaso3(CWnd* pParent /*=NULL*/)
	: CDialog(CDlgPaso3::IDD, pParent)
{
	//{{AFX_DATA_INIT(CDlgPaso3)
	m_editConfirmacion = _T("");
	m_editPIN = _T("");
	//}}AFX_DATA_INIT
}


void CDlgPaso3::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CDlgPaso3)
	DDX_Control(pDX, IDC_LBL_DESCRIPCION, m_lblDescripcion);
	DDX_Text(pDX, IDC_EDIT_CONFIRMACION, m_editConfirmacion);
	DDX_Text(pDX, IDC_EDIT_PIN, m_editPIN);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CDlgPaso3, CDialog)
	//{{AFX_MSG_MAP(CDlgPaso3)
	ON_WM_SHOWWINDOW()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CDlgPaso3 message handlers

BOOL CDlgPaso3::PreTranslateMessage(MSG* pMsg) 
{

	// TODO: Add your specialized code here and/or call the base class
	if(pMsg->message==WM_KEYDOWN)
	{
		if (pMsg->wParam==VK_ESCAPE)
			pMsg->wParam=NULL ;
		else if (pMsg->wParam == VK_RETURN ) {
			((CCLINDlg *) this->GetParent())->OnBtnsiguiente();
			pMsg->wParam = NULL;
			return TRUE;
		}
	}

	return CDialog::PreTranslateMessage(pMsg);
}

CString CDlgPaso3::GetPIN()
{
	UpdateData(TRUE);
	return m_editPIN;
}

CString CDlgPaso3::GetConfirmacion()
{
	UpdateData(TRUE);
	return m_editConfirmacion;
}

void CDlgPaso3::OnShowWindow(BOOL bShow, UINT nStatus) 
{
	CDialog::OnShowWindow(bShow, nStatus);
	
	// TODO: Add your message handler code here

	if ( nStatus == SW_HIDE ) {
		m_editPIN.Empty();
		m_editConfirmacion.Empty();
	}
	
}

BOOL CDlgPaso3::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here

	CFont *font = new CFont;
	font->CreatePointFont(12, "System");
	m_lblDescripcion.SetFont(font);
	delete font;

	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}
