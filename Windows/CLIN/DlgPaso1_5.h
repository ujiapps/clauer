#if !defined(AFX_DLGPASO1_5_H__249CD15E_68FF_4EBC_A5C1_14598B788ECB__INCLUDED_)
#define AFX_DLGPASO1_5_H__249CD15E_68FF_4EBC_A5C1_14598B788ECB__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// DlgPaso1_5.h : header file
//

#include <string>

using namespace std;

/////////////////////////////////////////////////////////////////////////////
// CDlgPaso1_5 dialog

class CDlgPaso1_5 : public CDialog
{
// Construction
public:
	void SetDNI (string DNI);
	void SetEmail (string email);
	void SetColectiu (string colectiu);
	CDlgPaso1_5(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CDlgPaso1_5)
	enum { IDD = IDD_DIALOG_PASO_1_5 };
	CString	m_editMail;
	CString	m_editDNI;
	CString m_editColectiu;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CDlgPaso1_5)
	public:
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CDlgPaso1_5)
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
public:
	afx_msg void OnEnChangeEditDni();
	afx_msg void OnEnChangeEditEmail();
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DLGPASO1_5_H__249CD15E_68FF_4EBC_A5C1_14598B788ECB__INCLUDED_)
