// DlgPaso1.cpp : implementation file
//

#include "stdafx.h"
#include "CLIN.h"
#include "DlgPaso1.h"
#include "CLINDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CDlgPaso1 dialog


CDlgPaso1::CDlgPaso1(CWnd* pParent /*=NULL*/)
	: CDialog(CDlgPaso1::IDD, pParent)
{
	//{{AFX_DATA_INIT(CDlgPaso1)
	m_editDNI = _T("");
	m_chkOmitir = FALSE;
	//}}AFX_DATA_INIT
}


void CDlgPaso1::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CDlgPaso1)
	DDX_Text(pDX, IDC_EDIT1, m_editDNI);
	DDX_Check(pDX, IDC_CHECK_OMITIR, m_chkOmitir);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CDlgPaso1, CDialog)
	//{{AFX_MSG_MAP(CDlgPaso1)
	ON_WM_SHOWWINDOW()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CDlgPaso1 message handlers


BOOL CDlgPaso1::PreTranslateMessage(MSG* pMsg) 
{
	// TODO: Add your specialized code here and/or call the base class

	if(pMsg->message==WM_KEYDOWN)
	{

		switch (pMsg->wParam) {

		case VK_ESCAPE:
			pMsg->wParam=NULL ;
			break;

		case VK_RETURN:

			((CCLINDlg *) this->GetParent())->OnBtnsiguiente();
			pMsg->wParam = NULL;
			return TRUE;

			break;

		case VK_TAB:
			((CCLINDlg *) this->GetParent())->SetFocus();
			pMsg->wParam=NULL;
			break;

		}
	}

	return CDialog::PreTranslateMessage(pMsg);


}

BOOL CDlgPaso1::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here

	this->m_editDNI = "";
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

CString CDlgPaso1::GetDNI()
{
	UpdateData(TRUE);
	return m_editDNI;

}



void CDlgPaso1::OnShowWindow(BOOL bShow, UINT nStatus) 
{
	CDialog::OnShowWindow(bShow, nStatus);
	
	// TODO: Add your message handler code here	

	this->m_editDNI = "";
	this->m_chkOmitir = FALSE;
	UpdateData(FALSE);

}

BOOL CDlgPaso1::OmitirBaseDatos()
{
	UpdateData(TRUE);
	return m_chkOmitir;
}
