#if !defined(AFX_DLGPASO5_H__1269F2CB_0BBD_42DF_A373_DFD26972B8BC__INCLUDED_)
#define AFX_DLGPASO5_H__1269F2CB_0BBD_42DF_A373_DFD26972B8BC__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// DlgPaso5.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CDlgPaso5 dialog

class CDlgPaso5 : public CDialog
{
// Construction
public:
	CDlgPaso5(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CDlgPaso5)
	enum { IDD = IDD_DIALOG_PASO_5 };
		// NOTE: the ClassWizard will add data members here
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CDlgPaso5)
	public:
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CDlgPaso5)
		// NOTE: the ClassWizard will add member functions here
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DLGPASO5_H__1269F2CB_0BBD_42DF_A373_DFD26972B8BC__INCLUDED_)
