#include "stdafx.h"
#include "Excepciones.h"


EExcepcion::EExcepcion (string &msg)
{
	this->msg = msg;
}

string EExcepcion::GetMSG (void)
{
	return msg;
}


void EExcepcion::PushFunc (string f)
{
	this->sPila.push(f);
}

void EExcepcion::PopFunc (void)
{
	this->sPila.pop();
}


string EExcepcion::TopFunc (void)
{
	return this->sPila.top();
}
