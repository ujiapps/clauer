// DlgPaso2_5.cpp : implementation file
//

#include "stdafx.h"
#include "CLIN.h"
#include "CLINDlg.h"
#include "DlgPaso2_5.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CDlgPaso2_5 dialog


CDlgPaso2_5::CDlgPaso2_5(CWnd* pParent /*=NULL*/)
	: CDialog(CDlgPaso2_5::IDD, pParent)
{
	//{{AFX_DATA_INIT(CDlgPaso2_5)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}


void CDlgPaso2_5::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CDlgPaso2_5)
	DDX_Control(pDX, IDC_LBL_DESCRIPCION, m_lblDescripcion);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CDlgPaso2_5, CDialog)
	//{{AFX_MSG_MAP(CDlgPaso2_5)
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CDlgPaso2_5 message handlers

BOOL CDlgPaso2_5::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here

	CFont *font = new CFont;
	font->CreatePointFont(12, "System");
	m_lblDescripcion.SetFont(font);
	delete font;
	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}

BOOL CDlgPaso2_5::PreTranslateMessage(MSG* pMsg) 
{
	// TODO: Add your specialized code here and/or call the base class

	if(pMsg->message==WM_KEYDOWN)
	{
		if (pMsg->wParam==VK_ESCAPE)
			pMsg->wParam=NULL ;
		else if (pMsg->wParam == VK_RETURN ) {
			((CCLINDlg *) this->GetParent())->OnBtnsiguiente();
			pMsg->wParam = NULL;
			return TRUE;
		}
	}
	
	return CDialog::PreTranslateMessage(pMsg);
}
