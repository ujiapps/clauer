#ifndef __EXCEPCIONES_H__
#define __EXCEPCIONES_H__

#include "stdafx.h"
#include <iostream>
#include <string>
#include <stack>


using namespace std;


class EExcepcion {

protected:
	string msg;
	stack<string> sPila;

public:
	EExcepcion (void) {};
	EExcepcion (string &msg);

	string GetMSG (void);

	void PushFunc  (string f);
	void PopFunc   (void);
	string TopFunc (void);
};



class ENoPKCS12 : public EExcepcion { 
public:
	ENoPKCS12 (string &msg) { this->msg = msg; }
};

class EErrorES : public EExcepcion { 
public:
	EErrorES (string &msg) { this->msg = msg; }
};

class EPwdIncorrecta : public EExcepcion { 
public:
	EPwdIncorrecta (string &msg) { this->msg = msg; }
};


class EDNIIncorrecto : public EExcepcion {
public:
	EDNIIncorrecto (string &msg) { this->msg = msg; }
};


#endif
