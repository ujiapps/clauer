// ThrPaso4.cpp : implementation file
//

#include "stdafx.h"
#include "CLIN.h"
#include "ThrPaso4.h"
#include "CLINDlg.h"
#include "CertsToArray/CAs.h"

#include <USBLowLevel/usb_lowlevel.h>
#include <libFormat/usb_format.h>
#include <LIBIMPORT/LIBIMPORT.h>
#include <LIBRT/LIBRT.h>
#include <BaseDatos/BaseDatos.hpp>
#include <CRYPTOWrapper/CRYPTOWrap.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CThrPaso4

IMPLEMENT_DYNCREATE(CThrPaso4, CWinThread)

CThrPaso4::CThrPaso4()
{
	this->m_strPINClauer = "";
	this->m_strPINDisquete = "";
	this->m_bAutoDelete = FALSE;
	m_bError = FALSE;
}

CThrPaso4::~CThrPaso4()
{
}

BOOL CThrPaso4::InitInstance()
{
	// TODO:  perform and per-thread initialization here

	m_bFinished = FALSE;
	m_bError = FALSE;

	return TRUE;
}

int CThrPaso4::ExitInstance()
{
	// TODO:  perform any per-thread cleanup here
	return CWinThread::ExitInstance();
}

BEGIN_MESSAGE_MAP(CThrPaso4, CWinThread)
	//{{AFX_MSG_MAP(CThrPaso4)
		// NOTE - the ClassWizard will add and remove mapping macros here.
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CThrPaso4 message handlers


void CThrPaso4::SetDlg(CDlgPaso4 *dlgPaso4)
{
	this->m_dlgPaso4 = dlgPaso4;
}






int CThrPaso4::Run()
{
	int result, retval = 0;
	int numDispositivos;
	char ** unidades = NULL;
	int *dispositivos = NULL;
	HANDLE_DISPOSITIVO handle;
	CString msg;
	USBCERTS_HANDLE hClauer;
	BOOL dispositivoIniciado = FALSE;
	int esRoot;

	try {

		m_bError = FALSE;
		m_dlgPaso4->SetPaso(100/5);
		m_dlgPaso4->SetDescripcion("Inicializando dispositivo");

		/* Formateamos
		 */

		while ( 1 ) {

			result = lowlevel_enumerar_dispositivos( NULL, NULL, &numDispositivos );

			if ( result != ERR_LOWLEVEL_NO ) {
				m_bError = TRUE;
				m_strErrMsg = "Imposible realizar la inicializaci�n del dispositivo.\n";

				retval = 1;
				goto finRun;
			}

			if ( numDispositivos > 1 ) {
				MessageBox(AfxGetApp()->GetMainWnd()->m_hWnd, 
					"El programa ha detectado m�s de un Stick USB en el sistema.\nPor favor, deje insertado s�lo el que quiera inicializar\ny pulse el bot�n Aceptar",
					"CLIN :: S�lo un Stick",
					MB_ICONEXCLAMATION);
			}
			else if ( numDispositivos == 0 ) {
				MessageBox(AfxGetApp()->GetMainWnd()->m_hWnd, 
					"El programa no ha detectado ning�n Stick USB en el sistema.\nPor favor, inserte el Stick USB que desee inicializar y pulse el bot�n Aceptar",
					"CLIN :: S�lo un Stick",
					MB_ICONEXCLAMATION);
			} else if ( numDispositivos == 1 )
				break;

		}

		unidades = new char * [numDispositivos];

		for ( int j=0; j<numDispositivos; j++ ) 
			unidades[j] = new char[5];
			
		dispositivos = new int [numDispositivos];

		result = lowlevel_enumerar_dispositivos( unidades, dispositivos, &numDispositivos );

		if ( result != ERR_LOWLEVEL_NO ) {
			m_bError = TRUE;
			m_strErrMsg = "Imposible realizar la inicializaci�n del dispositivo.\n";

			retval = 1;
			goto finRun;
		}

		result = lowlevel_abrir_dispositivo( dispositivos[0], &handle );
		if ( result != ERR_LOWLEVEL_NO ) {
			m_bError = TRUE;
			m_strErrMsg = "Imposible realizar la inicializaci�n del dispositivo.\n";

			retval = 1;
			goto finRun;
		}

		m_dlgPaso4->Paso();	

		long C,H,S;
		PARTICION particiones[4], nuevas_particiones[4];

		if ( format_obtener_geometria_optima(dispositivos[0], &H, &S, &C) != ERR_FORMAT_NO ) {
			m_bError = TRUE;
			m_strErrMsg = "Imposible realizar la inicializaci�n del dispositivo.\n";

			retval = 1;
			goto finRun;
		}

		if ( format_crear_mbr(handle, H,C,S) != ERR_FORMAT_NO ) {
			m_bError = TRUE;
			m_strErrMsg = "Imposible realizar la inicializaci�n del dispositivo.\n";

			retval = 1;
			goto finRun;
		}


		if ( lowlevel_leer_particiones(handle, particiones) != ERR_FORMAT_NO ) {
			m_bError = TRUE;
			m_strErrMsg = "Imposible realizar la inicializaci�n del dispositivo.\n";

			retval = 1;
			goto finRun;
		}

		if ( format_cambiar_particiones_mbr(handle, 95, particiones, nuevas_particiones) != ERR_FORMAT_NO ) {
			m_bError = TRUE;
			m_strErrMsg = "Imposible realizar la inicializaci�n del dispositivo.\n";

			retval = 1;
			goto finRun;
		}

		result = format_releer_mbr( handle );

		if ( result == ERR_FORMAT_FUNCION_NO_DISPONIBLE ) {

			MessageBox(AfxGetApp()->GetMainWnd()->m_hWnd, "Por favor, retire el stick USB y pulse el bot�n Aceptar\n",
					"CLIN :: Extraiga y vuelva a insertar el stick", MB_ICONEXCLAMATION);

			msg = "Por favor, inserte de nuevo el stick y pulse el bot�n Aceptar";

			while (1) {

				MessageBox(AfxGetApp()->GetMainWnd()->m_hWnd, msg.GetBuffer(0),
					"CLIN :: Extraiga y vuelva a insertar el stick", MB_ICONEXCLAMATION);

				result = lowlevel_enumerar_dispositivos( NULL, NULL, &numDispositivos );

				if ( result != 0 ) {
					m_bError = TRUE;
					m_strErrMsg = "Imposible realizar la inicializaci�n del dispositivo.\n";

					retval = 1;
					goto finRun;
				}

				if ( numDispositivos == 0 ) {
					msg = "No ha insertado todav�a su Stick USB. Por favor, \n";
					msg += "ins�rtelo de nuevo y pulse el bot�n Aceptar";
				} else if ( numDispositivos > 1 ) {
					msg = "El programa ha detectado m�s de un Stick USB insertado en el sistema.\n";
					msg += "Por favor, deje insertado �nicamente el Stick con el que empez� el proceso\n";
					msg += "de inicializaci�n.";
				} else if ( numDispositivos == 1 )
					break;
			}
			result = lowlevel_cerrar_dispositivo( handle );
			if ( result != ERR_LOWLEVEL_NO ) {
				m_bError = TRUE;
				m_strErrMsg = "Imposible realizar la inicializaci�n del dispositivo.\n";

				retval = 1;
				goto finRun;
			}

			result = lowlevel_abrir_dispositivo( dispositivos[0], &handle );
			if ( result != ERR_LOWLEVEL_NO ) {
				m_bError = TRUE;
				m_strErrMsg = "Imposible realizar la inicializaci�n del dispositivo.\n";

				retval = 1;
				goto finRun;
			}

		}

		m_dlgPaso4->Paso();	
		m_dlgPaso4->SetDescripcion("Formateando dispositivo...");
/*
		result = format_formatear_unidad_logica_win( unidades[0] );
		if ( result != ERR_FORMAT_NO ) {
			m_bError = TRUE;
			m_strErrMsg = "Imposible realizar la inicializaci�n del dispositivo.\n";

			retval = 1;
			goto finRun;
		}
*/
		m_dlgPaso4->Paso();	
/*
		result = format_crear_zona_cripto( handle, nuevas_particiones[3], m_strPINClauer.GetBuffer(0), NULL );
		if ( result != ERR_FORMAT_NO ) {
			m_bError = TRUE;
			m_strErrMsg = "Imposible realizar la inicializaci�n del dispositivo.\n";

			retval = 1;
			goto finRun;
		}*/

		result = format_crear_clauer(dispositivos[0],unidades[0],m_strPINClauer.GetBuffer(0),0);
		if ( result != ERR_FORMAT_NO ) {
			m_bError = TRUE;
			m_strErrMsg = "Imposible crear Clauer.\n";

			retval = 1;
			goto finRun;
		}

		m_dlgPaso4->Paso();	

		result = lowlevel_cerrar_dispositivo( handle );
		if ( result != ERR_LOWLEVEL_NO ) {
			m_bError = TRUE;
			m_strErrMsg = "Imposible realizar la inicializaci�n del dispositivo.\n";

			retval = 1;
			goto finRun;
		}


		/* Ahora vamos a importar los certificados del disquete
		 */

		HANDLE hFind;
		WIN32_FIND_DATA findData;
		int pkcs12Encontrados=0;
		BOOL masFicheros;
		char *filtro[2] = {"A:\\*.p12", "A:\\*.pfx"};

		unsigned char *devices[MAX_DEVICES];
		int nDisp = 0;
	
		/* Iteramos para ambas m�scaras
		 */

		LIBRT_Ini();

		if ( LIBRT_ListarDispositivos(&nDisp, devices) != 0 ) {
			m_bError = TRUE;
			m_strErrMsg = "No se pudieron importar los PKCS12. ";

			retval = 1;
			goto finRun;
		}

		if ( LIBRT_IniciarDispositivo ( (unsigned char *) devices[0], this->m_strPINClauer.GetBuffer(0), &hClauer ) != 0 ) {

			for ( int k = 0 ; k < nDisp ; k++ ) 
				free(devices[k]);

			m_bError = TRUE;
			m_strErrMsg = "No se pudieron importar los PKCS12. ";

			retval = 1;
			goto finRun;
		}

		dispositivoIniciado = TRUE;

		for ( int k = 0 ; k < nDisp ; k++ ) 
			free(devices[k]);




		for ( int f = 0 ; f < 2 ; f++ ) {

			hFind = FindFirstFile(filtro[f], &findData);

			if ( hFind == INVALID_HANDLE_VALUE ) {

				if ( f == 0 )
					continue;
				else  { //if ( pkcs12Encontrados == 0 ) {
					FindClose(hFind);
					break;
				}
			}
			
			do {
			
				if ( !LIBIMPORT_ImportarPKCS12((CString("A:\\") + CString(findData.cFileName)).GetBuffer(0), 
											   this->m_strPINDisquete.GetBuffer(0), 
											   &hClauer) ) 
				{

					m_bError = TRUE;
					m_strErrMsg = "No se pudieron importar los PKCS12.";

					retval = 1;
					goto finRun;
				}

				++pkcs12Encontrados;

				masFicheros = FindNextFile(hFind, &findData);

				if ( !masFicheros ) {
					DWORD err;
					err = GetLastError();
					if ( err == ERROR_NO_MORE_FILES )
						break;
					else {
						m_bError = TRUE;
						m_strErrMsg = "Ocurri� un error de E/S.\n";
						m_strErrMsg += "de registro. ��NO PULSE NING�N BOT�N!!";
						
						retval = 1;
						goto finRun;
					}
				}
			
			} while (1);

			FindClose(hFind);
		}

		if ( pkcs12Encontrados == 0 ) {
			m_bError = TRUE;
			
			m_strErrMsg = "La aplicaci�n no encontr� certificados en la unidad A:\n";

			retval = 1;
			goto finRun;
		}

		
		/* Importamos las CAs
		 */

		BYTE bloque[TAM_BLOQUE];
		long aux;

		for ( int i = 0 ; i < gNumCAs ; i++ ) {

			SecureZeroMemory(bloque, TAM_BLOQUE);
			BLOQUE_Set_Claro(bloque);

			esRoot = CRYPTO_X509_EsRoot (gCAs[i], gTamCAs[i]);

			if (  esRoot == 1 ) {

				BLOQUE_CERTRAIZ_Nuevo(bloque);
				BLOQUE_CERTRAIZ_Set_Tam(bloque, gTamCAs[i]);
				BLOQUE_CERTRAIZ_Set_Objeto(bloque, gCAs[i], gTamCAs[i]);

			} else if ( esRoot == 0 ) {
				
				BLOQUE_CERTINTERMEDIO_Nuevo(bloque);
				BLOQUE_CERTINTERMEDIO_Set_Tam(bloque, gTamCAs[i]);
				BLOQUE_CERTINTERMEDIO_Set_Objeto(bloque, gCAs[i], gTamCAs[i]);

			} else {
				m_bError = TRUE;
			
				m_strErrMsg = "No se pudieron importar las CAs\n";

				retval = 1;
				goto finRun;				
			}
			
			if ( LIBRT_InsertarBloqueCrypto(&hClauer, bloque, &aux) != 0 ) {
				m_bError = TRUE;
			
				m_strErrMsg = "No se pudieron importar las CAs\n";

				retval = 1;
				goto finRun;				
			}
		}

		/* Insertamos un bloque de tipo 0x0e (indica que la contrase�a no ha sido cambiada todav�a)
		 */

		SecureZeroMemory(bloque, TAM_BLOQUE);
		BLOQUE_Set_Claro(bloque);
		*(bloque + 1) = 0x0e;

		if ( LIBRT_InsertarBloqueCrypto(&hClauer, bloque, &aux) != 0 ) {
			m_bError = TRUE;
			m_strErrMsg = "No se pudo marcar clauer como sin contrase�a cambiada\n";
			retval = 1;
			goto finRun;
		}

		/* Finalizamos el dispositivo
		 */

		if ( LIBRT_FinalizarDispositivo(&hClauer) != 0 ) {
			m_bError = TRUE;
			
			m_strErrMsg = "No se pudo completar la importaci�n.\n";

			retval = 1;
			goto finRun;				
		}

		dispositivoIniciado = FALSE;
		
		/* Copiar el instalador en la zona de datos
		 */

		HANDLE hSource, hDest;
		BYTE buf[10240];
		DWORD bytesWritten, bytesRead;
		string ficheros[2];

		/* Creo el directorio clauer en el clauer :-P
		 */

		
		if ( ! CreateDirectory((string(unidades[0])+string("\\")+string("clauer")).c_str(), NULL) ) {
			m_bError = TRUE;
			m_strErrMsg = "No se pudo crear directorio clauer de destino\n";
			retval = 1;
			goto finRun;
		}

		if ( ! SetFileAttributes((string(unidades[0])+string("\\")+string("clauer")).c_str(), FILE_ATTRIBUTE_HIDDEN) ) {
			m_bError = TRUE;
			m_strErrMsg = "No se pudo ocultar directorio clauer\n";
			retval = 1;
			goto finRun;
		}

		ficheros[0] = "FilesToCopy\\clauer\\setup-clauer.exe";
		ficheros[1] = "FilesToCopy\\clauer\\autorun.inf";
		ficheros[2] = "FilesToCopy\\autorun.inf";

		for ( int k = 0 ; k < 3 ; k++ ) {

			hSource = CreateFile(ficheros[k].c_str(), GENERIC_READ, 0, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL | FILE_FLAG_NO_BUFFERING, 0);
			if ( hSource == INVALID_HANDLE_VALUE ) {
				m_bError = TRUE;
				
				m_strErrMsg = "No se puede leer instalador Base.\n";

				retval = 1;
				goto finRun;
			}

			// OJO!!!! A pelo la ruta de creaci�n. Cuidado con esto

			string dest;
			dest = string(unidades[0]) + string("\\") + string(ficheros[k].c_str()+12);


			hDest = CreateFile(dest.c_str(), 
							   GENERIC_WRITE, 
							   0, 
							   NULL, 
							   CREATE_ALWAYS, 
							   FILE_ATTRIBUTE_NORMAL | FILE_FLAG_WRITE_THROUGH, 
							   0);

			if ( hDest == INVALID_HANDLE_VALUE ) {

				DWORD err = GetLastError();
				CloseHandle(hSource);
				m_bError = TRUE;
				
				m_strErrMsg = "No se puede escribir instalador Base.\n";

				retval = 1;
				goto finRun;
			}


			if ( ! ReadFile(hSource, buf, 10240, &bytesRead, NULL) ) {
				CloseHandle(hSource);
				CloseHandle(hDest);

				m_bError = TRUE;
				
				m_strErrMsg = "No se puede leer instalador Base.\n";

				retval = 1;
				goto finRun;
			}

			
			while ( bytesRead > 0 ) {


				if ( ! WriteFile(hDest, buf, bytesRead, &bytesWritten, NULL) ) {

					CloseHandle(hSource);
					CloseHandle(hDest);

					m_bError = TRUE;

					m_strErrMsg = "No se puede escribir instalador Base.\n";
					retval = 1;
					goto finRun;

				}

				if ( bytesRead != bytesWritten ) {

					CloseHandle(hSource);
					CloseHandle(hDest);

					m_bError = TRUE;

					m_strErrMsg = "No se puede escribir instalador Base.\n";
					retval = 1;
					goto finRun;

				}


				if ( ! ReadFile(hSource, buf, 10240, &bytesRead, NULL) ) {
					CloseHandle(hSource);
					CloseHandle(hDest);

					m_bError = TRUE;
				
					m_strErrMsg = "No se puede leer instalador Base.\n";

					retval = 1;
					goto finRun;
				}
			}

			CloseHandle(hSource);
			CloseHandle(hDest);

		}

		/* Hacemos oculto autorun.inf
		 */
		
		string autorunFile;
		autorunFile = string(unidades[0]) + string("\\") + string("autorun.inf");
		if ( ! SetFileAttributes(autorunFile.c_str(), FILE_ATTRIBUTE_ARCHIVE|FILE_ATTRIBUTE_HIDDEN) ) {
			m_bError = TRUE;
			m_strErrMsg = "No se pudo establecer atributos para autorun.inf";
			retval=1;
			goto finRun;
		}

		/* Actualizamos base de datos
		 */

		m_dlgPaso4->SetDescripcion("Actualizando la base de datos");

		if ( ! this->m_bOmitirBaseDatos ) {
			try {
				CBaseDatos bd;
				bd.RegistrarStick(m_strDNI.GetBuffer(0), hClauer.idDispositivo);
			}
			catch (EBaseDatos &) {
				m_bError = TRUE;
				m_strErrMsg = "Se produjo un error al tratar de dar de alta el stick. ";
				m_strErrMsg += "NO ENTREGUE EL STICK AL USUARIO";
							
				retval = 1;
				goto finRun;			
			}
		}

		m_dlgPaso4->SetDescripcion("Inicializaci�n completada con �xito...");
	
		m_bFinished = TRUE;
	}
	catch (...) {
		// Error desconocido

		m_bError = TRUE;
		m_strErrMsg = "Ocurri� un error grave en la aplicaci�n.\n";

		retval = 1;
		goto finRun;
	}

finRun:

	if ( unidades ) {
		for ( int j = 0 ; j < numDispositivos; j++ ) 
			if (unidades[j])
				delete [] unidades[j];

		delete [] unidades;
	}

	if (dispositivos)
		delete [] dispositivos;


	if ( dispositivoIniciado )
		LIBRT_FinalizarDispositivo(&hClauer);

	m_bFinished = TRUE;

	return retval;
}



BOOL CThrPaso4::Terminado()
{
	return m_bFinished;
}


BOOL CThrPaso4::GetError()
{
	return m_bError;
}

CString CThrPaso4::GetMsgError()
{
	return m_strErrMsg;
}


void CThrPaso4::SetPINDisquete(CString pin)
{
	m_strPINDisquete = pin;
}

void CThrPaso4::SetPINClauer(CString pin)
{
	m_strPINClauer = pin;
}


void CThrPaso4::SetDNI(CString dni)
{
	this->m_strDNI = dni;
}

void CThrPaso4::SetComprobacion(BOOL omitir)
{
	this->m_bOmitirBaseDatos = omitir;

}
