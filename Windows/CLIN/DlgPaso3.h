#if !defined(AFX_DLGPASO3_H__45B70A43_1502_4FE7_ACB6_043D2FB4917C__INCLUDED_)
#define AFX_DLGPASO3_H__45B70A43_1502_4FE7_ACB6_043D2FB4917C__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// DlgPaso3.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CDlgPaso3 dialog

class CDlgPaso3 : public CDialog
{
// Construction
public:
	CString GetConfirmacion(void);
	CString GetPIN(void);
	CDlgPaso3(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CDlgPaso3)
	enum { IDD = IDD_DIALOG_PASO_3 };
	CStatic	m_lblDescripcion;
	CString	m_editConfirmacion;
	CString	m_editPIN;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CDlgPaso3)
	public:
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CDlgPaso3)
	afx_msg void OnShowWindow(BOOL bShow, UINT nStatus);
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DLGPASO3_H__45B70A43_1502_4FE7_ACB6_043D2FB4917C__INCLUDED_)
