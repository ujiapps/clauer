//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by CLIN.rc
//
#define IDD_CLIN_DIALOG                 102
#define IDR_MAINFRAME                   128
#define IDD_DIALOG_PASO_1               129
#define IDD_DIALOG_PASO_2               130
#define IDD_DIALOG1                     131
#define IDD_DIALOG_PASO_3               132
#define IDD_DIALOG_PASO_4               133
#define IDD_DIALOG_PASO_5               134
#define IDB_BITMAP_PASO_1               135
#define IDB_BITMAP_PASO_2               136
#define IDB_BITMAP_PASO_3               137
#define IDB_BITMAP_PASO_4               139
#define IDB_BITMAP_PASO_5               140
#define IDD_DIALOG_PASO_2_5             141
#define IDB_BITMAP_PASO_6               141
#define IDD_DIALOG_PASO_1_5             143
#define IDC_BTNSIGUIENTE                1000
#define IDC_BUTTON2                     1001
#define IDC_BTN_SALIR                   1001
#define IDC_GROUPBOX                    1002
#define IDC_FRAME                       1003
#define IDC_LBL_PASO_1                  1004
#define IDC_LBL_PASO_2                  1005
#define IDC_LBL_PASO_3                  1006
#define IDC_EDIT1                       1007
#define IDC_LBL_PIN                     1009
#define IDC_LBL_DESCRIPCION             1010
#define IDC_EDIT2                       1011
#define IDC_EDIT_CONFIRMACION           1011
#define IDC_PROGRESS1                   1012
#define IDC_PROGRESO                    1012
#define IDC_BTN_ANTERIOR                1014
#define IDC_EDIT_PIN                    1015
#define IDC_IMAGENES                    1016
#define IDC_LBL_DESCRIPCION_1           1017
#define IDC_BTNIDIOMA                   1018
#define IDC_EDIT_EMAIL                  1024
#define IDC_EDIT_DNI                    1025
#define IDC_EDIT_EMAIL2                 1026
#define IDC_EDIT_COLECTIU               1026
#define IDC_CHECK_OMITIR                1027
#define IDC_CHECK_OMITIR_DNIS           1028

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        146
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1029
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
