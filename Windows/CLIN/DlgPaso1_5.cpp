// DlgPaso1_5.cpp : implementation file
//

#include "stdafx.h"
#include "CLIN.h"
#include "DlgPaso1_5.h"
#include "CLINDlg.h"
#include ".\dlgpaso1_5.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CDlgPaso1_5 dialog


CDlgPaso1_5::CDlgPaso1_5(CWnd* pParent /*=NULL*/)
	: CDialog(CDlgPaso1_5::IDD, pParent)

{
	//{{AFX_DATA_INIT(CDlgPaso1_5)
	m_editMail = _T("");
	m_editDNI = _T("");
	m_editColectiu = _T("");
	//}}AFX_DATA_INIT
}


void CDlgPaso1_5::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CDlgPaso1_5)
	DDX_Text(pDX, IDC_EDIT_EMAIL, m_editMail);
	DDX_Text(pDX, IDC_EDIT_DNI, m_editDNI);
	DDX_Text(pDX, IDC_EDIT_COLECTIU, m_editColectiu);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CDlgPaso1_5, CDialog)
	//{{AFX_MSG_MAP(CDlgPaso1_5)
	//}}AFX_MSG_MAP
	ON_EN_CHANGE(IDC_EDIT_DNI, OnEnChangeEditDni)
	ON_EN_CHANGE(IDC_EDIT_EMAIL, OnEnChangeEditEmail)
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CDlgPaso1_5 message handlers


void CDlgPaso1_5::SetEmail(string email)
{

	this->m_editMail = email.c_str();
	UpdateData(FALSE);

}

void CDlgPaso1_5::SetDNI(string DNI)
{
	this->m_editDNI = DNI.c_str();
	//this->m_lblDNI = DNI.c_str();
	UpdateData(FALSE);
}

void CDlgPaso1_5::SetColectiu(string colectiu)
{

	this->m_editColectiu = colectiu.c_str();
	UpdateData(FALSE);

}

BOOL CDlgPaso1_5::PreTranslateMessage(MSG* pMsg) 
{
	if(pMsg->message==WM_KEYDOWN)
	{

		switch (pMsg->wParam) {

		case VK_ESCAPE:
			pMsg->wParam=NULL ;
			break;

		case VK_RETURN:

			((CCLINDlg *) this->GetParent())->OnBtnsiguiente();
			pMsg->wParam = NULL;
			return TRUE;

			break;

		case VK_TAB:
			((CCLINDlg *) this->GetParent())->SetFocus();
			pMsg->wParam=NULL;
			break;

		}
	}

	return CDialog::PreTranslateMessage(pMsg);

}

void CDlgPaso1_5::OnEnChangeEditDni()
{
	// TODO:  Si �ste es un control RICHEDIT, el control no
	// enviar� esta notificaci�n a menos que se anule la funci�n CDialog::OnInitDialog()
	// y se llame a CRichEditCtrl().SetEventMask()
	// con el indicador ENM_CHANGE ORed en la m�scara.

	// TODO:  Agregue aqu� el controlador de notificaci�n de controles
}

void CDlgPaso1_5::OnEnChangeEditEmail()
{
	// TODO:  Si �ste es un control RICHEDIT, el control no
	// enviar� esta notificaci�n a menos que se anule la funci�n CDialog::OnInitDialog()
	// y se llame a CRichEditCtrl().SetEventMask()
	// con el indicador ENM_CHANGE ORed en la m�scara.

	// TODO:  Agregue aqu� el controlador de notificaci�n de controles
}
