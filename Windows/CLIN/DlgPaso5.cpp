// DlgPaso5.cpp : implementation file
//

#include "stdafx.h"
#include "CLIN.h"
#include "DlgPaso5.h"
#include "CLINDlg.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CDlgPaso5 dialog


CDlgPaso5::CDlgPaso5(CWnd* pParent /*=NULL*/)
	: CDialog(CDlgPaso5::IDD, pParent)
{
	//{{AFX_DATA_INIT(CDlgPaso5)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}


void CDlgPaso5::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CDlgPaso5)
		// NOTE: the ClassWizard will add DDX and DDV calls here
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CDlgPaso5, CDialog)
	//{{AFX_MSG_MAP(CDlgPaso5)
		// NOTE: the ClassWizard will add message map macros here
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CDlgPaso5 message handlers

BOOL CDlgPaso5::PreTranslateMessage(MSG* pMsg) 
{
	if(pMsg->message==WM_KEYDOWN)
	{
		switch (pMsg->wParam) {

		case VK_ESCAPE:
			pMsg->wParam=NULL ;
			break;

		case VK_RETURN:

			((CCLINDlg *) this->GetParent())->OnBtnsiguiente();
			pMsg->wParam = NULL;
			return TRUE;

			break;

		case VK_TAB:
			((CCLINDlg *) this->GetParent())->SetFocus();
			pMsg->wParam=NULL;
			break;

		}
	}

	return CDialog::PreTranslateMessage(pMsg);
}
