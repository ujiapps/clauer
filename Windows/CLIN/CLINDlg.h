// CLINDlg.h : header file
//

#if !defined(AFX_CLINDLG_H__9042E0EC_4638_4362_86C2_86C607FF62E5__INCLUDED_)
#define AFX_CLINDLG_H__9042E0EC_4638_4362_86C2_86C607FF62E5__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include "DlgPaso1.h"
#include "DlgPaso2.h"
#include "DlgPaso3.h"
#include "DlgPaso4.h"
#include "DlgPaso5.h"
#include "DlgPaso2_5.h"
#include "DlgPaso1_5.h"
#include "ThrPaso4.h"

/////////////////////////////////////////////////////////////////////////////
// CCLINDlg dialog

class CCLINDlg : public CDialog
{
// Construction
public:
	CCLINDlg(CWnd* pParent = NULL);	// standard constructor

	friend class CDlgPaso1;
	friend class CDlgPaso1_5;
	friend class CDlgPaso2;
	friend class CDlgPaso2_5;
	friend class CDlgPaso3;
	friend class CDlgPaso4;
	friend class CDlgPaso5;
	friend class CThrPaso4;

// Dialog Data
	//{{AFX_DATA(CCLINDlg)
	enum { IDD = IDD_CLIN_DIALOG };
	CStatic	m_frame;
	CStatic	m_pnlIzquierdo;
	CButton	m_btnAnterior;
	CButton	m_btnSalir;
	CButton	m_btnSiguiente;
	//}}AFX_DATA

	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CCLINDlg)
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:
	CThrPaso4 * thrPaso4;
	int pinCount;
	void SetPaso (int paso);
	void VerificarPKCS12 (CString pwd, CString dni);
	int m_paso;	// Indica en qu� paso estamos actualmente: [1, 5]
	CDlgPaso5 m_dlgPaso5;
	CDlgPaso4 m_dlgPaso4;
	CDlgPaso3 m_dlgPaso3;
	CDlgPaso2 m_dlgPaso2;
	CDlgPaso1 m_dlgPaso1;
	CDlgPaso2_5 m_dlgPaso2_5;
	CDlgPaso1_5 m_dlgPaso1_5;
	HICON m_hIcon;

	BOOL m_bOmitir;
	BOOL m_bOmitirDNI;

	string dni;

	// Generated message map functions
	//{{AFX_MSG(CCLINDlg)
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	afx_msg void OnBtnsiguiente();
	afx_msg void OnBtnAnterior();
	afx_msg void OnBtnSalir();
	afx_msg void OnTimer(UINT nIDEvent);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CLINDLG_H__9042E0EC_4638_4362_86C2_86C607FF62E5__INCLUDED_)
