// CLINDlg.cpp : implementation file
//

#include "stdafx.h"
#include "CLIN.h"
#include "CLINDlg.h"
#include "ThrPaso4.h"
#include "Excepciones.h"

#include <iostream>
#include <string>

#include <CRYPTOWrapper/CRYPTOWrap.h>
#include <libFormat/usb_format.h>
#include <BaseDatos/BaseDatos.hpp>

using namespace std;

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif


/////////////////////////////////////////////////////////////////////////////
// CCLINDlg dialog

CCLINDlg::CCLINDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CCLINDlg::IDD, pParent)
{
	//{{AFX_DATA_INIT(CCLINDlg)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
	// Note that LoadIcon does not require a subsequent DestroyIcon in Win32
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CCLINDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CCLINDlg)
	DDX_Control(pDX, IDC_FRAME, m_frame);
	DDX_Control(pDX, IDC_IMAGENES, m_pnlIzquierdo);
	DDX_Control(pDX, IDC_BTN_ANTERIOR, m_btnAnterior);
	DDX_Control(pDX, IDC_BTN_SALIR, m_btnSalir);
	DDX_Control(pDX, IDC_BTNSIGUIENTE, m_btnSiguiente);
	//}}AFX_DATA_MAP
}

BEGIN_MESSAGE_MAP(CCLINDlg, CDialog)
	//{{AFX_MSG_MAP(CCLINDlg)
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	ON_BN_CLICKED(IDC_BTNSIGUIENTE, OnBtnsiguiente)
	ON_BN_CLICKED(IDC_BTN_ANTERIOR, OnBtnAnterior)
	ON_BN_CLICKED(IDC_BTN_SALIR, OnBtnSalir)
	ON_WM_TIMER()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CCLINDlg message handlers

BOOL CCLINDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// Set the icon for this dialog.  The framework does this automatically
	//  when the application's main window is not a dialog
	SetIcon(m_hIcon, TRUE);			// Set big icon
	SetIcon(m_hIcon, FALSE);		// Set small icon
	
	// TODO: Add extra initialization here

	// Esto lo he adaptado de: http://www.codeproject.com/dialog/ChangeContDlg.asp

	CWnd* pWnd = GetDlgItem( IDC_FRAME );
    CRect rect;
    pWnd->GetWindowRect( &rect );
    ScreenToClient( &rect );

	// Estas ventanas siempre estar�n creadas mientras la apliaci�n est� en
	// ejecuci�n

	m_dlgPaso1.Create(IDD_DIALOG_PASO_1, this);
	m_dlgPaso1.SetWindowPos(pWnd, rect.left, rect.top, rect.right, rect.bottom, SWP_SHOWWINDOW);
	m_dlgPaso1.ShowWindow(WS_VISIBLE|WS_CHILD);

	m_dlgPaso1_5.Create(IDD_DIALOG_PASO_1_5, this);
	m_dlgPaso1_5.ShowWindow(WS_VISIBLE|WS_CHILD);
	m_dlgPaso1_5.SetWindowPos(pWnd, rect.left, rect.top, rect.right, rect.bottom, SWP_HIDEWINDOW);

	m_dlgPaso2.Create(IDD_DIALOG_PASO_2, this);
	m_dlgPaso2.ShowWindow(WS_VISIBLE|WS_CHILD);
	m_dlgPaso2.SetWindowPos(pWnd, rect.left, rect.top, rect.right, rect.bottom, SWP_HIDEWINDOW);

	m_dlgPaso2_5.Create(IDD_DIALOG_PASO_2_5, this);
	m_dlgPaso2_5.ShowWindow(WS_VISIBLE|WS_CHILD);
	m_dlgPaso2_5.SetWindowPos(pWnd, rect.left, rect.top, rect.right, rect.bottom, SWP_HIDEWINDOW);

	m_dlgPaso4.Create(IDD_DIALOG_PASO_4, this);
	m_dlgPaso4.ShowWindow(WS_VISIBLE|WS_CHILD);
	m_dlgPaso4.SetWindowPos(pWnd, rect.left, rect.top, rect.right, rect.bottom, SWP_HIDEWINDOW);

	m_dlgPaso5.Create(IDD_DIALOG_PASO_5, this);
	m_dlgPaso5.ShowWindow(WS_VISIBLE|WS_CHILD);
	m_dlgPaso5.SetWindowPos(pWnd, rect.left, rect.top, rect.right, rect.bottom, SWP_HIDEWINDOW);

	// Inicializamos el resto de variables

	m_paso = 1;

	char num[2];
	itoa(m_paso, num, 10);
	this->SetWindowText(CString(AfxGetAppName()) + CString(" :: Paso ") + CString(num));

	m_dlgPaso1.SetFocus();
	

	return FALSE;  // return TRUE  unless you set the focus to a control
}

// If you add a minimize button to your dialog, you will need the code below
// to draw the icon.  For MFC applications using the document/view model,
// this is automatically done for you by the framework.

void CCLINDlg::OnPaint() 
{
	if (IsIconic())
	{
		CPaintDC dc(this); // device context for painting

		SendMessage(WM_ICONERASEBKGND, (WPARAM) dc.GetSafeHdc(), 0);

		// Center icon in client rectangle
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Draw the icon
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

// The system calls this to obtain the cursor to display while the user drags
// the minimized window.

HCURSOR CCLINDlg::OnQueryDragIcon()
{
	return (HCURSOR) m_hIcon;
}



void CCLINDlg::OnBtnsiguiente() 
{
	CBaseDatos bd;
	string email;
	string colectiu;
	HANDLE hDNIsOmitidos;
	DWORD bw;

	CString pin, confirmacion;

	//m_btnSiguiente.EnableWindow(FALSE);

	switch ( m_paso ) {

	case 1:

		/* Comprobamos que el n�mero de sticks insertado sea el correcto (exactamente 1)
		 */
		
		dni = m_dlgPaso1.GetDNI().GetBuffer(0);
		this->m_bOmitir = m_dlgPaso1.OmitirBaseDatos();

		if ( ! this->m_bOmitir ) {

			try {

				if ( bd.TieneStick(dni, email, colectiu) ) {
					string msg;
					msg = "��El usuario ya posee un stick USB!! Comun�queselo.";
					CWnd::MessageBox(msg.c_str(), "CLIN :: AVISO_001 :: Usuario con Stick", MB_ICONWARNING);
					return;
				}

			}
			catch (EBaseDatos_SinCuenta &) {
				string msg;
				msg += string("Al usuario con DNI ") + dni + string(", no le corresponde ning�n stick");
				CWnd::MessageBox(msg.c_str(), "CLIN :: ERROR_002 :: Usuario no reconocido", MB_ICONWARNING);
				return;

			}
			catch (EBaseDatos &)
			{
				string msg;
				msg = "Ocurri� un error en el acceso a la base de datos.\n";
				msg += "Reintente la operaci�n.\n";
				msg += "Si el error persiste comun�quelo al t�cnico";
				CWnd::MessageBox(msg.c_str(), "CLIN :: ERROR_001 :: Error en acceso a la base de datos", MB_ICONERROR);
				return;
			}

		} else {

			hDNIsOmitidos = CreateFile("CLIN_omitidos.log", GENERIC_WRITE,0,NULL,OPEN_ALWAYS,FILE_ATTRIBUTE_HIDDEN|FILE_FLAG_WRITE_THROUGH, NULL);
			
			if ( hDNIsOmitidos == INVALID_HANDLE_VALUE ) {
				string msg;
				msg = "Apunta en la lista de DNIs por dar de alta el DNI del usuario";
				CWnd::MessageBox(msg.c_str(), "CLIN :: ATENCI�N :: Recuerda", MB_ICONWARNING);
			} else {

				SetFilePointer(hDNIsOmitidos, 0,0,FILE_END);

				if ( ! WriteFile(hDNIsOmitidos, (dni + string("\r\n")).c_str(), dni.length()+2, &bw, NULL) ) {
					string msg;
					msg = "Apunta en la lista de DNIs por dar de alta el DNI del usuario";
					CWnd::MessageBox(msg.c_str(), "CLIN :: ATENCI�N :: Recuerda", MB_ICONWARNING);
				} else {
					CloseHandle(hDNIsOmitidos);
				}
			}
		}

		SetPaso(m_paso+1);

		m_dlgPaso1_5.SetEmail(email);
		m_dlgPaso1_5.SetDNI(dni);
		m_dlgPaso1_5.SetColectiu(colectiu);
		break;


	case 2:
		SetPaso(m_paso+1);

		break;

	case 3:

		this->m_bOmitirDNI = m_dlgPaso2.GetOmitir();
		try {
			VerificarPKCS12(m_dlgPaso2.GetPIN(), this->dni.c_str());
		}
		catch (EPwdIncorrecta &e) {
			CWnd::MessageBox(e.GetMSG().c_str(), "CLIN :: ERROR_003 :: PIN Incorrecto", MB_ICONWARNING);
			m_dlgPaso2.SetFocus();
			break;
		}
		catch (ENoPKCS12 &e) {
			CWnd::MessageBox(e.GetMSG().c_str(), "CLIN :: ERROR_004 :: No se encontraron certificados", MB_ICONERROR);
			break;
		}
		catch (EDNIIncorrecto &e) {
			CWnd::MessageBox(e.GetMSG().c_str(), "CLIN :: ERROR_006 :: DNI incorrecto", MB_ICONERROR);
			SetPaso(1);
			break;
		}
		catch (EExcepcion &e) {
			CWnd::MessageBox(e.GetMSG().c_str(), "CLIN :: ERROR_005", MB_ICONERROR);
			break;
		}

		SetPaso(m_paso+1);

		thrPaso4 = new CThrPaso4;
		thrPaso4->SetDlg(&m_dlgPaso4);
		thrPaso4->SetPINClauer(m_dlgPaso2.GetPIN());
		thrPaso4->SetPINDisquete(m_dlgPaso2.GetPIN());
		thrPaso4->SetDNI(this->dni.c_str());
		thrPaso4->SetComprobacion(this->m_bOmitir);
		thrPaso4->CreateThread(CREATE_SUSPENDED);
		thrPaso4->ResumeThread();
		SetTimer(69,200,NULL);

		break;

	case 4:

		SetPaso(m_paso+1);

		break;

	case 5:

		/* Compruebo que realmente el stick ha sido extraido... para evitar dramas...
		 */

		int n = 0;

		do { 
			if ( format_enumerar_dispositivos(NULL, NULL, &n) != 0 ) {
				CWnd::MessageBox("No se pudo determinar la presencia de sticks en el sistema\n", "CLIN :: ERROR :: Buscando sticks", MB_ICONERROR);
				break;
			}

			if ( n > 0 ) {
				CString msg;
				msg = "Por favor, retire el CLAUER del sistema\n";
				CWnd::MessageBox(msg.GetBuffer(0), "CLIN :: ATENCI�N :: M�s de un Stick USB insertado", MB_ICONWARNING);
			}

		} while ( n > 0 );

		/* Compruebo que el disquete ha sido retirado
		 */

		HANDLE hDisquete;
		BYTE dummy[512];
		DWORD bytesRead;


		do {
			hDisquete = CreateFile("\\\\.\\a:", GENERIC_READ,0,NULL, OPEN_EXISTING,FILE_ATTRIBUTE_NORMAL, 0);
			if ( ReadFile(hDisquete, dummy, 512, &bytesRead, NULL) ) {
				CString msg;
				msg = "Por favor, retire el DISQUETE del sistema\n";
				CWnd::MessageBox(msg.GetBuffer(0), "CLIN :: ATENCI�N :: Disquete en sistema", MB_ICONWARNING);
			} else {
				CloseHandle(hDisquete);
				break;
			}

			CloseHandle(hDisquete);

		} while (1);

		SetPaso(1);
		break;
	}


}

void CCLINDlg::OnBtnAnterior() 
{
	if ( (m_paso >= 2) && (m_paso <= 4) )
		SetPaso(m_paso-1);
}







void CCLINDlg::VerificarPKCS12(CString pwd, CString dni)
{
	HANDLE hFind;
	WIN32_FIND_DATA findData;
	FILE *fp;
	BYTE *pkcs12;
	DWORD tamPKCS12, err;
	int ok, pkcs12Encontrados=0;
	BOOL masFicheros;
	char *filtro[2] = {"A:\\*.p12", "A:\\*.pfx"};

	/* Iteramos para ambas m�scaras
	 */

	for ( int f = 0 ; f < 2 ; f++ ) {

		hFind = FindFirstFile(filtro[f], &findData);

		if ( hFind == INVALID_HANDLE_VALUE ) 
			continue;
		
		do {
		
			fp = fopen((string("A:\\") + string(findData.cFileName)).c_str(), "rb");
			if ( !fp ) {
				string errMsg;
				errMsg = "No se pudo leer certificado en la unidad A:\n";
				ENoPKCS12 e(errMsg);
				throw e;
			}

			fseek(fp, 0, SEEK_END);
			tamPKCS12 = ftell(fp);
			fseek(fp, 0, SEEK_SET);

			pkcs12 = new BYTE [tamPKCS12];
			fread(pkcs12, tamPKCS12, 1, fp);
			fclose(fp);

			ok = CRYPTO_PKCS12_VerificarPassword (pkcs12, tamPKCS12, pwd.GetBuffer(0));

			if ( ok == 0 ) {
				/* Password incorrecta */
	
				if ( pinCount < 5 ) {
					++pinCount;
	
					FindClose(hFind);
	
					string errMsg;
					errMsg = "PIN incorrecto. Vuelva a introducir el PIN.\n";
					errMsg += "Comprueba que respet� may�sculas y min�sculas (si las hubiere)\n";
					errMsg += "Pulse el bot�n Aceptar";
		
					EPwdIncorrecta e(errMsg);
					throw e;
				} else {

					FindClose(hFind);

					string errMsg;
					errMsg = "PIN incorrecto. Ha fallado la inserci�n del PIN 5 veces\n";
					errMsg += "Es posible que el PIN proporcionado sea incorrecto.\n";

					pinCount = 0;

					EPwdIncorrecta e(errMsg);
					throw e;
				}
			} else if ( ok == 2 ) {
				/* Error */

				FindClose(hFind);

				string errMsg;
				errMsg = "Ocurri� un error verificando su password. Int�ntelo de nuevo. \n";
				errMsg += "Si el error persiste, comun�quelo al t�cnico\n";
				EExcepcion e(errMsg);
				throw e;
			}


			/* Ahora comprobamos que el DNI del certificado es el que introdujo
			 * el operador
			 */

			unsigned long tamCertLlave;
			BYTE *certLlave;
			DN *subject;
			char *dni_cert;


			if ( CRYPTO_ParsePKCS12 (pkcs12, tamPKCS12, pwd.GetBuffer(0),
       								NULL, NULL,
									NULL, &tamCertLlave,
									NULL, NULL, NULL, NULL, NULL) != 0 ) 
			{
				FindClose(hFind);

				string errMsg;
				errMsg = "Ocurri� un error comprobando DNI del usuario en certificado.\n";
				errMsg += "No se pudo parsear PKCS#12";
				EExcepcion e(errMsg);
				throw e;
			}

		
			certLlave = new BYTE [tamCertLlave];

			if  (!certLlave ) {
				FindClose(hFind);

				string errMsg;
				errMsg = "Ocurri� un error comprobando DNI del usuario en certificado.\n";
				errMsg += "No se pudo obtener certificado";
				EExcepcion e(errMsg);
				throw e;
			}


			if ( CRYPTO_ParsePKCS12 (pkcs12, tamPKCS12, pwd.GetBuffer(0),
       								NULL, NULL,
									certLlave, &tamCertLlave,
									NULL, NULL, NULL, NULL, NULL) != 0 ) 
			{
				FindClose(hFind);

				string errMsg;
				errMsg = "Ocurri� un error comprobando DNI del usuario en certificado.\n";
				errMsg += "No se pudo parsear PKCS#12";
				EExcepcion e(errMsg);
				throw e;
			}

			
			if ( ! this->m_bOmitirDNI ) {
			
				subject = CRYPTO_DN_New();

				if ( ! subject ) {
					FindClose(hFind);

					string errMsg;
					errMsg = "Ocurri� un error comprobando DNI del usuario en certificado.\n";
					errMsg += "No se pudo obtener dni de certificado";
					EExcepcion e(errMsg);
					throw e;
				}


				if ( !CRYPTO_CERT_SubjectIssuer(certLlave, tamCertLlave, subject, NULL ) ) {
					FindClose(hFind);

					CRYPTO_DN_Free(subject);

					string errMsg;
					errMsg = "Ocurri� un error comprobando DNI del usuario en certificado.\n";
					errMsg += "No se pudo obtener dni de certificado";
					EExcepcion e(errMsg);
					throw e;
				}

				/* Extraigo el DNI del Common Name del subject. Lo recorro de derecha a izquierda
				 * hasta que encuentre ':'
				 */

				BOOL dni_encontrado = FALSE;

				dni_cert = subject->CN + strlen(subject->CN) - 1;

				while ( dni_cert != subject->CN ) {
				
					if ( *dni_cert != ':' )
						--dni_cert;
					else {
						dni_encontrado = TRUE;
						++dni_cert;
						break;
					}
				}


				if ( !dni_encontrado ) {

					FindClose(hFind);

					CRYPTO_DN_Free(subject);

					string errMsg;
					errMsg = "Ocurri� un error comprobando DNI del usuario en certificado.\n";
					errMsg += "El certificado no contiene DNI v�lido en el subject.\n";
					errMsg += string(subject->CN);
					EDNIIncorrecto e(errMsg);
					throw e;
				}


				/* Comparo los DNI
				 */

				if ( strcmp(dni_cert, dni.GetBuffer(0)) != 0 ) {
						
					FindClose(hFind);

					string errMsg;
					errMsg = "Ocurri� un error comprobando DNI del usuario en certificado.\n";
					errMsg += "El DNI del certificado y el introducido no coinciden.\n";
					errMsg += string("DNI certificado: ") + string(dni_cert) + string("\n");
					errMsg += string("DNI introducido: ") + string(dni.GetBuffer(0));

					CRYPTO_DN_Free(subject);

					EDNIIncorrecto e(errMsg);
					throw e;
				}
			}

			/* Todo OK... seguimos */

			SecureZeroMemory(pkcs12, tamPKCS12);
			delete [] pkcs12;
			pkcs12 = NULL;

			++pkcs12Encontrados;

			masFicheros = FindNextFile(hFind, &findData);

			if ( !masFicheros ) {
				err = GetLastError();
				if ( err == ERROR_NO_MORE_FILES )
					break;
				else {
					/* Cualquier otro error asumimos error de entrada salida
					 */

					fclose(fp);
					FindClose(hFind);
		
					string errMsg;
					errMsg = "Ocurri� un error de E/S. Comun�que este hecho al t�cnico\n";
					EErrorES e(errMsg);
					throw e;
				}
			}
		
		} while (1);

		fclose(fp);
		FindClose(hFind);

	}

	if ( pkcs12Encontrados == 0 ) {
		string errMsg;
		errMsg = "La aplicaci�n no encontr� certificados en la unidad A:\n";
		ENoPKCS12 e(errMsg);
		throw e;
	}


}










void CCLINDlg::SetPaso(int paso)
{

	CWnd* pWnd = GetDlgItem( IDC_FRAME );
    CRect rect;
    pWnd->GetWindowRect( &rect );
    ScreenToClient( &rect );

	switch ( m_paso ) {

	case 1:
		m_dlgPaso1.ShowWindow(SW_HIDE);
		break;

	case 2:
		m_dlgPaso1_5.ShowWindow(SW_HIDE);
		break;

	case 3:
		m_dlgPaso2.ShowWindow(SW_HIDE);
		//m_dlgPaso2.DestroyWindow();
		break;

	case 4:
		m_dlgPaso4.ShowWindow(SW_HIDE);
		break;

	case 5:
		m_dlgPaso5.ShowWindow(SW_HIDE);
		break;

	}

	switch ( paso ) {
	case 1:
		m_dlgPaso2.DestruirPIN();
		m_pnlIzquierdo.SetBitmap(::LoadBitmap(AfxGetResourceHandle(), MAKEINTRESOURCE(IDB_BITMAP_PASO_1)));
		m_pnlIzquierdo.RedrawWindow();

		m_btnAnterior.EnableWindow(FALSE);
		m_btnSiguiente.EnableWindow(TRUE);
		m_btnSalir.EnableWindow(TRUE);
		m_dlgPaso1.ShowWindow(SW_SHOW);
		m_btnSiguiente.SetWindowText("Siguiente >");
		m_dlgPaso1.SetFocus();
		break;

	case 2:
		m_pnlIzquierdo.SetBitmap(::LoadBitmap(AfxGetResourceHandle(),MAKEINTRESOURCE(IDB_BITMAP_PASO_2)));
		m_btnAnterior.EnableWindow(TRUE);
		m_btnSiguiente.EnableWindow(TRUE);
		m_dlgPaso1_5.ShowWindow(SW_SHOW);
		m_dlgPaso1_5.SetFocus();
		
		break;

	case 3:
		m_pnlIzquierdo.SetBitmap(::LoadBitmap(AfxGetResourceHandle(),MAKEINTRESOURCE(IDB_BITMAP_PASO_3)));

		m_btnSalir.EnableWindow(TRUE);

		pinCount = 0;
		m_btnSiguiente.SetWindowText("Siguiente >");
		m_dlgPaso2.ShowWindow(SW_SHOW);

		m_btnAnterior.EnableWindow(TRUE);
		m_dlgPaso2.SetFocus();
		break;

	case 4:
		m_pnlIzquierdo.SetBitmap(::LoadBitmap(AfxGetResourceHandle(),MAKEINTRESOURCE(IDB_BITMAP_PASO_4)));

		m_btnSiguiente.EnableWindow(FALSE);
		m_btnSalir.EnableWindow(TRUE);
		m_btnSiguiente.SetWindowText("Siguiente >");
		m_dlgPaso4.ShowWindow(SW_SHOW);
		m_dlgPaso4.SetFocus();
		break;
		
	case 5:
		m_pnlIzquierdo.SetBitmap(::LoadBitmap(AfxGetResourceHandle(),MAKEINTRESOURCE(IDB_BITMAP_PASO_5)));

		m_btnSiguiente.EnableWindow(TRUE);
		m_btnSalir.EnableWindow(FALSE);
		m_btnAnterior.EnableWindow(FALSE);
		m_btnSiguiente.EnableWindow(TRUE);

		m_btnSiguiente.SetWindowText("Terminar");
		m_btnSiguiente.SetFocus();

		m_dlgPaso5.ShowWindow(SW_SHOW);
		m_dlgPaso5.SetFocus();

		break;
	}

	m_paso = paso;

	char num[2];
	itoa(m_paso, num, 10);
	this->SetWindowText(CString(AfxGetAppName()) + CString(" :: Paso ") + CString(num));
}

void CCLINDlg::OnBtnSalir() 
{
	// TODO: Add your control notification handler code here
	
	EndDialog(IDOK);
}

void CCLINDlg::OnTimer(UINT nIDEvent) 
{
	// TODO: Add your message handler code here and/or call default

	if ( nIDEvent == 69 ) {

		if ( thrPaso4->Terminado() ) {
			KillTimer(69);

			if ( !thrPaso4->GetError() ) {
				CWnd::MessageBox("Inicializaci�n termin� con �xito\nPulse Aceptar", "CLIN :: Finalizado",MB_ICONINFORMATION);
				OnBtnsiguiente();
			} else {
				CWnd::MessageBox(thrPaso4->GetMsgError().GetBuffer(0), "CLIN :: ERROR", MB_ICONERROR);
				SetPaso(1);
			}
		}
	}
	
	CDialog::OnTimer(nIDEvent);
}
