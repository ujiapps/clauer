// DlgPaso2.cpp : implementation file
//

#include "stdafx.h"
#include "CLIN.h"
#include "DlgPaso2.h"
#include "CLINDlg.h"

#include "Excepciones.h"

#include <CRYPTOWrapper/CRYPTOWrap.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CDlgPaso2 dialog


CDlgPaso2::CDlgPaso2(CWnd* pParent /*=NULL*/)
	: CDialog(CDlgPaso2::IDD, pParent)
{
	//{{AFX_DATA_INIT(CDlgPaso2)
	m_editPIN = _T("");
	m_chkOmitirDNIs = FALSE;
	//}}AFX_DATA_INIT
}


void CDlgPaso2::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CDlgPaso2)
	DDX_Control(pDX, IDC_LBL_DESCRIPCION, m_lblDescripcion);
	DDX_Text(pDX, IDC_EDIT_PIN, m_editPIN);
	DDX_Check(pDX, IDC_CHECK_OMITIR_DNIS, m_chkOmitirDNIs);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CDlgPaso2, CDialog)
	//{{AFX_MSG_MAP(CDlgPaso2)
	ON_WM_SHOWWINDOW()
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CDlgPaso2 message handlers

BOOL CDlgPaso2::PreTranslateMessage(MSG* pMsg) 
{
	// TODO: Add your specialized code here and/or call the base class
	if(pMsg->message==WM_KEYDOWN)
	{
		switch (pMsg->wParam) {

		case VK_ESCAPE:
			pMsg->wParam=NULL ;
			break;

		case VK_RETURN:

			((CCLINDlg *) this->GetParent())->OnBtnsiguiente();
			pMsg->wParam = NULL;
			return TRUE;

			break;

		case VK_TAB:
			((CCLINDlg *) this->GetParent())->SetFocus();
			pMsg->wParam=NULL;
			break;

		}
	}

	return CDialog::PreTranslateMessage(pMsg);

}
	


BOOL CDlgPaso2::OnInitDialog() 
{
	CDialog::OnInitDialog();
	
	// TODO: Add extra initialization here

/*	CFont *font = new CFont;
	font->CreatePointFont(12, "System");
	m_lblDescripcion.SetFont(font);
	delete font;
*/	
	return TRUE;  // return TRUE unless you set the focus to a control
	              // EXCEPTION: OCX Property Pages should return FALSE
}




CString CDlgPaso2::GetPIN()
{

	UpdateData(TRUE);
	return m_editPIN;

}



void CDlgPaso2::OnShowWindow(BOOL bShow, UINT nStatus) 
{
	CDialog::OnShowWindow(bShow, nStatus);
	
	// TODO: Add your message handler code here

	if ( bShow ) {
		m_editPIN.Empty();
		m_chkOmitirDNIs = FALSE;
		UpdateData(FALSE);
	}
	
}



void CDlgPaso2::DestruirPIN (void)
{
	UpdateData();
	
	for ( int i = 0 ; i < m_editPIN.GetLength() ; i++ )
		m_editPIN.SetAt(i, '0');

	m_editPIN="";

}

BOOL CDlgPaso2::GetOmitir()
{
	UpdateData(TRUE);
	return this->m_chkOmitirDNIs;

}
