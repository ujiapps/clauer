#if !defined(AFX_DLGPASO2_H__B8D15E5B_3784_4A59_B603_0062116C279E__INCLUDED_)
#define AFX_DLGPASO2_H__B8D15E5B_3784_4A59_B603_0062116C279E__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// DlgPaso2.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CDlgPaso2 dialog

class CDlgPaso2 : public CDialog
{
// Construction
public:
	BOOL GetOmitir(void);
	CString GetPIN(void);
	CDlgPaso2(CWnd* pParent = NULL);   // standard constructor

	void DestruirPIN (void);



// Dialog Data
	//{{AFX_DATA(CDlgPaso2)
	enum { IDD = IDD_DIALOG_PASO_2 };
	CStatic	m_lblDescripcion;
	CString	m_editPIN;
	BOOL	m_chkOmitirDNIs;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CDlgPaso2)
	public:
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CDlgPaso2)
	virtual BOOL OnInitDialog();
	afx_msg void OnShowWindow(BOOL bShow, UINT nStatus);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DLGPASO2_H__B8D15E5B_3784_4A59_B603_0062116C279E__INCLUDED_)
