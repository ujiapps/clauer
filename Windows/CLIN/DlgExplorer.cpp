// DlgExplorer.cpp : implementation file
//

#include "stdafx.h"
#include "CLIN.h"
#include "DlgExplorer.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#undef THIS_FILE
static char THIS_FILE[] = __FILE__;
#endif

/////////////////////////////////////////////////////////////////////////////
// CDlgExplorer dialog


CDlgExplorer::CDlgExplorer(CWnd* pParent /*=NULL*/)
	: CDialog(CDlgExplorer::IDD, pParent)
{
	//{{AFX_DATA_INIT(CDlgExplorer)
		// NOTE: the ClassWizard will add member initialization here
	//}}AFX_DATA_INIT
}


void CDlgExplorer::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	//{{AFX_DATA_MAP(CDlgExplorer)
	DDX_Control(pDX, IDC_EXPLORER1, m_IE);
	//}}AFX_DATA_MAP
}


BEGIN_MESSAGE_MAP(CDlgExplorer, CDialog)
	//{{AFX_MSG_MAP(CDlgExplorer)
		// NOTE: the ClassWizard will add message map macros here
	//}}AFX_MSG_MAP
END_MESSAGE_MAP()

/////////////////////////////////////////////////////////////////////////////
// CDlgExplorer message handlers
