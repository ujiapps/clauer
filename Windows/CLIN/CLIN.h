// CLIN.h : main header file for the CLIN application
//

#if !defined(AFX_CLIN_H__5364E89F_80FF_479A_BAEE_EFAF60705FF7__INCLUDED_)
#define AFX_CLIN_H__5364E89F_80FF_479A_BAEE_EFAF60705FF7__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#ifndef __AFXWIN_H__
	#error include 'stdafx.h' before including this file for PCH
#endif

#include "resource.h"		// main symbols

/////////////////////////////////////////////////////////////////////////////
// CCLINApp:
// See CLIN.cpp for the implementation of this class
//

class CCLINApp : public CWinApp
{
public:
	CCLINApp();

// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CCLINApp)
	public:
	virtual BOOL InitInstance();
	//}}AFX_VIRTUAL

// Implementation

	//{{AFX_MSG(CCLINApp)
		// NOTE - the ClassWizard will add and remove member functions here.
		//    DO NOT EDIT what you see in these blocks of generated code !
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};


/////////////////////////////////////////////////////////////////////////////

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_CLIN_H__5364E89F_80FF_479A_BAEE_EFAF60705FF7__INCLUDED_)
