#if !defined(AFX_DLGPASO4_H__125B81EB_DEEC_44F8_B388_E310F8D8926F__INCLUDED_)
#define AFX_DLGPASO4_H__125B81EB_DEEC_44F8_B388_E310F8D8926F__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// DlgPaso4.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CDlgPaso4 dialog

class CDlgPaso4 : public CDialog
{
// Construction
public:
	void Reset(void);
	void Paso (void);
	void SetPaso (int paso);
	void SetDescripcion(CString descripcion);
	CDlgPaso4(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CDlgPaso4)
	enum { IDD = IDD_DIALOG_PASO_4 };
	CStatic	m_lblDescripcion_1;
	CStatic	m_lblDescripcion;
	CProgressCtrl	m_prgProgreso;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CDlgPaso4)
	public:
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CDlgPaso4)
	afx_msg void OnShowWindow(BOOL bShow, UINT nStatus);
	virtual BOOL OnInitDialog();
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DLGPASO4_H__125B81EB_DEEC_44F8_B388_E310F8D8926F__INCLUDED_)
