#if !defined(AFX_DLGPASO1_H__935B4090_DC4A_4D4A_9C5D_5BFC4D9EA9F4__INCLUDED_)
#define AFX_DLGPASO1_H__935B4090_DC4A_4D4A_9C5D_5BFC4D9EA9F4__INCLUDED_

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000
// DlgPaso1.h : header file
//

/////////////////////////////////////////////////////////////////////////////
// CDlgPaso1 dialog

class CDlgPaso1 : public CDialog
{
// Construction
public:
	BOOL OmitirBaseDatos();
	CString GetDNI(void);
	CDlgPaso1(CWnd* pParent = NULL);   // standard constructor

// Dialog Data
	//{{AFX_DATA(CDlgPaso1)
	enum { IDD = IDD_DIALOG_PASO_1 };
	CString	m_editDNI;
	BOOL	m_chkOmitir;
	//}}AFX_DATA


// Overrides
	// ClassWizard generated virtual function overrides
	//{{AFX_VIRTUAL(CDlgPaso1)
	public:
	virtual BOOL PreTranslateMessage(MSG* pMsg);
	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // DDX/DDV support
	//}}AFX_VIRTUAL

// Implementation
protected:

	// Generated message map functions
	//{{AFX_MSG(CDlgPaso1)
	virtual BOOL OnInitDialog();
	afx_msg void OnShowWindow(BOOL bShow, UINT nStatus);
	//}}AFX_MSG
	DECLARE_MESSAGE_MAP()
};

//{{AFX_INSERT_LOCATION}}
// Microsoft Visual C++ will insert additional declarations immediately before the previous line.

#endif // !defined(AFX_DLGPASO1_H__935B4090_DC4A_4D4A_9C5D_5BFC4D9EA9F4__INCLUDED_)
