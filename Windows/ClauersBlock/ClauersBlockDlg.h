// ClauersBlockDlg.h: archivo de encabezado
//

#pragma once
#include "afxwin.h"


// Cuadro de di�logo de CClauersBlockDlg
class CClauersBlockDlg : public CDialog
{
// Construcci�n
public:
	CClauersBlockDlg(CWnd* pParent = NULL);	// Constructor est�ndar

// Datos del cuadro de di�logo
	enum { IDD = IDD_CLAUERSBLOCK_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// Compatibilidad con DDX/DDV


// Implementaci�n
protected:
	HICON m_hIcon;

	// Funciones de asignaci�n de mensajes generadas
	virtual BOOL OnInitDialog();
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
public:

	afx_msg void OnBnClickedOk();
	afx_msg void OnBnClickedOk3();
	void ShowID();
	CString m_showID;
	CEdit m_showIDControl;
	CButton m_ButtonCrear;
	CButton m_ButtonBorrar;
	CButton m_ButtonShowID;
	CButton m_ButtonSalir;
	afx_msg void OnBnClickedOk2();
};
