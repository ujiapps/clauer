// ClauersBlockDlg.cpp: archivo de implementaci�n
//

#include "stdafx.h"
#include "ClauersBlock.h"
#include "ClauersBlockDlg.h"
#include ".\clauersblockdlg.h"

#include <LIBRT/LIBRT.h>
#include <libFormat/usb_format.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// Cuadro de di�logo de CClauersBlockDlg



CClauersBlockDlg::CClauersBlockDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CClauersBlockDlg::IDD, pParent)
	, m_showID(_T(""))
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CClauersBlockDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_EDIT1, m_showID);
	DDX_Control(pDX, IDC_EDIT1, m_showIDControl);
	DDX_Control(pDX, IDOK, m_ButtonCrear);
	DDX_Control(pDX, IDOK2, m_ButtonBorrar);
	DDX_Control(pDX, IDOK3, m_ButtonShowID);
	DDX_Control(pDX, IDCANCEL, m_ButtonSalir);
}

BEGIN_MESSAGE_MAP(CClauersBlockDlg, CDialog)
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	//}}AFX_MSG_MAP
	ON_BN_CLICKED(IDOK, OnBnClickedOk)
	ON_BN_CLICKED(IDOK3, OnBnClickedOk3)
	ON_BN_CLICKED(IDOK2, OnBnClickedOk2)
END_MESSAGE_MAP()


// Controladores de mensaje de CClauersBlockDlg

BOOL CClauersBlockDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// Establecer el icono para este cuadro de di�logo. El marco de trabajo realiza esta operaci�n
	//  autom�ticamente cuando la ventana principal de la aplicaci�n no es un cuadro de di�logo
	SetIcon(m_hIcon, TRUE);			// Establecer icono grande
	SetIcon(m_hIcon, FALSE);		// Establecer icono peque�o

	// TODO: agregar aqu� inicializaci�n adicional
	
	return TRUE;  // Devuelve TRUE  a menos que establezca el foco en un control
}

// Si agrega un bot�n Minimizar al cuadro de di�logo, necesitar� el siguiente c�digo
//  para dibujar el icono. Para aplicaciones MFC que utilicen el modelo de documentos y vistas,
//  esta operaci�n la realiza autom�ticamente el marco de trabajo.

void CClauersBlockDlg::OnPaint() 
{
	if (IsIconic())
	{
		CPaintDC dc(this); // Contexto de dispositivo para dibujo

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Centrar icono en el rect�ngulo de cliente
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Dibujar el icono
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

// El sistema llama a esta funci�n para obtener el cursor que se muestra mientras el usuario arrastra
//  la ventana minimizada.
HCURSOR CClauersBlockDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}

void CClauersBlockDlg::OnBnClickedOk()
{
	// TODO: Agregue aqu� su c�digo de controlador de notificaci�n de control
	// Crear Clauer

	int result;
	int soy;

	int numDispositivos;
	char ** unidades = NULL;
	int *dispositivos = NULL;

	char IDClauer[41];
	int i=0;
	int ndisp;
	BYTE * disp;
	USBCERTS_HANDLE h;


	m_showID="Iniciando ....";
	UpdateData(FALSE);

	m_ButtonCrear.EnableWindow(FALSE);
	m_ButtonBorrar.EnableWindow(FALSE);
	m_ButtonShowID.EnableWindow(FALSE);
	m_ButtonSalir.EnableWindow(FALSE);

	result = lowlevel_enumerar_dispositivos( NULL, NULL, &numDispositivos );

	if ( result != ERR_LOWLEVEL_NO ) {
		AfxMessageBox("ERRORO: Enumerando dispositivos.\n", MB_SYSTEMMODAL|MB_OK|MB_ICONINFORMATION);
		m_showID="ERROR";
		UpdateData(FALSE);
		goto finFormat;
	}

	if ( numDispositivos > 1 ) {
		AfxMessageBox("ERROR: Hay mas de un stick USB conectado.\n", MB_SYSTEMMODAL|MB_OK|MB_ICONINFORMATION);
		m_showID="ERROR";
		UpdateData(FALSE);
		goto finFormat;
	}else if ( numDispositivos == 0 ) {
		AfxMessageBox("ERROR: No se han detectado sticks USB.\n", MB_SYSTEMMODAL|MB_OK|MB_ICONINFORMATION);
		m_showID="ERROR";
		UpdateData(FALSE);
		goto finFormat;
	}

	m_showID="Enumerando dispositivos ....";
	UpdateData(FALSE);

	unidades = new char * [numDispositivos];

	for ( int j=0; j<numDispositivos; j++ ) 
		unidades[j] = new char[5];
			
	dispositivos = new int [numDispositivos];

	result = lowlevel_enumerar_dispositivos( unidades, dispositivos, &numDispositivos );

	if ( result != ERR_LOWLEVEL_NO ) {
		AfxMessageBox("ERROR: Enumerando dispositivos.\n", MB_SYSTEMMODAL|MB_OK|MB_ICONINFORMATION);
		m_showID="ERROR";
		UpdateData(FALSE);
		goto finFormat;
	}

	m_showID="Comprovando permisos ....";
	UpdateData(FALSE);

	result = lowlevel_soy_admin( dispositivos[0], &soy );
	if ( result != ERR_LOWLEVEL_NO ) {
		AfxMessageBox("Ocurrio un error comprobando si eres administrador.\n", MB_SYSTEMMODAL|MB_OK|MB_ICONINFORMATION);
		m_showID="ERROR";
		UpdateData(FALSE);
		goto finFormat;
	}
	if (!soy)
	{
		AfxMessageBox("Debes ser administrador para crear un Clauer.\n", MB_SYSTEMMODAL|MB_OK|MB_ICONINFORMATION);
		m_showID="ERROR";
		UpdateData(FALSE);
		goto finFormat;
	}

	m_showID="Formatenado ....";
	UpdateData(FALSE);

	result=format_crear_clauer(dispositivos[0],unidades[0],"mediateca",99);

	if ( result != ERR_FORMAT_NO ) {
		AfxMessageBox("Ocurrio un error creando el Clauer.\n", MB_SYSTEMMODAL|MB_OK|MB_ICONINFORMATION);
		m_showID="ERROR";
		UpdateData(FALSE);
		goto finFormat;
	}

	m_showID="Obteniendo identificador del Clauer ....";
	UpdateData(FALSE);

	LIBRT_Ini();
	if(LIBRT_HayRuntime()){
		LIBRT_ListarDispositivos(&ndisp,&disp);
		if(ndisp==1){
			LIBRT_IniciarDispositivo(disp[0],NULL,&h);
			for(i=0;i<20;i++){
				sprintf((IDClauer)+2*i,"%02x",h.idDispositivo[i]);
			}
			LIBRT_FinalizarDispositivo(&h);
			m_showID=IDClauer;
			UpdateData(FALSE);
		}
	}else{
		m_showID="ERROR";
		UpdateData(FALSE);
		AfxMessageBox("Debes de tener instalado el software base.\n", MB_SYSTEMMODAL|MB_OK|MB_ICONINFORMATION);
		goto finFormat;
	}

	LIBRT_Fin();
		
	goto finFormat;

finFormat:

	m_ButtonCrear.EnableWindow(TRUE);
	m_ButtonBorrar.EnableWindow(TRUE);
	m_ButtonShowID.EnableWindow(TRUE);
	m_ButtonSalir.EnableWindow(TRUE);

}

void CClauersBlockDlg::OnBnClickedOk3()
{
	// TODO: Agregue aqu� su c�digo de controlador de notificaci�n de control
	// Mostrar Id del Clauer

	char IDClauer[41];
	int i=0;
	int ndisp;
	BYTE * disp;
	USBCERTS_HANDLE h;

	m_ButtonCrear.EnableWindow(FALSE);
	m_ButtonBorrar.EnableWindow(FALSE);
	m_ButtonShowID.EnableWindow(FALSE);
	m_ButtonSalir.EnableWindow(FALSE);

	m_showID="Obteniendo identificador del Clauer ....";
	UpdateData(FALSE);

	LIBRT_Ini();
	if(LIBRT_HayRuntime()){
		LIBRT_ListarDispositivos(&ndisp,&disp);
		if(ndisp==1){
			LIBRT_IniciarDispositivo(disp[0],NULL,&h);
			for(i=0;i<20;i++){
				sprintf((IDClauer)+2*i,"%02x",h.idDispositivo[i]);
			}
			LIBRT_FinalizarDispositivo(&h);
			m_showID=IDClauer;
			UpdateData(FALSE);
		}else if(ndisp==0){
			AfxMessageBox("Debe introducir un Clauer.\n", MB_SYSTEMMODAL|MB_OK|MB_ICONINFORMATION);
			m_showID="ERROR";
			UpdateData(FALSE);
		}else if(ndisp>1){
			AfxMessageBox("ERROR: Hay mas de un Clauer conectado.\n", MB_SYSTEMMODAL|MB_OK|MB_ICONINFORMATION);
			m_showID="ERROR";
			UpdateData(FALSE);
		}else{
			AfxMessageBox("Ocurrio un error detectando el Clauer.\n", MB_SYSTEMMODAL|MB_OK|MB_ICONINFORMATION);
			m_showID="ERROR";
			UpdateData(FALSE);
		}
	}else{
		AfxMessageBox("Debes de tener instalado el software base.\n", MB_SYSTEMMODAL|MB_OK|MB_ICONINFORMATION);
		m_showID="ERROR";
		UpdateData(FALSE);
	}
	LIBRT_Fin();
	m_ButtonCrear.EnableWindow(TRUE);
	m_ButtonBorrar.EnableWindow(TRUE);
	m_ButtonShowID.EnableWindow(TRUE);
	m_ButtonSalir.EnableWindow(TRUE);
}

void CClauersBlockDlg::OnBnClickedOk2()
{
	// TODO: Agregue aqu� su c�digo de controlador de notificaci�n de control
	// Eliminar Clauer

	int result;
	int soy;

	int numDispositivos;
	char ** unidades = NULL;
	int *dispositivos = NULL;

	int i=0;

	m_showID="Iniciando ....";
	UpdateData(FALSE);

	m_ButtonCrear.EnableWindow(FALSE);
	m_ButtonBorrar.EnableWindow(FALSE);
	m_ButtonShowID.EnableWindow(FALSE);
	m_ButtonSalir.EnableWindow(FALSE);

	result = lowlevel_enumerar_dispositivos( NULL, NULL, &numDispositivos );

	if ( result != ERR_LOWLEVEL_NO ) {
		AfxMessageBox("ERRORO: Enumerando dispositivos.\n", MB_SYSTEMMODAL|MB_OK|MB_ICONINFORMATION);
		m_showID="ERROR";
		UpdateData(FALSE);
		goto finFormat;
	}

	if ( numDispositivos > 1 ) {
		AfxMessageBox("ERROR: Hay mas de un stick USB conectado.\n", MB_SYSTEMMODAL|MB_OK|MB_ICONINFORMATION);
		m_showID="ERROR";
		UpdateData(FALSE);
		goto finFormat;
	}else if ( numDispositivos == 0 ) {
		AfxMessageBox("ERROR: No se han detectado sticks USB.\n", MB_SYSTEMMODAL|MB_OK|MB_ICONINFORMATION);
		m_showID="ERROR";
		UpdateData(FALSE);
		goto finFormat;
	}

	m_showID="Enumerando dispositivos ....";
	UpdateData(FALSE);

	unidades = new char * [numDispositivos];

	for ( int j=0; j<numDispositivos; j++ ) 
		unidades[j] = new char[5];
			
	dispositivos = new int [numDispositivos];

	result = lowlevel_enumerar_dispositivos( unidades, dispositivos, &numDispositivos );

	if ( result != ERR_LOWLEVEL_NO ) {
		AfxMessageBox("ERROR: Enumerando dispositivos.\n", MB_SYSTEMMODAL|MB_OK|MB_ICONINFORMATION);
		m_showID="ERROR";
		UpdateData(FALSE);
		goto finFormat;
	}

	m_showID="Comprovando permisos ....";
	UpdateData(FALSE);

	result = lowlevel_soy_admin( dispositivos[0], &soy );
	if ( result != ERR_LOWLEVEL_NO ) {
		AfxMessageBox("Ocurrio un error comprobando si eres administrador.\n", MB_SYSTEMMODAL|MB_OK|MB_ICONINFORMATION);
		m_showID="ERROR";
		UpdateData(FALSE);
		goto finFormat;
	}
	if (!soy)
	{
		AfxMessageBox("Debes ser administrador para crear un Clauer.\n", MB_SYSTEMMODAL|MB_OK|MB_ICONINFORMATION);
		m_showID="ERROR";
		UpdateData(FALSE);
		goto finFormat;
	}

	m_showID="Formatenado ....";
	UpdateData(FALSE);

	result=format_crear_clauer(dispositivos[0],unidades[0],"mediateca",100);

	if ( result != ERR_FORMAT_NO ) {
		AfxMessageBox("Ocurrio un error creando el Clauer.\n", MB_SYSTEMMODAL|MB_OK|MB_ICONINFORMATION);
		m_showID="ERROR";
		UpdateData(FALSE);
		goto finFormat;
	}

	m_showID="Clauer eliminadado";
	UpdateData(FALSE);
		
	goto finFormat;

finFormat:

	m_ButtonCrear.EnableWindow(TRUE);
	m_ButtonBorrar.EnableWindow(TRUE);
	m_ButtonShowID.EnableWindow(TRUE);
	m_ButtonSalir.EnableWindow(TRUE);
}
