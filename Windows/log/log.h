#ifndef LOG_H
#define LOG_H

#include <windows.h>

#include <stdio.h>
#include <string.h>
#include <stdlib.h>


#ifdef __cplusplus
extern "C" {
#endif

typedef struct LOG_HANDLE
{
	LPTSTR nombreFichero;
	FILE *fpFichero;
} LOG_HANDLE;



BOOL LOG_Nuevo (IN LPTSTR nombreFichero, OUT LOG_HANDLE *logHandle);
BOOL LOG_Escribir(IN LOG_HANDLE logHandle, IN LPTSTR info);
BOOL LOG_Fin (IN OUT LOG_HANDLE *logHandle);


#ifdef __cplusplus
}
#endif



#endif
