#include "log.h"



BOOL LOG_Nuevo (IN LPTSTR nombreFichero, OUT LOG_HANDLE *logHandle)
{
	
/*	logHandle->fpFichero = fopen(nombreFichero, "a");
	
	if ( !logHandle->fpFichero )
		return FALSE;
*/
	logHandle->nombreFichero = (LPTSTR) malloc (sizeof(CHAR)*(strlen(nombreFichero)+1));
	strcpy(logHandle->nombreFichero, nombreFichero);
	
	return TRUE;
}


BOOL LOG_Escribir(IN LOG_HANDLE logHandle, IN LPTSTR info)
{
	FILE *fp = NULL;

	fp = fopen(logHandle.nombreFichero, "a");

	if ( !fp ) return FALSE;

	fwrite((void *)info,1,strlen(info)*sizeof(CHAR),fp);

	fclose(fp);
	fp = NULL;

	return TRUE;
}



BOOL LOG_Fin (IN LOG_HANDLE *logHandle)
{
	/*fclose(logHandle->fpFichero);*/
	free(logHandle->nombreFichero);
	/*logHandle->fpFichero = NULL;*/
	logHandle->nombreFichero = NULL;

	return TRUE;
}


