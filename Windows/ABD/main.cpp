#include <windows.h>

#include <iostream>

#include <BaseDatos/BaseDatos.hpp>
#include <BaseDatos/EBaseDatos.hpp>



int main (int argc, char **argv)
{
	CBaseDatos *bd = NULL;
	BYTE id[20];
	int nDisp = 0;
	string dni, email;


	try {

		cout << "Actualizador de la Base de Datos - Projecte Clauer'2005" << endl;
		cout << "by YaCe (Yet Another Computer Engineer) :: NISU" << endl << endl;

		memset(id, 0, 20);

		try {
			bd = new CBaseDatos();
		} 
		catch (EBaseDatos &e) {
			cerr << "[ERROR] OCURRI� un ERROR creando objeto de acceso a Base de Datos: " << endl;
			cerr << e << endl;
			return 1;
		}

		while ( 1 ) {
			
			cout << "NIF del personaje (RETURN para salir): " << flush;
			cin >> dni;

			if ( dni.length() == 0 ) 
				continue;

			bool tiene;

			try {
				tiene = bd->TieneStick(dni, email);
			}
			catch ( EBaseDatos &e ) {
				cerr << "[ERROR] Ocurri� un error comprobando si el usuario "
					 << dni 
					 << " tiene stick o no"
					 << endl
					 << e
					 << endl;

				continue;
			}


			if ( tiene ) {

				cout << "[ATENCI�N] El usuario ya estaba dado de alta en la base de datos" 
					 << endl;

			} else {

				try {
					bd->RegistrarStick (dni, id);
				}
				catch (EBaseDatos &e) {
					cerr << "[ERROR] Registrando stick"
						 << endl
						 << e
						 << endl;

					continue;
				}

				cout << "[NA] Usuario registrado correctamente" << endl;
			}

		}

	} catch (...) {
		cerr << "[ERROR] Ocurri� un error inesperado. A la porra..." << endl;
		return 1;
	}


	return 0;

}

