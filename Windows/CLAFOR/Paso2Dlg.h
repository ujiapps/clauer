#pragma once
#include "afxcmn.h"


// Cuadro de di�logo de CPaso2Dlg

class CPaso2Dlg : public CDialog
{
	DECLARE_DYNAMIC(CPaso2Dlg)

public:
	CPaso2Dlg(CWnd* pParent = NULL);   // Constructor est�ndar
	virtual ~CPaso2Dlg();

// Datos del cuadro de di�logo
	enum { IDD = IDD_DIALOG_PASO_2 };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // Compatibilidad con DDX o DDV

	DECLARE_MESSAGE_MAP()
public:
	// Hace que se incremente el paso de la barra de progreso
	void Paso(void);
protected:
	// La barra de progreso
	CProgressCtrl m_progreso;
public:
	// Establece el tama�o del paso
	void SetPaso(int paso);
	BOOL OnInitDialog(void);
	// La descripci�n del paso
	CString m_strDescripcion;
	// El label PASO 2
	CString m_strPaso2;
};
