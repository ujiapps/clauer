// Paso1Dlg.cpp: archivo de implementación
//

#include "stdafx.h"
#include "CLAFOR.h"
#include "Paso1Dlg.h"


// Cuadro de diálogo de CPaso1Dlg

IMPLEMENT_DYNAMIC(CPaso1Dlg, CDialog)
CPaso1Dlg::CPaso1Dlg(CWnd* pParent /*=NULL*/)
	: CDialog(CPaso1Dlg::IDD, pParent)
	, m_strDescrPaso1(_T(""))
	, m_strLblPaso1(_T(""))
	, m_strLblPaso2(_T(""))
	, m_strLblPaso(_T(""))
{
}

CPaso1Dlg::~CPaso1Dlg()
{
}

void CPaso1Dlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_LBL_DESCR_PASO_1, m_strDescrPaso1);
	DDX_Text(pDX, IDC_LBL_PASO_1_PASO_1, m_strLblPaso1);
	DDX_Text(pDX, IDC_LBL_PASO_2_PASO_1, m_strLblPaso2);
	DDX_Text(pDX, IDC_LBL_PASO_1, m_strLblPaso);
}


BEGIN_MESSAGE_MAP(CPaso1Dlg, CDialog)
END_MESSAGE_MAP()


// Controladores de mensajes de CPaso1Dlg
BOOL CPaso1Dlg::OnInitDialog()
{	
	m_strDescrPaso1.LoadString(IDS_PASO1_DESCRIPCION);
	m_strLblPaso1.LoadString(IDS_PASO1_PASO_1);
	m_strLblPaso2.LoadString(IDS_PASO1_PASO_2);
	m_strLblPaso.LoadString(IDS_PASO);
	m_strLblPaso += _T(" 1");

	UpdateData(FALSE);

	return TRUE;
}
