// CLAFOR.cpp : define los comportamientos de las clases para la aplicaci�n.
//

#include "stdafx.h"
#include "CLAFOR.h"
#include "CLAFORDlg.h"
#include ".\clafor.h"

#include <LIBRT/LIBRT.h>
#include <CRYPTOWrapper/CRYPTOWrap.h>

#ifdef _DEBUG
#define new DEBUG_NEW
#endif


// CCLAFORApp

BEGIN_MESSAGE_MAP(CCLAFORApp, CWinApp)
	ON_COMMAND(ID_HELP, CWinApp::OnHelp)
END_MESSAGE_MAP()


// Construcci�n de CCLAFORApp

CCLAFORApp::CCLAFORApp()
{
	// TODO: agregar aqu� el c�digo de construcci�n,
	// Colocar toda la inicializaci�n importante en InitInstance
}


// El �nico objeto CCLAFORApp

CCLAFORApp theApp;


// Inicializaci�n de CCLAFORApp

BOOL CCLAFORApp::InitInstance()
{
	// Windows XP requiere InitCommonControls() si un manifiesto de 
	// aplicaci�n especifica el uso de ComCtl32.dll versi�n 6 o posterior para habilitar
	// estilos visuales. De lo contrario, se generar� un error al crear ventanas.

	InitCommonControls();

	CWinApp::InitInstance();

	if (!AfxSocketInit())
	{
		AfxMessageBox(IDP_SOCKETS_INIT_FAILED);
		return FALSE;
	}

	LIBRT_Ini();
	CRYPTO_Ini();

	if ( ! LIBRT_HayRuntime () ) {
		CString msg, caption;

		msg.LoadString(IDS_ERROR_NO_CLAUER_BASE);
		caption.LoadString(IDS_ERROR_CAPTION);

		MessageBox(NULL, msg.GetBuffer(0), caption.GetBuffer(0), MB_ICONERROR|MB_OK);

		return FALSE;
	}

	CCLAFORDlg dlg;
	m_pMainWnd = &dlg;
	INT_PTR nResponse = dlg.DoModal();
	if (nResponse == IDOK)
	{
		// TODO: insertar aqu� el c�digo para controlar 
		//  cu�ndo se descarta el cuadro de di�logo con Aceptar
	}
	else if (nResponse == IDCANCEL)
	{
		// TODO: insertar aqu� el c�digo para controlar 
		//  cu�ndo se descarta el cuadro de di�logo con Cancelar
	}

	// Dado que el cuadro de di�logo se ha cerrado, devolver FALSE para salir
	//  de la aplicaci�n en vez de iniciar el suministro de mensajes de dicha aplicaci�n.
	return FALSE;
}

