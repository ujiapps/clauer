// DialogPIN.cpp: archivo de implementación
//

#include "stdafx.h"
#include "CLAFOR.h"
#include "DialogPIN.h"
#include ".\dialogpin.h"

#include <LIBRT/libRT.h>


// Cuadro de diálogo de CDialogPIN

IMPLEMENT_DYNAMIC(CDialogPIN, CDialog)
CDialogPIN::CDialogPIN(CWnd* pParent /*=NULL*/)
	: CDialog(CDialogPIN::IDD, pParent)
	, m_strPIN(_T(""))
{
}

CDialogPIN::CDialogPIN(CString strDevice, CWnd *pParent /*= NULL*/)
:CDialog(CDialogPIN::IDD, pParent)
	, m_strDevice(strDevice)
	, m_strPIN(_T(""))
{
}

CDialogPIN::~CDialogPIN()
{
}

void CDialogPIN::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_EDIT_PIN, m_strPIN);
}


BEGIN_MESSAGE_MAP(CDialogPIN, CDialog)
	ON_BN_CLICKED(IDOK, OnBnClickedOk)
END_MESSAGE_MAP()


// Machaca el contenido del string

void CDialogPIN::DestruirPIN(void)
{
	m_strPIN.Empty();
	UpdateData(TRUE);
}

void CDialogPIN::OnBnClickedOk()
{
	/* Si el PIN es incorrecto, no salimos
	 */

	USBCERTS_HANDLE hClauer;

	UpdateData();

	if ( LIBRT_IniciarDispositivo((unsigned char *) m_strDevice.GetString(), m_strPIN.GetBuffer(), &hClauer) != 0 ) {
		m_strPIN.Empty();
		UpdateData(FALSE);
		AfxMessageBox(IDS_REINTENTAR, MB_OK|MB_ICONWARNING);
	} else {
		LIBRT_FinalizarDispositivo(&hClauer);
		OnOK();
	}

}


