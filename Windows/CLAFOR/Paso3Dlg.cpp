// Paso3Dlg.cpp: archivo de implementación
//

#include "stdafx.h"
#include "CLAFOR.h"
#include "Paso3Dlg.h"
#include ".\paso3dlg.h"


// Cuadro de diálogo de CPaso3Dlg

IMPLEMENT_DYNAMIC(CPaso3Dlg, CDialog)
CPaso3Dlg::CPaso3Dlg(CWnd* pParent /*=NULL*/)
	: CDialog(CPaso3Dlg::IDD, pParent)
	, m_strLblPaso3(_T(""))
	, m_strLblDescrPaso3(_T(""))
	, m_strLblPaso1Paso3(_T(""))
	, m_strLblPaso2Paso3(_T(""))
{
}

CPaso3Dlg::~CPaso3Dlg()
{
}

void CPaso3Dlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Text(pDX, IDC_LBL_PASO3, m_strLblPaso3);
	DDX_Text(pDX, IDC_LBL_DESCR_PASO_3, m_strLblDescrPaso3);
	DDX_Text(pDX, IDC_LBL_PASO_1_PASO_3, m_strLblPaso1Paso3);
	DDX_Text(pDX, IDC_LBL_PASO_2_PASO_3, m_strLblPaso2Paso3);
}


BEGIN_MESSAGE_MAP(CPaso3Dlg, CDialog)
END_MESSAGE_MAP()


// Controladores de mensajes de CPaso3Dlg

BOOL CPaso3Dlg::OnInitDialog(void)
{

	m_strLblPaso3.LoadString(IDS_PASO);
	m_strLblPaso3 += _T(" 3");
	m_strLblDescrPaso3.LoadString(IDS_PASO3_DESCRIPCION);
	m_strLblPaso1Paso3.LoadString(IDS_PASO3_PASO_1);
	m_strLblPaso2Paso3.LoadString(IDS_PASO3_PASO_2);
	UpdateData(FALSE);

	return TRUE;
}
