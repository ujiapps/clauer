#pragma once


// Cuadro de di�logo de CPaso3Dlg

class CPaso3Dlg : public CDialog
{
	DECLARE_DYNAMIC(CPaso3Dlg)

public:
	CPaso3Dlg(CWnd* pParent = NULL);   // Constructor est�ndar
	virtual ~CPaso3Dlg();

// Datos del cuadro de di�logo
	enum { IDD = IDD_DIALOG_PASO_3 };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // Compatibilidad con DDX o DDV

	DECLARE_MESSAGE_MAP()
public:
	CString m_strLblPaso3;
	CString m_strLblDescrPaso3;
	CString m_strLblPaso1Paso3;
	CString m_strLblPaso2Paso3;
	BOOL OnInitDialog(void);
};
