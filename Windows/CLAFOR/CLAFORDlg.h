// CLAFORDlg.h: archivo de encabezado
//

#pragma once
#include "paso1dlg.h"
#include "paso2dlg.h"
#include "paso3dlg.h"


// Cuadro de di�logo de CCLAFORDlg
class CCLAFORDlg : public CDialog
{
// Construcci�n
public:
	CCLAFORDlg(CWnd* pParent = NULL);	// Constructor est�ndar

// Datos del cuadro de di�logo
	enum { IDD = IDD_CLAFOR_DIALOG };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);	// Compatibilidad con DDX/DDV


// Implementaci�n
protected:
	HICON m_hIcon;

	// Funciones de asignaci�n de mensajes generadas
	virtual BOOL OnInitDialog();
	afx_msg void OnSysCommand(UINT nID, LPARAM lParam);
	afx_msg void OnPaint();
	afx_msg HCURSOR OnQueryDragIcon();
	DECLARE_MESSAGE_MAP()
protected:
	// El primer paso
	CPaso1Dlg m_dlgPaso1;
	// Di�logo del paso 2
	CPaso2Dlg m_dlgPaso2;
	// Actualiza la interfaz de usuario al paso especificado
	void SetPaso(int paso);
	// El paso actual en el que nos encontramos
	int m_paso;
public:
	afx_msg void OnBnClickedBtnSiguiente();
protected:
	// Di�logo del paso 3
	CPaso3Dlg m_dlgPaso3;
public:
	afx_msg void OnBnClickedButtonSalir();
protected:
	// El bot�n siguiente
	CButton m_btnSiguiente;
public:
	void SoyElThreadYHeAcabado(void);
	afx_msg void OnTimer(UINT nIDEvent);
};
