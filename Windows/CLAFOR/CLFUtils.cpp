#include <stdafx.h>

#include <wincrypt.h>
#include "Error.h"

#include <CRYPTOWrapper/CRYPTOWrap.h>
#include "DialogPIN.h"


int CLAFUTILS_Clauer_Ok ( USBCERTS_HANDLE *hClauer, 
						  INFO_KEY_CONTAINER kc, 
						  bool signature,
						  bool & arreglar )
{
	unsigned char bloque[TAM_BLOQUE], noid[TAM_BLOQUE];
	bool certEncontrado = false, usoEsperadoSignature = false;
	unsigned char *idKc;
	long nBloque;

	try {

		ZeroMemory((void *) noid, TAM_BLOQUE);

		if ( signature )
			idKc = (unsigned char *) kc.idSignature;
		else
			idKc = (unsigned char *) kc.idExchange;
	
		/* 
			Si no tiene llave asociada del tipo especificado, pues
			entonces no hay que arreglar nada
		 */

		arreglar = false;

		if ( memcmp((const void *) idKc, noid, 20 ) == 0 ) 
			return 0;

		/* 
		   1. Busco el certificado asociado
		   2. Compruebo que el uso del certificado es v�lido para el tipo
		 	  de llave especificado
		*/

		if ( LIBRT_LeerTipoBloqueCrypto(hClauer, BLOQUE_CERT_PROPIO, TRUE, bloque, &nBloque) != 0 ) 
			throw 1;

		while ( !certEncontrado && (nBloque != -1) ) {

			if ( memcmp((const void *) BLOQUE_CERTPROPIO_Get_Id(bloque), 
				        (const void *) idKc,
						20 ) == 0 ) 
			{
				certEncontrado = true;

			} else {
				if ( LIBRT_LeerTipoBloqueCrypto(hClauer, BLOQUE_CERT_PROPIO, FALSE, bloque, &nBloque) != 0 ) 
					throw 1;
			}
		}

		if ( certEncontrado ) {

			unsigned long keyUsage;

			/* 
			    Si nos ha pedido AT_SIGNATURE el uso correcto del certificado es
			    DIGITAL_SIGNATURE, KEY_CERT_SIGN, KU_CRL_SIGN
			 */

			if ( CRYPTO_X509_Get_KeyUsage ( BLOQUE_CERTPROPIO_Get_Objeto(bloque), 
										    BLOQUE_CERTPROPIO_Get_Tam(bloque),
										    &keyUsage) )
			{
				throw 1;
			}

			if ( ( keyUsage & CRYPTO_KU_DIGITAL_SIGNATURE ) || 
 				 ( keyUsage & CRYPTO_KU_KEY_CERT_SIGN ) ||
				 ( keyUsage & CRYPTO_KU_CRL_SIGN ) )
			{
				usoEsperadoSignature = true;
			}

			if ( ( keyUsage & CRYPTO_KU_KEY_ENCIPHERMENT ) ||
				 ( keyUsage & CRYPTO_KU_DATA_ENCIPHERMENT ) ||
				 ( keyUsage & CRYPTO_KU_KEY_AGREEMENT ) ||
				 ( keyUsage & CRYPTO_KU_ENCIPHER_ONLY ) ||
				 ( keyUsage & CRYPTO_KU_DECIPHER_ONLY ) )
			{
				usoEsperadoSignature = false;
			}

			if ( signature && ! usoEsperadoSignature )
				arreglar = true;
			else
				arreglar = false;
		}

	}
	catch ( int i ) {
		SecureZeroMemory(bloque, TAM_BLOQUE);
		return i;
	}
	catch ( ... ) {
		return 1;
	}
	
	return 0;
}



int CLAFUTILS_Arreglar ( USBCERTS_HANDLE *hClauer,
						 BYTE bloqueContainer[TAM_BLOQUE],
						 long nBloqueContainer,
						 INFO_KEY_CONTAINER kc,
						 bool arreglarSignature )
{
	CDialogPIN dlg;
	BYTE bloque[TAM_BLOQUE];
	long nBloque, uSig, uEx;
	unsigned char *id, idEx[20], idSig[20];
	ALG_ID algId;
	int intentos;

	try {

		/* 
		 * Si el handle no es autenticado, establecemos una sesi�n autenticada
		 */

		if ( ! hClauer->auth ) {

			intentos = 0;
			while (1) {

				if ( dlg.DoModal() == IDCANCEL ) 
					throw ERR_CANCEL;

				if ( intentos == 0 )
					if ( LIBRT_FinalizarDispositivo(hClauer) != 0 ) 
						throw ERR_UNKNOWN;
				
				unsigned char *disp[MAX_DEVICES];
				int nDisp;

				do {

					if ( LIBRT_ListarDispositivos(&nDisp, disp) != 0 ) {
						AfxMessageBox(IDS_ERROR_ENUMERACION, MB_ICONERROR|MB_OK, 0);
						throw 1;
					}

					if ( nDisp == 0 ) {
						if ( AfxMessageBox(IDS_WARN_NO_CLAUERS, MB_ICONWARNING|MB_OKCANCEL, 0) == IDCANCEL )
							throw ERR_CANCEL;
					} else if ( nDisp > 1 ) {

						for ( int k = 0 ; k < nDisp ; k++ )
							free(disp[0]);
						if ( AfxMessageBox(IDS_WARN_MAS_DE_UN_CLAUER, MB_ICONWARNING|MB_OKCANCEL, 0) == IDCANCEL ) 
							throw ERR_CANCEL;
						
					} else {
						break;
					}

				} while ( 1 );

				if ( LIBRT_IniciarDispositivo(disp[0], dlg.GetPIN().GetBuffer(0), hClauer) != 0 ) {
					if ( 3 == intentos )
						throw 1;
					else {
						AfxMessageBox(IDS_REINTENTAR, MB_OK|MB_ICONWARNING);
						++intentos;
					}
				} else
					break;
			}
		}

		if ( arreglarSignature ) {
			algId = CALG_RSA_KEYX;
			id = kc.idSignature;
		} else {
			algId = CALG_RSA_SIGN;
			id = kc.idExchange;
		}

		/*
		 * Modificamos el blob
		 */

		if ( CLAFUTILS_Get_Blob (hClauer, id, bloque, nBloque) != 0 ) 
			throw 1;
		
		*((WORD *)(BLOQUE_PRIVKEYBLOB_Get_Objeto(bloque) + 2 + sizeof(WORD))) = algId;

		if ( LIBRT_EscribirBloqueCrypto(hClauer, nBloque, bloque) != 0 )
			throw 1;

		/*
		 * Modificamos el container
		 */

		BYTE *bKc;

		memcpy((void *) idEx, BLOQUE_KeyContainer_Get_ID_Exchange(bloqueContainer, kc.nombreKeyContainer), 20);
		memcpy((void *) idSig, BLOQUE_KeyContainer_Get_ID_Signature(bloqueContainer, kc.nombreKeyContainer), 20);
		
		bKc  = BLOQUE_KeyContainer_Buscar(bloqueContainer, kc.nombreKeyContainer);
		uSig = BLOQUE_KeyContainer_GetUnidadSignature(bKc);
		uEx  = BLOQUE_KeyContainer_GetUnidadExchange(bKc);

		BLOQUE_KeyContainer_EstablecerEXCHANGE      ( bloqueContainer, kc.nombreKeyContainer, uSig, FALSE );
		BLOQUE_KeyContainer_EstablecerSIGNATURE     ( bloqueContainer, kc.nombreKeyContainer, uEx, FALSE );
		BLOQUE_KeyContainer_Establecer_ID_Exchange  ( bloqueContainer, kc.nombreKeyContainer, idSig );
		BLOQUE_KeyContainer_Establecer_ID_Signature ( bloqueContainer, kc.nombreKeyContainer, idEx );

		if ( LIBRT_EscribirBloqueCrypto(hClauer, nBloqueContainer, bloqueContainer) != 0 ) 
			throw 1;

	}
	catch ( int i ) {
		return i;
	}
	catch ( ... ) {
		return 1;
	}

	return 0;
}


int CLAFUTILS_Get_Blob ( USBCERTS_HANDLE *hClauer,
						 unsigned char *id,
					     BYTE bloque[TAM_BLOQUE],
						 long & nBloque )
{

	bool encontrado = false;

	try {

		/* 
		 * Asumo que es autenticado
		 */

		if ( LIBRT_LeerTipoBloqueCrypto(hClauer, BLOQUE_PRIVKEY_BLOB, TRUE, bloque, &nBloque) != 0 ) 
			throw 1;

		while ( !encontrado && (nBloque != -1) ) {

			if ( memcmp((const void *) BLOQUE_PRIVKEYBLOB_Get_Id(bloque), 
				        (const void *) id,
						20 ) == 0 ) 
			{
				encontrado = true;

			} else {
				if ( LIBRT_LeerTipoBloqueCrypto(hClauer, BLOQUE_PRIVKEY_BLOB, FALSE, bloque, &nBloque) != 0 ) 
					throw 1;
			}
		}

	}
	catch ( int i ) {
		throw i;
	}
	catch ( ... ) {
		throw 1;
	}

	if ( encontrado )
		return 0;
	else
		return 1;
}

int CLAFUTILS_Get_Cert ( USBCERTS_HANDLE *hClauer,
						 unsigned char *id,
					     BYTE bloque[TAM_BLOQUE],
						 long & nBloque )
{

	bool encontrado = false;

	try {

		/* 
		 * Asumo que es autenticado
		 */

		if ( LIBRT_LeerTipoBloqueCrypto(hClauer, BLOQUE_CERT_PROPIO, TRUE, bloque, &nBloque) != 0 ) 
			throw 1;

		while ( !encontrado && (nBloque != -1) ) {

			if ( memcmp((const void *) BLOQUE_CERTPROPIO_Get_Id(bloque), 
				        (const void *) id,
						20 ) == 0 ) 
			{
			
				encontrado = true;

			} else {
				if ( LIBRT_LeerTipoBloqueCrypto(hClauer, BLOQUE_CERT_PROPIO, FALSE, bloque, &nBloque) != 0 ) 
					throw 1;
			}



		}

	}
	catch ( int i ) {
		throw i;
	}
	catch ( ... ) {
		throw 1;
	}

	if ( encontrado )
		return 0;
	else
		return 1;

}



