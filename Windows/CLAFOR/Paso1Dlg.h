#pragma once
#include "afxwin.h"


// Cuadro de di�logo de CPaso1Dlg

class CPaso1Dlg : public CDialog
{
	DECLARE_DYNAMIC(CPaso1Dlg)

public:
	CPaso1Dlg(CWnd* pParent = NULL);   // Constructor est�ndar
	virtual ~CPaso1Dlg();

// Datos del cuadro de di�logo
	enum { IDD = IDD_DIALOG_PASO_1 };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // Compatibilidad con DDX o DDV

	DECLARE_MESSAGE_MAP()
protected:

	virtual BOOL OnInitDialog();

	// Descripci�n del paso 1
	CString m_strDescrPaso1;
	// Texto del paso 1
	CString m_strLblPaso1;
	// Texto del paso 2
	CString m_strLblPaso2;

	// El texto paso 1
	CString m_strLblPaso;
};
