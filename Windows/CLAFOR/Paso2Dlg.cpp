// Paso2Dlg.cpp: archivo de implementaci�n
//

#include "stdafx.h"
#include "CLAFOR.h"
#include "Paso2Dlg.h"
#include ".\paso2dlg.h"


// Cuadro de di�logo de CPaso2Dlg

IMPLEMENT_DYNAMIC(CPaso2Dlg, CDialog)
CPaso2Dlg::CPaso2Dlg(CWnd* pParent /*=NULL*/)
	: CDialog(CPaso2Dlg::IDD, pParent)
	, m_strDescripcion(_T(""))
	, m_strPaso2(_T(""))
{
}

CPaso2Dlg::~CPaso2Dlg()
{
}

void CPaso2Dlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_PROGRESO, m_progreso);
	DDX_Text(pDX, IDC_LBL_DESCR_PASO_2, m_strDescripcion);
	DDX_Text(pDX, IDC_LBL_PASO_2, m_strPaso2);
}





BEGIN_MESSAGE_MAP(CPaso2Dlg, CDialog)
END_MESSAGE_MAP()


// Hace que se incremente el paso de la barra de progreso

void CPaso2Dlg::Paso(void)
{
	m_progreso.StepIt();
}

// Establece el tama�o del paso
void CPaso2Dlg::SetPaso(int paso)
{
	m_progreso.SetStep(paso);
}


BOOL CPaso2Dlg::OnInitDialog(void)
{
	
	m_strDescripcion.LoadString(IDS_PASO2_DESCRIPCION);
	m_strPaso2.LoadString(IDS_PASO);
	m_strPaso2 += _T(" 2");
	UpdateData(FALSE); 
	return TRUE;
}
