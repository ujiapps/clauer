// CLAFORDlg.cpp: archivo de implementaci�n
//

#include "stdafx.h"
#include "CLAFOR.h"
#include "CLAFORDlg.h"
#include ".\clafordlg.h"
#include "ThrTrabajo.h"
#include "Resource.h"
#include "DialogPIN.h"
#include "Error.h"

#include <LIBRT/LIBRT.h>
#include <CRYPTOWrapper/CRYPTOWrap.h>
#include <clrep.h>
#include <clui/clui.h>


#include "afxwin.h"

#ifdef _DEBUG
#define new DEBUG_NEW
#endif

// Error del thread

static int gErrThread     = 0;       // indica si el thread termin� con errores
static bool gThrTerminado = false;   // indica si el thread termin� o no
static bool gThrCancel    = false;   // indica si el usuario cancel� la operaci�n

// Cuadro de di�logo CAboutDlg utilizado para el comando Acerca de

class CAboutDlg : public CDialog
{
public:
	CAboutDlg();

// Datos del cuadro de di�logo
	enum { IDD = IDD_ABOUTBOX };

	protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // Compatibilidad con DDX/DDV

// Implementaci�n
protected:
	DECLARE_MESSAGE_MAP()
public:
	CButton m_btnAceptar;
};

CAboutDlg::CAboutDlg() : CDialog(CAboutDlg::IDD)
{
}

void CAboutDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDOK, m_btnAceptar);
}

BEGIN_MESSAGE_MAP(CAboutDlg, CDialog)
END_MESSAGE_MAP()


// Cuadro de di�logo de CCLAFORDlg



CCLAFORDlg::CCLAFORDlg(CWnd* pParent /*=NULL*/)
	: CDialog(CCLAFORDlg::IDD, pParent)
	, m_paso(0) 
{
	m_hIcon = AfxGetApp()->LoadIcon(IDR_MAINFRAME);
}

void CCLAFORDlg::DoDataExchange(CDataExchange* pDX)
{
	CDialog::DoDataExchange(pDX);
	DDX_Control(pDX, IDC_BTN_SIGUIENTE, m_btnSiguiente);
}

BEGIN_MESSAGE_MAP(CCLAFORDlg, CDialog)
	ON_WM_SYSCOMMAND()
	ON_WM_PAINT()
	ON_WM_QUERYDRAGICON()
	//}}AFX_MSG_MAP
	ON_BN_CLICKED(IDC_BTN_SIGUIENTE, OnBnClickedBtnSiguiente)
	ON_BN_CLICKED(IDC_BUTTON_SALIR, OnBnClickedButtonSalir)
	ON_WM_TIMER()
END_MESSAGE_MAP()


// Controladores de mensaje de CCLAFORDlg

BOOL CCLAFORDlg::OnInitDialog()
{
	CDialog::OnInitDialog();

	// Agregar el elemento de men� "Acerca de..." al men� del sistema.

	// IDM_ABOUTBOX debe estar en el intervalo de comandos del sistema.
	ASSERT((IDM_ABOUTBOX & 0xFFF0) == IDM_ABOUTBOX);
	ASSERT(IDM_ABOUTBOX < 0xF000);

	CMenu* pSysMenu = GetSystemMenu(FALSE);
	if (pSysMenu != NULL)
	{
		CString strAboutMenu;
		strAboutMenu.LoadString(IDS_ABOUTBOX);
		if (!strAboutMenu.IsEmpty())
		{
			pSysMenu->AppendMenu(MF_SEPARATOR);
			pSysMenu->AppendMenu(MF_STRING, IDM_ABOUTBOX, strAboutMenu);
		}
	}

	// Establecer el icono para este cuadro de di�logo. El marco de trabajo realiza esta operaci�n
	//  autom�ticamente cuando la ventana principal de la aplicaci�n no es un cuadro de di�logo
	SetIcon(m_hIcon, TRUE);			// Establecer icono grande
	SetIcon(m_hIcon, FALSE);		// Establecer icono peque�o

	// TODO: agregar aqu� inicializaci�n adicional

	m_paso = 1;

	CWnd* pWnd = GetDlgItem( IDC_PASOS );
    CRect rect;
    pWnd->GetWindowRect( &rect );
    ScreenToClient( &rect );

	// Estas ventanas siempre estar�n creadas mientras la apliaci�n est� en
	// ejecuci�n

	m_dlgPaso1.Create(IDD_DIALOG_PASO_1, this);
	m_dlgPaso1.SetWindowPos(pWnd, rect.left, rect.top, rect.right, rect.bottom, SWP_SHOWWINDOW);
	m_dlgPaso1.ShowWindow(WS_VISIBLE|WS_CHILD);

	m_dlgPaso2.Create(IDD_DIALOG_PASO_2, this);
	m_dlgPaso2.SetWindowPos(pWnd, rect.left, rect.top, rect.right, rect.bottom, SWP_SHOWWINDOW);

	m_dlgPaso3.Create(IDD_DIALOG_PASO_3, this);
	m_dlgPaso3.SetWindowPos(pWnd, rect.left, rect.top, rect.right, rect.bottom, SWP_SHOWWINDOW);

	CString msg;
	msg.LoadString(IDS_BTN_SIGUIENTE);
	this->m_btnSiguiente.SetWindowText(msg.GetBuffer(0));

	return TRUE;  // Devuelve TRUE  a menos que establezca el foco en un control
}

void CCLAFORDlg::OnSysCommand(UINT nID, LPARAM lParam)
{
	if ((nID & 0xFFF0) == IDM_ABOUTBOX)
	{
		CAboutDlg dlgAbout;
		dlgAbout.DoModal();
	}
	else
	{
		CDialog::OnSysCommand(nID, lParam);
	}
}

// Si agrega un bot�n Minimizar al cuadro de di�logo, necesitar� el siguiente c�digo
// para dibujar el icono. Para aplicaciones MFC que utilicen el modelo de documentos y vistas,
// esta operaci�n la realiza autom�ticamente el marco de trabajo.

void CCLAFORDlg::OnPaint() 
{
	if (IsIconic())
	{
		CPaintDC dc(this); // Contexto de dispositivo para dibujo

		SendMessage(WM_ICONERASEBKGND, reinterpret_cast<WPARAM>(dc.GetSafeHdc()), 0);

		// Centrar icono en el rect�ngulo de cliente
		int cxIcon = GetSystemMetrics(SM_CXICON);
		int cyIcon = GetSystemMetrics(SM_CYICON);
		CRect rect;
		GetClientRect(&rect);
		int x = (rect.Width() - cxIcon + 1) / 2;
		int y = (rect.Height() - cyIcon + 1) / 2;

		// Dibujar el icono
		dc.DrawIcon(x, y, m_hIcon);
	}
	else
	{
		CDialog::OnPaint();
	}
}

// El sistema llama a esta funci�n para obtener el cursor que se muestra mientras el usuario arrastra
//  la ventana minimizada.
HCURSOR CCLAFORDlg::OnQueryDragIcon()
{
	return static_cast<HCURSOR>(m_hIcon);
}


// Actualiza la interfaz de usuario al paso especificado
void CCLAFORDlg::SetPaso(int paso)
{
	CString msg;
	
	/* Ocultamos la interfaz del paso actual
	 */

	switch ( m_paso ) {
	case 1:
		m_dlgPaso1.ShowWindow(SW_HIDE);
		break;

	case 2:
		m_dlgPaso2.ShowWindow(SW_HIDE);
		break;

	case 3:
		m_dlgPaso3.ShowWindow(SW_HIDE);
		break;
	}

	/* Mostramos la interfaz correspondiente al paso pedido
	 */

	switch ( paso ) {
	case 1:
		m_btnSiguiente.EnableWindow(TRUE);

		msg.LoadString(IDS_BTN_SIGUIENTE);
		m_btnSiguiente.SetWindowText(msg.GetBuffer(0));

		m_dlgPaso1.ShowWindow(SW_SHOW);
		break;

	case 2:
		m_btnSiguiente.EnableWindow(TRUE);

		msg.LoadString(IDS_BTN_INICIAR);
		m_btnSiguiente.SetWindowText(msg.GetBuffer(0));

		m_dlgPaso2.ShowWindow(SW_SHOW);
		break;

	case 3:
		m_btnSiguiente.EnableWindow(TRUE);

		msg.LoadString(IDS_BTN_END);
		m_btnSiguiente.SetWindowText(msg.GetBuffer(0));

		m_dlgPaso3.ShowWindow(SW_SHOW);
		m_btnSiguiente.SetFocus();
	}

	m_paso = paso;

}



UINT MyControllingFunction( LPVOID pParam )
{
	CPaso2Dlg *dlgPaso2 = (CPaso2Dlg *) pParam;

	USBCERTS_HANDLE hClauer;
	int nDisp=0;
	bool dispositivoIniciado = false;
	unsigned char *disp[MAX_DEVICES];

	try {

		dlgPaso2->SetPaso(100/CRepStepList::totalSteps);

		do {

			LIBRT_RegenerarCache();

			if ( LIBRT_ListarDispositivos(&nDisp, disp) != 0 ) {
				gErrThread = 1;
				throw gErrThread;
			}

			if ( nDisp == 0 ) {
				if ( AfxMessageBox(IDS_WARN_NO_CLAUERS, MB_ICONWARNING|MB_OKCANCEL, 0) == IDCANCEL ) {
					gErrThread = 1;
					throw gErrThread;
				}
			} else {
				break;
			}

		} while ( 1 );


		char szDevice[CLUI_MAX_DEVICE_LEN], szPass[CLUI_MAX_PASS_LEN];
		int ret;
		CString aux;

		aux.LoadString(IDS_PASSWORD);

		ret = CLUI_AskGlobalPassphrase ( theApp.GetMainWnd()->m_hWnd,						// Handle a la ventana padre
								   TRUE,						// Indica si debe enumerar o no dispositivos
								   TRUE,				// Indica si calcular o no propietario
								   TRUE,				// Indica si se debe o no verificar password
								   (char *) aux.GetString(),
								   szDevice,	// El dispositivo elegido o que se desea utilizar
								   szPass );

		if ( ret == CLUI_CANCEL ) {
			gThrTerminado = true;	
			AfxEndThread(0);
			return 0;
		}


		/* Empezamos con el arreglo
		 */

		CRepStepList rsl;
		IClRepairStep *rs;
		int i = 1;
		while ( rs = rsl.next() ) {
			rs->setPwd(szPass);
			if ( rs->repair((char *)szDevice) != CLREP_OK ) {
				CString msg, format;
				format.LoadString(IDS_ERROR_STEP_N);
				msg.Format(format.GetBuffer(), i);
				AfxMessageBox(msg.GetBuffer(), MB_ICONERROR|MB_OK, 0);
				gErrThread = 1;
				throw gErrThread;
			}
			dlgPaso2->Paso();
			++i;
		}		
	}
	catch ( int i ) {
		gErrThread = i;
	}
	catch (...) {
		gErrThread = 1;
	}

	for ( int k = 0 ; k < nDisp ; k++ ) 
		free(disp[k]);

	gThrTerminado = true;

	if ( 0 == gErrThread ) {

		HKEY hKey;

		/* Borro la clave de Run de HKEY_CURRENTUSER 
		 */

		if ( RegOpenKeyEx(HKEY_CURRENT_USER, 
						  _T("Software\\Microsoft\\Windows\\CurrentVersion\\Run"), 
						  0, 
						  KEY_ALL_ACCESS, 
						  &hKey) == ERROR_SUCCESS ) 
		{
			RegDeleteValue(hKey, _T("CLAFOR"));
			RegCloseKey(hKey);
		}
	}

	AfxEndThread(1, TRUE);
	return 0;
}


void CCLAFORDlg::OnBnClickedBtnSiguiente()
{
	/* Realizamos las acciones asociadas al paso actual
	 */

	if ( 1 == m_paso ) {

		/* S�lo admitimos un clauer insertado
		 */

		unsigned char *disp[MAX_DEVICES];
		int nDisp;

		do {
			LIBRT_RegenerarCache();

			if ( LIBRT_ListarDispositivos(&nDisp, disp) != 0 ) {
				AfxMessageBox(IDS_ERROR_ENUMERACION, MB_ICONERROR|MB_OK, 0);
				return;
			}

			for ( int i = 0 ; i < nDisp ; i++ )
				free(disp[i]);

			if ( nDisp == 0 ) {
				if ( AfxMessageBox(IDS_WARN_NO_CLAUERS, MB_ICONWARNING|MB_OKCANCEL, 0) == IDCANCEL )
					return;
			} else {
				break;
			}

		} while ( 1 );

		SetPaso(2);

	} else if ( 2 == m_paso ) {

		/* Deshabilito el bot�n hasta que se acabe el proceso
		 */

		m_btnSiguiente.EnableWindow(FALSE);
		
		CButton *btn = ( CButton * ) GetDlgItem(IDC_BUTTON_SALIR);
		if ( btn ) 
			btn->EnableWindow(FALSE);
		
		CThrTrabajo *thrTrabajo = NULL;
		thrTrabajo = (CThrTrabajo *) AfxBeginThread( MyControllingFunction,
													 (LPVOID) &m_dlgPaso2,
													 THREAD_PRIORITY_NORMAL,
													 0,
													 CREATE_SUSPENDED,
													 NULL );

		thrTrabajo->ResumeThread();
		SetTimer(69,200,NULL);

	} else if ( 3 == m_paso ) {

		EndDialog(0);
	}

}


void CCLAFORDlg::OnBnClickedButtonSalir()
{
	EndDialog(IDCANCEL);
}


void CCLAFORDlg::SoyElThreadYHeAcabado(void)
{
	this->SetPaso(m_paso+1);
}

void CCLAFORDlg::OnTimer(UINT nIDEvent)
{
	if ( nIDEvent == 69 ) {

		if ( gThrTerminado ) {
			KillTimer(69);

			SetPaso(m_paso+1);

			if ( gThrCancel ) {
				AfxMessageBox(IDS_USER_CANCEL, MB_OK|MB_ICONWARNING);
			} else {

				switch ( gErrThread ) {

				case ERR_CANCEL:
					AfxMessageBox(IDS_ERROR_CANCEL, MB_OK|MB_ICONWARNING);
					EndDialog(0);
					break;

				case 1:
					AfxMessageBox(IDS_ACTUALIZACION_ERROR, MB_OK|MB_ICONERROR);
					EndDialog(1);
					break;

				default:
					AfxMessageBox(IDS_ACTUALIZACION_EXITO, MB_OK|MB_ICONINFORMATION);
					break;
				}

			}

		}

	}

	CDialog::OnTimer(nIDEvent);
}

