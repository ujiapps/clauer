#pragma once

#include "stdafx.h"
#include "Resource.h"

// Cuadro de di�logo de CDialogPIN

class CDialogPIN : public CDialog
{
	DECLARE_DYNAMIC(CDialogPIN)

public:
	CDialogPIN(CWnd* pParent = NULL);   // Constructor est�ndar
	CDialogPIN(CString strDevice, CWnd *pParent = NULL);
	virtual ~CDialogPIN();

// Datos del cuadro de di�logo
	enum { IDD = IDD_DIALOG_PIN };

protected:
	virtual void DoDataExchange(CDataExchange* pDX);    // Compatibilidad con DDX o DDV

	DECLARE_MESSAGE_MAP()
public:
	CString m_strPIN;
	CString m_strDevice;

protected:
	// Machaca el contenido del string
	void DestruirPIN(void);

public:
	afx_msg void OnBnClickedOk();

	// Devuelve el PIN
	CString GetPIN(void)
	{
		return m_strPIN;
	}
};
