//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by CLAFOR.rc
//
#define IDM_ABOUTBOX                    0x0010
#define IDD_ABOUTBOX                    100
#define IDS_ABOUTBOX                    101
#define IDD_CLAFOR_DIALOG               102
#define IDD_PRINCIPAL_DLG               102
#define IDS_PASO                        102
#define IDP_SOCKETS_INIT_FAILED         103
#define IDS_PASO1_DESCRIPCION           104
#define IDS_PASO1_PASO_1                105
#define IDS_PASO1_PASO_2                106
#define IDS_BTN_SIGUIENTE               107
#define IDS_BTN_SALIR                   108
#define IDS_PASO2_DESCRIPCION           109
#define IDS_ERROR_NO_CLAUER_BASE        110
#define IDS_ERROR_CAPTION               111
#define IDS_ERROR_ENUMERACION           112
#define IDS_WARN_NO_CLAUERS             113
#define IDS_WARN_CAPTION                114
#define IDS_WARN_MAS_DE_UN_CLAUER       115
#define IDS_ERROR_IMPOSIBLE_INICIAR_DISPOSITIVO 116
#define IDS_ERROR_ACCESO_DISPOSITIVO    117
#define IDS_SALIR                       118
#define IDS_STRING119                   119
#define IDS_ACTUALIZACION_EXITO         119
#define IDS_PASO3_DESCRIPCION           120
#define IDS_PASO3_PASO_1                121
#define IDS_PASO3_PASO_2                122
#define IDS_ERROR_CANCEL                123
#define IDS_REINTENTAR                  124
#define IDS_ACTUALIZACION_ERROR         125
#define IDS_ERROR_STEP_N                126
#define IDS_PASSWORD                    127
#define IDS_BTN_END                     128
#define IDD_DIALOG_PASO_1               129
#define IDS_USER_CANCEL                 129
#define IDD_DIALOG_PASO_2               130
#define IDD_DIALOG_PASO_3               131
#define IDS_BTN_INICIAR                 133
#define IDD_DIALOG_PIN                  134
#define IDI_ICON1                       136
#define IDR_MAINFRAME                   136
#define IDI_UJI                         138
#define IDC_BTN_SIGUIENTE               1002
#define BITMAP_CLAFOR                   1002
#define IDC_BUTTON_SALIR                1004
#define IDC_PASOS                       1005
#define IDC_IMAGEN_IZQ                  1006
#define IDC_PROGRESO                    1010
#define IDC_LBL_PASO_2                  1012
#define IDC_LBL_DESCR_PASO_2            1013
#define IDC_LBL_DESCR_PASO_1            1014
#define IDC_LBL_PASO_1_PASO_1           1015
#define IDC_LBL_PASO_2_PASO_1           1016
#define IDC_LBL_PASO_1                  1017
#define IDC_EDIT_PIN                    1019
#define IDC_LBL_PASO3                   1020
#define IDC_LBL_DESCR_PASO_3            1021
#define IDC_LBL_PASO_1_PASO_3           1022
#define IDC_LBL_PASO_2_PASO_3           1023

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        139
#define _APS_NEXT_COMMAND_VALUE         32771
#define _APS_NEXT_CONTROL_VALUE         1024
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
