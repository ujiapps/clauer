#ifndef __CLAF_UTILS_HPP__
#define __CLAF_UTILS_HPP__

#include <LIBRT/LIBRT.h>


int CLAFUTILS_Clauer_Ok ( USBCERTS_HANDLE *hClauer, 
						  INFO_KEY_CONTAINER kc, 
						  bool signature,
						  bool & arreglar );

int CLAFUTILS_Arreglar ( USBCERTS_HANDLE *hClauer,
						 BYTE bloqueContainer[TAM_BLOQUE],
						 long nBloqueContainer,
						 INFO_KEY_CONTAINER kc,
						 bool arreglarSignature );


int CLAFUTILS_Get_Blob ( USBCERTS_HANDLE *hClauer,
						 unsigned char *id,
					     BYTE bloque[TAM_BLOQUE],
						 long & nBloque );

int CLAFUTILS_Get_Cert ( USBCERTS_HANDLE *hClauer,
						 unsigned char *id,
					     BYTE bloque[TAM_BLOQUE],
						 long & nBloque );


/* ERRORES asociados a las excepciones */

#define ERR_NO_BLOB			1
#define ERR_UNKNOWN			2
#define ERR_CANCEL			3


#endif
