Requisitos:

1-. Deshabilitar para el usuario el uso de C:\WINDOWS\system32\taskmgr.exe

2-. Ejecutar mmc, Archivo -> Agregar o quitar complemento -> Agregar -> Directivas de grupo -> Finalizar. Directiva equipo local -> Configuraci�n del equipo ->  Configuraci�n de Windows -> Archivos de comandos (inicio/apagado) -> Inicio -> Agregar, y en �Nombre del archivo de comandos� poner C:\Archivos de programa\Universitat Jaume I\Projecte Clauer\Clablock\BlockCheck.exe

3-. Reiniciar


El instalador a�ade dos entradas en el registro:

    * HKEY_LOCAL_MACHINE\SOFTWARE\Universitat Jaume I\Projecte Clauer\CLABLOCK_AULA
      que debe contener un c�digo de ubicaci�n de 6 letras, por ejemplo MEDIAT .
    * HKEY_LOCAL_MACHINE\SOFTWARE\Universitat Jaume I\Projecte Clauer\CLABLOCK_SESION
      que contiene los minutos que transcurren desde que se retira el clauer hasta que se cierra la sesi�n si no se introduce otro clauer (ver apartado correspondiente).
    * HKEY_LOCAL_MACHINE\SOFTWARE\Universitat Jaume I\Projecte Clauer\CLABLOCK_HOST
      con el nombre del servidor donde conectar por TLS sin autenticaci�n (explicado en el apartado correspondiente), por ejemplo clablock.nisu.org.
    * HKEY_LOCAL_MACHINE\SOFTWARE\Universitat Jaume I\Projecte Clauer\CLABLOCK_PATH
      con el path del script invocado dentro de ese servidor, poe ejemplo /x.php.
    * HKEY_LOCAL_MACHINE\SOFTWARE\Universitat Jaume I\Projecte Clauer\CLABLOCK_HOSTSSL
      con el nombre del servidor donde conectar con autenticaci�n por certificados.
    * HKEY_LOCAL_MACHINE\SOFTWARE\Universitat Jaume I\Projecte Clauer\CLABLOCK_PATHSSL
      con el path del script invocado dentro de ese servidor. 

El programa BlockCheck debe instalarse en el mismo directorio donde se ha instalado ClaBlock. BlockCheck es un Servicio de Windows y por ello se inicia con el sistema, no con la sesi�n.

El programa ClaBlock debe instalarse en un directorio donde el usuario no tenga permisos de escritura, por defecto se instala en Archivos de programa, donde es esperable que no pueda escribir. El programa debe iniciarse autom�ticamente al inicio de sesi�n, lo que es realizado por el instalador, modificando el registro.


Projecte Clauer
Mauro Esteve Ferrer
maesteve@sg.uji.es
13-12-2005

