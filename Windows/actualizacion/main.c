#include <windows.h>
#include <winhttp.h>
#include <time.h>
#include <stdio.h>
#include <stdlib.h>

#define PRG_NAME				"Clauer Store Provider"
#define PRG_NAME_WEB			"usbpki-base"
#define PRG_VERSION				"2005040702"
#define PRG_URL_EXPIRE			"http://expire.nisu.org/"
#define PRG_REG_KEY				"SOFTWARE\\Universitat Jaume I\\Projecte Clauer"
#define PRG_REG_VALOR			"UP_USBPKIBASE"
#define PRG_REG_VALOR_UNO_SOLO  "UP_ONLYYOU"


/* 0 OK
 * 1 UP
 * 2 Error == No puedo conectar opr ejemplo
 */

int PRG_ConectaServidor (void)
{
    DWORD dwSize = 0;
    DWORD dwDownloaded = 0;
	DWORD dwTotal = 0;
    BYTE *pszOutBuffer = NULL, *aux;
    BOOL  bResults = FALSE;
    HINTERNET  hSession = NULL, 
               hConnect = NULL,
               hRequest = NULL;

	char *URLBytes = NULL;
	WCHAR *url = NULL;
	int ret = 0;

    // Use WinHttpOpen to obtain a session handle.
    hSession = WinHttpOpen( L"Clauer Update/1.0",  
                            WINHTTP_ACCESS_TYPE_DEFAULT_PROXY,
                            WINHTTP_NO_PROXY_NAME, 
                            WINHTTP_NO_PROXY_BYPASS, 0);

	if ( !hSession ) {
		ret = 2;
		goto finPRG_ConectaServidor;
	}	

    // Specify an HTTP server.

    hConnect = WinHttpConnect( hSession, L"expire.nisu.org",
                               INTERNET_DEFAULT_HTTP_PORT, 0);

	if ( !hConnect ) {
		ret = 2;
		goto finPRG_ConectaServidor;
	}


	URLBytes = (char *) malloc ( sizeof(PRG_NAME_WEB) + strlen("?prg=&ver=0000000000") + 1 );

	if ( !URLBytes ) {
		ret = 2;
		goto finPRG_ConectaServidor;
	}


	url = (WCHAR *) malloc ( sizeof(WCHAR) * (sizeof(PRG_NAME_WEB) + strlen("?prg=&ver=0000000000") + 1));

	if ( !url ) {
		ret = 2;
		goto finPRG_ConectaServidor;
	}

	sprintf(URLBytes, "?prg=%s&ver=%s", PRG_NAME_WEB, PRG_VERSION);

	if ( !MultiByteToWideChar(CP_ACP,MB_PRECOMPOSED, URLBytes, -1, url, sizeof(PRG_NAME_WEB) + strlen("?prg=&ver=0000000000") + 1) ) {
		ret = 2;
		goto finPRG_ConectaServidor;
	}

    // Create an HTTP request handle.

    hRequest = WinHttpOpenRequest( hConnect, L"GET", url,
                                   NULL, WINHTTP_NO_REFERER, 
                                   WINHTTP_DEFAULT_ACCEPT_TYPES, 
                                   0);

	if ( !hRequest ) {
		ret = 2;
		goto finPRG_ConectaServidor;
	}

    // Send a request.

    bResults = WinHttpSendRequest( hRequest,
                                   WINHTTP_NO_ADDITIONAL_HEADERS, 0,
                                   WINHTTP_NO_REQUEST_DATA, 0, 
                                   0, 0);

	if ( !bResults ) {
		ret = 2;
		goto finPRG_ConectaServidor;
	}

 
    // End the request.

    bResults = WinHttpReceiveResponse( hRequest, NULL);

	if ( !bResults ) {
		DWORD err;
		err = GetLastError();
		ret = 2;
		goto finPRG_ConectaServidor;
	}


	pszOutBuffer = (BYTE *) malloc (1024 * 1024);

	if  (!pszOutBuffer) {
		ret = 2;
		goto finPRG_ConectaServidor;
	}

	aux = pszOutBuffer;

    // Keep checking for data until there is nothing left.
    do 
    {
        // Check for available data.
        dwSize = 0;
        if (!WinHttpQueryDataAvailable( hRequest, &dwSize)) {
			ret = 2;
			goto finPRG_ConectaServidor;
		}

        // Read the Data.

		if ( dwTotal + dwSize >= (1024*1024+1) ) 
			break;
		

        ZeroMemory(aux, dwSize+1);

        if (!WinHttpReadData( hRequest, (LPVOID)aux, dwSize, &dwDownloaded)) {
			ret = 2;
			goto finPRG_ConectaServidor;
		}
    
        // Free the memory allocated to the buffer.
        
		dwTotal += dwDownloaded;
		if ( dwTotal >= (1024*1024 + 1) ) {
			dwSize = 0;
		} else {
			aux += dwDownloaded;
		}

    } while (dwSize>0);

	if ( memcmp(pszOutBuffer, "OK", 2) == 0 ) {
		ret = 0;
	} else if ( memcmp(pszOutBuffer, "UP", 2) == 0 ) {
		ret = 1;
	} else {
		ret = 2;
	}


finPRG_ConectaServidor:

    if (hRequest) 
		WinHttpCloseHandle(hRequest);

    if (hConnect) 
		WinHttpCloseHandle(hConnect);

    if (hSession) 
		WinHttpCloseHandle(hSession);

	if ( URLBytes )
		free(URLBytes);

	if ( url )
		free(url);

	if ( pszOutBuffer )
		free(pszOutBuffer);

	return ret;

}




/* 0 no hay que comprobar
 * 1 Hay que comprobar
 */

int PRG_Comprobar (void)
{
	int ret = 1;
	HKEY hKey;
	DWORD tamValue = 0, tipo;
	time_t hoy = time(NULL), fecha_registro;
	BYTE byteFechaRegistro[100];


	if ( RegOpenKeyEx(HKEY_LOCAL_MACHINE, PRG_REG_KEY, 0, KEY_ALL_ACCESS, &hKey) != ERROR_SUCCESS ) {
		ret = 1;
		goto finPRG_Comprobar;
	}

	tamValue = sizeof(time_t);

	if ( RegQueryValueEx(hKey, PRG_REG_VALOR_UNO_SOLO, NULL, &tipo, (BYTE *) &fecha_registro, &tamValue) != ERROR_SUCCESS ) {

		if ( RegSetValueEx(hKey, PRG_REG_VALOR_UNO_SOLO, 0, REG_BINARY, (BYTE *) &hoy, sizeof(time_t)) != ERROR_SUCCESS ) {
			ret = 1;
			goto finPRG_Comprobar;
		}

	} else {

		if ( tipo != REG_BINARY ) {
			ret = 1;
			goto finPRG_Comprobar;
		}

		/* Si ha pasado m�s de un d�a, entonces indicamos que hay que comprobar, si no decimos que no
		 */

		if ( hoy - fecha_registro >= 86400 ) {
			ret = 0;

			if ( RegSetValueEx(hKey, PRG_REG_VALOR_UNO_SOLO, 0, REG_BINARY, (BYTE *) &hoy, sizeof(time_t)) != ERROR_SUCCESS ) {
				ret = 1;
				goto finPRG_Comprobar;
			}
		}

	}

finPRG_Comprobar:

	RegCloseKey(hKey);

	return ret;
}








/* 0 No caducado
 * 1 Caducado: MENSAJE 1 + MENSAJE 2
 * 2 Error
 * 3 Warning: MENSAJE 1
 */

int PRG_Caducado (void)
{
	HKEY hKey = NULL, hKey2 = NULL;
	DWORD tamValue;
	char *regwar = NULL;
	int ret = 0;
	time_t ltime;
	time_t ltime_regwar;
	struct tm tm_regwar;
	char aux[5];
	double diff;
	char fecha[9];
	time_t hoy;
	DWORD type;


	if ( ! PRG_Comprobar() ) {
		ret = 0;
		goto finPRG_Caducado;
	}


	if ( RegOpenKeyEx(HKEY_LOCAL_MACHINE, PRG_REG_KEY, 0, KEY_ALL_ACCESS, &hKey) != ERROR_SUCCESS ) {
		ret = 2;
		goto finPRG_Caducado;
	}

	if ( RegQueryValueEx(hKey, PRG_REG_VALOR, NULL, NULL, NULL, &tamValue) != ERROR_SUCCESS ) {
		
		switch ( PRG_ConectaServidor() ) {

		case 1:

			time(&hoy);
			memset(fecha,0,9);
			strftime(fecha, 10, "%Y%m%d", gmtime(&hoy));
			if ( RegSetValueEx(hKey, PRG_REG_VALOR, 0, REG_SZ, fecha, 9) != ERROR_SUCCESS ) {
				ret = 2;
				goto finPRG_Caducado;
			}
			tamValue = 9;

			break;

		case 2:
			ret = 0;
			goto finPRG_Caducado;

		case 0:
			ret = 0;
			goto finPRG_Caducado;

		}

	}


	if ( RegCloseKey(hKey) != ERROR_SUCCESS ) {
		ret = 2;
		goto finPRG_Caducado;
	}
	hKey = NULL;
	
	if ( RegOpenKeyEx(HKEY_LOCAL_MACHINE, PRG_REG_KEY, 0, KEY_ALL_ACCESS, &hKey2) != ERROR_SUCCESS ) {
		ret = 2;
		goto finPRG_Caducado;
	}
	
	regwar = (char *) malloc (tamValue);

	if ( !regwar ) {
		ret = 2;
		goto finPRG_Caducado;
	}

	if ( RegQueryValueEx(hKey2, PRG_REG_VALOR, NULL, &type, (BYTE *)regwar, &tamValue) != ERROR_SUCCESS ) {
		ret = 2;
		goto finPRG_Caducado;
	}

	/* Regwar est� definida 
	 */

	/* Fecha de hoy */

    time( &ltime );

	memset(&tm_regwar,0, sizeof(struct tm));

	memcpy(aux, regwar, 4);
	aux[4] = 0;
	tm_regwar.tm_year = atoi(aux) - 1900;

	memcpy(aux, regwar+4, 2);
	aux[2] = 0;
	tm_regwar.tm_mon = atoi(aux) - 1;

	memcpy(aux, regwar+6, 2);
	aux[2] = 0;
	tm_regwar.tm_mday = atoi(aux);

	ltime_regwar = mktime( &tm_regwar);

	diff = difftime(ltime, ltime_regwar);

	if ( diff > 1296000 ) {

		/* Si es mayor de quince d�as
		 */

		ret = 1;
		goto finPRG_Caducado;

	} else {

		ret = 3;
		goto finPRG_Caducado;
	}

	
finPRG_Caducado:

	if ( hKey ) 
		RegCloseKey(hKey);

	if ( hKey2 )
		RegCloseKey(hKey2);

	if ( regwar )
		free(regwar);


	

	return ret;
}













void main() 
{
	int v;
	v = PRG_Caducado();
	printf("%d\n", v);
}