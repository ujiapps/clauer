#include <windows.h>

#include <stdio.h>
#include <time.h>

#define PRG_NAME				"Clauer Store Provider"
#define PRG_NAME_WEB			"usbpki-base"
#define PRG_VERSION				"2005040702"
#define PRG_URL_EXPIRE			"http://expire.nisu.org/"
#define PRG_REG_KEY				"SOFTWARE\\Universitat Jaume I\\Projecte Clauer"
#define PRG_REG_VALOR			"UP_USBPKIBASE"
#define PRG_REG_VALOR_UNO_SOLO  "UP_ONLYYOU"

/* 0 no hay que comprobar
 * 1 Hay que comprobar
 */

int PRG_Comprobar (void)
{
	int ret = 1;
	HKEY hKey;
	DWORD tamValue = 0, tipo;
	time_t hoy = time(NULL), fecha_registro;
	BYTE byteFechaRegistro[100];


	if ( RegOpenKeyEx(HKEY_LOCAL_MACHINE, PRG_REG_KEY, 0, KEY_ALL_ACCESS, &hKey) != ERROR_SUCCESS ) {
		ret = 1;
		goto finPRG_Comprobar;
	}

	tamValue = sizeof(time_t);

	if ( RegQueryValueEx(hKey, PRG_REG_VALOR_UNO_SOLO, NULL, &tipo, &fecha_registro, &tamValue) != ERROR_SUCCESS ) {

		if ( RegSetValueEx(hKey, PRG_REG_VALOR_UNO_SOLO, 0, REG_BINARY, &hoy, sizeof(time_t)) != ERROR_SUCCESS ) {
			ret = 1;
			goto finPRG_Comprobar;
		}

	} else {

		if ( tipo != REG_BINARY ) {
			ret = 1;
			goto finPRG_Comprobar;
		}

		/* Si ha pasado m�s de un d�a, entonces indicamos que hay que comprobar, si no decimos que no
		 */

		if ( hoy - fecha_registro >= 86400 ) {
			ret = 0;

			if ( RegSetValueEx(hKey, PRG_REG_VALOR_UNO_SOLO, 0, REG_BINARY, &hoy, sizeof(time_t)) != ERROR_SUCCESS ) {
				ret = 1;
				goto finPRG_Comprobar;
			}
		}

	}

finPRG_Comprobar:

	RegCloseKey(hKey);

	return ret;
}





int main (void)
{

	printf("PRG_Comprobar() = %d\n", PRG_Comprobar());
	fflush(stdout);

}
