#ifndef __BASE64_H__
#define __BASE64_H__

#include <stdio.h>
#include <stdlib.h>

#ifdef __cplusplus
extern "C" {
#endif


void B64_Codificar (unsigned char *in, unsigned long tamIn, unsigned char *out, unsigned long *tamOut);
void B64_Decodificar (unsigned char *in, unsigned long tamIn, unsigned char *out, unsigned long *tamOut);


#ifdef __cplusplus
}
#endif


#endif
