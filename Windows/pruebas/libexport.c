#include "libexport.h"

#ifdef WIN32
#include <windows.h>
#include <wincrypt.h>
#endif

#include <stdlib.h>

#include <LIBRT/libRT.h>
#include <CRYPTOWrapper/CRYPTOWrap.h>
#include <clio/clio.h>





int CLEXPORT_P12 ( char *szDevice, 
		   char *szPwd, 
		   long bn, 
		   char *szP12Pwd,
		   unsigned char *p12, 
		   unsigned long *p12Size )
{
  USBCERTS_HANDLE hClauer;
  unsigned char bCert[TAM_BLOQUE], bKey[TAM_BLOQUE], *pkcs12 = NULL;
  int keyFound = 0;
  unsigned long tamPkcs12;
  FILE *fp = NULL;
  long privKeyBlock;
  char aux[256];

  int ret = 0;

  /* Comprobaci�n de par�metros
   */

  if ( ! szDevice )
    return ERR_CL_BAD_PARAM;
  if ( ! szPwd )
    return ERR_CL_BAD_PARAM;
  if ( ! szP12Pwd )
    return ERR_CL_BAD_PARAM;
  if ( ! p12Size )
    return ERR_CL_BAD_PARAM;

  if ( LIBRT_IniciarDispositivo(szDevice, szPwd, &hClauer) != 0 ) {
    ret = 1;
    goto endCLEXPORT_P12;
  }

  if ( LIBRT_LeerBloqueCrypto(&hClauer, bn, bCert) != 0 ) {
    LIBRT_FinalizarDispositivo(&hClauer);
    ret = 1;
    goto endCLEXPORT_P12;
  }

  if ( ! BLOQUE_Es_Claro(bCert) && (*(bCert+1) != BLOQUE_CERT_PROPIO) ) {
    LIBRT_FinalizarDispositivo(&hClauer);
    ret = 1;
    goto endCLEXPORT_P12;
  }


  /* Buscamos llave privada asociada */

  if ( LIBRT_LeerTipoBloqueCrypto(&hClauer, BLOQUE_LLAVE_PRIVADA, 1, bKey, &privKeyBlock) != 0 ) {
    LIBRT_FinalizarDispositivo(&hClauer);
    ret = 1;
    goto endCLEXPORT_P12;
  }

  while ( ( ! keyFound ) && (privKeyBlock != -1) ) {

    if ( memcmp(BLOQUE_LLAVEPRIVADA_Get_Id(bKey),
		BLOQUE_CERTPROPIO_Get_Id(bCert),
		20) == 0 ) {

      keyFound = 1;
    } else {

      SecureZeroMemory((void *) bKey, TAM_BLOQUE);
      if ( LIBRT_LeerTipoBloqueCrypto(&hClauer, BLOQUE_LLAVE_PRIVADA, 0, bKey, &privKeyBlock) != 0 ) {
	LIBRT_FinalizarDispositivo(&hClauer);
	ret = 1;
	goto endCLEXPORT_P12;
      }

    }

  }

  LIBRT_FinalizarDispositivo(&hClauer);

  if ( ! keyFound ) {
    ret = ERR_CL_NO_KEY_FOUND;
    goto endCLEXPORT_P12;
  }

  /* Creamos el PKCS#12 */

  if ( CRYPTO_PKCS12_Crear ( BLOQUE_LLAVEPRIVADA_Get_Objeto(bKey),
			     BLOQUE_LLAVEPRIVADA_Get_Tam(bKey),
			     NULL,
			     BLOQUE_CERTPROPIO_Get_Objeto(bCert), 
			     BLOQUE_CERTPROPIO_Get_Tam(bCert),
			     NULL,
			     NULL,
			     0,
			     szP12Pwd,
			     BLOQUE_CERTPROPIO_Get_FriendlyName(bCert),
			     NULL,
			     &tamPkcs12) != 0 ) 
    {
      ret = ERR_CL_CREATING_P12;
      goto endCLEXPORT_P12;
    }

  /* Si s�lo quer�a saber el tama�o, ya podemos volver
   */

  if ( ! p12 ) {
    ret = 0;
    *p12Size = tamPkcs12;
    goto endCLEXPORT_P12;
  }

  if ( *p12Size < tamPkcs12 ) {
    ret = ERR_CL_NOT_ENOUGH_BUFFER;
    goto endCLEXPORT_P12;
  }

  if ( CRYPTO_PKCS12_Crear ( BLOQUE_LLAVEPRIVADA_Get_Objeto(bKey),
			     BLOQUE_LLAVEPRIVADA_Get_Tam(bKey),
			     NULL,
			     BLOQUE_CERTPROPIO_Get_Objeto(bCert), 
			     BLOQUE_CERTPROPIO_Get_Tam(bCert),
			     NULL,
			     NULL,
			     0,
			     szP12Pwd,
			     BLOQUE_CERTPROPIO_Get_FriendlyName(bCert),
			     p12,
			     &tamPkcs12) != 0 ) 
    {
      SecureZeroMemory(p12, tamPkcs12);
      ret = ERR_CL_CREATING_P12;
      goto endCLEXPORT_P12;
    }

 endCLEXPORT_P12:

  SecureZeroMemory(bCert, TAM_BLOQUE);
  SecureZeroMemory(bKey, TAM_BLOQUE);

  return ret;

}






int CLEXPORT_CRYPTO ( char *szDevice, char *szOutFile )
{
  int nDisp, i, ret = CL_SUCCESS;
  clauer_handle_t hClauer;
  unsigned char block[BLOCK_SIZE];
  block_info_t ib;
  FILE *fp = NULL;
  unsigned long blocksToRead;

  if ( ! szDevice ) 
    return ERR_CL_BAD_PARAM;
  if ( ! szOutFile )
    return ERR_CL_BAD_PARAM;

  ret = IO_Open((char*) szDevice, &hClauer, IO_RD, IO_CHECK_IS_CLAUER );
  switch ( ret ) {
  case IO_SUCCESS:
    break;

  case ERR_IO_NO_PERM:
    ret = ERR_CL_NO_PERM;
    goto endCLEXPORT_CRYPTO;

  default:
    ret = ERR_CL;
    goto endCLEXPORT_CRYPTO;
  }

  if ( IO_ReadInfoBlock(hClauer, &ib) != IO_SUCCESS ) {
    IO_Close(hClauer);
    ret = ERR_CL;
    goto endCLEXPORT_CRYPTO;
  }
  
  /* Calculo el total de bloques a leer: 1 + res + objetos
   */
  
  blocksToRead = 1 + ib.rzSize;
  if ( ib.cb > -1 ) {
    blocksToRead += ib.cb;
    ib.totalBlocks = ib.cb;
    ib.cb          = -1;
  } else {
    blocksToRead += ib.totalBlocks;
  }
  

  /* Actualizamos el total blocks y el current block
   */

  
  /* Abrimos el fichero de salida
   */

  fp = fopen(szOutFile, "wb");
  if ( ! fp ) {
    IO_Close(hClauer);
    ret = ERR_CL_CANNOT_OPEN_FILE;
    goto endCLEXPORT_CRYPTO;
  }
  
  /* Escribimos el bloque de informaci�n
   */

  if ( fwrite(&ib, BLOCK_SIZE, 1, fp) == 0 ) {
    IO_Close(hClauer);
    ret = ERR_CL;
    goto endCLEXPORT_CRYPTO;
  }

  while ( blocksToRead-- && ((ret = IO_Read(hClauer, block)) == IO_SUCCESS) ) {
    if ( fwrite(block, BLOCK_SIZE, 1, fp) == 0 ){
      IO_Close(hClauer);
      ret = ERR_CL;
      goto endCLEXPORT_CRYPTO;
    }
  }

  fclose(fp);
  fp = NULL;

  if ( ret != IO_SUCCESS ) {
    if ( ret != ERR_IO_EOF ) {
      IO_Close(hClauer);
      ret = ERR_CL;
      goto endCLEXPORT_CRYPTO;
    }
  }

  IO_Close(hClauer);

 endCLEXPORT_CRYPTO:

  if ( fp ) 
    fclose(fp);

  SecureZeroMemory(block, BLOCK_SIZE);

  if ( ret != IO_SUCCESS ) 
    DeleteFile(szOutFile);

  return ret;
}





#ifdef __TEST__

#include <stdio.h>

int main ( void )
{
  char dev[] = "C:\\cryf_000.cla";
  char clauerOutFile[] = "c:\\cryf_001.cla";
  char pwd[] = "123clauer";
  char szP12Pwd[] = "jajajaja";
  unsigned char *p12;
  unsigned long p12Size;
  FILE *fp;

  int ret;

  LIBRT_Ini();
  CRYPTO_Ini();

  ret = CLEXPORT_P12(dev, pwd, 3, szP12Pwd, NULL, &p12Size);

  if ( ret == 0 ) {
    printf("[+] Exporting PKCS#12 ( part I )\n"
	   "    Buffer size: %ld\n", p12Size);
    
  } else {
    printf("[-] Exporting PKCS#12 ( part I): %d\n", ret);
    return 1;
  }
  fflush(stdout);

  p12 = malloc ( p12Size );
  if ( ! p12 ) {
    printf("[TEST ERROR] Not enoough memory\n");
    return 1;
  }

  ret = CLEXPORT_P12(dev, pwd, 3, szP12Pwd, p12, &p12Size);
  if ( ret == 0 ) {
    printf("[+] Exporting PKCS#12 ( part II )\n");
  } else {
    printf("[-] Exporting PKCS#12 ( part II )\n");
  }
  
  fp = fopen("C:\\yo.p12", "wb");
  fwrite(p12, p12Size, 1, fp);
  fclose(fp);

  ret = CLEXPORT_CRYPTO(dev, clauerOutFile);
  if ( ret == 0 ) {
    printf("[+] Exporting crypto zone\n");
  } else {
    printf("[-] Exporting crypto zone: %ld\n", ret);
  }
  fflush(stdout);

  return 0;
}


#endif
