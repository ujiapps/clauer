#include "libdel.h"

#ifdef WIN32
#include <windows.h>
#include <wincrypt.h>
#endif


#include <LIBRT/libRT.h>
#include <CRYPTOWrapper/CRYPTOWrap.h>



int CLDEL_CertKeys ( char *szDevice,
		     char *szPwd,
		     long bn )
{

  int ret = 0;
  
  USBCERTS_HANDLE h;
  int nDisp, i, j, tam, l, found= 0, rewrite=0;
  unsigned long bNumbers;
  long * hBlock = NULL;
  unsigned char *devs[MAX_DEVICES], *blocks = NULL, type, blockC[TAM_BLOQUE];
  char aux='k', nl;
  unsigned char bloque[TAM_BLOQUE], * bloqueAux;
  unsigned char id[20], zero[20];
  unsigned char *auxBlocks = NULL;
  
  unsigned int lstContainerSize = NUM_KEY_CONTAINERS;
  INFO_KEY_CONTAINER lstContainer[NUM_KEY_CONTAINERS];
  
#ifdef WIN32
  PCCERT_CONTEXT certCtx;
  BYTE *certDer;
  unsigned long tamDer;
#elif defined(LINUX)
  DN *subject, *issuer;
#endif
  
  if ( ! szDevice )
    return ERR_CL_BAD_PARAM;
  if ( ! szPwd )
    return ERR_CL_BAD_PARAM;
  if ( bn < 0 )
    return ERR_CL_BAD_PARAM;

  memset(zero,0,20);
  
  if ( LIBRT_IniciarDispositivo(szDevice, szPwd, &h) != 0 ) {
    ret = ERR_CL_CANNOT_INITIALIZE;
    goto endCLDEL_CertKeys;
  }

  
  if ( LIBRT_LeerBloqueCrypto( &h, bn, bloque) != 0) {
    LIBRT_FinalizarDispositivo(&h);
    ret = ERR_CL;
    goto endCLDEL_CertKeys;
  }


  if ( *(bloque+1) != BLOQUE_CERT_PROPIO ) {
    LIBRT_FinalizarDispositivo(&h);
    ret = ERR_CL_BAD_BLOCK_TYPE;
    goto endCLDEL_CertKeys;
  }

  /*  Nos copiamos el identificador del certificado. */

  memcpy(id, BLOQUE_CERTPROPIO_Get_Id(bloque), 20);
  
  if ( LIBRT_BorrarBloqueCrypto ( &h, bn) != 0 ) { 
    LIBRT_FinalizarDispositivo(&h);
    ret = ERR_CL;
    goto endCLDEL_CertKeys;
  }
  
  /* Ahora, buscamos todos los bloques que contengan este 
   * identificador y los borramos, excepto los key containers
   * en los cuales s�lo debemos modificar el puntero a la llave
   */
  
  if ( LIBRT_LeerTodosBloquesOcupados ( &h, NULL, NULL, &bNumbers ) != 0 ) {
    LIBRT_FinalizarDispositivo(&h);
    ret = ERR_CL;
    goto endCLDEL_CertKeys;
  }
  
  blocks = (unsigned char *) malloc (bNumbers * TAM_BLOQUE);
  
  if ( ! blocks ) {
    LIBRT_FinalizarDispositivo(&h);
    ret = ERR_CL_OUT_OF_MEMORY;
    goto endCLDEL_CertKeys;
  }

  auxBlocks = blocks;
  
  hBlock = ( long * ) malloc ( sizeof(long) * bNumbers );
  
  if ( ! hBlock ) {
    LIBRT_FinalizarDispositivo(&h);
    ret = ERR_CL_OUT_OF_MEMORY;
    goto endCLDEL_CertKeys;
  }
  
  if ( LIBRT_LeerTodosBloquesOcupados ( &h, hBlock, blocks, &bNumbers ) != 0 ) {
    LIBRT_FinalizarDispositivo(&h);
    ret = ERR_CL;
    goto endCLDEL_CertKeys;
  }
  
  for ( i = 0 ; i < bNumbers ; i++ ) {
    type = *(blocks+1);
    if ( type ==  BLOQUE_KEY_CONTAINERS ){
      rewrite= 0;
      if ( BLOQUE_KeyContainer_Enumerar(blocks, lstContainer, &lstContainerSize) != 0 ) {
	LIBRT_FinalizarDispositivo(&h);
	ret = ERR_CL;
	goto endCLDEL_CertKeys;
      }
      for ( j = 0 ; j < lstContainerSize ; j++ ) {
	if ( memcmp(lstContainer[j].idExchange, id, 20) == 0 ) {
	  if ( memcmp(lstContainer[j].idSignature, id, 20) == 0 || 
	       memcmp(lstContainer[j].idSignature, zero, 20) == 0) {
	    if ( BLOQUE_KeyContainer_Borrar ( blocks , lstContainer[j].nombreKeyContainer ) != 0 ){
	      LIBRT_FinalizarDispositivo(&h);
	      ret = ERR_CL;
	      goto endCLDEL_CertKeys;
	    }
	    else {
	      rewrite= 1; 
	    }
	  }
	  else{
	    if (  BLOQUE_KeyContainer_Establecer_ID_Exchange ( blocks, lstContainer[j].nombreKeyContainer, zero ) != 0  ){
	      LIBRT_FinalizarDispositivo(&h);
	      ret = ERR_CL;
	      goto endCLDEL_CertKeys;
	    }
	    else{
	      rewrite= 1; 
	    }
	  }
	} else if ( memcmp(lstContainer[j].idSignature, id, 20) == 0 ) {
	  if ( memcmp(lstContainer[j].idExchange , id, 20) == 0 || 
	       memcmp(lstContainer[j].idExchange , zero, 20) == 0) {
	    if ( BLOQUE_KeyContainer_Borrar ( blocks , lstContainer[j].nombreKeyContainer ) != 0 ){
	      LIBRT_FinalizarDispositivo(&h);
	      ret = ERR_CL;
	      goto endCLDEL_CertKeys;
	    }
	    else {
	      /* Es necesario reescribir el bloque en el clauer */
	      rewrite= 1; 
	    }
	  }
	  else{
	    if (  BLOQUE_KeyContainer_Establecer_ID_Signature ( blocks, lstContainer[j].nombreKeyContainer, zero ) != 0 ){
	      LIBRT_FinalizarDispositivo(&h);
	      ret = ERR_CL;
	      goto endCLDEL_CertKeys;
	    }
	    else{
	      rewrite= 1;	
	    }
	  }
	}
      }
      if ( rewrite == 1 ){
	if ( BLOQUE_KeyContainer_Enumerar(blocks, lstContainer, &lstContainerSize) != 0 ) {
	  LIBRT_FinalizarDispositivo(&h);
	  ret = ERR_CL;
	  goto endCLDEL_CertKeys;
	}
	
	if ( lstContainerSize == 0 ){
	  if ( LIBRT_BorrarBloqueCrypto ( &h, hBlock[i] ) != 0 ) { 
	    LIBRT_FinalizarDispositivo(&h);
	    ret = ERR_CL;
	    goto endCLDEL_CertKeys;
	  }
	}
	else{
	  if ( LIBRT_EscribirBloqueCrypto ( &h, hBlock[i], blocks ) != 0 ){
	    ret = ERR_CL;
	    goto endCLDEL_CertKeys;
	  }
	}
      }
    }
    else {
      found= 0;
      switch ( type ) {
      case BLOQUE_LLAVE_PRIVADA:
	if ( memcmp(id, BLOQUE_LLAVEPRIVADA_Get_Id(blocks), 20 ) == 0 ){
	  found= 1;
	}
	break;
	
      case BLOQUE_PRIVKEY_BLOB:
	if ( memcmp(id, BLOQUE_PRIVKEYBLOB_Get_Id(blocks), 20 ) == 0 ){
	  found= 1;
	}
	break;
      }
      if ( found ){
	if ( LIBRT_BorrarBloqueCrypto ( &h, hBlock[i] ) != 0 ) { 
	  LIBRT_FinalizarDispositivo(&h);
	  ret = ERR_CL;
	  goto endCLDEL_CertKeys;
	}
      }
    }
    blocks += TAM_BLOQUE;
  }

  LIBRT_FinalizarDispositivo(&h);

 endCLDEL_CertKeys:

  printf("1\n");

  if ( auxBlocks ) {
    SecureZeroMemory(auxBlocks, bNumbers*TAM_BLOQUE);
    free(blocks);
  }

  printf("2\n");

  if ( hBlock ) {
    SecureZeroMemory(hBlock, bNumbers * sizeof(long));
    free(blocks);
  }

  printf("3\n");

  
  return ret;
}



#ifdef __TEST__

int main ( void ) {

  char szDev[] = "c:\\cryf_000.cla";
  char szPwd[] = "123clauer";
  int ret;

  LIBRT_Ini();
  CRYPTO_Ini();

  ret = CLDEL_CertKeys(szDev, szPwd, 0);

  if ( ret == ERR_CL_BAD_BLOCK_TYPE ) {
    printf("[+] CLDEL_CertKeys Ok\n");
  } else {
    printf("[-] CLDEL_CertKeys: ret = %d\n", ret);
    return;
  }
  fflush(stdout);

  ret = CLDEL_CertKeys(szDev, szPwd, 3);

  if ( ret == CL_SUCCESS ) {
    printf("[+] Trying to delete cert && keys on a cert block\n");
  } else {
    printf("[-] Trying to delete cert && keys on a cert block: %d\n", ret);
    return;
  }

  

  return 0;
}




int CLDEL_Object ( char *szDevice,
		   char *szPwd,
		   long bn )
{
  USBCERTS_HANDLE hClauer;

  int ret = CL_SUCCESS;

  if ( ! szDevice ) 
    return ERR_BAD_PARAM;
  if ( ! szPwd ) 
    return ERR_BAD_PARAM;
  if ( bn < 0 ) 
    return ERR_BAD_PARAM;

  if ( LIBRT_IniciarDispositivo(szDevice, szPwd, &hClauer) != 0 ) {
    ret = ERR_CL_CANNOT_INITIALIZE;
    goto endCLDEL_Object;
  }

  if ( LIBRT_BorrarBloqueCrypto(&hClauer, bn) != 0 ) {
    LIBRT_FinalizarDispositivo(&hClauer);
    ret = ERR_CL;
    goto endCLDEL_Object;
  }

  LIBRT_FinalizarDispositivo(&hClauer);

 endCLDEL_Object:


  return ret;
}







#endif



