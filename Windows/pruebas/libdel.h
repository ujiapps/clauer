#ifndef __CLAUER_LIBDEL_H__
#define __CLAUER_LIBDEL_H__


#include "err.h"

#ifdef __cplusplus
extern "C" {
#endif


int CLDEL_CertKeys ( char *szDevice,
		     char *pwd,
		     long bn );

  int CLDEL_Object ( char *szDevice,
		     char *szPwd,
		     long bn );
  

#ifdef __cplusplus
}
#endif


#endif
