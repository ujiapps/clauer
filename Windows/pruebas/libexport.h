#ifndef __CLAUER_LIBEXPORT_H__
#define __CLAUER_LIBEXPORT_H__

#include "err.h"

#ifdef __cplusplus
extern "C" {
#endif

/*! \brief Exports a pkcs#12 from the clauer.
 *
 * Exports a pkcs#12 from the clauer.
 *
 * \param szDevice
 *        The clauer source
 *
 * \param szPwd
 *        The clauer's password
 *
 * \param bn
 *        The clauer's block number
 *
 * \param p12
 *        [OUT] The pkcs12
 * 
 * \param p12Size
 *        [IN/OUT] p12 size in bytes
 *
 * \retval 0
 *         Ok
 *
 * \retval != 0
 *         Error
 */

int CLEXPORT_P12 ( char *szDevice, 
		   char *szPwd, 
		   long bn, 
		   char *szP12Pwd,
		   unsigned char *p12, 
		   unsigned long *p12Size );





int CLEXPORT_CRYPTO ( char *szDevice, char *szOutFile );


#ifdef __cplusplus
}
#endif

#endif
