#include "misc.h"

#include <Winhttp.h>
#include <Sddl.h>
#include "common_defines.h"

#include <stdio.h>

#include <log/log.h>

extern "C" {
extern int g_bClauerLOG;
}

/* Indica si el servicio debe indicar o no reinicio del sistema
 */

BOOL CLUPDATE_HasToReboot ( void )
{
	HKEY hKey = 0;
	DWORD dwReboot = 0, dwSize = 0, dwType = 0;

	if ( RegOpenKeyEx(HKEY_LOCAL_MACHINE, _T("Software\\Universitat Jaume I\\Projecte Clauer"), 0, KEY_READ, &hKey) != ERROR_SUCCESS ) {
		return FALSE;
	}
	if ( RegQueryValueEx(hKey, _T("REBOOT"), NULL, &dwType, NULL, &dwSize) != ERROR_SUCCESS ) {
		RegCloseKey(hKey);
		return FALSE;
	}
	if ( dwType != REG_DWORD || dwSize != sizeof(DWORD) ) {
		RegCloseKey(hKey);
		return FALSE;
	}
	if ( RegQueryValueEx(hKey, _T("REBOOT"), NULL, &dwType, (LPBYTE) &dwReboot, &dwSize) != ERROR_SUCCESS ) {
		RegCloseKey(hKey);
		return FALSE;
	}
	RegCloseKey(hKey);

	return dwReboot == 1;
}

/* SERVER_ERROR ---> Error
 * SERVER_OK --> Ok
 * SERVER_UP ---> Update
 * SERVER_NEW ---> New
 */

int CLUPDATE_AskServer (void)
{
    DWORD dwSize = 0;
    DWORD dwDownloaded = 0;
	DWORD dwTotal = 0;
    BYTE *pszOutBuffer = NULL, *aux = NULL;
    BOOL  bResults = FALSE;
    HINTERNET  hSession = NULL, 
               hConnect = NULL,
               hRequest = NULL;

	char *URLBytes = NULL;
	WCHAR *url = NULL;
	int ret = SERVER_OK;

	LOG_BeginFunc(1);

    // Use WinHttpOpen to obtain a session handle.

    hSession = WinHttpOpen( L"Clauer Update/2.0",  
                            WINHTTP_ACCESS_TYPE_DEFAULT_PROXY,
                            WINHTTP_NO_PROXY_NAME, 
                            WINHTTP_NO_PROXY_BYPASS, 0);
////LOG_Mark();
	if ( ! hSession ) {
		ret = SERVER_ERROR;
		goto finPRG_ConectaServidor;
	}	

////LOG_Mark();
	/* Establecemos los diversos timeouts:
	 *
	 *      - Timeout para resolver nombres: 10s
	 *      - Timeout conexi�n al servidor: 10s
	 *		- Timeout para el env�o de la petici�n: 10s
	 *      - Timeout para la respuesta: 10s
	 */

	if ( ! WinHttpSetTimeouts(hSession, 10000, 10000, 10000, 10000) ) {
		ret = SERVER_ERROR;
		goto finPRG_ConectaServidor;
	}

////LOG_Mark();

    // Specify an HTTP server

    hConnect = WinHttpConnect( hSession, CLUPDATE_NOTIFY_SERVER,
                               INTERNET_DEFAULT_HTTP_PORT, 0);

////LOG_Mark();
	if ( !hConnect ) {
		ret = SERVER_ERROR;
		goto finPRG_ConectaServidor;
	}

////LOG_Mark();
	URLBytes = (char *) malloc ( strlen(CLUPDATE_NOTIFY_PRG_NAME) + VERSION_LEN + strlen(CLUPDATE_NOTIFY_TEMPLATE_PRINTF) + 1 );
////LOG_Mark();
	if ( ! URLBytes ) {
////LOG_Mark();
		ret = SERVER_ERROR;
		goto finPRG_ConectaServidor;
	}

////LOG_Mark();
	url = (WCHAR *) malloc ( sizeof(WCHAR) * (strlen(CLUPDATE_NOTIFY_PRG_NAME) + strlen(CLUPDATE_NOTIFY_TEMPLATE) + 1));
////LOG_Mark();
	if ( !url ) {
		////LOG_Mark();
		ret = SERVER_ERROR;
		goto finPRG_ConectaServidor;
	}
////LOG_Mark();
	/* Obtengo la versi�n del software del clauer. Si no pudiera obtenerla
	 * indico que el software est� caducado poniendo una fecha irrancional :-)
	 */

	char version[VERSION_LEN+1];
	DWORD dwType, dwVersionSize;
	HKEY hKey;

////LOG_Mark();
	if ( RegOpenKeyExA(HKEY_LOCAL_MACHINE, "Software\\Universitat Jaume I\\Projecte Clauer", 0, KEY_READ, &hKey) == ERROR_SUCCESS ) {
		////LOG_Mark();
		if ( RegQueryValueExA(hKey, "VERSION", NULL, &dwType, NULL, &dwVersionSize) == ERROR_SUCCESS ) {
			////LOG_Mark();
			if ( dwType == REG_SZ && dwVersionSize == (VERSION_LEN+1) ) {
				////LOG_Mark();
				if ( RegQueryValueExA(hKey, "VERSION", NULL, &dwType, (LPBYTE) version, &dwVersionSize) != ERROR_SUCCESS ) {
					////LOG_Mark();
					memset(version, '0', sizeof version - 1);
				}
			} else {
				////LOG_Mark();
				memset(version, '0', sizeof version - 1);				
			}
		} else {
			////LOG_Mark();
			memset(version, '0', sizeof version - 1);
		}
	} else {
		////LOG_Mark();
		memset(version, '0', sizeof version - 1);
	}

	version[VERSION_LEN] = 0;
	LOG_Debug(1, "VERSION: %s", version);
	sprintf(URLBytes, CLUPDATE_NOTIFY_TEMPLATE_PRINTF, CLUPDATE_NOTIFY_PRG_NAME, version);

	if ( ! MultiByteToWideChar(CP_ACP,MB_PRECOMPOSED, URLBytes, -1, url, strlen(CLUPDATE_NOTIFY_PRG_NAME) + strlen(CLUPDATE_NOTIFY_TEMPLATE) + 1) ) {
		ret = SERVER_ERROR;
		goto finPRG_ConectaServidor;
	}

    // Create an HTTP request handle.

    hRequest = WinHttpOpenRequest( hConnect, L"GET", url,
                                   NULL, WINHTTP_NO_REFERER, 
                                   WINHTTP_DEFAULT_ACCEPT_TYPES, 
                                   0);

	if ( !hRequest ) {
		ret = SERVER_ERROR;
		goto finPRG_ConectaServidor;
	}

    // Send a request.

    bResults = WinHttpSendRequest( hRequest,
                                   WINHTTP_NO_ADDITIONAL_HEADERS, 0,
                                   WINHTTP_NO_REQUEST_DATA, 0, 
                                   0, 0);

	if ( ! bResults ) {
		ret = SERVER_ERROR;
		goto finPRG_ConectaServidor;
	}
    // End the request.

    bResults = WinHttpReceiveResponse( hRequest, NULL);
	if ( !bResults ) {
		ret = SERVER_ERROR;
		goto finPRG_ConectaServidor;
	}

	pszOutBuffer = (BYTE *) malloc (1024);
	if  ( ! pszOutBuffer ) {
		ret = SERVER_ERROR;
		goto finPRG_ConectaServidor;
	}
	ZeroMemory(pszOutBuffer, 1024);
	aux = pszOutBuffer;

    // Keep checking for data until there is nothing left.

    do 
    {
        // Check for available data.

        dwSize = 0;
        if (!WinHttpQueryDataAvailable( hRequest, &dwSize)) {
			ret = SERVER_ERROR;
			goto finPRG_ConectaServidor;
		}

        // Read the Data.

		if ( (dwTotal + dwSize) >= (1024 + 1) ) 
			break;
		
        ZeroMemory(aux, dwSize+1);
        if (!WinHttpReadData( hRequest, (LPVOID)aux, dwSize, &dwDownloaded)) {
			ret = SERVER_ERROR;
			goto finPRG_ConectaServidor;
		}

       
		dwTotal += dwDownloaded;
		if ( dwTotal >= (1024 + 1) ) {
			dwSize = 0;
		} else {
			aux += dwDownloaded;
		}

    } while (dwSize > 0);

	if ( memcmp(pszOutBuffer, "OK", 2) == 0 ) {
		ret = SERVER_OK;
	} else if ( memcmp(pszOutBuffer, "UP", 2) == 0 ) {
		ret = SERVER_UP;
	} else if ( memcmp(pszOutBuffer, "NW", 2) == 0 ) {
		ret = SERVER_NEW;
	} else if ( memcmp(pszOutBuffer, "RE", 2) == 0 ) {
		ret = SERVER_RELEASE;
	} else {
		ret = SERVER_OK;
	}

finPRG_ConectaServidor:

	if (hRequest ) {
		WinHttpCloseHandle(hRequest);
	}

	if (hConnect) {
		WinHttpCloseHandle(hConnect);
	}

	if (hSession) {
		WinHttpCloseHandle(hSession);
	}

	if ( URLBytes ) {
		free(URLBytes);
	}
	 
	if ( url ) {
		free(url); 
	}

	if ( pszOutBuffer ) {
		free(pszOutBuffer);
	}

	LOG_EndFunc(1, ret);

	return ret;
}











#define CLUPDATE_DOWNLOAD_BUFFER_SIZE	10240

/* Descarga la actualizaci�n y la almacena en el fichero especificado por szOutFileName.
 * Las medidas de protecci�n sobre el fichero son las siguientes:
 *
 *         1� Se establece una ACL que permite lectura y ejecuci�n para administradores y local system
 *            control total y deniega el acceso al resto de usuarios
 *
 *         2� Deja el handle del fichero abierto en modo exclusivo, de esta forma evitamos que pueda ser
 *            sobreescrito.
 *
 * IMPORTANTE. Desde fuera se debe llamar a CloseHandle(hFile)
 */

BOOL CLUPDATE_DOWNLOAD_Setup ( TCHAR szOutFileName[MAX_PATH+1], HANDLE *hFile )
{
	BOOL ret = TRUE;

	DWORD dwSize = 0, dwDownloaded = 0, dwTotal = 0;
    BYTE *pszOutBuffer = NULL;
    HINTERNET  hSession = NULL, hConnect = NULL, hRequest = NULL;

	WCHAR url[] = CLUPDATE_DOWNLOAD_URL;
	DWORD dwBytesWritten, dwBytesToRead;

	TCHAR szSecurityDescriptor[] = _T("D:")
		_T("(A;OICI;GRGX;;;BA)")	  // Administradores lectura y ejecuci�n
		_T("(A;OICI;GA;;;SY)")	      // Local System control total
		_T("(D;OICI;GA;;;WD)");	      // Resto del mundo nada
	PSECURITY_ATTRIBUTES sa = NULL;

	LOG_BeginFunc(5);

	if ( ! hFile ) {
			return FALSE;
	}
	if ( ! szOutFileName ){
			return FALSE;
	}

	*hFile = 0;

	sa = new SECURITY_ATTRIBUTES;
	if ( ! sa ) {
		ret = FALSE;
		goto finCLUPDATE_DOWNLOAD_Setup;
	}

	ZeroMemory(sa, sizeof(SECURITY_ATTRIBUTES));

	sa->nLength        = sizeof(SECURITY_ATTRIBUTES);
	sa->bInheritHandle = FALSE;

	if ( ! ConvertStringSecurityDescriptorToSecurityDescriptor(szSecurityDescriptor,
															  SDDL_REVISION_1,
															  &(sa->lpSecurityDescriptor),
															  NULL) )
	{
		LOG_Error(1,"ConvertStringSecurityDescriptorToSecurityDescriptor: %ld", GetLastError());
		ret = FALSE;
		goto finCLUPDATE_DOWNLOAD_Setup;
	}

	*hFile = CreateFile(szOutFileName, GENERIC_WRITE, 0, sa, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, 0);

	if ( hFile == INVALID_HANDLE_VALUE ) {
		LOG_Error(1, "Creating updating file: %ld", GetLastError());
		*hFile = 0;
		ret = FALSE;
		goto finCLUPDATE_DOWNLOAD_Setup;
	}

	// Use WinHttpOpen to obtain a session handle.

    hSession = WinHttpOpen( L"Clauer Update/2.0",  
                            WINHTTP_ACCESS_TYPE_DEFAULT_PROXY,
                            WINHTTP_NO_PROXY_NAME, 
                            WINHTTP_NO_PROXY_BYPASS, 0);

	if ( ! hSession ) {
		LOG_Error(1,"WinHttpOpen: %ld", GetLastError());
		ret = FALSE;
		goto finCLUPDATE_DOWNLOAD_Setup;
	}	

	/* Establecemos los diversos timeouts:
	 *
	 *      - Timeout para resolver nombres: 5s
	 *      - Timeout conexi�n al servidor: 2s
	 *		- Timeout para el env�o de la petici�n: 2s
	 *      - Timeout para la respuesta: 2s
	 */

	if ( ! WinHttpSetTimeouts(hSession, 10000, 10000, 10000, 10000) ) {
		//LOG_Mark();
		ret = FALSE;
		goto finCLUPDATE_DOWNLOAD_Setup;
	}

	// Specify an HTTP server.
//LOG_Mark();
    hConnect = WinHttpConnect( hSession, CLUPDATE_DOWNLOAD_SERVER,
                               INTERNET_DEFAULT_HTTP_PORT, 0);
//LOG_Mark();
	if ( !hConnect ) {
		//LOG_Mark();
		ret = FALSE;
		goto finCLUPDATE_DOWNLOAD_Setup;
	}
//LOG_Mark();
    // Create an HTTP request handle.

    hRequest = WinHttpOpenRequest( hConnect, L"GET", url,
                                   NULL, WINHTTP_NO_REFERER, 
                                   WINHTTP_DEFAULT_ACCEPT_TYPES, 
                                   0);
//LOG_Mark();
	if ( !hRequest ) {
		//LOG_Mark();
		ret = FALSE;
		goto finCLUPDATE_DOWNLOAD_Setup;
	}
//LOG_Mark();
    // Send a request.

    if ( ! WinHttpSendRequest( hRequest,
                               WINHTTP_NO_ADDITIONAL_HEADERS, 0,
                               WINHTTP_NO_REQUEST_DATA, 0, 
							   0, 0) )
	{
		//LOG_Mark();
		ret = FALSE;
		goto finCLUPDATE_DOWNLOAD_Setup;
	}
 //LOG_Mark();
    // Obtenemos la respuesta

	if ( ! WinHttpReceiveResponse( hRequest, NULL) ) {
		//LOG_Mark();
		ret = FALSE;
		goto finCLUPDATE_DOWNLOAD_Setup;
	}

//LOG_Mark();
	pszOutBuffer = new BYTE [ CLUPDATE_DOWNLOAD_BUFFER_SIZE ];
//LOG_Mark();
	if ( ! pszOutBuffer ) {
		//LOG_Mark();
		ret = FALSE;
		goto finCLUPDATE_DOWNLOAD_Setup;
	}

	//LOG_Mark();

    // Keep checking for data until there is nothing left.

    dwSize = 0;
    if ( ! WinHttpQueryDataAvailable( hRequest, &dwSize) ) {
		//LOG_Mark();
		ret = FALSE;
		goto finCLUPDATE_DOWNLOAD_Setup;
	}
//LOG_Mark();
    while ( dwSize > 0 )
    {
        // Read the Data.
		
//LOG_Mark();
		dwBytesToRead = (dwSize > CLUPDATE_DOWNLOAD_BUFFER_SIZE) ? CLUPDATE_DOWNLOAD_BUFFER_SIZE : dwSize;
//LOG_Mark();
		while ( dwSize > 0 ) {
//LOG_Mark();
			if ( ! WinHttpReadData( hRequest, pszOutBuffer, dwBytesToRead, &dwDownloaded)) {
				//LOG_Mark();
				ret = FALSE;
				goto finCLUPDATE_DOWNLOAD_Setup;
			}
//LOG_Mark();
			if ( ! WriteFile(*hFile, pszOutBuffer, dwDownloaded, &dwBytesWritten, NULL) ) {
				//LOG_Mark();
				DWORD err = GetLastError();
				ret = FALSE;
				goto finCLUPDATE_DOWNLOAD_Setup;
			}
//LOG_Mark();
			dwSize  -= dwDownloaded;
			dwTotal += dwDownloaded;
		}
//LOG_Mark();
		if ( ! WinHttpQueryDataAvailable( hRequest, &dwSize) ) {
			//LOG_Mark();
			ret = FALSE;
			goto finCLUPDATE_DOWNLOAD_Setup;
		}    

//LOG_Mark();
    }

//LOG_Mark();
finCLUPDATE_DOWNLOAD_Setup:


//LOG_Mark();
	if ( !ret && *hFile ) {
		//LOG_Mark();
		CloseHandle(*hFile);
		//LOG_Mark();
		*hFile = 0;
		//LOG_Mark();
		DeleteFile(szOutFileName);
		//LOG_Mark();
	}
//LOG_Mark();
	if ( sa ) {
		//LOG_Mark();
		if ( sa->lpSecurityDescriptor ) {
			//LOG_Mark();
			LocalFree(sa->lpSecurityDescriptor);
			//LOG_Mark();
		}
		//LOG_Mark();
		delete [] sa;
		//LOG_Mark();
	}
//LOG_Mark();

if (hRequest) {
	//LOG_Mark();
		WinHttpCloseHandle(hRequest);
		//LOG_Mark();
}

//LOG_Mark();

if (hConnect) {
//LOG_Mark();
		WinHttpCloseHandle(hConnect);
//LOG_Mark();
}

if (hSession) {
	//LOG_Mark();
		WinHttpCloseHandle(hSession);
		//LOG_Mark();
}

if ( pszOutBuffer ) {
		//LOG_Mark();
		delete [] pszOutBuffer;
		//LOG_Mark();
}

	LOG_EndFunc(5, ret);

	return ret;

}







BOOL CLUPDATE_Get_AskingPeriod ( DWORD *dwAskingPeriod )
{
	HKEY hKey;
	DWORD dwType, dwSize;

	LOG_BeginFunc(5);

	if ( ! dwAskingPeriod ) {
		//LOG_Mark();
		return FALSE;
	}

	//LOG_Mark();

	if ( RegOpenKeyEx(HKEY_LOCAL_MACHINE, _T("Software\\Universitat Jaume I\\Projecte Clauer"), 0, KEY_READ, &hKey) != ERROR_SUCCESS ) {
		//LOG_Mark();
		*dwAskingPeriod = DEFAULT_ASKING_PERIOD;
		return TRUE;
	}
	if ( RegQueryValueEx(hKey, _T("PERIOD"), NULL, &dwType, NULL, &dwSize) != ERROR_SUCCESS ) {
		//LOG_Mark();
		RegCloseKey(hKey);
		//LOG_Mark();
		*dwAskingPeriod = DEFAULT_ASKING_PERIOD;
		return TRUE;
	}
	//LOG_Mark();
	if ( dwSize != sizeof(DWORD) || dwType != REG_DWORD ) {
		//LOG_Mark();
		RegCloseKey(hKey);
		//LOG_Mark();
		return TRUE;
	}
	//LOG_Mark();
	if ( RegQueryValueEx(hKey, _T("PERIOD"), NULL, &dwType, (LPBYTE) dwAskingPeriod, &dwSize) != ERROR_SUCCESS ) {
		//LOG_Mark();
		RegCloseKey(hKey);
		//LOG_Mark();
		*dwAskingPeriod = DEFAULT_ASKING_PERIOD;
		//LOG_Mark();
		return TRUE;
	}
	//LOG_Mark();
	RegCloseKey(hKey);
	//LOG_Mark();
	if ( *dwAskingPeriod != 0 ) {
		//LOG_Mark();
		if ( *dwAskingPeriod < MINIMUM_ASKING_PERIOD ) {
			//LOG_Mark();
			*dwAskingPeriod = DEFAULT_ASKING_PERIOD;
			//LOG_Mark();
		} else if ( *dwAskingPeriod > MAXIMUM_ASKING_PERIOD ){
			//LOG_Mark();
			*dwAskingPeriod = DEFAULT_ASKING_PERIOD;
			//LOG_Mark();
		}
	}
//LOG_Mark();


	LOG_EndFunc(5, TRUE);

	return TRUE;
}






BOOL CLUPDATE_Get_UpdateMode ( UPDATE_MODE *updMode )
{
	DWORD dwType, dwSize, dwMode;
	HKEY hKey;

	LOG_BeginFunc(10);

	if ( ! updMode ) {
		LOG_MsgError(1, "updMode is NULL");
		return FALSE;
	}

	if ( RegOpenKeyEx(HKEY_LOCAL_MACHINE, _T("Software\\Universitat Jaume I\\Projecte Clauer"), 0, KEY_READ, &hKey) != ERROR_SUCCESS ) {
		LOG_Error(1, "Cannot open Software\\Universitat Jaume I\\Projecte Clauer: %ld", GetLastError());
		return FALSE;
	}

	if ( RegQueryValueEx(hKey, _T("UPDATE_MODE"), NULL, &dwType, NULL, &dwSize) != ERROR_SUCCESS ) {
		LOG_Error(1, "Cannot get value UPDATE_MODE: %ld", GetLastError());
		RegCloseKey(hKey);
		return FALSE;
	}	

	if ( dwType != REG_DWORD || dwSize != sizeof(DWORD) ) {
		LOG_MsgError(1, "PERIOD isn't of the type expected or its size is greater");
		RegCloseKey(hKey);
		return FALSE;
	}

	if ( RegQueryValueEx(hKey, _T("UPDATE_MODE"), NULL, &dwType, (LPBYTE) &dwMode, &dwSize) != ERROR_SUCCESS ) {
		LOG_Error(1, "Cannot get value UPDATE_MODE: %ld", GetLastError());
		RegCloseKey(hKey);
		return FALSE;
	}

	RegCloseKey(hKey);

	if ( dwMode < 0 || dwMode > 3 ) {
		LOG_Error(1, "Mode obtained is not valid: %ld", dwMode);
		return FALSE;
	}

	*updMode = (UPDATE_MODE) dwMode;

	LOG_EndFunc(1, TRUE);

	return TRUE;
}




BOOL CLUPDATE_Reboot ( void )
{
   HANDLE hToken;             
   TOKEN_PRIVILEGES tkp;       
 
   BOOL fResult;             

   if ( ! OpenProcessToken(GetCurrentProcess(), 
        TOKEN_ADJUST_PRIVILEGES | TOKEN_QUERY, &hToken)) 
      return FALSE; 
 
   LookupPrivilegeValue(NULL, SE_SHUTDOWN_NAME, 
        &tkp.Privileges[0].Luid); 
 
   tkp.PrivilegeCount = 1;  // one privilege to set    
   tkp.Privileges[0].Attributes = SE_PRIVILEGE_ENABLED; 
 
   AdjustTokenPrivileges(hToken, FALSE, &tkp, 0, 
      (PTOKEN_PRIVILEGES) NULL, 0); 
 
   if (GetLastError() != ERROR_SUCCESS) 
      return FALSE; 
 
   fResult = InitiateSystemShutdown( 
      NULL,    
      NULL,    
      0,       
      FALSE,   
      TRUE);   
 
   if (!fResult) 
      return FALSE; 
 
   tkp.Privileges[0].Attributes = 0; 
   AdjustTokenPrivileges(hToken, FALSE, &tkp, 0, 
        (PTOKEN_PRIVILEGES) NULL, 0); 
 
   return TRUE; 
}
