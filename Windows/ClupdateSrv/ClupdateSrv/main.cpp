// clos-win.cpp : Defines the entry point for the console application.
//

#define WIN32_LEAN_AND_MEAN

#include <windows.h>
#include <winsvc.h>
#include <winuser.h>
#include <winbase.h>
#include <tchar.h>

#include <log/log.h>

#include <stdio.h>
#include <stdlib.h>

#include <shellapi.h>

#include "misc.h"
#include "ui.h"
#include "lang.h"


/* Some userful defines
 */

#define MY_SERVICE_DESCRIPTION		_T("Clauer Updating service")
#define MY_SERVICE_NAME				_T("ClupdateSrv")
#define MY_DEBUG_FILE_NAME			"C:\\ClupdateSrv.log"


SERVICE_STATUS          g_MyServiceStatus; 
SERVICE_STATUS_HANDLE   g_MyServiceStatusHandle; 

static BOOL g_bParar;

extern "C" {
extern int g_bClauerLOG;
}

HMODULE g_hLangDll = 0;

/*
 * Prototipos de las funciones del servicio
 */

VOID  WINAPI MyServiceStart (DWORD dwArgc, LPTSTR *lpszArgv); 
DWORD MyServiceCtrlHandler (DWORD opcode,DWORD dwEventType,LPVOID lpEventData,LPVOID lpContext); 
DWORD MyServiceInitialization (DWORD argc, LPTSTR *argv, DWORD *specificError); 

void RunService       (void);
void InstallService   (void);
void UninstallService (void);
void ImprimirUso      (TCHAR *nombrePrograma);

int CLUPDATE_SRV_main ( void );



void InstallService()
{
	SERVICE_DESCRIPTION descripcionServicio;

	LOG_BeginFunc(10);

	SC_HANDLE serviceControlManager = OpenSCManager( 0, 0, SC_MANAGER_CREATE_SERVICE );

	if ( serviceControlManager )
	{
		TCHAR path[ MAX_PATH + 1 ];
		if ( GetModuleFileName( 0, path, sizeof(path)/sizeof(path[0]) ) > 0 )
		{

			SC_HANDLE service = CreateService( serviceControlManager,
							MY_SERVICE_NAME, MY_SERVICE_NAME,
							SERVICE_ALL_ACCESS, SERVICE_WIN32_OWN_PROCESS|SERVICE_INTERACTIVE_PROCESS,
							SERVICE_AUTO_START, SERVICE_ERROR_IGNORE, path,
							NULL, NULL, _T("HTTP\0HTTPFilter\0Dnscache\0Tcpip\0"), NULL, NULL );


			if ( ! service ) {
				LOG_Error(1, "Imposible instalar servicio: %ld", GetLastError());
				LOG_EndFunc(10, 1);
				exit(1);
			}

			/*
			 * Introducimos el texto descriptivo del servicio
			 */

			descripcionServicio.lpDescription = MY_SERVICE_DESCRIPTION;

			if ( ! ChangeServiceConfig2(service,SERVICE_CONFIG_DESCRIPTION, (LPVOID) &descripcionServicio) ) {
				LOG_Error(1, "Imposible establecer la descripci�n del servicio: %ld\n", GetLastError());
				LOG_EndFunc(10, 1);
				exit(1);
			}

			if ( service )
				CloseServiceHandle( service );
		}

		CloseServiceHandle( serviceControlManager );
	}

	LOG_EndFunc(10, 0);
}






void UninstallService()
{
	LOG_BeginFunc(10);

	SC_HANDLE serviceControlManager = OpenSCManager( 0, 0, SC_MANAGER_CONNECT );

	if ( serviceControlManager )
	{
		SC_HANDLE service = OpenService( serviceControlManager,
			MY_SERVICE_NAME, SERVICE_QUERY_STATUS | DELETE );
		if ( service )
		{
			SERVICE_STATUS serviceStatus;
			if ( QueryServiceStatus( service, &serviceStatus ) )
			{
				if ( serviceStatus.dwCurrentState == SERVICE_STOPPED )
					DeleteService( service );
			}

			CloseServiceHandle( service );
		}

		CloseServiceHandle( serviceControlManager );
	}

	LOG_EndFunc(10, 0);
}


 

int _tmain( int argc, TCHAR* argv[] )
{
	g_bClauerLOG = 1;
	LOG_Ini(LOG_WHERE_FILE, 10, MY_DEBUG_FILE_NAME );

	LOG_BeginFunc(10);

	if ( argc > 1 && lstrcmpi( argv[1], TEXT("/i") ) == 0 )
	{
		InstallService();
	}
	else if ( argc > 1 && lstrcmpi( argv[1], TEXT("/u") ) == 0 )
	{
		UninstallService();
	}
	else if ( argc > 1 && lstrcmpi( argv[1], TEXT("/h") ) == 0 )
	{
		ImprimirUso(argv[0]);
	}
	else {
		RunService();
	}

	LOG_EndFunc(10,0);

	return 0;
}



/* Punto de entrada al servicio
 */

void RunService(void) 
{ 
	LOG_BeginFunc(10);
    SERVICE_TABLE_ENTRY DispatchTable[] = 
    { 
        { MY_SERVICE_NAME, (LPSERVICE_MAIN_FUNCTION) MyServiceStart }, 
        { NULL,              NULL          } 
    }; 
    StartServiceCtrlDispatcher(DispatchTable);
	LOG_EndFunc(10, 0);
} 
 


VOID WINAPI MyServiceStart (DWORD dwArgc, LPTSTR *lpszArgv)
{
	int result;

	LOG_BeginFunc(10);
	g_bParar = FALSE;

	g_MyServiceStatus.dwServiceType             = SERVICE_WIN32_OWN_PROCESS |SERVICE_INTERACTIVE_PROCESS;
	g_MyServiceStatus.dwCurrentState            = SERVICE_START_PENDING;
	g_MyServiceStatus.dwControlsAccepted        = SERVICE_ACCEPT_SHUTDOWN | SERVICE_ACCEPT_STOP | SERVICE_ACCEPT_PAUSE_CONTINUE;
	g_MyServiceStatus.dwWin32ExitCode           = 0;
	g_MyServiceStatus.dwServiceSpecificExitCode = 0;
	g_MyServiceStatus.dwCheckPoint              = 0;
	g_MyServiceStatus.dwWaitHint                = 0;

	g_MyServiceStatusHandle = RegisterServiceCtrlHandlerEx( MY_SERVICE_NAME, (LPHANDLER_FUNCTION_EX) MyServiceCtrlHandler, NULL); 
 
    if (g_MyServiceStatusHandle == (SERVICE_STATUS_HANDLE)0) 
    {
		LOG_Error(1, "Cannot register service: %ld", GetLastError());
		LOG_EndFunc(10, -1);
        return; 
    } 

    // Initialization complete - report running status. 

    g_MyServiceStatus.dwCurrentState       = SERVICE_RUNNING; 
    g_MyServiceStatus.dwCheckPoint         = 0; 
    g_MyServiceStatus.dwWaitHint           = 0; 
 
	if ( ! SetServiceStatus (g_MyServiceStatusHandle, &g_MyServiceStatus) ) {
		LOG_Error(1, "Cannot set service status (RUNNING): %ld", GetLastError());
		LOG_EndFunc(10, -1);
		return;
	}

    // This is where the service does its work. 

	result = CLUPDATE_SRV_main();

	if ( result != 0 ) {	
		g_MyServiceStatus.dwCurrentState            = SERVICE_STOPPED;
		g_MyServiceStatus.dwCheckPoint              = 0; 
		g_MyServiceStatus.dwWaitHint                = 0;
		g_MyServiceStatus.dwWin32ExitCode           = ERROR_SERVICE_SPECIFIC_ERROR;
		g_MyServiceStatus.dwServiceSpecificExitCode = result;
	} else {
		g_MyServiceStatus.dwWin32ExitCode = 0;
		g_MyServiceStatus.dwCurrentState = SERVICE_STOPPED;
		g_MyServiceStatus.dwCheckPoint = 0;
		g_MyServiceStatus.dwWaitHint = 0;
	}

	if ( ! SetServiceStatus (g_MyServiceStatusHandle, &g_MyServiceStatus) ) {
		LOG_Error(1, "Cannot set service status (STOPPED): %ld", GetLastError());
		LOG_EndFunc(10, -1);
		return;
	}

	LOG_EndFunc(10, 0);

    return; 
}




DWORD MyServiceCtrlHandler (DWORD opcode,DWORD dwEventType,LPVOID lpEventData,LPVOID lpContext)
{
	LOG_BeginFunc(10);

    switch(opcode) {

		case SERVICE_CONTROL_PAUSE:
			LOG_Msg(10, "Recibido CONTROL_PAUSE");
			g_MyServiceStatus.dwCurrentState = SERVICE_PAUSED;
			break;

		case SERVICE_CONTROL_CONTINUE:
			LOG_Msg(10, "Recibido CONTROL_CONTINUE");
			g_MyServiceStatus.dwCurrentState = SERVICE_RUNNING;
			break;

		case SERVICE_CONTROL_STOP:
			LOG_Msg(10, "Recibido CONTROL_STOP");
			g_bParar = TRUE;

			g_MyServiceStatus.dwWin32ExitCode = 0;
			g_MyServiceStatus.dwCurrentState = SERVICE_STOPPED;
			g_MyServiceStatus.dwCheckPoint = 0;
			g_MyServiceStatus.dwWaitHint = 0;

			if ( ! SetServiceStatus(g_MyServiceStatusHandle, &g_MyServiceStatus) ) 
				LOG_Error(1, "Cannot set service status (STOPPED): %ld", GetLastError());
		
			LOG_EndFunc(10, 0);
			return NO_ERROR;

		case SERVICE_CONTROL_INTERROGATE: 
			// Fall through to send current status. 
			LOG_Msg(10, "SERVICE_CONTROL_INTERROGATE received");
			break; 

		default:
			LOG_Msg(10, "CALL_NOT_IMPLEMENTED");
			return ERROR_CALL_NOT_IMPLEMENTED;
    } 
 
	if ( ! SetServiceStatus (g_MyServiceStatusHandle,  &g_MyServiceStatus)) 
		LOG_MsgError(1, "Cannot set service status");
   
	LOG_EndFunc(10, 0);

    return NO_ERROR; 

}


void ImprimirUso (TCHAR *nombrePrograma)
{

	fprintf(stderr, "%s [/i] | [/u] | [/r]\n", nombrePrograma);
	fprintf(stderr, "\t. /i Instala el servicio\n");
	fprintf(stderr, "\t. /u Desinstala el servicio\n");
	fprintf(stderr, "\t. /r Ejecuta el servicio\n");

}



int CLUPDATE_SRV_main ( void )
{
	int serverResponse;
	TCHAR szSetupDir[MAX_PATH+17+1], szAux[MAX_PATH+17+4+1];
	DWORD dwAskingPeriod;
	UPDATE_MODE mode;
	BOOL bReboot, bRebootCancel = FALSE;

	HANDLE hOutFile = 0;

	LOG_BeginFunc(5);
	// Cargamos la DLL de idioma correspondiente

	HKEY hKey;
	DWORD dwType, dwSize;
	TCHAR idioma[10];

	if ( RegOpenKeyEx(HKEY_LOCAL_MACHINE, _T("Software\\Universitat Jaume I\\Projecte Clauer"), 0, KEY_READ, &hKey) != ERROR_SUCCESS ) {
		return 1;
	}
	if ( RegQueryValueEx(hKey, _T("Idioma"), NULL, &dwType, NULL, &dwSize) != ERROR_SUCCESS ) {
		RegCloseKey(hKey);
		return 1;		
	}
	if ( dwType != REG_SZ ) {
		RegCloseKey(hKey);
		return 1;				
	}
	if ( dwSize > 10*sizeof(TCHAR) ) {
		RegCloseKey(hKey);
		return 1;		
	}
	if ( RegQueryValueEx(hKey, _T("Idioma"), NULL, &dwType, (LPBYTE) idioma, &dwSize) != ERROR_SUCCESS ) {
		RegCloseKey(hKey);
		return 1;		
	}
	RegCloseKey(hKey);
	if ( _tcsncmp(idioma, _T("1027"), 10) == 0 ) {
		/* catal�n */
		g_hLangDll = LoadLibrary(_T("va_language.dll"));
		if ( ! g_hLangDll ) {
			return 1;
		}
	} else if ( _tcsncmp(idioma, _T("1027"), 10) == 0 ) { 
		g_hLangDll = LoadLibrary(_T("ar_language.dll"));
		if ( ! g_hLangDll ) {
			return 1;
		}
	
	} else {
		/* espa�ol */

		g_hLangDll = LoadLibrary(_T("es_language.dll"));
		if ( ! g_hLangDll ) {
			return 1;
		}
	}

	if ( ! GetTempPath(MAX_PATH, szSetupDir) ) {
		LOG_Error(1, "Cannot determine temp path: %ld", GetLastError());
		return 1;
	}
	_tcscat(szSetupDir, _T("update-clauer.exe"));


	do {
		try {

			if ( ! bRebootCancel ) {
				if ( CLUPDATE_HasToReboot() ) {
					bReboot = CLUPDATESRV_Show_Reboot();
					if ( bReboot ) {
						if ( CLUPDATE_Reboot() )
							return 0;
					} else
						bRebootCancel = TRUE;
				}
			}

			if ( ! CLUPDATE_Get_UpdateMode(&mode) ) {
				LOG_MsgError(1, "Cannot get update mode. Reinstall clauer software!!");
				return 1;
			}
			if ( mode != MODE_AUTO ) {
				LOG_Msg(5, "Updating mode not in AUTOMATIC. Exiting");
				return 0;
			}

			switch ( serverResponse = CLUPDATE_AskServer() ) {

			case SERVER_UP:
				LOG_Msg(10, "Server response: UP");

			case SERVER_NEW:

				if ( serverResponse == SERVER_NEW )
					LOG_Msg(10, "Server response: NEW");
				if ( ! CLUPDATE_DOWNLOAD_Setup(szSetupDir, &hOutFile) ) {
					LOG_MsgError(1, "downloading setup");
					break;
				}
				STARTUPINFO si;
				PROCESS_INFORMATION pi;
				ZeroMemory(&si, sizeof si);	
				si.cb = sizeof si;
				ZeroMemory(&pi, sizeof pi);
				_tcscpy(szAux, szSetupDir);
				_tcscat(szAux, _T(" /S"));
				// Cerramos el handle del fichero de actualizaci�n para permitir el acceso y tal y cual...
				CloseHandle(hOutFile);
				if ( ! CreateProcess(szSetupDir, szAux, NULL, NULL, FALSE, 0, NULL, NULL, &si, &pi) ) 
					LOG_Error(1, "CreateProcess: %ld", GetLastError());

				break;

			case SERVER_ERROR:
				LOG_MsgError(1, "ERROR asking server for updates");
				break;

			case SERVER_OK:
				LOG_Msg(10, "Server response: OK");
				break;

			}
			CLUPDATE_Get_AskingPeriod(&dwAskingPeriod);
			LOG_Debug(10, "Asking Period: %ld", dwAskingPeriod);
		}
		catch (...) {
			LOG_MsgError(1, "Se produjo una excepci�n");
		}
		Sleep(dwAskingPeriod);
	} while ( dwAskingPeriod != 0 );

	LOG_Msg(5, "Exiting because asking period has been set to START...");

	return 0;
}

