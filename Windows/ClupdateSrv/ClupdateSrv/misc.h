#ifndef __CLUPDATE_MISC_H__
#define __CLUPDATE_MISC_H__

#include <windows.h>
#include <tchar.h>

#define VERSION_SIZE	10

#define SERVER_ERROR	-1
#define SERVER_OK		0
#define SERVER_UP		1
#define SERVER_NEW		2
#define SERVER_RELEASE	3


enum UPDATE_MODE { MODE_NORMAL, MODE_EXPERT, MODE_INI, MODE_AUTO};



int  CLUPDATE_AskServer        ( void );
BOOL CLUPDATE_DOWNLOAD_Setup   ( TCHAR szOutFileName[MAX_PATH+1], HANDLE *hFile );
BOOL CLUPDATE_Get_AskingPeriod ( DWORD *dwAskingPeriod );
BOOL CLUPDATE_Get_UpdateMode   ( UPDATE_MODE *updMode );

BOOL CLUPDATE_HasToReboot ( void );
BOOL CLUPDATE_Reboot      ( void );

#endif
