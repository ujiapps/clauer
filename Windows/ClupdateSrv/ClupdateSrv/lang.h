#ifndef __CLUPDATESRV_LANG__H__
#define __CLUPDATESRV_LANG__H__

#include <windows.h>
#include <tchar.h>
#include <lang/resource.h>

#define MAX_STRING_SIZE	500

LPTSTR GetClauerString ( UINT strId );

#endif
