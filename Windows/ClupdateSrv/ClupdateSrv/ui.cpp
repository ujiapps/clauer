#include <windows.h>
#include "ui.h"
#include "resource.h"
#include "lang.h"





INT_PTR CALLBACK CLUPDATESRV_DlgProc ( HWND hwndDlg,
									   UINT uMsg,
									   WPARAM wParam,
									   LPARAM lParam )
{
	INT_PTR ret = TRUE;

	int nDevs = 0;
	RECT dimDialog, dimDesktop;
	HWND hDesktop;

	switch ( uMsg ) {

	case WM_INITDIALOG:

		hDesktop = GetDesktopWindow();
		GetWindowRect(hDesktop,&dimDesktop);
		GetWindowRect(hwndDlg,&dimDialog);
		SetWindowPos(hwndDlg,
			HWND_TOP,
			((dimDesktop.right-dimDesktop.left)-(dimDialog.right-dimDialog.left))/2,
			((dimDesktop.bottom-dimDesktop.top)-(dimDialog.bottom-dimDialog.top))/2,
			0,
			0,
			SWP_NOSIZE);

		SetDlgItemText(hwndDlg, LBL_DOWNLOADED, GetClauerString(IDS_CLUPDATESRV_DOWNLOADED));
		SetDlgItemText(hwndDlg, LBL_MUST_REBOOT, GetClauerString(IDS_CLUPDATESRV_NEED_TO_REBOOT));
		SetDlgItemText(hwndDlg, BTN_REBOOT, GetClauerString(IDS_CLUPDATESRV_REBOOT));
		SetDlgItemText(hwndDlg, BTN_CANCEL, GetClauerString(IDS_CLUPDATESRV_CANCEL));

		return TRUE;

	case WM_CLOSE:

		EndDialog(hwndDlg, CLUPDATESRV_BUTTON_CANCEL);
		return TRUE;
		break;

	case WM_COMMAND:

		switch (LOWORD(wParam)) {
		case BTN_REBOOT:
			EndDialog(hwndDlg, CLUPDATESRV_BUTTON_REBOOT);
            return TRUE;

		case BTN_CANCEL:

			EndDialog(hwndDlg, CLUPDATESRV_BUTTON_CANCEL);
			return TRUE;
		}

		break;
	}



	return FALSE;


}


BOOL CLUPDATESRV_Show_Reboot ( void )
{
	INT_PTR ret;

	ret = DialogBox(GetModuleHandle(NULL), MAKEINTRESOURCE(DLG_REBOOT), NULL, CLUPDATESRV_DlgProc);	
	if ( ret == CLUPDATESRV_BUTTON_REBOOT )
		return TRUE;
	else
		return FALSE;

}
