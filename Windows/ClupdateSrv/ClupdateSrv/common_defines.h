/* Defines espec�ficos para cada release de clupdate
 */

#ifndef __COMMON_DEFINES_H__
#define __COMMON_DEFINES_H__

#define VERSION_LEN	10

/* Defines que afectan al per�odo de comprobaci�n de updates
 */

#define MINIMUM_ASKING_PERIOD	1800000  /* ( 30 minutes ) */
#define MAXIMUM_ASKING_PERIOD	86400000 /* ( 1 day ) */
#define DEFAULT_ASKING_PERIOD   3600000  /* ( 1 hora ) */

/* Defines dependientes de a qui�n se dirigen */

#ifdef CLUPDATE_PIPELINE

#define URL_HELP		_T("http://www.aavv.com/common/pi/gestion_usuarios/clauer/ayuda.html")

#define CLUPDATE_DOWNLOAD_SERVER	L"www.aavv.com"
#define CLUPDATE_DOWNLOAD_URL		L"common/pi/gestion_usuarios/clauer/setup-clauer-lu.exe"

#define CLUPDATE_NOTIFY_SERVER			L"www.aavv.com"
#define CLUPDATE_NOTIFY_TEMPLATE		"common/pi/gestion_usuarios/clauer/expire.php3?prg=&ver=0000000000"
#define CLUPDATE_NOTIFY_TEMPLATE_PRINTF	"common/pi/gestion_usuarios/clauer/expire.php3?prg=%s&ver=%s"
#define CLUPDATE_NOTIFY_PRG_NAME		"usbpki-base"

#elif CLUPDATE_CATCERT

#define URL_HELP	_T("http://clauer.uji.es/es/tutWin.html")

#define CLUPDATE_DOWNLOAD_SERVER	L"dwnl.nisu.org"
#define CLUPDATE_DOWNLOAD_URL		L"dwnl?fic=setup-clauer-idCAT.exe&dwnl=si"

#define CLUPDATE_NOTIFY_SERVER			L"expire-idcat.nisu.org"
#define CLUPDATE_NOTIFY_TEMPLATE		"?prg=&ver=0000000000"
#define CLUPDATE_NOTIFY_TEMPLATE_PRINTF	"?prg=%s&ver=%s"
#define CLUPDATE_NOTIFY_PRG_NAME		"usbpki-base"

#else

/*#define URL_HELP	_T("http://clauer.uji.es/es/tutWin.html")

#define CLUPDATE_DOWNLOAD_SERVER	L"juan.nisu.org"
#define CLUPDATE_DOWNLOAD_URL		L"test_clupdate/dwnl.php?fic=setup-clauer.exe&dwnl=si"

#define CLUPDATE_NOTIFY_SERVER			L"juan.nisu.org"
#define CLUPDATE_NOTIFY_TEMPLATE		"test_clupdate/index.php?prg=&ver=0000000000"
#define CLUPDATE_NOTIFY_TEMPLATE_PRINTF	"test_clupdate/index.php?prg=%s&ver=%s"
#define CLUPDATE_NOTIFY_PRG_NAME		"usbpki-base"
*/

#define URL_HELP	_T("http://clauer.uji.es/es/tutWin.html")

#define CLUPDATE_DOWNLOAD_SERVER	L"dwnl.nisu.org"
#define CLUPDATE_DOWNLOAD_URL		L"dwnl?fic=setup-clauer.exe&dwnl=si"

#define CLUPDATE_NOTIFY_SERVER			L"expire.nisu.org"
#define CLUPDATE_NOTIFY_TEMPLATE		"?prg=&ver=0000000000"
#define CLUPDATE_NOTIFY_TEMPLATE_PRINTF	"?prg=%s&ver=%s"
#define CLUPDATE_NOTIFY_PRG_NAME		"usbpki-base"

#endif

#endif

