//{{NO_DEPENDENCIES}}
// Microsoft Visual C++ generated include file.
// Used by ClupdateSrv.rc
//
#define IDI_ICON1                       102
#define IDI_UJI                         102
#define DLG_REBOOT                      103
#define IDB_CLUPDATESRV                 104
#define BTN_REBOOT                      1001
#define BTN_CANCEL                      1002
#define LBL_DOWNLOADED                  1003
#define LBL_MUST_REBOOT                 1004

// Next default values for new objects
// 
#ifdef APSTUDIO_INVOKED
#ifndef APSTUDIO_READONLY_SYMBOLS
#define _APS_NEXT_RESOURCE_VALUE        105
#define _APS_NEXT_COMMAND_VALUE         40001
#define _APS_NEXT_CONTROL_VALUE         1006
#define _APS_NEXT_SYMED_VALUE           101
#endif
#endif
