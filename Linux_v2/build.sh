#!/bin/bash 

# Clauer multidistro packager tool
# Paul Santapau <santapau@sg.uji.es>
# Tue Nov 11 19:26:56 CET 2008


VERSION=3.0.6
#PACKAGE="$PWD"

#Different  types for customizations  in clos.conf: 
custom=("uji" "accv" "catcert" "coitav" "uclm")


create_debian_files(){
cat >control<<-EOF
Package: clauerlinux
Version: $VERSION
Section: utils
Priority: optional
Maintainer: Paul Santapau <santapau@sg.uji.es> 
EOF

case "$1" in
ubuntu-hardy)
cat >>control<<-EOF
	Depends: libc6 (>= 2.7-1), libssl0.9.8 (>= 0.9.8f-5)
EOF
	;;

ubuntu-maverick)
cat >>control<<EOF
  Depends: libc6 (>= 2.8~20080505), libssl0.9.8 (>= 0.9.8g), libnss3-1d (>=3.12.0.3)
EOF
        ;;

ubuntu-intrepid)
	cat >>control<<EOF
  Depends: libc6 (>= 2.8~20080505), libssl0.9.8 (>= 0.9.8g), libnss3-1d (>=3.12.0.3)
EOF
        ;;

ubuntu-jaunty)
        cat >>control<<EOF
Depends: libc6 (>= 2.8~20080505), libssl0.9.8 (>= 0.9.8g), libnss3-1d (>=3.12.0.3)
EOF
        ;;
debian-etch)
         cat >>control<<EOF
Depends: libc6 (>= 2.3.6-6), libssl0.9.8 (>= 0.9.8c)
EOF
	;;
 	debian-lenny)
         cat >>control<<EOF
Depends: libc6 (>= 2.7-15), libssl0.9.8 (>= 0.9.8g-13)
EOF
	;;
  debian-squeeze) 
         cat >>control<<-EOF
Depends: libc6 (>= 2.7-15), libssl0.9.8 (>= 0.9.8g-13)
EOF
        ;;
	debian-sid)
          cat >>control<<EOF
Depends: libc6 (>= 2.7-1), libssl0.9.8 (>= 0.9.8f-5)
EOF
	;;
    *)
	echo "[ERROR] Unknown debian flavor $1"
	exit -1	
     ;;
esac

cat >>control<<EOF
Architecture: i386
Description: ClauerLinux is a set of tools for handling x509 certificates from a USB device as a cryptographic software device. The package contains a simplified operating system (clos), a set of tools (clauer-utils) and an implementation of the pkcs#11 standard well tested with Mozilla/Ice* family browsers and Thunderbird mail user agent. 
EOF


cat >copyright<<EOF
  This package was debianized by Paul Santapau <santapau@sg.uji.es> on
  Mon Nov 10 18:57:23 CET 2008.

  Copyright:
   
   Universitat Jaume I (www.uji.es)
EOF

cat >changelog<<EOF 
 $(cat $PACKAGE/package/ChangeLog)
EOF

cat >postinst<<EOFP
#!/bin/bash



function unins_mano () {
  xargs rm -f <<-EOF
	/usr/local/lib/libCRYPTOWrap.a
	/usr/local/lib/libCRYPTOWrap.la
	/usr/local/lib/libCRYPTOWrap.so
	/usr/local/lib/libCRYPTOWrap.so.0
	/usr/local/lib/libCRYPTOWrap.so.0.0.0
	/usr/local/lib/libIMPORT.a
	/usr/local/lib/libIMPORT.la
	/usr/local/lib/libIMPORT.so
	/usr/local/lib/libIMPORT.so.0
	/usr/local/lib/libIMPORT.so.0.0.0
	/usr/local/lib/libRT.a
	/usr/local/lib/libRT.la
	/usr/local/lib/libRT.so
	/usr/local/lib/libRT.so.0
	/usr/local/lib/libRT.so.0.0.0
	/usr/local/lib/libclauerpkcs11.a
	/usr/local/lib/libclauerpkcs11.la
	/usr/local/lib/libclauerpkcs11.so
	/usr/local/lib/libclauerpkcs11.so.0
	/usr/local/lib/libclauerpkcs11.so.0.0.0
	/usr/local/lib/libclio.a
	/usr/local/lib/libclio.la
	/usr/local/lib/libclio.so
	/usr/local/lib/libclio.so.0
	/usr/local/lib/libclio.so.0.0.0
	/usr/local/lib/libpkcs11.a
	/usr/local/lib/libpkcs11.la
	/usr/local/lib/libpkcs11.so
	/usr/local/lib/libpkcs11.so.0
	/usr/local/lib/libpkcs11.so.0.0.0
	/usr/local/bin/cldel
	/usr/local/bin/clexport
	/usr/local/bin/climport
	/usr/local/bin/clls
	/usr/local/bin/clmakefs
	/usr/local/bin/clos
	/usr/local/bin/clos.log
	/usr/local/bin/clpasswd
	/usr/local/bin/clview
	/usr/local/bin/clwblock
	/usr/local/bin/fclauer
	/usr/local/bin/firefox-install-pkcs11.sh
	/usr/local/bin/firefox-uninstall-pkcs11.sh
	/usr/local/bin/makeclos-rc
	/usr/local/bin/unmakeclos-rc
	EOF
}


cat > /etc/init.d/clos <<EOF
#!/bin/bash

# mm at nisu.org 2004
# modified by psn 2006
# use at your own risk

# chkconfig: 2345 95 20
# description: Clauer Os Service
# processname: clos


### BEGIN INIT INFO
# Provides: 		defaultdaemon
# Required-Start: 	\\\$local_fs \\\$network
# Required-Stop: 	\\\$local_fs \\\$network
# Default-Start: 	2 3 4 5
# Default-Stop: 	0 1 6 
# Short-Description: 	Start daemon at boot time
# Description:Enable service provided by daemon.
### END INIT INFO


case "\\\$1" in
    start)
        echo  "Starting clos ..."
        if [ ! X"\\\$(pidof clos)" = X"" ]
          then
            echo "clos is already running, try doing clos restart"
            exit 1
        fi

        /usr/sbin/clos

        if [ ! X"\\\$(pidof clos)" = X"" ]
           then
              echo "clos started successfully"
              exit 0
        fi
        ;;
    stop)
        echo  "Stopping clos ..."
        if [ ! X"\\\$(pidof clos)" = X"" ]
           then
               kill \\\$(pidof clos)
           else
               echo "It seems like clos is not running"
        fi
        ;;
    restart)
        echo  "Stopping clos ..."
        if [ ! X"\\\$(pidof clos)" = X"" ]
           then
               kill \\\$(pidof clos)
           else
               echo "It seems like clos is not running"
        fi
        sleep 1
        echo  "Starting clos ..."

        /usr/sbin/clos

        if [ ! X"\\\$(pidof clos)" = X"" ]
           then
              echo "clos started successfully"
              exit 0
        fi
        ;;
    *)
        echo "Usage: \\\$0 {start|stop|restart}"
        exit 1
        ;;
esac
EOF

ln -s /usr/lib/libclauerpkcs11.so.0.0.0 /usr/lib/libclauerpkcs11.so
chmod u+x /etc/init.d/clos
echo "Adding clos to startup runlevels ..."
update-rc.d clos defaults >/dev/null
export LDCONFIG_NOTRIGGER="notrig"
/sbin/ldconfig
nohup /usr/sbin/clos > /dev/null 2>&1 

if [ -d /usr/lib/firefox-addons ]
then
  FFXDIR=/usr/lib/firefox-addons
else
  FFXDIR=$(ls -d1 /usr/lib/{iceweasel,firefox} 2>/dev/null)
fi

touch $FFXDIR/.autoreg 2>/dev/null

unins_mano

tmp=/tmp/key.deb.\$\$
cat >\$tmp <<-EOF
	-----BEGIN PGP PUBLIC KEY BLOCK-----
	Version: GnuPG v1.4.6 (GNU/Linux)

	mQGiBElhzs4RBACHoUVQYJjam6COEZDcmwePt+yPYOmxloKqzNvmTLSDFxIjIrGv
	tliesJGAubHhEoSedyaR6yDWr8fqmz3ewzfW4WgShElEBYQ5u4enXH4uL3LA8sFm
	2nIGr2W1SueGa96X//sP6xIUnKbhXt+IX0ul4B798TKc2QnnS3q7NNgE9wCgl8Hn
	lM+929LtOvnDhgwWzZzQlgcD/2PeqD2yuZKCge2Xo/cYCfNeD0D6fXjjc4t1P42U
	NTE6aqMHaBMYbuBhh59KtEUybjXUYWBcLndQJjJWYNRb5BLku83srOoAdNVuupkw
	mAeI9vPNGCNMgHyNRIY+H0xxGMuLaw3ea1I/qK1eg09CKKb+brORaEdgNIdCFpCy
	B13AA/9i+7dFVTp7HU2WI7jss4lPvNn55EQ9iWCR6vJ2d9ZwK7nSG0pl2UIoM/iK
	mURgVamZ/1HzcbYhLzzJK8ZKtd/oBv+GJJn6TX8MeVyx7pNWtTHl/0oozKqxuSmF
	ceNN9vLqgpqqpTl0XHo+8c2oz17eqfbdeVgMvxYPIKvBABNHirRCUGF1bCBTYW50
	YXBhdSAoQ2xhdWVyIHBhY2thZ2UgcmVwb3NpdG9yeSBrZXkpIDxzYW50YXBhdUBz
	Zy51amkuZXM+iGYEExECACYFAklhzs4CGwMFCQeEzgAGCwkIBwMCBBUCCAMEFgID
	AQIeAQIXgAAKCRCMUozkmDL2ttDXAJ91k0x4cpEA4GGC2goSxwP99COsRACfXNmV
	fG+ijil7zQb164H1v7Ri+9u5Ag0ESWHO3RAIAOZF9IWc7KUpBcfNt19S/JHIxtbQ
	9YkmzVWBAuXCsXdA1L2HXj9CVYUaTvKI/q/Ug9OsAHyI1oJRCPLo6Cw/HnZpwlXm
	qZud35wv6tcoA/2iI19wRi35iTXfb3IVepU160gdnqbJjdg1e8cPCwpyMl/BeWH2
	aCcFT6UuF8qnKZbhjxOSbs+IDathGyilLs32T32m5q2ebJckRDZCjz+4BNbLbm+C
	0Deae093PVz3ZvTWQF4bkVU94uRBz8HpiqV1jYsFzBVCer79NlWRMrCVZYCcbNC7
	Mn5VDBNFD20pnQC1LQ0waYpWdDBQNQrVlM7Twm8SRNdOu41rhjTZjBpFn6MAAwYH
	/jEWGg04FHnKsNrvS9zSjzdEa4QuQv8UnZg1RoBdcCjgGENWt229QE2otgfngHO6
	2nHjLj+1Y5ZFvAINBcWO3SqHRUIcao7MF1aONrlZq9cYRYSB0STq1AtR93olJ8Iv
	bdpWGoPTl7mvJPxwyloFV/sJHJsHJyx1OgJPZ65Wo7QhsgxhWcGu4YZhg4dTt+sa
	MfNh7eqa4VL2Dnag7XcGPeB9TBt/wpBZFfMPslIKjkx8DmLdIScw5DNC2BnGxV97
	Gfec2WOiLdmnTh/N5e5bXEKhd6OKE4XJNu2vtpKhe/+QN02l5tjkMEZSGemK26xx
	qnuBFL2c8/v9iMb7q642MVGITwQYEQIADwUCSWHO3QIbDAUJB4TOAAAKCRCMUozk
	mDL2tniSAJwO/H2t8tR996fjj2304gAx/rGIzACcCwzIgwHX0kvQ4MYxsjTsy1Ox
	XRE=
	=EdaA
	-----END PGP PUBLIC KEY BLOCK-----
	EOF


dist="$1"
prov="$2"

case "\$dist" in
ubuntu-maverick)                                                                                                                               
   cfg="deb http://paul.nisu.org/clauer/dists/\$prov/ubuntu/ maverick universe"
  ;;   
ubuntu-hardy)
   cfg="deb http://paul.nisu.org/clauer/dists/\$prov/ubuntu/ hardy universe"  
  ;;
ubuntu-intrepid)
  cfg="deb http://paul.nisu.org/clauer/dists/\$prov/ubuntu/  intrepid universe"
  ;;
debian-etch)
  cfg="deb http://paul.nisu.org/clauer/dists/\$prov/debian/  etch contrib"
  ;;
debian-lenny)
  cfg="deb http://paul.nisu.org/clauer/dists/\$prov/debian/  lenny contrib"
  ;;
debian-squeeze)
  cfg="deb http://paul.nisu.org/clauer/dists/\$prov/debian/  squeeze contrib"
  ;;
debian-sid)
  cfg="deb http://paul.nisu.org/clauer/dists/\$prov/debian/  sid contrib"
  ;;
esac


echo "\$cfg" > /etc/apt/sources.list.d/clauer.list &&
apt-key add \$tmp && rm \$tmp
EOFP

cat >postrm<<EOF
#!/bin/bash
if [ -f /etc/init.d/clos ]
then 
  /etc/init.d/clos stop 
  rm /etc/init.d/clos 
  echo "Removing links from run levels ..."
  update-rc.d -f clos remove >/dev/null
fi 

if [ -f /etc/apt/sources.list.d/clauer.list ]
then 
  rm /etc/apt/sources.list.d/clauer.list
fi 

if [ -f /usr/lib/firefox-3.0.5/.autoreg  ]
then
   touch /usr/lib/firefox-3.0.5/.autoreg 2>/dev/null
fi
unlink /usr/lib/libclauerpkcs11.so

ldconfig
EOF

chmod 0775 postinst 
chmod 0775 postrm 

cat >conffiles<<EOF
/etc/clos.conf
EOF

}


create_fedora_spec(){
 cat > rpmbuild/SPECS/ClauerLinux.spec<<-EOF

Name:           ClauerLinux
Version:        $VERSION
Release:        0.2.$1%{?dist}
Summary:        Clauer, an USB based cryptographic software device.

Group:          Applications/System
License:        UJI non-commercial
URL:            http://proyectostic.uji.es/pr/clauer/
Source0:        http://proyectostic.uji.es/pr/clauer/getdown.php/ClauerLinux-3.0.6.tar.gz
BuildRoot:      %{_tmppath}/%{name}-%{version}-%{release}-root-%(%{__id_u} -n)

BuildRequires: openssl-devel >= 0.9.7, gcc-c++ >= 4.0, libstdc++-devel >= 4.0 
Requires: openssl >= 0.9.7, libstdc++ >= 4.0, nss >= 3.12.2.0, xulrunner >= 1.9.0.10
#Requires: openssl >= 0.9.7, libstdc++ >= 4.0, nss >= 3.12.2.0, xulrunner >= 1.9.0.10

%description
This is a software that convert a single CDROM or an USB flash disk into an authentication tool capable to perform authentication with several levels of secrity, in particular with X509 certificates.

%prep
%setup -q


%build
%configure
make %{?_smp_mflags}
(cd /home/clauer/clauerMan/source/clauerManagerFirefox/ && /bin/bash ff3install.sh \$(cat /home/clauer/pers) -pcre -i )


%install
rm -rf \$RPM_BUILD_ROOT
make install DESTDIR=\$RPM_BUILD_ROOT
mkdir -p  $2/usr/lib/firefox-3.6/extensions/clauerManager@uji.es
cp -r /usr/lib/firefox-3.6/extensions/clauerManager@uji.es  $2/usr/lib/firefox-3.6/extensions/
cp /home/clauer/clauerPK11inst.xpi /usr/bin/

%clean
rm -rf \$RPM_BUILD_ROOT
rm -rf /usr/lib/firefox-3.6/extensions/clauerManager@uji.es

%files
/usr/bin/cldel
/usr/bin/clexport
/usr/bin/climport
/usr/bin/clls
/usr/bin/clmakefs
/usr/bin/clpasswd
/usr/bin/clview
/usr/bin/clwblock
/usr/bin/makeclos
/usr/bin/unmakeclos
/usr/lib/libCRYPTOWrap.a
/usr/lib/libCRYPTOWrap.la
/usr/lib/libCRYPTOWrap.so
/usr/lib/libCRYPTOWrap.so.0
/usr/lib/libCRYPTOWrap.so.0.0.0
/usr/lib/libIMPORT.a
/usr/lib/libIMPORT.la
/usr/lib/libIMPORT.so
/usr/lib/libIMPORT.so.0
/usr/lib/libIMPORT.so.0.0.0
/usr/lib/libRT.a
/usr/lib/libRT.la
/usr/lib/libRT.so
/usr/lib/libRT.so.0
/usr/lib/libRT.so.0.0.0
/usr/lib/libclauerpkcs11.a
/usr/lib/libclauerpkcs11.la
/usr/lib/libclauerpkcs11.so
/usr/lib/libclauerpkcs11.so.0
/usr/lib/libclauerpkcs11.so.0.0.0
/usr/lib/libclio.a
/usr/lib/libclio.la
/usr/lib/libclio.so
/usr/lib/libclio.so.0
/usr/lib/libclio.so.0.0.0
/usr/sbin/clos
/usr/sbin/fclauer
/usr/lib/firefox-3.6/extensions/clauerManager@uji.es/install.rdf
/usr/lib/firefox-3.6/extensions/clauerManager@uji.es/defaults/preferences/clauerprefs.js
/usr/lib/firefox-3.6/extensions/clauerManager@uji.es/chrome/clauermanager.jar
/usr/lib/firefox-3.6/extensions/clauerManager@uji.es/chrome.manifest
/usr/lib/firefox-3.6/extensions/clauerManager@uji.es/components/IclauerCrypto.xpt
/usr/lib/firefox-3.6/extensions/clauerManager@uji.es/components/IclauerExposer.xpt
/usr/lib/firefox-3.6/extensions/clauerManager@uji.es/components/clauerExposer.js
/usr/lib/firefox-3.6/extensions/clauerManager@uji.es/platform/Linux_x86-gcc3/components/clauerCrypto.so



%defattr(-,root,root,-)
%doc AUTHORS NEWS ChangeLog README THANKS LICENCE


%post
/usr/bin/makeclos /usr/sbin
/etc/init.d/clos start
/usr/bin/chcon -t textrel_shlib_t '/usr/lib/firefox-3.6/extensions/clauerManager@uji.es/platform/Linux_x86-gcc3/components/clauerCrypto.so'
touch /usr/lib/firefox-3.6/.autoreg

%preun
/etc/init.d/clos stop
/usr/bin/unmakeclos
rm -rf /usr/lib/firefox-3.6/extensions/clauerManager@uji.es

%changelog
* Fri Mar 13 2009 Paul Santapau <santapau@sg.uji.es> 0.1%{?dist}
- Build the RPM release.
EOF

}

#if [ $# -ne 1 ]
#then 
# echo "Usage: $0 distro [unpacked_source_dir]" 
# echo "       where distro is one of:      "
# echo "                    debian-etch|debian-lenny|debian-squeeze|debian-sid"
# echo "                    ubuntu-hardy|ubuntu-intrepid"
# echo "                    fedora-10| ...           " 
# exit -1
#fi



#PACKAGE="$PWD/$2"
PACKAGE="/home/clauer/package"
CURDIR="$PWD"
CLAUERMAN="/home/clauer/clauerMan"

if [ -f dist ]
then 
 DIS=$(cat dist)
else
 DIS=$1
fi

#We are in a chroot so mount the proc! 
mount -t proc proc proc 
# try to mount the devpts filesystem
mount -t devpts devpts /dev/pts/

cd $HOME 

case "$DIS" in
	ubuntu-*|debian-*)
		#./packageIt.sh 
		RDIR="$PWD/clauerlinux-$VERSION"
		mkdir -p $RDIR  
		(cd $PACKAGE && ./configure && make clean && make)
		mkdir -p $RDIR/usr/bin
    mkdir -p $RDIR/usr/lib
    
		#compile clauerMan for each distro
    (cd $CLAUERMAN/source/clauerManagerFirefox && 
    /bin/bash -x ff3install.sh -pcre -i) 
   
    if [ -d /usr/lib/firefox-addons/ ]                                                                                                                                                                      
    then                                                                                                                                                                                                    
      mkdir -p $RDIR/usr/lib/firefox-addons/extensions/                                                                                                                                                     
      cp -r /usr/lib/firefox-addons/extensions/clauerManager@uji.es  $RDIR/usr/lib/firefox-addons/extensions/                                                                                               
    else      
	 	  if [ -d /usr/lib/iceweasel ]
		  then 
			   mkdir -p $RDIR/usr/lib/iceweasel/extensions/  
		     cp -r /usr/lib/iceweasel/extensions/clauerManager@uji.es  $RDIR/usr/lib/iceweasel/extensions/                	
		  else 
			 mkdir -p  $RDIR/usr/lib/firefox/extensions/  
			 cp -r /usr/lib/firefox/extensions/clauerManager@uji.es  $RDIR/usr/lib/firefox/extensions/                
			fi
		fi
     
    #cp $CURDIR/insclauer.sh $RDIR/usr/bin 
    cd $RDIR
    mkdir DEBIAN 
		find $PACKAGE/clauer-utils/.libs/ -maxdepth 1  -type f -perm +0111   -exec cp {} $RDIR/usr/bin/ \;
		find $PACKAGE/  -iname "*.so.0.0.0*"  -exec cp {} $RDIR/usr/lib/ \;	
		mkdir $RDIR/etc/
    mkdir $RDIR/usr/sbin/
    #mkdir -p usr/share/pixmaps/ 
		cp $PACKAGE/clos/.libs/clos $RDIR/usr/sbin/
		cp $PACKAGE/clauer-utils/fclauer $RDIR/usr/sbin/
		cp $PACKAGE/clos/clos.conf $RDIR/etc/
    #cp $CURDIR/clauer-firefox.png usr/share/pixmaps/
    cp $PACKAGE/pkcs11/{firefox-install-pkcs11.sh,firefox-uninstall-pkcs11.sh}  $RDIR/usr/bin/
    sed -i "s/CHANGEME/\/usr\/lib/g" $RDIR/usr/bin/{firefox-install-pkcs11.sh,firefox-uninstall-pkcs11.sh}
  

    #Iterate over the different customizations and change the type in clos.conf
		for i in $(seq 0 $((${#custom[*]}-1)))
    do 
			echo -ne $i > /home/clauer/pers
			(cd DEBIAN  
			create_debian_files $DIS ${custom[$i]} )
			sed -i "s/type *= *[0-9]/type= $i/g" etc/clos.conf
			dpkg -b ../clauerlinux-$VERSION ../clauerlinux_$VERSION\_${DIS}_${custom[$i]}_i386.deb
    done 
		#rm -rf $RDIR
	;;
	     
	fedora-*|centos-*)
    ( 
      cd $PACKAGE/clos	
      LINES=$(($(grep -n  install-exec-hook Makefile.am | cut -f 1 -d ":" | tr -d '\n')-1))
      if [ $LINES -ne -1 ]
      then 
        head -$LINES Makefile.am > Makefile.am.1
        mv Makefile.am.1 Makefile.am
      fi 
    )
    (
      cd $PACKAGE/pkcs11
      LINES=$(($(grep -n  install-exec-hook Makefile.am | cut -f 1 -d ":" | tr -d '\n')-1))
			if [ $LINES -ne -1 ]
			then
				head -$LINES Makefile.am > Makefile.am.1
      fi
			mv Makefile.am.1 Makefile.am
    )

    (cd $PACKAGE && ./configure && make clean && make)
    #for each distro	 
    for i in $(seq 4 $((${#custom[*]}-1)))
    do
			echo -ne $i > /home/clauer/pers
      sed -i "s/type *= *[0-9]/type= $i/g" $PACKAGE/clos/clos.conf
      (cd $PACKAGE &&  make dist)
			rm -rf rpmbuild/SOURCES/*  rpmbuild/SPECS/*
			case "$DIS" in 
				fedora-*)
       	    create_fedora_spec ${custom[$i]}  "/home/clauer/rpmbuild/BUILDROOT/ClauerLinux-3.0.6-0.2.uclm.fc13.i386"
				;;
				centos-*)
       	    create_fedora_spec ${custom[$i]} "/var/tmp/ClauerLinux-3.0.6-0.2.uclm-root-root" 
				;;
		  esac
      cp $PACKAGE/ClauerLinux*.tar.gz rpmbuild/SOURCES/
      (
       cd rpmbuild && rpmbuild -bb SPECS/ClauerLinux.spec 
      ) 
    done
	;;

#    opensuse-11)
#	;;

	*)
		echo "ERROR: $DIS unrecognized distro."
		umount proc 
		umount /dev/pts
		exit -1 
	;;
esac 
umount proc 
exit 0 
