#!/bin/bash
h
# Fecha:       16-03-2006
# Descripci�n: Crea un paquete con todo el software para Linux
#              este paquete es gestionado por las autotools
#              autoconf, automake y libtool.
#
#             Paul Santapau Nebot 
#                 Clauer Team 


# Version of ClauerLinux Package
VERSION=3.0.6

LIBIMPORT_PATH=../Comunes/LIBIMPORT/
CRYPTOWrapper_PATH=../Comunes/CRYPTOWrapper/
LIBMSG_PATH=../Comunes/LIBMSG/
LIBRT_PATH=../Comunes/LIBRT/
CLIO_PATH=../Comunes/clio/
PACKNAME=package 

if [ -x $PACKNAME ]; 
then 
    rm -rf $PACKNAME
fi;

echo "Limpiando los proyectos"

cd pkcs11
make clean  
cd -

cd clauer-utils 
make clean 
cd - 

cd clos 
make clean
cd -


echo "Creando directorios ..."
mkdir    $PACKNAME
mkdir    $PACKNAME/clauer-utils $PACKNAME/clos 
mkdir -p $PACKNAME/pkcs11/install  
mkdir    $PACKNAME/LIBIMPORT
mkdir    $PACKNAME/CRYPTOWrapper
mkdir    $PACKNAME/LIBRT
mkdir    $PACKNAME/LIBRT/UtilBloques 
mkdir    $PACKNAME/clio

echo "Copiando contenidos de los directorios ..."

cp ../Comunes/blocktypes.h          $PACKNAME 
cp ../Comunes/clauer-utils/src/*               $PACKNAME/clauer-utils/
cp ../Comunes/clauer-utils/po/*.mo             $PACKNAME/clauer-utils/
cp ../Comunes/clos/src/*                       $PACKNAME/clos/
cp ../Comunes/pkcs11/src/*                     $PACKNAME/pkcs11/
cp ../Comunes/pkcs11/0LEEME                    $PACKNAME/pkcs11/
cp ../Comunes/installClauerPK11Module/clauerPK11inst.xpi $PACKNAME/pkcs11/
cp $LIBIMPORT_PATH/*.{c,h}          $PACKNAME/LIBIMPORT
cp $CRYPTOWrapper_PATH/*.{c,h}      $PACKNAME/CRYPTOWrapper
cp $LIBRT_PATH/*.{c,h}              $PACKNAME/LIBRT
cp $LIBRT_PATH/UtilBloques/*.{c,h}  $PACKNAME/LIBRT/UtilBloques
cp $LIBMSG_PATH/libMSG.{c,h}        $PACKNAME/LIBRT
cp $CLIO_PATH/*.{c,h}               $PACKNAME/clio
cp $LIBRT_PATH/log.{c,h}            $PACKNAME/clio




echo "Creando ficheros README AUTHORS NEWS THANKS ChangeLog"
cd $PACKNAME

find -name makefile -exec rm -f {} \;

#Algunos arreglos en la estructura del dir
cp LIBRT/UtilBloques/*.{c,h} LIBRT/
sed -i "s/UtilBloques\/UtilBloques.h/UtilBloques.h/g" LIBRT/libRT.h
sed -i "s/LIBMSG\/libMSG.h/libMSG.h/g" LIBRT/libRT.h
sed -i "s/<libMSG.h>/\"libMSG.h\"/g" LIBRT/libRT.h

rm -rf LIBRT/UtilBloques/
rm -f CRYPTOWrapper/makefile LIBMSG/makefile LIBRT/makefile 

cat > README.MAC<<EOF
 
  Notas para los usuarios de Machintosh:
   
    Actualizaci�n: Estamos trabajando en una nueva versi�n para MAC-Intel
           de forma que se distribuir� compilada y con  un asistente para 
           la   instalaci�n,   aunque  no  hay  fecha  prevista  para  el 
           lanzamiento, se prevee en unos meses.
 
    A dia de hoy no existe soporte oficial por parte del equipo técnico 
    del clauer para este  software  funcionando bajo equipos Machintosh, 
    no  obstante,  es  posible  compilar  el  código  y  ejecutarlo  en 
    Machintosh con procesadores Power Pc. 
    
    Hay  que  tener  en  cuenta  que  si este código se compila bajo la 
    mencionada  arquitectura, SOLO  FUNCIONARAN  los  clauers que hayan 
    sido  creados  bajo la misma, y estos a su vez no serán compatibles 
    con el  software  que se ejecute  bajo una máquina con procesadores 
    Intel.

    El objetivo a corto  plazo de las pruebas de compilación es ofrecer 
    soporte para los nuevos Mac-Intel.

  Las instrucciones que se detallan a continuación han sido probadas 
  bajo el sistema operativo Mac OsX 10.4.7

  
  Instrucciones:
    
    Para compilar el software en un MAC, realiza los pasos:
	
	./configure
	make 
    
    Si todo ha compilado de forma correcta, tendrás en el directorio 
    clos un ejecutable con el mismo nombre, ejecutalo:
	
	cd clos 
	./clos 
	
    Copia el módulo Pkcs\#11 a /usr/lib ;
	
	cp pkcs11/.libs/libclauerpkcs11.0.0.0.dlyb /usr/lib/libclauerpkcs11.dlyb
	
    Ejecuta el Firefox, ves a las preferencias ->  Avanzado -> Seguridad 
    ->  Administrar  ->  Dispositivos  de seguridad -> Cargar y busca el 
    módulo pkcs11, una vez cargado, debería funcionar.

    En  el  directorio clauer-utils, existe un conjunto de utilidades que 
    te permitirán crear clauer bajo MAC. Por ejemplo para crear un clauer 
    nuevo, inserta un clauer Usb ya particionado y ejecuta el comando:
	
	clauer-utils/mkfscrypto /dev/diskXs4 password
	
    Donde  /dev/diskXs4 es la cuarta partición de tu clauer, cambia la X por 
    el número de dispositivo que le ha asignado el sistema a tu Usb.
    
    Seguidamente importa cuantos certificados quieras mediante el comando:
	
	clauer-utils/climport fichero.p12
	
    Siendo fichero.p12 un Pkcs\#12 con tus certificados.
EOF

cat >README<<EOF


    * Nota acerca de la versión
	Esta versión es ya estable.
 
    * Compilaci�n
	(Como root)
	Para compilar el pkcs11 para el Clauer deberemos efectuar los
	siguientes pasos:

	$ tar zxvf ClauerLinux-x.y.tar.gz
	$ cd ClauerLinux-x.ydir

	$ ./configure
	$ make
	$ make install

	Esto har� que la librer�a se instale en nuestro sistema, comunmente en
	/usr/local/ si no indicamos nada mediante la opci�n --prefix en el
	comando configure.

	Al instalarlo por primera vez, el sistema  operativo del clauer no se estar� ejecutando,
	por tanto, la primera vez, es necesario hacer
	\# /etc/init.d/clos restart

	NOTA 64 bits: Para compilaci�n bajo x86_64, debemos invocar el configure con la 
		      opción --enable-64 para que utilize las bibliotecas bajo /usr/lib64 
		      y pase una macro al compilador ( x86_64 ) que "normalize" el tipo long. 


    * Instalaci�n en Mozilla Firefox y en Mozilla Thunderbird
	(Como usuario)
	- Mozilla Firefox

	    Para instalar el m�dulo en Mozilla Firefox ejecuta como usuario convencional

	    $ firefox-install-pkcs11.sh

	    Firefox te preguntar� sobre la instalaci�n de un nuevo m�dulo, aceptalo.

	- Mozilla Thunderbird

	    Para Instalar el m�dulo en Mozilla Thunderbird, ejecuta Tunderbird, ve
	    al men� Editar-> Preferencias -> Advanced -> Manage Security Devices
	    y haz click sobre la opci�n Load de la derecha, selecciona el m�dulo
	    pkcs11 de la ruta en la que lo hayas instalado (por defecto /usr/local)
	    y dale a aceptar.

	    Otra forma de instalaci�n m�s avanzada, para evitar que thunderbird te 
	    pregunte por la contrase�a al visualizar tus certificados y s�lo lo haga
	    al acceder a la llave privada es la siguiente:
		- Ejecuta Mozilla Thunderbird.
		- Haz click en la pesta�a de herramientas y click nuevamente en consola JavaScript.
		- Introduce la siguiente l�nea en el campo de texto "evaluar":
		  
		    pkcs11.addmodule("Modulo pkcs11 Clauer", "/usr/local/lib/libclauerpkcs11.so", 0x1<<28, 0);
		    
		  y pulsar enter. Esta l�nea le indica al firefox que instale el m�dulo pkcs11 
		  que se encuentra en /usr/local/lib/libclauerpkcs11.so ( deberas cambiarlo si pasaste 
		  --prefix al configure ), y que le ponga el flag PKCS11_PUB_READABLE_CERT_FLAG
 		  (0x1<<28) que indica que thunderbird no debe preguntar por la contrase�a sino es
		  para acceder a la llave privada.



    Por Pa�l Santapau Nebot.
    santapau@sg.uji.es
EOF



cat >AUTHORS<<EOF 
    Manuel Mollar   mm@nisu.org
    Francisco Arag� paco@nisu.org
    Juan Segarra    juan@nisu.org
    Mauro Esteve    maesteve@sg.uji.es
    Paul Santapau   santapau@sg.uji.es 
EOF

cat >ChangeLog <<EOF

  * ClauerLinux 3.0.4 (17-02-09)

      Clauer Manager for Linux packages

  * ClauerLinux 3.0.3 (18-11-08)

       LIBIMPORT: 
            
           A�adida alguna funci�n extra para compatibilidad con componente ffox
           
       pkcs#11: 
          
           Establece FriendlyName a FIRMA/CIFRADO ante un C_CreateObject.

       clos:

           El fichero de configuraci�n se instala/desinstala correctamente.


  * ClauerLinux 3.0.2 (08-10-08)
 
       clos:

          Implementada funci�n de formateo de partici�n criptog�fica 
          en linux.
 
          Implementada funci�n de retorno de versi�n y tipo del software del clauer 
          instalado en la m�quina. El tipo puede ser utilizado por aplicaciones 
          externas y se lee directamente del fichero de configuraci�n del clos.

          Implementado an�lisis de fichero de configuraci�n, por el momento con estos 
          valores: 

	      type= 1          #indica el tipo de software. 
	      version= 3.0.2   #indica la versi�n actual del clauer.


       clauer-utils: 
           
          Corregidos bugs menores. 

                   

  * ClauerLinux 3.0.1 (22-11-07)
       
         Corregido bug en pkcs11. Si un certificado se encontraba en el 
         bloque posici�n 0 del clauer, �ste lo ignoraba.  
 
  * ClauerLinux 3.0.0 (03-07-07)
  
         Mejoras generales de estabilidad, unificaci�n de versiones 
         para Windows/Linux/Mac.

  * ClauerLinux 1l ( 14-05-07 )
 
         pkcs#11: 
             
               Corregido bug al tratar con algunos certificados con serial 
               number de m�s de 4 bytes.

 	clauer-utils.
		
 	       fclauer:
			
		   Corregido bug al formatear la partici�n sdX1, en algunas  
                   distribuciones daba error al desmontar el dispositivo.
  
  * ClauerLinux 1k ( 27-04-07 )
 
          General: 

               Mejoras notables en la gesti�n de las conexiones 
               TCP/IP en concreto, eliminaci�n de los estados 
               TIME_WAIT y CLOSE_WAIT.

          

  * ClauerLinux 1j ( 23-01-07  )

          pkcs#11:

                Implementados mecanismos:
                        CKM_SHA1_RSA_PKCS
                        CKM_RSA_PKCS_KEY_PAIR_GEN.

                Implementadas funciones:
                         C_VerifyInit
                         C_Verify

                A�adido tambi�n fichero leeme en el package.


  * ClauerLinux 1i ( 13-10-06  )
         
           Clauer-utils:
	      
	        climport:: Implementa todos los modos de cifrado de llaves (EXPERIMENTAL)
		clls::     Muestra los bloques de tipos desconocidos e implementa un nuevo
		           modo de listado raw.

	   clos:
	   
	        Corregido bug de sincronizaci�n de cach�..




  * ClauerLinux 1h ( 29-09-06  )
         
           Clauer-utils:
	      
	        Soporte multilenguaje para Espa�ol, Catalan e Ingl�s.

	   clos:
	   
	        Mejoras de gestion de errores.

	
	   Recompilado en MAC.
	      
       

    
  * ClauerLinux 1g ( 10-08-2006 )

	    pkcs11:

		Implementada la funci�n C_CreateObject y de c�lculo del 
		identificador de llaves y certificados basado en el sha1 
		del m�dulo de la llave por compatibilidad con Mozilla.
		
		Mediante esta implementaci�n es posible que Mozilla genere
		llaves y las almacene en el clauer directamente as� como 
		posteriormente su/s certificado asociado/s.


  * ClauerLinux 1f ( 26-07-2006 )

	    clos:
		
		Corregido error en el comportamiento frente a errores de chach�.
		
		Mejorado el script rc de inicio.
    
  * ClauerLinux 1e ( 25-07-2006 )

	    clos:
		
		Añadida opci�n del socket SO_REUSEADDR para que omita la comprobaci�n
		de conexiones en TIME_WAIT cuando hace bind.

	    clauer-utils:
		
		Correcci�n de algunos errores, añadidos c�digos de error al salir 
		e implementadas las opciones para facilitar passwords desde la l�nea
		de comandos de modo no seguro.

	    libRT:
	    
		Añadidas llamadas a Regenerar Cache antes de realizar escrituras.


  * ClauerLinux 1d
    
		Primera compilación en Mac con procesadores Power Pc, a pesar de 
		no dar soporte oficial, el c�digo es compilble en este tipo de 
		m�quinas, el objetivo de esto es dar soporte para los actuales 
		Mac-intel.

	LIBRT:
		
		Mejora de implementaci�n de gesti�n de cach�.

	pkcs\#11:
	
		Corregido error en LoadClauerObjects que daba SIGSEGV.
    
  * ClauerLinux 1c
	
	clos: 
		Corregidos problemas de detecci�n de dispositivos por el uso de 
		la caché.
	
  * ClauerLinux 1b ( 09-06-06 )

        clos:
	   
		A�adido soporte redhat para el script de instalaci�n.  ( gracias la parche de Jos� Traver )

 
  * ClauerLinux 1a ( 06-06-06 )
	
	clos: 
		Mejorada la detecci�n de dispositivos conectados al
 		sistema.
	
	Corregidos varios bugs menores.
	
  * ClauerLinux 1 ( 26-05-2006 )
	 
	  Primera versi�n estable.
         
	  clos:
	  
		Implementadas opciones -h, -c y -f, hacer clos -h 
		para ver su significado.
	  
          fclauer:
		Corregidos varios problemas con formateo y automount.          
  
	  clauer-utils:

		Corregidos varios errores e implementaci�n de opciones en las 
		utilidades.
	    
	  pkcs11:
	  
	    clauer.cpp:
		    
		Corregido bug en la detección del cambio de clauer :: getRSAKey().
	 

  * ClauerLinux 0.1f ( 03-05-2005 ) 
		
	clauer-utils:
	    
	    misc.c:
 	
		Ahora muestra como propietario el Subject del primer certificado
		que encuentra en el clauer.
		
	
	pkcs#11:
            clauer.cpp:

                Cambiado valor de macro en if OPENSSL_VERSION_NUMBER al de la
                versión openssl-0.9.7j, la �ltima versi�n que no pasa
                par�metro 2 de la funci�n i2d_X509 como const.


	clauer-utils:
		
	    fclauer:
		  
		Corregido error al introducir passwords que contienen espacios.
	
	clos:
		Adaptados tipos long para compatibilidad 64 bits.
 
	libRT:
		Adaptados tipos long para compatibilidad 64 bits.
		
	clio:
		A�adida biblioteca clio de entrada/salida.
	
	configure.in: 
		
		A�adido flag ( --enable-64 ) para 64 bits que realiza un 
		setup de compilación para arquitecturas IAx86_64 
		

		
  * ClauerLinux 0.1e ( 06-04-2006 )
    
	clauer-utils:
	    
	    clpasswd:
		
		Corregido problema de liberaci�n de memoria 
		por el cual sucedia un SIGSEV.
	
	clos:
	    
	    Corregido error de cambio de password: Si en una misma sesi�n, se realizaba
	    m�s de un cambio de password, siendo el nuevo password de tama�o superior al
	    password anterior, se daba un error en la liberaci�n de memoria.

	pkcs11:
	
	    Ahora permitimos sesiones R/W para poder cambiar la password. Por tanto 
	    desde el men�:  

		Edit->preferences->Security Devices->Clauer USB Token 

	    podemos cambiar la password.
	

  * ClauerLinux 0.1d ( 04-04-2006 )
    
	clauer-utils:
	
	    climport:
	
		Corregido problema en liberaci�n de memoria, daba SIGSEGV
		si hab�a m�s de un clauer en el sistema.
	
	pkcs11:
	    
		Corregido bug al realizar la firma por el cual fallaba el proceso
 		de firma al tener más de un clauer conectado en el sistema.
		
		Incluido soporte para libcrypto.so.0.9.8 mediante 
		#if (OPENSSL_VERSION_NUMBER <=  0x0090707fL);

		Corregido bug al pedir la password, además de no volver a preguntar 
		la password al introducir una password incorrecta, si despues de 
		introducir una password incorrecta se introducia la correcta, el proceso
		de firma fallaba.

	CRYPTOWrapper:
		
		Corregido error al situar el Friendly Name en el bloque del certificado
		correspondiente.
	
		Corregidos warnings de compilaci�n por casting de tipos.


  * ClauerLinux 0.1c ( 03-04-2006 )
    
	clauer-utils:
	
	    Portada la LIBIMPORT a Linux, implementada al utilidad climport
	    para importar pkcs12 al clauer desde Linux. 


  * ClauerLinux 0.1b

	
	firefox-install-pkcs11.sh:
	    
	    Modificado par�metro de instalaci�n, ahora no pide la password del 
	    Clauer para ver los certificados, sino que lo hace �nicamente al
	    utilizar la llave privada.

	
	README:
	    
	    Añadidas nuevas instrucciones de instalación para mozilla-thunderbird,
	    de forma que el pkcs11 solicite la contraseña únicamente frente a usos 
	    de la llave privada.
    
	
	Corregidos algunos errores de la instalaci�n:
	    fclauer -> no encontraba mkfscrypto a pesar de estar instalado
	    Algunos ejecutable, como clos, deben estar en sbin.

  	
	Configure.in:
	    
	    Ahora checkea tambi�n que este instalada la librer�a pthreads
	    dados el nuevo sistema de detecci�n de clauers conectados 
	    del pkcs11 en el cual esa tarea la lleva un thread.
	   
 
	pkcs11:
	    
	    Modificada la detecci�n de clauers conectados a la m�quina,
	    se pasa de chequeo frente a uso de objetos a chequeo 
	    mediante un timer que comprueba la existencia de un clauer
	    conectado a la máquina cada n segundos.
	    
	    Tambi�n se ha cambiado la filosof�a frente a los slots, antes 
	    un slot ten�a siempre un token presente y se devolvian objetos 
	    en funci�n de si este estaba realmente conectado o no. Ahora 
	    cuando no existe clauer conectado a la m�quina, el slot pasa
	    a no tener token presente.
	    
	    Corregidos timeouts en las conexiones por culpa de iniciar dispositivo
	    sin finalizarlo.
	    
	    En esta versi�n insertamos tambi�n objetos llave p�blica a pesar de 
	    poder extraerse del certificado, ya que para que firefox sepa que 
	    dispone del certificado y las dos llaves sin pedir contrase�a, es 
	    necesario que "encuentre" tres objetos (certificado, llave p�blica
	    y llave privada) con el mismo CKA_ID.
	    

	clos:
	    
	    Un error en la programaci�n hac�a que clos no mostrara su pid 
	    por la salida estandar al ser ejecutado y que por tanto el 
	    /etc/init.d/clos no funcionara frente a un stop, ya que 
 	    /var/run/clos.pid no conten�a el pid del clos en ejecuci�n.
	    
	    Ahora acepta s�lo conexiones locales.


	clauer-utils:
	    
	    mkfscrypto:
		Corregidos problemas de formateo de ficheros en vez de 
		dispositivos de bloques.
    

EOF

cat > LICENCE <<EOF
        
                      LICENCIA

1. Este programa puede ser ejecutado sin ninguna restricción
   por parte del usuario final del mismo.

2. La  Universitat Jaume I autoriza la copia y  distribución
   del programa con cualquier fin y por cualquier medio  con
   la  �nica limitaci�n de que, de forma  apropiada, se haga
   constar  en  cada  una  de las copias la  autor�a de esta
   Universidad  y  una reproducci�n  exacta de las presentes
   condiciones   y   de   la   declaraci�n  de  exenci�n  de
   responsabilidad.

3. La  Universitat  Jaume  I autoriza  la  modificaci�n  del
   software  y  su  redistribuci�n  siempre que en el cambio
   del  c�digo  conste la autor�a de la Universidad respecto
   al  software  original  y  la  url de descarga del c�digo
   fuente  original. Adem�s, su denominaci�n no debe inducir
   a  error  o  confusi�n con el original. Cualquier persona
   o  entidad  que  modifique  y  redistribuya  el  software
   modificado deber�  informar de tal circunstancia mediante
   el  env�o  de  un  mensaje  de  correo  electr�nico  a la
   direcci�n  clauer@uji.es  y  remitir una copia del c�digo
   fuente modificado.

4. El  c�digo  fuente  de todos los programas amparados bajo
   esta licencia  est�  disponible para su descarga gratuita
   desde la p�gina web http//:clauer.uji.es.

5. El hecho en s� del uso, copia o distribuci�n del presente
   programa implica la aceptaci�n de estas condiciones.

6. La  copia y distribuci�n del programa supone la extensi�n
   de las presentes condiciones al destinatario.
   El  distribuidor no puede imponer condiciones adicionales
   que limiten las aqu� establecidas.

       DECLARACI�N DE EXENCI�N DE RESPONSABILIDAD

Este  programa  se  distribuye  gratuitamente. La Universitat
Jaume  I  no  ofrece  ning�n  tipo de garant�a sobre el mismo
ni acepta ninguna responsabilidad por su uso o  imposibilidad
de uso.

EOF

cat > THANKS <<EOF
Luca Olivetti for this patches and the work on Mandriva rpm package. 
Bob Relyea <rrelyea@redhat.com> Engineering Lead of nss project for solving 
				our password prompting issues.
EOF

touch NEWS


echo "Creando configure.in ..." 
cat >configure.in<<EOF
dnl configure
dnl Fichero de configuracion del pryecto clauer-linux
dnl solo falta encontrar el directorio de openssl ;-)
AC_INIT()  
AM_INIT_AUTOMAKE(ClauerLinux,$VERSION)
AC_PROG_CXX
dnl AC_ISC_POSIX
AM_PROG_LIBTOOL
dnl AC_STDC_HEADERS
AC_PROG_RANLIB
AC_PROG_INSTALL
AC_LANG_CPLUSPLUS
PACKAGE=ClauerLinux
VERSION=$VERSION
MSGS=""

dnl ***************************************************
dnl Comprobación de donde esta instalado OpenSSL
dnl adaptado directamente del configure.in de curl-ssl
dnl ***************************************************

dnl The include files 
dnl Defaultnclude files
dnl Default path
dnl Default library path

dnl check if --enable-64 is set
LIB_PATH="/usr/lib"
IN=no
AC_ARG_ENABLE(64,dnl
[  --enable-64    Enable compilation setup for 64 bit machines
],
LIB_PATH="/usr/lib64"
CPPFLAGS=-DIAx86_64
)


dnl check if --enable-32bit-emulation is set
AC_ARG_ENABLE(32bit-emulation,dnl
[  --enable-32bit-emulation    Compile to a 32 bit target on an intel  X86_64 platform 
],
IN=yes
)


if test X"\$IN" = Xyes
then
LIB_PATH="/usr/lib/"
CPPFLAGS="-m32"
LDFLAGS="-m32 -Wl,-melf_i386" 
fi

AC_MSG_RESULT(\$build)

case "\$build" in
  powerpc-apple-* )
	    CPPFLAGS="\$CPPFLAGS -DMAC"
            MSGS=$MSGS" 
  ------------------------------------ READ THIS ----------------------------------

  It  seems  like  you  are  compiling Clauer software on a Mac with a Power PC 
  processor there is no official support for Mac with such a kind of processor,
  just Mac-Intel version will be supported in short.

  If you want to go on with the compilation be aware that only the clauers that you
  build under Mac with mkfscrypto and climport will work properly and them will not 
  be usable on an Intel Systems.

  ----------------------------------------------------------------------------------
  "  ;;
    
    
  i686-apple-* | i386-apple-*)
    CPPFLAGS="\$CPPFLAGS -DMAC"
    MSGS=$MSGS" 
  ------------------------------------ READ THIS ----------------------------------

  It  seems  like  you  are  compiling Clauer software on a Mac with an Intel 
  processor there is no official support for Mac but it has been tested and
  should work properly.

  Please if this software does not configure or compile properly send and e-mail 
  with the error output produced by configure or make to santapau@sg.uji.es.

  ----------------------------------------------------------------------------------
  "  ;;



  x86_64-*-* | IA84-*-* ) 
	    MSGS=$MSGS"
  ------------------------------------ READ THIS ----------------------------------

  It seems like you are compiling Clauer software on an 64 bit architecture, you 
  have to set the flag --enable-64 for compiling on this architecture.
		 
  NOTE: On some Linux distributions, Firefox is compiled by default for 32 bit
        because  of  some plugin unavaliability on 64 bit, this will make that 
        you can not use the pkcs11 module compiled for 64 bit with this method
        you should recompile it with 32 bit simulation.

  ----------------------------------------------------------------------------------
  "  ;;
		
 i686-pc-linux-gnu )
	     MSGS=$MSGS"";;
		    
esac


SSL_INCLUDE_PATH="/usr/include/openssl"
AC_ARG_WITH(ssl-include,dnl
[  --with-ssl-include= DIR    Where to look for SSL include files
                              DIR defaults  to "/usr/include/openssl"],
SSL_INCLUDE_PATH=\$withval
)

if test X"\$SSL-INCLUDE_PATH" = Xno
then
AC_CHECKING(WARNING: We should need to indicate where is openssl installed)
else
AC_MSG_CHECKING(where to look for OpenSSL)
CPPFLAGS="\$CPPFLAGS -I\$SSL_PATH/include"
INC_SSLPATH="\$SSL_INCLUDE_PATH"
AC_MSG_RESULT(\$SSL_INCLUDE_PATH)
INC_SSLPATH="-I\$SSL_INCLUDE_PATH"
fi

dnl the libraries files path
dnl default libraries path
SSL_LIBRARIES_PATH=\$LIB_PATH 
AC_ARG_WITH(ssl-libraries,dnl 
[  --with-ssl-libraries= DIR    Where to look for SSL Libraries files
 			      DIR defaults to "/usr/lib" 
],     
SSL_LIBRARIES_PATH=\$withval
)

if test X"\$SSL_LIBRARIES_PATH" = Xno
then
AC_MSG_CHECKING(WARNING: We should need to indicate where is openssl installed)
else
	
AC_MSG_CHECKING(where to look for OpenSSL)
    CPPFLAGS="\$CPPFLAGS -L\$SSL_LIBRARIES_PATH" 
LIB_SSLPATH="\$SSL_LIBRARIES_PATH" 

AC_MSG_RESULT(\$SSL_LIBRARIES_PATH)
LIB_SSLPATH="-L\$SSL_LIBRARIES_PATH"
fi


dnl pthreads existence 
PTHREAD_LIBRARY_PATH=\$LIB_PATH 
AC_ARG_WITH(pthread-library,dnl 
[  --with-pthread-library= DIR    Where to look for pthread Library files
 			          DIR defaults to "/usr/lib" 
],     
PTHREAD_LIBRARY_PATH=\$withval
)

if test X"\$PTHREAD_LIBRARY_PATH" = Xno
then
AC_MSG_CHECKING(WARNING: We should need to indicate where is pthread library installed)
else
AC_MSG_CHECKING(where to look for pthread )
CPPFLAGS="\$CPPFLAGS -L\$PTHREAD_LIBRARY_PATH";
AC_MSG_RESULT(\$PTHREAD_LIBRARY_PATH)
fi

dnl End of pthreads checking


dnl Check if we can found the needed ssl and pthread include
dnl and libreries.

LDFLAGS="\$LDFLAGS \$LIB_SSLPATH -L\$PTHREAD_LIBRARY_PATH -L\$LIB_PATH";
AC_CHECK_LIB(pthread,pthread_create,,
AC_MSG_WARN([We can't links against the pthread library])
AC_MSG_ERROR([Perhaps you need to use the --with-pthread-library directive])
) 


AC_CHECK_LIB(crypto,RSA_sign,,
AC_MSG_WARN([We can't links against the ssl library])
AC_MSG_ERROR([Perhaps you need to use the --with-ssl-libraries directive])
) 

AC_MSG_RESULT(\$CPPFLAGS)

CPPFLAGS="\$CPPFLAGS \$INC_SSLPATH" 

AC_MSG_RESULT(\$CPPFLAGS)

dnl ********************************************************
dnl             Check for openssl headers 
dnl ********************************************************

OPENSSL_HEADERS="0"

AC_CHECK_HEADERS(openssl/x509.h openssl/rsa.h openssl/asn1.h,
		    OPENSSL_HEADERS="1"
		    CPPFLAGS="\$CPPFLAGS -I\$SSL_INCLUDE_PATH/openssl/"
)

AC_CHECK_HEADERS(x509.h rsa.h asn1.h,
		    OPENSSL_HEADERS="1"
)

if test X"\$OPENSSL_HEADERS" = X"0"; then
    AC_MSG_WARN([Can't find ssl headers])
    AC_MSG_ERROR([You must specify where to look with --with-ssl-include])
fi 


AC_SUBST(LIB_SSLPATH,LIB_SSLPATH)
AC_SUBST(INC_SSLPATH,INC_SSLPATH)

AC_OUTPUT([Makefile clio/Makefile CRYPTOWrapper/Makefile LIBRT/Makefile  LIBIMPORT/Makefile clos/Makefile clauer-utils/Makefile pkcs11/Makefile])

if test \! X"\$MSGS" = X""; then
    AC_MSG_WARN([\$MSGS])
fi

EOF



echo "Creando Makefiles ..."

cat >Makefile.am<<EOF
SUBDIRS = clio CRYPTOWrapper LIBRT LIBIMPORT pkcs11 clauer-utils clos 
EXTRA_DIST = AUTHORS ChangeLog NEWS README THANKS LICENCE blocktypes.h pkcs11/firefox-install-pkcs11.sh pkcs11/clauerPK11inst.xpi clos/makeclos clos/clos.conf clos/unmakeclos clauer-utils/fclauer 
EOF


cat >clauer-utils/Makefile.am<<EOF
bin_PROGRAMS= clexport  climport  clls  clpasswd  clview  clwblock  clmakefs cldel 
sbin_SCRIPTS= fclauer
EXTRA_DIST= clauer-utils_es.mo clauer-utils_ca.mo  

clexport_SOURCES= clexport.c misc.c misc.h nls.h nls.c
climport_SOURCES= climport.c misc.c nls.h nls.c
clls_SOURCES= clls.c misc.c nls.h nls.c
clpasswd_SOURCES= clpasswd.c misc.c nls.h nls.c
clview_SOURCES= clview.c misc.c nls.h nls.c 
clwblock_SOURCES= clwblock.c misc.c 
clmakefs_SOURCES= clmakefs.c misc.c nls.c nls.h
cldel_SOURCES= cldel.c misc.c nls.c nls.h

INCLUDES= -O4 -DLINUX -I../
LDADD= ../CRYPTOWrapper/libCRYPTOWrap.la ../LIBRT/libRT.la ../clio/libclio.la -lcrypto
climport_LDADD= ../CRYPTOWrapper/libCRYPTOWrap.la ../LIBRT/libRT.la ../LIBIMPORT/libIMPORT.la -lcrypto

install-exec-hook:
	if [  -d /usr/share/locale/es/LC_MESSAGES/  ]; then cp clauer-utils_es.mo /usr/share/locale/es/LC_MESSAGES/clauer-utils.mo; fi
	if [  -d /usr/share/locale/ca/LC_MESSAGES/  ]; then cp clauer-utils_ca.mo /usr/share/locale/ca/LC_MESSAGES/clauer-utils.mo; fi             
EOF

cat >clos/Makefile.am<<EOF
sbin_PROGRAMS= clos
bin_SCRIPTS= makeclos unmakeclos

clos_SOURCES= format.c common.c auth.c block.c session.c smem.c stub.c transport.c func.c log.c main.c format.h session.h smem.h stub.h transport.h err.h func.h log.h auth.h block.h common.h

INCLUDES= -O4 -DDEBUG_CRYF -DLINUX -I../ -I../clio/
LDADD= ../CRYPTOWrapper/libCRYPTOWrap.la ../clio/libclio.la

install-exec-hook:
		    ./makeclos \$(sbindir)
		     /etc/init.d/clos start


uninstall-local:
		    ./unmakeclos
		     killall clos


EOF

cat >clos/clos.conf<<EOF
#
# Configuration file for clos
# Universitat Jaume I
#

type= 4
#version of the clauer software
version= $VERSION

EOF


cat >pkcs11/Makefile.am<<EOF
#bin_SCRIPTS = firefox-install-pkcs11.sh clauerPK11inst.xpi
lib_LTLIBRARIES = libclauerpkcs11.la
libclauerpkcs11_la_SOURCES = certif.cpp certx509.cpp clauer.cpp clave.cpp clvprvda.cpp clvrsa.cpp common.cpp lobjeto.cpp log.cpp mechanism.cpp objeto.cpp pkcs11.cpp  slot.cpp threads.cpp certif.h  certx509.h  clauer.h  clave.h  clvprvda.h  clvrsa.h  common.h  lobjeto.h  log.h  mechanism.h  objeto.h  pkcs11f.h  pkcs11.h  pkcs11t.h  slot.h threads.h

INCLUDES = -O4 -DLINUX -D_UNIX  -I../ -DLABEL_PURPOSE   # -DDEBUG
libclauerpkcs11_la_LIBADD =  -lssl -lcrypto ../CRYPTOWrapper/libCRYPTOWrap.la ../LIBRT/libRT.la

#install-exec-hook:
	#sed -i -e "s/CHANGEME/\$(subst /,\/,\$(datadir))/g" \$(bindir)/firefox-install-pkcs11.sh
	#mv \$(bindir)/clauerPK11inst.xpi \$(datadir)/  
	#if [ -L /usr/lib/libclauerpkcs11.so ];then unlink /usr/lib/libclauerpkcs11.so; fi 
	#if [ ! -L /usr/lib/libclauerpkcs11.so ];then ln -s  \$(libdir)\/libclauerpkcs11.so /usr/lib/libclauerpkcs11.so; fi 

#uninstall-hook:
	#if [ -L /usr/lib/libclauerpkcs11.so ];then unlink /usr/lib/libclauerpkcs11.so; fi 

EOF


cat >CRYPTOWrapper/Makefile.am<<EOF
lib_LTLIBRARIES = libCRYPTOWrap.la
libCRYPTOWrap_la_SOURCES = CRYPTOWrap.c CRYPTOWrap.h

INCLUDES = -O4 -fPIC -DLINUX # -DDEBUG
LIBADD = -L @LIB_SSLPATH@ -lssl -lcrypto 
EOF

cat >LIBRT/Makefile.am<<EOF
lib_LTLIBRARIES = libRT.la
libRT_la_SOURCES = libRT.c  libRT.h  LIBRTMutex.c  LIBRTMutex.h  UtilBloques.c  UtilBloques.h libMSG.c libMSG.h log.c log.h
INCLUDES = -O4 -fPIC -DLINUX -I../ # -DDEBUG
LIBADD = ../LIBMG/libMSG.la
EOF

cat >clio/Makefile.am<<EOF
lib_LTLIBRARIES = libclio.la
libclio_la_SOURCES = clio.c  clio.h log.c log.h 
INCLUDES = -O4 -fPIC -DLINUX -I../ # -DDEBUG
EOF

cat >LIBIMPORT/Makefile.am<<EOF
lib_LTLIBRARIES = libIMPORT.la
libIMPORT_la_SOURCES = LIBIMPORT.c  LIBIMPORT.h
INCLUDES = -O4 -DLINUX -I../ # -DDEBUG
LIBADD =  ../CRYPTOWrapper/libCRYPTOWrap.la ../LIBRT/libRT.la -lcrypto
EOF


# Le ponemos la licencia a todos los .h, .c y .cpp
echo "Poniendo licencia a todos los fuentes (.h,.c,.cpp) ..."
find -iregex ".*\.[ch]" -exec ../licenceMe.sh {} \;  
find -iregex ".*\.cpp"  -exec ../licenceMe.sh {} \;  

echo "Generando script configure y Makefiles ..."
cp /usr/share/libtool/config/ltmain.sh  .
aclocal
autoconf
automake-1.4 -a

cp LICENCE COPYING 

cd -
